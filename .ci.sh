#!/bin/sh

export JAVA_TOOL_OPTIONS="-Dfile.encoding=UTF-8"

# to avoid issues with character encoding and filesystems
export LANG=C.UTF-8

TAG=$(git describe --always)
echo git tag is: "$TAG"
echo MAVEN_OPTS: "$MAVEN_OPTS"
echo JAVA_TOOL_OPTIONS: $JAVA_TOOL_OPTIONS
echo LANG: $LANG

# -U to force snapshot update
make build-tests deb prep-docker-build && \
echo && \
echo OK && exit 0

echo [FAILED] && exit 1
