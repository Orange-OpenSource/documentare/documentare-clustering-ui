#!/usr/bin/env bash

VERSION=2.50.0

die() {
  local msg="$1"
  echo
  echo "🔴 [ERROR] $msg"
  echo
  exit 1
}

info() {
  local msg="$1"
  echo
  echo "💚 $msg"
  echo
}

if ! brew install autoconf automake bison pkg-config gd gts libpng librsvg libtool pango
then
  die "failed to install required build & graphviz libraries dependencies"
fi

if bison --version | grep 'bison (GNU Bison) 2.3'
then
  die "the bison version installed by brew is not in your PATH; Check version under /usr/local/Cellar/bison/ and update your PATH in your shell config file..."
fi

if ! curl -f -o graphviz-"$VERSION".tar.gz https://gitlab.com/graphviz/graphviz/-/archive/2.50.0/graphviz-"$VERSION".tar.gz
then
  die "failed to download graphviz"
fi

rm -rf /tmp/graphviz-"$VERSION" && mkdir /tmp/graphviz-"$VERSION"
if ! tar xvf graphviz-"$VERSION".tar.gz -C /tmp/
then
    die "failed to extract graphviz tarball"
fi

if ! (rm -rf "$HOME/bin/graphviz-$VERSION" && mkdir -p "$HOME/bin/graphviz-$VERSION" && cd /tmp/graphviz-"$VERSION" && \
./autogen.sh && \
./configure \
      --prefix="$HOME/bin/graphviz-$VERSION" \
      --disable-debug \
      --disable-dependency-tracking \
      --disable-php \
      --disable-swig \
      --disable-tcl \
      --with-quartz \
      --without-gdk \
      --without-gtk \
      --without-poppler \
      --without-qt \
      --without-x \
      --with-freetype2 \
      --with-gdk-pixbuf \
      --with-gts \
      && \
      make && make install
)
then
  die "failed to build graphviz"
fi

info "Add this to your shell config file (.bashrc or .zshrc): PATH=~/bin/graphviz-$VERSION/bin:\$PATH"
