#!/usr/bin/env bash

DATE=$(date "+%Y-%m-%d")


ROOT_DIR="/root"
DATA_SIMDOC_DIRNAME='.data-simdoc'
RAW_IMAGE_CACHE_DIRNAME='.data-simdoc-raw-cache-dir'

MAXWELL_BACKUP_TARBALL=maxwell_backup_"$DATE".tar
POSTGRES_ARCHIVE=dump_"$DATE".dump
POSTGRES_DB_NAME="sandboxdb"
RAW_IMAGE_CACHE_TARBALL=raw_image_cache_"$DATE".tar
DATA_SIMDOC_TARBALL=data_simdoc_"$DATE".tar

CLUSTERING_UI_CONTAINER=clustering-ui
POSTGRES_CONTAINER=postgres



die() {
  local msg="$1"
  echo
  echo "🔴 [ERROR] $msg"
  echo
  exit 1
}

warn() {
  local msg="$1"
  echo
  echo "⚠️"
  echo "⚠️  $msg"
  echo "⚠️"
  echo
}

info() {
  local msg="$1"
  echo
  echo "💚 $msg"
  echo
}

if ! docker stop "$CLUSTERING_UI_CONTAINER"; then
  die "Failed to stop container $CLUSTERING_UI_CONTAINER"
fi

if ! docker exec -ti "$POSTGRES_CONTAINER" sh -c "rm -f /tmp/$POSTGRES_ARCHIVE ; su postgres -c \"pg_dump --format=custom --file /tmp/$POSTGRES_ARCHIVE $POSTGRES_DB_NAME\""; then
  die "Failed to dump postgres database"
fi

if ! docker cp -q "$POSTGRES_CONTAINER:/tmp/$POSTGRES_ARCHIVE" .; then
  die "Failed to copy postgres backup file from container $POSTGRES_CONTAINER to launch directory"
fi
info "Postgres DB backup"

if ! docker start "$CLUSTERING_UI_CONTAINER"; then
  die "Failed to start container $CLUSTERING_UI_CONTAINER"
fi

if ! docker exec -ti "$CLUSTERING_UI_CONTAINER" sh -c "cd $ROOT_DIR && tar cf $DATA_SIMDOC_TARBALL $DATA_SIMDOC_DIRNAME"; then
  die "Failed to tar data-simdoc folder"
fi

if ! docker cp -q "$CLUSTERING_UI_CONTAINER:$ROOT_DIR/$DATA_SIMDOC_TARBALL" .; then
  die "Failed to copy data-simdoc backup file from container $CLUSTERING_UI_CONTAINER to launch directory"
fi

info "Data backup"

if ! docker exec -ti "$CLUSTERING_UI_CONTAINER" sh -c "cd $ROOT_DIR && tar cf $RAW_IMAGE_CACHE_TARBALL $RAW_IMAGE_CACHE_DIRNAME"; then
  die "Failed to tar raw_image_cache folder"
fi

if ! docker cp -q "$CLUSTERING_UI_CONTAINER:$ROOT_DIR/$RAW_IMAGE_CACHE_TARBALL" .; then
  die "Failed to copy raw_image_cache backup file from container $CLUSTERING_UI_CONTAINER to launch directory"
fi

info "Raw image cache backup"

BACKUP_LIST="$POSTGRES_ARCHIVE $RAW_IMAGE_CACHE_TARBALL $DATA_SIMDOC_TARBALL"

# split BACKUP_LIST by space; do not add "" around the variable to force word splitting
IFS=' '
if ! tar cf "$MAXWELL_BACKUP_TARBALL" $BACKUP_LIST; then
  die "Failed to tar all backup files in backup file"
else
  info "Build archive"
fi

if ! rm -f "$POSTGRES_ARCHIVE" "$RAW_IMAGE_CACHE_TARBALL" "$DATA_SIMDOC_TARBALL"; then
  die "Failed to delete all temporary backup files"
fi
