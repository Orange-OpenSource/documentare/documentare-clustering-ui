## Backup and restore scripts

1.) for **bare metal** workstations (z820, etc):
- `backup_maxwell.sh`
- `restore_maxwell.sh`

2.) when you run java server natively **on your machine**, with _postgres_ and _redis_ in _docker_:
- `backup_local.sh`
- `restore_local.sh`

3.) when you run maxwell with **docker compose**:
- `backup_docker.sh`
- `restore_docker.sh`

## Backup

### `backup_maxwell.sh`
- launch the backup: `sudo bash backup_maxwell.sh`
- result: file `maxwell_backup_"YYYY_MM-DD".tar` is generated

If an error occurs, the script will exit with an error message.

### `backup_local.sh`
- launch the backup: `bash backup_local.sh`
- result: file `maxwell_backup_"YYYY_MM-DD".tar` is generated

If an error occurs, the script will exit with an error message.

### `backup_docker.sh`
- launch the backup: `bash backup_docker.sh`
- result: file `maxwell_backup_"YYYY_MM-DD".tar` is generated

If an error occurs, the script will exit with an error message.

## Restore

### `restore_maxwell.sh`
- restore the backup: `sudo bash restore_maxwell.sh maxwell_backup_DATE.tar`
- ⚠️: keep format `maxwell_backup_DATE.tar` and do not rename the file
- argument: backup file name in the same directory as the script

If an error occurs, the script will exit with an error message.

### `restore_local.sh`
- restore the backup: `bash restore_local.sh`
- ⚠️: keep format `maxwell_backup_DATE.tar` and do not rename the file
- argument: backup file name in the same directory as the script

If an error occurs, the script will exit with an error message.

### `restore_docker.sh`
- restore the backup: `bash restore_in_docker.sh`
- ⚠️: keep format `maxwell_backup_DATE.tar` and do not rename the file
- argument: backup file name in the same directory as the script

If an error occurs, the script will exit with an error message.
