#!/usr/bin/env bash

MAXWELL_BACKUP_TARBALL="$1"

DATE=${MAXWELL_BACKUP_TARBALL:15:10}
echo "$DATE"

die() {
  local msg="$1"
  echo
  echo "🔴 [ERROR] $msg"
  echo
  exit 1
}

info() {
  local msg="$1"
  echo
  echo "💚 $msg"
  echo
}

if [ ! -f "$MAXWELL_BACKUP_TARBALL" ]; then
  die "Maxwell Backup File not found"
fi

if ! tar xf "$MAXWELL_BACKUP_TARBALL"; then
  die "Failed to extract maxwell backup file tar"
fi




ROOT_DIR="/root"
DATA_SIMDOC_DIRNAME='.data-simdoc'
RAW_IMAGE_CACHE_DIRNAME='.data-simdoc-raw-cache-dir'

POSTGRES_ARCHIVE=dump_"$DATE".dump
POSTGRES_DB_NAME="sandboxdb"
RAW_IMAGE_CACHE_TARBALL=raw_image_cache_"$DATE".tar
DATA_SIMDOC_TARBALL=data_simdoc_"$DATE".tar

CLUSTERING_UI_CONTAINER=clustering-ui
POSTGRES_CONTAINER=postgres



if [ -f "$POSTGRES_ARCHIVE" ]; then
  if ! docker stop "$CLUSTERING_UI_CONTAINER"; then
    die "Failed to stop container $CLUSTERING_UI_CONTAINER"
  fi
  if ! docker cp -q "$POSTGRES_ARCHIVE" "$POSTGRES_CONTAINER":/tmp; then
    die "Failed to copy postgres archive in container $POSTGRES_CONTAINER:/tmp"
  fi
  if ! docker exec -ti "$POSTGRES_CONTAINER" su postgres -c "pg_restore --clean --dbname $POSTGRES_DB_NAME /tmp/$POSTGRES_ARCHIVE"; then
    die "Failed to restore db"
  fi
  if ! docker restart "$POSTGRES_CONTAINER"; then
    die "Failed to restart container $POSTGRES_CONTAINER"
  fi
  if ! docker start "$CLUSTERING_UI_CONTAINER"; then
    die "Failed to start container $CLUSTERING_UI_CONTAINER"
  fi
  info "Postgres DB restored"
fi

if [ -f "$DATA_SIMDOC_TARBALL" ]; then
  if ! docker cp -q "$DATA_SIMDOC_TARBALL" "$CLUSTERING_UI_CONTAINER:$ROOT_DIR"; then
    die "Failed to copy data_simdoc backup file in container $CLUSTERING_UI_CONTAINER:$ROOT_DIR"
  fi
  docker exec -ti "$CLUSTERING_UI_CONTAINER" sh -c "rm -rf $ROOT_DIR/$DATA_SIMDOC_DIRNAME/*; rm -rf $ROOT_DIR/$DATA_SIMDOC_DIRNAME/thumbnails/*"
  if ! docker exec -ti "$CLUSTERING_UI_CONTAINER" tar xf "$ROOT_DIR/$DATA_SIMDOC_TARBALL" -C "$ROOT_DIR" --no-same-owner; then
    die "Failed to extract data_simdoc backup file in $ROOT_DIR in container $CLUSTERING_UI_CONTAINER"
  fi
  if ! docker restart "$CLUSTERING_UI_CONTAINER"; then
    die "Failed to restart container $CLUSTERING_UI_CONTAINER"
  fi
  info "Data restored"
fi

if [ -f "$RAW_IMAGE_CACHE_TARBALL" ]; then
  if ! docker cp -q "$RAW_IMAGE_CACHE_TARBALL" "$CLUSTERING_UI_CONTAINER:$ROOT_DIR"; then
    die "Failed to copy raw_image_cache backup file in container $CLUSTERING_UI_CONTAINER:$ROOT_DIR"
  fi
  if ! docker exec -ti "$CLUSTERING_UI_CONTAINER" sh -c "rm -rf $ROOT_DIR/$RAW_IMAGE_CACHE_DIRNAME/*"; then
    die "Failed to delete file in $ROOT_DIR/$RAW_IMAGE_CACHE_DIRNAME in container $CLUSTERING_UI_CONTAINER"
  fi
  if ! docker exec -ti "$CLUSTERING_UI_CONTAINER" tar xPf "$ROOT_DIR/$RAW_IMAGE_CACHE_TARBALL" -C "$ROOT_DIR" --no-same-owner; then
    die "Failed to extract raw_image_cache backup file in container $CLUSTERING_UI_CONTAINER"
  fi
  if ! docker restart "$CLUSTERING_UI_CONTAINER"; then
    die "Failed to restart container $CLUSTERING_UI_CONTAINER"
  fi
  info "Raw images cache restored"
fi

if ! rm -f "$POSTGRES_ARCHIVE" "$RAW_IMAGE_CACHE_TARBALL" "$DATA_SIMDOC_TARBALL"
then
	die "Failed to delete all temporary backup files"
else
    info "temporary tarballs cleaned up successfully"
fi
