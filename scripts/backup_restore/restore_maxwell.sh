#!/usr/bin/env bash


die() {
  local msg="$1"
  echo
  echo "🔴 [ERROR] $msg"
  echo
  exit 1
}

info() {
  local msg="$1"
  echo
  echo "💚 $msg"
  echo
}

MAXWELL_BACKUP="$1"

if [ ! -f "$MAXWELL_BACKUP" ];
then
	die "Maxwell Backup File not found"
fi

DATE=${MAXWELL_BACKUP:15:10}
info "Backup date is $DATE"

if ! tar xf "$MAXWELL_BACKUP"
then
	die "Failed to extract maxwell backup tarball"
fi


BACKUP_DATA_SIMDOC_DIRNAME='.data-simdoc'
BACKUP_RAW_IMAGE_CACHE_DIRNAME='.data-simdoc-raw-cache-dir'

SERVER_RAW_IMAGE_CACHE_DIR='/tmp'
SERVER_RAW_IMAGE_CACHE_DIRNAME='raw_image_cache'
SERVER_DATA_SIMDOC_ROOT_DIR='/'
SERVER_DATA_SIMDOC_ROOT_DIRNAME='data-simdoc'

POSTGRES_ARCHIVE=dump_"$DATE".dump
POSTGRES_DB_NAME="sandboxdb"
RAW_IMAGE_CACHE_TARBALL=raw_image_cache_"$DATE".tar
DATA_SIMDOC_TARBALL=data_simdoc_"$DATE".tar



if ! service clustering-ui stop
then
	die "Failed to stop service clustering-ui"
fi
info "clustering-ui stopped"

# postgres user can access curent directory, move archive in /tmp
if ! (cp $POSTGRES_ARCHIVE /tmp && cd /tmp && su postgres -c "pg_restore --clean --dbname $POSTGRES_DB_NAME $POSTGRES_ARCHIVE")
then
	die "Failed to restore postgres db"
fi

info "Postgres DB backup restored"

if ! rm -rf "$SERVER_RAW_IMAGE_CACHE_DIR/$SERVER_RAW_IMAGE_CACHE_DIRNAME"
then
	die "Failed to delete $SERVER_RAW_IMAGE_CACHE_DIR/$SERVER_RAW_IMAGE_CACHE_DIRNAME"
else
  info "$SERVER_RAW_IMAGE_CACHE_DIR/$SERVER_RAW_IMAGE_CACHE_DIRNAME deleted"
fi
if ! (cp "$RAW_IMAGE_CACHE_TARBALL" "$SERVER_RAW_IMAGE_CACHE_DIR/$RAW_IMAGE_CACHE_TARBALL")
then
	die "Failed to copy $RAW_IMAGE_CACHE_TARBALL in $SERVER_RAW_IMAGE_CACHE_DIR"
else
  info "$RAW_IMAGE_CACHE_TARBALL copied to $SERVER_RAW_IMAGE_CACHE_DIR/$RAW_IMAGE_CACHE_TARBALL"
fi
if ! (cd $SERVER_RAW_IMAGE_CACHE_DIR && tar xPf "$RAW_IMAGE_CACHE_TARBALL" && rm "$RAW_IMAGE_CACHE_TARBALL" && mv "$BACKUP_RAW_IMAGE_CACHE_DIRNAME" "$SERVER_RAW_IMAGE_CACHE_DIRNAME")
then
	die "Failed to extract raw images backup in $SERVER_RAW_IMAGE_CACHE_DIR"
else
  info "in dir $SERVER_RAW_IMAGE_CACHE_DIR: untar $RAW_IMAGE_CACHE_TARBALL and move $BACKUP_RAW_IMAGE_CACHE_DIRNAME to $SERVER_RAW_IMAGE_CACHE_DIRNAME"
fi

if ! rm -rf "$SERVER_DATA_SIMDOC_ROOT_DIR/$SERVER_DATA_SIMDOC_ROOT_DIRNAME"
then
	die "Failed to delete dir $SERVER_DATA_SIMDOC_ROOT_DIR/$SERVER_DATA_SIMDOC_ROOT_DIRNAME"
else
  info "dir deleted $SERVER_DATA_SIMDOC_ROOT_DIR/$SERVER_DATA_SIMDOC_ROOT_DIRNAME"
fi

if ! cp "$DATA_SIMDOC_TARBALL" "$SERVER_DATA_SIMDOC_ROOT_DIR"
then
	die "Failed to copy $DATA_SIMDOC_TARBALL backup file in $SERVER_DATA_SIMDOC_ROOT_DIR"
fi
if ! (cd $SERVER_DATA_SIMDOC_ROOT_DIR && tar xPf "$DATA_SIMDOC_TARBALL" && rm "$DATA_SIMDOC_TARBALL" && mv "$BACKUP_DATA_SIMDOC_DIRNAME" "$SERVER_DATA_SIMDOC_ROOT_DIRNAME")
then
	die "Failed to extract $DATA_SIMDOC_TARBALL backup in $SERVER_DATA_SIMDOC_ROOT_DIR"
else
  info "tarball $DATA_SIMDOC_TARBALL extracted to $SERVER_DATA_SIMDOC_ROOT_DIR/$SERVER_DATA_SIMDOC_ROOT_DIRNAME"
fi

if ! service clustering-ui restart
then
	die "Failed to restart service clustering-ui"
else
  info "clustering-ui restarted"
fi

if ! rm -f "$POSTGRES_ARCHIVE" "$RAW_IMAGE_CACHE_TARBALL" "$DATA_SIMDOC_TARBALL"
then
	die "Failed to delete all temporary backup files"
else
    info "temporary tarballs cleaned up successfully"
fi

echo
echo "--------------------------------"
echo
echo "🍻 We are all done, it worked 😘"
echo
