#!/usr/bin/env bash

DATE=$(date "+%Y-%m-%d")

SERVER_RAW_IMAGE_CACHE_DIR='/tmp'
SERVER_RAW_IMAGE_CACHE_DIRNAME='raw_image_cache'
SERVER_DATA_SIMDOC_ROOT_DIR='/'
SERVER_DATA_SIMDOC_ROOT_DIRNAME='data-simdoc'

BACKUP_ROOT_DIR="$HOME"
BACKUP_DATA_SIMDOC_DIRNAME='.data-simdoc'
BACKUP_RAW_IMAGE_CACHE_DIRNAME='.data-simdoc-raw-cache-dir'

MAXWELL_BACKUP_TARBALL=maxwell_backup_"$DATE".tar
POSTGRES_ARCHIVE=dump_"$DATE".dump
POSTGRES_DB_NAME="sandboxdb"
RAW_IMAGE_CACHE_TARBALL=raw_image_cache_"$DATE".tar
DATA_SIMDOC_TARBALL=data_simdoc_"$DATE".tar

die() {
  local msg="$1"
  echo
  echo "🔴 [ERROR] $msg"
  echo
  exit 1
}

info() {
  local msg="$1"
  echo
  echo "💚 $msg"
  echo
}

cleanup_backup_directories() {
  rm -rf "$BACKUP_ROOT_DIR/$BACKUP_DATA_SIMDOC_DIRNAME"
  rm -rf "$BACKUP_ROOT_DIR/$BACKUP_RAW_IMAGE_CACHE_DIRNAME"
}
init_backup_directories() {
  cleanup_backup_directories

  info "copy $SERVER_RAW_IMAGE_CACHE_DIR/$SERVER_RAW_IMAGE_CACHE_DIRNAME to $BACKUP_ROOT_DIR/$BACKUP_RAW_IMAGE_CACHE_DIRNAME"
  if ! cp -r "$SERVER_RAW_IMAGE_CACHE_DIR/$SERVER_RAW_IMAGE_CACHE_DIRNAME" "$BACKUP_ROOT_DIR/$BACKUP_RAW_IMAGE_CACHE_DIRNAME"; then
    die "copy failed"
  fi
  info "copy $SERVER_DATA_SIMDOC_ROOT_DIR/$SERVER_DATA_SIMDOC_ROOT_DIRNAME to $BACKUP_ROOT_DIR/$BACKUP_DATA_SIMDOC_DIRNAME"
  if ! cp -r "$SERVER_DATA_SIMDOC_ROOT_DIR/$SERVER_DATA_SIMDOC_ROOT_DIRNAME" "$BACKUP_ROOT_DIR/$BACKUP_DATA_SIMDOC_DIRNAME"; then
    die "copy failed"
  fi
}


init_backup_directories


if ! service clustering-ui stop
then
	die "Failed to stop service clustering-ui"
else
  info "clustering-ui stopped successfully"
fi

if ! sh -c "rm -f /tmp/$POSTGRES_ARCHIVE ; (cd /tmp && su postgres -c \"pg_dump --format=custom --file /tmp/$POSTGRES_ARCHIVE $POSTGRES_DB_NAME\") && mv /tmp/$POSTGRES_ARCHIVE ."
then
  die "Failed to dump postgres database"
else
  info "Postgres DB backup"
fi

if ! (cd "$BACKUP_ROOT_DIR" && tar cf "$RAW_IMAGE_CACHE_TARBALL" "$BACKUP_RAW_IMAGE_CACHE_DIRNAME")
then
	die "Failed to tar raw files"
else
  info "raw images cache backed up successfully"
fi

if ! cp "$BACKUP_ROOT_DIR/$RAW_IMAGE_CACHE_TARBALL" .
then
	die "Failed to copy $BACKUP_ROOT_DIR/$RAW_IMAGE_CACHE_TARBALL in current directory"
else
  info "raw files tarball copied to current directory successfully"
fi

if ! (cd "$BACKUP_ROOT_DIR" && tar cf "$DATA_SIMDOC_TARBALL" "$BACKUP_DATA_SIMDOC_DIRNAME")
then
	die "Failed to tar $BACKUP_ROOT_DIR/$BACKUP_DATA_SIMDOC_DIRNAME folder"
else
  info "$BACKUP_ROOT_DIR/$BACKUP_DATA_SIMDOC_DIRNAME folder backed up successfully"
fi

if ! cp "$BACKUP_ROOT_DIR/$DATA_SIMDOC_TARBALL" .
then
	die "Failed to copy $BACKUP_ROOT_DIR/$DATA_SIMDOC_TARBALL in current directory"
else
  info "data tarball copied to current directory successfully"
fi

if ! service clustering-ui restart
then
	die "Failed to start service clustering-ui"
else
    info "clustering-ui restarted successfully"
fi

if ! tar cf "$MAXWELL_BACKUP_TARBALL" "$POSTGRES_ARCHIVE" "$RAW_IMAGE_CACHE_TARBALL" "$DATA_SIMDOC_TARBALL"
then
	die "Failed to tar all backup files in backup file"
else
    info "final tarball created successfully"
fi

if ! rm -rf "$POSTGRES_ARCHIVE" "$RAW_IMAGE_CACHE_TARBALL" "$DATA_SIMDOC_TARBALL" "$BACKUP_ROOT_DIR/$RAW_IMAGE_CACHE_TARBALL" "$BACKUP_ROOT_DIR/$DATA_SIMDOC_TARBALL" "$BACKUP_ROOT_DIR/$BACKUP_DATA_SIMDOC_DIRNAME" "$BACKUP_ROOT_DIR/$BACKUP_RAW_IMAGE_CACHE_DIRNAME"
then
	die "Failed to delete all temporary backup files"
else
    info "temporary tarballs cleaned up successfully"
fi

echo
echo "--------------------------------"
echo
echo "🍻 We are all done, it worked 😘"
echo
