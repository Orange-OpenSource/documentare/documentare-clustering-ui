#!/bin/bash

INPUTFILE=$1
DIRECTORY_FILE=$2

rm -rf $DIRECTORY_FILE
mkdir $DIRECTORY_FILE

while IFS=";" read -r rec_column1 rec_column2 rec_column3 rec_column4
do
  echo "cluster id : $rec_column2"
  echo "distance: $rec_column4"
  echo ""
  file_name="$DIRECTORY_FILE"/"$rec_column2".txt
  echo "File_name : $file_name"
  if [ -f "$file_name" ]
  then
	echo "$rec_column4" >> "$file_name"
  else
	touch "$file_name"
	echo "$rec_column4" >> "$file_name"
  fi
done < "$INPUTFILE"
