#!/bin/bash

INPUTFILE=$1
DIRECTORY_FILE=$2

rm -rf $DIRECTORY_FILE
mkdir $DIRECTORY_FILE

echo "$INPUTFILE"
sed 's/^M/DenisBoisset/' "$INPUTFILE" > fileWithoutCRNL
sed ':a;N;$!ba;s/\n//g' fileWithoutCRNL > fileWithoutNL
sed 's/DenisBoisset/'\\\n'/g' fileWithoutNL > fileToAnalyse
sed 's//'\\\n'/g' fileToAnalyse > file

while IFS=";" read -r rec_column1 rec_column2
do
  file_name="$DIRECTORY_FILE"/"$rec_column1"
  echo "File_name : $file_name"
  echo "$rec_column1;$rec_column2" > $file_name
done < file


