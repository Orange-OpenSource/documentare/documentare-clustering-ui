#!/usr/bin/env bash

POSTGRES_SERVICE="postgresql@15"
PSQL="$(brew --prefix $POSTGRES_SERVICE)/bin/psql"
CMD_START_SERVICE="brew services run"
CMD_GREP_POSTGRES_PROCESS="pgrep -l postgres"
CMD_STOP_SERVICE="brew services stop"
DATABASE="sandboxdb"


CMD="$1"

usage() {
  echo
  echo usage: ./run-maxwell.sh COMMAND
  echo
  echo available commands:
  printf '   stop                stop maxwell/postgres\n'
  printf '   start               start maxwell/postgres\n'
  printf '   restart             stop then start maxwell/postgres\n'
  echo
  printf '   update     download last clustering-ui version (tagged)\n'
  printf '   reset      remove local clustering-ui version(s) and ⚠️  CLEAR POSTGRES DB ⚠️ . RAW caches are kept.\n'
  echo
}

error() {
  local msg="$1"
  echo
  echo "🔴 [ERROR] $msg"
  echo
  exit 1
}
warn() {
  local msg="$1"
  echo
  echo "⚠️ [WARN] $msg"
  echo
}

info() {
  local msg="$1"
  echo "✅ $msg"
}


check_internet_connexion() {
  local fail="$1"
  if ! curl -f -s https://www.google.com > /dev/null; then
    if [ "$fail" == "fail" ]
    then
      error "can not connect to internet (disable VPN? / remove proxy variables?)"
      exit 1
    else
      warn "can not connect to internet (disable VPN? / remove proxy variables?) => this can cause errors to update clustering-ui or pull docker images..."
    fi
  fi
}

############# BREW SERVICES #############
check_service() {
    local service=$1
    if ! brew services list | grep "$service" > /dev/null; then
      die "failed to find $service service; is it installed? You can install it with 'brew install $service'"
    fi
}
start_service() {
	if $CMD_START_SERVICE "$1" > /dev/null
	then
		info "[SERVICE] $1 started"
	else
	  check_service "$1"
		die "[SERVICE] failed to start $1"
	fi
}
stop_service() {
	if $CMD_STOP_SERVICE "$1" > /dev/null
	then
		info "[SERVICE] $1 stopped"
	else
		die "[SERVICE] failed to stop $1"
	fi
}
wait_for_postgres() {
  while :
  do
  	if $PSQL postgres -c 'SELECT * FROM pg_roles;' &> /dev/null
    then
  		info "[$POSTGRES_SERVICE] running"
  		break
  	fi
  	sleep 1
  done
}
create_postgres_user() {
  $PSQL postgres -c "CREATE USER postgres PASSWORD 'sandbox';" &> /dev/null
}
update_postgres_user_password() {
  if ! $PSQL postgres -c "ALTER USER postgres PASSWORD 'sandbox';" > /dev/null
  then
    die "[$POSTGRES_SERVICE] failed to update password"
  fi
}
update_postgres_createdb_permission() {
  if ! $PSQL postgres -c "ALTER USER postgres createdb;" > /dev/null
  then
    die "[$POSTGRES_SERVICE] failed to add 'createdb' permission"
  fi
}
############# /BREW SERVICES #############


check_raw_converter() {
  if ! which raw-converter >/dev/null; then
      error "failed to find raw-converter in your PATH; is it installed?"
      exit 1
  fi
}

check_jq() {
  if ! which jq >/dev/null; then
      error "failed to find jq in your PATH; is it installed?"
      exit 1
  fi
}

check_graphviz() {
  if ! which gvmap >/dev/null; then
      error "failed to find graphviz app (gvmap) in your PATH; is graphviz installed?"
      exit 1
  fi
}

run_postgres() {
  if $CMD_GREP_POSTGRES_PROCESS > /dev/null
  then
    info "[SERVICE] $POSTGRES_SERVICE already started"
  else
    start_service "$POSTGRES_SERVICE"
  fi
  wait_for_postgres
  create_postgres_user
  update_postgres_user_password
  update_postgres_createdb_permission
  info "[$POSTGRES_SERVICE] configured"
  return "$?"
}

show_databases() {
    echo
    echo "====================================================================="
    echo "🔵 Remaining databases in postgres:"
    $PSQL postgres -c '\list'
    echo "====================================================================="
    echo
}
drop_database() {
    if $PSQL postgres -c "DROP DATABASE IF EXISTS $DATABASE;" &> /dev/null
    then
      info "'$DATABASE' database dropped"
    else
      show_databases
      error "failed to drop '$DATABASE' database"
    fi
}

reset() {
  drop_database
  stop
  for jarfile in $(find . -name clustering-ui\*.jar)
  do
    info "remove $jarfile"
    rm -f "$jarfile"
  done

  warn "images raw cache are not cleared"
}

stop() {
  if $CMD_GREP_POSTGRES_PROCESS > /dev/null
  then
    stop_service "$POSTGRES_SERVICE"
  else
    info "[SERVICE] $POSTGRES_SERVICE already stopped"
  fi

  local clustering_ui_pid=$(ps aux | grep "java " | grep "clustering-ui" | awk '// { print $2 }')
  if [ -z "$clustering_ui_pid" ]
  then
    info "clustering-ui is already stopped"
  else
    kill "$clustering_ui_pid"
    info "clustering-ui stopped"
  fi
}

start() {
  run_postgres

  local clustering_ui_instances=$(ps aux | grep 'java -jar' | grep -c 'clustering-ui' | tr -d ' ')
  if [ "$clustering_ui_instances" != "0" ]
  then
    error "an instance of clustering-ui is already running, please use stop command and retry"
  fi

  local clustering_ui_jar=$(find . -name clustering-ui\*.jar)
  java -XX:+UseParallelGC -Dspring.aot.enabled=true -jar "$clustering_ui_jar" --spring.profiles.active=dev
}

restart() {
  stop
  start
}

is_not_empty() {
  local msg="$1"
  local string_to_check="$2"
  if [ -z "$string_to_check" ]
  then
    error "$msg"
  fi
}

retrieve_last_clustering_ui_version() {
  check_internet_connexion "fail"

  local last_tag_pipeline_id=$(curl -s "https://gitlab.com/api/v4/projects/4101624/pipelines?scope=tags&per_page=1&page=1" | jq 'first'  | jq '.id')
  is_not_empty "clustering-ui last version pipeline could not be retrieved" "$last_tag_pipeline_id"

  local last_tag_job=$(curl -s "https://gitlab.com/api/v4/projects/4101624/pipelines/$last_tag_pipeline_id/jobs"  | jq '( first(.[]  | select(.name == "release-job")))')
  local last_tag_job_id=$(echo "$last_tag_job"  | jq '.id')
  local last_tag_job_version=$(echo "$last_tag_job"  | jq -r '.ref')
  is_not_empty "clustering-ui last version build job could not be retrieved" "$last_tag_job_id"
  is_not_empty "clustering-ui last version could not be retrieved" "$last_tag_job_version"

  if curl -f -o clustering-ui-"$last_tag_job_version".jar "https://gitlab.com/api/v4/projects/4101624/jobs/$last_tag_job_id/artifacts/target/clustering-ui-0.0.1-SNAPSHOT.jar"
  then
    echo
    info "version $last_tag_job_version downloaded successfully and saved to file clustering-ui-$last_tag_job_version.jar"
    echo
  else
    error "failed to download last version ($last_tag_job_version) artifact"
  fi
}

check_if_clustering_ui_jar_is_available() {
  local nb_clustering_ui_jar=$(find . -name clustering-ui\*.jar | wc -l | tr -d ' ')
  if [ "$nb_clustering_ui_jar" == "0" ]
  then
    error "no clustering-ui jar found in current directory, please download it with according command"
  fi
  if [ "$nb_clustering_ui_jar" != "1" ]
  then
    error "more than one clustering-ui jar found in current directory, please keep only one"
  fi
}


check_internet_connexion "warn"
check_raw_converter
check_graphviz
check_jq


case "$CMD" in
'stop')
  stop
  ;;
'start')
  check_if_clustering_ui_jar_is_available
  start
  ;;
'restart')
  restart
  ;;
'update')
  retrieve_last_clustering_ui_version
  ;;
'reset')
  reset
  ;;
*)
  usage
  ;;
esac
