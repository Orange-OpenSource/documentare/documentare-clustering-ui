FROM debian:bullseye-slim
LABEL maintainer="denis.boisset@orange.com & christophe.maldivi@orange.com"

#    addgroup --system --gid 101 maxwell && \
#    adduser --system --disabled-login --ingroup maxwell --no-create-home --home /nonexistent --gecos "maxwell user" --shell /bin/false --uid 101 maxwell && \

# imagemagick: for thumbnail creation
# ghostscript: add pdf support to imagemagick
# Create "/usr/share/man/man1" manually due to debian-slim issue (https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=863199)
# hadolint ignore=DL3008
RUN mkdir -p /usr/share/man/man1 && \
    apt-get update && \
    apt-get install --no-install-recommends -y openjdk-17-jre-headless imagemagick ghostscript graphviz wget && \
    wget https://gitlab.com/Orange-OpenSource/documentare/image-converter/-/jobs/1146070497/artifacts/raw/raw-converter_8.0.0_amd64.deb && \
    dpkg -i raw-converter_8.0.0_amd64.deb ;  apt-get -f install --no-install-recommends -y && \
    apt-get purge -y wget && apt-get clean && \
    rm -rf /var/lib/apt/lists/*
# remove security policies which disable PDF thumbnail generation
COPY imagemagick-policy.xml /etc/ImageMagick-6/policy.xml

ENV LC_ALL C.UTF-8

WORKDIR application
COPY prep-docker-build/clustering-ui.jar .

EXPOSE 8888

CMD ["java", "-XX:+UseParallelGC", "-Dspring.aot.enabled=true", "-jar", "clustering-ui.jar", "--spring.profiles.active=docker"]
