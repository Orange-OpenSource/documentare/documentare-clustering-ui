package com.orange.documentare.clientui.biz.domain

import org.assertj.core.api.BDDAssertions.catchThrowable
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.io.TempDir
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider
import org.junit.jupiter.params.provider.ArgumentsSource
import java.io.File
import java.util.stream.Stream


class ClientRootDirTest {

  @TempDir
  private lateinit var tmpDir: File

  @Test
  fun `root dir is created even if many subdirectories have to be created`() {
    // given
    val clientRootDirPath = "${tmpDir.absolutePath}/subDir1/subDir2"

    // when
    ClientRootDir(clientRootDirPath).createDirectoriesIfNeeded()

    // then
    then(File(clientRootDirPath)).isDirectory()
  }

  @Test
  fun `do not fail if directory already exists`() {
    // given
    val clientRootDirPath = tmpDir.absolutePath
    val clientRootDir = ClientRootDir(clientRootDirPath)
    File(ClientRootDir.inputDirectoryPath(clientRootDirPath)).mkdir()

    // when / then do not raise exception
    clientRootDir.createDirectoriesIfNeeded()
  }

  @Test
  fun `raise exception if it is not possible to create directory`() {
    // when
    val throwable = catchThrowable { ClientRootDir("/xxx").createDirectoriesIfNeeded() }

    // then
    then(throwable).isNotNull()
    then(throwable.message).isEqualTo("❌ Failed to create directory /xxx/input")
  }


  internal class ImageOrNotProvider : ArgumentsProvider {
    override fun provideArguments(context: ExtensionContext): Stream<out Arguments> {
      return Stream.of(
        Arguments.of("png", "/input/titi.png"),  // null strings should be considered blank
        Arguments.of("jpg", "/input/titi.jpg"),  // null strings should be considered blank
        Arguments.of("JPG", "/input/titi.JPG"),  // null strings should be considered blank
        Arguments.of("PNG", "/input/titi.PNG"),  // null strings should be considered blank
        Arguments.of("jpeg", "/input/titi.jpeg"),  // null strings should be considered blank
        Arguments.of("JPEG", "/input/titi.JPEG"),  // null strings should be considered blank
        Arguments.of("bmp", "/input/titi.bmp"),  // null strings should be considered blank
        Arguments.of("BMP", "/input/titi.BMP"),

        Arguments.of("txt", "/img/no_image.png"),
      )
    }
  }

  @ArgumentsSource(ImageOrNotProvider::class)
  @ParameterizedTest
  fun `is file an image`(extension: String, expectedUri: String) {
    // given
    val txtFile = File("$tmpDir/titi.$extension")
    txtFile.createNewFile()

    // when
    val imageUri: String = ClientRootDir.imageUri(txtFile.name)

    // then
    then(imageUri).isEqualTo(expectedUri)
  }
}
