package com.orange.documentare.clientui.biz.domain.classifier

import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class ClassifierDistanceToReferenceFileTest {

  @Test
  fun `auto-include if cluster is a singleton`() {
    // given
    val clusterCenterSingleton = HierarchyExamples.clientExportRefFileLevel0F7Singleton()
    val clusterDistancesStats = clusterCenterSingleton.clusterDistancesStats!!
    val referenceFile = ClassifierDistanceToReferenceFile(clusterCenterSingleton, clusterDistancesStats, Int.MAX_VALUE, 100.0)

    // then
    then(referenceFile.singletonAutoInclusion()).isTrue
    then(referenceFile.distanceUnderOrEqualThreshold()).isFalse
  }

  @Test
  fun `if not a singleton, auto-include if distance is below threshold`() {
    // given
    val clusterCenter = HierarchyExamples.clientExportRefFileLevel0F0NotSingleton()
    val clusterDistancesStats = clusterCenter.clusterDistancesStats!!
    val threshold = clusterDistancesStats.threshold(1.0F)
    val distance = (threshold - 1).toInt()
    val referenceFile = ClassifierDistanceToReferenceFile(clusterCenter, clusterDistancesStats, distance, threshold)

    // then
    then(referenceFile.distanceUnderOrEqualThreshold()).isTrue
    then(referenceFile.singletonAutoInclusion()).isFalse
  }

  @Test
  fun `if not a singleton, auto-include if distance is equal to threshold`() {
    // given
    val clusterCenter = HierarchyExamples.clientExportRefFileLevel0F0NotSingleton()
    val clusterDistancesStats = clusterCenter.clusterDistancesStats!!
    val threshold = clusterDistancesStats.threshold(1.0F)
    val distance = threshold.toInt()
    val referenceFile = ClassifierDistanceToReferenceFile(clusterCenter, clusterDistancesStats, distance, threshold)

    // then
    then(referenceFile.distanceUnderOrEqualThreshold()).isTrue
    then(referenceFile.singletonAutoInclusion()).isFalse
  }

  @Test
  fun `if not a singleton, DO NOT auto-include if distance is above threshold`() {
    // given
    val clusterCenter = HierarchyExamples.clientExportRefFileLevel0F0NotSingleton()
    val clusterDistancesStats = clusterCenter.clusterDistancesStats!!
    val threshold = clusterDistancesStats.threshold(1.0F)
    val distance = (threshold + 1).toInt()

    // then
    val referenceFile = ClassifierDistanceToReferenceFile(clusterCenter, clusterDistancesStats, distance, threshold)
    then(referenceFile.distanceUnderOrEqualThreshold()).isFalse
    then(referenceFile.singletonAutoInclusion()).isFalse
  }
}
