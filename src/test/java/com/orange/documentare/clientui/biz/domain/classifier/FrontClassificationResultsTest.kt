package com.orange.documentare.clientui.biz.domain.classifier

import com.orange.documentare.clientui.biz.domain.ClientRootDir
import com.orange.documentare.clientui.biz.domain.ClientUIController
import com.orange.documentare.clientui.biz.domain.FrontClassificationResults
import com.orange.documentare.clusteringui.biz.file.query.ClusterDistancesStats
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelFile
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class FrontClassificationResultsTest {

  @Test
  fun `map to Front object AFTER`() {
    then(
      FrontClassificationResults.from(
        Classifier.Examples.classifierResults()
      ).results
    ).isEqualTo(ClientUIController.Examples.CONTROLLER_RESULTS)
  }

  @Test
  fun `class name over file named class`() {
    then(FrontClassificationResults.displayName("class name", listOf("titi"), null)).isEqualTo("class name")
  }

  @Test
  fun `file named class`() {
    then(FrontClassificationResults.displayName(null, listOf("titi"), null)).isEqualTo("titi")
  }

  @Test
  fun `file named classes`() {
    then(FrontClassificationResults.displayName(null, listOf("titi", "alf"), null)).isEqualTo("titi / alf")
  }

  @Test
  fun `cluster multi hypothesis class name`() {
    then(FrontClassificationResults.displayName(null, listOf("A"), "class C 50% / class A 50%")).isEqualTo("class C 50% / class A 50%")
  }

  @Test
  fun `reference file is NOT an image`() {
    // given
    val filename = "file-without-extension"
    val webPath = "/web/path"
    val modelFile = buildReferenceModelFile(filename, webPath)

    // when
    val refModelImageWebPath = FrontClassificationResults.refModelImageWebPath(modelFile)

    // then
    then(refModelImageWebPath).isEqualTo(ClientRootDir.NO_IMAGE)
  }

  @Test
  fun `reference file IS an image`() {
    // given
    val filename = "file.png"
    val webPath = "/web/path"
    val modelFile = buildReferenceModelFile(filename, webPath)

    // when
    val refModelImageWebPath = FrontClassificationResults.refModelImageWebPath(modelFile)

    // then
    then(refModelImageWebPath).isEqualTo(webPath)
  }

  private fun buildReferenceModelFile(
    filename: String,
    webPath: String
  ) = ClientHierarchyReferenceModelFile(
    "aggregateId",
    filename,
    true,
    false,
    ClusterDistancesStats.testExample(),
    null /* TODO CLIENT +++++ */,
    null,
    null,
    "/abs/path",
    webPath,
    "/web/thumbnail/path"
  )
}
