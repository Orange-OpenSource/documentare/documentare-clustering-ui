package com.orange.documentare.clientui.biz.domain.classifier

import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import java.io.File

class ClassifierQueueTest {

  private val classifierQueue = ClassifierQueue()

  private val file1 = File("file1")
  private val file2 = File("file2")

  @Test
  fun `check queue evolution`() {
    // WHEN at start
    then(classifierQueue.dequeue()).isNull()

    // WHEN adding two elements
    classifierQueue.add(listOf(file1, file2))
    then(classifierQueue.dequeue()).isEqualTo(file1)
    then(classifierQueue.dequeue()).isEqualTo(file2)
    then(classifierQueue.dequeue()).isNull()
  }
}
