package com.orange.documentare.clientui.biz.domain.classifier

import com.orange.documentare.clientui.biz.domain.classifier.events.ClassifierDistancesComputed
import com.orange.documentare.clientui.biz.domain.classifier.events.ClassifierEvent
import com.orange.documentare.clientui.biz.domain.classifier.events.ClassifierListener
import com.orange.documentare.clusteringui.biz.file.query.ClusterDistancesStats
import com.orange.documentare.clusteringui.biz.parameters.domain.DistanceParameters
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModel
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelFile
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelLevel
import com.orange.documentare.core.computationserver.biz.distances.DistancesRequestResultDTO
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import java.io.File

class ClassifierFindClosestClusterInWholeHierarchyTest {

  val events = mutableListOf<ClassifierEvent>()
  private val listener: ClassifierListener = object : ClassifierListener {
    override fun onEvent(event: ClassifierEvent) {
      events.add(event)
    }
  }

  @Test
  fun `Level 0 - closest cluster match`() {
    // given
    val refFileAndDistance1 = Pair(clientHierarchyReferenceModelFile(id = 1, mean = 10.0, std = 0.1), /* distance */ 10)
    val refFileAndDistance2 = Pair(clientHierarchyReferenceModelFile(id = 2, mean = 12.0, std = 0.1), /* distance */ 12)

    val hierarchyReferenceModel = ClientHierarchyReferenceModel(
      listOf(
        // Level 0
        ClientHierarchyReferenceModelLevel(
          listOf(
            // cluster 1
            listOf(refFileAndDistance1.first),
            // cluster 2
            listOf(refFileAndDistance2.first)
          )
        )
      ),
      DistanceParameters.Examples.defaults()
    )
    val classifier = buildClassifier(hierarchyReferenceModel, intArrayOf(refFileAndDistance1.second, refFileAndDistance2.second))

    // when
    val closestReferenceFiles = classifier.findClosestReferenceFiles(File("file_to_classify"), emptyList(), hierarchyReferenceModel, 0, ClassificationHierarchyPath())

    // then
    then(closestReferenceFiles.all().map { it.referenceFile.aggregateId }).containsExactly("aggregate-id-1", "aggregate-id-2")
    then(events).containsExactly(ClassifierDistancesComputed("file_to_classify", listOf("filename-1", "filename-2"), listOf(10, 12), 0))
  }

  @Test
  fun `Level 0 - closest singleton cluster match`() {
    // given
    val refFileAndDistance1 = Pair(clientHierarchyReferenceModelFileSingleton(id = 1), /* distance */ 10)
    val refFileAndDistance2 = Pair(clientHierarchyReferenceModelFile(id = 2, mean = 11.0, std = 0.1), /* distance */ 11)

    val hierarchyReferenceModel = ClientHierarchyReferenceModel(
      listOf(
        // Level 0
        ClientHierarchyReferenceModelLevel(
          listOf(
            // cluster 1
            listOf(refFileAndDistance1.first),
            // cluster 2
            listOf(refFileAndDistance2.first)
          )
        )
      ),
      DistanceParameters.Examples.defaults()
    )
    val classifier = buildClassifier(hierarchyReferenceModel, intArrayOf(refFileAndDistance1.second, refFileAndDistance2.second))

    // when
    val closestReferenceFiles = classifier.findClosestReferenceFiles(File("file_to_classify"), emptyList(), hierarchyReferenceModel, 0, ClassificationHierarchyPath())

    // then
    then(closestReferenceFiles.all().map { it.referenceFile.aggregateId }).containsExactly("aggregate-id-1", "aggregate-id-2")
    then(events).containsExactly(ClassifierDistancesComputed("file_to_classify", listOf("filename-1", "filename-2"), listOf(10, 11), 0))
  }

  @Test
  fun `Level 0 - closest cluster does NOT match, but second matches`() {
    // given
    val refFileAndDistance1 = Pair(clientHierarchyReferenceModelFile(id = 1, mean = 10.0, std = 0.1), /* distance */ 11)
    val refFileAndDistance2 = Pair(clientHierarchyReferenceModelFile(id = 2, mean = 12.0, std = 0.1), /* distance */ 12) /* MATCH */
    val refFileAndDistance3 = Pair(clientHierarchyReferenceModelFile(id = 3, mean = 10.0, std = 0.1), /* distance */ 13)

    val hierarchyReferenceModel = ClientHierarchyReferenceModel(
      listOf(
        // Level 0
        ClientHierarchyReferenceModelLevel(
          listOf(
            // cluster 1
            listOf(refFileAndDistance1.first),
            // cluster 2
            listOf(refFileAndDistance2.first),
            // cluster 3
            listOf(refFileAndDistance3.first)
          )
        )
      ),
      DistanceParameters.Examples.defaults()
    )
    val classifier = buildClassifier(hierarchyReferenceModel, intArrayOf(refFileAndDistance1.second, refFileAndDistance2.second, refFileAndDistance3.second))

    // when
    val closestReferenceFiles = classifier.findClosestReferenceFiles(File("file_to_classify"), emptyList(), hierarchyReferenceModel, 0, ClassificationHierarchyPath())

    // then
    then(closestReferenceFiles.all().map { it.referenceFile.aggregateId }).containsExactly("aggregate-id-2")
    then(events).containsExactly(ClassifierDistancesComputed("file_to_classify", listOf("filename-1", "filename-2", "filename-3"), listOf(11, 12, 13), 0))
  }

  @Test
  fun `Level 0 - closest cluster does NOT match, but second singleton cluster matches`() {
    // given
    val refFileAndDistance1 = Pair(clientHierarchyReferenceModelFile(id = 1, mean = 10.0, std = 0.1), /* distance */ 11)
    val refFileAndDistance2 = Pair(clientHierarchyReferenceModelFileSingleton(id = 2), /* distance */ 12)

    val hierarchyReferenceModel = ClientHierarchyReferenceModel(
      listOf(
        // Level 0
        ClientHierarchyReferenceModelLevel(
          listOf(
            // cluster 1
            listOf(refFileAndDistance1.first),
            // cluster 2
            listOf(refFileAndDistance2.first)
          )
        )
      ),
      DistanceParameters.Examples.defaults()
    )
    val classifier = buildClassifier(hierarchyReferenceModel, intArrayOf(refFileAndDistance1.second, refFileAndDistance2.second))

    // when
    val closestReferenceFiles = classifier.findClosestReferenceFiles(File("file_to_classify"), emptyList(), hierarchyReferenceModel, 0, ClassificationHierarchyPath())

    // then
    then(closestReferenceFiles.all().map { it.referenceFile.aggregateId }).containsExactly("aggregate-id-2")
    then(events).containsExactly(ClassifierDistancesComputed("file_to_classify", listOf("filename-1", "filename-2"), listOf(11, 12), 0))
  }

  @Test
  fun `Level 0 - no cluster match, take first`() {
    // given
    val refFileAndDistance1 = Pair(clientHierarchyReferenceModelFile(id = 1, mean = 10.0, std = 0.1), /* distance */ 15)
    val refFileAndDistance2 = Pair(clientHierarchyReferenceModelFile(id = 2, mean = 12.0, std = 0.1), /* distance */ 16)

    val hierarchyReferenceModel = ClientHierarchyReferenceModel(
      listOf(
        // Level 0
        ClientHierarchyReferenceModelLevel(
          listOf(
            // cluster 1
            listOf(refFileAndDistance1.first),
            // cluster 2
            listOf(refFileAndDistance2.first)
          )
        )
      ),
      DistanceParameters.Examples.defaults()
    )
    val classifier = buildClassifier(hierarchyReferenceModel, intArrayOf(refFileAndDistance1.second, refFileAndDistance2.second))

    // when
    val closestReferenceFiles = classifier.findClosestReferenceFiles(File("file_to_classify"), emptyList(), hierarchyReferenceModel, 0, ClassificationHierarchyPath())

    // then
    then(closestReferenceFiles.all().map { it.referenceFile.aggregateId }).containsExactly("aggregate-id-1")
    then(events).containsExactly(ClassifierDistancesComputed("file_to_classify", listOf("filename-1", "filename-2"), listOf(15, 16), 0))
  }

  @Test
  fun `Level 2-1-0 - second nearest neighbor for level 2, closest for level 1, then third nearest neighbor for level 0 `() {
    // given
    val level2RefFileAndDistance1 = Pair(clientHierarchyReferenceModelFile(id = 21, mean = 10.0, std = 0.1), /* distance */ 20)
    val level2RefFileAndDistance2 = Pair(clientHierarchyReferenceModelFile(id = 22, mean = 21.0, std = 0.1), /* distance */ 21) /* match */
    val level2RefFileAndDistance3 = Pair(clientHierarchyReferenceModelFile(id = 23, mean = 22.0, std = 0.1), /* distance */ 22)
    val level2RefFileAndDistance4 = Pair(clientHierarchyReferenceModelFile(id = 24, mean = 23.0, std = 0.1), /* distance */ 23)

    val level1RefFileAndDistance1 = Pair(clientHierarchyReferenceModelFile(id = 22, mean = 20.0, std = 0.1), /* distance */ 20) /* match */
    val level1RefFileAndDistance2 = Pair(clientHierarchyReferenceModelFile(id = 12, mean = 21.0, std = 0.1), /* distance */ 21)
    val level1RefFileAndDistance3 = Pair(clientHierarchyReferenceModelFile(id = 13, mean = 22.0, std = 0.1), /* distance */ 22)
    val level1RefFileAndDistance4 = Pair(clientHierarchyReferenceModelFile(id = 14, mean = 23.0, std = 0.1), /* distance */ 23)

    val level0RefFileAndDistance1 = Pair(clientHierarchyReferenceModelFile(id = 22, mean = 10.0, std = 0.1), /* distance */ 20)
    val level0RefFileAndDistance2 = Pair(clientHierarchyReferenceModelFile(id = 2, mean = 10.0, std = 0.1), /* distance */ 21)
    val level0RefFileAndDistance3 = Pair(clientHierarchyReferenceModelFile(id = 3, mean = 22.0, std = 0.1), /* distance */ 22) /* match */
    val level0RefFileAndDistance4 = Pair(clientHierarchyReferenceModelFile(id = 4, mean = 23.0, std = 0.1), /* distance */ 23) /* match 2d */


    val hierarchyReferenceModel = ClientHierarchyReferenceModel(
      listOf(
        // Level 0
        ClientHierarchyReferenceModelLevel(
          listOf(
            // cluster 1
            listOf(level0RefFileAndDistance1.first),
            // cluster 2 - MATCH
            listOf(level0RefFileAndDistance2.first),
            // cluster 3
            listOf(level0RefFileAndDistance3.first),
            // cluster 4
            listOf(level0RefFileAndDistance4.first),
          )
        ),
        // Level 1
        ClientHierarchyReferenceModelLevel(
          listOf(
            // cluster 1 - MATCH - add level 0 ref files to force looking at it at lower level
            listOf(level1RefFileAndDistance1.first, level0RefFileAndDistance2.first, level0RefFileAndDistance3.first, level0RefFileAndDistance4.first),
            // cluster 2
            listOf(level1RefFileAndDistance2.first),
            // cluster 3
            listOf(level1RefFileAndDistance3.first),
            // cluster 4
            listOf(level1RefFileAndDistance4.first)
          )
        ),
        // Level 2
        ClientHierarchyReferenceModelLevel(
          listOf(
            // cluster 1
            listOf(level2RefFileAndDistance1.first),
            // cluster 2
            listOf(level2RefFileAndDistance2.first),
            // cluster 3 - MATCH
            listOf(level2RefFileAndDistance3.first),
            // cluster 4 - MATCH 2d
            listOf(level2RefFileAndDistance4.first)
          )
        )
      ),
      DistanceParameters.Examples.defaults()
    )
    val classifier = buildClassifier(hierarchyReferenceModel, intArrayOf(20, 21, 22, 23))

    // when
    val level2ClosestReferenceFiles = classifier.findClosestReferenceFiles(File("file_to_classify"), emptyList(), hierarchyReferenceModel, 2, ClassificationHierarchyPath())

    // then
    then(level2ClosestReferenceFiles.all().map { it.referenceFile.aggregateId }).containsExactly("aggregate-id-3", "aggregate-id-4")
    then(events).containsExactly(
      ClassifierDistancesComputed("file_to_classify", listOf("filename-21", "filename-22", "filename-23", "filename-24"), listOf(20, 21, 22, 23), 2),
      ClassifierDistancesComputed("file_to_classify", listOf("filename-22"), listOf(20, 21, 22, 23), 1),
      ClassifierDistancesComputed("file_to_classify", listOf("filename-22", "filename-2", "filename-3", "filename-4"), listOf(20, 21, 22, 23), 0)
    )
  }

  private fun buildClassifier(
    hierarchyReferenceModel: ClientHierarchyReferenceModel,
    distances: IntArray
  ): Classifier {
    val clientHierarchyExport = object : ClientHierarchyExport {
      override fun exportClientHierarchy(): ClientHierarchyReferenceModel {
        return hierarchyReferenceModel
      }

      override fun exportClientHierarchyForStatisticsConsolidation(): ClientHierarchyReferenceModel {
        TODO("Not yet implemented")
      }
    }
    val classifier = Classifier("", "", clientHierarchyExport, listener)
    classifier.testPurposeSetComputeDistance { _, _ -> DistancesRequestResultDTO.with(distances) }
    return classifier
  }


  private fun clientHierarchyReferenceModelFileSingleton(id: Int): ClientHierarchyReferenceModelFile =
    clientHierarchyReferenceModelFile(id, Double.NaN, Double.NaN, singleton = true)

  private fun clientHierarchyReferenceModelFile(id: Int, mean: Double, std: Double, singleton: Boolean = false): ClientHierarchyReferenceModelFile {
    val clusterDistancesStats1 = if (singleton) ClusterDistancesStats(1, Double.NaN, Double.NaN, true, "") else clStats(mean, std)
    return refFileWith(id, clusterDistancesStats1)
  }

  private fun clStats(mean: Double, std: Double): ClusterDistancesStats {
    return ClusterDistancesStats(3, mean, std, false, "")
  }

  private fun refFileWith(id: Int, clusterDistancesStats: ClusterDistancesStats): ClientHierarchyReferenceModelFile {
    return ClientHierarchyReferenceModelFile(
      aggregateId = "aggregate-id-$id",
      filename = "filename-$id",
      clusterCenter = true,
      multiset = true,
      clusterDistancesStats,
      clusterMultiHypothesisName = null /* TODO CLIENT +++++ */,
      className = "",
      fileNamedClass = null,
      absolutePath = "",
      webPath = "",
      webThumbnailPath = ""
    )
  }
}
