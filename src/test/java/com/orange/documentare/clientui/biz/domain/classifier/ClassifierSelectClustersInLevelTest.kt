package com.orange.documentare.clientui.biz.domain.classifier

import com.orange.documentare.clientui.biz.domain.classifier.events.ClassifierDistancesComputed
import com.orange.documentare.clientui.biz.domain.classifier.events.ClassifierEvent
import com.orange.documentare.clientui.biz.domain.classifier.events.ClassifierListener
import com.orange.documentare.clusteringui.biz.file.query.ClusterDistancesStats
import com.orange.documentare.clusteringui.biz.parameters.domain.DistanceParameters
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.AggregateId
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModel
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelFile
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelLevel
import com.orange.documentare.core.computationserver.biz.distances.DistancesRequestResultDTO
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import java.io.File

class ClassifierSelectClustersInLevelTest {

  val events = mutableListOf<ClassifierEvent>()
  private val listener: ClassifierListener = object : ClassifierListener {
    override fun onEvent(event: ClassifierEvent) {
      events.add(event)
    }
  }

  @Test
  fun `Level 1 - we do not compute distances to singletons and we keep all singletons for lower level iteration`() {
    // given
    val level1RefFileSingleton1 = clientHierarchyReferenceModelFileSingleton(id = 11)
    val level1RefFileSingleton2 = clientHierarchyReferenceModelFileSingleton(id = 12)
    val level1RefFileAndDistance3 = Pair(clientHierarchyReferenceModelFile(id = 18, mean = 23.0, std = 0.1), /* distance */ 21)
    val level1RefFile4 = clientHierarchyReferenceModelFile(id = 19, mean = 23.0, std = 0.1)
    val level1RefFileSingleton5 = clientHierarchyReferenceModelFileSingleton(id = 13)

    val hierarchyReferenceModel = ClientHierarchyReferenceModel(
      listOf(
        // Level 0
        // we stop at level 1 with test instrumentation, so we do not need to provide data for level 0
        ClientHierarchyReferenceModelLevel(emptyList()),
        // Level 1
        ClientHierarchyReferenceModelLevel(
          listOf(
            listOf(level1RefFileSingleton1),
            listOf(level1RefFileSingleton2),
            listOf(level1RefFileAndDistance3.first, level1RefFile4),
            listOf(level1RefFileSingleton5)
          )
        )
      ),
      DistanceParameters.Examples.defaults()
    )
    val classifier = buildClassifier(hierarchyReferenceModel, intArrayOf(20 /* sole distance to compute to level1RefFile4 */))

    // when
    var actualInReferenceFilesAggregateId: List<AggregateId> = emptyList()
    val testCaptureNextCall: FindClosestReferenceFilesSignature = { _: File,
                                                                    inReferenceFilesAggregateId: List<AggregateId>,
                                                                    _: ClientHierarchyReferenceModel,
                                                                    _: Int,
                                                                    _: ClassificationHierarchyPath
      ->
      actualInReferenceFilesAggregateId = inReferenceFilesAggregateId
      // no used
      ClassifierClosestSortedReferenceFiles(emptyList())
    }
    classifier.findClosestReferenceFiles(File("file_to_classify"), emptyList(), hierarchyReferenceModel, 1, ClassificationHierarchyPath(), testCaptureNextCall)

    // then
    then(actualInReferenceFilesAggregateId).containsExactly("aggregate-id-11", "aggregate-id-12", "aggregate-id-18", "aggregate-id-19", "aggregate-id-13")
    then(events).containsExactly(
      ClassifierDistancesComputed("file_to_classify", listOf("filename-18"), listOf(20), 1)
    )
  }

  @Test
  fun `Level 1 - we stop at once if we found no matching cluster nor singletons, we return first cluster only for display purpose, UNRELIABLE`() {
    // given
    val level1RefFileAndDistance1 = Pair(clientHierarchyReferenceModelFile(id = 18, mean = 23.0, std = 0.1), /* distance */ 50)

    val hierarchyReferenceModel = ClientHierarchyReferenceModel(
      listOf(
        // Level 0
        // we stop at level 1 with test instrumentation, so we do not need to provide data for level 0
        ClientHierarchyReferenceModelLevel(emptyList()),
        // Level 1
        ClientHierarchyReferenceModelLevel(
          listOf(
            listOf(level1RefFileAndDistance1.first)
          )
        )
      ),
      DistanceParameters.Examples.defaults()
    )
    val classifier = buildClassifier(hierarchyReferenceModel, intArrayOf(level1RefFileAndDistance1.second /* sole distance to compute to level1RefFile4 */))
    var actualNextIterationCalled: Boolean? = null
    val testCaptureNextCall: FindClosestReferenceFilesSignature = { _: File,
                                                                    _: List<AggregateId>,
                                                                    _: ClientHierarchyReferenceModel,
                                                                    _: Int,
                                                                    _: ClassificationHierarchyPath
      ->
      actualNextIterationCalled = true
      // no used
      ClassifierClosestSortedReferenceFiles(emptyList())
    }
    val classificationHierarchyPath = ClassificationHierarchyPath()

    // when
    val closestReferenceFiles =
      classifier.findClosestReferenceFiles(File("file_to_classify"), emptyList(), hierarchyReferenceModel, 1, classificationHierarchyPath, testCaptureNextCall)

    // then
    then(actualNextIterationCalled).isNull()
    then(closestReferenceFiles.all().map { it.referenceFile }).containsExactly(level1RefFileAndDistance1.first)
    then(classificationHierarchyPath.trustWorthiness()).isEqualTo(ClassificationTrustWorthiness(ClassificationTrustWorthinessType.UNRELIABLE, bonusMalusScore=-1))
  }

  private fun buildClassifier(
    hierarchyReferenceModel: ClientHierarchyReferenceModel,
    distances: IntArray
  ): Classifier {
    val clientHierarchyExport = object : ClientHierarchyExport {
      override fun exportClientHierarchy(): ClientHierarchyReferenceModel {
        return hierarchyReferenceModel
      }

      override fun exportClientHierarchyForStatisticsConsolidation(): ClientHierarchyReferenceModel {
        TODO("Not yet implemented")
      }
    }
    val classifier = Classifier("", "", clientHierarchyExport, listener)
    classifier.testPurposeSetComputeDistance { _, _ -> DistancesRequestResultDTO.with(distances) }
    return classifier
  }


  private fun clientHierarchyReferenceModelFileSingleton(id: Int): ClientHierarchyReferenceModelFile =
    clientHierarchyReferenceModelFile(id, Double.NaN, Double.NaN, singleton = true)

  private fun clientHierarchyReferenceModelFile(id: Int, mean: Double, std: Double, singleton: Boolean = false): ClientHierarchyReferenceModelFile {
    val clusterDistancesStats1 = if (singleton) ClusterDistancesStats(1, Double.NaN, Double.NaN, true, "") else clStats(mean, std)
    return refFileWith(id, clusterDistancesStats1)
  }

  private fun clStats(mean: Double, std: Double): ClusterDistancesStats {
    return ClusterDistancesStats(3, mean, std, false, "")
  }

  private fun refFileWith(id: Int, clusterDistancesStats: ClusterDistancesStats): ClientHierarchyReferenceModelFile {
    return ClientHierarchyReferenceModelFile(
      aggregateId = "aggregate-id-$id",
      filename = "filename-$id",
      clusterCenter = true,
      multiset = true,
      clusterDistancesStats,
      clusterMultiHypothesisName = null /* TODO CLIENT +++++ */,
      className = "",
      fileNamedClass = null,
      absolutePath = "",
      webPath = "",
      webThumbnailPath = ""
    )
  }
}
