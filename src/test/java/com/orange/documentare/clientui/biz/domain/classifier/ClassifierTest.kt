package com.orange.documentare.clientui.biz.domain.classifier

import com.orange.documentare.clientui.biz.domain.ClientRootDir
import com.orange.documentare.clientui.biz.domain.classifier.Classifier.Examples
import com.orange.documentare.clientui.biz.domain.classifier.events.*
import com.orange.documentare.clusteringui.biz.file.query.ClusterDistancesStats
import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples
import com.orange.documentare.clusteringui.biz.parameters.domain.DistanceParameters
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModel
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelFile
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelLevel
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.orange.documentare.core.computationserver.biz.distances.DistancesRequestResultDTO
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

class ClassifierTest {

  @TempDir
  lateinit var distancePrepDir: File

  @TempDir
  lateinit var rawImageCacheDir: File

  @TempDir
  lateinit var referenceFilesDir: File

  private lateinit var classifier: Classifier

  private val events = mutableListOf<ClassifierEvent>()

  @BeforeEach
  fun setup() {
    FileIO.initFilesDirectory(referenceFilesDir.absolutePath)
    classifier = Classifier(distancePrepDir.absolutePath, rawImageCacheDir.absolutePath, HierarchyExamples.clientHierarchyExport(), object : ClassifierListener {
      override fun onEvent(event: ClassifierEvent) {
        events.add(event)
      }
    })
  }


  @Test
  fun `running and results available state evolve`() {
    // given
    classifier.testPurposeSetComputeDistance { _, referenceFiles ->
      // computation is running
      then(classifier.running()).isTrue
      then(classifier.resultsAvailable()).isFalse
      DistancesRequestResultDTO.with(List(referenceFiles.size) { index -> index + 1 }.toIntArray())
    }

    // initial values
    then(classifier.running()).isFalse
    then(classifier.resultsAvailable()).isFalse
    then(classifier.results()).isEmpty()

    // when
    classifier.runAsync(Examples.CLASSIFIER_FILES_TO_CLASSIFY)

    // then
    classifier.testPurposeWaitComputationFinished()
    then(classifier.resultsAvailable()).isTrue
    then(classifier.results()).isNotEmpty

    // when
    // restart to check resultsAvailable is reset (checked in fake testPurposeSetComputeDistance)
    classifier.runAsync(Examples.CLASSIFIER_FILES_TO_CLASSIFY)
    classifier.testPurposeWaitComputationFinished()
  }

  @Test
  fun `clear kills running thread and reset queue`() {
    // given
    var classifierThreadWasInterrupted = false
    var classifierThreadIsSleeping = false
    classifier.testPurposeSetComputeDistance { _, _ ->
      try {
        classifierThreadIsSleeping = true
        // block classifier thread
        Thread.sleep(Int.MAX_VALUE.toLong())
      } catch (e: InterruptedException) {
        classifierThreadWasInterrupted = true
      }
      DistancesRequestResultDTO.with(intArrayOf(1))
    }
    classifier.runAsync(listOf(File("file1"), File("file2"), File("file3"), File("file4")))

    while (!classifierThreadIsSleeping) {
      Thread.sleep(10)
    }

    // when
    classifier.clear()

    // then
    while (!classifierThreadWasInterrupted) {
      Thread.sleep(10)
    }

    then(classifier.running()).isFalse
    then(classifier.resultsAvailable()).isFalse
    then(classifier.results()).isEmpty()
    then(classifier.queueIsEmpty()).isTrue
  }

  @Test
  fun `if queue is not empty yet, we can enqueue new files`() {
    // given
    var classifierThreadIsSleeping = false
    classifier.testPurposeSetComputeDistance { _, _ ->
      try {
        classifierThreadIsSleeping = true
        // block classifier thread
        Thread.sleep(Int.MAX_VALUE.toLong())
      } catch (_: InterruptedException) {
      }
      DistancesRequestResultDTO.with(intArrayOf(1))
    }
    classifier.runAsync(listOf(File("file1"), File("file2")))

    while (!classifierThreadIsSleeping) {
      Thread.sleep(10)
    }

    // then
    then(classifier.filesWaitingInQueue()).isEqualTo(listOf(File("file2")))

    // when
    classifier.runAsync(listOf(File("file3"), File("file4")))

    // then
    then(classifier.filesWaitingInQueue()).isEqualTo(listOf(File("file2"), File("file3"), File("file4")))
  }

  // TODO CLIENT RESTART HERE
  @Disabled
  @Test
  fun `find closest references files digging in hierarchy`() {
    // given
    classifier.testPurposeSetComputeDistance { _, referenceFiles ->
      DistancesRequestResultDTO.with(List(referenceFiles.size) { index -> 100 + index }.toIntArray())
    }

    // when
    classifier.runAsync(Examples.CLASSIFIER_FILES_TO_CLASSIFY)

    // then
    classifier.testPurposeWaitComputationFinished()
    then(events).isEqualTo(Examples.CLASSIFIER_EVENTS)
    then(classifier.results()).isEqualTo(Examples.classifierResults())

    events.forEach { println(it) }
  }

  // TODO CLIENT RESTART HERE
  @Disabled
  @Test
  // ⚠️  test with real files! no mock!
  fun `find closest references files with multiset as sole reference file coming from previous level`() {
    // given
    val clientHierarchyExport = clientHierarchyExportToTestMultisetElementAsSoleReferenceFileFromPreviousLevel()
    classifier = Classifier(distancePrepDir.absolutePath, rawImageCacheDir.absolutePath, clientHierarchyExport, object : ClassifierListener {
      override fun onEvent(event: ClassifierEvent) {
        events.add(event)
      }
    })

    // when
    classifier.runAsync(listOf(fileFor("F0")))

    // then
    classifier.testPurposeWaitComputationFinished()
    val closestRefFile = classifier.results().first().closestSortedReferenceFiles.first()
    then(closestRefFile.distance).isEqualTo(0)
    then(classifier.results()).isEqualTo(multisetElementAsSoleReferenceFileFromPreviousLevelClassifierResults())
    then(events).isEqualTo(multisetElementAsSoleReferenceFileFromPreviousLevelExpectedEvents())

    events.forEach { println(it) }
  }

  private fun clientHierarchyExportToTestMultisetElementAsSoleReferenceFileFromPreviousLevel() = object : ClientHierarchyExport {
    override fun exportClientHierarchy(): ClientHierarchyReferenceModel {
      return ClientHierarchyExport.cleanLevel0(fullExportedClientHierarchiesToTestMultisetElementAsSoleReferenceFileFromPreviousLevel())
    }

    override fun exportClientHierarchyForStatisticsConsolidation(): ClientHierarchyReferenceModel {
      return fullExportedClientHierarchiesToTestMultisetElementAsSoleReferenceFileFromPreviousLevel()
    }
  }

  private val distanceThreshold = 900000.0
  private val clusterDistancesStats = ClusterDistancesStats.testExampleWithThreshold(distanceThreshold)
  private fun fullExportedClientHierarchiesToTestMultisetElementAsSoleReferenceFileFromPreviousLevel(): ClientHierarchyReferenceModel {
    return ClientHierarchyReferenceModel(
      listOf(
        ClientHierarchyReferenceModelLevel(
          // level 0
          listOf(
            listOf(
              refFile("F0", clusterCenter = true, clusterDistancesStats = clusterDistancesStats),
              refFile("F1")
            ),
            listOf(
              refFile("F2", clusterCenter = true, clusterDistancesStats = clusterDistancesStats),
              refFile("F3")
            ),
            listOf(refFile("F4ab", clusterCenter = true, clusterDistancesStats = ClusterDistancesStats.testExampleSingleton("F4ab"))),
            listOf(refFile("F5cde", clusterCenter = true, clusterDistancesStats = ClusterDistancesStats.testExampleSingleton("F5cde"))),
            listOf(refFile("F6fghi", clusterCenter = true, clusterDistancesStats = ClusterDistancesStats.testExampleSingleton("F6fghi")))
          )
        ),
        ClientHierarchyReferenceModelLevel(
          // level 1
          listOf(
            // 'F0' is a multiset which could be the only reference file coming from previous level in test
            // => useful to test the case where it is not the cluster center which is the reference file
            listOf(
              refFile("F2", clusterCenter = true, clusterDistancesStats = clusterDistancesStats),
              refFile("F0", multiset = true),
              refFile("F4ab"),
              refFile("F5cde")
            ),
            listOf(refFile("F6fghi", clusterCenter = true, clusterDistancesStats = ClusterDistancesStats.testExampleSingleton("F6fghi")))
          )
        ),
        ClientHierarchyReferenceModelLevel(
          // level 2
          listOf(
            listOf(refFile("F0", clusterCenter = true, clusterDistancesStats = ClusterDistancesStats.testExampleSingleton("F0"))),
            listOf(refFile("F2", clusterCenter = true, clusterDistancesStats = ClusterDistancesStats.testExampleSingleton("F2"))),
            listOf(refFile("F6fghi", clusterCenter = true, clusterDistancesStats = ClusterDistancesStats.testExampleSingleton("F6fghi")))
          )
        )
      ), DistanceParameters.Examples.defaults()
    )
  }

  private fun multisetElementAsSoleReferenceFileFromPreviousLevelClassifierResults(): List<ClassifierResult> {
    return listOf(
      ClassifierResult(
        ClassifiedFile("F0", ClientRootDir.NO_IMAGE), ClassifierClosestSortedReferenceFiles(
          listOf(
            ClassifierDistanceToReferenceFile(refFile("F0", clusterCenter = true, clusterDistancesStats = clusterDistancesStats), clusterDistancesStats, 0, distanceThreshold),
            ClassifierDistanceToReferenceFile(refFile("F2", clusterCenter = true, clusterDistancesStats = clusterDistancesStats), clusterDistancesStats, 111111, distanceThreshold),
            ClassifierDistanceToReferenceFile(
              refFile("F4ab", clusterCenter = true, clusterDistancesStats = ClusterDistancesStats.testExampleSingleton("F4ab")),
              ClusterDistancesStats.testExampleSingleton("F4ab"),
              272727,
              0.0
            )
          )
        ), ClassificationHierarchyPath(
          mutableListOf(
            listOf(
              HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
                "F0",
                singletonAutoInclusion = false,
                distanceUnderOrEqualThreshold = true,
                null,
                level = 2,
                0,
                ClusterDistancesStats.testExampleSingleton("F0").threshold(0.0f)
              ), HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
                "F2",
                singletonAutoInclusion = true,
                distanceUnderOrEqualThreshold = false,
                null,
                level = 2,
                111111,
                0.0
              ), HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
                "F6fghi",
                singletonAutoInclusion = true,
                distanceUnderOrEqualThreshold = false,
                null,
                level = 2,
                384615,
                0.0
              )
            ),
            listOf(
              HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
                "F2",
                singletonAutoInclusion = false,
                distanceUnderOrEqualThreshold = true,
                null,
                level = 1,
                100000,
                distanceThreshold
              ), HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
                "F6fghi",
                singletonAutoInclusion = true,
                distanceUnderOrEqualThreshold = false,
                null,
                level = 1,
                384615,
                0.0
              )
            ),
            listOf(
              HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
                "F0",
                singletonAutoInclusion = false,
                distanceUnderOrEqualThreshold = true,
                null,
                level = 0,
                0,
                distanceThreshold
              ),
              HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
                "F2",
                singletonAutoInclusion = false,
                distanceUnderOrEqualThreshold = true,
                null,
                level = 0,
                111111,
                distanceThreshold
              ), HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
                "F4ab",
                singletonAutoInclusion = true,
                distanceUnderOrEqualThreshold = false,
                null,
                level = 0,
                272727,
                0.0
              ), HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
                "F5cde",
                singletonAutoInclusion = true,
                distanceUnderOrEqualThreshold = false,
                null,
                level = 0,
                272727,
                0.0
              ), HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
                "F6fghi",
                singletonAutoInclusion = true,
                distanceUnderOrEqualThreshold = false,
                null,
                level = 0,
                384615,
                0.0
              )
            )
          )
        )
      )
    )
  }

  private fun multisetElementAsSoleReferenceFileFromPreviousLevelExpectedEvents() = listOf(
    ClassifierStarted("F0"),
    ClassifierDistancesComputed("F0", listOf("F0", "F2", "F6fghi"), listOf(0, 111111, 384615), 2),
    ClassifierDistancesComputed("F0", listOf("F2", "F6fghi"), listOf(100000, 384615), 1),
    ClassifierDistancesComputed("F0", listOf("F0", "F2", "F4ab", "F5cde", "F6fghi"), listOf(0, 111111, 272727, 272727, 384615), 0),
    ClassifierClosestReferenceFileFound("F0", "F0", clusterDistancesStats, distanceThreshold),
    ClassifierHierarchyPathRecorded(
      "F0",
      ClassificationHierarchyPath(
        mutableListOf(
          listOf(
            HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
              "F0",
              singletonAutoInclusion = false,
              distanceUnderOrEqualThreshold = true,
              null,
              level = 2,
              0,
              0.0
            ),
            HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
              "F2",
              singletonAutoInclusion = true,
              distanceUnderOrEqualThreshold = false,
              null,
              level = 2,
              111111,
              0.0
            ),
            HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
              "F6fghi",
              singletonAutoInclusion = true,
              distanceUnderOrEqualThreshold = false,
              null,
              level = 2,
              384615,
              0.0
            )
          ),
          listOf(
            HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
              "F2",
              singletonAutoInclusion = false,
              distanceUnderOrEqualThreshold = true,
              null,
              level = 1,
              100000,
              distanceThreshold
            ),
            HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
              "F6fghi",
              singletonAutoInclusion = true,
              distanceUnderOrEqualThreshold = false,
              null,
              level = 1,
              384615,
              0.0
            )
          ),
          listOf(
            HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
              "F0",
              singletonAutoInclusion = false,
              distanceUnderOrEqualThreshold = true,
              null,
              level = 0,
              0,
              distanceThreshold
            ),
            HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
              "F2",
              singletonAutoInclusion = false,
              distanceUnderOrEqualThreshold = true,
              null,
              level = 0,
              111111,
              distanceThreshold
            ),
            HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
              "F4ab",
              singletonAutoInclusion = true,
              distanceUnderOrEqualThreshold = false,
              null,
              level = 0,
              272727,
              0.0
            ),
            HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
              "F5cde",
              singletonAutoInclusion = true,
              distanceUnderOrEqualThreshold = false,
              null,
              level = 0,
              272727,
              0.0
            ),
            HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
              "F6fghi",
              singletonAutoInclusion = true,
              distanceUnderOrEqualThreshold = false,
              null,
              level = 0,
              384615,
              0.0
            )
          )
        )
      )
    )
  )


  private fun refFile(
    filename: String,
    clusterCenter: Boolean = false,
    multiset: Boolean = false,
    clusterDistancesStats: ClusterDistancesStats? = null
  ): ClientHierarchyReferenceModelFile {
    val file = fileFor(filename)
    file.writeBytes(filename.toByteArray())
    return ClientHierarchyReferenceModelFile(
      "aggregateId-${filename}",
      filename,
      clusterCenter,
      multiset,
      clusterDistancesStats,
      clusterMultiHypothesisName = null /* TODO CLIENT +++++ */,
      null,
      null,
      file.absolutePath,
      "notUsed",
      "notUsed"
    )
  }

  private fun fileFor(filename: String) = File(referenceFilesDir.absolutePath + File.separator + filename)

}
