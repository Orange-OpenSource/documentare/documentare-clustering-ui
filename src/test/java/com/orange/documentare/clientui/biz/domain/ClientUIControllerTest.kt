package com.orange.documentare.clientui.biz.domain

import com.orange.documentare.clientui.biz.domain.classifier.Classifier
import com.orange.documentare.clientui.biz.domain.classifier.events.ClassifierCleared
import com.orange.documentare.clientui.biz.domain.classifier.events.ClassifierEvent
import com.orange.documentare.clientui.biz.domain.classifier.events.ClassifierListener
import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples.clientHierarchyExport
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.orange.documentare.core.computationserver.biz.distances.DistancesRequestResultDTO
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

class ClientUIControllerTest {
  private lateinit var controller: ClientUIController
  private lateinit var clientRootDir: ClientRootDir

  @TempDir
  lateinit var clientStorageDirectory: File

  @TempDir
  lateinit var distancePrepDir: File

  @TempDir
  lateinit var rawImageCacheDir: File

  @TempDir
  lateinit var referenceFilesDir: File

  private lateinit var classifier: Classifier
  private var classifierEvents = mutableListOf<ClassifierEvent>()
  private val classifierListener = object : ClassifierListener {
    override fun onEvent(event: ClassifierEvent) {
      classifierEvents.add(event)
    }
  }

  @BeforeEach
  fun setup() {
    clientRootDir = ClientRootDir(clientStorageDirectory.absolutePath)
    classifier = Classifier(distancePrepDir.absolutePath, rawImageCacheDir.absolutePath, clientHierarchyExport(), classifierListener)
    classifier.testPurposeSetComputeDistance { _, referenceFiles ->
      DistancesRequestResultDTO.with(List(referenceFiles.size) { index -> index + 100 }.toIntArray())
    }
    controller = ClientUIController(
      clientHierarchyExport(),
      clientRootDir,
      distancePrepDir.absolutePath,
      rawImageCacheDir.absolutePath,
      classifierListener,
      classifier
    )
  }

  @Test
  fun `files are stored in input directory`() {
    // when
    val filesToUpLoadExample = ClientUIController.Examples.CONTROLLER_FILES_TO_UPLOAD_EXAMPLE
    controller.upload(filesToUpLoadExample)

    // then
    then(clientRootDir.isEmpty()).isFalse

    val files = filesToUpLoadExample.map { Pair(File(clientStorageDirectory.absolutePath + File.separator + "input" + File.separator + it.first), it.second) }
    files.forEach {
      then(it.first).isFile()
    }
    files.forEach {
      then(it.first.readBytes()).isEqualTo(it.second)
    }
  }

  @Test
  fun `storage directories are initialized`() {
    // given
    val rootPath = clientStorageDirectory.absolutePath
    val expectedInputDirectoryPath = rootPath + File.separator + "input"

    // when
    val files = clientStorageDirectory.listFiles()

    // then
    then(files).isNotNull
    then(files!!.map { it.absolutePath }).containsExactlyInAnyOrder(
      expectedInputDirectoryPath
    )
    then(File(expectedInputDirectoryPath)).isDirectory()
  }

  @Test
  fun `raise an error if list of files to upload is empty`() {
    // when
    val clientUploadResult = controller.upload(ClientUIController.Examples.CONTROLLER_NO_FILES_TO_UPLOAD_EXAMPLE)

    // then
    then(clientUploadResult.statusCode).isEqualTo(400)
    then(clientUploadResult.message).isEqualTo("no files provided")
  }

  @Test
  fun `can upload files and classifier is ran`() {
    // given
    FileIO.initFilesDirectory(referenceFilesDir.absolutePath)

    // when
    val uploadResult = controller.upload(ClientUIController.Examples.CONTROLLER_FILES_TO_UPLOAD_EXAMPLE)

    // then
    then(uploadResult.statusCode).isEqualTo(201)
    then(uploadResult.message).isEqualTo("")

    // classifier is called asynchronously
    classifier.testPurposeWaitComputationFinished()
    then(classifierEvents).isNotEmpty
    then(controller.results()).isEqualTo(ClientUIController.Examples.CONTROLLER_RESULTS)
  }

  @Test
  fun clear() {
    // given
    controller.upload(ClientUIController.Examples.CONTROLLER_FILES_TO_UPLOAD_EXAMPLE)
    // classifier is called asynchronously
    classifier.testPurposeWaitComputationFinished()
    then(controller.resultsAvailable()).isTrue
    then(controller.results()).isNotEmpty
    classifierEvents.clear()

    // when
    controller.clear()

    // then
    then(clientRootDir.isEmpty()).isTrue
    then(controller.resultsAvailable()).isFalse
    then(controller.results()).isEmpty()
    then(classifierEvents).containsExactly(ClassifierCleared())
  }
}
