package com.orange.documentare.clientui.biz.domain.classifier

import com.orange.documentare.clientui.biz.domain.classifier.Classifier.Companion.currentLevelReferenceFilesToCompareTo
import com.orange.documentare.clusteringui.biz.file.query.ClusterDistancesStats
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelFile
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Retrieve reference files (to compare to) in current hierarchy level")
class ClassifierRetrieveReferenceFilesInCurrentLevelTest {

  private val anotherClusterRefFiles = listOf(
    clusterCenterWithId(100),
    regularFileWithId(101)
  )

  private val cc1 = clusterCenterWithId(1)
  private val cc3 = clusterCenterWithId(3)
  private val ms2 = multisetWithId(2)

  @Test
  fun `find ref file in current hierarchy level`() {
    // Given
    val inReferenceFiles = listOf(cc1, cc3)
    val levelReferenceFiles = listOf(
      listOf(cc1, regularFileWithId(2)),
      listOf(cc3, regularFileWithId(4))
    )

    // When
    val refFilesToCompareToInCurrentLevel = currentLevelReferenceFilesToCompareTo(inReferenceFiles.map { it.aggregateId }, levelReferenceFiles, 1)

    // Then
    then(refFilesToCompareToInCurrentLevel).containsExactly(
      ReferenceFileContext(cc1, listOf(cc1), cc1.clusterDistancesStats!!),
      ReferenceFileContext(cc3, listOf(cc3), cc3.clusterDistancesStats!!)
    )
  }

  @Test
  fun `filter out multiset since its cluster center in current level is also 'inReferenceFiles', but keep multiset in multiset files`() {
    // Given
    val inReferenceFiles = listOf(cc1, ms2)
    val levelReferenceFiles = listOf(
      listOf(cc1, ms2),
      anotherClusterRefFiles
    )

    // When
    val refFilesToCompareToInCurrentLevel = currentLevelReferenceFilesToCompareTo(inReferenceFiles.map { it.aggregateId }, levelReferenceFiles, 1)

    // Then
    then(refFilesToCompareToInCurrentLevel).containsExactly(
      ReferenceFileContext(cc1, listOf(cc1, ms2), cc1.clusterDistancesStats!!)
    )
  }

  @Test
  fun `multiset file is in 'inReferenceFiles', but not cluster center THEN replace multiset by its cluster center and clustering stats are coming from cluster center`() {
    // Given
    val inReferenceFiles = listOf(ms2)
    val levelReferenceFiles = listOf(
      listOf(cc1, ms2),
      anotherClusterRefFiles
    )

    // When
    val refFilesToCompareToInCurrentLevel = currentLevelReferenceFilesToCompareTo(inReferenceFiles.map { it.aggregateId }, levelReferenceFiles, 1)

    // Then
    then(refFilesToCompareToInCurrentLevel).containsExactly(
      ReferenceFileContext(cc1, listOf(cc1, ms2), cc1.clusterDistancesStats!!)
    )
  }

  @Test
  fun `no reference files are provided, SO get all cluster centers`() {
    // Given
    val inReferenceFiles = emptyList<ClientHierarchyReferenceModelFile>()
    val levelReferenceFiles = listOf(
      listOf(cc1, regularFileWithId(2)),
      listOf(cc3, regularFileWithId(4))
    )

    // When
    val refFilesToCompareToInCurrentLevel = currentLevelReferenceFilesToCompareTo(inReferenceFiles.map { it.aggregateId }, levelReferenceFiles, 1)

    // Then
    then(refFilesToCompareToInCurrentLevel).containsExactly(
      ReferenceFileContext(cc1, listOf(cc1), cc1.clusterDistancesStats!!),
      ReferenceFileContext(cc3, listOf(cc3), cc3.clusterDistancesStats!!)
    )
  }


  @Test
  fun `no reference files are provided and current level contains a multiset`() {
    // Given
    val inReferenceFiles = emptyList<ClientHierarchyReferenceModelFile>()
    val levelReferenceFiles = listOf(listOf(cc1, ms2))

    // When
    val refFilesToCompareToInCurrentLevel = currentLevelReferenceFilesToCompareTo(inReferenceFiles.map { it.aggregateId }, levelReferenceFiles, 1)

    // Then
    then(refFilesToCompareToInCurrentLevel).containsExactly(
      ReferenceFileContext(cc1, listOf(cc1, ms2), cc1.clusterDistancesStats!!)
    )
  }

  private fun clusterCenterWithId(id: Int): ClientHierarchyReferenceModelFile = buildReferenceFile(id)
  private fun multisetWithId(id: Int): ClientHierarchyReferenceModelFile = buildReferenceFile(id, clusterCenter = false, multiset = true)
  private fun regularFileWithId(id: Int): ClientHierarchyReferenceModelFile = buildReferenceFile(id, clusterCenter = false, multiset = false)

  private fun buildReferenceFile(id: Int, clusterCenter: Boolean = true, multiset: Boolean = false): ClientHierarchyReferenceModelFile {
    return ClientHierarchyReferenceModelFile(
      "aggregateId$id",
      "filename$id",
      clusterCenter,
      multiset,
      // introduce some randomness with id
      ClusterDistancesStats(4, 100.0 + id, 10.0 + id, false),
      clusterMultiHypothesisName = null /* TODO CLIENT +++++ */,
      null,
      listOf("filename$id"),
      "/tmp/filename$id",
      "not used webPath",
      "not used thumbnail webPath"
    )
  }
}
