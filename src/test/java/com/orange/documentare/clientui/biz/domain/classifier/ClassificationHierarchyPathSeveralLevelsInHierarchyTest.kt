package com.orange.documentare.clientui.biz.domain.classifier

import com.orange.documentare.clientui.biz.domain.classifier.ClassificationTrustWorthinessType.DOUBT
import com.orange.documentare.clientui.biz.domain.classifier.ClassificationTrustWorthinessType.RELIABLE
import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class ClassificationHierarchyPathSeveralLevelsInHierarchyTest {
  private val hierarchyPath = ClassificationHierarchyPath()

  @Test
  fun `DOUBT then RELIABLE moving down the hierarchy`() {
    // given
    val clusterCenterLevel3Singleton = HierarchyExamples.clientExportRefFileLevel3F0Singleton()
    val clusterCenterLevel3SingletonClusterDistancesStats = clusterCenterLevel3Singleton.clusterDistancesStats!!
    val clusterCenterLevel2NotSingleton = HierarchyExamples.clientExportRefFileLevel2F0NotSingleton()
    val clusterCenterLevel2NotSingletonClusterDistancesStats = clusterCenterLevel2NotSingleton.clusterDistancesStats!!
    val clusterDistanceThreshold = 111.0
    val aboveThreshold = (clusterDistanceThreshold + 1).toInt()
    val belowThreshold = (clusterDistanceThreshold - 1).toInt()

    // when/then
    hierarchyPath.add(
      listOf(
        ClassifierDistanceToReferenceFile(
          clusterCenterLevel3Singleton,
          clusterCenterLevel3SingletonClusterDistancesStats,
          aboveThreshold,
          clusterDistanceThreshold
        )
      ), 3
    )
    then(hierarchyPath.trustWorthiness()).isEqualTo(ClassificationTrustWorthiness(DOUBT, 0))

    // when/then
    hierarchyPath.add(
      listOf(
        ClassifierDistanceToReferenceFile(
          clusterCenterLevel2NotSingleton,
          clusterCenterLevel2NotSingletonClusterDistancesStats,
          belowThreshold,
          clusterDistanceThreshold
        )
      ), 2
    )
    then(hierarchyPath.trustWorthiness()).isEqualTo(ClassificationTrustWorthiness(RELIABLE, 4))
  }

  @Test
  fun `RELIABLE, go through the whole hierarchy to reach level 0 in success`() {
    // given
    val clusterCenterLevel1Singleton = HierarchyExamples.clientExportRefFileLevel3F0Singleton()
    val clusterCenterLevel1SingletonClusterDistancesStats = clusterCenterLevel1Singleton.clusterDistancesStats!!
    val clusterCenterLevel0NotSingleton = HierarchyExamples.clientExportRefFileLevel0F0NotSingleton()
    val clusterCenterLevel0NotSingletonClusterDistancesStats = clusterCenterLevel0NotSingleton.clusterDistancesStats!!
    val clusterDistanceThreshold = 111.0
    val aboveThreshold = (clusterDistanceThreshold + 1).toInt()
    val belowThreshold = (clusterDistanceThreshold - 1).toInt()

    // when/then
    hierarchyPath.add(
      listOf(
        ClassifierDistanceToReferenceFile(
          clusterCenterLevel1Singleton,
          clusterCenterLevel1SingletonClusterDistancesStats,
          aboveThreshold,
          clusterDistanceThreshold
        )
      ), 1
    )
    then(hierarchyPath.trustWorthiness()).isEqualTo(ClassificationTrustWorthiness(DOUBT, 0))

    // when/then
    hierarchyPath.add(
      listOf(
        ClassifierDistanceToReferenceFile(
          clusterCenterLevel0NotSingleton,
          clusterCenterLevel0NotSingletonClusterDistancesStats,
          belowThreshold,
          clusterDistanceThreshold
        )
      ), 0
    )
    then(hierarchyPath.trustWorthiness()).isEqualTo(ClassificationTrustWorthiness(RELIABLE, 4))
  }


  @Test
  fun `DOUBT, go through the whole hierarchy to reach level 0 with a singleton as closest cluster`() {
    // given
    val clusterCenterLevel2Singleton = HierarchyExamples.clientExportRefFileLevel3F0Singleton()
    val clusterCenterLevel2SingletonClusterDistancesStats = clusterCenterLevel2Singleton.clusterDistancesStats!!
    val clusterCenterLevel1NotSingleton = HierarchyExamples.clientExportRefFileLevel2F0NotSingleton()
    val clusterCenterLevel1NotSingletonClusterDistancesStats = clusterCenterLevel1NotSingleton.clusterDistancesStats!!
    val clusterCenterLevel0Singleton = HierarchyExamples.clientExportRefFileLevel0F7Singleton()
    val clusterCenterLevel0SingletonClusterDistancesStats = clusterCenterLevel0Singleton.clusterDistancesStats!!
    val clusterDistanceThreshold = 111.0
    val aboveThreshold = (clusterDistanceThreshold + 1).toInt()
    val belowThreshold = (clusterDistanceThreshold - 1).toInt()

    // when/then
    hierarchyPath.add(
      listOf(
        ClassifierDistanceToReferenceFile(
          clusterCenterLevel2Singleton,
          clusterCenterLevel2SingletonClusterDistancesStats,
          aboveThreshold,
          clusterDistanceThreshold
        )
      ), 2
    )
    then(hierarchyPath.trustWorthiness()).isEqualTo(ClassificationTrustWorthiness(DOUBT, 0))

    // when/then
    hierarchyPath.add(
      listOf(
        ClassifierDistanceToReferenceFile(
          clusterCenterLevel1NotSingleton,
          clusterCenterLevel1NotSingletonClusterDistancesStats,
          belowThreshold,
          clusterDistanceThreshold
        )
      ), 1
    )
    then(hierarchyPath.trustWorthiness()).isEqualTo(ClassificationTrustWorthiness(RELIABLE, 4))

    // when/then
    hierarchyPath.add(
      listOf(
        ClassifierDistanceToReferenceFile(
          clusterCenterLevel0Singleton,
          clusterCenterLevel0SingletonClusterDistancesStats,
          aboveThreshold,
          clusterDistanceThreshold
        )
      ), 0
    )
    then(hierarchyPath.trustWorthiness()).isEqualTo(ClassificationTrustWorthiness(DOUBT, 4))
  }
}
