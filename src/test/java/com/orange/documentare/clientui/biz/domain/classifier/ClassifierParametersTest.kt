package com.orange.documentare.clientui.biz.domain.classifier

import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport
import org.assertj.core.api.BDDAssertions.catchThrowable
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider
import org.junit.jupiter.params.provider.ArgumentsSource
import java.util.stream.Stream

class ClassifierParametersTest {

  private val classifier = Classifier("", "", ClientHierarchyExport.fakeForTestClientHierarchyExport())

  @Test
  fun defaults() {
    then(classifier.classifierParameters()).isEqualTo(ClassifierParameters(2f))
  }

  @Test
  fun `can update parameters`() {
    // when
    classifier.updateClassifierParameters(ClassifierParameters(5.52f))
    // then
    then(classifier.classifierParameters()).isEqualTo(ClassifierParameters(5.52f))
  }

  @ParameterizedTest
  @ArgumentsSource(InvalidThreshold::class)
  fun `throw for invalid parameters`(threshold: Float) {
    // when
    val throwable = catchThrowable { classifier.updateClassifierParameters(ClassifierParameters(threshold)) }
    // then
    then(throwable.message).isEqualTo("\uD83D\uDD34 invalid classifier parameters: ClassifierParameters(clusterDistancesThresholdStandardDeviationFactor=$threshold)")
  }

  internal class InvalidThreshold : ArgumentsProvider {
    override fun provideArguments(context: ExtensionContext): Stream<out Arguments> {
      return Stream.of(
        Arguments.of(10.1f),
        Arguments.of(-0.1f)
      )
    }
  }
}
