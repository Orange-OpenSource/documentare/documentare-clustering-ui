package com.orange.documentare.clientui.biz.transport

import com.orange.documentare.clientui.biz.domain.ClientRootDir
import com.orange.documentare.clientui.biz.domain.ClientUIController
import com.orange.documentare.clientui.biz.domain.FrontClassificationResult
import com.orange.documentare.clientui.biz.domain.classifier.Classifier
import com.orange.documentare.clientui.biz.domain.classifier.ClassifierParameters
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport.Companion.fakeForTestClientHierarchyExport
import jakarta.servlet.*
import jakarta.servlet.http.*
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.springframework.ui.Model
import java.io.BufferedReader
import java.security.Principal
import java.util.*

class ClientUITest {

  private val fakeClientRootDir = ClientRootDir.fakeForTestNoIO()
  private val clientUI = ClientUI(ClientUIController(fakeForTestClientHierarchyExport(), fakeClientRootDir, "", ""))
  private val noListener = null

  @Test
  fun `home returns client home page`() {
    then(clientUI.clientHome(fakeHttpServletRequest())).isEqualTo("client-home")
  }

  @Test
  fun `home redirects to classification busy page`() {
    // given
    val clientUI =
      ClientUI(ClientUIController(fakeForTestClientHierarchyExport(), fakeClientRootDir, "", "", noListener, Classifier.fakingAlreadyRunningForTestPurpose()))
    // when/then
    then(clientUI.clientHome(fakeHttpServletRequest())).isEqualTo("redirect:/client/client-classification-running")
  }

  @Test
  fun `home redirects to classification results page`() {
    // given
    val clientUI =
      ClientUI(ClientUIController(fakeForTestClientHierarchyExport(), fakeClientRootDir, "", "", noListener, Classifier.withFakeResultsForTestPurpose()))
    // when/then
    then(clientUI.clientHome(fakeHttpServletRequest())).isEqualTo("redirect:/client/client-classification-results")
  }

  @Test
  fun `busy returns busy page`() {
    // given
    val clientUI =
      ClientUI(ClientUIController(fakeForTestClientHierarchyExport(), fakeClientRootDir, "", "", noListener, Classifier.fakingAlreadyRunningForTestPurpose()))
    // when/then
    then(clientUI.clientClassificationRunning(fakeHttpServletRequest())).isEqualTo("client-classification-running")
  }

  @Test
  fun `busy redirects to results page`() {
    // given
    val clientUI =
      ClientUI(ClientUIController(fakeForTestClientHierarchyExport(), fakeClientRootDir, "", "", noListener, Classifier.withFakeResultsForTestPurpose()))
    // when/then
    then(clientUI.clientClassificationRunning(fakeHttpServletRequest())).isEqualTo("redirect:/client/client-classification-results")
  }

  @Test
  fun `busy redirects to home page`() {
    then(clientUI.clientClassificationRunning(fakeHttpServletRequest())).isEqualTo("redirect:/client/")
  }

  @Test
  fun `classification results returns results page`() {
    // given
    val captureAttributes: MutableList<Pair<String, List<FrontClassificationResult>>> = mutableListOf()
    val controller = ClientUIController.withFakeResultsForTestPurpose()
    val clientUI =
      ClientUI(controller)
    // when
    val clientClassificationResults = clientUI.clientClassificationResults(fakeModel(captureAttributes))

    // then
    then(clientClassificationResults).isEqualTo("client-classification-results")
    then(captureAttributes).hasSize(1)
    then(captureAttributes.first().first).isEqualTo("classificationResults")
    then(captureAttributes.first().second).isEqualTo(ClientUIController.Examples.CONTROLLER_RESULTS)
  }

  @Test
  fun `classification results redirects to busy page`() {
    // given
    val clientUI =
      ClientUI(ClientUIController(fakeForTestClientHierarchyExport(), fakeClientRootDir, "", "", noListener, Classifier.fakingAlreadyRunningForTestPurpose()))
    // when/then
    then(clientUI.clientClassificationResults(fakeModel())).isEqualTo("redirect:/client/client-classification-running")
  }

  @Test
  fun `classification results redirects to home page`() {
    then(clientUI.clientClassificationResults(fakeModel())).isEqualTo("redirect:/client/")
  }

  //
  // PARAMETERS
  //

  @Test
  fun `parameters page contains current parameters`() {
    // given
    val captureAttributes: MutableList<Pair<String, ClassifierParameters>> = mutableListOf()

    // then
    then(clientUI.clientParameters(fakeModelParameters(captureAttributes))).isEqualTo("client-parameters")
    then(captureAttributes).hasSize(1)
    then(captureAttributes.first().first).isEqualTo("parameters")
    then(captureAttributes.first().second).isEqualTo(ClassifierParameters.defaults())
  }

  @Test
  fun `update parameters`() {
    // given
    val expectedClusterDistancesThresholdStandardDeviationFactor = 0.5f

    // when
    val redirect = clientUI.updateParameters(UpdateParametersDTO(expectedClusterDistancesThresholdStandardDeviationFactor))

    // then
    then(redirect).isEqualTo("client-home")

    val captureAttributes: MutableList<Pair<String, ClassifierParameters>> = mutableListOf()
    clientUI.clientParameters(fakeModelParameters(captureAttributes))
    then(captureAttributes.first().second).isEqualTo(ClassifierParameters(expectedClusterDistancesThresholdStandardDeviationFactor))
  }


  //
  // CLEAR
  //

  @Test
  fun clear() {
    // when
    clientUI.clear()

    // then
    then(fakeClientRootDir.clearFakeStorageCalled).isTrue
  }

  private fun fakeModelParameters(captureAttributes: MutableList<Pair<String, ClassifierParameters>> = mutableListOf()): Model = object : Model {
    override fun addAttribute(attributeName: String, attributeValue: Any?): Model {
      captureAttributes.add(Pair(attributeName, attributeValue as ClassifierParameters))
      return this
    }

    override fun addAttribute(attributeValue: Any): Model {
      TODO("Not yet implemented")
    }

    override fun addAllAttributes(attributeValues: MutableCollection<*>): Model {
      TODO("Not yet implemented")
    }

    override fun addAllAttributes(attributes: MutableMap<String, *>): Model {
      TODO("Not yet implemented")
    }

    override fun mergeAttributes(attributes: MutableMap<String, *>): Model {
      TODO("Not yet implemented")
    }

    override fun containsAttribute(attributeName: String): Boolean {
      TODO("Not yet implemented")
    }

    override fun getAttribute(attributeName: String): Any? {
      TODO("Not yet implemented")
    }

    override fun asMap(): MutableMap<String, Any> {
      TODO("Not yet implemented")
    }

  }


  private fun fakeModel(captureAttributes: MutableList<Pair<String, List<FrontClassificationResult>>> = mutableListOf()): Model = object : Model {
    override fun addAttribute(attributeName: String, attributeValue: Any?): Model {
      captureAttributes.add(Pair(attributeName, attributeValue as List<FrontClassificationResult>))
      return this
    }

    override fun addAttribute(attributeValue: Any): Model {
      TODO("Not yet implemented")
    }

    override fun addAllAttributes(attributeValues: MutableCollection<*>): Model {
      TODO("Not yet implemented")
    }

    override fun addAllAttributes(attributes: MutableMap<String, *>): Model {
      TODO("Not yet implemented")
    }

    override fun mergeAttributes(attributes: MutableMap<String, *>): Model {
      TODO("Not yet implemented")
    }

    override fun containsAttribute(attributeName: String): Boolean {
      TODO("Not yet implemented")
    }

    override fun getAttribute(attributeName: String): Any? {
      TODO("Not yet implemented")
    }

    override fun asMap(): MutableMap<String, Any> {
      TODO("Not yet implemented")
    }

  }

  private fun fakeHttpServletRequest(): HttpServletRequest = object : HttpServletRequest {
    override fun getAttribute(p0: String?): Any {
      TODO("Not yet implemented")
    }

    override fun getAttributeNames(): Enumeration<String> {
      TODO("Not yet implemented")
    }

    override fun getCharacterEncoding(): String {
      TODO("Not yet implemented")
    }

    override fun setCharacterEncoding(p0: String?) {
      TODO("Not yet implemented")
    }

    override fun getContentLength(): Int {
      TODO("Not yet implemented")
    }

    override fun getContentLengthLong(): Long {
      TODO("Not yet implemented")
    }

    override fun getContentType(): String {
      TODO("Not yet implemented")
    }

    override fun getInputStream(): ServletInputStream {
      TODO("Not yet implemented")
    }

    override fun getParameter(p0: String?): String {
      TODO("Not yet implemented")
    }

    override fun getParameterNames(): Enumeration<String> {
      TODO("Not yet implemented")
    }

    override fun getParameterValues(p0: String?): Array<String> {
      TODO("Not yet implemented")
    }

    override fun getParameterMap(): MutableMap<String, Array<String>> {
      TODO("Not yet implemented")
    }

    override fun getProtocol(): String {
      TODO("Not yet implemented")
    }

    override fun getScheme(): String {
      TODO("Not yet implemented")
    }

    override fun getServerName(): String {
      TODO("Not yet implemented")
    }

    override fun getServerPort(): Int {
      TODO("Not yet implemented")
    }

    override fun getReader(): BufferedReader {
      TODO("Not yet implemented")
    }

    override fun getRemoteAddr(): String {
      TODO("Not yet implemented")
    }

    override fun getRemoteHost(): String {
      TODO("Not yet implemented")
    }

    override fun setAttribute(p0: String?, p1: Any?) {
      TODO("Not yet implemented")
    }

    override fun removeAttribute(p0: String?) {
      TODO("Not yet implemented")
    }

    override fun getLocale(): Locale {
      TODO("Not yet implemented")
    }

    override fun getLocales(): Enumeration<Locale> {
      TODO("Not yet implemented")
    }

    override fun isSecure(): Boolean {
      TODO("Not yet implemented")
    }

    override fun getRequestDispatcher(p0: String?): RequestDispatcher {
      TODO("Not yet implemented")
    }

    override fun getRemotePort(): Int {
      TODO("Not yet implemented")
    }

    override fun getLocalName(): String {
      TODO("Not yet implemented")
    }

    override fun getLocalAddr(): String {
      TODO("Not yet implemented")
    }

    override fun getLocalPort(): Int {
      TODO("Not yet implemented")
    }

    override fun getServletContext(): ServletContext {
      TODO("Not yet implemented")
    }

    override fun startAsync(): AsyncContext {
      TODO("Not yet implemented")
    }

    override fun startAsync(p0: ServletRequest?, p1: ServletResponse?): AsyncContext {
      TODO("Not yet implemented")
    }

    override fun isAsyncStarted(): Boolean {
      TODO("Not yet implemented")
    }

    override fun isAsyncSupported(): Boolean {
      TODO("Not yet implemented")
    }

    override fun getAsyncContext(): AsyncContext {
      TODO("Not yet implemented")
    }

    override fun getDispatcherType(): DispatcherType {
      TODO("Not yet implemented")
    }

    override fun getRequestId(): String {
      TODO("Not yet implemented")
    }

    override fun getProtocolRequestId(): String {
      TODO("Not yet implemented")
    }

    override fun getServletConnection(): ServletConnection {
      TODO("Not yet implemented")
    }

    override fun getAuthType(): String {
      TODO("Not yet implemented")
    }

    override fun getCookies(): Array<Cookie> {
      TODO("Not yet implemented")
    }

    override fun getDateHeader(p0: String?): Long {
      TODO("Not yet implemented")
    }

    override fun getHeader(p0: String?): String {
      TODO("Not yet implemented")
    }

    override fun getHeaders(p0: String?): Enumeration<String> {
      TODO("Not yet implemented")
    }

    override fun getHeaderNames(): Enumeration<String> {
      TODO("Not yet implemented")
    }

    override fun getIntHeader(p0: String?): Int {
      TODO("Not yet implemented")
    }

    override fun getMethod(): String {
      TODO("Not yet implemented")
    }

    override fun getPathInfo(): String {
      TODO("Not yet implemented")
    }

    override fun getPathTranslated(): String {
      TODO("Not yet implemented")
    }

    override fun getContextPath(): String {
      TODO("Not yet implemented")
    }

    override fun getQueryString(): String {
      TODO("Not yet implemented")
    }

    override fun getRemoteUser(): String {
      TODO("Not yet implemented")
    }

    override fun isUserInRole(p0: String?): Boolean {
      TODO("Not yet implemented")
    }

    override fun getUserPrincipal(): Principal {
      TODO("Not yet implemented")
    }

    override fun getRequestedSessionId(): String {
      TODO("Not yet implemented")
    }

    override fun getRequestURI(): String {
      TODO("Not yet implemented")
    }

    override fun getRequestURL(): StringBuffer {
      TODO("Not yet implemented")
    }

    override fun getServletPath(): String {
      TODO("Not yet implemented")
    }

    override fun getSession(p0: Boolean): HttpSession {
      TODO("Not yet implemented")
    }

    override fun getSession(): HttpSession {
      TODO("Not yet implemented")
    }

    override fun changeSessionId(): String {
      TODO("Not yet implemented")
    }

    override fun isRequestedSessionIdValid(): Boolean {
      TODO("Not yet implemented")
    }

    override fun isRequestedSessionIdFromCookie(): Boolean {
      TODO("Not yet implemented")
    }

    override fun isRequestedSessionIdFromURL(): Boolean {
      TODO("Not yet implemented")
    }

    override fun authenticate(p0: HttpServletResponse?): Boolean {
      TODO("Not yet implemented")
    }

    override fun login(p0: String?, p1: String?) {
      TODO("Not yet implemented")
    }

    override fun logout() {
      TODO("Not yet implemented")
    }

    override fun getParts(): MutableCollection<Part> {
      TODO("Not yet implemented")
    }

    override fun getPart(p0: String?): Part {
      TODO("Not yet implemented")
    }

    override fun <T : HttpUpgradeHandler?> upgrade(p0: Class<T>?): T {
      TODO("Not yet implemented")
    }

  }
}
