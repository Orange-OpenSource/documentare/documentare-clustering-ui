package com.orange.documentare.clientui.biz.domain.classifier

import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples.clientExportRefFileLevel0F0NotSingleton
import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples.clientExportRefFileLevel0F7Singleton
import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples.clientExportRefFileLevel1F0NotSingleton
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ClassificationHierarchyPathTest {

  private val path = ClassificationHierarchyPath()
  private val threshold = 111.0
  private val distanceUnderThreshold = (threshold - 1).toInt()
  private val distanceAboveThreshold = (threshold + 1).toInt()
  private val clusterCenterNotSingleton = clientExportRefFileLevel0F0NotSingleton()
  private val clusterCenterNotSingletonMultihypothesis = clientExportRefFileLevel1F0NotSingleton()
  private val clusterCenterSingleton = clientExportRefFileLevel0F7Singleton()
  private val clusterNotSingletonDistancesStats = clusterCenterNotSingleton.clusterDistancesStats!!
  private val inLevel = 0

  @BeforeEach
  fun `not singleton check`() {
    then(clusterNotSingletonDistancesStats.singleton).isFalse
  }

  @Test
  fun `one cluster matching threshold criteria -- RELIABLE`() {
    // given
    path.add(
      listOf(ClassifierDistanceToReferenceFile(clusterCenterNotSingleton, clusterNotSingletonDistancesStats, distanceUnderThreshold, threshold)),
      inLevel
    )

    // when/then
    then(path.trustWorthiness()).isEqualTo(ClassificationTrustWorthiness(ClassificationTrustWorthinessType.RELIABLE, 2))
  }

  @Test
  fun `one singleton cluster -- DOUBT`() {
    // given
    val clusterSingletonDistancesStats = clusterCenterSingleton.clusterDistancesStats!!
    then(clusterSingletonDistancesStats.singleton).isTrue
    path.add(listOf(ClassifierDistanceToReferenceFile(clusterCenterSingleton, clusterSingletonDistancesStats, distanceAboveThreshold, threshold)), inLevel)

    // when/then
    then(path.trustWorthiness()).isEqualTo(ClassificationTrustWorthiness(ClassificationTrustWorthinessType.DOUBT, 0))
  }

  @Test
  fun `one cluster not matching inclusion criteria should raise an error`() {
    // when
    val runCatching = kotlin.runCatching {
      path.add(listOf(ClassifierDistanceToReferenceFile(clusterCenterNotSingleton, clusterNotSingletonDistancesStats, distanceAboveThreshold, threshold)), inLevel)
    }

    // then
    then(runCatching.isFailure).isTrue
    then(runCatching.exceptionOrNull()?.message).isEqualTo("cluster not matching inclusion criteria should not be added to hierarchy path", 0)
  }

  @Test
  fun `one multihypothesis cluster matching inclusion criteria -- DOUBT (multihypothesis)`() {
    // given
    path.add(
      listOf(
        ClassifierDistanceToReferenceFile(
          clusterCenterNotSingletonMultihypothesis,
          clusterCenterNotSingletonMultihypothesis.clusterDistancesStats!!,
          distanceUnderThreshold,
          threshold
        )
      ), inLevel
    )

    // when/then
    then(path.trustWorthiness()).isEqualTo(ClassificationTrustWorthiness(ClassificationTrustWorthinessType.DOUBT, 0))
  }

  @Test
  fun `more than one cluster matching inclusion criteria -- DOUBT`() {
    // given
    path.add(
      listOf(
        ClassifierDistanceToReferenceFile(
          clusterCenterNotSingleton,
          clusterNotSingletonDistancesStats, distanceUnderThreshold, threshold
        ),
        ClassifierDistanceToReferenceFile(
          clusterCenterNotSingleton,
          clusterNotSingletonDistancesStats, distanceUnderThreshold, threshold
        )
      ), inLevel
    )

    // when/then
    then(path.trustWorthiness()).isEqualTo(ClassificationTrustWorthiness(ClassificationTrustWorthinessType.DOUBT, 0))
  }

  @Test
  fun `no cluster matching inclusion criteria -- UNRELIABLE`() {
    // given
    path.add(listOf(ClassifierDistanceToReferenceFile(clusterCenterNotSingleton, clusterNotSingletonDistancesStats, distanceUnderThreshold, threshold)), inLevel)
    path.add(emptyList(), inLevel)

    // when/then
    then(path.trustWorthiness()).isEqualTo(ClassificationTrustWorthiness(ClassificationTrustWorthinessType.UNRELIABLE, 1))
  }
}

