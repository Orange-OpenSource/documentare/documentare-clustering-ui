package com.orange.documentare.clientui.biz.domain.classifier

import com.orange.documentare.clientui.biz.domain.classifier.events.ClassifierEvent
import com.orange.documentare.clientui.biz.domain.classifier.events.ClassifierListener
import com.orange.documentare.clusteringui.biz.file.query.ClusterDistancesStats
import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples
import com.orange.documentare.clusteringui.biz.parameters.domain.DistanceParameters
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelFile
import com.orange.documentare.core.computationserver.biz.distances.DistancesRequestResultDTO
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

class ClassifierDistanceTest {

  @TempDir
  lateinit var distancePrepDir: File

  @TempDir
  lateinit var rawImageCacheDir: File

  private lateinit var classifier: Classifier

  private val events = mutableListOf<ClassifierEvent>()

  @BeforeEach
  fun setup() {
    classifier = Classifier(
      distancePrepDir.absolutePath,
      rawImageCacheDir.absolutePath,
      HierarchyExamples.clientHierarchyExport(DistanceParameters.Examples.withoutRawConverter()),
      object : ClassifierListener {
        override fun onEvent(event: ClassifierEvent) {
          events.add(event)
        }
      })
  }

  private val mountain1 = File(javaClass.getResource("/client/mountain_1.jpg")!!.file)
  private val mountain2 = File(javaClass.getResource("/client/mountain_2.jpg")!!.file)
  private val garden = File(javaClass.getResource("/client/garden.jpg")!!.file)

  private val gardenAsRefFile = buildRefFile("garden", garden.absolutePath)
  private val mountain1AsRefFile = buildRefFile("mountain_1", mountain1.absolutePath)
  private val mountain2AsRefFile = buildRefFile("mountain_2", mountain2.absolutePath)

  @Test
  fun `distance between mountains and garden images`() {
    // given
    val mountain2ReferenceFileContext = ReferenceFileContext(mountain2AsRefFile, listOf(mountain2AsRefFile), mountain2AsRefFile.clusterDistancesStats!!)
    val gardenReferenceFileContext = ReferenceFileContext(gardenAsRefFile, listOf(gardenAsRefFile), gardenAsRefFile.clusterDistancesStats!!)

    // when
    val result = classifier.prodComputeDistances(mountain1, listOf(mountain2ReferenceFileContext, gardenReferenceFileContext))
    // then
    then(result).isEqualTo(DistancesRequestResultDTO.with(intArrayOf(929022, 965496)))
  }

  @Test
  fun `distance between garden and multiset`() {
    // given
    // simulate an ambiguous multiset containing mountains and gardens
    val mountainsReferenceFileContext = ReferenceFileContext(mountain1AsRefFile, listOf(mountain1AsRefFile, gardenAsRefFile), mountain1AsRefFile.clusterDistancesStats!!)
    // when
    val result3 = classifier.prodComputeDistances(garden, listOf(mountainsReferenceFileContext))
    // then
    then(result3).isEqualTo(DistancesRequestResultDTO.with(intArrayOf(468882)))
  }

  private fun buildRefFile(fileName: String, filePath: String) = ClientHierarchyReferenceModelFile(
    "",
    fileName,
    clusterCenter = true,
    multiset = false,
    clusterDistancesStats = ClusterDistancesStats.testExample(),
    clusterMultiHypothesisName = null /* TODO CLIENT +++++ */,
    className = null,
    fileNamedClass = listOf(fileName),
    absolutePath = filePath,
    webPath = "not used webPath",
    webThumbnailPath = "not used thumbnail webPath"
  )
}
