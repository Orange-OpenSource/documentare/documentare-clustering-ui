package com.orange.documentare.clientui.biz.domain.classifier

import com.orange.documentare.clientui.biz.domain.classifier.ClassificationHierarchyPath.Companion.computePathTrustWorthiness
import com.orange.documentare.clientui.biz.domain.classifier.ClassificationTrustWorthinessType.*
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class ClassificationHierarchyPathTrustWorthynessTest {

  @Test
  fun `figure out trustworthiness type and score`() {
    // Root level on the left, leaf level on the right
    then(computePathTrustWorthiness(listOf(UNRELIABLE))).isEqualTo(ClassificationTrustWorthiness(UNRELIABLE, -1))
    then(computePathTrustWorthiness(listOf(DOUBT))).isEqualTo(ClassificationTrustWorthiness(DOUBT, 0))
    then(computePathTrustWorthiness(listOf(RELIABLE))).isEqualTo(ClassificationTrustWorthiness(RELIABLE, 2))

    then(computePathTrustWorthiness(listOf(UNRELIABLE, UNRELIABLE, DOUBT))).isEqualTo(ClassificationTrustWorthiness(UNRELIABLE, -5))
    then(computePathTrustWorthiness(listOf(DOUBT, DOUBT))).isEqualTo(ClassificationTrustWorthiness(DOUBT, 0))
    then(computePathTrustWorthiness(listOf(DOUBT, RELIABLE))).isEqualTo(ClassificationTrustWorthiness(RELIABLE, 4))

    then(computePathTrustWorthiness(listOf(UNRELIABLE, UNRELIABLE, UNRELIABLE, RELIABLE, RELIABLE))).isEqualTo(ClassificationTrustWorthiness(RELIABLE, 6))

    then(computePathTrustWorthiness(listOf(RELIABLE, RELIABLE, UNRELIABLE, UNRELIABLE))).isEqualTo(ClassificationTrustWorthiness(UNRELIABLE, 3))
  }

  @Test
  fun `display trustworthiness type and score`() {
    // Root level on the left, leaf level on the right
    then(computePathTrustWorthiness(listOf(RELIABLE)).description()).isEqualTo("RELIABLE 2")
  }

}
