package com.orange.documentare.clientui.biz.domain.classifier

import com.orange.documentare.clientui.biz.domain.classifier.Classifier.Companion.ClosestReferenceFileData
import com.orange.documentare.clientui.biz.domain.classifier.Classifier.Companion.ClosestReferenceFileData.Companion.testExampleDistanceUnderClusterThreshold
import com.orange.documentare.clientui.biz.domain.classifier.Classifier.Companion.ClosestReferenceFileData.Companion.testExampleDistanceUnderClusterThresholdButAbove1
import com.orange.documentare.clientui.biz.domain.classifier.Classifier.Companion.ClosestReferenceFileData.Companion.testExampleNotMatchingAnyCriteria
import com.orange.documentare.clientui.biz.domain.classifier.Classifier.Companion.ClosestReferenceFileData.Companion.testExampleSingletonAutoInclusion
import com.orange.documentare.clientui.biz.domain.classifier.Classifier.Companion.doFindClustersMatchingInclusionCriteria
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class ClassifierFindMatchingClustersTest {

  @ParameterizedTest
  @MethodSource("closestReferenceFilesData")
  fun `find matching clusters`(testName: String, closestReferenceFilesData: List<ClosestReferenceFileData>, expectedShouldStop: Boolean, expectedMatchingClusters: List<String>) {
    // when
    val (stop, closestReferenceFilesIdMatchingInclusionCriteria) =
      doFindClustersMatchingInclusionCriteria(closestReferenceFilesData, "level N")

    // then
    then(stop).isEqualTo(expectedShouldStop)
    then(closestReferenceFilesIdMatchingInclusionCriteria).isEqualTo(expectedMatchingClusters)
  }

  companion object {
    @JvmStatic
    fun closestReferenceFilesData(): Stream<Arguments> {
      return Stream.of(
        `CONTINUE since 1 cluster matches distance threshold criteria`(),
        `CONTINUE since 1 cluster matches distance threshold criteria, another not matching any criteria`(),
        `CONTINUE since first cluster does not match any criteria, but second matches distance threshold criteria`(),
        `CONTINUE since 1 cluster matches singleton inclusion criteria`(),
        `CONTINUE with 2 clusters matching distance threshold criteria`(),
        `CONTINUE with 2 clusters matching distance threshold criteria and 1 singleton cluster`(),
        `CONTINUE with 1 cluster, another one removed with distance above 1`(),

        `STOP since 1 clusters is not matching any criteria`(),
        `STOP since 2 clusters are not matching any criteria`(),
        `STOP since 1 cluster is matching distance threshold criteria, but distance is above 1`(),
      )
    }

    private fun `CONTINUE since 1 cluster matches distance threshold criteria`() = Arguments.of(
      object {}.javaClass.enclosingMethod.name,
      listOf(
        testExampleDistanceUnderClusterThreshold("id1")
      ), false, listOf("id1")
    )

    private fun `CONTINUE since 1 cluster matches distance threshold criteria, another not matching any criteria`() = Arguments.of(
      object {}.javaClass.enclosingMethod.name,
      listOf(
        testExampleDistanceUnderClusterThreshold("id1"),
        testExampleNotMatchingAnyCriteria("id2"),
      ), false, listOf("id1")
    )

    private fun `CONTINUE since first cluster does not match any criteria, but second matches distance threshold criteria`() = Arguments.of(
      object {}.javaClass.enclosingMethod.name,
      listOf(
        testExampleNotMatchingAnyCriteria("id1"),
        testExampleDistanceUnderClusterThreshold("id2"),
      ), false, listOf("id2")
    )

    private fun `CONTINUE since 1 cluster matches singleton inclusion criteria`() = Arguments.of(
      object {}.javaClass.enclosingMethod.name,
      listOf(
        testExampleSingletonAutoInclusion("id1")
      ), false, listOf("id1")
    )

    private fun `STOP since 1 clusters is not matching any criteria`() = Arguments.of(
      object {}.javaClass.enclosingMethod.name,
      listOf(
        testExampleNotMatchingAnyCriteria("id1"),
      ), true, listOf("id1" /* only for display */)
    )

    private fun `STOP since 1 cluster is matching distance threshold criteria, but distance is above 1`() = Arguments.of(
      object {}.javaClass.enclosingMethod.name,
      listOf(
        testExampleDistanceUnderClusterThresholdButAbove1("id1"),
      ), true, listOf("id1" /* only for display */)
    )

    private fun `STOP since 2 clusters are not matching any criteria`() = Arguments.of(
      object {}.javaClass.enclosingMethod.name,
      listOf(
        testExampleNotMatchingAnyCriteria("id1"),
        testExampleNotMatchingAnyCriteria("id2"),
      ), true, listOf("id1" /* only for display */)
    )

    private fun `CONTINUE with 2 clusters matching distance threshold criteria`() = Arguments.of(
      object {}.javaClass.enclosingMethod.name,
      listOf(
        testExampleDistanceUnderClusterThreshold("id1"),
        testExampleDistanceUnderClusterThreshold("id2")
      ), false, listOf("id1", "id2")
    )

    private fun `CONTINUE with 1 cluster, another one removed with distance above 1`() = Arguments.of(
      object {}.javaClass.enclosingMethod.name,
      listOf(
        testExampleDistanceUnderClusterThreshold("id1"),
        testExampleDistanceUnderClusterThresholdButAbove1("id2")
      ), false, listOf("id1")
    )

    private fun `CONTINUE with 2 clusters matching distance threshold criteria and 1 singleton cluster`() = Arguments.of(
      object {}.javaClass.enclosingMethod.name,
      listOf(
        testExampleDistanceUnderClusterThreshold("id1"),
        testExampleDistanceUnderClusterThreshold("id2"),
        testExampleSingletonAutoInclusion("id3")
      ), false, listOf("id1", "id2", "id3")
    )
  }
}
