package com.orange.documentare.computationserver.tasks

import com.orange.documentare.computationserver.ComputationServerResponse
import com.orange.documentare.core.computationserver.biz.task.AvailableResultsTasksIdDTO
import com.orange.documentare.core.computationserver.biz.task.TaskController
import com.orange.documentare.core.computationserver.biz.task.Tasks
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

internal class GetAvailableTasksIdApiTest {

  private val tasks = Tasks()
  private val getAvailableTasksIdApi = GetAvailableTasksIdApi(TaskController(tasks))

  @Test
  fun `returns 200, and finished tasks id, 'ok' and 'not ok'`() {
    // given
    // not finished task
    tasks.newTask()
    val okId = tasks.newTask()
    val notOkId = tasks.newTask()
    tasks.addResult(okId, "ok")
    tasks.addErrorResult(notOkId, "not ok")

    // when
    val computationServerResponse: ComputationServerResponse<AvailableResultsTasksIdDTO> = getAvailableTasksIdApi.retrieveAvailableResultsTasksId()

    // then
    then(computationServerResponse.code()).isEqualTo(200)
    then(computationServerResponse.dto().availableResultsTasksId).containsExactlyInAnyOrder(okId, notOkId)
  }
}
