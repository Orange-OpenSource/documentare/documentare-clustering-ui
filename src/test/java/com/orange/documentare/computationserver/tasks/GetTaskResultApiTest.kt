package com.orange.documentare.computationserver.tasks

import com.orange.documentare.computationserver.ComputationServerResponse
import com.orange.documentare.core.computationserver.biz.task.TaskController
import com.orange.documentare.core.computationserver.biz.task.Tasks
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import java.util.*

internal class GetTaskResultApiTest {

  private val tasks = Tasks()
  private val getTaskResultApi = GetTaskResultApi(TaskController(tasks))

  @Test
  fun `returns 200 when task finished successfully`() {
    // given
    val taskId = tasks.newTask()
    val expectedResult = """{ "res": "a result" }"""
    tasks.addResult(taskId, expectedResult)

    // when
    val computationServerResponse: ComputationServerResponse<Any?> = getTaskResultApi.retrieveTaskResult(taskId)

    // then
    then(computationServerResponse).isEqualTo(ComputationServerResponse.with(expectedResult, 200))
  }

  @Test
  fun `returns 204 when task is not finished yet`() {
    // given
    val taskId = tasks.newTask()

    // when
    val computationServerResponse: ComputationServerResponse<Any?> = getTaskResultApi.retrieveTaskResult(taskId)

    // then
    then(computationServerResponse).isEqualTo(ComputationServerResponse.with(null, 204))
  }

  @Test
  fun `returns 404 NOT_FOUND for an unknown task id`() {
    // when
    val computationServerResponse: ComputationServerResponse<Any?> = getTaskResultApi.retrieveTaskResult("1234")

    // then
    then(computationServerResponse).isEqualTo(ComputationServerResponse.with(null, 404))
  }

  @Test
  fun `returns 207 Multistatus when clustering task is in error`() {
    // given
    val taskId = tasks.newTask()
    val expectedResult = """{ "res": "a result" }"""
    tasks.addErrorResult(taskId, expectedResult)

    // when
    val computationServerResponse: ComputationServerResponse<Any?> = getTaskResultApi.retrieveTaskResult(taskId)

    // then
    then(computationServerResponse).isEqualTo(ComputationServerResponse.with(Optional.of(expectedResult), 207))
  }
}
