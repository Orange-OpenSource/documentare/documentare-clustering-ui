package com.orange.documentare.computationserver.clustering

import com.orange.documentare.computationserver.ComputationServerResponse
import com.orange.documentare.core.computationserver.biz.RemoteTaskDTO
import com.orange.documentare.core.computationserver.biz.SharedDirectory
import com.orange.documentare.core.computationserver.biz.clustering.ClusteringController
import com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest
import com.orange.documentare.core.computationserver.biz.prepdata.AppPrepData
import com.orange.documentare.core.computationserver.biz.task.Tasks
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File
import java.util.*

// TODO:
// - Simdoc server : devrait se reposer sur le computation server pour faire le job
// - Simdoc server : erreur 500 / ClusteringRequestState.ERROR impossible, à nettoyer
// - ComputationServer, test du ClusteringController : cas d'erreur et 503 pas testés
// - ComputationServer : erreur 500 / ClusteringRequestState.ERROR impossible, à nettoyer
class ClusteringIntTest {
  private val tasks = Tasks()
  private lateinit var clusteringApi: ClusteringApi

  @TempDir
  lateinit var cacheDir: File

  @BeforeEach
  fun setup() {
    clusteringApi = ClusteringApi(buildClusteringController())
  }

  @Test
  fun `returns 400 BAD Request when request is invalid, input directory empty`() {
    // given
    val invalidRequest: ClusteringRequest = ClusteringRequest.builder().build()

    // when
    val computationServerResponse: ComputationServerResponse<RemoteTaskDTO> = clusteringApi.computeClustering(invalidRequest)

    // then
    then(computationServerResponse).isEqualTo(ComputationServerResponse.error<RemoteTaskDTO>(400, "inputDirectory and bytesData are missing"))
  }

  @Test
  fun `returns 400 BAD Request when request is invalid, input directory not reachable`(@TempDir outputDir: File) {
    // given
    val invalidRequest: ClusteringRequest = ClusteringRequest.builder()
      .inputDirectory("/r/t/y/u/u")
      .outputDirectory(outputDir.absolutePath)
      .build()

    // when
    val computationServerResponse: ComputationServerResponse<RemoteTaskDTO> = clusteringApi.computeClustering(invalidRequest)

    then(computationServerResponse).isEqualTo(ComputationServerResponse.error<RemoteTaskDTO>(400, "inputDirectory can not be reached: /r/t/y/u/u"))
  }

  @Test
  fun `returns 200 OK with a task Id when clustering server is available`(@TempDir outputDir: File) {
    // given
    val request = ClusteringRequest.builder()
      .inputDirectory(inputDirectory())
      .outputDirectory(outputDir.absolutePath)
      .build()

    // when
    val computationServerResponse: ComputationServerResponse<RemoteTaskDTO> = clusteringApi.computeClustering(request)

    // then
    then(computationServerResponse.code()).isEqualTo(200)

    val remoteTaskDTO = computationServerResponse.dto()
    then(remoteTaskDTO.id).isNotBlank

    val uuid = UUID.fromString(remoteTaskDTO.id)
    then(uuid.version()).isEqualTo(4)

    waitForTasksToBeFinished()
  }

  @Test
  fun `returns 503 when clustering server is no more available`(@TempDir outputDir: File) {
    // given
    val request = ClusteringRequest.builder()
      .inputDirectory(inputDirectory())
      .outputDirectory(outputDir.absolutePath)
      .build()

    // when
    var computationServerResponse: ComputationServerResponse<RemoteTaskDTO>
    do {
      computationServerResponse = clusteringApi.computeClustering(request)
    } while (computationServerResponse.code() != 503)

    waitForTasksToBeFinished()

    // then
    then(computationServerResponse.errorMessage()).isEqualTo("can not accept more tasks")
  }


  private fun buildClusteringController(): ClusteringController {
    val sharedDirectory: SharedDirectory = object : SharedDirectory {
      override fun sharedDirectoryAvailable() = false

      override fun sharedDirectoryRootPath(): String? {
        TODO("Not yet implemented")
      }
    }
    val appPrepData = AppPrepData("", cacheDir.absolutePath, sharedDirectory)
    return ClusteringController(appPrepData, sharedDirectory, tasks)
  }

  // Wait for all tasks to be finished to avoid raising errors (outputDir would be erased with tasks trying to access it...)
  private fun waitForTasksToBeFinished() {
    do {
      Thread.sleep(10)
    } while (!tasks.isIdle())
  }

  companion object {
    private fun inputDirectory(): String {
      return File(ClusteringIntTest::class.java.getResource("/test-dir")?.file ?: "failed to load test resource").absolutePath
    }
  }
}

