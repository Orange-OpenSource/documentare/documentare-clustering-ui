package com.orange.documentare.computationserver.distances

import com.orange.documentare.computationserver.ComputationServerResponse
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData
import com.orange.documentare.core.computationserver.biz.RemoteTaskDTO
import com.orange.documentare.core.computationserver.biz.SharedDirectory
import com.orange.documentare.core.computationserver.biz.distances.DistancesController
import com.orange.documentare.core.computationserver.biz.distances.api.DistancesRequest
import com.orange.documentare.core.computationserver.biz.prepdata.AppPrepData
import com.orange.documentare.core.computationserver.biz.task.Tasks
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import java.util.*


internal class DistancesApiTest {

  private val tasks = Tasks()
  private lateinit var distancesApi: DistancesApi

  lateinit var cacheDir: File

  @BeforeEach
  fun setup() {
    // do not use @TmpDir since as our code delete files in it at the same time junit try to delete the dir, we got strange exceptions...
    cacheDir = File("/tmp/" + UUID.randomUUID());
    cacheDir.mkdirs()
    distancesApi = DistancesApi(buildDistancesController())
  }

  @AfterEach
  fun cleanup() {
    // sometimes it may be seen as not empty just after the test
    cacheDir.deleteRecursively()
  }

  @Test
  fun `returns 400 BAD Request when request is invalid`() {
    // given
    val invalidRequest = DistancesRequest()
    // when
    val computationServerResponse: ComputationServerResponse<RemoteTaskDTO> = distancesApi.computeDistances(invalidRequest)

    // then
    then(computationServerResponse).isEqualTo(ComputationServerResponse.error<RemoteTaskDTO>(400, "element is missing"))
  }

  @Test
  fun `returns 200 OK with a task Id when clustering server is available`() {
    // given
    val request = DistancesRequest.builder()
      .element(arrayOf(load("f2")))
      .compareTo(loadFiles())
      .build()

    // when
    val computationServerResponse: ComputationServerResponse<RemoteTaskDTO> = distancesApi.computeDistances(request)

    // then
    then(computationServerResponse.code()).isEqualTo(200)
    then(computationServerResponse.dto().id).isNotBlank
  }

  @Test
  fun `returns 503 when clustering server is no more available`() {
    // given
    val request = DistancesRequest.builder()
      .element(arrayOf(load("f2")))
      .compareTo(loadFiles())
      .build()

    // when
    var computationServerResponse: ComputationServerResponse<RemoteTaskDTO>
    do {
      computationServerResponse = distancesApi.computeDistances(request)
    } while (computationServerResponse.code() != 503)

    // then
    then(computationServerResponse.errorMessage()).isEqualTo("can not accept more tasks")
  }

  private fun buildDistancesController(): DistancesController {
    return DistancesController(tasks, AppPrepData("", cacheDir.absolutePath, object : SharedDirectory {
      override fun sharedDirectoryAvailable() = false

      override fun sharedDirectoryRootPath(): String? {
        TODO("Not yet implemented")
      }

    }))
  }

  companion object {
    private val FILES = arrayOf("f3", "f1", "f4", "f2")

    private fun load(id: String): BytesData {
      val path = "/test-dir/"
      val file = File(DistancesApiTest::class.java.getResource(path + id)?.file ?: "failed to load test resource")
      return BytesData(listOf(id), listOf(file.absolutePath))
    }

    private fun loadFiles() = FILES.map { this.load(it) }.toTypedArray()
  }
}
