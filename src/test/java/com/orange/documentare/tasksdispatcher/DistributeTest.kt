/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.tasksdispatcher

import com.orange.documentare.tasksdispatcher.TasksDispatcher.ChangeListener
import jakarta.json.bind.JsonbBuilder
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import java.util.function.BiConsumer

class DistributeTest {

  private val server = MockWebServer()
  private val url = server.url("/").toString()
  private val urls = listOf(url)
  private val resultObserver = BiConsumer<ApiWrapper<MyApi, Result>, Result> { obj, result -> (obj as MyApiWrapper).result = result }

  @Test
  fun `Receive a task id and then a result`() {
    // when / then
    runTest(DefaultChangeListener(server), "I am a result for task 1")
  }

  @Test
  fun `receive 503 errors (server not available), then receive a task id and then a result`() {
    // given
    var err503Count = 0
    val changeListener = object : DefaultChangeListener(server) {
      override fun willRetrieveATaskId(url: String) {
        if (err503Count++ == 0) {
          mockServerUnavailable()
        } else {
          super.willRetrieveATaskId(url)
        }
      }
    }

    // when / then
    runTest(changeListener, "I am a result for task 1")
  }

  @Test
  fun `receive a task id, then 503 error while retrieving result, then redistribute task`() {
    // given
    var err503Count = 0
    val changeListener = object : DefaultChangeListener(server) {
      override fun willRetrieveResult(forTaskId: String, url: String) {
        if (err503Count++ == 0) {
          mockServerUnavailable()
          tasksOnGoing.clear()
        } else {
          super.willRetrieveResult(forTaskId, url)
        }
      }
    }

    // when / then
    runTest(changeListener, "I am a result for task 2")
  }

  @Test
  fun `receive a task id, then a result which embeds an error, then redistribute task`() {
    // given
    var resultIsAnError = true
    val changeListener = object : DefaultChangeListener(server) {
      override fun willRetrieveResult(forTaskId: String, url: String) {
        if (resultIsAnError) {
          resultIsAnError = false
          tasksOnGoing.clear()
          mockRetrieveResultWith207AndErrorResult()
        } else {
          mockRetrieveResult(forTaskId)
        }
      }
    }

    // when / then
    runTest(changeListener, "I am a result for task 2")
  }

  @Test
  fun `receive a task id, then network error while retrieving result (exception), then redistribute task`() {
    // given
    var raiseException = true
    val changeListener = object : DefaultChangeListener(server) {
      override fun willRetrieveResult(forTaskId: String, url: String) {
        if (raiseException) {
          mockRetrieveResultWithParseException()
          raiseException = false
          tasksOnGoing.clear()
        } else {
          super.willRetrieveResult(forTaskId, url)
        }
      }
    }

    // when / then
    runTest(changeListener, "I am a result for task 2")
  }


  @Test
  fun `can receive two results`() {
    // when
    val myApiWrapper1 = MyApiWrapper()
    val myApiWrapper2 = MyApiWrapper()
    val apiWrappers = listOf(myApiWrapper1, myApiWrapper2)

    // when
    TasksDispatcher(apiWrappers, urls, resultObserver, DefaultChangeListener(server), null)
      .distributeTasks()

    // then
    then(myApiWrapper1.result!!.result).isEqualTo("I am a result for task 1")
    then(myApiWrapper2.result!!.result).isEqualTo("I am a result for task 2")
  }

  private fun runTest(changeListener: DefaultChangeListener, expectedResult: String) {
    val myApiWrapper = MyApiWrapper()
    val apiWrappers = listOf(myApiWrapper)

    // when
    TasksDispatcher(apiWrappers, urls, resultObserver, changeListener, null)
      .distributeTasks()

    // then
    then(myApiWrapper.result!!.result).isEqualTo(expectedResult)
  }

  open class DefaultChangeListener(private val server: MockWebServer) : ChangeListener {
    internal var tasksOnGoing = mutableListOf<String>()
    private var taskIdCount = 1

    override fun willRetrieveAvailableResultsTasksId(url: String) {
      if (tasksOnGoing.isNotEmpty()) {
        mockAvailableResultsTasksIdWith(tasksOnGoing)
      } else {
        mockAvailableResultsTasksIdWith(emptyList())
      }
    }

    override fun willRetrieveATaskId(url: String) {
      val taskId = taskIdCount.toString()
      taskIdCount++
      mockRetrieveTaskId(taskId)
    }

    override fun willRetrieveResult(forTaskId: String, url: String) {
      mockRetrieveResult(forTaskId)
    }

    override fun receivedTaskId(taskId: String, url: String) {
      tasksOnGoing.add(taskId)
    }

    override fun receivedResultForTaskId(taskId: String, url: String) {
      tasksOnGoing.remove(taskId)
    }

    private fun mockAvailableResultsTasksIdWith(tasksIdList: List<String>) {
      val availableResultsTasksIdDTO = AvailableResultsTasksIdDTO(tasksIdList)
      val json = JsonbBuilder.create().toJson(availableResultsTasksIdDTO)
      server.enqueue(MockResponse().setBody(json))
    }

    private fun mockRetrieveTaskId(taskId: String) {
      server.enqueue(MockResponse().setBody("{\"id\": \"$taskId\"}"))
    }

    internal fun mockRetrieveResult(taskId: String) {
      server.enqueue(MockResponse().setBody("{\"result\": \"I am a result for task $taskId\"}"))
    }

    internal fun mockRetrieveResultWithParseException() {
      server.enqueue(MockResponse().setBody("result\": }"))
    }

    internal fun mockRetrieveResultWith207AndErrorResult() {
      server.enqueue(MockResponse().setResponseCode(207).setBody("{\"result\": \"contains an error\"}"))
    }

    internal fun mockServerUnavailable() {
      server.enqueue(MockResponse().setResponseCode(503))
    }
  }
}
