/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.tasksdispatcher

import com.orange.documentare.computationserver.ComputationServerResponse
import retrofit2.Response
import java.io.IOException
import java.util.*

class MyApiWrapper : ApiWrapper<MyApi, Result>() {
  private val requestId = UUID.randomUUID().toString()

  var result: Result? = null

  override fun callComputationApi(url: String): ComputationServerResponse<RemoteTaskDTO> {
    val remoteTaskDTOResponse: Response<RemoteTaskDTO>
    return try {
      remoteTaskDTOResponse = buildNetworkApi(MyApi::class.java, url).computationApi().execute()
      ComputationServerResponse.with(remoteTaskDTOResponse.body(), remoteTaskDTOResponse.code())
    } catch (e: IOException) {
      ComputationServerResponse.hasThrown(e)
    }
  }

  override fun callTaskApi(taskId: String, url: String): ComputationServerResponse<Result> {
    val resultResponse: Response<Result>
    return try {
      resultResponse = buildNetworkApi(MyApi::class.java, url).taskApi(taskId).execute()
      ComputationServerResponse.with(resultResponse.body(), resultResponse.code())
    } catch (e: IOException) {
      ComputationServerResponse.hasThrown(e)
    }
  }

  override fun requestId() = requestId
}
