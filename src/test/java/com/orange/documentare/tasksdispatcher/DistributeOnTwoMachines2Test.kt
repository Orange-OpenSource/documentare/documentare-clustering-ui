/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.tasksdispatcher

import jakarta.json.bind.JsonbBuilder
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import java.util.function.BiConsumer

class DistributeOnTwoMachines2Test {
  private val server1 = MockWebServer()
  private val server2 = MockWebServer()

  private val servers = listOf(server1, server2)
  private val mapUrlToServer = servers.associateBy { it.url("/").toString() }

  private val resultObserver = BiConsumer<ApiWrapper<MyApi, Result>, Result> { obj, result -> (obj as MyApiWrapper).result = result }


  @Test
  fun `Receive a task id and then a result on two machines`() {
    // given
    val apiWrappers = listOf(MyApiWrapper(), MyApiWrapper())

    // when
    TasksDispatcher(apiWrappers, mapUrlToServer.keys.toList(), resultObserver, DefaultChangeListener(mapUrlToServer), null)
      .distributeTasks()

    // then
    then(apiWrappers.map { it.result!!.result }).containsExactly(
      "I am a result for task 1, from server with port ${server1.port}",
      "I am a result for task 2, from server with port ${server2.port}"
    )
  }

  @Test
  fun `First machine is not available, and all tasks are distributed to second server`() {
    // given
    val apiWrappers = listOf(MyApiWrapper(), MyApiWrapper())

    val changeListener = object : DefaultChangeListener(mapUrlToServer) {
      override fun willRetrieveATaskId(url: String) {
        if (mapUrlToServer.keys.toList().indexOf(url) == 0) {
          mockServerUnavailable(url)
        } else {
          super.willRetrieveATaskId(url)
        }
      }
    }

    // when
    TasksDispatcher(apiWrappers, mapUrlToServer.keys.toList(), resultObserver, changeListener, null)
      .distributeTasks()

    // then
    then(apiWrappers.map { it.result!!.result }).containsExactlyInAnyOrder(
      "I am a result for task 1, from server with port ${server2.port}",
      "I am a result for task 2, from server with port ${server2.port}"
    )
  }

  open class DefaultChangeListener(private val mapUrlToServer: Map<String, MockWebServer>) : TasksDispatcher.ChangeListener {
    private var mapServerUrlToOnGoingTasks = mutableMapOf<String, MutableList<String>>()
    private var taskIdCount = 1

    override fun willRetrieveAvailableResultsTasksId(url: String) {
      val tasksOnGoing = mapServerUrlToOnGoingTasks.getOrDefault(url, mutableListOf())
      mapServerUrlToOnGoingTasks[url] = tasksOnGoing
      if (tasksOnGoing.isNotEmpty()) {
        mockAvailableResultsTasksIdWith(tasksOnGoing, url)
      } else {
        mockAvailableResultsTasksIdWith(emptyList(), url)
      }
    }

    override fun willRetrieveATaskId(url: String) {
      val taskId = taskIdCount.toString()
      taskIdCount++
      mockRetrieveTaskId(taskId, url)
    }

    override fun willRetrieveResult(forTaskId: String, url: String) {
      mockRetrieveResult(forTaskId, url)
    }

    override fun receivedTaskId(taskId: String, url: String) {
      val tasksOnGoing = mapServerUrlToOnGoingTasks.getOrDefault(url, mutableListOf())
      tasksOnGoing.add(taskId)
      mapServerUrlToOnGoingTasks[url] = tasksOnGoing
    }

    override fun receivedResultForTaskId(taskId: String, url: String) {
      mapServerUrlToOnGoingTasks[url]!!.remove(taskId)
    }

    private fun mockAvailableResultsTasksIdWith(tasksIdList: List<String>, url: String) {
      val availableResultsTasksIdDTO = AvailableResultsTasksIdDTO(tasksIdList)
      val json = JsonbBuilder.create().toJson(availableResultsTasksIdDTO)
      getServerFromUrl(url).enqueue(MockResponse().setBody(json))
    }

    private fun mockRetrieveTaskId(taskId: String, url: String) {
      getServerFromUrl(url).enqueue(MockResponse().setBody("{\"id\": \"$taskId\"}"))
    }

    private fun mockRetrieveResult(taskId: String, url: String) {
      val server = getServerFromUrl(url)
      server.enqueue(MockResponse().setBody("{\"result\": \"I am a result for task $taskId, from server with port ${server.port}\"}"))
    }

    internal fun mockServerUnavailable(url: String) {
      getServerFromUrl(url).enqueue(MockResponse().setResponseCode(503))
    }

    private fun getServerFromUrl(url: String) = mapUrlToServer[url] ?: error("should match a server")
  }
}
