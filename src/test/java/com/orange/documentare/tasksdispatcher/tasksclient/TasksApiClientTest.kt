/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.tasksdispatcher.tasksclient

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class TasksApiClientTest {
  private val server = MockWebServer()
  private val url = server.url("/").toString()

  @Test
  fun `can retrieve available results tasks id`() {
    // given
    server.enqueue(MockResponse().setBody("""{"availableResultsTasksId":["1234"]}"""))

    // when
    val availableResultsTasksIdDTO = RemoteTasksApiClient(url).availableResultsTasksIdSync()

    // then
    then(availableResultsTasksIdDTO).isNotNull
    then(availableResultsTasksIdDTO?.availableResultsTasksId).containsExactly("1234")
  }
}
