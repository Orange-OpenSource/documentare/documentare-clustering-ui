/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.tasksdispatcher

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface MyApi {
  @GET("/do")
  fun computationApi(): Call<RemoteTaskDTO>

  @GET("/task/{taskId}")
  fun taskApi(@Path("taskId") taskId: String): Call<Result>
}
