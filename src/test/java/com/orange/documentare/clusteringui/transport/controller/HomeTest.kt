package com.orange.documentare.clusteringui.transport.controller

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.transport.Home
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class HomeTest {
  @Test
  fun create_thumbnails_view_model_from_DB_DomainFiles() {
    // given
    val files: MutableList<DomainFile> = mutableListOf()
    // valid hex string has a 24 length size
    files.add(DomainFile.builder().aggregateId("000000000000000000000001").build())
    files.add(DomainFile.builder().aggregateId("000000000000000000000002").build())

    // when
    val thumbnailsViewModel: List<String> = Home.buildThumbnailsModel(files)

    // then
    then(thumbnailsViewModel).containsExactly("/img/no_image.png", "/img/no_image.png")
  }
}
