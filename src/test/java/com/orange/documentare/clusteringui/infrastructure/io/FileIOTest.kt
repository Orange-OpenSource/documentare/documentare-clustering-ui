package com.orange.documentare.clusteringui.infrastructure.io

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

/*
 * Copyright (c) 2018 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
class FileIOTest {

  @TempDir
  lateinit var tmpDir: File

  @BeforeEach
  fun setup() {
    FileIO.initFilesDirectory(tmpDir.absolutePath + "/not-initialized-yet/")
  }

  @Test
  fun compute_file_web_path_of_a_file_with_an_extension() {
    // given
    val domainFile = DomainFile.builder()
      .aggregateId(ID)
      .filename(FILENAME_WITH_EXTENSION)
      .build()

    // when
    val webPath = FileIO.fileWebPathOf(domainFile)

    // then
    then(webPath).isEqualTo("/9876.toto")
  }

  @Test
  fun compute_file_web_path_of_a_file_without_an_extension() {
    // given
    val domainFile = DomainFile.builder()
      .aggregateId(ID)
      .filename(FILENAME_WITHOUT_EXTENSION)
      .build()

    // when
    val webPath = FileIO.fileWebPathOf(domainFile)

    // then
    then(webPath).isEqualTo("/9876")
  }

  @Test
  fun compute_file_path_of_a_file_with_an_extension() {
    // given
    val domainFile = DomainFile.builder()
      .aggregateId(ID)
      .filename(FILENAME_WITH_EXTENSION)
      .build()

    // when
    val path = FileIO.filePathOf(domainFile)

    // then
    then(path).isEqualTo(tmpDir.absolutePath + "/not-initialized-yet/9876.toto")
  }

  @Test
  fun compute_file_path_of_a_file_without_an_extension() {
    // given
    val domainFile = DomainFile.builder()
      .aggregateId(ID)
      .filename(FILENAME_WITHOUT_EXTENSION)
      .build()

    // when
    val path = FileIO.filePathOf(domainFile)

    // then
    then(path).isEqualTo(tmpDir.absolutePath + "/not-initialized-yet/9876")
  }

  @Test
  fun compute_files_paths_of_files_with_and_without_an_extension() {
    // given
    val fileWithExtension = DomainFile.builder()
      .aggregateId(ID)
      .filename(FILENAME_WITH_EXTENSION)
      .build()
    val fileWithoutExtension = DomainFile.builder()
      .aggregateId(ID)
      .filename(FILENAME_WITHOUT_EXTENSION)
      .build()
    val files = listOf(fileWithExtension, fileWithoutExtension, fileWithExtension)

    // when
    val paths = FileIO.filesPathOf(files)

    // then
    then(paths).containsExactly(
      tmpDir.absolutePath + "/not-initialized-yet/9876.toto",
      tmpDir.absolutePath + "/not-initialized-yet/9876",
      tmpDir.absolutePath + "/not-initialized-yet/9876.toto"
    )
  }

  @Test
  fun `thumbnail is not available`() {
    then(FileIO.hasThumbnail(DomainFile.builder().aggregateId("id0").build())).isFalse
  }

  @Test
  fun `thumbnail is available`() {
    // given
    val domainFile = DomainFile.builder().aggregateId("id0").build()
    File(FileIO.thumbnailPathOf(domainFile)).createNewFile()

    // when
    then(FileIO.hasThumbnail(domainFile)).isTrue
  }

  companion object {
    private const val ID = "9876"
    private const val FILENAME_WITH_EXTENSION = "arthur.toto"
    private const val FILENAME_WITHOUT_EXTENSION = "arthur"
  }
}
