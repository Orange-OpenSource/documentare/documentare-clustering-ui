package com.orange.documentare.clusteringui.infrastructure.persistence

import com.orange.documentare.clusteringui.biz.file.domain.DomainFileHistory
import com.orange.documentare.clusteringui.biz.file.event.ClusterLocked
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.tophe.ddd.infrastructure.persistence.RepoPageable
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import java.util.*

internal class DBEventRepositoryTest {

  private val fakeHardPersistenceRepository = EventInMemoryRepositoryPostgresForTestPurpose()
  private val dbEventRepository: DBEventRepository = DBEventRepository(fakeHardPersistenceRepository)

  @Test
  fun `by default data are persisted in hard persistence repository`() {
    // when
    dbEventRepository.insert(listOf(FileCreated.example(), ClusterLocked.example()))

    // then
    then(fakeHardPersistenceRepository.count()).isEqualTo(2)
  }

  @Test
  fun `pause hard persistence and append events to in memory DB, we can load Files with new events`() {
    // given
    val fileCreated = FileCreated.example()

    // when
    dbEventRepository.insert(listOf(fileCreated))
    dbEventRepository.pauseHardPersistenceAndAppendToVolatilePersistence()
    dbEventRepository.insert(listOf(ClusterLocked.example()))

    // then
    then(fakeHardPersistenceRepository.count()).isEqualTo(1)
    val domainFile = DomainFileHistory.from(listOf(fileCreated, ClusterLocked.example()))
    then(dbEventRepository.load(fileCreated.aggregateId)).isEqualTo(domainFile)
  }

  @Test
  fun `reset to go back to normal persistence mode`() {
    // when
    dbEventRepository.pauseHardPersistenceAndAppendToVolatilePersistence()
    dbEventRepository.insert(listOf(FileCreated.example()))

    then(dbEventRepository.isVolatilePersistenceEmpty()).isFalse
    then(dbEventRepository.isNormalPersistenceMode()).isFalse

    dbEventRepository.resumeHardPersistenceAndDropVolatilePersistence()

    // then
    then(dbEventRepository.isVolatilePersistenceEmpty()).isTrue
    then(dbEventRepository.isNormalPersistenceMode()).isTrue
  }


  @Test
  fun `stop persistence completely would raise an exception if we try to append an event`() {
    // given
    val fileCreated = FileCreated.example()

    // when
    dbEventRepository.stopHardAndVolatilePersistence()

    val result = runCatching { dbEventRepository.insert(listOf(fileCreated)) }

    // then
    then(result.isFailure).isTrue
    then(result.exceptionOrNull()?.message).isEqualTo("\n\n❌ persistence is stopped but you tried to persist an event\n")
  }

  @Test
  fun `not supported operations with stopped or paused persistence raise an exception`() {
    // when COUNT
    dbEventRepository.pauseHardPersistenceAndAppendToVolatilePersistence()
    val result1 = runCatching { dbEventRepository.count() }

    dbEventRepository.resumeHardPersistenceAndDropVolatilePersistence()
    dbEventRepository.stopHardAndVolatilePersistence()
    val result2 = runCatching { dbEventRepository.count() }

    // when FIND_ALL
    dbEventRepository.pauseHardPersistenceAndAppendToVolatilePersistence()
    val result3 = runCatching { dbEventRepository.findAll() }

    dbEventRepository.resumeHardPersistenceAndDropVolatilePersistence()
    dbEventRepository.stopHardAndVolatilePersistence()
    val result4 = runCatching { dbEventRepository.findAll() }

    // when FIND_ALL_IN_PAGE
    dbEventRepository.pauseHardPersistenceAndAppendToVolatilePersistence()
    val result5 = runCatching { dbEventRepository.findAllInPage(RepoPageable(0, 10, OptionalLong.empty())) }

    dbEventRepository.resumeHardPersistenceAndDropVolatilePersistence()
    dbEventRepository.stopHardAndVolatilePersistence()
    val result6 = runCatching { dbEventRepository.findAllInPage(RepoPageable(0, 10, OptionalLong.empty())) }

    // then
    then(result1.exceptionOrNull()?.message).isEqualTo("\n\n❌ persistence is stopped or paused and you tried a not supported operation: count\n")
    then(result2.exceptionOrNull()?.message).isEqualTo("\n\n❌ persistence is stopped or paused and you tried a not supported operation: count\n")

    then(result3.exceptionOrNull()?.message).isEqualTo("\n\n❌ persistence is stopped or paused and you tried a not supported operation: findAll\n")
    then(result4.exceptionOrNull()?.message).isEqualTo("\n\n❌ persistence is stopped or paused and you tried a not supported operation: findAll\n")

    then(result5.exceptionOrNull()?.message).isEqualTo("\n\n❌ persistence is stopped or paused and you tried a not supported operation: findAllInPage\n")
    then(result6.exceptionOrNull()?.message).isEqualTo("\n\n❌ persistence is stopped or paused and you tried a not supported operation: findAllInPage\n")
  }

  @Test
  fun `delete all operation clears volatile memory as well`() {
    // given

    // when
    dbEventRepository.pauseHardPersistenceAndAppendToVolatilePersistence()
    dbEventRepository.insert(listOf(FileCreated.example(), ClusterLocked.example()))
    dbEventRepository.deleteAll()

    // then
    then(dbEventRepository.isVolatilePersistenceEmpty()).isTrue
  }

  @Test
  fun `load several aggregates with hard persistence`() {
    // given
    val fileCreated1 = FileCreated.example()
    val fileCreated2 = FileCreated(UUID.randomUUID().toString(), "f2")
    val fileCreated3 = FileCreated(UUID.randomUUID().toString(), "f3")
    dbEventRepository.insert(listOf(fileCreated1, fileCreated2, fileCreated3))

    // when
    val domainFiles = dbEventRepository.load(listOf(fileCreated1.aggregateId, fileCreated3.aggregateId))

    // then
    then(domainFiles).containsExactly(
      DomainFileHistory.from(listOf(fileCreated1)),
      DomainFileHistory.from(listOf(fileCreated3))
    )
  }

  @Test
  fun `load several aggregates with volatile persistence`() {
    // given
    val fileCreated1 = FileCreated.example()
    val fileCreated2 = FileCreated(UUID.randomUUID().toString(), "f2", "cluster 2", true)
    val fileCreated3 = FileCreated(UUID.randomUUID().toString(), "f3", "cluster 3", true)

    val clusterLocked1 = ClusterLocked.example()
    val clusterLocked2 = ClusterLocked(fileCreated2.aggregateId, fileCreated2.clusterId, true)
    val clusterLocked3 = ClusterLocked(fileCreated3.aggregateId, fileCreated3.clusterId, true)

    dbEventRepository.insert(listOf(fileCreated1, fileCreated2, fileCreated3))
    dbEventRepository.pauseHardPersistenceAndAppendToVolatilePersistence()
    dbEventRepository.insert(listOf(clusterLocked1, clusterLocked2, clusterLocked3))

    // when
    val domainFiles = dbEventRepository.load(listOf(fileCreated1.aggregateId, fileCreated3.aggregateId))

    // then
    then(domainFiles).containsExactly(
      DomainFileHistory.from(listOf(fileCreated1, clusterLocked1)),
      DomainFileHistory.from(listOf(fileCreated3, clusterLocked3))
    )
  }
}
