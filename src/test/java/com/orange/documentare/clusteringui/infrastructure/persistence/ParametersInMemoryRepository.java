package com.orange.documentare.clusteringui.infrastructure.persistence;

import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters;
import com.orange.documentare.clusteringui.biz.parameters.persistence.ParametersRepository;
import com.tophe.ddd.infrastructure.persistence.InMemoryRepository;

public class ParametersInMemoryRepository extends InMemoryRepository<Parameters, Long> implements ParametersRepository {
}
