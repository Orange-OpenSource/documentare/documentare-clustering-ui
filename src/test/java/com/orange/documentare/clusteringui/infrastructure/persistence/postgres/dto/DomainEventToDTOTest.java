package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;


import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.DomainEventToDTO.mapDomainEventToDTO;
import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;
import static org.assertj.core.api.BDDAssertions.then;

import org.junit.jupiter.api.Test;

import com.orange.documentare.clusteringui.biz.file.event.*;

class DomainEventToDTOTest {

  @Test
  void checkMapToDTO() {
    then(addId(mapDomainEventToDTO(ClusterLocked.example()))).isEqualTo(ClusterLockedDTO.example());
    then(addId(mapDomainEventToDTO(ClusterModified.example()))).isEqualTo(ClusterModifiedDTO.example());
    then(addId(mapDomainEventToDTO(ClusterNotModified.example()))).isEqualTo(ClusterNotModifiedDTO.example());
    then(addId(mapDomainEventToDTO(ClusterPutInNamedClass.example()))).isEqualTo(ClusterPutInNamedClassDTO.example());
    then(addId(mapDomainEventToDTO(ClusterUnlocked.example()))).isEqualTo(ClusterUnlockedDTO.example());
    then(addId(mapDomainEventToDTO(DistancesToClusterCenterAdded.example()))).isEqualTo(DistancesToClusterCenterAddedDTO.example());
    then(addId(mapDomainEventToDTO(FileAddedToCluster.example()))).isEqualTo(FileAddedToClusterDTO.example());
    then(addId(mapDomainEventToDTO(FileCreated.example()))).isEqualTo(FileCreatedDTO.example());
    then(addId(mapDomainEventToDTO(FileDeleted.example()))).isEqualTo(FileDeletedDTO.example());
    then(addId(mapDomainEventToDTO(FileDuplicated.example()))).isEqualTo(FileDuplicatedDTO.example());
    then(addId(mapDomainEventToDTO(FileInfoReset.example()))).isEqualTo(FileInfoResetDTO.example());
    then(addId(mapDomainEventToDTO(FileMovedFromWaitingRoomToSortRoom.example()))).isEqualTo(FileMovedFromWaitingRoomToSortRoomDTO.example());
    then(addId(mapDomainEventToDTO(FilePutInNamedClass.example()))).isEqualTo(FilePutInNamedClassDTO.example());
    then(addId(mapDomainEventToDTO(FileRemovedFromCluster.example()))).isEqualTo(FileRemovedFromClusterDTO.example());
    then(addId(mapDomainEventToDTO(MultisetElementMovedToNextHierarchyLevel.example()))).isEqualTo(MultisetMovedToNextHierarchyLevelDTO.example());
  }

  @Test
  void checkMapToDomain() {
    then(ClusterLocked.example()).isEqualTo(ClusterLockedDTO.example().toDomain());
    then(ClusterModified.example()).isEqualTo(ClusterModifiedDTO.example().toDomain());
    then(ClusterNotModified.example()).isEqualTo(ClusterNotModifiedDTO.example().toDomain());
    then(ClusterPutInNamedClass.example()).isEqualTo(ClusterPutInNamedClassDTO.example().toDomain());
    then(ClusterUnlocked.example()).isEqualTo(ClusterUnlockedDTO.example().toDomain());
    then(DistancesToClusterCenterAdded.example()).isEqualTo(DistancesToClusterCenterAddedDTO.example().toDomain());
    then(FileAddedToCluster.example()).isEqualTo(FileAddedToClusterDTO.example().toDomain());
    then(FileCreated.example()).isEqualTo(FileCreatedDTO.example().toDomain());
    then(FileDeleted.example()).isEqualTo(FileDeletedDTO.example().toDomain());
    then(FileDuplicated.example()).isEqualTo(FileDuplicatedDTO.example().toDomain());
    then(FileInfoReset.example()).isEqualTo(FileInfoResetDTO.example().toDomain());
    then(FileMovedFromWaitingRoomToSortRoom.example()).isEqualTo(FileMovedFromWaitingRoomToSortRoomDTO.example().toDomain());
    then(FilePutInNamedClass.example()).isEqualTo(FilePutInNamedClassDTO.example().toDomain());
    then(FileRemovedFromCluster.example()).isEqualTo(FileRemovedFromClusterDTO.example().toDomain());
    then(MultisetElementMovedToNextHierarchyLevel.example()).isEqualTo(MultisetMovedToNextHierarchyLevelDTO.example().toDomain());
  }

  private EventDTO addId(EventDTO dto) {
    dto.id = exampleId(AGGREGATE_ID_EXAMPLE);
    return dto;
  }
}
