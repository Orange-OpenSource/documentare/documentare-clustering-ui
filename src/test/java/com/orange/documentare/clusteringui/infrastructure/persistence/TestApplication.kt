package com.orange.documentare.clusteringui.infrastructure.persistence

import org.springframework.boot.autoconfigure.SpringBootApplication


@SpringBootApplication
class TestApplication {
  companion object {
    @JvmStatic
    fun main(args: Array<String>) {
    }
  }
}