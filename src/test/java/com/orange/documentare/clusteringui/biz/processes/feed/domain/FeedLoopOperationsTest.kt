package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.biz.file.command.MoveFileFromWaitingRoomToSortRoomCommandHandler
import com.orange.documentare.clusteringui.biz.file.command.RemoveSelectedFilesFromClustersCommandHandler
import com.orange.documentare.clusteringui.biz.file.command.ResetFileInfoCommandHandler
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.domain.FileId
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.event.FilePutInNamedClass
import com.orange.documentare.clusteringui.biz.file.query.*
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.persistence.ParametersRepository
import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQueryHandler
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederPhases.Phase.*
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederPhases.UpdateType
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.*
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.ClusteringService
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.orange.documentare.clusteringui.infrastructure.persistence.ParametersInMemoryRepository
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.queries.QueryBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class FeedLoopOperationsTest {

  private val queryBus = QueryBus()
  private val commandBus = CommandBus()

  private val takeFilesReverse: (List<DomainFile>, Int) -> List<String> =
    { filesInWaitingRoom: List<DomainFile>, triggerThreshold: Int -> filesInWaitingRoom.take(triggerThreshold).map { file -> file.aggregateId }.reversed() }
  private val eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  private val projections = Projections(eventRepo)
  private val eventBus = EventBus(eventRepo)
  private val parametersRepository: ParametersRepository = ParametersInMemoryRepository()
  private val decrementMultisetThresholdAndBreakClusters = DecrementMultisetThresholdAndBreakClusters(commandBus, queryBus, 10)
  private val paramsFake = ParametersProviderFake()
  private val events = mutableListOf<FeedLoopEvent>()
  private val listener = object : FeedLoopEventListener {
    override fun onFeedLoopEvent(event: FeedLoopEvent) {
      events.add(event)
    }
  }


  @BeforeEach
  fun setup() {
    eventBus.register(projections)
    commandBus.register(
      MoveFileFromWaitingRoomToSortRoomCommandHandler(eventRepo, eventBus),
      ResetFileInfoCommandHandler(eventRepo, eventBus),
      RemoveSelectedFilesFromClustersCommandHandler(eventRepo, eventBus)
    )
    queryBus.register(
      AllClustersQueryHandler(projections),
      AllFilesToSortQueryHandler(projections),
      AllSingletonsNotLockedQueryHandler(projections),
      AllFilesInWaitingRoomQueryHandler(projections),
      ParametersQueryHandler(parametersRepository),
      MultiHypothesisClustersQueryHandler(projections),
      AllFilesToRemoveInClusterQueryHandler(projections)
    )
    parametersRepository.insert(listOf(paramsFake.firstLoopParams))
  }


  @Test
  fun `WITHOUT files class name, for 8 files in waiting room and trigger threshold set to 5, feedLoop runs twice with 5 files then with 3 files, with parameters update`() {
    // given
    fillDataBaseWithEigthFilesInWaitingRoom()
    val fakeClusteringService = fakeClusteringService()
    val feedLoop =
      FeedLoop(queryBus, commandBus, paramsFake.parametersProvider, fakeClusteringService, takeFilesReverse, listener, decrementMultisetThresholdAndBreakClusters)

    // when
    feedLoop.loop()

    // then
    then(events).containsExactly(
      FeederPhaseUpdated(UpdateType.AT_FEED_LOOP_START, INCREMENTAL_CLUSTERING),
      FeedLoopStarted(feedLoopCount = 0),
      MinMultisetThresholdUpdated(paramsFake.firstLoopParams.minMultisetThreshold),
      FilesMovedFromWaitingRoomToSortRoom(listOf(aggregateIdExample(4), aggregateIdExample(3), aggregateIdExample(2), aggregateIdExample(1), aggregateIdExample(0))),
      SingletonsNotLockedMovedToSortRoom(emptyList()),
      fakeClusteringService.distancesComputedAndClustersRecalculated,
      fakeClusteringService.globalOrPartialClusteringComputed,

      FeedLoopStarted(feedLoopCount = 1),
      MinMultisetThresholdUpdated(paramsFake.secondLoopParams.minMultisetThreshold),
      FilesMovedFromWaitingRoomToSortRoom(listOf(aggregateIdExample(7), aggregateIdExample(6), aggregateIdExample(5))),
      SingletonsNotLockedMovedToSortRoom(emptyList()),
      fakeClusteringService.distancesComputedAndClustersRecalculated,
      fakeClusteringService.globalOrPartialClusteringComputed,
      FeederPhaseUpdated(UpdateType.BEFORE_LOOP_FINALISATION, MULTISET_BALANCING),
      MultisetClustersBroke(brokeClusters = 0, multiSetThreshold = -1, maxMultisetOnAllClustersNotLocked = 0),
      FeederPhaseUpdated(updateType = UpdateType.AT_FEED_LOOP_START, phase = METADATA_BALANCING, metadataBalancingStep = MetadataBalancingStep.BREAK_CLUSTERS),

      FeedLoopStarted(feedLoopCount = 2),
      MultiHypothesisClustersBroke(filesIdMovedToWaitingRoom = emptyList(), metadataBalancingFinished = true),
      MultisetThresholdReset(multisetThreshold = 9999),
      FeederPhaseUpdated(updateType = UpdateType.AFTER_FEED_LOOP_INITIALISATION, phase = IDLE, metadataBalancingStep = null),
    )

    fakeClusteringService.check(paramsFake.firstLoopParams, paramsFake.secondLoopParams)
  }


  @Test
  fun `WITH files class name, for 8 files in waiting room and trigger threshold set to 5, feedLoop runs twice with 5 files then with 3 files, with parameters update`() {
    // given
    fillWaitingRoomWithFilesAndAddAMultiHypothesisCluster()
    val fakeClusteringService = fakeClusteringService()
    val feedLoop =
      FeedLoop(queryBus, commandBus, paramsFake.parametersProvider, fakeClusteringService, takeFilesReverse, listener, decrementMultisetThresholdAndBreakClusters)

    // when
    feedLoop.loop()

    // then
    then(events).containsExactly(
      FeederPhaseUpdated(updateType = UpdateType.AT_FEED_LOOP_START, phase = INCREMENTAL_CLUSTERING, metadataBalancingStep = null),

      FeedLoopStarted(feedLoopCount = 0),
      MinMultisetThresholdUpdated(minMultisetThreshold = 3),
      FilesMovedFromWaitingRoomToSortRoom(filesId = listOf(aggregateIdExample(4), aggregateIdExample(3), aggregateIdExample(2), aggregateIdExample(1), aggregateIdExample(0))),
      SingletonsNotLockedMovedToSortRoom(filesId = emptyList()),
      DistancesComputedAndClustersRecalculated(computedDistancesCount = 3, recalculatedClustersCount = 2, brokeClustersCount = 1),
      GlobalOrPartialClusteringComputed(filesCount = 2),

      FeedLoopStarted(feedLoopCount = 1),
      MinMultisetThresholdUpdated(minMultisetThreshold = 1),
      FilesMovedFromWaitingRoomToSortRoom(filesId = listOf(aggregateIdExample(7), aggregateIdExample(6), aggregateIdExample(5))),
      SingletonsNotLockedMovedToSortRoom(filesId = emptyList()),
      DistancesComputedAndClustersRecalculated(computedDistancesCount = 3, recalculatedClustersCount = 2, brokeClustersCount = 1),
      GlobalOrPartialClusteringComputed(filesCount = 2),
      FeederPhaseUpdated(updateType = UpdateType.BEFORE_LOOP_FINALISATION, phase = MULTISET_BALANCING, metadataBalancingStep = null),
      MultisetClustersBroke(brokeClusters = 0, multiSetThreshold = -1, maxMultisetOnAllClustersNotLocked = 0),
      FeederPhaseUpdated(updateType = UpdateType.AT_FEED_LOOP_START, phase = METADATA_BALANCING, metadataBalancingStep = MetadataBalancingStep.BREAK_CLUSTERS),

      FeedLoopStarted(feedLoopCount = 2),
      MultiHypothesisClustersBroke(filesIdMovedToWaitingRoom = listOf(FileId(fileId = aggregateIdExample(11))), metadataBalancingFinished = false),
      MultisetThresholdReset(multisetThreshold = 9999),
      FeederPhaseUpdated(updateType = UpdateType.AFTER_FEED_LOOP_INITIALISATION, phase = METADATA_BALANCING, metadataBalancingStep = MetadataBalancingStep.INCREMENTAL_CLUSTERING),
      FilesMovedFromWaitingRoomToSortRoom(filesId = listOf(aggregateIdExample(11))),
      SingletonsNotLockedMovedToSortRoom(filesId = emptyList()),
      DistancesComputedAndClustersRecalculated(computedDistancesCount = 3, recalculatedClustersCount = 2, brokeClustersCount = 1),
      GlobalOrPartialClusteringComputed(filesCount = 2),
      FeederPhaseUpdated(updateType = UpdateType.BEFORE_LOOP_FINALISATION, phase = METADATA_BALANCING, metadataBalancingStep = MetadataBalancingStep.MULTISET_BALANCING),
      MultisetClustersBroke(brokeClusters = 0, multiSetThreshold = -1, maxMultisetOnAllClustersNotLocked = 0),
      FeederPhaseUpdated(updateType = UpdateType.AT_FEED_LOOP_START, phase = METADATA_BALANCING, metadataBalancingStep = MetadataBalancingStep.BREAK_CLUSTERS),

      FeedLoopStarted(feedLoopCount = 3),
      MultiHypothesisClustersBroke(filesIdMovedToWaitingRoom = emptyList(), metadataBalancingFinished = false),
      MultisetThresholdReset(multisetThreshold = 9999),
      FeederPhaseUpdated(updateType = UpdateType.AFTER_FEED_LOOP_INITIALISATION, phase = METADATA_BALANCING, metadataBalancingStep = MetadataBalancingStep.INCREMENTAL_CLUSTERING),
      FilesMovedFromWaitingRoomToSortRoom(filesId = emptyList()),
      SingletonsNotLockedMovedToSortRoom(filesId = emptyList()),
      DistancesComputedAndClustersRecalculated(computedDistancesCount = 3, recalculatedClustersCount = 2, brokeClustersCount = 1),
      GlobalOrPartialClusteringComputed(filesCount = 2),
      FeederPhaseUpdated(updateType = UpdateType.BEFORE_LOOP_FINALISATION, phase = METADATA_BALANCING, metadataBalancingStep = MetadataBalancingStep.MULTISET_BALANCING),
      MultisetClustersBroke(brokeClusters = 0, multiSetThreshold = -1, maxMultisetOnAllClustersNotLocked = 0),
      FeederPhaseUpdated(updateType = UpdateType.AT_FEED_LOOP_START, phase = METADATA_BALANCING, metadataBalancingStep = MetadataBalancingStep.BREAK_CLUSTERS),

      FeedLoopStarted(feedLoopCount = 4),
      MultiHypothesisClustersBroke(filesIdMovedToWaitingRoom = emptyList(), metadataBalancingFinished = true),
      MultisetThresholdReset(multisetThreshold = 9999),
      FeederPhaseUpdated(updateType = UpdateType.AFTER_FEED_LOOP_INITIALISATION, phase = IDLE, metadataBalancingStep = null)
    )

    fakeClusteringService.check(paramsFake.firstLoopParams, paramsFake.secondLoopParams, true)
  }

  class ParametersProviderFake {
    internal val firstLoopParams = Parameters.builder().feed(true).feedThreshold(5).minMultisetThreshold(3).build()
    internal val secondLoopParams = Parameters.builder().feed(true).feedThreshold(5).minMultisetThreshold(1).build()

    private var loopCount = 0
    val parametersProvider = {
      loopCount += 1
      if (loopCount == 1) firstLoopParams else secondLoopParams
    }
  }

  class ClusteringServiceFake : ClusteringService {
    internal val distancesComputedAndClustersRecalculated = DistancesComputedAndClustersRecalculated(3, 2, 1)
    internal val globalOrPartialClusteringComputed = GlobalOrPartialClusteringComputed(2)

    private val actualParametersGlobalOrPartialClusteringSync: MutableList<Parameters?> = mutableListOf()
    private val actualAllFilesToSort: MutableList<List<DomainFile>?> = mutableListOf()
    private val actualParametersComputeDistancesThenRecalculateClustersSync: MutableList<Parameters?> = mutableListOf()

    override fun globalOrPartialClusteringSync(parameters: Parameters, allFilesToSort: List<DomainFile>): GlobalOrPartialClusteringComputed {
      actualParametersGlobalOrPartialClusteringSync.add(parameters)
      actualAllFilesToSort.add(allFilesToSort)
      return globalOrPartialClusteringComputed
    }

    override fun computeDistancesThenRecalculateClustersSync(parameters: Parameters): DistancesComputedAndClustersRecalculated {
      actualParametersComputeDistancesThenRecalculateClustersSync.add(parameters)
      return distancesComputedAndClustersRecalculated
    }

    override fun isRunning(): Boolean {
      TODO("Not yet implemented")
    }

    fun check(parameters: Parameters, parameters2: Parameters, metadataBalancingLoop: Boolean = false) {
      val filesToSort = mutableListOf(
        listOf(aggregateIdExample(0), aggregateIdExample(1), aggregateIdExample(2), aggregateIdExample(3), aggregateIdExample(4)),
        listOf(
          aggregateIdExample(0),
          aggregateIdExample(1),
          aggregateIdExample(2),
          aggregateIdExample(3),
          aggregateIdExample(4),
          aggregateIdExample(5),
          aggregateIdExample(6),
          aggregateIdExample(7)
        )
      )
      val parametersList = mutableListOf(parameters, parameters2)
      if (metadataBalancingLoop) {
        filesToSort.add(
          listOf(
            aggregateIdExample(0),
            aggregateIdExample(1),
            aggregateIdExample(2),
            aggregateIdExample(3),
            aggregateIdExample(4),
            aggregateIdExample(5),
            aggregateIdExample(6),
            aggregateIdExample(7),
            aggregateIdExample(11)
          )
        )
        filesToSort.add(
          listOf(
            aggregateIdExample(0),
            aggregateIdExample(1),
            aggregateIdExample(2),
            aggregateIdExample(3),
            aggregateIdExample(4),
            aggregateIdExample(5),
            aggregateIdExample(6),
            aggregateIdExample(7),
            aggregateIdExample(11)
          )
        )
        parametersList.add(parameters2)
        parametersList.add(parameters2)
      }
      then(actualAllFilesToSort.map { domainFile -> domainFile?.map { it.aggregateId } }).isEqualTo(filesToSort)
      then(actualParametersGlobalOrPartialClusteringSync).isEqualTo(parametersList)
      then(actualParametersComputeDistancesThenRecalculateClustersSync).isEqualTo(parametersList)
    }
  }

  private fun fakeClusteringService() = ClusteringServiceFake()

  private fun filesInWaitingRoom() = projections.files.ids()
    .map { id -> projections.files[id] }
    .filter { it.isInWaitingRoom() }

  private fun fillWaitingRoomWithFilesAndAddAMultiHypothesisCluster() {
    fillDataBaseWithEigthFilesInWaitingRoom(arrayOf("files class name"))
  }

  private fun fillDataBaseWithEigthFilesInWaitingRoom(fileNamedClass: Array<String>? = null) {
    val events = mutableListOf<Event>()
    for (i in 0..7) {
      val file = DomainFile.builder()
        .aggregateId(aggregateIdExample(i))
        .filename("f$i")
        .inWaitingRoom(true)
        .fileNamedClass(fileNamedClass)
        .build()
      events.add(FileCreated(file.aggregateId, file.filename, true))
      fileNamedClass?.let {
        events.add(FilePutInNamedClass(file.aggregateId, file.fileNamedClass))
      }
    }
    eventRepo.insert(events)
    if (fileNamedClass != null) {
      MultiHypothesisClustersView.Examples.multiHypothesisClusterWithOnlyOneDistinctFileInit(eventBus)
    }
    projections.testPurposeInitFromHistory(events)
    then(filesInWaitingRoom()).isNotEmpty
  }
}
