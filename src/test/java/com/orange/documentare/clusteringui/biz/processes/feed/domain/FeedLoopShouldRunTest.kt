package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.nhaarman.mockitokotlin2.mock
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeedLoopEventListener
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.ClusteringServiceImpl
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.queries.QueryBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`

class FeedLoopShouldRunTest {

  private lateinit var decrementMultisetThresholdAndBreakClusters: DecrementMultisetThresholdAndBreakClusters
  private var notLockedClusters: NotLockedClusters = mock()

  private val feedLoop: FeedLoop = buildFeedLoop()

  @Test
  fun `should not run if we should stop, even if there are a lot of files in the waiting room`() {
    // given
    val manyFilesInWaitingRoom = manyFilesInWaitingRoom()
    feedLoop.onStopFeederWhenPossible()
    then(feedLoop.stopFeederWhenPossible).isTrue

    // when
    val shouldRun = feedLoop.shouldRunLoop(manyFilesInWaitingRoom, emptyList(), emptyList(), FeedLoopEventListener.noOpListener())

    // then
    then(shouldRun).isFalse
  }

  @Test
  fun `should not run if there are no file in the waiting room AND decremental multiset breaker has finished`() {
    // given
    val feedLoop: FeedLoop = buildFeedLoop(2)
    val noFileInWaitingRoom = emptyList<DomainFile>()

    ensureDecrementalMultisetBreakerHasFinished()

    // when
    val shouldRun = feedLoop.shouldRunLoop(noFileInWaitingRoom, emptyList(), emptyList(), FeedLoopEventListener.noOpListener())

    // then
    then(shouldRun).isFalse
  }

  private fun ensureDecrementalMultisetBreakerHasFinished() {
    `when`(notLockedClusters.countMaxMultiset()).thenReturn(1)
    decrementMultisetThresholdAndBreakClusters.checkIfWeNeedToBreakClusters()
  }

  @Test
  fun `should run if the waiting room is not empty, even if decremental multiset breaker is finished`() {
    // given
    val feedLoop: FeedLoop = buildFeedLoop(6)
    val manyFilesInWaitingRoom = manyFilesInWaitingRoom()

    // when
    val shouldRun = feedLoop.shouldRunLoop(manyFilesInWaitingRoom, emptyList(), emptyList(), FeedLoopEventListener.noOpListener())

    // then
    then(shouldRun).isTrue
  }


  @Test
  fun `should run if the waiting room is not empty`() {
    // given
    val manyFilesInWaitingRoom = manyFilesInWaitingRoom()

    // when
    val shouldRun = feedLoop.shouldRunLoop(manyFilesInWaitingRoom, emptyList(), emptyList(), FeedLoopEventListener.noOpListener())

    // then
    then(shouldRun).isTrue
  }

  @Test
  fun `should NOT run if there are no file in the waiting room AND decremental multiset breaker has finished AND singletons list contents did not changed, BUT singletons order changed`() {
    // given
    val feedLoop: FeedLoop = buildFeedLoop(2)
    val noFileInWaitingRoom = emptyList<DomainFile>()

    ensureDecrementalMultisetBreakerHasFinished()

    // when
    val shouldRun = feedLoop.shouldRunLoop(
      noFileInWaitingRoom,
      listOf("1", "3", "2"),
      listOf("3", "2", "1"),
      FeedLoopEventListener.noOpListener()
    )

    // then
    then(shouldRun).isFalse
  }

  @Test
  fun `should NOT run if there are no file in the waiting room AND decremental multiset breaker has finished AND singletons changed BUT we need to stop if possible`() {
    // given
    val feedLoop: FeedLoop = buildFeedLoop(2)
    val noFileInWaitingRoom = emptyList<DomainFile>()
    ensureDecrementalMultisetBreakerHasFinished()
    feedLoop.onStopFeederWhenPossible()

    // when
    val shouldRun = feedLoop.shouldRunLoop(noFileInWaitingRoom, listOf("last singleton id"), listOf("changed singleton id"), FeedLoopEventListener.noOpListener())

    // then
    then(shouldRun).isFalse
  }


  private fun manyFilesInWaitingRoom() = IntRange(0, 100).map { DomainFile.builder().build() }

  private fun buildFeedLoop(minMultisetThreshold: Int = 2): FeedLoop {
    val parameters = Parameters.builder()
      .minMultisetThreshold(minMultisetThreshold).build()

    val queryBus: QueryBus = mock()
    val commandBus: CommandBus = mock()
    val clusteringService: ClusteringServiceImpl = mock()
    val takeFilesRandomly: (List<DomainFile>, Int) -> List<String> = { _: List<DomainFile>, _: Int -> emptyList() }

    decrementMultisetThresholdAndBreakClusters =
      DecrementMultisetThresholdAndBreakClusters(commandBus, queryBus, parameters.minMultisetThreshold, notLockedClusters = notLockedClusters)

    return FeedLoop(queryBus, commandBus, { parameters }, clusteringService, takeFilesRandomly, FeedLoopEventListener.noOpListener(), decrementMultisetThresholdAndBreakClusters)
  }
}
