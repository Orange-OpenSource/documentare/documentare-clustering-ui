package com.orange.documentare.clusteringui.biz.file.query.hierarchy

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.*
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.*
import com.orange.documentare.clusteringui.biz.file.query.DisplayHierarchyLevelFileTransform.Companion.CLASS_NO_NAME
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class DisplayHierarchyLevelFilesQueryLevel1Test {

  private var projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  private var handler = DisplayHierarchyLevelFilesQueryHandler(projections)

  @Test
  fun `one file deleted not to display`() {
    // given
    val mapClasses = mapOf(CLASS_NO_NAME to HashMap<String, ClusterUIView>())
    val hierarchyLevel = 1
    val expectedView = DisplayHierarchyLevelFilesView(
      2,
      hierarchyLevel,
      noFiles = false,
      notDeleted = emptyList(),
      notInClusters = emptyList(),
      mapClassViews = mapClasses
    )


    val aggregateId = "0"
    val events = listOf(
      FileCreated(aggregateId, "titi"),
      FileAddedToCluster(aggregateId, "10", true, null, false, true, false, null),
      MultisetElementMovedToNextHierarchyLevel(aggregateId, hierarchyLevel),
      FileMovedFromWaitingRoomToSortRoom(aggregateId),
      FileDeleted(aggregateId, null)
    )
    projections.testPurposeInitFromHistory(events)

    // when
    val response = handler.execute(DisplayHierarchyLevelFilesQuery(hierarchyLevel))

    // then
    then(response.success()).isTrue
    then(response.value()).isEqualTo(expectedView)
  }

  @Test
  fun `one file not in cluster to display`() {
    // given
    val aggregateId = "0"
    val mapClasses = mapOf(CLASS_NO_NAME to HashMap<String, ClusterUIView>())
    val domainFile = DomainFile.builder().aggregateId(aggregateId).filename("titi").build()
    val hierarchyLevel = 1
    val expectedView = DisplayHierarchyLevelFilesView(
      2,
      hierarchyLevel,
      noFiles = false,
      notDeleted = listOf(FileUIView.from(domainFile)),
      notInClusters = listOf(FileUIView.from(domainFile)),
      mapClassViews = mapClasses
    )

    val events = listOf(
      FileCreated(aggregateId, "titi"),
      FileAddedToCluster(aggregateId, "10", true, null, false, true, false, null),
      MultisetElementMovedToNextHierarchyLevel(aggregateId, hierarchyLevel),
      FileMovedFromWaitingRoomToSortRoom(aggregateId)
    )
    projections.testPurposeInitFromHistory(events)

    // when
    val response = handler.execute(DisplayHierarchyLevelFilesQuery(hierarchyLevel))

    // then
    then(response.success()).isTrue
    then(response.value()).isEqualTo(expectedView)
  }

  @Test
  fun `one file in cluster to display`() {
    // given
    val domainFile = DomainFile.builder().aggregateId(aggregateIdExample(0)).filename("titi").clusterCenter(true).clusterId("10").inMultiset(true).build()
    val mapClasses = mapOf(CLASS_NO_NAME to mapOf("10" to ClusterUIView("10", listOf(FileUIView.from(domainFile)))))
    val hierarchyLevel = 1
    val expectedView = DisplayHierarchyLevelFilesView(
      2,
      hierarchyLevel,
      noFiles = false,
      notDeleted = listOf(FileUIView.from(domainFile)),
      notInClusters = emptyList(),
      mapClassViews = mapClasses
    )

    val aggregateId = aggregateIdExample(0)
    val events = listOf(
      FileCreated(aggregateId, "titi"),
      FileAddedToCluster(aggregateId, "1", true, null, false, true, false, null),
      MultisetElementMovedToNextHierarchyLevel(aggregateId, hierarchyLevel),
      FileMovedFromWaitingRoomToSortRoom(aggregateId),
      FileAddedToCluster(aggregateId, "10", true, null, false, true, false, null)
    )
    projections.testPurposeInitFromHistory(events)
    // when
    val response = handler.execute(DisplayHierarchyLevelFilesQuery(hierarchyLevel))

    // then
    then(response.success()).isTrue
    then(response.value()).isEqualTo(expectedView)
  }

  @Test
  fun `one file in cluster 10 in className TEST to display and no cluster in className zzzz NO_NAMED`() {
    // given
    val domainFile = DomainFile.builder().aggregateId(aggregateIdExample(0)).filename("titi").clusterCenter(true).clusterId("10").inMultiset(true).build()
    val mapClasses = mapOf("TEST" to mapOf("10" to ClusterUIView("10", listOf(FileUIView.from(domainFile)))), CLASS_NO_NAME to HashMap())
    val hierarchyLevel = 1
    val expectedView = DisplayHierarchyLevelFilesView(
      2,
      hierarchyLevel,
      noFiles = false,
      notDeleted = listOf(FileUIView.from(domainFile)),
      notInClusters = emptyList(),
      mapClassViews = mapClasses
    )

    val aggregateId = aggregateIdExample(0)
    val events = listOf(
      FileCreated(aggregateId, "titi"),
      FileAddedToCluster(aggregateId, "1", true, null, false, true, false, null),
      MultisetElementMovedToNextHierarchyLevel(aggregateId, hierarchyLevel),
      FileMovedFromWaitingRoomToSortRoom(aggregateId),
      FileAddedToCluster(aggregateId, "10", true, null, false, true, false, "TEST")
    )
    projections.testPurposeInitFromHistory(events)

    // when
    val response = handler.execute(DisplayHierarchyLevelFilesQuery(hierarchyLevel))

    // then
    then(response.success()).isTrue
    then(response.value()).isEqualTo(expectedView)
  }

  @Test
  fun `classes are built with clusters`() {
    // given
    val hierarchyLevel = 1
    val events = listOf(
      FileCreated(aggregateIdExample(0), "filename-0"),
      FileCreated(aggregateIdExample(1), "filename-1"),
      FileCreated(aggregateIdExample(2), "filename-2"),
      FileCreated(aggregateIdExample(3), "filename-3"),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("1001").className("a").inMultiset(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("1001").className("a").build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("1001").className("a").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("1001").className("a").automaticInclude(true).build(),

      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(0), hierarchyLevel),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(1), hierarchyLevel),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(2), hierarchyLevel),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(3), hierarchyLevel),

      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(0)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(1)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(2)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(3)),


      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("100").className("a").inMultiset(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("100").className("a").build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("100").className("a").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("100").className("a").automaticInclude(true).build(),

      FileCreated(aggregateIdExample(4), "filename-4"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(4)).clusterId("200").className("a").clusterCenter(true).build(),

      FileCreated(aggregateIdExample(5), "filename-5"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(5)).clusterId("300").className("b").clusterCenter(true).build(),

      FileCreated(aggregateIdExample(10), "filename-10"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(10)).clusterId("400").clusterCenter(true).build()
    )
    projections.testPurposeInitFromHistory(events)

    // when
    val response = handler.execute(DisplayHierarchyLevelFilesQuery(hierarchyLevel))

    // then
    then(response.success()).isTrue
    val view = response.value()
    then(view.mapClassViews?.keys).containsExactlyInAnyOrder("a", "b", CLASS_NO_NAME)
    then(view.mapClassViews?.get("a")?.keys).containsExactlyInAnyOrder("100", "200")
    then(view.mapClassViews?.get("b")?.keys).containsExactlyInAnyOrder("300")
    then(view.mapClassViews?.get(CLASS_NO_NAME)?.keys).containsExactlyInAnyOrder("400")
  }

}
