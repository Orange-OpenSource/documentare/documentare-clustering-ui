package com.orange.documentare.clusteringui.biz.file.query.projection

import com.orange.documentare.clusteringui.biz.file.event.*
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Query Clusters Projection")
class ClustersProjectionTest {

  @Test
  fun build_and_update_files_not_in_clusters_data_structure() {
    // given
    val projection = ClustersProjection()

    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0"),

      FileCreated(aggregateIdExample(1), "f1"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("10").build(),

      FileCreated(aggregateIdExample(2), "f2"),
      FileDuplicated(aggregateIdExample(2), null),

      FileCreated(aggregateIdExample(5), "f5", "11", null),

      FileCreated(aggregateIdExample(6), "f6", "10", null),
      FileRemovedFromCluster(aggregateIdExample(6), "10", null)
    )

    // when
    events.forEach { projection.onEvent(it) }

    // then
    then(projection.filesNotInClusters()).containsExactly(
      aggregateIdExample(0), aggregateIdExample(6)
    )
  }

  @Test
  fun build_and_update_files_in_clusters_data_structure() {
    // given
    val projection = ClustersProjection()

    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0"),

      FileCreated(aggregateIdExample(1), "f1"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("10").build(),

      FileCreated(aggregateIdExample(2), "f2"),
      FileDuplicated(aggregateIdExample(2), null),

      FileCreated(aggregateIdExample(5), "f5", "11", null),

      FileCreated(aggregateIdExample(6), "f6", "10", null),
      FileRemovedFromCluster(aggregateIdExample(6), "10", null)
    )

    // when
    events.forEach { projection.onEvent(it) }

    // then
    then(projection.clusters().keys).hasSize(2)
    then(projection.clusters()["10"]).containsExactly(aggregateIdExample(1))
    then(projection.clusters()["11"]).containsExactly(aggregateIdExample(5))
  }


  @Test
  fun cluster_center_is_first_element_of_the_cluster() {
    // given
    val projection = ClustersProjection()

    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("10").build(),
      FileCreated(aggregateIdExample(1), "f1"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("10").clusterCenter(true).build(),
      FileCreated(aggregateIdExample(2), "f2"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("10").build(),

      FileCreated(aggregateIdExample(3), "f3", "11", false),
      FileCreated(aggregateIdExample(4), "f4", "11", true),
      FileCreated(aggregateIdExample(5), "f5", "11", false)
    )

    // when
    events.forEach { projection.onEvent(it) }

    // then
    then(projection.clusters().keys).hasSize(2)
    then(projection.clusters()["10"]).containsExactly(
      aggregateIdExample(1), aggregateIdExample(0), aggregateIdExample(2)
    )
    then(projection.clusters()["11"]).containsExactly(
      aggregateIdExample(4), aggregateIdExample(3), aggregateIdExample(5)
    )
  }

  @Test
  fun empty_cluster_is_removed() {
    // given
    val projection = ClustersProjection()

    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0", "10", false),
      FileRemovedFromCluster(aggregateIdExample(0), "10", null)
    )

    // when
    events.forEach { projection.onEvent(it) }

    // then
    then(projection.clusters().keys).hasSize(0)
  }

  @Test
  fun deleted_file_is_no_more_referenced() {
    // given
    val projection = ClustersProjection()

    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0"),
      FileDeleted(aggregateIdExample(0), null),

      FileCreated(aggregateIdExample(1), "f1"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("10").build(),
      FileDeleted(aggregateIdExample(1), "10")
    )

    // when
    events.forEach { projection.onEvent(it) }

    // then
    then(projection.clusters().keys).hasSize(0)
    then(projection.filesNotInClusters()).hasSize(0)
  }

  @Test
  fun duplicated_file_is_no_more_referenced() {
    // given
    val projection = ClustersProjection()

    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0"),
      FileDuplicated(aggregateIdExample(0), null)
    )

    // when
    events.forEach { projection.onEvent(it) }

    // then
    then(projection.clusters().keys).hasSize(0)
    then(projection.filesNotInClusters()).hasSize(0)
  }

  @Test
  fun file_is_no_more_in_cluster_after_reset() {
    // given
    val projection = ClustersProjection()

    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0", "10", true),
      FileInfoReset(aggregateIdExample(0), "10")
    )

    // when
    events.forEach { projection.onEvent(it) }

    // then
    then(projection.filesNotInClusters()).containsExactly(aggregateIdExample(0))
  }

  @Test
  fun init_from_files_projection() {
    // given
    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0"),

      FileCreated(aggregateIdExample(1), "f1"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("10").build(),

      FileCreated(aggregateIdExample(2), "f2"),
      FileDuplicated(aggregateIdExample(2), null),

      FileCreated(aggregateIdExample(5), "f5", "11", null),

      FileCreated(aggregateIdExample(6), "f6", "10", null),
      FileRemovedFromCluster(aggregateIdExample(6), "10", null)
    )

    val filesProjection = FilesProjection()
    filesProjection.initFromHistory(events)

    val clustersProjection = ClustersProjection()

    // when
    clustersProjection.initFromHistory(events)

    // then
    then(clustersProjection.filesNotInClusters()).containsExactly(aggregateIdExample(0), aggregateIdExample(6))
    then(clustersProjection.clusters().keys).hasSize(2)
    then(clustersProjection.clusters()["10"]).containsExactly(aggregateIdExample(1))
    then(clustersProjection.clusters()["11"]).containsExactly(aggregateIdExample(5))
  }


  @Test
  fun `a cluster is added in a class`() {
    // given
    val projection = ClustersProjection()

    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0", "10", true),
      ClusterPutInNamedClass(aggregateIdExample(0), "10", "a")
    )

    // when
    events.forEach { projection.onEvent(it) }

    // then
    then(projection.classes()).isEqualTo(mutableMapOf(Pair("a", listOf("10"))))
  }

  @Test
  fun `a cluster is unlocked so remove it from its belonging class`() {
    // given
    val projection = ClustersProjection()

    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0", "10", true),
      ClusterPutInNamedClass(aggregateIdExample(0), "10", "a"),
      ClusterUnlocked("0", "10", true, "a"),

      FileCreated(aggregateIdExample(1), "f1", "11", true),
      ClusterPutInNamedClass("1", "11", "b"),
      ClusterUnlocked("1", "11", true, "b"),

      FileCreated(aggregateIdExample(2), "f2", "22", true),
      ClusterPutInNamedClass("2", "22", "2"),
    )

    // when
    events.forEach { projection.onEvent(it) }

    // then
    then(projection.classes().keys).containsExactly("2")
  }

  @Test
  internal fun `construct classes`() {
    // given
    val events = listOf(
      FileCreated(aggregateIdExample(0), "filename-0"),
      FileCreated(aggregateIdExample(1), "filename-1"),
      FileCreated(aggregateIdExample(2), "filename-2"),
      FileCreated(aggregateIdExample(3), "filename-3"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("100").className("a").inMultiset(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("100").className("a").build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("100").className("a").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("100").className("a").automaticInclude(true).build(),

      FileCreated(aggregateIdExample(4), "filename-4"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(4)).clusterId("200").className("a").clusterCenter(true).build(),

      FileCreated(aggregateIdExample(5), "filename-5"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(5)).clusterId("300").className("b").clusterCenter(true).build(),

      FileCreated(aggregateIdExample(10), "filename-10"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(10)).clusterId("400").clusterCenter(true).build()
    )
    val projections = Projections(DBEventInMemoryRepositoryForTestPurpose())

    // when
    projections.testPurposeInitFromHistory(events)

    // then
    val classes = projections.clusters.classes()
    then(classes.keys).hasSize(2)
    then(classes["a"]).containsExactlyInAnyOrder("100", "200")
    then(classes["b"]).containsExactlyInAnyOrder("300")
  }
}
