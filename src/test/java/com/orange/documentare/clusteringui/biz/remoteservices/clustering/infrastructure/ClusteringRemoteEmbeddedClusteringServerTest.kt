package com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure

import com.orange.documentare.clusteringui.Couple
import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.orange.documentare.core.model.ref.clustering.Clustering
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import java.util.*

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

internal class ClusteringRemoteEmbeddedClusteringServerTest {

  private lateinit var clusteringRemote: ClusteringRemote
  private lateinit var tmpDir: File
  private lateinit var rawCacheDir: File
  private lateinit var distancesPrepDir: File

  @BeforeEach
  fun setup() {
    // do not use @TmpDir since as our code delete files in it at the same time junit try to delete the dir, we got strange exceptions...
    tmpDir = File("/tmp/" + UUID.randomUUID())
    rawCacheDir = File("/tmp/" + UUID.randomUUID())
    distancesPrepDir = File("/tmp/" + UUID.randomUUID())
    tmpDir.mkdirs()
    rawCacheDir.mkdirs()
    distancesPrepDir.mkdirs()

    clusteringRemote = ClusteringRemote(EmbeddedComputationServer.with(distancesPrepDir.absolutePath, rawCacheDir.absolutePath))
  }

  @AfterEach
  fun cleanup() {
    // sometimes it may be seen as not empty just after the test
    tmpDir.deleteRecursively()
    rawCacheDir.deleteRecursively()
    distancesPrepDir.deleteRecursively()
  }

  @Test
  fun `calculating clustering with embedded computation server`() {
    // given
    val file1: DomainFile = createFile(tmpDir, "titi", "I am titi")
    val file2: DomainFile = createFile(tmpDir, "toto", "I am toto")
    val files = listOf(file1, file2)
    val parameters = Parameters.defaults()

    // when
    val result: TryOrCatch<Clustering> = clusteringRemote.calculateClustering(parameters, files)

    // then
    then(result.isSuccess).isTrue
    val clustering = result.get()
    then(clustering.elements.map { it.id }).containsExactly("titi", "toto")
    then(clustering.elements[0].clusterCenter).isTrue
    then(clustering.elements[1].distanceToCenter).isEqualTo(312500)
    then(clustering.clusters).hasSize(1)
    then(clustering.clusters[0].elementsIndices).containsExactly(0, 1)
  }

  @Test
  fun `recalculate many clusters and handle not available error correctly`() {
    // given
    createFile(tmpDir, "titi", "I am titi")
    createFile(tmpDir, "toto", "I am toto")

    val enoughFilesToSaturateComputationServer = Runtime.getRuntime().availableProcessors() + 2
    val clustersFiles: List<Couple<PreviousClusterId, List<DomainFile>>> =
      (1..enoughFilesToSaturateComputationServer)
        .map { Couple(PreviousClusterId("$it"), listOf(createFile(tmpDir, "$it", "$it"))) }

    // when
    val result: Map<PreviousClusterId, TryOrCatch<Clustering>> = clusteringRemote.recalculateClusters(Parameters.defaults(), clustersFiles)

    // then
    then(result.size).isEqualTo(enoughFilesToSaturateComputationServer)
    result.forEach { (_, res) -> then(res.isSuccess).isTrue }
    result.forEach { (previousClusterId, res) -> then(res.get().elements.map { it.id }).containsExactly(previousClusterId.id) }
  }

  private fun createFile(tmpDir: File, filename: String, content: String): DomainFile {
    FileIO.initFilesDirectory(tmpDir.absolutePath)
    val filepath = tmpDir.absoluteFile.absolutePath + File.separator + filename
    File(filepath).writeText(content)
    return DomainFile.builder().aggregateId(filename).filename(filepath).build()
  }
}
