package com.orange.documentare.clusteringui.biz.processes.hierarchy.domain

import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederProcessFinished
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederService
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.infrastructure.event.PersistencePauseAndResume
import com.tophe.ddd.queries.QueryBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

internal class HierarchyLoopTest {

  private var persistencePaused = 0
  private var persistenceResumed = true
  private val persistencePauseAndResume = object : PersistencePauseAndResume {
    override fun pauseHardPersistenceAndAppendToVolatilePersistence() {
      persistencePaused++
      persistenceResumed = false
    }

    override fun stopHardAndVolatilePersistence() {
      persistenceResumed = true
    }

    override fun resumeHardPersistenceAndDropVolatilePersistence() {
      throw IllegalStateException("not needed here")
    }
  }

  @Test
  fun `no clusters, then do nothing and finish with persistence resumed`() {
    // given
    var actualFeederServiceCalled = false
    val feederService = object : FeederService {
      override fun launch(feederProcessFinished: FeederProcessFinished?) {
        actualFeederServiceCalled = true
      }
    }

    val getClusterCentersAndMultiset = { Pair(emptyList<String>(), 0) }
    var actualMoveMultisetToNewHierarchyCalled = false
    val moveMultisetToNextHierarchy = { _: List<String>, _: Int -> actualMoveMultisetToNewHierarchyCalled = true }
    val hierarchyLoop = HierarchyLoop(CommandBus(), QueryBus(), feederService, persistencePauseAndResume, getClusterCentersAndMultiset, moveMultisetToNextHierarchy)

    // when
    hierarchyLoop.run()

    // then
    then(actualFeederServiceCalled).isFalse
    then(actualMoveMultisetToNewHierarchyCalled).isFalse
    then(persistencePaused).isEqualTo(1)
    then(persistenceResumed).isTrue()
  }

  @Test
  fun `move clusters to new hierarchy then launch feeder`() {
    // given
    val clusterCentersIds = listOf("id0", "id1")

    var actualFeederServiceCalled = false
    val feederService = object : FeederService {
      override fun launch(feederProcessFinished: FeederProcessFinished?) {
        actualFeederServiceCalled = true
        feederProcessFinished?.onFeederProcessFinished()
      }
    }

    val getClusterCentersAndMultiset = { Pair(clusterCentersIds, 0) }
    var actualMoveMultisetToNewHierarchy = Pair(emptyList<String>(), -1)
    val moveMultisetToNextHierarchy = { clusterCenterIds: List<String>, nextHierarchyLevel: Int -> actualMoveMultisetToNewHierarchy = Pair(clusterCenterIds, nextHierarchyLevel) }
    val hierarchyLoop = HierarchyLoop(CommandBus(), QueryBus(), feederService, persistencePauseAndResume, getClusterCentersAndMultiset, moveMultisetToNextHierarchy)

    // when
    hierarchyLoop.run()

    // then
    then(actualMoveMultisetToNewHierarchy).isEqualTo(Pair(clusterCentersIds, 1))
    then(actualFeederServiceCalled).isTrue
    then(persistencePaused).isEqualTo(1)
    then(persistenceResumed).isTrue()
  }

  @Test
  fun `on feeder finished, move clusters to new hierarchy then launch feeder`() {
    // given
    val clusterCentersIds = listOf("id0", "id1")

    var actualFeederServiceCalled = false
    val feederService = object : FeederService {
      override fun launch(feederProcessFinished: FeederProcessFinished?) {
        actualFeederServiceCalled = true
      }
    }

    val getClusterCentersAndMultiset = { Pair(clusterCentersIds, 0) }
    var actualMoveMultisetToNewHierarchy = Pair(emptyList<String>(), -1)
    val moveMultisetToNextHierarchy = { clusterCenterIds: List<String>, nextHierarchyLevel: Int -> actualMoveMultisetToNewHierarchy = Pair(clusterCenterIds, nextHierarchyLevel) }
    val hierarchyLoop = HierarchyLoop(CommandBus(), QueryBus(), feederService, persistencePauseAndResume, getClusterCentersAndMultiset, moveMultisetToNextHierarchy)

    // when
    hierarchyLoop.onFeederProcessFinished()

    // then
    then(actualMoveMultisetToNewHierarchy).isEqualTo(Pair(clusterCentersIds, 1))
    then(actualFeederServiceCalled).isTrue
    then(persistenceResumed).isTrue()
  }
}
