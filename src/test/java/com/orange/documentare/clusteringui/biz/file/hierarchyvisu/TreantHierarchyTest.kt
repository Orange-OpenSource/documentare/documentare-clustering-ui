package com.orange.documentare.clusteringui.biz.file.hierarchyvisu

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.HierarchiesView
import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

class TreantHierarchyTest {

  @TempDir
  lateinit var tmpDir: File

  @Test
  fun `convert hierarchy view with a single root to treant model`() {
    // given
    val hierarchiesView = HierarchiesView(HierarchyExamples.hierarchiesWith4LevelsOneRoot())

    // when
    val treantDTO: TreantHierarchyDTO = TreantHierarchyDTO.from(hierarchiesView)

    // then
    then(TreantExample.serialize(treantDTO)).isEqualTo(HierarchyExamples.treantModelWith4LevelsOneRoot())
  }

  @Test
  fun `convert hierarchy view with multiple roots to treant model`() {
    // given
    FileIO.initFilesDirectory(tmpDir.absolutePath)
    HierarchyExamples.hierarchiesWith2LevelsTwoRootsFilesIds().map {
      File(FileIO.thumbnailPathOf(DomainFile.builder().aggregateId(it).build())).createNewFile()
    }

    val hierarchiesView = HierarchiesView(HierarchyExamples.hierarchiesWith2LevelsTwoRoots())

    // when
    val treantDTO: TreantHierarchyDTO = TreantHierarchyDTO.from(hierarchiesView)

    // then
    then(TreantExample.serialize(treantDTO)).isEqualTo(HierarchyExamples.treantModelWith2LevelsTwoRoots())
  }
}
