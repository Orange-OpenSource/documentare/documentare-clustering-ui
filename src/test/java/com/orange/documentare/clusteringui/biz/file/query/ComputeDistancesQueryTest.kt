package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster
import com.orange.documentare.clusteringui.biz.file.event.*
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.Event
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class ComputeDistancesQueryTest {
  var projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  var handler = ComputeDistancesQueryHandler(projections)

  @Test
  fun retrieve_files_and_multisets() { // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "0"),
      FileCreated(aggregateIdExample(1), "1"),
      FileCreated(aggregateIdExample(2), "2"),
      DistancesToClusterCenterAdded(aggregateIdExample(2), listOf(DistanceToCluster(100, "10"))),

      // cluster center
      FileCreated(aggregateIdExample(3), "3", "44", false),
      FileCreated(aggregateIdExample(4), "4", "44", true),

      // cluster center
      FileCreated(aggregateIdExample(5), "5", "55", true),

      // in multiset
      FileCreated(aggregateIdExample(6), "6"),
      FileAddedToCluster(aggregateIdExample(6), "55", false, 100, false, true, false, null),

      // duplicated
      FileCreated(aggregateIdExample(7), "7", "77", true),
      FileDuplicated(aggregateIdExample(7), "77"),

      // deleted
      FileCreated(aggregateIdExample(8), "8", "88", true),
      FileDeleted(aggregateIdExample(8), "88")
    )
    projections.testPurposeInitFromHistory(events)
    val notInClusterAndWithoutDistances = projections.files[aggregateIdExample(0)]

    // when
    val response = handler.execute(ComputeDistancesQuery(listOf(notInClusterAndWithoutDistances.aggregateId)))

    // then
    then(response.success()).isTrue
    val distancesQueryView = response.value()
    then(distancesQueryView.files).containsExactly(notInClusterAndWithoutDistances)
    then(distancesQueryView.mapClusterMultiset).isEqualTo(
      mutableMapOf(
        Pair("44", listOf(projections.files[aggregateIdExample(4)])),
        Pair("55", listOf(projections.files[aggregateIdExample(5)], projections.files[aggregateIdExample(6)]))
      )
    )
  }
}
