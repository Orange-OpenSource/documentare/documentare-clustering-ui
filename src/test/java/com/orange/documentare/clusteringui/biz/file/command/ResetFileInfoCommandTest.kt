package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.*
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.EventBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class ResetFileInfoCommandTest {

  val eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  val eventBus = EventBus(eventRepo)
  val handler = ResetFileInfoCommandHandler(eventRepo, eventBus)

  @Test
  fun file_info_are_reset_for_provided_file_id() {
    // given
    val fileId1 = aggregateIdExample(0)
    val fileId2 = aggregateIdExample(1)
    val initialEvents = mutableListOf(
      FileCreated(fileId1, "titi"),
      FileAddedToCluster.builder().aggregateId(fileId1).clusterId("10").clusterCenter(true).distanceToCenter(100).automaticInclude(true).inMultiset(true).build(),
      ClusterModified(fileId1, "10"),
      DistancesToClusterCenterAdded(fileId1, emptyList()),

      FileCreated(fileId2, "toto"),
      FileAddedToCluster.builder().aggregateId(fileId2).clusterId("20").clusterCenter(true).distanceToCenter(200).automaticInclude(true).inMultiset(true).build(),
      ClusterModified(fileId2, "20"),
      DistancesToClusterCenterAdded(fileId2, emptyList())
    )
    eventRepo.insert(initialEvents)
    val cmd = ResetFileInfoCommand(listOf(fileId1, fileId2))

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isTrue()
    initialEvents.apply {
      this.add(FileInfoReset(fileId1, "10"))
      this.add(FileInfoReset(fileId2, "20"))
      then(eventRepo.findAll()).containsAll(this)
    }
  }

  @Test
  fun file_info_is_not_reset_for_NOT_provided_file_id() {
    // given
    val fileToReset = DomainFile.builder().aggregateId(aggregateIdExample(0)).filename("0").build()
    val fileNotToReset = DomainFile.builder().aggregateId(aggregateIdExample(1)).filename("1")
      .clusterId("10")
      .build()
    val initialEvents = mutableListOf(
      FileCreated(fileToReset.aggregateId, fileToReset.filename),
      FileCreated(fileNotToReset.aggregateId, fileNotToReset.filename),
      FileAddedToCluster.builder().aggregateId(fileNotToReset.aggregateId).clusterId("10").build()
    )
    eventRepo.insert(initialEvents)
    val cmd = ResetFileInfoCommand(listOf(fileToReset.aggregateId))

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isTrue()
    initialEvents.apply {
      this.add(FileInfoReset(fileToReset.aggregateId, null))
      then(eventRepo.findAll()).containsAll(this)
    }
  }
}
