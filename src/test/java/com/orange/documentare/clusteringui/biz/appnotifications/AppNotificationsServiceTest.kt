package com.orange.documentare.clusteringui.biz.appnotifications

import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class AppNotificationsServiceTest {
  @Test
  fun `a notification is dispatched to all notifications listeners`() {
    // given
    var actualReceivedNotification1: AppNotification? = null
    var actualReceivedNotification2: AppNotification? = null
    val appNotificationsListener1: AppNotificationListener = { actualReceivedNotification1 = it }
    val appNotificationsListener2: AppNotificationListener = { actualReceivedNotification2 = it }
    val appNotificationsService: AppNotifications = AppNotificationsService()

    // when
    appNotificationsService.register(appNotificationsListener1)
    appNotificationsService.register(appNotificationsListener2)
    appNotificationsService.notify(AppNotification.COMPUTE_DISTANCES_TO_CLUSTERS_DONE)

    // then
    then(actualReceivedNotification1).isEqualTo(AppNotification.COMPUTE_DISTANCES_TO_CLUSTERS_DONE)
    then(actualReceivedNotification2).isEqualTo(AppNotification.COMPUTE_DISTANCES_TO_CLUSTERS_DONE)
  }

  @Test
  fun `can unregister a listener`() {
    // given
    var actualReceivedNotification1: AppNotification? = null
    var actualReceivedNotification2: AppNotification? = null
    val appNotificationsListener1: AppNotificationListener = { actualReceivedNotification1 = it }
    val appNotificationsListener2: AppNotificationListener = { actualReceivedNotification2 = it }
    val appNotificationsService: AppNotifications = AppNotificationsService()
    appNotificationsService.register(appNotificationsListener1)
    appNotificationsService.register(appNotificationsListener2)

    // when
    appNotificationsService.unregister(appNotificationsListener2)
    appNotificationsService.notify(AppNotification.COMPUTE_DISTANCES_TO_CLUSTERS_DONE)

    // then
    then(actualReceivedNotification1).isEqualTo(AppNotification.COMPUTE_DISTANCES_TO_CLUSTERS_DONE)
    then(actualReceivedNotification2).isEqualTo(null)
  }
}
