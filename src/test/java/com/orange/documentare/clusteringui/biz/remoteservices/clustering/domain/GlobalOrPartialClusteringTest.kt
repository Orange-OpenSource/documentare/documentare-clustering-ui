package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.appnotifications.AppNotification
import com.orange.documentare.clusteringui.biz.appnotifications.AppNotificationsService
import com.orange.documentare.clusteringui.biz.file.command.AddToClusterCommandHandler
import com.orange.documentare.clusteringui.biz.file.command.BinPurgeCommandHandler
import com.orange.documentare.clusteringui.biz.file.command.DuplicateFilesCommandHandler
import com.orange.documentare.clusteringui.biz.file.command.ResetFileInfoCommandHandler
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.domain.DomainFileBuilder
import com.orange.documentare.clusteringui.biz.file.event.*
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.AllFilesQueryHandler
import com.orange.documentare.clusteringui.biz.file.query.SelectedFilesQueryHandler
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.SaveClustering.Companion.testUUIDSupplier
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure.ClusteringRemoteApi
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.orange.documentare.core.model.ref.clustering.Cluster
import com.orange.documentare.core.model.ref.clustering.Clustering
import com.orange.documentare.core.model.ref.clustering.ClusteringElement
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.event.TestEventHandler
import com.tophe.ddd.queries.QueryBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import java.util.*

@DisplayName("Global clustering")
class GlobalOrPartialClusteringTest {

  private val testEventHandler = TestEventHandler()
  private val eventBus = EventBus(DBEventInMemoryRepositoryForTestPurpose())

  private val appNotifications = AppNotificationsService()
  private val clusteringService = ClusteringServiceImpl(
    DistanceSupplier().get(), appNotifications,
    EmbeddedComputationServer.disabled()
  )
  private val eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  private val projections = Projections(eventRepo)

  private val clusteringRemoteApi = mock<ClusteringRemoteApi>()

  @BeforeEach
  fun setUp() {
    val commandBus = CommandBus()
    commandBus.register(
      BinPurgeCommandHandler(eventRepo, eventBus),
      AddToClusterCommandHandler(eventRepo, eventBus),
      DuplicateFilesCommandHandler(eventRepo, eventBus),
      ResetFileInfoCommandHandler(eventRepo, eventBus)
    )
    val queryBus = QueryBus()
    queryBus.register(AllFilesQueryHandler(projections), SelectedFilesQueryHandler(projections))

    clusteringService.setCommandBus(commandBus)
    clusteringService.setQueryBus(queryBus)
    clusteringService.testClusteringRemote(clusteringRemoteApi)

    eventBus.register(testEventHandler)

    val uuidSupplier = UUIDSupplier()
    testUUIDSupplier { uuidSupplier.getAsString() }
  }

  @Test
  fun `raise an exception is we try to run a clustering while it is already running`() {
    // given
    clusteringService.testAlreadyInProgress()

    // when
    val files = projections.files.ids()
      .map { fileId -> projections.files.get(fileId) }
    val tryClustering = TryOrCatch.run { clusteringService.createNewClusterAsync(Parameters.defaults(), files, emptyList()) }

    // then
    then(clusteringService.isRunning()).isTrue()
    then(tryClustering.isFailure).isTrue()
  }

  @Test
  fun `thrash Bin should be purged before doing a global or partial clustering`() {
    // given
    val initialEvents = listOf(
      FileCreated(aggregateIdExample(0), "titi"),
      FileDuplicated(aggregateIdExample(0), null),
      FileCreated(aggregateIdExample(1), "1")
    )
    eventRepo.insert(initialEvents)
    projections.testPurposeInitFromHistory(initialEvents)
    val filesIdsInBin = listOf(aggregateIdExample(0))

    // when
    startThenWaitForClustering(filesIdsInBin)

    // then
    then(testEventHandler.events()).containsExactly(
      FileDeleted(aggregateIdExample(0), null)
    )
  }

  @Test
  fun `log an error and do not crash if global or partial clustering fails`() {
    // given
    `when`(clusteringRemoteApi.calculateClustering(any(), any())).thenThrow(RuntimeException("remote clustering failed"))

    // when
    startThenWaitForClustering()
    verifyRecalculateOneClusterIsNotCalled()
  }

  @Test
  fun `Global or partial clustering result is saved, and we notify it is done`() {
    // given
    initRepositoriesWith5Elements()

    setCalculateAllClustersReturnedValue(
      clusteringOfFiveElementsWithFirst3InSameClusterAndTheRestInSecondCluster(false), Optional.empty()
    )

    var actualAppNotification: AppNotification? = null
    appNotifications.register {
      actualAppNotification = it
    }

    // when
    startThenWaitForClustering()

    // then
    val uuidSupplier = UUIDSupplier()
    val clusterId0 = uuidSupplier.getAsString()
    val clusterId1 = uuidSupplier.getAsString()

    then(testEventHandler.events()).containsExactly(
      FileInfoReset(aggregateIdExample(0), null),
      FileInfoReset(aggregateIdExample(1), null),
      FileInfoReset(aggregateIdExample(2), null),
      FileInfoReset(aggregateIdExample(3), null),
      FileInfoReset(aggregateIdExample(4), null),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId(clusterId0).clusterCenter(true).distanceToCenter(0).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId(clusterId0).distanceToCenter(10).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId(clusterId0).distanceToCenter(10).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId(clusterId1).clusterCenter(true).distanceToCenter(0).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(4)).clusterId(clusterId1).distanceToCenter(10).build()
    )

    then(actualAppNotification).isEqualTo(AppNotification.GLOBAL_OR_PARTIAL_CLUSTERING_DONE)

    verifyRecalculateOneClusterIsNotCalled()
  }

  @Test
  fun `SYNC Global or partial clustering is done and result is saved`() {
    // given
    initRepositoriesWith5Elements()

    setCalculateAllClustersReturnedValue(
      clusteringOfFiveElementsWithFirst3InSameClusterAndTheRestInSecondCluster(false), Optional.empty()
    )

    // when
    val files = projections.files.ids()
      .map { fileId -> projections.files.get(fileId) }
    clusteringService.globalOrPartialClusteringSync(Parameters.defaults(), files)

    // then

    val uuidSupplier = UUIDSupplier()
    val clusterId0 = uuidSupplier.getAsString()
    val clusterId1 = uuidSupplier.getAsString()

    then(testEventHandler.events()).containsExactly(
      FileInfoReset(aggregateIdExample(0), null),
      FileInfoReset(aggregateIdExample(1), null),
      FileInfoReset(aggregateIdExample(2), null),
      FileInfoReset(aggregateIdExample(3), null),
      FileInfoReset(aggregateIdExample(4), null),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId(clusterId0).clusterCenter(true).distanceToCenter(0).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId(clusterId0).distanceToCenter(10).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId(clusterId0).distanceToCenter(10).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId(clusterId1).clusterCenter(true).distanceToCenter(0).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(4)).clusterId(clusterId1).distanceToCenter(10).build()
    )

    verifyRecalculateOneClusterIsNotCalled()
  }

  @Test
  fun `duplicate file is saved as a duplicate`() {
    // given
    initRepositoriesWith5Elements()

    setCalculateAllClustersReturnedValue(
      clusteringOfFiveElementsWithFirst3InSameClusterAndTheRestInSecondCluster(true), Optional.empty()
    )

    // when
    startThenWaitForClustering()

    // then
    val uuidSupplier = UUIDSupplier()
    val clusterId0 = uuidSupplier.getAsString()
    val clusterId1 = uuidSupplier.getAsString()

    then(testEventHandler.events()).containsExactly(
      FileInfoReset(aggregateIdExample(0), null),
      FileInfoReset(aggregateIdExample(1), null),
      FileInfoReset(aggregateIdExample(2), null),
      FileInfoReset(aggregateIdExample(3), null),
      FileInfoReset(aggregateIdExample(4), null),
      FileDuplicated(aggregateIdExample(4), null),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId(clusterId0).clusterCenter(true).distanceToCenter(0).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId(clusterId0).distanceToCenter(10).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId(clusterId0).distanceToCenter(10).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId(clusterId1).clusterCenter(true).distanceToCenter(0).build()
    )

    verifyRecalculateOneClusterIsNotCalled()
  }

  @Test
  fun `clustering result is saved, but deleted file in a cluster breaks the whole cluster`() {
    // given
    initRepositoriesWith5Elements()

    setCalculateAllClustersReturnedValue(
      clusteringOfFiveElementsWithFirst3InSameClusterAndTheRestInSecondCluster(false), Optional.of(aggregateIdExample(4))
    )

    // when
    startThenWaitForClustering()

    // then
    val uuidSupplier = UUIDSupplier()
    val clusterId0 = uuidSupplier.getAsString()

    // "3" & "4" were in same cluster which is broken due to the deleted file
    then(testEventHandler.events()).containsExactly(
      FileInfoReset(aggregateIdExample(0), null),
      FileInfoReset(aggregateIdExample(1), null),
      FileInfoReset(aggregateIdExample(2), null),
      FileInfoReset(aggregateIdExample(3), null),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId(clusterId0).clusterCenter(true).distanceToCenter(0).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId(clusterId0).distanceToCenter(10).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId(clusterId0).distanceToCenter(10).build()
    )

    verifyRecalculateOneClusterIsNotCalled()
  }

  @Test
  fun `deleted file is not modified even if it's a duplicate in clustering result`() {
    // given
    initRepositoriesWith5Elements()

    setCalculateAllClustersReturnedValue(
      clusteringOfFiveElementsWithFirst3InSameClusterAndTheRestInSecondCluster(true), Optional.of(aggregateIdExample(4))
    )

    // when
    startThenWaitForClustering()

    // then
    val uuidSupplier = UUIDSupplier()
    val clusterId0 = uuidSupplier.getAsString()

    // "3" & "4" were in same cluster which is broken due to the deleted file
    then(testEventHandler.events()).containsExactly(
      FileInfoReset(aggregateIdExample(0), null),
      FileInfoReset(aggregateIdExample(1), null),
      FileInfoReset(aggregateIdExample(2), null),
      FileInfoReset(aggregateIdExample(3), null),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId(clusterId0).clusterCenter(true).distanceToCenter(0).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId(clusterId0).distanceToCenter(10).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId(clusterId0).distanceToCenter(10).build()
    )

    verifyRecalculateOneClusterIsNotCalled()
  }

  private fun markFileAsDeleted(id: String) {
    CreateNewClusterTest.markFileAsDeleted(id, eventRepo, projections)
  }


  private fun initRepositoriesWith5Elements() {
    val f1 = b().aggregateId(aggregateIdExample(0)).filename("naf").build()
    val f2 = b().aggregateId(aggregateIdExample(1)).filename("nif").build()
    val f3 = b().aggregateId(aggregateIdExample(2)).filename("nouf").build()
    val f4 = b().aggregateId(aggregateIdExample(3)).filename("manouf").build()
    val f5 = b().aggregateId(aggregateIdExample(4)).filename("elouf").build()
    val events = listOf<Event>(
      FileCreated(f1.aggregateId, "naf"),
      FileCreated(f2.aggregateId, "nif"),
      FileCreated(f3.aggregateId, "nouf"),
      FileCreated(f4.aggregateId, "manouf"),
      FileCreated(f5.aggregateId, "elouf")
    )
    eventRepo.insert(events)
    projections.testPurposeInitFromHistory(events)
  }

  private fun clusteringOfFiveElementsWithFirst3InSameClusterAndTheRestInSecondCluster(lastIsADuplicate: Boolean): Clustering {
    val uuidSupplier = UUIDSupplier()
    val clusterId0 = uuidSupplier.get()
    val clusterId1 = uuidSupplier.get()
    val elements = listOf(
      ClusteringElement(aggregateIdExample(0), clusterId0, true, false, null, 0),
      ClusteringElement(aggregateIdExample(1), clusterId0, false, true, null, 10),
      ClusteringElement(aggregateIdExample(2), clusterId0, false, false, null, 10),
      ClusteringElement(aggregateIdExample(3), clusterId1, true, false, null, 0),
      ClusteringElement(aggregateIdExample(4), clusterId1, false, false, if (lastIsADuplicate) 3 else null, 10)
    )
    val clusterWithoutMultiset = listOf(
      Cluster(listOf(0, 1, 2)),
      Cluster(listOf(3, 4))
    )
    return Clustering(elements, null, clusterWithoutMultiset, null, null, null)
  }

  private fun startThenWaitForClustering(filesIdsInBin: List<String> = emptyList()) {

    val files = projections.files.ids()
      .map { fileId -> projections.files.get(fileId) }
    clusteringService.globalOrPartialClusteringAsync(Parameters.defaults(), files, filesIdsInBin)
    do {
      Thread.sleep(10)
    } while (clusteringService.isRunning())
  }

  private fun setCalculateAllClustersReturnedValue(result: Clustering, deletedFileId: Optional<String>) {
    if (deletedFileId.isPresent) {
      markFileAsDeleted(deletedFileId.get())
    }
    `when`(clusteringRemoteApi.calculateClustering(any(), any())).thenReturn(TryOrCatch.of { result })
  }

  private fun verifyRecalculateOneClusterIsNotCalled() {
    verify(clusteringRemoteApi, never()).recalculateClusters(any(), any())
  }

  private fun b(): DomainFileBuilder {
    return DomainFile.builder()
  }
}
