package com.orange.documentare.clusteringui.biz.processes.hierarchy.transport

import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.MultisetElementMovedToNextHierarchyLevel
import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.HierarchyLoopRunnable
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModel
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.PersistencePauseAndResume
import jakarta.servlet.ServletOutputStream
import jakarta.servlet.http.Cookie
import jakarta.servlet.http.HttpServletResponse
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File
import java.io.PrintWriter
import java.util.*

internal class HierarchyControllerTest {
  private val hierarchyService = HierarchyService()
  private val hierarchyController = HierarchyController(hierarchyService, mockClientHierarchyExport())

  @TempDir
  lateinit var tempDir: File

  @Test
  fun `start hierarchy loop`() {
    // given
    var hierarchyLoopCalled = false
    hierarchyService.testPurposeOverrideHierarchyLoop(object : HierarchyLoopRunnable {
      override fun run() {
        hierarchyLoopCalled = true
      }
    })

    // when
    hierarchyController.buildHierarchy()

    // then
    then(hierarchyLoopCalled).isTrue
  }

  @Test
  fun `keep only first hierarchy level`() {
    // given
    var resumeHardPersistenceAndDropVolatilePersistenceCalled = false
    val persistencePauseAndResume = object : PersistencePauseAndResume {
      override fun pauseHardPersistenceAndAppendToVolatilePersistence() {
        throw IllegalStateException("not needed here")
      }

      override fun stopHardAndVolatilePersistence() {
        throw IllegalStateException("not needed here")
      }

      override fun resumeHardPersistenceAndDropVolatilePersistence() {
        resumeHardPersistenceAndDropVolatilePersistenceCalled = true
      }

    }
    hierarchyService.setPersistencePauseAndResume(persistencePauseAndResume)

    val projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
    hierarchyService.setProjections(projections)

    val fileCreated = FileCreated.example()
    projections.onEvent(fileCreated)
    projections.onEvent(MultisetElementMovedToNextHierarchyLevel(fileCreated.aggregateId, 1))

    then(projections.hierarchyLevelCount()).isEqualTo(2)

    // when
    hierarchyController.keepOnlyFirstHierarchyLevel()

    // then
    then(resumeHardPersistenceAndDropVolatilePersistenceCalled).isTrue
    then(projections.hierarchyLevelCount()).isEqualTo(1)
  }

  @Test
  fun `export client hierarchy`() {
    // given
    FileIO.initFilesDirectory(tempDir.absolutePath)
    val httpServletResponse = MockHttpServletResponse()

    // when
    val fileSystemResource = hierarchyController.clientExport(httpServletResponse)

    // then
    FileIO.resetFilesDirectoryForTestsPurpose()
    then(fileSystemResource).isNotNull
    then(fileSystemResource.exists()).isTrue
    then(fileSystemResource.isFile).isTrue
    then(fileSystemResource.contentLength()).isGreaterThan(1000)
    then(httpServletResponse.actualContentType).isEqualTo("application/json")
    then(httpServletResponse.actualHeaderKey).isEqualTo("Content-Disposition")
    then(httpServletResponse.actualHeaderValue).isEqualTo("attachment; filename=client-export.json")
  }

  class MockHttpServletResponse : HttpServletResponse {

    var actualContentType: String? = null
    var actualHeaderKey: String? = null
    var actualHeaderValue: String? = null

    override fun getCharacterEncoding(): String {
      TODO("Not yet implemented")
    }

    override fun getContentType(): String {
      TODO("Not yet implemented")
    }

    override fun getOutputStream(): ServletOutputStream {
      TODO("Not yet implemented")
    }

    override fun getWriter(): PrintWriter {
      TODO("Not yet implemented")
    }

    override fun setCharacterEncoding(p0: String?) {
      TODO("Not yet implemented")
    }

    override fun setContentLength(p0: Int) {
      TODO("Not yet implemented")
    }

    override fun setContentLengthLong(p0: Long) {
      TODO("Not yet implemented")
    }

    override fun setContentType(contentType: String?) {
      this.actualContentType = contentType
    }

    override fun setBufferSize(p0: Int) {
      TODO("Not yet implemented")
    }

    override fun getBufferSize(): Int {
      TODO("Not yet implemented")
    }

    override fun flushBuffer() {
      TODO("Not yet implemented")
    }

    override fun resetBuffer() {
      TODO("Not yet implemented")
    }

    override fun isCommitted(): Boolean {
      TODO("Not yet implemented")
    }

    override fun reset() {
      TODO("Not yet implemented")
    }

    override fun setLocale(p0: Locale?) {
      TODO("Not yet implemented")
    }

    override fun getLocale(): Locale {
      TODO("Not yet implemented")
    }

    override fun addCookie(p0: Cookie?) {
      TODO("Not yet implemented")
    }

    override fun containsHeader(p0: String?): Boolean {
      TODO("Not yet implemented")
    }

    override fun encodeURL(p0: String?): String {
      TODO("Not yet implemented")
    }

    override fun encodeRedirectURL(p0: String?): String {
      TODO("Not yet implemented")
    }

    override fun sendError(p0: Int, p1: String?) {
      TODO("Not yet implemented")
    }

    override fun sendError(p0: Int) {
      TODO("Not yet implemented")
    }

    override fun sendRedirect(p0: String?) {
      TODO("Not yet implemented")
    }

    override fun setDateHeader(p0: String?, p1: Long) {
      TODO("Not yet implemented")
    }

    override fun addDateHeader(p0: String?, p1: Long) {
      TODO("Not yet implemented")
    }

    override fun setHeader(key: String?, value: String?) {
      actualHeaderKey = key
      actualHeaderValue = value
    }

    override fun addHeader(p0: String?, p1: String?) {
      TODO("Not yet implemented")
    }

    override fun setIntHeader(p0: String?, p1: Int) {
      TODO("Not yet implemented")
    }

    override fun addIntHeader(p0: String?, p1: Int) {
      TODO("Not yet implemented")
    }

    override fun setStatus(p0: Int) {
      TODO("Not yet implemented")
    }

    override fun getStatus(): Int {
      TODO("Not yet implemented")
    }

    override fun getHeader(p0: String?): String {
      TODO("Not yet implemented")
    }

    override fun getHeaders(p0: String?): MutableCollection<String> {
      TODO("Not yet implemented")
    }

    override fun getHeaderNames(): MutableCollection<String> {
      TODO("Not yet implemented")
    }

  }

  private fun mockClientHierarchyExport(): ClientHierarchyExport {
    return object : ClientHierarchyExport {
      override fun exportClientHierarchy(): ClientHierarchyReferenceModel {
        return HierarchyExamples.exportedClientHierarchiesWith4LevelsOneRootNew()
      }

      override fun exportClientHierarchyForStatisticsConsolidation(): ClientHierarchyReferenceModel {
        TODO("Not yet implemented")
      }
    }
  }

}
