package com.orange.documentare.clusteringui.biz.processes.feed.transport

import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederPhases.Phase
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederPhases.Phase.MetadataBalancingStep
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederPhases.UpdateType
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederProcessFinished
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederService
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeedLoopEvent
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeederPhaseUpdated
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.queries.QueryBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class FeederControllerTest {

  @Test
  fun `get feeder events as a string`() {
    // given
    val events = listOf(
      FeederPhaseUpdated(UpdateType.AT_FEED_LOOP_START, Phase.METADATA_BALANCING, MetadataBalancingStep.BREAK_CLUSTERS),
      FeederPhaseUpdated(UpdateType.AT_FEED_LOOP_START, Phase.METADATA_BALANCING, MetadataBalancingStep.INCREMENTAL_CLUSTERING),
    )
    val feederController = FeederController(CommandBus(), QueryBus(), feederServiceWithEvents(events))

    // when
    val displayString = feederController.feedEvents()

    // then
    then(displayString).contains(
      "❗ [AT_FEED_LOOP_START] Feeder phase is now <strong>METADATA_BALANCING</strong> (step <strong>INCREMENTAL_CLUSTERING</strong>)<br>"
    )
  }

  private fun feederServiceWithEvents(events: List<FeedLoopEvent>) = object : FeederService {
    override fun launch(feederProcessFinished: FeederProcessFinished?) {
      TODO("Not yet implemented")
    }

    override fun events() = events
  }
}
