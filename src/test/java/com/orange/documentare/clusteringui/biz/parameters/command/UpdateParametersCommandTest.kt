package com.orange.documentare.clusteringui.biz.parameters.command

import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.infrastructure.persistence.ParametersInMemoryRepository
import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class UpdateParametersCommandTest {

  private val repo = ParametersInMemoryRepository()
  private val handler = UpdateParametersCommandHandler(repo)

  @Test
  fun update_invalid_parameters_generates_an_error() {
    // given
    val cmd = UpdateParametersCommand.invalidExample().build()

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isFalse
  }

  @Test
  fun update_parameters_when_it_does_not_exists_yet() {
    // given
    val cmd = UpdateParametersCommand.validExample().pCount(1234).build()

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isTrue
    then(repo.findAll().iterator().hasNext()).isTrue
    then(repo.findAll().iterator().next().pCount).isEqualTo(1234)
  }

  @Test
  fun replace_current_parameters_when_it_exists() {
    // given
    val pCount = 1234
    val acutSdFactor = 1.11f
    val qcutSdFactor = 1.12f
    val scutSdFactor = 1.13f
    val ccutPercentile = 34
    val knnThreshold = 123456
    val enrollAcutSdFactor = 2.11f
    val enrollQcutSdFactor = 2.12f
    val enrollScutSdFactor = 2.13f
    val enrollCcutPercentile = 56
    val enrollKnnThreshold = 456
    val thresholdFactor = 1.2345f
    val feedThreshold = 12
    val minMultisetThreshold = 12
    val maxMultisetThreshold = 34

    var cmd = UpdateParametersCommand.validExample().build()
    handler.execute(cmd)

    cmd = UpdateParametersCommand.builder()
      .rawConverter(true)
      .pCount(pCount)
      .suffixArrayAlgorithm(SuffixArrayAlgorithm.SAIS)
      .acutSdFactor(acutSdFactor)
      .qcutSdFactor(qcutSdFactor)
      .scutSdFactor(scutSdFactor)
      .ccutPercentile(ccutPercentile)
      .knnThreshold(knnThreshold)
      .sloop(true)
      .enroll(true)
      .enrollAcutSdFactor(enrollAcutSdFactor)
      .enrollQcutSdFactor(enrollQcutSdFactor)
      .enrollScutSdFactor(enrollScutSdFactor)
      .enrollCcutPercentile(enrollCcutPercentile)
      .enrollKnnThreshold(enrollKnnThreshold)
      .thresholdFactor(thresholdFactor)
      .feed(true)
      .feedThreshold(feedThreshold)
      .minMultisetThreshold(minMultisetThreshold)
      .maxMultisetThreshold(maxMultisetThreshold)
      .build()

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isTrue
    then(repo.findAll()).hasSize(1)
    then(repo.findAll().iterator().next()).isEqualTo(
      Parameters.builder()
        .id(0)
        .rawConverter(true)
        .suffixArrayAlgorithm(SuffixArrayAlgorithm.SAIS)
        .pCount(pCount)
        .acutSdFactor(acutSdFactor)
        .qcutSdFactor(qcutSdFactor)
        .scutSdFactor(scutSdFactor)
        .ccutPercentile(ccutPercentile)
        .knnThreshold(knnThreshold)
        .sloop(true)
        .enroll(true)
        .enrollAcutSdFactor(enrollAcutSdFactor)
        .enrollQcutSdFactor(enrollQcutSdFactor)
        .enrollScutSdFactor(enrollScutSdFactor)
        .enrollCcutPercentile(enrollCcutPercentile)
        .enrollKnnThreshold(enrollKnnThreshold)
        .thresholdFactor(thresholdFactor)
        .feed(true)
        .feedThreshold(feedThreshold)
        .minMultisetThreshold(minMultisetThreshold)
        .maxMultisetThreshold(maxMultisetThreshold)
        .build()
    )
  }

  @Test
  fun `init command builder with parameters`() {
    // given
    val pCount = 1234
    val acutSdFactor = 1.11f
    val qcutSdFactor = 1.12f
    val scutSdFactor = 1.13f
    val ccutPercentile = 34
    val knnThreshold = 123456
    val enrollAcutSdFactor = 2.11f
    val enrollQcutSdFactor = 2.12f
    val enrollScutSdFactor = 2.13f
    val enrollCcutPercentile = 56
    val enrollKnnThreshold = 456
    val thresholdFactor = 1.2345f
    val feedThreshold = 12
    val maxMultisetThreshold = 34

    val parameters = Parameters.builder()
      .id(0)
      .pCount(pCount)
      .acutSdFactor(acutSdFactor)
      .qcutSdFactor(qcutSdFactor)
      .scutSdFactor(scutSdFactor)
      .ccutPercentile(ccutPercentile)
      .knnThreshold(knnThreshold)
      .sloop(true)
      .enroll(true)
      .enrollAcutSdFactor(enrollAcutSdFactor)
      .enrollQcutSdFactor(enrollQcutSdFactor)
      .enrollScutSdFactor(enrollScutSdFactor)
      .enrollCcutPercentile(enrollCcutPercentile)
      .enrollKnnThreshold(enrollKnnThreshold)
      .thresholdFactor(thresholdFactor)
      .feed(true)
      .feedThreshold(feedThreshold)
      .maxMultisetThreshold(maxMultisetThreshold)
      .build()

    // when
    val cmd = UpdateParametersCommand.initBuilderWith(parameters).build()

    // then
    then(cmd).isEqualTo(
      UpdateParametersCommand.builder()
        .pCount(pCount)
        .acutSdFactor(acutSdFactor)
        .qcutSdFactor(qcutSdFactor)
        .scutSdFactor(scutSdFactor)
        .ccutPercentile(ccutPercentile)
        .knnThreshold(knnThreshold)
        .sloop(true)
        .enroll(true)
        .enrollAcutSdFactor(enrollAcutSdFactor)
        .enrollQcutSdFactor(enrollQcutSdFactor)
        .enrollScutSdFactor(enrollScutSdFactor)
        .enrollCcutPercentile(enrollCcutPercentile)
        .enrollKnnThreshold(enrollKnnThreshold)
        .thresholdFactor(thresholdFactor)
        .feed(true)
        .feedThreshold(feedThreshold)
        .maxMultisetThreshold(maxMultisetThreshold)
        .build()
    )
  }
}
