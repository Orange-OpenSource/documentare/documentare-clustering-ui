package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.queries.QueryResponse
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class AllClustersQueryTest {
  var projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  var handler = AllClustersQueryHandler(projections)

  @Test
  fun retrieve_all_clusters() {
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "0"),
      FileCreated(aggregateIdExample(1), "1"),
      FileAddedToCluster(aggregateIdExample(0), "1", true, null, null, null, null, null)
    )
    projections.testPurposeInitFromHistory(events)

    // when
    val response: QueryResponse<Map<String, List<String>>> = handler.execute(AllClustersQuery())

    // then
    then(response.success()).isTrue()
    then(response.value()).hasSize(1)
  }
}
