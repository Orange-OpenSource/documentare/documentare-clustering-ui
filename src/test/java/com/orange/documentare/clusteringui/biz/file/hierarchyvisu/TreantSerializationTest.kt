package com.orange.documentare.clusteringui.biz.file.hierarchyvisu

import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class TreantSerializationTest {

  @Test
  fun `serialize treant model`() {
    // when
    val treantHierarchyJson = TreantExample.serialize(TreantExample.dto())

    // then
    then(treantHierarchyJson).isEqualTo(TreantExample.JSON_EXAMPLE)
  }
}
