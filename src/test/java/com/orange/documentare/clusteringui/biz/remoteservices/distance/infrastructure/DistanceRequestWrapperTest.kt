/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure

import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.biz.remoteservices.infrastructure.BytesDataUI
import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File
import java.io.IOException

@DisplayName("Distance request wrapper")
internal class DistanceRequestWrapperTest {

  @TempDir
  lateinit var tmpDir: File

  @DisplayName("initialize requestId with distance element id, 1 id")
  @Test
  @Throws(IOException::class)
  fun initialize_requestId_with_distance_element_id() {
    // given
    val ids = listOf("12")
    val distanceRequest = buildDistancesRequest(ids)

    // when
    val distanceRequestWrapper = DistanceRequestWrapper(distanceRequest, EmbeddedComputationServer.disabled())

    // then
    then(distanceRequestWrapper.requestId()).isEqualTo("12")
  }

  @DisplayName("initialize requestId with distance element ids, 2 id")
  @Test
  @Throws(IOException::class)
  fun initialize_requestId_with_distance_element_idS() {
    // given
    // more complex case: 2 id since it means we will concat the associated files
    val ids = listOf("12", "34")
    val distanceRequest = buildDistancesRequest(ids)

    // when
    val distanceRequestWrapper = DistanceRequestWrapper(distanceRequest, EmbeddedComputationServer.disabled())

    // then
    then(distanceRequestWrapper.requestId()).isEqualTo("12_34")
  }

  @Throws(IOException::class)
  private fun buildDistancesRequest(ids: List<String>): DistancesRequestUI {
    val file = File(tmpDir.absolutePath + File.separator + "test_distance_request")
    file.writeText("")
    val filepaths = listOf(file.absolutePath)
    val element = arrayOf(BytesDataUI(ids, filepaths))
    val rawConverter = true
    val pCount = 10000
    val suffixArrayAlgorithm = SuffixArrayAlgorithm.LIBDIVSUFSORT
    return DistancesRequestUI(
      element,
      element,
      rawConverter,
      pCount,
      suffixArrayAlgorithm
    )
  }
}
