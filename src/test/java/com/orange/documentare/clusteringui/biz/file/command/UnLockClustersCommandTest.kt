package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.event.ClusterLocked
import com.orange.documentare.clusteringui.biz.file.event.ClusterPutInNamedClass
import com.orange.documentare.clusteringui.biz.file.event.ClusterUnlocked
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.infrastructure.event.EventBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.io.IOException

@DisplayName("Command UnLockClusters")
class UnLockClustersCommandTest {
  /**
   * init eventBus with event repository so that events will be persisted automatically
   */
  var eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  var eventBus = EventBus(eventRepo)
  var handler = UnLockClustersCommandHandler(eventRepo, eventBus)

  @Test
  @Throws(IOException::class)
  fun a_unlock_clusters_command_generates_a_ClusterUnLocked_event() {
    // given
    val initialEvents = mutableListOf(
      FileCreated(aggregateIdExample(0), "f0", "10", true),
      ClusterLocked(aggregateIdExample(0), "10", true)
    )
    eventRepo.insert(initialEvents)
    val cmd = UnLockClustersCommand(listOf(aggregateIdExample(0)))

    // when
    val response: CommandResponse<*> = handler.execute(cmd)

    // then
    then(response.success()).isTrue()
    initialEvents.apply {
      this.add(
        ClusterUnlocked(
          aggregateIdExample(0),
          "10",
          false,
          null
        )
      )
      then(eventRepo.findAll()).containsAll(this)
    }
  }

  @Test
  @Throws(IOException::class)
  fun a_unlock_clusters_command_generates_a_ClusterUnLocked_event_and_remove_class_name() {
    // given
    val initialEvents = mutableListOf(
      FileCreated(aggregateIdExample(0), "f0", "10", true),
      ClusterLocked(aggregateIdExample(0), "10", true),
      ClusterPutInNamedClass(aggregateIdExample(0), "10", "test")
    )
    eventRepo.insert(initialEvents)
    val cmd = UnLockClustersCommand(listOf(aggregateIdExample(0)))

    // when
    val response: CommandResponse<*> = handler.execute(cmd)

    // then
    then(response.success()).isTrue()
    initialEvents.apply {
      this.add(
        ClusterUnlocked(
          aggregateIdExample(0),
          "10",
          false,
          null
        )
      )
      then(eventRepo.findAll()).containsAll(this)
    }
  }
}
