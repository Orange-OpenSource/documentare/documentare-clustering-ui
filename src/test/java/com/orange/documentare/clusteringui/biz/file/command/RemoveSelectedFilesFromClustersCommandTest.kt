package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.domain.DomainFileBuilder
import com.orange.documentare.clusteringui.biz.file.domain.Room
import com.orange.documentare.clusteringui.biz.file.event.ClusterModified
import com.orange.documentare.clusteringui.biz.file.event.DistancesToClusterCenterAdded
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.event.FileRemovedFromCluster
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.event.TestEventHandler
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Command RemoveSelectedFilesFromClusters")
class RemoveSelectedFilesFromClustersCommandTest {
  var testEventHandler = TestEventHandler()
  var eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  var eventBus = EventBus(eventRepo)
  var handler = RemoveSelectedFilesFromClustersCommandHandler(eventRepo, eventBus)

  @BeforeEach
  fun setUp() {
    eventBus.register(testEventHandler)
  }

  @Test
  fun can_remove_two_files_from_two_distinct_clusters() {
    // given
    val clusterCenterFile1 = b().aggregateId(aggregateIdExample(0)).filename("0").clusterId("10").clusterCenter(true).build()
    eventRepo.insert(listOf(FileCreated(clusterCenterFile1.aggregateId, "0", "10", true)))
    val file1InCluster1 = b().aggregateId(aggregateIdExample(1)).filename("1").clusterId("10").build()
    eventRepo.insert(listOf(FileCreated(file1InCluster1.aggregateId, "1", "10", null)))
    val file2InCluster1 = b().aggregateId(aggregateIdExample(2)).filename("2").clusterId("10").build()
    eventRepo.insert(listOf(FileCreated(file2InCluster1.aggregateId, "2", "10", false)))
    val file3InCluster1 = b().aggregateId(aggregateIdExample(3)).filename("3").clusterId("10").build()
    eventRepo.insert(listOf(FileCreated(file3InCluster1.aggregateId, "3", "10", false)))
    val file4InCluster1 = b().aggregateId(aggregateIdExample(4)).filename("4").clusterId("10").build()
    eventRepo.insert(listOf(FileCreated(file4InCluster1.aggregateId, "4", "10", false)))
    val clusterCenterFile2 = b().aggregateId(aggregateIdExample(5)).filename("5").clusterId("20").clusterCenter(true).build()
    eventRepo.insert(listOf(FileCreated(clusterCenterFile2.aggregateId, "5", "20", true)))
    val file6InCluster2 = b().aggregateId(aggregateIdExample(6)).filename("6").clusterId("20").build()
    eventRepo.insert(listOf(FileCreated(file6InCluster2.aggregateId, "6", "20", null)))
    val file7InCluster2 = b().aggregateId(aggregateIdExample(7)).filename("7").clusterId("20").build()
    eventRepo.insert(listOf(FileCreated(file7InCluster2.aggregateId, "7", "20", false)))
    val file8InCluster2 = b().aggregateId(aggregateIdExample(8)).filename("8").clusterId("20").build()
    eventRepo.insert(listOf(FileCreated(file8InCluster2.aggregateId, "8", "20", false)))
    val clusters: Map<String, List<DomainFile>> = mapOf(
      Pair("10", listOf(clusterCenterFile1, file1InCluster1, file2InCluster1, file3InCluster1, file4InCluster1)),
      Pair("20", listOf(clusterCenterFile2, file6InCluster2, file7InCluster2, file8InCluster2))
    )

    // when
    handler.execute(RemoveSelectedFilesFromClustersCommand(listOf(file1InCluster1.aggregateId, file6InCluster2.aggregateId), clusters, Room.SORT_ROOM))

    // then
    then(testEventHandler.events()).containsExactly(
      FileRemovedFromCluster(file1InCluster1.aggregateId, "10", null),
      FileRemovedFromCluster(file6InCluster2.aggregateId, "20", null),
      ClusterModified(clusterCenterFile1.aggregateId, "10"),
      ClusterModified(clusterCenterFile2.aggregateId, "20")
    )
  }

  @Test
  fun remove_cluster_center_should_remove_all_files_in_cluster() {
    // given
    val clusterCenterFile = b().aggregateId(aggregateIdExample(0)).filename("0").clusterId("10").clusterCenter(true).build()
    eventRepo.insert(listOf(FileCreated(clusterCenterFile.aggregateId, "0", "10", true)))
    val fileInCluster = b().aggregateId(aggregateIdExample(1)).filename("1").clusterId("10").build()
    eventRepo.insert(listOf(FileCreated(fileInCluster.aggregateId, "1", "10", null)))
    val clusters: Map<String, List<DomainFile>> = mapOf(Pair("10", listOf(clusterCenterFile, fileInCluster)))

    // when
    handler.execute(RemoveSelectedFilesFromClustersCommand(listOf(clusterCenterFile.aggregateId), clusters, Room.SORT_ROOM))

    // then
    then(testEventHandler.events()).containsExactly(
      FileRemovedFromCluster(clusterCenterFile.aggregateId, "10", null),
      FileRemovedFromCluster(fileInCluster.aggregateId, "10", null)
    )
  }

  @Test
  fun cluster_center_should_not_be_removed_if_it_is_the_last_element_of_the_cluster() {
    // given
    val clusterCenterFile = b().aggregateId(aggregateIdExample(0)).filename("id0").clusterId("10").clusterCenter(true).build()
    eventRepo.insert(listOf(FileCreated(clusterCenterFile.aggregateId, "id0", "10", true)))
    val fileInCluster = b().aggregateId(aggregateIdExample(1)).filename("id1").clusterId("10").build()
    eventRepo.insert(listOf(FileCreated(fileInCluster.aggregateId, "id1", "10", null)))
    val clusters: Map<String, List<DomainFile>> = mapOf(Pair("10", listOf(clusterCenterFile, fileInCluster)))

    // when
    val response = handler.execute(RemoveSelectedFilesFromClustersCommand(listOf(fileInCluster.aggregateId), clusters, Room.SORT_ROOM))

    // then
    then(response.success()).isTrue()
    then(testEventHandler.events()).containsExactly(
      FileRemovedFromCluster(fileInCluster.aggregateId, "10", null),
      ClusterModified(clusterCenterFile.aggregateId, "10")
    )
  }

  @Test
  fun cluster_center_is_not_removed_if_it_not_the_last_element() {
    // given
    val clusterCenterFile = b().aggregateId(aggregateIdExample(0)).filename("id0").clusterId("10").clusterCenter(true).build()
    eventRepo.insert(listOf(FileCreated(clusterCenterFile.aggregateId, "id0", "10", true)))
    val file1InCluster = b().aggregateId(aggregateIdExample(1)).filename("id1").clusterId("10").build()
    eventRepo.insert(listOf(FileCreated(file1InCluster.aggregateId, "id1", "10", null)))
    val file2InCluster = b().aggregateId(aggregateIdExample(2)).filename("id2").clusterId("10").build()
    eventRepo.insert(listOf(FileCreated(file2InCluster.aggregateId, "id2", "10", null)))
    val clusters: Map<String, List<DomainFile>> = mapOf(Pair("10", listOf(clusterCenterFile, file1InCluster, file2InCluster)))

    // when
    val response = handler.execute(RemoveSelectedFilesFromClustersCommand(listOf(file1InCluster.aggregateId), clusters, Room.SORT_ROOM))

    // then
    then(response.success()).isTrue()
    then(testEventHandler.events()).containsExactly(
      FileRemovedFromCluster(file1InCluster.aggregateId, "10", null),
      ClusterModified(clusterCenterFile.aggregateId, "10")
    )
  }

  @Test
  fun remove_cluster_center_should_remove_all_distances_linked_to_this_cluster() {
    // given
    val clusterCenterFile = b().aggregateId(aggregateIdExample(0)).filename("0").clusterId("10").clusterCenter(true).build()
    eventRepo.insert(listOf(FileCreated(clusterCenterFile.aggregateId, "0", "10", true)))
    val fileWithADistance = b().aggregateId(aggregateIdExample(1)).filename("1").distancesToClustersCenters(listOf(DistanceToCluster(100, "10"))).build()
    eventRepo.insert(listOf(FileCreated(fileWithADistance.aggregateId, "1")))
    eventRepo.insert(listOf(DistancesToClusterCenterAdded(fileWithADistance.aggregateId, listOf(DistanceToCluster(100, "10")))))
    val clusters: Map<String, List<DomainFile>> = mapOf(Pair("10", listOf(clusterCenterFile)))

    // when
    val response = handler.execute(RemoveSelectedFilesFromClustersCommand(listOf(clusterCenterFile.aggregateId), clusters, Room.SORT_ROOM))

    // then
    then(response.success()).isTrue()
    then(testEventHandler.events()).containsExactly(
      FileRemovedFromCluster(clusterCenterFile.aggregateId, "10", null)
    )
  }

  @Test
  fun remove_a_file_in_cluster_and_after_try_to_remove_this_file_and_the_cluster_center_can_remove_all_the_cluster() {
    // given
    val clusterCenterFile1 = b().aggregateId(aggregateIdExample(0)).filename("0").clusterId("10").clusterCenter(true).build()
    eventRepo.insert(listOf(FileCreated(clusterCenterFile1.aggregateId, "0", "10", true)))
    val file1InCluster1 = b().aggregateId(aggregateIdExample(1)).filename("1").clusterId("10").build()
    eventRepo.insert(listOf(FileCreated(file1InCluster1.aggregateId, "1", "10", null)))
    val file2InCluster1 = b().aggregateId(aggregateIdExample(2)).filename("2").clusterId("10").build()
    eventRepo.insert(listOf(FileCreated(file2InCluster1.aggregateId, "2", "10", false)))
    val file3InCluster1 = b().aggregateId(aggregateIdExample(3)).filename("3").clusterId("10").build()
    eventRepo.insert(listOf(FileCreated(file3InCluster1.aggregateId, "3", "10", false)))
    val file4InCluster1 = b().aggregateId(aggregateIdExample(4)).filename("4").clusterId("10").build()
    eventRepo.insert(listOf(FileCreated(file4InCluster1.aggregateId, "4", "10", false)))
    val clusters: Map<String, List<DomainFile>> = mapOf(Pair("10", listOf(clusterCenterFile1, file1InCluster1, file2InCluster1, file3InCluster1, file4InCluster1)))

    // when
    val response = handler.execute(RemoveSelectedFilesFromClustersCommand(listOf(file1InCluster1.aggregateId), clusters, Room.SORT_ROOM))

    // then
    then(response.success()).isTrue()
    then(testEventHandler.events()).containsExactly(
      FileRemovedFromCluster(file1InCluster1.aggregateId, "10", null),
      ClusterModified(clusterCenterFile1.aggregateId, "10")
    )
    val response1 = handler.execute(RemoveSelectedFilesFromClustersCommand(listOf(file1InCluster1.aggregateId, clusterCenterFile1.aggregateId), clusters, Room.SORT_ROOM))
    then(response1.success()).isTrue()
    then(testEventHandler.events()).containsExactly(
      FileRemovedFromCluster(file1InCluster1.aggregateId, "10", null),
      ClusterModified(clusterCenterFile1.aggregateId, "10"),
      FileRemovedFromCluster(clusterCenterFile1.aggregateId, "10", null),
      FileRemovedFromCluster(file1InCluster1.aggregateId, "10", null),
      FileRemovedFromCluster(file2InCluster1.aggregateId, "10", null),
      FileRemovedFromCluster(file3InCluster1.aggregateId, "10", null),
      FileRemovedFromCluster(file4InCluster1.aggregateId, "10", null)
    )
  }

  private fun b(): DomainFileBuilder = DomainFile.builder()
}
