package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.queries.QueryBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`

class DecrementMultisetThresholdAndBreakClustersTest {

  private val commandBus = mock<CommandBus>()
  private val queryBus = mock<QueryBus>()
  private val minMultiSetThreshold = 1
  private val notLockedClusters = mock<NotLockedClusters>()
  private val breakClusters = mock<BreakOutOfThresholdNotLockedClusters>()
  private val decrementMultiset = DecrementMultisetThresholdAndBreakClusters(commandBus, queryBus, minMultiSetThreshold, breakClusters, notLockedClusters)

  @Test
  fun `min threshold not reached after init`() {
    then(decrementMultiset.hasReachedMinThreshold()).isFalse()
  }

  @Test
  fun `break not locked clusters (relying on multiset threshold) then decrement multiset threshold`() {
    // given
    `when`(notLockedClusters.countMaxMultiset()).thenReturn(minMultiSetThreshold + 1)

    // when
    decrementMultiset.checkIfWeNeedToBreakClusters()

    // then
    verify(breakClusters).breakIfNumberOfMultisetIsGreaterThanMultisetThreshold(minMultiSetThreshold)
  }

  @Test
  fun `do nothing if current multiset threshold is lesser than minMultiSetThreshold`() {
    // given
    `when`(notLockedClusters.countMaxMultiset()).thenReturn(minMultiSetThreshold)

    // when
    decrementMultiset.checkIfWeNeedToBreakClusters()

    // then
    verify(breakClusters, never()).breakIfNumberOfMultisetIsGreaterThanMultisetThreshold(any())
  }

  @Test
  fun `reset current multiset threshold so that min threshold is not reached`() {
    // when
    repeat(10) { decrementMultiset.checkIfWeNeedToBreakClusters() }
    decrementMultiset.reset()

    // then
    then(decrementMultiset.hasReachedMinThreshold()).isFalse()
  }
}
