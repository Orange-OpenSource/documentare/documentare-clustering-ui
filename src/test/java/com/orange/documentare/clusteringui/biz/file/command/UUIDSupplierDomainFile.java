package com.orange.documentare.clusteringui.biz.file.command;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

class UUIDSupplierDomainFile {

  private List<String> uuids = new ArrayList<>();

  public String getAsString() {
    String uuid = UUID.randomUUID().toString();
    uuids.add(uuid);
    return uuid;
  }

  public String get(int index) {
    return uuids.get(index);
  }
}
