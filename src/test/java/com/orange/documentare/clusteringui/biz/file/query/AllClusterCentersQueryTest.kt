package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.MultisetElementMovedToNextHierarchyLevel
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

internal class AllClusterCentersQueryTest {
  private val projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  private val handler = AllClusterCentersAndMultiSetsQueryHandler(projections)
  private val aggregateId1 = "0"
  private val aggregateId2 = "1"
  private val aggregateId3 = "2"

  @Test
  fun `all cluster centers id with current hierarchy level`() {
    // given
    projections.testPurposeInitFromHistory(twoClusterCentersAndARegularFile())

    // when
    val response = handler.execute(AllClusterCentersAndMultiSetsQuery())

    // then
    then(response.value()).isEqualTo(AllClusterCentersAndMultiSetsQuery.AllClusterCentersAndMultiSets(listOf(aggregateId1, aggregateId2), 0))
  }

  @Test
  fun `all cluster centers and multisets id with current hierarchy level`() {
    // given
    projections.testPurposeInitFromHistory(twoClusterCentersAndAMultisetAndARegularFile())

    // when
    val response = handler.execute(AllClusterCentersAndMultiSetsQuery())

    // then
    then(response.value()).isEqualTo(AllClusterCentersAndMultiSetsQuery.AllClusterCentersAndMultiSets(listOf(aggregateId1, aggregateId2, aggregateId3), 0))
  }

  @Test
  fun `all cluster centers id with not the initial hierarchy level`() {
    // given
    projections.testPurposeInitFromHistory(twoClusterCentersAndARegularFile())
    projections.onEvent(
      MultisetElementMovedToNextHierarchyLevel(
        aggregateId1,
        1
      )
    )
    projections.onEvent(
      MultisetElementMovedToNextHierarchyLevel(
        aggregateId2,
        1
      )
    )

    // when
    val response = handler.execute(AllClusterCentersAndMultiSetsQuery())

    // then
    then(response.value()).isEqualTo(AllClusterCentersAndMultiSetsQuery.AllClusterCentersAndMultiSets(emptyList(), 1))
  }

  private fun twoClusterCentersAndARegularFile() = listOf(
    FileCreated(aggregateId1, "f0"),
    FileCreated(aggregateId2, "f1"),
    FileCreated("12345678", "f1324234234"),
    FileAddedToCluster(aggregateId1, "10", true, null, null, null, null, null),
    FileAddedToCluster(aggregateId2, "20", true, null, null, null, null, null)
  )

  private fun twoClusterCentersAndAMultisetAndARegularFile() = listOf(
    FileCreated(aggregateId1, "f0"),
    FileCreated(aggregateId2, "f1"),
    FileCreated(aggregateId3, "f2"),
    FileCreated("12345678", "f1324234234"),
    FileAddedToCluster(aggregateId1, "10", true, null, null, null, null, null),
    FileAddedToCluster(aggregateId2, "20", true, null, null, null, null, null),
    FileAddedToCluster(aggregateId3, "20", null, null, null, true, null, null)
  )
}
