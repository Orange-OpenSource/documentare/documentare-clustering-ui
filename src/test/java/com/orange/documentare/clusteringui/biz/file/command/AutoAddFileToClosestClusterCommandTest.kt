package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.ClusterModified
import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.event.TestEventHandler
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class AutoAddFileToClosestClusterCommandTest {

  internal var eventBus = EventBus(DBEventInMemoryRepositoryForTestPurpose())
  internal var testEventHandler = TestEventHandler()
  internal var eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  internal var handler = AutoAddFileToClosestClusterCommandHandler(eventRepo, eventBus)

  @BeforeEach
  fun setup() {
    eventBus.register(testEventHandler)
  }

  @Test
  fun `add file automatically to its closest cluster`() {
    // given
    val clusterCenter = DomainFile.builder().aggregateId(aggregateIdExample(0)).filename("0").clusterId("10").clusterCenter(true).build()
    val fileNotInCluster = DomainFile.builder().aggregateId(aggregateIdExample(1)).filename("1").build()
    eventRepo.insert(
      listOf(
        FileCreated(clusterCenter.aggregateId, clusterCenter.filename, "10", true),
        FileCreated(fileNotInCluster.aggregateId, fileNotInCluster.filename)
      )
    )

    val cmd = AutoAddFileToClosestClusterCommand(
      fileNotInCluster.aggregateId,
      DistanceToCluster(100, "10"),
      listOf(clusterCenter.aggregateId),
      2
    )

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isTrue()

    then(testEventHandler.events()).containsExactly(
      FileAddedToCluster.builder().aggregateId(fileNotInCluster.aggregateId).clusterId("10").distanceToCenter(100).automaticInclude(true).build(),
      ClusterModified(clusterCenter.aggregateId, "10")
    )
  }

  @Test
  fun `add file automatically to its closest cluster BUT do not tag cluster as modified since it is locked and multiset count is above threshold`() {
    // given
    val clusterCenter = DomainFile.builder().aggregateId(aggregateIdExample(0)).filename("0").clusterId("10").clusterCenter(true).inMultiset(true).lock(true).build()
    val fileNotInCluster = DomainFile.builder().aggregateId(aggregateIdExample(1)).filename("1").build()
    eventRepo.insert(
      listOf(
        FileCreated(clusterCenter.aggregateId, clusterCenter.filename),
        FileAddedToCluster.builder().aggregateId(clusterCenter.aggregateId).clusterCenter(true).inMultiset(true).lock(true).build(),
        FileCreated(fileNotInCluster.aggregateId, fileNotInCluster.filename)
      )
    )

    val cmd = AutoAddFileToClosestClusterCommand(
      fileNotInCluster.aggregateId,
      DistanceToCluster(100, "10"),
      listOf(clusterCenter.aggregateId),
      0
    )

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isTrue()

    then(testEventHandler.events()).containsExactly(
      FileAddedToCluster.builder().aggregateId(fileNotInCluster.aggregateId).clusterId("10").distanceToCenter(100).automaticInclude(true).build()
    )
  }

  @Test
  fun `add file automatically to its closest cluster AND tag cluster as modified since it is locked but multiset count is below threshold`() {
    // given
    val clusterCenter = DomainFile.builder().aggregateId(aggregateIdExample(0)).filename("0").clusterId("10").clusterCenter(true).inMultiset(true).lock(true).build()
    val fileNotInCluster = DomainFile.builder().aggregateId(aggregateIdExample(1)).filename("1").build()
    eventRepo.insert(
      listOf(
        FileCreated(clusterCenter.aggregateId, clusterCenter.filename),
        FileAddedToCluster.builder().aggregateId(clusterCenter.aggregateId).clusterId(clusterCenter.clusterId).clusterCenter(true).inMultiset(true).lock(true).build(),
        FileCreated(fileNotInCluster.aggregateId, clusterCenter.filename)
      )
    )

    val cmd = AutoAddFileToClosestClusterCommand(
      fileNotInCluster.aggregateId,
      DistanceToCluster(100, "10"),
      listOf(clusterCenter.aggregateId),
      10
    )

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isTrue()

    then(testEventHandler.events()).containsExactly(
      FileAddedToCluster.builder().aggregateId(fileNotInCluster.aggregateId).clusterId("10").distanceToCenter(100).automaticInclude(true).build(),
      ClusterModified(clusterCenter.aggregateId, "10")
    )
  }
}
