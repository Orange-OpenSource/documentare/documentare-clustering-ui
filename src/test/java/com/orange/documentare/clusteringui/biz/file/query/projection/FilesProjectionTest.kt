package com.orange.documentare.clusteringui.biz.file.query.projection

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.event.FileDeleted
import com.orange.documentare.clusteringui.biz.file.event.FileRemovedFromCluster
import com.tophe.ddd.infrastructure.event.Event
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Query Files Projection")
class FilesProjectionTest {

  @Test
  fun `setup projection thanks to events history`() {
    // given
    val projection = FilesProjection()
    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0"),
      FileCreated(aggregateIdExample(1), "f1"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("10").build()
    )

    // when
    projection.initFromHistory(events)

    // then
    then(projection.filesCount()).isEqualTo(2)
    then(projection[aggregateIdExample(0)]).isEqualTo(DomainFile.builder().aggregateId(aggregateIdExample(0)).filename("f0").build())
    then(projection[aggregateIdExample(1)]).isEqualTo(DomainFile.builder().aggregateId(aggregateIdExample(1)).filename("f1").clusterId("10").build())
  }

  @Test
  fun `modify file in projection thanks to new event`() {
    // given
    val projection = FilesProjection()
    val events = listOf(
      FileCreated(aggregateIdExample(1), "f1"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("10").build()
    )
    projection.initFromHistory(events)

    // when
    projection.onEvent(FileRemovedFromCluster(aggregateIdExample(1), "10", null))

    // then
    then(projection[aggregateIdExample(1)]).isEqualTo(DomainFile.builder().aggregateId(aggregateIdExample(1)).filename("f1").clusterId(null).build())
  }

  @Test
  fun `deleted file should be kept in projection`() {
    // given
    val projection = FilesProjection()
    val events = listOf<Event>(
      FileCreated(aggregateIdExample(1), "f1")
    )
    projection.initFromHistory(events)

    // when
    projection.onEvent(FileDeleted(aggregateIdExample(1), null))

    // then
    then(projection.filesCount()).isEqualTo(1)
  }
}
