package com.orange.documentare.clusteringui.biz.reset.command

import com.orange.documentare.clusteringui.biz.file.command.AddFilesCommand
import com.orange.documentare.clusteringui.biz.file.command.AddFilesCommandHandler
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.infrastructure.event.EventBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import java.io.IOException

class ResetCommandTest {
  var eventBus = EventBus(DBEventInMemoryRepositoryForTestPurpose())
  var eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  var projections = Projections(eventRepo)
  var handler = ResetCommandHandler(eventRepo, projections)

  @BeforeEach
  fun setup() {
    File(FILES_DIRECTORY).deleteRecursively()
    FileIO.initFilesDirectory(FILES_DIRECTORY)
  }

  @Test
  @Throws(IOException::class)
  fun delete_created_files_on_filesystem() {
    // given
    addFilesInDB()

    // when
    val response: CommandResponse<*> = handler.execute(ResetCommand())

    // then
    then(response.success()).isTrue
    then(File(FILES_DIRECTORY).listFiles()).hasSize(2)
    then(File("$FILES_DIRECTORY/thumbnails/").listFiles()).hasSize(0)
  }

  @Test
  @Throws(IOException::class)
  fun delete_created_files_in_db() {
    // given
    addFilesInDB()

    // when
    val response: CommandResponse<*> = handler.execute(ResetCommand())

    // then
    checkDBIsEmpty(response)
  }

  @Test
  @Throws(IOException::class)
  fun delete_created_files_in_db_even_if_persistence_is_stopped() {
    // given
    addFilesInDB()

    // when
    eventRepo.stopHardAndVolatilePersistence()
    val response: CommandResponse<*> = handler.execute(ResetCommand())

    // then
    checkDBIsEmpty(response)
  }

  @Test
  @Throws(IOException::class)
  fun delete_created_files_in_db_even_if_persistence_is_paused() {
    // given
    addFilesInDB()

    // when
    eventRepo.pauseHardPersistenceAndAppendToVolatilePersistence()
    val response: CommandResponse<*> = handler.execute(ResetCommand())

    // then
    checkDBIsEmpty(response)
  }

  private fun addFilesInDB() {
    val image = File(javaClass.getResource("/image.png").file)
    val bytes = image.readBytes()
    val file = AddFilesCommand.File(image.name, bytes)
    val addCmd = AddFilesCommand(listOf(file))
    val addHandler = AddFilesCommandHandler(eventBus)
    addHandler.execute(addCmd)
  }

  private fun checkDBIsEmpty(response: CommandResponse<*>) {
    then(response.success()).isTrue
    then(eventRepo.findAll().iterator().hasNext()).isFalse
  }

  @Test
  fun projections_are_reinitialised() {
    // given
    projections.onEvent(FileCreated(aggregateIdExample(0), ""))

    // when
    val response: CommandResponse<*> = handler.execute(ResetCommand())

    // then
    then(response.success()).isTrue
    then(projections.files.filesCount()).isEqualTo(0)
    then(projections.clusters.filesNotInClusters()).isEmpty()
    then(projections.clusters.clusters()).isEmpty()
  }

  companion object {
    const val FILES_DIRECTORY = "/tmp/ResetCommandTest"
  }
}
