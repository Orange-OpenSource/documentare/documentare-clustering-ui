package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.biz.appnotifications.AppNotificationsService
import com.orange.documentare.clusteringui.biz.file.command.ResetFileInfoCommandHandler
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.AllSingletonsNotLockedQueryHandler
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.ClusteringServiceImpl
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.DistanceSupplier
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.queries.QueryBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class FeedLoopMoveSingletonsToSortRoomTest {

  private lateinit var queryBus: QueryBus
  private lateinit var commandBus: CommandBus
  private lateinit var feedLoop: FeedLoop

  var eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  var projections = Projections(eventRepo)
  var eventBus = EventBus(eventRepo)

  @BeforeEach
  internal fun setup() {
    eventBus.register(projections)
    commandBus = CommandBus()
    commandBus.register(ResetFileInfoCommandHandler(eventRepo, eventBus))
    queryBus = QueryBus()
    queryBus.register(AllSingletonsNotLockedQueryHandler(projections))

    val takeFilesRandomly = { _: List<DomainFile>, _: Int -> emptyList<String>() }

    feedLoop = FeedLoop(
      queryBus, commandBus, { Parameters.defaults() }, ClusteringServiceImpl(
        DistanceSupplier().get(), AppNotificationsService(),
        EmbeddedComputationServer.disabled()
      ), takeFilesRandomly
    )
  }

  @Test
  fun `all singletons should be moved to sort room`() {
    // given
    fillDataBaseWith8Singletons()

    // when
    feedLoop.moveSingletonsNotLockedToSortRoom()

    // then
    then(filesInSortRoom()).hasSize(8)
  }

  private fun filesInSortRoom(): List<DomainFile> {
    return projections.files.ids()
      .map { id -> projections.files[id] }
      .filter { file -> !file.isInWaitingRoom() && !file.hasClusterId() }
  }

  private fun fillDataBaseWith8Singletons() {
    val events = mutableListOf<Event>()
    for (i in 0..7) {
      val id = aggregateIdExample(i)
      val clusterId = i.toString()
      val filename = "f$i"
      DomainFile.builder()
        .aggregateId(i.toString())
        .filename(filename)
        .clusterId(clusterId)
        .clusterCenter(true)
        .build()
      events.add(FileCreated(id, filename, clusterId, true))
    }
    eventRepo.insert(events)
    projections.testPurposeInitFromHistory(events)
  }
}
