package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.spy
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.AllFilesInWaitingRoomQuery
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQuery
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.ClusteringServiceImpl
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceService
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`

class FeederServiceOnProcessFinishedTest {

  private var distanceService = mock<DistanceService>()
  private var clusteringService = mock<ClusteringServiceImpl>()

  private var feederService = spy(FeederServiceImpl(distanceService, clusteringService))
  private var commandBus: CommandBus? = mock()
  private var queryBus: QueryBus? = mock()
  private var feedLoopMock: FeedLoop = mock()

  @BeforeEach
  fun setup() {
    feederService.commandBus = commandBus!!
    feederService.queryBus = queryBus!!
    `when`(clusteringService.isRunning()).thenReturn(false)
    `when`(distanceService.isRunning()).thenReturn(false)

    mockParameters()
    feederService.forceFeedLoopMockForUnitTestPurposeOnly(feedLoopMock)
  }

  @Test
  fun `on feeder process finished, then call listener `() {
    // given
    val filesInWaitingRoom = moreFilesThanTriggerThreshold()
    mockAllFilesInWaitingRoomQuery(filesInWaitingRoom)

    var onFeederProcessFinishedCalled = false
    var feederWasNoMoreRunning = false
    val onFeederProcessFinishedListener = object : FeederProcessFinished {
      override fun onFeederProcessFinished() {
        onFeederProcessFinishedCalled = true
        feederWasNoMoreRunning = !feederService.isRunning
      }
    }
    // when
    feederService.launch(onFeederProcessFinishedListener)

    var count = 0
    while (!onFeederProcessFinishedCalled && count < 500) {
      Thread.sleep(10)
      count++
    }

    // then
    then(onFeederProcessFinishedCalled).isTrue
    then(feederWasNoMoreRunning).isTrue
  }

  private fun mockAllFilesInWaitingRoomQuery(filesInWaitingRoom: List<DomainFile>) {
    val response = QueryResponse(TryOrCatch.success(filesInWaitingRoom))
    `when`<Any>(queryBus?.dispatch(AllFilesInWaitingRoomQuery())).thenReturn(response)
  }

  private fun moreFilesThanTriggerThreshold(): List<DomainFile> {
    return (0..TRIGGER_THRESHOLD * 2).map { DomainFile.builder().build() }
  }

  private fun mockParameters() {
    val response = QueryResponse(TryOrCatch.success(Parameters.builder().feed(true).feedThreshold(TRIGGER_THRESHOLD).minMultisetThreshold(2).maxMultisetThreshold(5).build()))
    `when`<Any>(queryBus?.dispatch(ParametersQuery())).thenReturn(response)
  }

  companion object {
    private const val TRIGGER_THRESHOLD = 5
  }
}
