package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.biz.file.command.RemoveSelectedFilesFromClustersCommandHandler
import com.orange.documentare.clusteringui.biz.file.domain.ClusterId
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.domain.FileId
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.AllFilesToRemoveInClusterQueryHandler
import com.orange.documentare.clusteringui.biz.file.query.MultiHypothesisClustersQueryHandler
import com.orange.documentare.clusteringui.biz.file.query.MultiHypothesisClustersView
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.MultiHypothesisClustersBroke
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.queries.QueryBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class BreakMultiHypothesisClustersTest {
  private val eventRepository = DBEventInMemoryRepositoryForTestPurpose()
  private val eventBus = EventBus(eventRepository)
  private val projections = Projections(eventRepository)
  private val queryBus = QueryBus()
  private val commandBus = CommandBus()

  @BeforeEach
  fun setup() {
    eventBus.register(projections)
    queryBus.register(MultiHypothesisClustersQueryHandler(projections), AllFilesToRemoveInClusterQueryHandler(projections))
    commandBus.register(RemoveSelectedFilesFromClustersCommandHandler(eventRepository, eventBus))
  }

  @Test
  fun `an error occurred while trying to retrieve multi-hypothesis clusters`() {
    // given
    val breakMultiHypothesisClusters = BreakMultiHypothesisClusters(QueryBus(), commandBus)

    // when
    val event: MultiHypothesisClustersBroke? = breakMultiHypothesisClusters.breakClusters()

    // then
    then(event).isNull()
  }

  @Test
  fun `an error occurred while trying to retrieve files to remove in cluster`() {
    // given
    MultiHypothesisClustersView.Examples.oneMultiHypothesisClusterWithTwoFilesInit(eventBus)
    val queryBus = QueryBus()
    queryBus.register(MultiHypothesisClustersQueryHandler(projections) /*, AllFilesToRemoveInClusterQueryHandler(projections)*/)
    val breakMultiHypothesisClusters = BreakMultiHypothesisClusters(queryBus, commandBus)

    // when
    val event: MultiHypothesisClustersBroke? = breakMultiHypothesisClusters.breakClusters()

    // then
    then(event).isNull()
  }

  @Test
  fun `an error occurred while trying to remove files from cluster`() {
    // given
    MultiHypothesisClustersView.Examples.oneMultiHypothesisClusterWithTwoFilesInit(eventBus)
    val commandBus = CommandBus()
    val breakMultiHypothesisClusters = BreakMultiHypothesisClusters(queryBus, commandBus)

    // when
    val event: MultiHypothesisClustersBroke? = breakMultiHypothesisClusters.breakClusters()

    // then
    then(event).isNull()
  }

  @Test
  fun `the number of multi-hypothesis clusters did change since last break`() {
    // given
    MultiHypothesisClustersView.Examples.oneMultiHypothesisClusterWithTwoFilesInit(eventBus)
    val breakMultiHypothesisClusters = BreakMultiHypothesisClusters(queryBus, commandBus)

    // when
    val event: MultiHypothesisClustersBroke? = breakMultiHypothesisClusters.breakClusters()

    // then
    then(event!!.metadataBalancingFinished).isFalse
  }

  @Test
  fun `the number of multi-hypothesis clusters did NOT change since last break`() {
    // given
    val breakMultiHypothesisClusters = BreakMultiHypothesisClusters(queryBus, commandBus)

    // when
    val event: MultiHypothesisClustersBroke? = breakMultiHypothesisClusters.breakClusters()

    // then
    then(event!!.metadataBalancingFinished).isTrue
  }

  @Test
  fun `all files in multi-hypothesis cluster are moved to waiting room if only the cluster center would be kept`() {
    // given
    MultiHypothesisClustersView.Examples.oneMultiHypothesisClusterWithTwoFilesInit(eventBus)
    val breakMultiHypothesisClusters = BreakMultiHypothesisClusters(queryBus, commandBus)

    // when
    val event: MultiHypothesisClustersBroke? = breakMultiHypothesisClusters.breakClusters()

    // then
    val filesIdToMoveToWaitingRoom = MultiHypothesisClustersView.Examples.oneMultiHypothesisClusterWithTwoFilesFilesIdToRemove()
    then(event).isEqualTo(MultiHypothesisClustersBroke(filesIdToMoveToWaitingRoom, false))
    checkFilesWereMovedToWaitingRoom(filesIdToMoveToWaitingRoom)
  }

  @Test
  fun `remove only file whose class name is distinct from cluster center and if another file would be kept`() {
    // given
    MultiHypothesisClustersView.Examples.multiHypothesisClusterWithOnlyOneDistinctFileInit(eventBus)
    val breakMultiHypothesisClusters = BreakMultiHypothesisClusters(queryBus, commandBus)

    // when
    val event: MultiHypothesisClustersBroke? = breakMultiHypothesisClusters.breakClusters()

    // then
    val filesIdToMoveToWaitingRoom = MultiHypothesisClustersView.Examples.multiHypothesisClusterWithOnlyOneDistinctFileFilesIdToRemove()
    then(event).isEqualTo(MultiHypothesisClustersBroke(filesIdToMoveToWaitingRoom, false))
    checkFilesWereMovedToWaitingRoom(filesIdToMoveToWaitingRoom)
  }

  @Test
  fun `do not move files to waiting room if metadata balancing is declared finished`() {
    // given
    MultiHypothesisClustersView.Examples.oneMultiHypothesisClusterWithTwoFilesInit(eventBus)
    val breakMultiHypothesisClusters = BreakMultiHypothesisClusters(queryBus, commandBus)
    breakMultiHypothesisClusters.testPurposeUpdatePreviousMultiHypothesisClustersMap(MultiHypothesisClustersView.Examples.oneMultiHypothesisClusterWithTwoFilesMap())

    // when
    val event: MultiHypothesisClustersBroke? = breakMultiHypothesisClusters.breakClusters()

    // then
    then(event).isEqualTo(MultiHypothesisClustersBroke(emptyList(), true))
  }

  @Test
  fun `multiset balancing is finished if number of files in multi hypothesis clusters has not changed`() {
    // given
    val f1 = DomainFile.builder().aggregateId(aggregateIdExample(1)).build()
    val f2 = DomainFile.builder().aggregateId(aggregateIdExample(2)).build()

    // when
    val metadataBalancingFinished = BreakMultiHypothesisClusters.isMetadataBalancingFinished(
      mapOf(
        Pair(ClusterId("titi"), listOf(f1, f2))
      ),
      mapOf(
        Pair(ClusterId("toto"), listOf(f2, f1))
      )
    )

    // then
    then(metadataBalancingFinished).isTrue
  }

  @Test
  fun `multiset balancing is NOT finished if number of files in multi hypothesis clusters has changed`() {
    // given
    val f1 = DomainFile.builder().aggregateId(aggregateIdExample(1)).build()
    val f2 = DomainFile.builder().aggregateId(aggregateIdExample(2)).build()

    // when
    val metadataBalancingFinished = BreakMultiHypothesisClusters.isMetadataBalancingFinished(
      mapOf(
        Pair(ClusterId("titi"), listOf(f1, f2))
      ),
      mapOf(
        Pair(ClusterId("toto"), listOf(f2))
      )
    )

    // then
    then(metadataBalancingFinished).isFalse
  }

  private fun checkFilesWereMovedToWaitingRoom(filesIdToMoveToWaitingRoom: List<FileId>) {
    filesIdToMoveToWaitingRoom.forEach {
      val domainFile = projections.files.currentHierarchyLevel()[it.fileId]!!
      then(domainFile.clusterId).isNull()
      then(domainFile.inWaitingRoom).isTrue
    }
  }
}

