package com.orange.documentare.clusteringui.biz.file.hierarchyvisu

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper

object TreantExample {

  fun serialize(treantHierarchyDTO: TreantHierarchyDTO) = ObjectMapper()
    .setSerializationInclusion(JsonInclude.Include.NON_NULL)
    .writerWithDefaultPrettyPrinter()
    .writeValueAsString(treantHierarchyDTO)

  fun dto(): TreantHierarchyDTO {
    return TreantHierarchyDTO(
      text = TreantText("Root"),
      children = listOf(
        TreantNode(TreantText("elephant"), listOf(TreantNode(TreantText("elephant")))),
        TreantNode(TreantText("opossum"))
      )
    )
  }

  const val JSON_EXAMPLE = """{
  "text" : {
    "name" : "Root"
  },
  "children" : [ {
    "text" : {
      "name" : "elephant"
    },
    "children" : [ {
      "text" : {
        "name" : "elephant"
      }
    } ]
  }, {
    "text" : {
      "name" : "opossum"
    }
  } ]
}"""

}

