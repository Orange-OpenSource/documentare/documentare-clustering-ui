package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.queries.QueryResponse
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class AllFilesToRemoveInClusterQueryTest {
  var projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  var handler = AllFilesToRemoveInClusterQueryHandler(projections)

  @Test
  fun return_two_clusters_when_remove_two_files_from_two_distinct_clusters() {
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "0", "10", true),
      FileCreated(aggregateIdExample(1), "1", "10", null),
      FileCreated(aggregateIdExample(2), "2", "10", false),
      FileCreated(aggregateIdExample(3), "3", "10", false),
      FileCreated(aggregateIdExample(4), "4", "10", false),
      FileCreated(aggregateIdExample(5), "5", "20", true),
      FileCreated(aggregateIdExample(6), "6", "20", null),
      FileCreated(aggregateIdExample(7), "7", "20", false),
      FileCreated(aggregateIdExample(8), "8", "20", false)
    )
    projections.testPurposeInitFromHistory(events)

    // when
    val response: QueryResponse<Map<String, List<DomainFile>>> = handler.execute(AllFilesToRemoveInClusterQuery(listOf(aggregateIdExample(1), aggregateIdExample(6))))

    // then
    then(response.success()).isTrue
    then(response.value()).hasSize(2)
  }

  @Test
  fun return_one_cluster_when_remove_cluster_center() {
    // given
    val events: Iterable<Event> = listOf<Event>(
      FileCreated(aggregateIdExample(0), "0", "10", true),
      FileCreated(aggregateIdExample(1), "1", "10", null)
    )
    projections.testPurposeInitFromHistory(events)


    // when
    val response: QueryResponse<Map<String, List<DomainFile>>> = handler.execute(AllFilesToRemoveInClusterQuery(listOf(aggregateIdExample(0))))


    // then
    then(response.success()).isTrue()
    then(response.value()).hasSize(1)
  }
}
