package com.orange.documentare.clusteringui.biz.file.query.projection

import com.orange.documentare.clusteringui.biz.file.event.ClusterPutInNamedClass
import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.event.MultisetElementMovedToNextHierarchyLevel
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.AGGREGATE_ID_EXAMPLE
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

internal class ProjectionsTest {

  @Test
  fun `init projections from eventRepository`() {
    // given
    val eventRepo = DBEventInMemoryRepositoryForTestPurpose()
    eventRepo.insert(
      listOf(
        FileCreated(aggregateIdExample(0), "f0"),
        FileCreated(aggregateIdExample(1), "f1"),
        FileCreated(aggregateIdExample(2), "f2"),
        FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("10").build(),
        FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("10").clusterCenter(true).build(),
        FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("10").build(),

        FileCreated(aggregateIdExample(3), "f3", "11", false),
        FileCreated(aggregateIdExample(4), "f4", "11", true),
        FileCreated(aggregateIdExample(5), "f5", "11", false),

        FileCreated(aggregateIdExample(6), "f6"),
        FileCreated(aggregateIdExample(7), "f7"),

        ClusterPutInNamedClass(aggregateIdExample(4), "11", "titi")
      )
    )

    // when
    val projections = Projections(eventRepo)

    // then
    then(projections.files.filesCount()).isEqualTo(8)
    then(projections.files.ids()).containsExactlyInAnyOrder(
      aggregateIdExample(0),
      aggregateIdExample(1),
      aggregateIdExample(2),
      aggregateIdExample(3),
      aggregateIdExample(4),
      aggregateIdExample(5),
      aggregateIdExample(6),
      aggregateIdExample(7)
    )
    then(projections.clusters.clusters().size).isEqualTo(2)
    then(projections.clusters.clusters()).isEqualTo(
      mapOf(
        Pair("10", listOf(aggregateIdExample(1), aggregateIdExample(0), aggregateIdExample(2))),
        Pair("11", listOf(aggregateIdExample(4), aggregateIdExample(3), aggregateIdExample(5)))
      )
    )
    then(projections.clusters.classes().size).isEqualTo(1)
  }

  @Test
  fun `init projections with hierarchies`() {
    // given
    val eventRepo = DBEventInMemoryRepositoryForTestPurpose()
    eventRepo.insert(
      listOf(
        FileCreated(aggregateIdExample(0), "f0"),
        FileCreated(aggregateIdExample(1), "f1"),
        FileCreated(aggregateIdExample(2), "f2"),
        FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("10").build(),
        FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("10").clusterCenter(true).build(),
        FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("10").build(),

        FileCreated(aggregateIdExample(3), "f3", "11", false),
        FileCreated(aggregateIdExample(4), "f4", "11", true),
        FileCreated(aggregateIdExample(5), "f5", "11", false),

        MultisetElementMovedToNextHierarchyLevel(
          aggregateIdExample(1),
          1
        ),
        MultisetElementMovedToNextHierarchyLevel(
          aggregateIdExample(4),
          1
        ),

        FileAddedToCluster.builder().aggregateId(aggregateIdExample(4)).clusterId("20").build(),

        ClusterPutInNamedClass(aggregateIdExample(4), "20", "titi")
      )
    )

    // when
    val projections = Projections(eventRepo)

    // then
    then(projections.files.filesCount()).isEqualTo(2)
    then(projections.files.ids()).containsExactlyInAnyOrder(aggregateIdExample(1), aggregateIdExample(4))
    then(projections.clusters.clusters().size).isEqualTo(1)
    then(projections.clusters.clusters()).isEqualTo(
      mapOf(
        Pair("20", listOf(aggregateIdExample(4)))
      )
    )
    then(projections.clusters.classes().size).isEqualTo(1)

    val filesHierarchyLevel = projections.files.hierarchyLevel(0)
    val clustersHierarchyLevel = projections.clusters.hierarchyLevel(0)
    then(filesHierarchyLevel.filesCount()).isEqualTo(6)
    then(filesHierarchyLevel.ids()).containsExactlyInAnyOrder(
      aggregateIdExample(0),
      aggregateIdExample(1),
      aggregateIdExample(2),
      aggregateIdExample(3),
      aggregateIdExample(4),
      aggregateIdExample(5)
    )
    then(clustersHierarchyLevel.clusters().size).isEqualTo(2)
    then(clustersHierarchyLevel.clusters()).isEqualTo(
      mapOf(
        Pair("10", listOf(aggregateIdExample(1), aggregateIdExample(0), aggregateIdExample(2))),
        Pair("11", listOf(aggregateIdExample(4), aggregateIdExample(3), aggregateIdExample(5)))
      )
    )
    then(clustersHierarchyLevel.classes().size).isEqualTo(0)
  }

  @Test
  fun `after a hierarchy reset, we should keep only initial first hierarchy level`() {
    // given
    val eventRepo = DBEventInMemoryRepositoryForTestPurpose()
    val aggregateId = AGGREGATE_ID_EXAMPLE.toString()
    eventRepo.insert(
      listOf(
        FileCreated(aggregateId, "f1"),
        FileAddedToCluster.builder().aggregateId(aggregateId).clusterId("10").build()
      )
    )

    val projections = Projections(eventRepo)
    then(projections.files.ids()).isEqualTo(listOf(aggregateId))
    then(projections.clusters.clusters().containsKey("10")).isTrue


    simulateEventToCreateANewHierarchy(projections, aggregateId)
    then(projections.hierarchyLevelCount()).isEqualTo(2)

    // when
    projections.keepOnlyFirstHierarchyLevel()

    // then
    then(projections.hierarchyLevelCount()).isEqualTo(1)
    then(projections.files.ids()).isEqualTo(listOf(aggregateId))
    then(projections.clusters.clusters().containsKey("10")).isTrue
  }

  private fun simulateEventToCreateANewHierarchy(projections: Projections, aggregateId: String) {
    projections.onEvent(
      MultisetElementMovedToNextHierarchyLevel(
        aggregateId,
        1
      )
    )
  }
}
