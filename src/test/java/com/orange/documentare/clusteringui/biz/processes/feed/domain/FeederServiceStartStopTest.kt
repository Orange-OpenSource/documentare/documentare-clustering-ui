package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.spy
import com.nhaarman.mockitokotlin2.verify
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.parameters.command.UpdateParametersCommand
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQuery
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeedLoopStarted
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.ClusteringServiceImpl
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceService
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`

class FeederServiceStartStopTest {

  private val distanceService: DistanceService = mock()
  private val clusteringService: ClusteringServiceImpl = mock()
  private val feederService = spy(FeederServiceImpl(distanceService, clusteringService))
  private var queryBus: QueryBus = mock()
  private var commandBus: CommandBus = mock()
  private var feedLoopMock: FeedLoop = mock()

  @BeforeEach
  fun setup() {
    feederService.queryBus = queryBus
    feederService.commandBus = commandBus
    mockParameters()

    feederService.forceFeedLoopMockForUnitTestPurposeOnly(feedLoopMock)
  }

  @Test
  fun `check initial state`() {
    checkInitialState()
  }

  private fun checkInitialState() {
    then(feederService.isRunning).isFalse
    then(feederService.stopFeederWhenPossible).isFalse
    then(feederService.getThenClearDataUpdated()).isFalse
  }

  @Test
  fun `feed loop is launched, is running, ends and is no more running`() {
    // when
    val optionalTask = feederService.tryToLaunchFeeder(false)
    then(optionalTask).isNotNull
    optionalTask?.join()

    // then
    verify(feedLoopMock).loop()
    checkInitialState()
  }

  @Test
  fun `feed loop is launched, and receives New File Created event`() {
    // given
    `when`(feedLoopMock.loop()).then {
      feederService.onEvent(FileCreated.example())
    }

    // when
    feederService.tryToLaunchFeeder(false)?.join()

    // then
    verify(feedLoopMock).onNewFileCreated()
  }

  @Test
  fun `feed loop is launched, and receives On New Feed Loop event`() {
    // given
    `when`(feedLoopMock.loop()).then {
      feederService.onFeedLoopEvent(FeedLoopStarted(0))
    }

    // when
    feederService.tryToLaunchFeeder(false)?.join()

    // then
    then(feederService.getThenClearDataUpdated()).isTrue
  }

  @Test
  fun `feed loop throws an Out of Memory Error, and we go back to initial state`() {
    // given
    `when`(feedLoopMock.loop()).then {
      throw OutOfMemoryError("out of memory")
    }

    // when
    feederService.tryToLaunchFeeder(false)?.join()

    // then
    checkInitialState()
  }

  @Test
  fun `feed loop is launched, and receives Stop Feeder event, and disable Stop Feed parameter`() {
    // given
    `when`(feedLoopMock.loop()).then {
      feederService.stopFeederWhenPossible()
    }

    val parameters = mockParameters()
    val updatedParametersCommand = UpdateParametersCommand.initBuilderWith(parameters)
      .feed(false)
      .build()
    val responseUpdate = CommandResponse(TryOrCatch.success(null))
    `when`<Any>(commandBus.dispatch<CommandResponse<UpdateParametersCommand>>(updatedParametersCommand)).thenReturn(responseUpdate)

    // when
    feederService.tryToLaunchFeeder(false)?.join()

    // then
    verify(feedLoopMock).onStopFeederWhenPossible()
    verify(commandBus).dispatch<CommandResponse<UpdateParametersCommand>>(updatedParametersCommand)
  }

  private fun mockParameters(): Parameters {
    val parameters = Parameters.builder().feed(true).feedThreshold(10).minMultisetThreshold(2).maxMultisetThreshold(5).build()
    val responseQuery = QueryResponse(TryOrCatch.success(parameters))
    `when`<Any>(queryBus.dispatch(ParametersQuery())).thenReturn(responseQuery)
    return parameters
  }
}
