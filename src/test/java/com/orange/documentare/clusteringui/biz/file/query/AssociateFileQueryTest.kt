package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster
import com.orange.documentare.clusteringui.biz.file.event.DistancesToClusterCenterAdded
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.event.FileDuplicated
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.Event
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class AssociateFileQueryTest {
  var projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  var handler = AssociateFileQueryHandler(projections)

  @Test
  fun build_association_file_view() {
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "f0", "1", true),
      FileCreated(aggregateIdExample(1), "f1", "1", false),
      FileCreated(aggregateIdExample(2), "titi"),
      DistancesToClusterCenterAdded(aggregateIdExample(2), listOf(DistanceToCluster(1000, "1")))
    )
    projections.testPurposeInitFromHistory(events)
    val query = AssociateFileQuery(aggregateIdExample(2))

    // when
    val response = handler.execute(query)

    // then
    then(response.success()).isTrue()
    val view = response.value()
    then(view.fileUIView.filename).isEqualTo("titi")
    then(view.clustersCenterViews!!.keys.hashCode()).isEqualTo(49)
    then(view.clustersCenterViews!!.keys).hasSize(1)
  }

  @Test
  fun build_association_file_view_without_duplicates() {
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "f0", "1", true),
      FileCreated(aggregateIdExample(1), "f1", "1", false),
      FileCreated(aggregateIdExample(2), "titi"),
      DistancesToClusterCenterAdded(aggregateIdExample(2), listOf(DistanceToCluster(1000, "1"))),
      FileDuplicated(aggregateIdExample(1), null)
    )
    projections.testPurposeInitFromHistory(events)
    val query = AssociateFileQuery(aggregateIdExample(2))

    // when
    val response = handler.execute(query)

    // then
    then(response.success()).isTrue()
    val view = response.value()
    then(view.fileUIView.filename).isEqualTo("titi")
    then(view.clustersCenterViews!!.keys.hashCode()).isEqualTo(49)
    then(view.clustersCenterViews!!.keys).hasSize(1)
  }

  @Test
  fun build_association_file_view_without_nearest_cluster() {
    // given
    val events: Iterable<Event> = setOf(
      FileCreated(aggregateIdExample(0), "f0")
    )
    projections.testPurposeInitFromHistory(events)
    val query = AssociateFileQuery(aggregateIdExample(0))

    // when
    val response = handler.execute(query)

    // then
    then(response.success()).isTrue()
    val view = response.value()
    then(view.clustersCenterViews).isNull()
  }
}
