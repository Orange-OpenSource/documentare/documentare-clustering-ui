package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.event.MultisetElementMovedToNextHierarchyLevel
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.Event
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class ClusterFilesQueryTest {
  var projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  var handler = ClusterFilesQueryHandler(projections)

  @Test
  fun `retrieve all files in cluster at level 0`() {
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "0", "1", true),
      FileCreated(aggregateIdExample(1), "1", "1", false),
      FileCreated(aggregateIdExample(2), "2", "1", false)
    )
    projections.testPurposeInitFromHistory(events)

    // when
    val response = handler.execute(ClusterFilesQuery("1"))

    // then
    then(response.success()).isTrue
    then(response.value()).hasSize(3)
  }

  @Test
  fun `retrieve all files in cluster at level 0 or level 1`() {
    // given
    val events: Iterable<Event> = listOf(
      //
      // Level 0
      //

      // Cluster 1
      FileCreated(aggregateIdExample(0), "0", "level0-cluster-1", true),
      FileCreated(aggregateIdExample(1), "1", "level0-cluster-1", false),
      FileCreated(aggregateIdExample(2), "2", "level0-cluster-1", false),
      // Cluster 2
      FileCreated(aggregateIdExample(3), "3", "level0-cluster-2", true),
      FileCreated(aggregateIdExample(4), "4", "level0-cluster-2", false),
      FileCreated(aggregateIdExample(5), "5", "level0-cluster-2", false),

      //
      // Level 1
      //
      // Move files to second hierarchy level
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(0), 1),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(3), 1),
      // Cluster 11
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("level1-cluster-11").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("level1-cluster-11").build(),
    )
    projections.testPurposeInitFromHistory(events)

    // when
    val responseLevel0 = handler.execute(ClusterFilesQuery("level0-cluster-1", 0))
    val responseLevel1 = handler.execute(ClusterFilesQuery("level1-cluster-11", 1))

    // then
    then(responseLevel0.success()).isTrue
    then(responseLevel0.value().map { it.aggregateId }).containsExactly(aggregateIdExample(0), aggregateIdExample(1), aggregateIdExample(2))
    then(responseLevel1.success()).isTrue
    then(responseLevel1.value().map { it.aggregateId }).containsExactly(aggregateIdExample(0), aggregateIdExample(3))
  }
}
