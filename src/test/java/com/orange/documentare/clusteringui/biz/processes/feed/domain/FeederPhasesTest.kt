package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederPhases.Phase.*
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederPhases.UpdateType
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeederPhaseUpdated
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

internal class FeederPhasesTest {

  @Test
  fun `initial phase is IDLE, metadataBalancingStep is not initialised`() {
    // when
    val phases = FeederPhases.idle()

    // then
    then(phases.current()).isEqualTo(IDLE)
    then(phases.metadataBalancingStep()).isNull()
  }

  @Test
  fun `move to INCREMENTAL_CLUSTERING phase if there are files in the waiting room, coming from IDLE phase`() {
    // given
    val filesInWaitingRoom = filesPresentInWaitingRoom()
    val phases = FeederPhases.idle()

    // when
    val updateEvent = phases.updateAtFeedLoopStart(filesInWaitingRoom, multisetBalanceNotFinished())

    // then
    then(phases.current()).isEqualTo(INCREMENTAL_CLUSTERING)
    then(updateEvent?.phase).isEqualTo(phases.current())
  }

  @Test
  fun `in INCREMENTAL_CLUSTERING phase, no update after loop initialisation`() {
    // given
    val phases = FeederPhases.testPurposeCreateWithPhase(INCREMENTAL_CLUSTERING)

    // when
    val updateEvent = phases.updateAfterFeedLoopInitialisation(metadataBalancingFinished = true)

    // then
    then(phases.current()).isEqualTo(INCREMENTAL_CLUSTERING)
    then(phases.metadataBalancingStep()).isNull()
    then(updateEvent).isNull()
  }

  @Test
  fun `stay in IDLE phase if the waiting room is empty`() {
    // given
    val filesInWaitingRoom = noFilesInWaitingRoom()
    val phases = FeederPhases.idle()

    // when
    val updateEvent = phases.updateAtFeedLoopStart(filesInWaitingRoom, multisetBalanceNotFinished())

    // then
    then(phases.current()).isEqualTo(IDLE)
    then(updateEvent).isNull()
  }

  @Test
  fun `move to MULTISET_BALANCING phase if we were in INCREMENTAL_CLUSTERING phase and waiting room is empty`() {
    // given
    val noFilesInWaitingRoom = noFilesInWaitingRoom()
    val phases = FeederPhases.testPurposeCreateWithPhase(INCREMENTAL_CLUSTERING)

    // when
    val updateEvent = phases.updateBeforeLoopFinalisation(noFilesInWaitingRoom)

    // then
    then(phases.current()).isEqualTo(MULTISET_BALANCING)
    then(updateEvent?.phase).isEqualTo(phases.current())
  }

  @Test
  fun `in MULTISET_BALANCING phase, no update after loop initialisation`() {
    // given
    val phases = FeederPhases.testPurposeCreateWithPhase(MULTISET_BALANCING)

    // when
    val updateEvent = phases.updateAfterFeedLoopInitialisation(metadataBalancingFinished = true)

    // then
    then(phases.current()).isEqualTo(MULTISET_BALANCING)
    then(phases.metadataBalancingStep()).isNull()
    then(updateEvent).isNull()
  }

  @Test
  fun `stay in INCREMENTAL_CLUSTERING phase if waiting room is NOT empty`() {
    // given
    val filesInWaitingRoom = filesPresentInWaitingRoom()
    val phases = FeederPhases.testPurposeCreateWithPhase(INCREMENTAL_CLUSTERING)

    // when
    val updateEvent = phases.updateBeforeLoopFinalisation(filesInWaitingRoom)

    // then
    then(phases.current()).isEqualTo(INCREMENTAL_CLUSTERING)
    then(updateEvent).isNull()
  }

  @Test
  fun `stay in MULTISET_BALANCING phase if waiting room is NOT empty`() {
    // given
    val filesInWaitingRoom = filesPresentInWaitingRoom()
    val phases = FeederPhases.testPurposeCreateWithPhase(MULTISET_BALANCING)

    // when
    val updateEvent = phases.updateAtFeedLoopStart(filesInWaitingRoom, multisetBalanceNotFinished())

    // then
    then(phases.current()).isEqualTo(MULTISET_BALANCING)
    then(updateEvent).isNull()
  }

  @Test
  fun `stay in MULTISET_BALANCING phase if multiset min threshold has not been reached`() {
    // given
    val phases = FeederPhases.testPurposeCreateWithPhase(MULTISET_BALANCING)

    // when
    val updateEvent = phases.updateAtFeedLoopStart(noFilesInWaitingRoom(), multisetBalanceNotFinished())

    // then
    then(phases.current()).isEqualTo(MULTISET_BALANCING)
    then(updateEvent).isNull()
  }

  @Test
  fun `stay in MULTISET_BALANCING phase if it is not finished`() {
    // given
    val phases = FeederPhases.testPurposeCreateWithPhase(MULTISET_BALANCING)

    // when
    val updateEvent = phases.updateAtFeedLoopStart(filesPresentInWaitingRoom(), multisetBalanceNotFinished())

    // then
    then(phases.current()).isEqualTo(MULTISET_BALANCING)
    then(updateEvent).isNull()
  }

  // Even if there is no metadata, indeed: we will move directly to IDLE at the next update since there is no multi-hypothesis cluster
  @Test
  fun `move to METADATA_BALANCING phase, substate is BREAK_CLUSTERS if singletons did NOT changed since last loop AND waiting room is empty AND multiset min threshold was reached`() {
    // given
    val phases = FeederPhases.testPurposeCreateWithPhase(MULTISET_BALANCING)

    // when
    val updateEvent = phases.updateAtFeedLoopStart(noFilesInWaitingRoom(), multisetBalanceFinished())

    // then
    then(phases.current()).isEqualTo(METADATA_BALANCING)
    then(phases.metadataBalancingStep()).isEqualTo(MetadataBalancingStep.BREAK_CLUSTERS)
    then(updateEvent).isEqualTo(FeederPhaseUpdated(UpdateType.AT_FEED_LOOP_START, METADATA_BALANCING, MetadataBalancingStep.BREAK_CLUSTERS))
  }


  @Test
  fun `move to METADATA_BALANCING phase if MULTISET_BALANCING is finished and metadata are provided, at loop start, substate is BREAK_CLUSTERS`() {
    // given
    val phases = FeederPhases.testPurposeCreateWithPhase(MULTISET_BALANCING)

    // when
    val updateEvent = phases.updateAtFeedLoopStart(noFilesInWaitingRoom(), multisetBalanceFinished())

    // then
    then(phases.current()).isEqualTo(METADATA_BALANCING)
    then(phases.metadataBalancingStep()).isEqualTo(MetadataBalancingStep.BREAK_CLUSTERS)
    then(updateEvent).isEqualTo(FeederPhaseUpdated(UpdateType.AT_FEED_LOOP_START, METADATA_BALANCING, MetadataBalancingStep.BREAK_CLUSTERS))
  }

  @Test
  fun `in METADATA_BALANCING move to substate INCREMENTAL_CLUSTERING after we broke clusters in loop initialisation, multi-hypothesis clusters changed`() {
    // given
    val phases = FeederPhases.testPurposeCreateWithPhase(METADATA_BALANCING, MetadataBalancingStep.BREAK_CLUSTERS)

    // when
    val updateEvent = phases.updateAfterFeedLoopInitialisation(metadataBalancingFinished = false)

    // then
    then(phases.current()).isEqualTo(METADATA_BALANCING)
    then(phases.metadataBalancingStep()).isEqualTo(MetadataBalancingStep.INCREMENTAL_CLUSTERING)
    then(updateEvent).isEqualTo(FeederPhaseUpdated(UpdateType.AFTER_FEED_LOOP_INITIALISATION, METADATA_BALANCING, MetadataBalancingStep.INCREMENTAL_CLUSTERING))
  }

  @Test
  fun `in METADATA_BALANCING move to IDLE after we broke clusters in loop initialisation, multi-hypothesis clusters did NOT changed`() {
    // given
    val phases = FeederPhases.testPurposeCreateWithPhase(METADATA_BALANCING, MetadataBalancingStep.BREAK_CLUSTERS)

    // when
    val updateEvent = phases.updateAfterFeedLoopInitialisation(metadataBalancingFinished = true)

    // then
    then(phases.current()).isEqualTo(IDLE)
    then(updateEvent).isEqualTo(FeederPhaseUpdated(UpdateType.AFTER_FEED_LOOP_INITIALISATION, IDLE))
  }

  @Test
  fun `in METADATA_BALANCING move to substate MULTISET_BALANCING after incremental clustering operation, if waiting room is empty`() {
    // given
    val phases = FeederPhases.testPurposeCreateWithPhase(METADATA_BALANCING, MetadataBalancingStep.INCREMENTAL_CLUSTERING)

    // when
    val updateEvent = phases.updateBeforeLoopFinalisation(noFilesInWaitingRoom())

    // then
    then(phases.current()).isEqualTo(METADATA_BALANCING)
    then(phases.metadataBalancingStep()).isEqualTo(MetadataBalancingStep.MULTISET_BALANCING)
    then(updateEvent).isEqualTo(FeederPhaseUpdated(UpdateType.BEFORE_LOOP_FINALISATION, METADATA_BALANCING, MetadataBalancingStep.MULTISET_BALANCING))
  }

  @Test
  fun `in METADATA_BALANCING move to substate BREAK_CLUSTERS if step MULTISET_BALANCING is finished`() {
    // given
    val phases = FeederPhases.testPurposeCreateWithPhase(METADATA_BALANCING, MetadataBalancingStep.MULTISET_BALANCING)

    // when
    val updateEvent = phases.updateAtFeedLoopStart(noFilesInWaitingRoom(), multisetBalanceFinished())

    // then
    then(phases.current()).isEqualTo(METADATA_BALANCING)
    then(phases.metadataBalancingStep()).isEqualTo(MetadataBalancingStep.BREAK_CLUSTERS)
    then(updateEvent).isEqualTo(FeederPhaseUpdated(UpdateType.AT_FEED_LOOP_START, METADATA_BALANCING, MetadataBalancingStep.BREAK_CLUSTERS))
  }

  private fun multisetBalanceNotFinished() = object : MultisetBalance {
    override fun isFinished() = false
  }

  private fun multisetBalanceFinished() = object : MultisetBalance {
    override fun isFinished() = true
  }

  private fun noFilesInWaitingRoom(): List<DomainFile> = emptyList()

  private fun filesPresentInWaitingRoom(): List<DomainFile> = listOf(DomainFile.Examples.addedFile())
}
