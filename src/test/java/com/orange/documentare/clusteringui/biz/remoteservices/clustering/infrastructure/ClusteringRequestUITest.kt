package com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure

import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

internal class ClusteringRequestUITest {
  @Test
  fun `transform clustering request UI to computation server clustering request`() {
    // given
    val pCount = 100
    val acutSdFactor = 1.0f
    val qcutSdFactor = 1.1f
    val scutSdFactor = 1.2f
    val ccutPercentile = 80
    val knnThreshold = 6
    val enrollAcutSdFactor = 2.0f
    val enrollQcutSdFactor = 2.1f
    val enrollScutSdFactor = 2.2f
    val enrollCcutPercentile = 90
    val enrollKnnThreshold = 7
    val suffixArrayAlgorithm = SuffixArrayAlgorithm.LIBDIVSUFSORT
    val rawConverter = true
    val outputDirectory = "/tmp/output/"

    val clusteringRequestUI = ClusteringRequestUI.builder()
      .bytesData(emptyArray())
      .outputDirectory(outputDirectory)
      .rawConverter(rawConverter)
      .pCount(pCount)
      .acut(acutSdFactor)
      .qcut(qcutSdFactor)
      .scut(scutSdFactor)
      .ccut(ccutPercentile)
      .knnThreshold(knnThreshold)
      .sloop()
      .consolidateCluster()
      .enroll()
      .enrollAcut(enrollAcutSdFactor)
      .enrollQcut(enrollQcutSdFactor)
      .enrollScut(enrollScutSdFactor)
      .enrollCcut(enrollCcutPercentile)
      .enrollK(enrollKnnThreshold)
      .suffixArrayAlgorithm(suffixArrayAlgorithm)
      .build()

    // when
    val clusteringRequest: ClusteringRequest = clusteringRequestUI.toComputationServerClusteringRequest()

    // then
    val expectedClusteringRequest = ClusteringRequest.builder()
      .bytesData(emptyArray())
      .outputDirectory(outputDirectory)
      .rawConverter(rawConverter)
      .pCount(pCount)
      .acutSdFactor(acutSdFactor)
      .qcutSdFactor(qcutSdFactor)
      .scutSdFactor(scutSdFactor)
      .ccutPercentile(ccutPercentile)
      .knnThreshold(knnThreshold)
      .sloop(clusteringRequestUI.sloop)
      .consolidateCluster(clusteringRequestUI.consolidateCluster)
      .enroll(clusteringRequestUI.enroll)
      .enrollAcutSdFactor(enrollAcutSdFactor)
      .enrollQcutSdFactor(enrollQcutSdFactor)
      .enrollScutSdFactor(enrollScutSdFactor)
      .enrollCcutPercentile(enrollCcutPercentile)
      .enrollKnnThreshold(enrollKnnThreshold)
      .suffixArrayAlgorithm(suffixArrayAlgorithm)
      .build()

    then(clusteringRequest).isEqualTo(expectedClusteringRequest)
  }

}
