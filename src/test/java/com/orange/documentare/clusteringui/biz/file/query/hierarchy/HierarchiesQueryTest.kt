package com.orange.documentare.clusteringui.biz.file.query.hierarchy

import com.orange.documentare.clusteringui.biz.file.query.HierarchiesQuery
import com.orange.documentare.clusteringui.biz.file.query.HierarchiesQueryHandler
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class HierarchiesQueryTest {

  @Test
  fun `retrieve hierarchies with clusters`() {
    // given
    val projection = HierarchyExamples.projectionWith4LevelsOneRoot()
    val handler = HierarchiesQueryHandler(projection)

    // when
    val response = handler.execute(HierarchiesQuery())

    // then
    then(response.success()).isTrue
    then(response.value().hierarchies).isEqualTo(HierarchyExamples.hierarchiesWith4LevelsOneRoot())
  }
}
