package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.*
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.infrastructure.event.EventBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Command DuplicateFiles")
class DuplicateFilesCommandTest {
  /**
   * init eventBus with event repository so that events will be persisted automatically
   */
  var eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  var eventBus = EventBus(eventRepo)
  var handler = DuplicateFilesCommandHandler(eventRepo, eventBus)

  @Test
  fun a_duplicate_file_command_generates_a_FileDuplicated_event() {
    // given
    val file = DomainFile.builder()
      .aggregateId(aggregateIdExample(0))
      .filename("f0")
      .clusterId("10")
      .distancesToClustersCenters(emptyList())
      .distanceToCenter(100)
      .clusterModified(true)
      .automaticInclude(true)
      .build()
    val initialEvents = mutableListOf(
      FileCreated(aggregateIdExample(0), "f0"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("10").distanceToCenter(100).automaticInclude(true).build(),
      DistancesToClusterCenterAdded(aggregateIdExample(0), emptyList()),
      ClusterModified(aggregateIdExample(0), "10")
    )
    eventRepo.insert(initialEvents)
    val cmd = DuplicateFilesCommand(listOf(file.aggregateId))

    // when
    val response: CommandResponse<*> = handler.execute(cmd)

    // then
    then(response.success()).isTrue()
    initialEvents.apply {
      this.add(FileDuplicated(file.aggregateId, "10"))
      then(eventRepo.findAll()).containsAll(this)
    }
  }
}
