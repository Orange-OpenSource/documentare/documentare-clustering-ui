package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

class UUIDSupplier {
  companion object {
    const val FIRST_UUID = 0
  }

  private var numId: Int = FIRST_UUID

  fun getAsString(): String = "" + get()
  fun get(): Int = numId++
}
