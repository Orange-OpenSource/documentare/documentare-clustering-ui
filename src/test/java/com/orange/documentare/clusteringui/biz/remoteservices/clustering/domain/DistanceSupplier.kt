package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.AggregateId
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceService

internal class DistanceSupplier {

  var computeDistancesOfInSameThread: ((parameters: Parameters, fileIds: List<String>) -> Unit)? = null
  var computeDistancesOf: ((parameters: Parameters, fileIds: List<String>) -> Unit)? = null

  fun get(): DistanceService {
    return object : DistanceService {

      override fun isDistanceBeingComputedFor(id: String) = false

      override fun isRunning() = true

      override fun computeDistancesOf(parameters: Parameters, fileIds: List<String>) {
        computeDistancesOf?.let {
          it(parameters, fileIds)
        }
      }

      override fun computeDistancesOfInSameThread(parameters: Parameters, fileIds: List<String>) {
        computeDistancesOfInSameThread?.let {
          it(parameters, fileIds)
        }
      }

      override fun computeDistancesToCentroidAnywhereInHierarchyInSameThread(files: List<AggregateId>, centroidMultisetFiles: List<AggregateId>): List<Pair<AggregateId, Int>> {
        TODO("Not yet implemented")
      }
    }
  }
}
