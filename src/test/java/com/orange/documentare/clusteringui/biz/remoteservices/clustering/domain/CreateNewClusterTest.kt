package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.appnotifications.AppNotification
import com.orange.documentare.clusteringui.biz.appnotifications.AppNotificationsService
import com.orange.documentare.clusteringui.biz.file.command.AddToClusterCommandHandler
import com.orange.documentare.clusteringui.biz.file.command.BinPurgeCommandHandler
import com.orange.documentare.clusteringui.biz.file.command.DuplicateFilesCommandHandler
import com.orange.documentare.clusteringui.biz.file.command.ResetFileInfoCommandHandler
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile.Companion.builder
import com.orange.documentare.clusteringui.biz.file.domain.DomainFileBuilder
import com.orange.documentare.clusteringui.biz.file.event.*
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.*
import com.orange.documentare.clusteringui.biz.file.query.AllFilesQueryHandler
import com.orange.documentare.clusteringui.biz.file.query.ClusterFilesQueryHandler
import com.orange.documentare.clusteringui.biz.file.query.SelectedFilesQueryHandler
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.SaveClustering.Companion.testUUIDSupplier
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure.ClusteringRemoteApi
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.orange.documentare.core.model.ref.clustering.Cluster
import com.orange.documentare.core.model.ref.clustering.Clustering
import com.orange.documentare.core.model.ref.clustering.ClusteringElement
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.event.TestEventHandler
import com.tophe.ddd.queries.QueryBus
import org.assertj.core.api.BDDAssertions.then
import org.assertj.core.util.Lists
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import java.util.stream.Collectors

@DisplayName("Clustering service, Create new cluster")
internal class CreateNewClusterTest {
  private val testEventHandler = TestEventHandler()
  private val eventBus = EventBus(DBEventInMemoryRepositoryForTestPurpose())
  private val appNotifications = AppNotificationsService()
  private val clusteringService = ClusteringServiceImpl(
    DistanceSupplier().get(), appNotifications,
    EmbeddedComputationServer.disabled()
  )
  private val eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  private val projections = Projections(eventRepo)
  private val clusteringRemoteApi = mock<ClusteringRemoteApi>()

  @BeforeEach
  fun setUp() {
    val commandBus = CommandBus()
    commandBus.register(
      BinPurgeCommandHandler(eventRepo, eventBus),
      AddToClusterCommandHandler(eventRepo, eventBus),
      DuplicateFilesCommandHandler(eventRepo, eventBus),
      ResetFileInfoCommandHandler(eventRepo, eventBus)
    )
    val queryBus = QueryBus()
    queryBus.register(AllFilesQueryHandler(projections), SelectedFilesQueryHandler(projections), ClusterFilesQueryHandler(projections))
    clusteringService.setCommandBus(commandBus)
    clusteringService.setQueryBus(queryBus)
    clusteringService.testClusteringRemote(clusteringRemoteApi)
    eventBus.register(testEventHandler)
    eventBus.register(projections)
    val uuidSupplier = UUIDSupplier()
    testUUIDSupplier { uuidSupplier.getAsString() }
  }

  @DisplayName("Create cluster result is saved and we notify it is done")
  @Test
  @Throws(InterruptedException::class)
  fun create_cluster_is_done_and_result_is_saved() {
    // given
    val (_id) = b().aggregateId(aggregateIdExample(0)).filename("naf").build()
    val (_id1) = b().aggregateId(aggregateIdExample(1)).filename("nif").build()
    val (_id2) = b().aggregateId(aggregateIdExample(2)).filename("nouf").build()
    val (_id3) = b().aggregateId(aggregateIdExample(3)).filename("manouf").build()
    val (_id4) = b().aggregateId(aggregateIdExample(4)).filename("elouf").build()
    val events: List<Event> = Lists.newArrayList<Event>(
      FileCreated(_id, "naf"),
      FileCreated(_id1, "nif"),
      FileCreated(_id2, "nouf"),
      FileCreated(_id3, "manouf"),
      FileCreated(_id4, "elouf")
    )
    eventRepo.insert(events)
    projections.testPurposeInitFromHistory(events)
    stubCreateClusterReturnedValue(
      clusteringOfFiveElementsInSameClusterWithAMultiset(false), false
    )
    val filesIdsInBin = emptyList<String>()

    var actualAppNotification: AppNotification? = null
    appNotifications.register {
      actualAppNotification = it
    }

    // when
    startThenWaitForClustering(filesIdsInBin)

    // then
    then(testEventHandler.events()).containsExactly(
      FileInfoReset(aggregateIdExample(0), null),
      FileInfoReset(aggregateIdExample(1), null),
      FileInfoReset(aggregateIdExample(2), null),
      FileInfoReset(aggregateIdExample(3), null),
      FileInfoReset(aggregateIdExample(4), null),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("0").clusterCenter(true).inMultiset(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("0").inMultiset(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("0").distanceToCenter(10).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("0").distanceToCenter(10).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(4)).clusterId("0").distanceToCenter(10).build()
    )

    then(actualAppNotification).isEqualTo(AppNotification.CREATE_CLUSTER_DONE)

    verifyCalculateClusteringIsNotCalled()
  }

  @DisplayName("duplicate file is saved as a duplicate")
  @Test
  @Throws(InterruptedException::class)
  fun duplicate_file_is_saved_as_a_duplicate() {
    // given
    val (_id) = b().aggregateId(aggregateIdExample(0)).filename("naf").build()
    val (_id1) = b().aggregateId(aggregateIdExample(1)).filename("nif").build()
    val (_id2) = b().aggregateId(aggregateIdExample(2)).filename("nouf").build()
    val (_id3) = b().aggregateId(aggregateIdExample(3)).filename("manouf").build()
    val (_id4) = b().aggregateId(aggregateIdExample(4)).filename("elouf").build()
    val events: List<Event> = Lists.newArrayList<Event>(
      FileCreated(_id, "naf"),
      FileCreated(_id1, "nif"),
      FileCreated(_id2, "nouf"),
      FileCreated(_id3, "manouf"),
      FileCreated(_id4, "elouf")
    )
    eventRepo.insert(events)
    projections.testPurposeInitFromHistory(events)
    stubCreateClusterReturnedValue(
      clusteringOfFiveElementsInSameClusterWithAMultiset(true), false
    )
    val filesIdsInBin = emptyList<String>()

    // when
    startThenWaitForClustering(filesIdsInBin)

    // then
    then(testEventHandler.events()).containsExactly(
      FileInfoReset(aggregateIdExample(0), null),
      FileInfoReset(aggregateIdExample(1), null),
      FileInfoReset(aggregateIdExample(2), null),
      FileInfoReset(aggregateIdExample(3), null),
      FileInfoReset(aggregateIdExample(4), null),
      FileDuplicated(aggregateIdExample(4), null),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("0").clusterCenter(true).inMultiset(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("0").inMultiset(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("0").distanceToCenter(10).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("0").distanceToCenter(10).build()
    )
    verifyCalculateClusteringIsNotCalled()
  }

  @DisplayName("cluster is not created if a file was deleted in the meantime")
  @Test
  @Throws(InterruptedException::class)
  fun cluster_is_not_created_if_a_file_was_deleted_in_the_meantime() {
    // given
    val (_id) = b().aggregateId(aggregateIdExample(0)).filename("naf").build()
    val (_id1) = b().aggregateId(aggregateIdExample(1)).filename("nif").build()
    val (_id2) = b().aggregateId(aggregateIdExample(2)).filename("nouf").build()
    val (_id3) = b().aggregateId(aggregateIdExample(3)).filename("manouf").build()
    val (_id4) = b().aggregateId(aggregateIdExample(4)).filename("elouf").build()
    val events: List<Event> = Lists.newArrayList<Event>(
      FileCreated(_id, "naf"),
      FileCreated(_id1, "nif"),
      FileCreated(_id2, "nouf"),
      FileCreated(_id3, "manouf"),
      FileCreated(_id4, "elouf")
    )
    eventRepo.insert(events)
    projections.testPurposeInitFromHistory(events)
    stubCreateClusterReturnedValue(
      clusteringOfFiveElementsInSameClusterWithAMultiset(false), true
    )
    val filesIdsInBin = emptyList<String>()

    // when
    startThenWaitForClustering(filesIdsInBin)

    // then
    then(testEventHandler.events()).isEmpty()
    verifyCalculateClusteringIsNotCalled()
  }

  @DisplayName("cluster is not created if a file was deleted in the meantime, even if it is a duplicate")
  @Test
  @Throws(InterruptedException::class)
  fun cluster_is_not_created_if_a_file_was_deleted_in_the_meantime_even_if_it_is_a_duplicate() {
    // given
    val (_id) = b().aggregateId(aggregateIdExample(0)).filename("naf").build()
    val (_id1) = b().aggregateId(aggregateIdExample(1)).filename("nif").build()
    val (_id2) = b().aggregateId(aggregateIdExample(2)).filename("nouf").build()
    val (_id3) = b().aggregateId(aggregateIdExample(3)).filename("manouf").build()
    val (_id4) = b().aggregateId(aggregateIdExample(4)).filename("elouf").build()
    val events: List<Event> = Lists.newArrayList<Event>(
      FileCreated(_id, "naf"),
      FileCreated(_id1, "nif"),
      FileCreated(_id2, "nouf"),
      FileCreated(_id3, "manouf"),
      FileCreated(_id4, "elouf")
    )
    eventRepo.insert(events)
    projections.testPurposeInitFromHistory(events)
    stubCreateClusterReturnedValue(
      clusteringOfFiveElementsInSameClusterWithAMultiset(true), true
    )
    val filesIdsInBin = emptyList<String>()

    // when
    startThenWaitForClustering(filesIdsInBin)

    // then
    then(testEventHandler.events()).isEmpty()
    verifyCalculateClusteringIsNotCalled()
  }

  @DisplayName("log error if remote service failed")
  @Test
  @Throws(InterruptedException::class)
  fun log_error_if_remote_service_failed() {
    // given
    Mockito.`when`(clusteringRemoteApi.recalculateClusters(any(), any())).thenThrow(IllegalStateException())
    val filesIdsInBin = emptyList<String>()

    // when
    startThenWaitForClustering(filesIdsInBin)

    // then
    then(testEventHandler.events()).isEmpty()
    verifyCalculateClusteringIsNotCalled()
  }

  private fun clusteringOfFiveElementsWithoutMultiset(lastIsADuplicate: Boolean): Clustering {
    val elements: List<ClusteringElement> = Lists.newArrayList(
      ClusteringElement(aggregateIdExample(0), 0, true, false, null, null),
      ClusteringElement(aggregateIdExample(1), 0, false, true, null, null),
      ClusteringElement(aggregateIdExample(2), 0, false, false, null, 10),
      ClusteringElement(aggregateIdExample(3), 0, false, false, null, 10),
      ClusteringElement(aggregateIdExample(4), 0, false, false, if (lastIsADuplicate) 3 else null, 10)
    )
    val clusterWithoutMultiset = listOf(Cluster(listOf(0, 1, 2, 3, 4)))
    return Clustering(elements, null, clusterWithoutMultiset, null, null, null)
  }

  private fun clusteringOfFiveElementsInSameClusterWithAMultiset(lastIsADuplicate: Boolean): Clustering {
    val elementsIndices: List<Int> = Lists.newArrayList(0, 1, 2, 3, 4)
    val multisetElementsIndices: List<Int> = Lists.newArrayList(0, 1)
    val cluster = Cluster(elementsIndices, multisetElementsIndices)
    val clustering = clusteringOfFiveElementsWithoutMultiset(lastIsADuplicate)
    return Clustering(clustering.elements, null, listOf(cluster), null, null, null)
  }

  @Throws(InterruptedException::class)
  private fun startThenWaitForClustering(filesIdsInBin: List<String>) {
    val files = projections.files.ids().stream().map { fileId: String? -> projections.files[fileId!!] }.collect(Collectors.toList())
    clusteringService.createNewClusterAsync(Parameters.builder().pCount(1).build(), files, filesIdsInBin)
    do {
      Thread.sleep(10)
    } while (clusteringService.isRunning())
  }

  private fun stubCreateClusterReturnedValue(result: Clustering, markFileAsDeleted: Boolean) {
    if (markFileAsDeleted) {
      markFileAsDeleted()
    }
    Mockito.`when`(clusteringRemoteApi.createCluster(any(), any())).thenReturn(TryOrCatch.success(result))
  }

  private fun b(): DomainFileBuilder {
    return builder()
  }

  private fun markFileAsDeleted() {
    markFileAsDeleted(aggregateIdExample(4), eventRepo, projections)
  }

  private fun verifyCalculateClusteringIsNotCalled() {
    Mockito.verify(clusteringRemoteApi, Mockito.never()).calculateClustering(any(), any())
  }

  companion object {
    fun markFileAsDeleted(id: String?, eventRepo: DBEventInMemoryRepositoryForTestPurpose, projections: Projections) {
      val fileDeleted = FileDeleted(id, null)
      eventRepo.insert(listOf(fileDeleted))
      projections.onEvent(fileDeleted)
    }
  }
}
