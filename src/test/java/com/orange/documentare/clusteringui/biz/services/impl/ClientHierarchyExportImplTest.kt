package com.orange.documentare.clusteringui.biz.services.impl

import com.orange.documentare.clusteringui.biz.file.query.ClusterDistancesStats
import com.orange.documentare.clusteringui.biz.file.query.ClusterFilesQueryHandler
import com.orange.documentare.clusteringui.biz.file.query.DistanceToCenterStatsForAllFilesInClusterQueryHandler
import com.orange.documentare.clusteringui.biz.file.query.HierarchiesQueryHandler
import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples
import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples.exportedClientHierarchiesWith4LevelsOneRootParameters
import com.orange.documentare.clusteringui.biz.parameters.domain.DistanceParameters
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQuery
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.StatisticsConsolidation.ComputedDistances
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.StatisticsConsolidation.ComputedDistancesExamples
import com.orange.documentare.clusteringui.biz.processes.hierarchy.transport.HierarchyStatisticsConsolidation
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryHandler
import org.assertj.core.api.BDDAssertions.catchException
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import java.io.File

class ClientHierarchyExportImplTest {

  private val projection = HierarchyExamples.projectionWith4LevelsOneRoot()
  private val queryBus = QueryBus().apply {
    register(
      HierarchiesQueryHandler(projection),
      ClusterFilesQueryHandler(projection),
      DistanceToCenterStatsForAllFilesInClusterQueryHandler(projection),
      fakeParametersHandler(exportedClientHierarchiesWith4LevelsOneRootParameters())
    )
  }
  private val clientHierarchyExport: ClientHierarchyExport = ClientHierarchyExportImpl(noOpHierarchyStatisticsConsolidation())
    .apply { setQueryBus(queryBus) }


  @Test
  fun `exported hierarchy with hierarchy weighted statistics`() {

    // when
    val hierarchyReferenceModel = clientHierarchyExport.exportClientHierarchy()

    // then
    then(hierarchyReferenceModel).isEqualTo(HierarchyExamples.exportedClientHierarchiesWith4LevelsOneRootNew())
    then(hierarchyReferenceModel.distanceParameters).isEqualTo(exportedClientHierarchiesWith4LevelsOneRootParameters())
  }

  @Test
  fun `serialize exported hierarchy with hierarchy weighted statistics`() {
    // given
    // since other tests may have changed the files directory path...
    FileIO.resetFilesDirectoryForTestsPurpose()

    // when
    val serialize = clientHierarchyExport.exportClientHierarchy().serialize()

    // then
    val expectedJsonString = File(javaClass.getResource("/expected_serialized_hierarchy.json")!!.file).readText().trim()
    then(serialize).isEqualTo(expectedJsonString)
  }

  @Test
  fun `exported hierarchy with level 0 without cluster stats yet`() {
    // when
    val hierarchyReferenceModel = clientHierarchyExport.exportClientHierarchyForStatisticsConsolidation()

    // then
    then(hierarchyReferenceModel).isEqualTo(HierarchyExamples.exportedClientHierarchiesForStatisticsConsolidation())
    then(hierarchyReferenceModel.distanceParameters).isEqualTo(exportedClientHierarchiesWith4LevelsOneRootParameters())
  }

  @Test
  fun `compute cluster statistics, throw exception if distances are not available`() {
    // given
    val hierarchyStatisticsConsolidation = object : HierarchyStatisticsConsolidation {
      override fun computedDistancesAvailable() = false

      override fun computedDistances(): ComputedDistances {
        return ComputedDistancesExamples.computedDistancesWith3Levels
      }
    }
    val clientHierarchyExport = ClientHierarchyExportImpl(hierarchyStatisticsConsolidation)

    // when
    val catchException = catchException { clientHierarchyExport.clusterStatisticsInUpperHierarchyLevels(ComputedDistancesExamples.clusterCenterLevel1AggregateId, 1) }

    // then
    then(catchException.message).isEqualTo("❌  hierarchy distances statistics not available yet")
  }

  @Test
  fun `compute cluster statistics for level 1`() {
    // given
    val hierarchyStatisticsConsolidation = object : HierarchyStatisticsConsolidation {
      override fun computedDistancesAvailable() = true

      override fun computedDistances(): ComputedDistances {
        return ComputedDistancesExamples.computedDistancesWith3Levels
      }

    }
    val clientHierarchyExport = ClientHierarchyExportImpl(hierarchyStatisticsConsolidation)

    // when
    val clusterDistancesStats = clientHierarchyExport.clusterStatisticsInUpperHierarchyLevels(ComputedDistancesExamples.clusterCenterLevel1AggregateId, 1)

    // then
    then(clusterDistancesStats).isEqualTo(
      ClusterDistancesStats(
        4,
        mean = 25.0,
        standardDeviation = 7.0710678118654755,
        false,
        "centroid[f100, f101] distances(used 2, skipped 2) IntStatistics(min=20, max=30, mean=25.0, standardDeviation=7.0710678118654755, percentile=25.0) [f102 -> 20, f103 -> 30]"
      )
    )
  }

  @Test
  fun `compute cluster statistics for level 2 with singleton`() {
    // given
    val hierarchyStatisticsConsolidation = object : HierarchyStatisticsConsolidation {
      override fun computedDistancesAvailable() = true

      override fun computedDistances(): ComputedDistances {
        return ComputedDistancesExamples.computedDistancesWith3Levels
      }

    }
    val clientHierarchyExport = ClientHierarchyExportImpl(hierarchyStatisticsConsolidation)

    // when
    val clusterDistancesStats = clientHierarchyExport.clusterStatisticsInUpperHierarchyLevels(ComputedDistancesExamples.clusterCenterLevel2AggregateId, 2)

    // then
    then(clusterDistancesStats.singleton).isTrue
    then(clusterDistancesStats.description).isEqualTo("centroid[f200]")
  }

  private fun fakeParametersHandler(fakeParameters: DistanceParameters): QueryHandler<ParametersQuery, Parameters> {
    return object : QueryHandler<ParametersQuery, Parameters>() {
      override fun doExecute(command: ParametersQuery): Parameters {
        return Parameters.defaults().toBuilder()
          .suffixArrayAlgorithm(fakeParameters.suffixArrayAlgorithm).rawConverter(fakeParameters.rawConverterEnabled)
          .pCount(fakeParameters.rawConverterPixelCount)
          .build()
      }
    }
  }

  private fun noOpHierarchyStatisticsConsolidation() = object : HierarchyStatisticsConsolidation {
    override fun computedDistancesAvailable() = false

    override fun computedDistances(): ComputedDistances {
      TODO("Not yet implemented")
    }

  }
}
