package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.event.FilePutInNamedClass
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class PutFileInNamedClassCommandTest {

  val eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  val eventBus = EventBus(eventRepo)
  val handler = PutFileInNamedClassCommandHandler(eventRepo, eventBus)

  @Test
  fun `put file to named class, then associated event was produced`() {
    // given
    val fileAggregateId = aggregateIdExample(0)
    val initialEvents: MutableList<Event> = mutableListOf(
      FileCreated(fileAggregateId, "titi"),
    )
    eventRepo.insert(initialEvents)
    val fileNamedClass = arrayOf("class bird")
    val cmd = PutFileInNamedClassCommand(fileAggregateId, fileNamedClass)

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isTrue
    initialEvents.apply {
      this.add(FilePutInNamedClass(fileAggregateId, fileNamedClass))
      then(eventRepo.findAll()).containsAll(this)
    }
  }
}
