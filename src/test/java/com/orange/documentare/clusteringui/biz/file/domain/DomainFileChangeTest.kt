package com.orange.documentare.clusteringui.biz.file.domain

import com.orange.documentare.clusteringui.biz.file.event.DistancesToClusterCenterAdded
import com.tophe.ddd.infrastructure.event.Event
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class DomainFileChangeTest {

  @Test
  fun move_to_cluster() {
    // given
    val f = DomainFile.builder().build()

    // when
    val fileAddedToCluster = f.moveToCluster("10", true, null, null, null, null, null)

    // then
    then(fileAddedToCluster.clusterId).isEqualTo("10")
    then(fileAddedToCluster.clusterCenter()).isTrue()
  }

  @Test
  fun add_distances_to_clusters_center() {
    // given
    val f = DomainFile.builder().build()

    // when
    val tuple: Event? = f.addDistancesToClustersCenters(listOf(DistanceToCluster(100, "10")))

    // then
    then(tuple).isEqualTo(DistancesToClusterCenterAdded(f.aggregateId, listOf(DistanceToCluster(100, "10"))))
  }

  @Test
  fun add_not_more_than_5_distances_to_clusters_center() {
    // given
    val f = DomainFile.builder().build()
    val tooManyDistances = (0..300).reversed().map { DistanceToCluster(it, "$it") }

    // when
    val tuple: Event? = f.addDistancesToClustersCenters(tooManyDistances)

    // then
    then(tuple).isInstanceOf(DistancesToClusterCenterAdded::class.java)
    val distancesToClusterCenterAdded = tuple as DistancesToClusterCenterAdded
    then(distancesToClusterCenterAdded.distancesToClustersCenters).hasSize(5)
  }

  @Test
  fun add_distances_to_clusters_center_keeping_5_lowers() {
    // given
    val f = DomainFile.builder().build()
    val tooManyDistances = listOf(
      DistanceToCluster(11, "1"),
      DistanceToCluster(1000, "2"),
      DistanceToCluster(50, "3"),
      DistanceToCluster(3000, "4"),
      DistanceToCluster(33, "5"),
      DistanceToCluster(1, "6"),
      DistanceToCluster(10, "7")
    )
    val expectedDistances = DistancesToClusterCenterAdded(
      f.aggregateId, listOf(
        DistanceToCluster(1, "6"),
        DistanceToCluster(10, "7"),
        DistanceToCluster(11, "1"),
        DistanceToCluster(33, "5"),
        DistanceToCluster(50, "3")
      )
    )

    // when
    val tuple: Event? = f.addDistancesToClustersCenters(tooManyDistances)

    // then
    val distancesToClusterCenterAdded = tuple as DistancesToClusterCenterAdded
    then(distancesToClusterCenterAdded).isEqualTo(expectedDistances)
  }
}
