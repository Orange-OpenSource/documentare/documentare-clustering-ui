package com.orange.documentare.clusteringui.biz.file.domain

import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

internal class DomainFileToBuilderTest {
  @Test
  fun `check builder does not loose data`() {
    // given
    val domainFile = DomainFile.builder()
      .aggregateId("id")
      .fileNamedClass(arrayOf("named class"))
      .automaticInclude(true)
      .clusterCenter(true)
      .className("class name")
      .clusterId("12")
      .clusterModified(true)
      .delete(true)
      .distanceToCenter(100)
      .distancesToClustersCenters(listOf(DistanceToCluster(100, "10")))
      .duplicate(true)
      .inMultiset(true)
      .filename("file name")
      .inWaitingRoom(true)
      .lock(true)
      .build()

    // then
    then(domainFile).isEqualTo(domainFile.toBuilder().build())
  }
}
