package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.infrastructure.io.FileIO.initFilesDirectory
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.event.TestEventHandler
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.io.File

@DisplayName("Command AddFiles")
class AddFilesInWaitingRoomCommandTest {
  private val testEventHandler = TestEventHandler()
  private val eventBus = EventBus(DBEventInMemoryRepositoryForTestPurpose())
  private val uuidSupplierDomainFile = UUIDSupplierDomainFile()
  private val handler = AddFilesInWaitingRoomCommandHandler(eventBus) { uuidSupplierDomainFile.asString }

  @BeforeEach
  fun setup() {
    File(FILES_DIRECTORY).deleteRecursively()
    initFilesDirectory(FILES_DIRECTORY)
    eventBus.register(testEventHandler)
  }

  @Test
  fun add_file_named_titi_in_waiting_room_then_check_it_exists_in_db() {
    // given
    val image = File(javaClass.getResource("/image.png")!!.file)
    val bytes = image.readBytes()
    val filename = image.name
    val file = AddFilesInWaitingRoomCommand.File(filename, bytes)
    val cmd = AddFilesInWaitingRoomCommand(listOf(file))

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isTrue
    val id = uuidSupplierDomainFile.get(0)
    then(File("$FILES_DIRECTORY$id.png")).exists()
    then(File(FILES_DIRECTORY + "thumbnails/" + id)).exists()
    then(events()).containsExactly(FileCreated(id, filename, true))
  }


  @Test
  fun `added files aggregate ids are returned`() {
    // given
    val image = File(javaClass.getResource("/image.png")!!.file)
    val bytes = image.readBytes()
    val filename = image.name
    val file1 = AddFilesInWaitingRoomCommand.File(filename, bytes)
    val file2 = AddFilesInWaitingRoomCommand.File("another", bytes)
    val cmd = AddFilesInWaitingRoomCommand(listOf(file1, file2))

    // when
    val response: CommandResponse<List<String>> = handler.execute(cmd)

    // then
    then(response.success()).isTrue
    then(response.value()).containsExactly(uuidSupplierDomainFile.get(0), uuidSupplierDomainFile.get(1))
  }

  private fun events(): List<Event> {
    return testEventHandler.events()
  }

  companion object {
    const val FILES_DIRECTORY = "/tmp/"
  }
}
