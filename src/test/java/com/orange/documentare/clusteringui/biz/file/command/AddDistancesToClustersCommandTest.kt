package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.domain.DomainFileBuilder
import com.orange.documentare.clusteringui.biz.file.event.DistancesToClusterCenterAdded
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.infrastructure.event.EventBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Command AddDistancesToClusters")
class AddDistancesToClustersCommandTest {
  /** init eventBus with event repository so that events will be persisted automatically  */
  var eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  var eventBus = EventBus(eventRepo)
  var handler = AddDistancesToClustersCommandHandler(eventRepo, eventBus)

  @Test
  fun distances_to_clusters_centers_are_added() {
    // given
    val file = b().aggregateId(aggregateIdExample(0)).filename("0").build()
    eventRepo.insert(listOf(FileCreated(file.aggregateId, file.filename)))
    val distancesToClustersCenters = listOf(DistanceToCluster(10000, "1"))
    val cmd = AddDistancesToClustersCommand(file.aggregateId, distancesToClustersCenters)
    // when
    val response: CommandResponse<*> = handler.execute(cmd)
    // then
    then(response.success()).isTrue
    then(eventRepo.findAll()).containsExactly(
      FileCreated(file.aggregateId, file.filename),
      DistancesToClusterCenterAdded(file.aggregateId, distancesToClustersCenters)
    )
  }

  @Test
  fun distances_to_clusters_centers_are_added_and_are_same_as_previous_ones() {
    // given
    val distancesToClustersCenters = listOf(DistanceToCluster(10000, "1"))
    val file = b().aggregateId(aggregateIdExample(0)).filename("0").distancesToClustersCenters(distancesToClustersCenters).build()
    eventRepo.insert(
      listOf(
        FileCreated(file.aggregateId, file.filename),
        DistancesToClusterCenterAdded(file.aggregateId, distancesToClustersCenters)
      )
    )
    val cmd = AddDistancesToClustersCommand(file.aggregateId, distancesToClustersCenters)
    // when
    val response: CommandResponse<*> = handler.execute(cmd)
    // then
    then(response.success()).isTrue
    then(eventRepo.findAll()).containsExactly(
      FileCreated(file.aggregateId, file.filename),
      DistancesToClusterCenterAdded(file.aggregateId, distancesToClustersCenters)
    )
  }

  private fun b(): DomainFileBuilder {
    return DomainFile.builder()
  }
}
