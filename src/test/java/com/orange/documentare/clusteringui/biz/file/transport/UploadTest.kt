package com.orange.documentare.clusteringui.biz.file.transport

import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.file.command.AddFilesCommand
import com.orange.documentare.clusteringui.biz.file.command.AddFilesInWaitingRoomCommand
import com.orange.documentare.clusteringui.biz.file.command.PutFileInNamedClassCommand
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.tophe.ddd.commands.Command
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.queries.QueryBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class UploadTest {
  private val upload = Upload(CommandBus(), QueryBus())
  private val actualCommands: MutableList<Command> = mutableListOf()

  private val expectedAddedFileAggregateId = "aggregateId"

  @BeforeEach
  fun setup() {
    upload.commandBusDispatch = {
      actualCommands.add(it)
      CommandResponse(TryOrCatch.success(listOf(expectedAddedFileAggregateId)))
    }
  }

  @Test
  fun `add files in sort room`() {
    // given
    upload.commandBusDispatch = {
      actualCommands.add(it)
      CommandResponse(TryOrCatch.success(listOf(expectedAddedFileAggregateId)))
    }

    upload.queryParameters = { Parameters.builder().feed(false).build() }

    // when
    val filenamesAndContent = listOf(Pair("file1", byteArrayOf(1, 2)), Pair("file2", byteArrayOf(3, 4)))
    val addedFilesAggregateIds = upload.addFiles(filenamesAndContent, null)

    // then
    then(addedFilesAggregateIds).containsExactly(expectedAddedFileAggregateId)
    then(actualCommands).containsExactly(
      AddFilesCommand(
        listOf(
          AddFilesCommand.File("file1", byteArrayOf(1, 2)),
          AddFilesCommand.File("file2", byteArrayOf(3, 4))
        )
      )
    )
  }

  @Test
  fun `add file in sort room with class name`() {
    // given
    upload.commandBusDispatch = {
      actualCommands.add(it)
      CommandResponse(TryOrCatch.success(listOf(expectedAddedFileAggregateId)))
    }
    upload.queryParameters = { Parameters.builder().feed(false).build() }

    // when
    val filenamesAndContent = listOf(Pair("file1", byteArrayOf(1, 2)))
    val fileNamedClass = arrayOf("class name")
    val addedFilesAggregateIds = upload.addFiles(filenamesAndContent, fileNamedClass)

    // then
    then(addedFilesAggregateIds).containsExactly(expectedAddedFileAggregateId)
    then(actualCommands).containsExactly(
      AddFilesCommand(
        listOf(
          AddFilesCommand.File("file1", byteArrayOf(1, 2))
        )
      ),
      PutFileInNamedClassCommand(expectedAddedFileAggregateId, fileNamedClass)
    )
  }

  @Test
  fun `add files in waiting room`() {
    // given
    upload.commandBusDispatch = {
      actualCommands.add(it)
      CommandResponse(TryOrCatch.success(listOf(expectedAddedFileAggregateId + "1", expectedAddedFileAggregateId + "2")))
    }
    upload.queryParameters = { Parameters.builder().feed(true).build() }

    // when
    val filenamesAndContent = listOf(Pair("file1", byteArrayOf(1, 2)), Pair("file2", byteArrayOf(3, 4)))
    val addedFilesAggregateIds = upload.addFiles(filenamesAndContent, null)

    // then
    then(addedFilesAggregateIds).containsExactly(expectedAddedFileAggregateId + "1", expectedAddedFileAggregateId + "2")
    then(actualCommands).containsExactly(
      AddFilesInWaitingRoomCommand(
        listOf(
          AddFilesInWaitingRoomCommand.File("file1", byteArrayOf(1, 2)),
          AddFilesInWaitingRoomCommand.File("file2", byteArrayOf(3, 4))
        )
      )
    )
  }

  @Test
  fun `add file in waiting room with class name`() {
    // given
    upload.commandBusDispatch = {
      actualCommands.add(it)
      CommandResponse(TryOrCatch.success(listOf(expectedAddedFileAggregateId)))
    }
    upload.queryParameters = { Parameters.builder().feed(true).build() }

    // when
    val filenamesAndContent = listOf(Pair("file1", byteArrayOf(1, 2)))
    val fileNamedClass = arrayOf("class name")
    val addedFilesAggregateIds = upload.addFiles(filenamesAndContent, fileNamedClass)

    // then
    then(addedFilesAggregateIds).containsExactly(expectedAddedFileAggregateId)
    then(actualCommands).containsExactly(
      AddFilesInWaitingRoomCommand(
        listOf(
          AddFilesInWaitingRoomCommand.File("file1", byteArrayOf(1, 2))
        )
      ),
      PutFileInNamedClassCommand(expectedAddedFileAggregateId, fileNamedClass)
    )
  }

  @Test
  fun `throw error if we try to add multiple files while indicating a named class`() {
    // given
    upload.queryParameters = { Parameters.builder().feed(true).build() }
    val fileNamedClass = arrayOf("class name")

    // when
    val filenamesAndContent = listOf(Pair("file1", byteArrayOf(1, 2)), Pair("file2", byteArrayOf(3, 4)))
    val runCatching = kotlin.runCatching { upload.addFiles(filenamesAndContent, fileNamedClass) }

    // then
    then(runCatching.isFailure).isTrue
    then(runCatching.exceptionOrNull()).isInstanceOf(IllegalStateException::class.java)
    then(runCatching.exceptionOrNull()!!.message).isEqualTo("can not upload multiple files in the same request if clustering class name is provided")
  }
}
