package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileDeleted
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.infrastructure.event.EventBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.io.File
import java.io.IOException

@DisplayName("Command DeleteFiles")
class DeleteFilesCommandTest {
  /** init eventBus with event repository so that events will be persisted automatically  */
  private var eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  private var eventBus = EventBus(eventRepo)
  private var handler = DeleteFilesCommandHandler(eventRepo, eventBus)
  private var uuidSupplierDomainFile = UUIDSupplierDomainFile()

  @BeforeEach
  fun setup() {
    File(FILES_DIRECTORY).deleteRecursively()
    FileIO.initFilesDirectory(FILES_DIRECTORY)
  }

  @Test
  @Throws(IOException::class)
  fun create_then_delete_file_named_titi_then_check_it_does_not_exist_in_db() {
    // given
    val image = File(javaClass.getResource("/image.png").file)
    val bytes = image.readBytes()
    val file = AddFilesCommand.File(image.name, bytes)
    val addCmd = AddFilesCommand(listOf(file))
    val addHandler = AddFilesCommandHandler(eventBus)
    addHandler.testPurposeSetUuidSuplier { uuidSupplierDomainFile.asString }
    addHandler.execute(addCmd)
    val addedFileId = uuidSupplierDomainFile.get(0)
    val cmd = DeleteFilesCommand(listOf(addedFileId))

    // when
    val response: CommandResponse<*> = handler.execute(cmd)

    // then
    then(response.success()).isTrue
    then(File("$FILES_DIRECTORY$addedFileId.png").exists()).isTrue
    then(File("$FILES_DIRECTORY/thumbnails/$addedFileId").exists()).isTrue
    then(eventRepo.findAll()).containsExactly(
      FileCreated(addedFileId, image.name),
      FileDeleted(addedFileId, null)
    )
  }

  companion object {
    const val FILES_DIRECTORY = "/tmp/"
  }
}
