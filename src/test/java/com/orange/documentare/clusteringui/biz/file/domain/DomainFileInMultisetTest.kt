package com.orange.documentare.clusteringui.biz.file.domain

import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class DomainFileInMultisetTest {

  @Test
  fun `a cluster center is in multiset`() {
    val domainFile = DomainFile.builder().clusterCenter(true).build()
    then(domainFile.inMultiset()).isTrue
  }

  @Test
  fun `a file in multiset but not a cluster center is in multiset`() {
    val domainFile = DomainFile.builder().inMultiset(true).build()
    then(domainFile.inMultiset()).isTrue
  }
}
