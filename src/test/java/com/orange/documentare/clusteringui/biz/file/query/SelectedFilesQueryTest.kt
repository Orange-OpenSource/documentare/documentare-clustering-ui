package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.event.MultisetElementMovedToNextHierarchyLevel
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.queries.QueryResponse
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class SelectedFilesQueryTest {
  var projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  var handler = SelectedFilesQueryHandler(projections)

  @Test
  fun `retrieve all files selected`() {
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "0", "1", true),
      FileCreated(aggregateIdExample(1), "1", "1", false),
      FileCreated(aggregateIdExample(2), "2", "1", false)
    )
    projections.testPurposeInitFromHistory(events)

    // when
    val response: QueryResponse<List<DomainFile>> = handler.execute(SelectedFilesQuery(listOf(aggregateIdExample(1), aggregateIdExample(2))))

    // then
    then(response.success()).isTrue()
    then(response.value()).hasSize(2)
    then(response.value().map { f: DomainFile -> f.aggregateId }).contains(aggregateIdExample(1), aggregateIdExample(2))
  }

  @Test
  fun `can not retrieve file in a lower level`() {
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "0", "1", true),
      FileCreated(aggregateIdExample(1), "1", "1", false),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(0), 1)
    )
    projections.testPurposeInitFromHistory(events)

    // when
    val response: QueryResponse<List<DomainFile>> = handler.execute(SelectedFilesQuery(listOf(aggregateIdExample(1))))

    // then
    then(response.success()).isFalse()
    then(response.failureCause()).contains("java.lang.IllegalStateException : file id not found: ")
  }
}
