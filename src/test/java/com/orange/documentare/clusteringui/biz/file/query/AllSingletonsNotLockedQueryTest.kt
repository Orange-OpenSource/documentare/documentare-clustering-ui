package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.event.ClusterLocked
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.Event
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class AllSingletonsNotLockedQueryTest {
  var projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  var handler = AllSingletonsNotLockedQueryHandler(projections)

  @Test
  fun return_nothing_if_no_singletons_are_not_locked() {
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "0", "1", true),
      ClusterLocked(aggregateIdExample(0), "1", true),
      FileCreated(aggregateIdExample(1), "1", "2", true),
      ClusterLocked(aggregateIdExample(1), "2", true),
      FileCreated(aggregateIdExample(3), "3", "3", true),
      FileCreated(aggregateIdExample(4), "4", "3", false)
    )
    projections.testPurposeInitFromHistory(events)
    // when
    val response = handler.execute(AllSingletonsNotLockedQuery())

    // then
    then(response.success()).isTrue()
    then(response.value()).hasSize(0)
  }

  @Test
  fun return_one_singleton_if_only_one_singleton_is_not_locked() {
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "0", "1", true),
      FileCreated(aggregateIdExample(1), "1", "2", true),
      ClusterLocked(aggregateIdExample(1), "2", true),
      FileCreated(aggregateIdExample(3), "3", "3", true),
      FileCreated(aggregateIdExample(4), "4", "3", false)
    )
    projections.testPurposeInitFromHistory(events)
    // when
    val response = handler.execute(AllSingletonsNotLockedQuery())

    // then
    then(response.success()).isTrue()
    then(response.value()).hasSize(1)
  }
}
