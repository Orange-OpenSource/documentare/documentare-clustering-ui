package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.file.command.RemoveSelectedFilesFromClustersCommand
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.domain.Room
import com.orange.documentare.clusteringui.biz.file.query.AllClustersQuery
import com.orange.documentare.clusteringui.biz.file.query.SelectedFilesQuery
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.processes.feed.domain.BreakOutOfThresholdNotLockedClusters
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*

class BreakOutOfThresholdNotLockedClustersTest {

  private val commandBus = mock(CommandBus::class.java)
  private val queryBus = mock(QueryBus::class.java)

  private val breakOutOfThresholdClusters = BreakOutOfThresholdNotLockedClusters(commandBus, queryBus)
  private val defaultMaxMultisetThreshold = Parameters.defaults().maxMultisetThreshold

  @Test
  fun `log error if we fail to retrieve clusters`() {
    // given
    weFailToQueryAllClusters()

    // when
    breakOutOfThresholdClusters.breakIfNumberOfMultisetIsGreaterThanMultisetThreshold(defaultMaxMultisetThreshold)

    // then

  }

  @Test
  fun `log error if we fail to select cluster files`() {
    // given
    val fileId = "f0"
    val allClusters = mutableMapOf(Pair("cluster0", listOf(fileId)))
    setUpQueryAllClustersResponseWith(allClusters)
    weFailToQuerySelectedFiles(listOf(fileId))

    // when
    breakOutOfThresholdClusters.breakIfNumberOfMultisetIsGreaterThanMultisetThreshold(defaultMaxMultisetThreshold)

  }

  @Test
  fun `do nothing if cluster multiset is small`() {
    // given
    val fileId = "f0"
    val file = DomainFile.builder().aggregateId(fileId).clusterCenter(true).inMultiset(true).build()
    val allClusters = mutableMapOf(Pair("cluster0", listOf(fileId)))
    setUpQueryAllClustersResponseWith(allClusters)
    setUpQuerySelectedFilesWith(listOf(fileId), listOf(file))

    // when
    breakOutOfThresholdClusters.breakIfNumberOfMultisetIsGreaterThanMultisetThreshold(defaultMaxMultisetThreshold)

    // then
    clusterIsNotDeleted()
  }

  @Test
  fun `log error if command to delete cluster fails`() {
    // given
    val fileId = "f0"
    val file = DomainFile.builder().aggregateId(fileId).clusterCenter(true).inMultiset(true).build()
    val clusterId = "cluster0"
    val allClusters = mutableMapOf(Pair(clusterId, listOf(fileId)))
    setUpQueryAllClustersResponseWith(allClusters)
    setUpQuerySelectedFilesWith(listOf(fileId), listOf(file))
    weFailToDeleteCluster(listOf(fileId), listOf(file))

    // when
    breakOutOfThresholdClusters.breakIfNumberOfMultisetIsGreaterThanMultisetThreshold(0)

  }

  @Test
  fun `delete cluster if multiset is too big`() {
    // given
    val fileId = "f0"
    val file = DomainFile.builder().aggregateId(fileId).clusterCenter(true).inMultiset(true).build()
    val clusterId = "cluster0"
    val allClusters = mutableMapOf(Pair(clusterId, listOf(fileId)))
    setUpQueryAllClustersResponseWith(allClusters)
    setUpQuerySelectedFilesWith(listOf(fileId), listOf(file))
    setupCommandRemoveFilesFromCluster(listOf(fileId), listOf(file))

    // when
    breakOutOfThresholdClusters.breakIfNumberOfMultisetIsGreaterThanMultisetThreshold(0)

    // then
    clusterIsDeleted(listOf(fileId), listOf(file))
  }

  @Test
  fun `do nothing if multiset is too big but cluster is locked`() {
    // given
    val fileId = "f0"
    val file = DomainFile.builder().aggregateId(fileId).clusterCenter(true).inMultiset(true).lock(true).build()
    val clusterId = "cluster0"
    val allClusters = mutableMapOf(Pair(clusterId, listOf(fileId)))
    setUpQueryAllClustersResponseWith(allClusters)
    setUpQuerySelectedFilesWith(listOf(fileId), listOf(file))
    setupCommandRemoveFilesFromCluster(listOf(fileId), listOf(file))

    // when
    breakOutOfThresholdClusters.breakIfNumberOfMultisetIsGreaterThanMultisetThreshold(0)

    // then
    clusterIsNotDeleted()
  }

  private fun clusterIsNotDeleted() {
    verify(commandBus, never()).dispatch<CommandResponse<Unit>>(ArgumentMatchers.any(RemoveSelectedFilesFromClustersCommand::class.java))
  }

  private fun clusterIsDeleted(fileIdsInCluster: List<String>, files: List<DomainFile>) {
    val clusterModified: Map<String, List<DomainFile>> = mapOf(Pair("cluster0", files))
    verify(commandBus).dispatch<CommandResponse<Unit>>(RemoveSelectedFilesFromClustersCommand(fileIdsInCluster, clusterModified, Room.WAITING_ROOM))
  }


  private fun weFailToQuerySelectedFiles(filesId: List<String>) {
    querySelectedFiles(filesId, TryOrCatch.failure(IllegalStateException()))
  }

  private fun weFailToQueryAllClusters() {
    queryAllClusters(TryOrCatch.failure(IllegalStateException()))
  }

  private fun setUpQueryAllClustersResponseWith(allClusters: Map<String, List<String>>) {
    queryAllClusters(TryOrCatch.of { allClusters })
  }

  private fun setUpQuerySelectedFilesWith(filesIdsInCluster: List<String>, response: List<DomainFile>) {
    querySelectedFiles(filesIdsInCluster, TryOrCatch.of { response })
  }

  private fun weFailToDeleteCluster(fileIdsInCluster: List<String>, files: List<DomainFile>) {
    val removeFilesFromClusterResponse = CommandResponse<Unit>(TryOrCatch.failure(IllegalStateException()))
    val clusterModified: Map<String, List<DomainFile>> = mapOf(Pair("cluster0", files))
    `when`(commandBus.dispatch<CommandResponse<Unit>>(RemoveSelectedFilesFromClustersCommand(fileIdsInCluster, clusterModified, Room.WAITING_ROOM))).thenReturn(
      removeFilesFromClusterResponse
    )
  }

  private fun setupCommandRemoveFilesFromCluster(fileIdsInCluster: List<String>, files: List<DomainFile>) {
    val removeFilesFromClusterResponse = CommandResponse(TryOrCatch.success(Unit))
    val clusterModified: Map<String, List<DomainFile>> = mapOf(Pair("cluster0", files))
    `when`(commandBus.dispatch<CommandResponse<Unit>>(RemoveSelectedFilesFromClustersCommand(fileIdsInCluster, clusterModified, Room.WAITING_ROOM))).thenReturn(
      removeFilesFromClusterResponse
    )
  }

  private fun queryAllClusters(tryResponse: TryOrCatch<Map<String, List<String>>>) {
    val allClustersQueryResponse = QueryResponse(tryResponse)
    `when`(queryBus.dispatch<QueryResponse<Map<String, List<String>>>>(AllClustersQuery())).thenReturn(allClustersQueryResponse)
  }

  private fun querySelectedFiles(filesIdsInCluster: List<String>, tryResponse: TryOrCatch<List<DomainFile>>) {
    val selectedFilesQueryResponse = QueryResponse(tryResponse)
    `when`(queryBus.dispatch<QueryResponse<List<DomainFile>>>(SelectedFilesQuery(filesIdsInCluster))).thenReturn(selectedFilesQueryResponse)
  }

}
