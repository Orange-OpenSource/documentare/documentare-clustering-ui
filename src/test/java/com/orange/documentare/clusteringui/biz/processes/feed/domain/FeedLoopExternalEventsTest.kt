package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeedLoopEventListener
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.ClusteringServiceImpl
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.queries.QueryBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class FeedLoopExternalEventsTest {
  val queryBus: QueryBus = mock()
  val commandBus: CommandBus = mock()
  val parameters: Parameters = Parameters.defaults()
  val clusteringService: ClusteringServiceImpl = mock()
  private val takeFilesRandomly: (List<DomainFile>, Int) -> List<String> = { _: List<DomainFile>, _: Int -> emptyList() }

  private val decrementMultisetThresholdAndBreakClusters: DecrementMultisetThresholdAndBreakClusters = mock()
  private val feedLoop =
    FeedLoop(queryBus, commandBus, { parameters }, clusteringService, takeFilesRandomly, FeedLoopEventListener.noOpListener(), decrementMultisetThresholdAndBreakClusters)

  @Test
  fun `reset decremental multiset breaker when a new file is created`() {
    // when
    feedLoop.onNewFileCreated()

    // then
    verify(decrementMultisetThresholdAndBreakClusters).reset()
  }

  @Test
  fun `takes into account a request to stop the feed loop`() {
    // when
    then(feedLoop.stopFeederWhenPossible).isFalse
    feedLoop.onStopFeederWhenPossible()

    // then
    then(feedLoop.stopFeederWhenPossible).isTrue
  }
}
