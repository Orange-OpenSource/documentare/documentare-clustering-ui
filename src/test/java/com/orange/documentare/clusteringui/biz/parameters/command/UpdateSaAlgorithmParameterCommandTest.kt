package com.orange.documentare.clusteringui.biz.parameters.command

import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.infrastructure.persistence.ParametersInMemoryRepository
import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test


class UpdateSaAlgorithmParameterCommandTest {

  private val repo = ParametersInMemoryRepository()
  private val handler = UpdateParametersCommandHandler(repo)


  @Test
  fun can_select_DivSufSort_algorithm() {
    // given
    val cmd = UpdateParametersCommand.validExample()
      .suffixArrayAlgorithm(SuffixArrayAlgorithm.LIBDIVSUFSORT)
      .build()

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isTrue
    then(repo.findAll().iterator().hasNext()).isTrue
    then(repo.findAll().iterator().next().suffixArrayAlgorithm).isEqualTo(SuffixArrayAlgorithm.LIBDIVSUFSORT)
  }

  @Test
  fun can_select_SAIS_algorithm() {
    // given
    var cmd = UpdateParametersCommand.validExample()
      .suffixArrayAlgorithm(SuffixArrayAlgorithm.LIBDIVSUFSORT)
      .build()
    handler.execute(cmd)

    cmd = UpdateParametersCommand.validExample()
      .suffixArrayAlgorithm(SuffixArrayAlgorithm.SAIS)
      .build()

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isTrue
    then(repo.findAll()).hasSize(1)
    then(repo.findAll().iterator().next()).isEqualTo(
      Parameters.validExample()
        .id(0)
        .suffixArrayAlgorithm(SuffixArrayAlgorithm.SAIS)
        .build()
    )
  }

}
