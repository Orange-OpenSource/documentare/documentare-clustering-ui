package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.nhaarman.mockitokotlin2.doCallRealMethod
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeedLoopEventListener
import org.junit.jupiter.api.Test

class FeedLoopWithSortRoomTest {

  private val parameters = Parameters.defaults()
  private var feedLoop: FeedLoop = mock()

  @Test
  fun `stop feeder when requested`() {
    // given
    val feedLoopCount = 0
    doCallRealMethod().`when`(feedLoop).onStopFeederWhenPossible()
    doCallRealMethod().`when`(feedLoop).feedWithSortRoom(feedLoopCount, parameters, FeedLoopEventListener.noOpListener())

    // when
    feedLoop.onStopFeederWhenPossible()
    feedLoop.feedWithSortRoom(0, parameters, FeedLoopEventListener.noOpListener())

    // then
    verify(feedLoop, never()).moveSingletonsNotLockedToSortRoom()
    verify(feedLoop, never()).computeDistancesThenRecalculateModifiedClusters(parameters)
    verify(feedLoop, never()).partialClustering(parameters)
  }
}
