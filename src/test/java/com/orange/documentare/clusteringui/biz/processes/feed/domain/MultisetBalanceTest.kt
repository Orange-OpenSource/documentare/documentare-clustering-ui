package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class MultisetBalanceTest {

  @Test
  fun `is NOT finished if there are files in the waiting room`() {
    then(MultisetBalance.get(filesPresentInWaitingRoom(), multisetMinThresholdNotReached(), singletonsChangedSinceLastLoop()).isFinished()).isFalse
  }

  @Test
  fun `is NOT finished if multiset min threshold has not been reached`() {
    then(MultisetBalance.get(noFilesInWaitingRoom(), multisetMinThresholdNotReached(), singletonsChangedSinceLastLoop()).isFinished()).isFalse
  }

  @Test
  fun `is NOT finished if singletons changed since last loop`() {
    then(MultisetBalance.get(noFilesInWaitingRoom(), multisetMinThresholdReached(), singletonsChangedSinceLastLoop()).isFinished()).isFalse
  }

  @Test
  fun `is finished if waiting room is empty AND we reached min multiset threshold AND singletons did NOT changed since last loop`() {
    then(MultisetBalance.get(noFilesInWaitingRoom(), multisetMinThresholdReached(), singletonsDidNotChangedSinceLastLoop()).isFinished()).isTrue
  }


  private fun singletonsChangedSinceLastLoop(): Boolean = true
  private fun singletonsDidNotChangedSinceLastLoop(): Boolean = false

  private fun multisetMinThresholdNotReached(): Boolean = false

  private fun multisetMinThresholdReached(): Boolean = true

  private fun noFilesInWaitingRoom(): List<DomainFile> = emptyList()

  private fun filesPresentInWaitingRoom(): List<DomainFile> = listOf(DomainFile.builder().build())
}
