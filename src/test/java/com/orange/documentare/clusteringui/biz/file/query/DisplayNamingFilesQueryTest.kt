package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.event.ClusterLocked
import com.orange.documentare.clusteringui.biz.file.event.ClusterPutInNamedClass
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.Event
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class DisplayNamingFilesQueryTest {
  var projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  var handler = DisplayNamingFilesQueryHandler(projections)

  @Test
  fun no_cluster_named() {
    // when
    val response = handler.execute(DisplayNamingFilesQuery())

    // then
    then(response.success()).isTrue()
    then(response.value().noClusters).isTrue()
  }

  @Test
  fun a_locked_cluster_without_className_can_be_named() {
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "titi", "10", true),
      ClusterLocked(aggregateIdExample(0), "10", true)
    )
    projections.testPurposeInitFromHistory(events)

    // when
    val response = handler.execute(DisplayNamingFilesQuery())

    // then
    then(response.success()).isTrue()
    val `val` = response.value()
    then(`val`.noClusters).isFalse()
    then(`val`.sortedMapClusterByListSize).hasSize(1)
    then(`val`.classNameArrays).isEmpty()
  }

  @Test
  fun a_locked_cluster_with_className_can_not_be_named() {
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "titi", "10", true),
      ClusterLocked(aggregateIdExample(0), "10", true),
      ClusterPutInNamedClass(aggregateIdExample(0), "10", "test")
    )
    projections.testPurposeInitFromHistory(events)

    // when
    val response = handler.execute(DisplayNamingFilesQuery())

    // then
    then(response.success()).isTrue()
    val value = response.value()
    then(value.noClusters).isTrue()
    then(value.sortedMapClusterByListSize).hasSize(0)
    then(value.classNameArrays[0]).isEqualTo("test")
  }
}
