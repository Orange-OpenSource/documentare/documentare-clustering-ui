package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.event.FileMovedFromWaitingRoomToSortRoom
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Command MoveFileFromWaitingRoomToSortRoom")
class MoveFileFromWaitingRoomToSortRoomCommandTest {
  /**
   * init eventBus with event repository so that events will be persisted automatically
   */
  var eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  var eventBus = EventBus(eventRepo)
  var handler = MoveFileFromWaitingRoomToSortRoomCommandHandler(eventRepo, eventBus)

  @Test
  fun a_move_file_from_waiting_room_to_sort_room_command_generates_a_FileMovedFromWaitingRoomToSortRoom_event() {
    // given
    val initialEvents = mutableListOf<Event>(
      FileCreated(aggregateIdExample(0), "f0", true),
      FileCreated(aggregateIdExample(1), "f1", true)
    )
    eventRepo.insert(initialEvents)
    val cmd = MoveFileFromWaitingRoomToSortRoomCommand(listOf(aggregateIdExample(0)))

    // when
    val response: CommandResponse<*> = handler.execute(cmd)

    // then
    then(response.success()).isTrue
    initialEvents.apply {
      this.add(FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(0)))
      then(eventRepo.findAll()).containsAll(this)
    }
  }
}
