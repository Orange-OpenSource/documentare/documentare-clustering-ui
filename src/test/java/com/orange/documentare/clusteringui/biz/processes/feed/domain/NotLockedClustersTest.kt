package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.nhaarman.mockitokotlin2.mock
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.AllClustersQuery
import com.orange.documentare.clusteringui.biz.file.query.SelectedFilesQuery
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`

class NotLockedClustersTest {

  private val queryBus: QueryBus = mock()
  private val notLockedClusters = NotLockedClusters(queryBus)

  @Test
  fun `if an error occurred while retrieving all clusters, log an error safely`() {
    // given
    `when`(queryBus.dispatch<QueryResponse<Map<String, List<String>>>>(AllClustersQuery())).thenReturn(QueryResponse(TryOrCatch.failure(IllegalStateException("an error occurred"))))

    // when
    val maxMultiset = notLockedClusters.countMaxMultiset()

    // then
    then(maxMultiset).isEqualTo(0)
  }

  @Test
  fun `returns the size of the larger multiset`() {
    // given
    val fileIDs = listOf("file1Id", "file2Id")
    allClustersQueryReturns(mutableMapOf(Pair("cluster1", fileIDs)))

    val file1 = DomainFile.builder().aggregateId("file1Id").build()
    val file2 = DomainFile.builder().aggregateId("file2Id").inMultiset(true).build()
    selectedFilesQueryReturns(fileIDs, listOf(file1, file2))

    queryBus.dispatch<QueryResponse<List<DomainFile>>>(SelectedFilesQuery(fileIDs))


    // when
    val maxMultiset = notLockedClusters.countMaxMultiset()

    // then
    then(maxMultiset).isEqualTo(1)
  }

  @Test
  fun `returns the size of the larger multiset, BUT excludes locked cluster`() {
    // given
    val fileIDs = listOf("file1Id", "file2Id")
    allClustersQueryReturns(mutableMapOf(Pair("cluster1", fileIDs)))

    val file1 = DomainFile.builder().aggregateId("file1Id").lock(true).build()
    val file2 = DomainFile.builder().aggregateId("file2Id").inMultiset(true).build()
    selectedFilesQueryReturns(fileIDs, listOf(file1, file2))

    queryBus.dispatch<QueryResponse<List<DomainFile>>>(SelectedFilesQuery(fileIDs))


    // when
    val maxMultiset = notLockedClusters.countMaxMultiset()

    // then
    then(maxMultiset).isEqualTo(0)
  }

  private fun selectedFilesQueryReturns(fileIDs: List<String>, filesInCluster: List<DomainFile>) {
    `when`(queryBus.dispatch<QueryResponse<List<DomainFile>>>(SelectedFilesQuery(fileIDs))).thenReturn(QueryResponse(
      TryOrCatch.of {
        filesInCluster
      }
    ))
  }

  private fun allClustersQueryReturns(allClusters: Map<String, List<String>>) {
    `when`(queryBus.dispatch<QueryResponse<Map<String, List<String>>>>(AllClustersQuery())).thenReturn(QueryResponse(
      TryOrCatch.of { allClusters }
    ))
  }
}
