package com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.orange.documentare.clusteringui.Couple
import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource
import org.mockito.Mockito.`when`

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

internal class ClusteringRemoteTest {

  private val clusteringRemote = mock<ClusteringRemote>()

  @ParameterizedTest
  @EnumSource(SuffixArrayAlgorithm::class)
  fun `consolidate cluster parameters are built correctly`(suffixArrayAlgorithm: SuffixArrayAlgorithm) {
    // given
    val parameters = Parameters.builder().pCount(1234).suffixArrayAlgorithm(suffixArrayAlgorithm).build()

    // when
    val consolidateClusterParams = ClusteringRemote(EmbeddedComputationServer.disabled()).consolidateClusterParams(parameters)

    // then
    val expectedParameters = Parameters.builder().pCount(1234).suffixArrayAlgorithm(suffixArrayAlgorithm).consolidateCluster(true).build()
    then(consolidateClusterParams).isEqualTo(expectedParameters)
  }

  @ParameterizedTest
  @EnumSource(SuffixArrayAlgorithm::class)
  fun `clustering request takes suffix array algorithm parameter, when consolidating cluster`(suffixArrayAlgorithm: SuffixArrayAlgorithm) {
    // given
    val parameters = Parameters.consolidateCluster(1234, suffixArrayAlgorithm)

    // when
    val clusteringRequest = ClusteringRemote(EmbeddedComputationServer.disabled()).buildClusteringRequest(parameters, emptyList())

    // then
    then(clusteringRequest.suffixArrayAlgorithm).isEqualTo(suffixArrayAlgorithm)
  }

  @Test
  fun `creating a cluster requires that we set the consolidateCluster parameter`() {
    // given
    val files = listOf(DomainFile.builder().build())
    val parameters = Parameters.defaults()
    `when`(clusteringRemote.createCluster(parameters, files)).thenCallRealMethod()
    `when`(clusteringRemote.genericCalculateClustering(any(), any())).thenReturn(mutableMapOf(Pair(mock(), mock())))

    // when
    clusteringRemote.createCluster(parameters, files)

    val expectedParameters = Parameters.consolidateCluster(parameters.pCount, parameters.suffixArrayAlgorithm)
    // then
    verify(clusteringRemote).genericCalculateClustering(expectedParameters, listOf(Couple(PreviousClusterId.none(), files)))
  }

  @Test
  fun `recalculating a cluster requires that we set the consolidateCluster parameter`() {
    // given
    val files = listOf(DomainFile.builder().build())
    val oneCluster = listOf(Couple(PreviousClusterId("10"), files))
    val parameters = Parameters.defaults()
    `when`(clusteringRemote.recalculateClusters(parameters, oneCluster)).thenCallRealMethod()

    // when
    clusteringRemote.recalculateClusters(parameters, oneCluster)

    val expectedParameters = Parameters.consolidateCluster(parameters.pCount, parameters.suffixArrayAlgorithm)
    // then
    verify(clusteringRemote).genericCalculateClustering(expectedParameters, oneCluster)
  }

  @Test
  fun `calculating clustering redirects to proper generic function`() {
    // given
    val files = listOf(DomainFile.builder().build())
    val parameters = Parameters.defaults()
    `when`(clusteringRemote.calculateClustering(parameters, files)).thenCallRealMethod()
    `when`(clusteringRemote.genericCalculateClustering(any(), any())).thenReturn(mutableMapOf(Pair(mock(), mock())))

    // when
    clusteringRemote.calculateClustering(parameters, files)

    // then
    verify(clusteringRemote).genericCalculateClustering(parameters, listOf(Couple(PreviousClusterId.none(), files)))
  }

  @Test
  fun `calculating clustering build requests taking into account all possible parameters`() {
    // given
    val pCount = 100
    val acutSdFactor = 1.0f
    val qcutSdFactor = 1.1f
    val scutSdFactor = 1.2f
    val ccutPercentile = 80
    val knnThreshold = 6
    val enrollAcutSdFactor = 2.0f
    val enrollQcutSdFactor = 2.1f
    val enrollScutSdFactor = 2.2f
    val enrollCcutPercentile = 90
    val enrollKnnThreshold = 7
    val suffixArrayAlgorithm = SuffixArrayAlgorithm.SAIS
    val p = Parameters.builder()
      .rawConverter(true)
      .pCount(pCount)
      .acutSdFactor(acutSdFactor)
      .qcutSdFactor(qcutSdFactor)
      .scutSdFactor(scutSdFactor)
      .ccutPercentile(ccutPercentile)
      .knnThreshold(knnThreshold)
      .sloop(true)
      .consolidateCluster(true)
      .enroll(true)
      .enrollAcutSdFactor(enrollAcutSdFactor)
      .enrollQcutSdFactor(enrollQcutSdFactor)
      .enrollScutSdFactor(enrollScutSdFactor)
      .enrollCcutPercentile(enrollCcutPercentile)
      .enrollKnnThreshold(enrollKnnThreshold)
      .suffixArrayAlgorithm(suffixArrayAlgorithm)
      .build()

    val file = DomainFile.builder().aggregateId("id0").filename("file0").build()
    val files = listOf(file)
    `when`(clusteringRemote.genericCalculateClustering(p, listOf(Couple(PreviousClusterId("10"), files)))).thenCallRealMethod()
    `when`(clusteringRemote.bytesDataOf(files)).thenReturn(emptyArray())
    `when`(clusteringRemote.doRequest(any(), any(), any())).thenReturn(mutableMapOf())
    val urls = listOf("http://toto")
    ClusteringRemote.initRemoteSimdocServerForClustering(urls)
    FileIO.initFilesDirectory("/tmp")

    // when
    clusteringRemote.genericCalculateClustering(p, listOf(Couple(PreviousClusterId("10"), files)))

    // then
    val expectedClusteringRequestUI = ClusteringRequestUI.builder()
      .bytesData(emptyArray())
      .outputDirectory("/tmp//output/")
      .rawConverter(true)
      .pCount(pCount)
      .acut(acutSdFactor)
      .qcut(qcutSdFactor)
      .scut(scutSdFactor)
      .ccut(ccutPercentile)
      .knnThreshold(knnThreshold)
      .sloop()
      .consolidateCluster()
      .enroll()
      .enrollAcut(enrollAcutSdFactor)
      .enrollQcut(enrollQcutSdFactor)
      .enrollScut(enrollScutSdFactor)
      .enrollCcut(enrollCcutPercentile)
      .enrollK(enrollKnnThreshold)
      .suffixArrayAlgorithm(suffixArrayAlgorithm)
      .build()
    verify(clusteringRemote).doRequest(urls, p, listOf(Couple(PreviousClusterId("10"), expectedClusteringRequestUI)))
  }

  @Test
  fun `throw exception if remote urls are not set`() {
    // given
    `when`(clusteringRemote.genericCalculateClustering(any(), any())).thenCallRealMethod()

    // when
    try {
      clusteringRemote.genericCalculateClustering(Parameters.defaults(), emptyList())
    } catch (e: IllegalStateException) {
      // then
      then(e.message).isEqualTo("remote simdoc servers urls are not set")
    }
  }
}
