package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.domain.AssociateFile
import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.ClusterModified
import com.orange.documentare.clusteringui.biz.file.event.DistancesToClusterCenterAdded
import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.event.TestEventHandler
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Command AddFilesToClosestCluster")
class AddFilesToClustersCommandTest {
  var eventBus = EventBus(DBEventInMemoryRepositoryForTestPurpose())
  var testEventHandler = TestEventHandler()
  var eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  var handler = AddFilesToClustersCommandHandler(eventRepo, eventBus)

  @BeforeEach
  fun setup() {
    eventBus.register(testEventHandler)
  }

  @Test
  fun file_is_added_to_its_closest_cluster() {
    // given
    val clusterCenter = DomainFile.builder().aggregateId(aggregateIdExample(0)).filename("0").clusterId("10").clusterCenter(true).build()
    val fileToAdd = DomainFile.builder().aggregateId(aggregateIdExample(1)).filename("1")
      .distancesToClustersCenters(
        listOf(DistanceToCluster(100, "10"))
      )
      .build()
    eventRepo.insert(
      listOf(
        FileCreated(clusterCenter.aggregateId, clusterCenter.filename, "10", true),
        FileCreated(fileToAdd.aggregateId, fileToAdd.filename),
        DistancesToClusterCenterAdded(fileToAdd.aggregateId, listOf(DistanceToCluster(100, "10")))
      )
    )

    val associateFiles = listOf(AssociateFile(fileToAdd.aggregateId, clusterCenter.aggregateId))
    val cmd = AddFilesToClustersCommand(associateFiles)

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isTrue()
    then(testEventHandler.events()).containsExactly(
      FileAddedToCluster.builder().aggregateId(fileToAdd.aggregateId).clusterId("10").distanceToCenter(100).build(),
      ClusterModified(clusterCenter.aggregateId, "10")
    )
  }
}
