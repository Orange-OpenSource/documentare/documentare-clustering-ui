package com.orange.documentare.clusteringui.biz.parameters.command

import com.orange.documentare.clusteringui.biz.parameters.command.UpdateParametersCommand.ParametersValidationException
import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import org.assertj.core.api.BDDAssertions.catchException
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class UpdateParametersCommandValidationTest {

  @Test
  fun `invalid parameters throws exception`() {
    check(builder().rawConverter(true).build(), "raw converter is enabled but pCount is not valid: null")
    check(builder().feed(true).build(), "feed is enabled but feedThreshold is not valid: null")
    check(builder().build(), "suffixArrayAlgorithm is not valid: null")
    check(builder().suffixArrayAlgorithm(SuffixArrayAlgorithm.LIBDIVSUFSORT).build(), "thresholdFactor is not valid: null")

    check(
      builder()
        .suffixArrayAlgorithm(SuffixArrayAlgorithm.LIBDIVSUFSORT)
        .thresholdFactor(1f)
        .build(), "minMultisetThreshold is not valid: null"
    )
    check(
      builder()
        .suffixArrayAlgorithm(SuffixArrayAlgorithm.LIBDIVSUFSORT)
        .thresholdFactor(1f)
        .minMultisetThreshold(1)
        .build(), "maxMultisetThreshold is not valid: null"
    )
    check(
      builder()
        .suffixArrayAlgorithm(SuffixArrayAlgorithm.LIBDIVSUFSORT)
        .thresholdFactor(1f)
        .minMultisetThreshold(2)
        .maxMultisetThreshold(1)
        .build(), "minMultisetThreshold(2) should not be lower than maxMultisetThreshold(1)"
    )
  }

  private fun check(cmd: UpdateParametersCommand, expectedError: String) {
    val exception = catchException { cmd.validate() }
    then(exception).isInstanceOf(ParametersValidationException::class.java)
    then(exception.message).isEqualTo(expectedError)
  }

  private fun builder(): UpdateParametersCommand.UpdateParametersCommandBuilder =
    UpdateParametersCommand.builder()
}
