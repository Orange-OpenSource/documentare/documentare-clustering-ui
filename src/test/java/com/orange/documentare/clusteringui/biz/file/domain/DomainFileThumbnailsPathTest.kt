package com.orange.documentare.clusteringui.biz.file.domain

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile.Companion.builder
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import java.util.stream.Collectors

class DomainFileThumbnailsPathTest {
  @Test
  fun regroup_thumbnails_web_path_per_cluster_id() {
    // given
    val files: MutableList<DomainFile> = ArrayList()
    files.add(builder().aggregateId("000000000000000000000001").clusterId("1").build())
    files.add(builder().aggregateId("000000000000000000000002").clusterId("1").build())
    files.add(builder().aggregateId("000000000000000000000003").clusterId("2").build())
    files.add(builder().aggregateId("000000000000000000000004").clusterId("2").build())

    // when
    val filesOrderedByClusterId: Map<String, List<DomainFile>> = files.stream()
      .collect(Collectors.groupingBy { (_, _, clusterId) -> clusterId })

    val thumbnailsPathOrderedByClusterId: MutableMap<String, List<String>> = HashMap()

    filesOrderedByClusterId.forEach { (clusterId: String, domainFiles: List<DomainFile>) ->
      thumbnailsPathOrderedByClusterId[clusterId] = domainFiles
        .map { FileIO.thumbnailWebPathOf(it) }
    }

    // then
    then(thumbnailsPathOrderedByClusterId["1"]).containsExactly("/img/no_image.png", "/img/no_image.png")
    then(thumbnailsPathOrderedByClusterId["2"]).containsExactly("/img/no_image.png", "/img/no_image.png")
  }
}
