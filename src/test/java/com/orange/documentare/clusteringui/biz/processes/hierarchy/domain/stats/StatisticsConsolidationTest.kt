package com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats

import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateExampleIndex
import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.StatisticsConsolidation.Companion.collectDistancesToCompute
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.StatisticsConsolidation.ComputedDistancesExamples
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.StatisticsConsolidation.ComputedDistancesExamples.a_
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.StatisticsConsolidation.ComputedDistancesExamples.cca_
import com.orange.documentare.clusteringui.biz.processes.hierarchy.transport.HierarchyService
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceService
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModel
import org.assertj.core.api.BDDAssertions.catchThrowable
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class StatisticsConsolidationTest {

  @Test
  fun `collect distances to compute`() {
    // given
    val exportedClientHierarchies = HierarchyExamples.exportedClientHierarchiesForStatisticsConsolidation()

    // when
    val distancesToCompute = collectDistancesToCompute(exportedClientHierarchies)

    // then
    // TODO CLIENT +++ we should not compute distance to the same element
    then(distancesToCompute.distancesToCompute).isEqualTo(
      listOf(
        // level 1
        Pair(
          1, mapOf(
            Pair(cca_(0, 1), listOf(a_(0), a_(1), a_(2), a_(3), a_(4), a_(5), a_(6), a_(7))),
            Pair(cca_(4, 1), listOf(a_(4), a_(5), a_(6)))
          )
        ),
        // level 2
        Pair(
          2, mapOf(
            Pair(cca_(0, 2), listOf(a_(0), a_(1), a_(2), a_(3), a_(4), a_(5), a_(6), a_(7))),
          )
        )
      )
    )
    println(distancesToCompute.toString())
    then(distancesToCompute.toString()).isEqualTo(
      "\n" +
        "\n" +
        "Distances to compute for cluster statistics:\n" +
        "\n" +
        "\tLevel 1:\n" +
        "\t\tcentroid of cluster 'f0'\t->\t[f0, f1, f2, f3, f4, f5, f6, f7]\n" +
        "\t\tcentroid of cluster 'f4'\t->\t[f4, f5, f6]\n" +
        "\n" +
        "\tLevel 2:\n" +
        "\t\tcentroid of cluster 'f0'\t->\t[f0, f1, f2, f3, f4, f5, f6, f7]\n"
    )
  }

  @Test
  fun `compute collected distances`() {
    // given
    val exportedClientHierarchies = HierarchyExamples.exportedClientHierarchiesForStatisticsConsolidation()
    val hierarchyService = HierarchyService()
    val clientHierarchyExport = object : ClientHierarchyExport {
      override fun exportClientHierarchy(): ClientHierarchyReferenceModel {
        TODO("Not yet implemented")
      }

      override fun exportClientHierarchyForStatisticsConsolidation(): ClientHierarchyReferenceModel {
        return exportedClientHierarchies
      }
    }
    hierarchyService.setDistanceService(fakeDistanceService())
    hierarchyService.setClientHierarchyExport(clientHierarchyExport)

    // when - check behaviour when result is not yet available
    then(hierarchyService.computedDistancesAvailable()).isFalse
    val throwable = catchThrowable(hierarchyService::computedDistances)
    then(throwable.message).isEqualTo("Computed Distances not available yet, call computedDistancesAvailable() to check first")

    // when - launch computation and wait for it to finish
    hierarchyService.launchHierarchyStatisticsConsolidation()
    waitFor(hierarchyService)
    val computedDistances = hierarchyService.computedDistances()

    // then
    then(computedDistances).isEqualTo(ComputedDistancesExamples.computedDistancesWithTwoLevels)
  }

  private fun waitFor(hierarchyService: HierarchyService) {
    while (!hierarchyService.computedDistancesAvailable()) {
      Thread.sleep(10)
    }
  }

  private fun fakeDistanceService() = object : DistanceService {
    override fun isRunning(): Boolean {
      TODO("Not yet implemented")
    }

    override fun isDistanceBeingComputedFor(id: String): Boolean {
      TODO("Not yet implemented")
    }

    override fun computeDistancesOf(parameters: Parameters, fileIds: List<String>) {
      TODO("Not yet implemented")
    }

    override fun computeDistancesOfInSameThread(parameters: Parameters, fileIds: List<String>) {
      TODO("Not yet implemented")
    }

    override fun computeDistancesToCentroidAnywhereInHierarchyInSameThread(files: List<AggregateId>, centroidMultisetFiles: List<AggregateId>): List<Pair<AggregateId, Int>> {
      return files.map { it to aggregateExampleIndex(it) }
    }

  }
}
