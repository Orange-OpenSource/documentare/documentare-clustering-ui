package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.event.TestEventHandler
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Command CreateCluster")
class CreateClusterCommandTest {
  var eventBus = EventBus(DBEventInMemoryRepositoryForTestPurpose())
  var testEventHandler = TestEventHandler()
  var eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  var handler = CreateClusterCommandHandler(eventRepo, eventBus)

  @BeforeEach
  fun setup() {
    eventBus.register(testEventHandler)
  }

  @Test
  fun create_singleton_cluster() {
    // given
    val id = DomainFile.builder().aggregateId(aggregateIdExample(0)).filename("0").build().aggregateId
    eventRepo.insert(listOf(FileCreated(id, id)))
    val cmd = CreateClusterCommand(listOf(id), "2")

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isTrue()
    then(testEventHandler.events()).containsExactly(
      FileAddedToCluster.builder().aggregateId(id).clusterId("2").clusterCenter(true).build()
    )
  }

  @Test
  fun create_singleton_with_two_files() {
    // given
    val id1 = DomainFile.builder().aggregateId(aggregateIdExample(1)).filename("f0").build().aggregateId
    val id2 = DomainFile.builder().aggregateId(aggregateIdExample(2)).filename("f1").build().aggregateId
    eventRepo.insert(
      listOf(
        FileCreated(id1, "f0"),
        FileCreated(id2, "f1")
      )
    )
    val cmd = CreateClusterCommand(listOf(id1, id2), "3")

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isTrue()
    then(testEventHandler.events()).containsExactly(
      FileAddedToCluster.builder().aggregateId(id2).clusterId("3").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(id1).clusterId("3").build()
    )
  }
}
