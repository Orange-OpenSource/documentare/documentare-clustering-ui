package com.orange.documentare.clusteringui.biz.file.query.projection

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.MultisetElementMovedToNextHierarchyLevel
import com.tophe.ddd.infrastructure.event.Event
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Query Files Projection Hierarchy")
class FilesProjectionHierarchyTest {
  private val aggregateId = "12345"
  private val projection = FilesProjection()

  @Test
  fun `files projection is initialized with ONE hierarchy level, and next level has value 1`() {
    checkCurrentHierarchyLevelIs(projection, 0)
  }

  @Test
  fun `after a reset, files projection is initialized with ONE hierarchy level, and next level has value 1`() {
    // given
    initProjectionWith(
      listOf(
        FileCreated(aggregateId, "f1"),
        FileAddedToCluster.builder().aggregateId(aggregateId).clusterId("10").build()
      )
    )
    simulateEventToCreateANewHierarchy()
    checkCurrentHierarchyLevelIs(projection, 1)

    // when
    projection.reset()

    // then
    checkCurrentHierarchyLevelIs(projection, 0)
    then(projection.currentHierarchyLevel().filesCount()).isEqualTo(0)
  }

  @Test
  fun `after a hierarchy reset, we should keep only initial first hierarchy level`() {
    // given
    then(projection.currentHierarchyLevel().ids()).isEmpty()

    initProjectionWith(
      listOf(
        FileCreated(aggregateId, "f1"),
        FileAddedToCluster.builder().aggregateId(aggregateId).clusterId("10").build()
      )
    )
    simulateEventToCreateANewHierarchy()

    checkCurrentHierarchyLevelIs(projection, 1)
    then(projection.currentHierarchyLevel().ids()).isEqualTo(listOf(aggregateId))

    // when
    projection.keepOnlyFirstHierarchyLevel()

    // then
    checkCurrentHierarchyLevelIs(projection, 0)
    then(projection.currentHierarchyLevel().ids()).isEqualTo(listOf(aggregateId))
  }

  @Test
  fun `on MultisetElementMovedToNextHierarchyLevel then create new hierarchy level if it does not exist`() {
    // given
    initProjectionWith(
      listOf(
        FileCreated(aggregateId, "f1"),
        FileAddedToCluster.builder().aggregateId(aggregateId).clusterId("10").build()
      )
    )

    // when
    simulateEventToCreateANewHierarchy()

    // then
    checkCurrentHierarchyLevelIs(projection, 1)
  }

  @Test
  fun `on MultisetElementMovedToNextHierarchyLevel then DO NOT create new hierarchy level if it exists yet`() {
    // given
    val aggregateId2 = "8890"
    initProjectionWith(
      listOf(
        FileCreated(aggregateId, "f1"),
        FileAddedToCluster.builder().aggregateId(aggregateId).clusterId("10").build(),
        FileCreated(aggregateId2, "f2"),
        FileAddedToCluster.builder().aggregateId(aggregateId2).clusterId("20").build()
      )
    )

    val nextHierarchyLevel = projection.nextHierarchyLevel()
    projection.onEvent(
      MultisetElementMovedToNextHierarchyLevel(
        aggregateId,
        nextHierarchyLevel
      )
    )

    // when
    projection.onEvent(
      MultisetElementMovedToNextHierarchyLevel(
        aggregateId2,
        nextHierarchyLevel
      )
    )

    // then
    checkCurrentHierarchyLevelIs(projection, 1)
  }

  @Test
  fun `on MultisetElementMovedToNextHierarchyLevel then copy file to new hierarchy, and clear cluster attributes and put in Waiting Room`() {
    // given
    val filename = "f1"
    initProjectionWith(
      listOf(
        FileCreated(aggregateId, filename),
        FileAddedToCluster.builder().aggregateId(aggregateId).clusterId("10").build()
      )
    )

    // when
    simulateEventToCreateANewHierarchy()

    // then
    then(projection[aggregateId]).isEqualTo(DomainFile.builder().aggregateId(aggregateId).filename(filename).inWaitingRoom(true).build())
  }

  private fun simulateEventToCreateANewHierarchy() {
    projection.onEvent(
      MultisetElementMovedToNextHierarchyLevel(
        aggregateId,
        projection.nextHierarchyLevel()
      )
    )
  }

  private fun initProjectionWith(events: List<Event>) {
    projection.initFromHistory(events)
  }

  private fun checkCurrentHierarchyLevelIs(projection: FilesProjection, currentHierarchyLevelId: Int) {
    then(projection.currentHierarchyLevelIndex()).isEqualTo(currentHierarchyLevelId)
    then(projection.hierarchyLevelCount()).isEqualTo(currentHierarchyLevelId + 1)
    then(projection.nextHierarchyLevel()).isEqualTo(currentHierarchyLevelId + 1)
  }
}
