package com.orange.documentare.clusteringui.biz.file.query.hierarchy

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.ClusterUIView
import com.orange.documentare.clusteringui.biz.file.query.DisplayHierarchyLevelFileTransform
import com.orange.documentare.clusteringui.biz.file.query.DisplayHierarchyLevelFileTransform.Companion.CLASS_NO_NAME
import com.orange.documentare.clusteringui.biz.file.query.DisplayHierarchyLevelFileTransform.Companion.orderClustersWithFileViewsBySize
import com.orange.documentare.clusteringui.biz.file.query.DisplayHierarchyLevelFileTransform.Companion.orderFilesInClusterByBizCriteria
import com.orange.documentare.clusteringui.biz.file.query.FileUIView
import com.orange.documentare.clusteringui.biz.file.query.projection.FilesProjection
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.Event
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class DisplayHierarchyLevelFileTransformTest {

  private val clusterId = "100"
  private val filesIDs = listOf("1", "2", "3", "4", "5", "6")
  private val clustersWithFilesIDs = mutableMapOf(Pair(clusterId, filesIDs))
  private val filesProjection = FilesProjection()

  @Test
  fun `order files in cluster, cluster center first, the rest in IDs reversed order`() {
    // given
    val expectedFileIDsOrder = listOf("3", "6", "5", "4", "2", "1")

    val events = filesIDs.map { fileId ->
      val clusterCenter = fileId == "3"
      FileCreated(fileId, "filename-$fileId", clusterId, clusterCenter)
    }

    // when/then
    testOrderingFilesInCluster(events, expectedFileIDsOrder)
  }

  @Test
  fun `order files in cluster, cluster center first, then multiset elements, the rest in IDs reversed order`() {
    // given
    val expectedFileIDsOrder = listOf("3", "2", "6", "5", "4", "1")

    val events = filesIDs.flatMap { fileId ->
      when (fileId) {
        "3" -> listOf(FileCreated(fileId, "filename-$fileId", clusterId, true))
        "2" -> listOf(
          FileCreated(fileId, "filename-$fileId"),
          FileAddedToCluster.builder().aggregateId(fileId).clusterId(clusterId).inMultiset(true).build()
        )

        else -> {
          listOf(FileCreated(fileId, "filename-$fileId", clusterId, false))
        }
      }
    }

    // when/then
    testOrderingFilesInCluster(events, expectedFileIDsOrder)
  }

  @Test
  fun `order files in cluster, cluster center first, then multiset elements, then the rest in IDs reversed order, but files included automatically last`() {
    // given
    val expectedFileIDsOrder = listOf("3", "2", "6", "4", "1", "5")

    val events = filesIDs.flatMap { fileId ->
      when (fileId) {
        "3" -> listOf(FileCreated(fileId, "filename-$fileId", clusterId, true))
        "2" -> listOf(
          FileCreated(fileId, "filename-$fileId"),
          FileAddedToCluster.builder().aggregateId(fileId).clusterId(clusterId).inMultiset(true).build()
        )

        "5" -> listOf(
          FileCreated(fileId, "filename-$fileId"),
          FileAddedToCluster.builder().aggregateId(fileId).clusterId(clusterId).automaticInclude(true).build()
        )

        else -> {
          listOf(FileCreated(fileId, "filename-$fileId", clusterId, false))
        }
      }
    }

    // when/then
    testOrderingFilesInCluster(events, expectedFileIDsOrder)
  }


  @Test
  fun `order clusters with FileView by size`() {
    // given
    val oneFile = ClusterUIView("200", listOf(buildFileView("10")))
    val twoFiles = ClusterUIView("100", listOf(buildFileView("1"), buildFileView("2")))
    val threeFiles = ClusterUIView("400", listOf(buildFileView("3"), buildFileView("4"), buildFileView("5")))
    val fourFiles = ClusterUIView("300", listOf(buildFileView("6"), buildFileView("7"), buildFileView("8"), buildFileView("9")))
    val clustersWithFileViews = mutableMapOf(
      Pair("100", twoFiles),
      Pair("200", oneFile),
      Pair("300", fourFiles),
      Pair("400", threeFiles)
    )

    // when
    val orderedClusters = orderClustersWithFileViewsBySize(clustersWithFileViews)

    // then
    then(orderedClusters).isEqualTo(
      mutableListOf(
        Pair("300", fourFiles),
        Pair("400", threeFiles),
        Pair("100", twoFiles),
        Pair("200", oneFile)
      ).toMap()
    )
  }


  @Test
  internal fun `construct classes map with group of clusters and file view ordered`() {
    // given
    val events = listOf(
      FileCreated(aggregateIdExample(0), "filename-0"),
      FileCreated(aggregateIdExample(1), "filename-1"),
      FileCreated(aggregateIdExample(2), "filename-2"),
      FileCreated(aggregateIdExample(3), "filename-3"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("100").className("a").inMultiset(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("100").className("a").build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("100").className("a").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("100").className("a").automaticInclude(true).build(),

      FileCreated(aggregateIdExample(4), "filename-4"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(4)).clusterId("200").className("a").clusterCenter(true).build(),

      FileCreated(aggregateIdExample(5), "filename-5"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(5)).clusterId("300").className("b").clusterCenter(true).build(),

      FileCreated(aggregateIdExample(10), "filename-10"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(10)).clusterId("400").clusterCenter(true).build()
    )
    val projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
    projections.testPurposeInitFromHistory(events)

    val transform = DisplayHierarchyLevelFileTransform(projections.files.hierarchyLevel(0), projections.clusters.hierarchyLevel(0)) { FileUIView.from(it) }

    // when
    val classesWithFileViews = transform.mapClasses()

    // then
    then(classesWithFileViews.keys).containsExactly("a", "b", CLASS_NO_NAME)
    then(classesWithFileViews["a"]?.keys).containsExactly("100", "200")
    then(classesWithFileViews["b"]?.keys).containsExactly("300")
    then(classesWithFileViews[CLASS_NO_NAME]?.keys).containsExactly("400")
    then(classesWithFileViews["a"]?.get("100")?.fileUIViews?.map { it.aggregateId }).containsExactly(
      aggregateIdExample(2),
      aggregateIdExample(0),
      aggregateIdExample(3),
      aggregateIdExample(1)
    )
    then(classesWithFileViews["a"]?.get("200")?.fileUIViews?.map { it.aggregateId }).containsExactly(aggregateIdExample(4))
    then(classesWithFileViews["b"]?.get("300")?.fileUIViews?.map { it.aggregateId }).containsExactly(aggregateIdExample(5))
    then(classesWithFileViews[CLASS_NO_NAME]?.get("400")?.fileUIViews?.map { it.aggregateId }).containsExactly(aggregateIdExample(10))
  }

  @Test
  fun `if class names exist, use it directly without overriding it with multi-hypothesis generated class names`() {
    // given
    val knownClassesWithClusterIDs = mapOf(Pair("class A", listOf("cluster id 1")))
    val clusters = emptyList<ClusterUIView>()

    // when
    val retrieveClassesWithClusterIDs = DisplayHierarchyLevelFileTransform.retrieveClassesWithClusterIDs(knownClassesWithClusterIDs, clusters)

    // then
    then(retrieveClassesWithClusterIDs).isEqualTo(knownClassesWithClusterIDs)
  }

  @Test
  fun `if there is no known class name and there is a cluster with a multi-hypothesis, this cluster should appear in a class whose name is its multi-hypothesis`() {
    // given
    val knownClassesWithClusterIDs = emptyMap<String, List<String>>()
    val domainFile = DomainFile.builder().fileNamedClass(arrayOf("Covid")).build()
    val fileUIView = FileUIView.from(domainFile)
    val clusterUIView = ClusterUIView("clusterId-100", listOf(fileUIView))
    val clusters = listOf(clusterUIView)

    // when
    val retrieveClassesWithClusterIDs = DisplayHierarchyLevelFileTransform.retrieveClassesWithClusterIDs(knownClassesWithClusterIDs, clusters)

    // then
    val expectedClasses = mapOf(Pair("Covid 100%", listOf("clusterId-100")))
    then(retrieveClassesWithClusterIDs).isEqualTo(expectedClasses)
  }

  @Test
  fun `if there is no known class name and there is two clusters with distinct multi-hypothesis plus one cluster without multi-hypothesis, only multi-hypothesis clusters should appear in distinct classes whose name is the multi-hypothesis`() {
    // given
    val knownClassesWithClusterIDs = emptyMap<String, List<String>>()
    val domainFile1 = DomainFile.builder().fileNamedClass(arrayOf("Covid")).build()
    val domainFile2 = DomainFile.builder().fileNamedClass(arrayOf("Normal")).build()
    val domainFile3 = DomainFile.builder().build()
    val fileUIView1 = FileUIView.from(domainFile1)
    val fileUIView2 = FileUIView.from(domainFile2)
    val fileUIView3 = FileUIView.from(domainFile3)
    val clusterUIView1 = ClusterUIView("clusterId-12", listOf(fileUIView1))
    val clusterUIView2 = ClusterUIView("normal-34", listOf(fileUIView2))
    val clusterUIView3 = ClusterUIView("nothing-56", listOf(fileUIView3))
    val clusters = listOf(clusterUIView1, clusterUIView2, clusterUIView3)

    // when
    val retrieveClassesWithClusterIDs = DisplayHierarchyLevelFileTransform.retrieveClassesWithClusterIDs(knownClassesWithClusterIDs, clusters)

    // then
    val expectedClasses = mapOf(Pair("Covid 100%", listOf("clusterId-12")), Pair("Normal 100%", listOf("normal-34")))
    then(retrieveClassesWithClusterIDs).isEqualTo(expectedClasses)
  }

  private fun buildFileView(fileId: String) = FileUIView.from(
    DomainFile.builder()
      .aggregateId(fileId)
      .filename("filename-$fileId")
      .build()
  )

  private fun testOrderingFilesInCluster(events: List<Event>, expectedFileIDsOrder: List<String>?) {
    filesProjection.initFromHistory(events)

    // when
    val filesOrderedInCluster = orderFilesInClusterByBizCriteria(clustersWithFilesIDs, filesProjection.hierarchyLevel(0))

    // then
    val idsOrderedInCluster = filesOrderedInCluster[clusterId]!!.map { it.aggregateId }
    then(idsOrderedInCluster).isEqualTo(expectedFileIDsOrder)
  }
}
