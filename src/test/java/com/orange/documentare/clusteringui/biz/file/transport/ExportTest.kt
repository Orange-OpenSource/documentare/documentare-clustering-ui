package com.orange.documentare.clusteringui.biz.file.transport

import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.DisplayHomeFilesView
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.tophe.ddd.queries.QueryBus
import org.assertj.core.api.BDDAssertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

internal class ExportTest {

  @TempDir
  lateinit var tempDir: File

  @BeforeEach
  fun setup() {
    FileIO.initFilesDirectory(tempDir.absolutePath)
  }

  @Test
  fun `export to csv without named class`() {
    // given
    val expectedCSV = File(javaClass.getResource("/expected_exported_classes.csv")!!.file)
    val clustersInClasses = DisplayHomeFilesView.Examples.clustersInClasses()

    // when
    val createExportClassesFile: String = Export(QueryBus()).createExportClassesFile(clustersInClasses)
    // then
    then(contentOf(File(createExportClassesFile))).isEqualTo(contentOf(expectedCSV))
  }

  @Test
  fun `export to csv with named class`() {
    // given
    val expectedCSV = File(javaClass.getResource("/expected_exported_classes_with_file_named_class.csv")!!.file)
    val clustersInClasses = DisplayHomeFilesView.Examples.clustersInClassesWithFileNamedClass()

    // when
    val createExportClassesFile: String = Export(QueryBus()).createExportClassesFile(clustersInClasses)
    // then
    then(contentOf(File(createExportClassesFile))).isEqualTo(contentOf(expectedCSV))
  }

  @Test
  fun `export to csv with invalid state, missing distance to center`() {
    // given
    val clustersInClasses = DisplayHomeFilesView.Examples.invalidStateWithFileWithoutDistanceToCenter()

    // when
    val throwable = catchThrowable { Export(QueryBus()).createExportClassesFile(clustersInClasses) }

    // then
    then(throwable).isInstanceOf(IllegalStateException::class.java)
    then(throwable.message).isEqualTo(
      "[\uD83D\uDED1 CSV EXPORT ERROR] file F2 is not a cluster center nor in multiset but have no distance to cluster center: FileUIView(aggregateId=${
        aggregateIdExample(
          2
        )
      }, filename=F2, clusterCenter=false, thumbnailPath=/img/no_image.png, filePath=/${aggregateIdExample(2)}, distancesToCluster=[], computeDistanceInProgress=false, automaticInclude=false, clusterModified=false, inMultiset=false, lock=false, fileNamedClass=null, distanceToCenter=null, viewStyleBackgroundColor=white, viewStyleTextColor=black)"
    )
  }
}
