package com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure

import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.parameters.domain.DistanceParameters
import com.orange.documentare.clusteringui.biz.remoteservices.infrastructure.BytesDataUI
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

class DistanceRemoteTest {

  @BeforeEach
  fun setup() {
    DistanceRemote.initRemoteSimdocServerForDistance(emptyList())
  }

  @Test
  fun `prepare distances requests`(@TempDir tmpDir: File) {
    // given
    val params = DistanceParameters.Examples.defaults()
    val file1 = createFile(tmpDir, "titi", "hello ${"titi"}")
    val file2 = createFile(tmpDir, "toto", "hello ${"toto"}")

    val file3 = createFile(tmpDir, "tata", "hello ${"tata"}")
    val file4 = createFile(tmpDir, "tyty", "hello ${"tyty"}")
    val files = arrayListOf(file1, file2)

    val mapClusterMultiset = mutableMapOf(
      Pair(
        "1234", listOf(file3)
      ),
      Pair("5678", listOf(file4))
    )

    // when
    val prepareDistancesRequests = DistanceRemote.prepareDistancesRequests(files, params, mapClusterMultiset.values)

    // then
    val file1BytesData = BytesDataUI(
      ArrayList(listOf(file1.aggregateId)),
      ArrayList(listOf(FileIO.filePathOf(file1)))
    )
    val file2BytesData = BytesDataUI(
      ArrayList(listOf(file2.aggregateId)),
      ArrayList(listOf(FileIO.filePathOf(file2)))
    )
    val file3BytesData = BytesDataUI(
      ArrayList(listOf(file3.aggregateId)),
      ArrayList(listOf(FileIO.filePathOf(file3)))
    )
    val file4BytesData = BytesDataUI(
      ArrayList(listOf(file4.aggregateId)),
      ArrayList(listOf(FileIO.filePathOf(file4)))
    )
    val distancesRequestUI1 = DistancesRequestUI(
      arrayOf(file1BytesData),
      arrayOf(file3BytesData, file4BytesData),
      params.rawConverterEnabled,
      params.rawConverterPixelCount,
      params.suffixArrayAlgorithm
    )
    val distancesRequestUI2 = DistancesRequestUI(
      arrayOf(file2BytesData),
      arrayOf(file3BytesData, file4BytesData),
      params.rawConverterEnabled,
      params.rawConverterPixelCount,
      params.suffixArrayAlgorithm
    )
    then(prepareDistancesRequests).hasSize(2)
    then(prepareDistancesRequests[0]).isEqualTo(distancesRequestUI1)
    then(prepareDistancesRequests[1]).isEqualTo(distancesRequestUI2)
  }

  @Test
  fun `prepare distances requests, then use same clusters files object to optimize memory`(@TempDir tmpDir: File) {
    // given
    val params = DistanceParameters.Examples.defaults()
    val file1 = createFile(tmpDir, "titi", "hello ${"titi"}")
    val file2 = createFile(tmpDir, "toto", "hello ${"toto"}")

    val file3 = createFile(tmpDir, "tata", "hello ${"tata"}")
    val file4 = createFile(tmpDir, "tyty", "hello ${"tyty"}")
    val files = arrayListOf(file1, file2)

    val mapClusterMultiset = mutableMapOf(
      Pair(
        "1234", listOf(file3)
      ),
      Pair("5678", listOf(file4))
    )

    // when
    val prepareDistancesRequests = DistanceRemote.prepareDistancesRequests(files, params, mapClusterMultiset.values)

    // then
    // compare java memory references with ==, to make sure it is exactly the same object in memory, and not a copy
    then(prepareDistancesRequests[0].elements.contentEquals(prepareDistancesRequests[1].elements)).isTrue
  }


  @Test
  fun `on error occurred while computing remote distance, then return error`() {
    // given
    val exception = IllegalStateException("an error occurred")

    val remoteDistance: RemoteDistancePort = object : RemoteDistancePort {
      override fun request(urls: List<String>, distancesRequestUIS: List<DistancesRequestUI>): TryOrCatch<Map<DistancesRequestUI, DistancesRequestResult>> {
        return TryOrCatch.failure(exception)
      }
    }

    val distanceRemote = DistanceRemote(EmbeddedComputationServer.disabled(), remoteDistance)

    // when
    val result = distanceRemote.doTheJob(DistanceParameters.Examples.defaults(), emptyList(), emptyList())

    // then
    then(result.isFailure).isTrue
    then(result.cause).isEqualTo(exception)
  }

  @Test
  fun `on distances requests results received, then return distances per file map`(@TempDir tmpDir: File) {
    // given
    val params = DistanceParameters.Examples.defaults()
    val file1 = createFile(tmpDir, "titi", "hello ${"titi"}")
    val file2 = createFile(tmpDir, "toto", "hello ${"toto"}")
    val distancesFile1 = intArrayOf(1)
    val distancesFile2 = intArrayOf(2)

    val remoteDistance: RemoteDistancePort = object : RemoteDistancePort {
      override fun request(urls: List<String>, distancesRequestUIS: List<DistancesRequestUI>): TryOrCatch<Map<DistancesRequestUI, DistancesRequestResult>> {
        val elementAsArray1 = arrayOf(
          BytesDataUI(
            ArrayList(listOf(file1.aggregateId)),
            ArrayList(listOf(FileIO.filePathOf(file1)))
          )
        )
        val distancesRequestUI1 = DistancesRequestUI(
          elementAsArray1,
          emptyArray(),
          params.rawConverterEnabled,
          params.rawConverterPixelCount,
          params.suffixArrayAlgorithm
        )
        val elementAsArray2 = arrayOf(
          BytesDataUI(
            ArrayList(listOf(file2.aggregateId)),
            ArrayList(listOf(FileIO.filePathOf(file2)))
          )
        )
        val distancesRequestUI2 = DistancesRequestUI(
          elementAsArray2,
          emptyArray(),
          params.rawConverterEnabled,
          params.rawConverterPixelCount,
          params.suffixArrayAlgorithm
        )
        val map: Map<DistancesRequestUI, DistancesRequestResult> = mutableMapOf(
          Pair(
            distancesRequestUI1,
            DistancesRequestResult.with(distancesFile1)
          ),
          Pair(
            distancesRequestUI2,
            DistancesRequestResult.with(distancesFile2)
          )
        )
        return TryOrCatch.success(map)
      }
    }

    val distanceRemote = DistanceRemote(EmbeddedComputationServer.disabled(), remoteDistance)
    val files = arrayListOf(file1, file2)

    // when
    val result = distanceRemote.doTheJob(params, files, emptyList())

    // then
    then(result.isSuccess).isTrue
    then(result.get()).hasSize(2)
    then(result.get()[file1]).isEqualTo(DistancesRequestResult.with(distancesFile1))
    then(result.get()[file2]).isEqualTo(DistancesRequestResult.with(distancesFile2))
  }


  private fun createFile(tmpDir: File, filename: String, content: String): DomainFile {
    FileIO.initFilesDirectory(tmpDir.absolutePath)
    val filepath = tmpDir.absoluteFile.absolutePath + File.separator + filename
    File(filepath).writeText(content)
    return DomainFile.builder().aggregateId(filename).filename(filepath).build()
  }

  @Test
  internal fun `build distance request providing correct parameters`() {
    // given
    val parameters = DistanceParameters.Examples.defaults()
    val bytesDataElements: Array<BytesDataUI> = emptyArray()
    val bytesData: Array<BytesDataUI> = emptyArray()

    // when
    val buildDistancesRequest = DistanceRemote.buildDistancesRequest(bytesDataElements, bytesData, parameters)

    // then
    then(buildDistancesRequest).isEqualTo(
      DistancesRequestUI(
        bytesData,
        bytesDataElements,
        parameters.rawConverterEnabled,
        parameters.rawConverterPixelCount,
        parameters.suffixArrayAlgorithm
      )
    )
  }
}
