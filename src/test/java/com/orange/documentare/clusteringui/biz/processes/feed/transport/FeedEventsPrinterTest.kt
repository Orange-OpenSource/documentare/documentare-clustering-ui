package com.orange.documentare.clusteringui.biz.processes.feed.transport

import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederPhases
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederPhases.UpdateType
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeedLoopStarted
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeederPhaseUpdated
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

internal class FeedEventsPrinterTest {
  @Test
  fun `print events in reversed order`() {
    // when
    val displayString = FeedEventsPrinter.print(
      listOf(
        FeederPhaseUpdated(UpdateType.AT_FEED_LOOP_START, FeederPhases.Phase.METADATA_BALANCING, FeederPhases.Phase.MetadataBalancingStep.BREAK_CLUSTERS),
        FeederPhaseUpdated(UpdateType.AT_FEED_LOOP_START, FeederPhases.Phase.METADATA_BALANCING, FeederPhases.Phase.MetadataBalancingStep.INCREMENTAL_CLUSTERING)
      ), ""
    )

    // then
    then(displayString).isEqualTo(
      "<html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"></head><body>❗ [AT_FEED_LOOP_START] Feeder phase is now <strong>METADATA_BALANCING</strong> (step <strong>INCREMENTAL_CLUSTERING</strong>)<br>❗ [AT_FEED_LOOP_START] Feeder phase is now <strong>METADATA_BALANCING</strong> (step <strong>BREAK_CLUSTERS</strong>)<br></body> <script>setTimeout(() => { document.location.reload(); }, 30000);</script> </html>"
    )
  }

  @Test
  fun `on FeedLoopStarted, add an extra line feed`() {
    // when
    val displayString = FeedEventsPrinter.print(
      listOf(
        FeederPhaseUpdated(UpdateType.AT_FEED_LOOP_START, FeederPhases.Phase.METADATA_BALANCING, FeederPhases.Phase.MetadataBalancingStep.BREAK_CLUSTERS),
        FeedLoopStarted(feedLoopCount = 2),
        FeederPhaseUpdated(UpdateType.AT_FEED_LOOP_START, FeederPhases.Phase.METADATA_BALANCING, FeederPhases.Phase.MetadataBalancingStep.INCREMENTAL_CLUSTERING)
      ), ""
    )

    // then
    then(displayString).isEqualTo(
      "<html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"></head><body>❗ [AT_FEED_LOOP_START] Feeder phase is now <strong>METADATA_BALANCING</strong> (step <strong>INCREMENTAL_CLUSTERING</strong>)<br>♻️ Feed Loop number <strong>2</strong> has just started<br><br>❗ [AT_FEED_LOOP_START] Feeder phase is now <strong>METADATA_BALANCING</strong> (step <strong>BREAK_CLUSTERS</strong>)<br></body> <script>setTimeout(() => { document.location.reload(); }, 30000);</script> </html>"
    )
  }
}
