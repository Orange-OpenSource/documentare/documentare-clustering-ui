package com.orange.documentare.clusteringui.biz.file.query.projection

import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.MultisetElementMovedToNextHierarchyLevel
import com.tophe.ddd.infrastructure.event.Event
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Query Files Projection Hierarchy")
class ClustersProjectionHierarchyTest {
  val aggregateId = "12345"
  val projection = ClustersProjection()

  @Test
  fun `clusters projection is initialized with ONE hierarchy level, and next level has value 1`() {
    checkCurrentHierarchyLevelIs(projection, 0)
  }

  @Test
  fun `after a reset, clusters projection is initialized with ONE hierarchy level, and next level has value 1`() {
    // given
    initProjectionWith(
      listOf(
        FileCreated(aggregateId, "f1"),
        FileAddedToCluster.builder().aggregateId(aggregateId).clusterId("10").build()
      )
    )
    then(projection.clusters()).isNotEmpty

    simulateEventToCreateANewHierarchy()
    then(projection.hierarchyLevelCount()).isEqualTo(2)

    // when
    projection.reset()

    // then
    checkCurrentHierarchyLevelIs(projection, 0)
    then(projection.clusters()).isEmpty()
  }


  @Test
  fun `after a hierarchy reset, we should keep only initial first hierarchy level`() {
    // given
    initProjectionWith(
      listOf(
        FileCreated(aggregateId, "f1"),
        FileAddedToCluster.builder().aggregateId(aggregateId).clusterId("10").build()
      )
    )
    then(projection.clusters().containsKey("10")).isTrue

    simulateEventToCreateANewHierarchy()
    then(projection.hierarchyLevelCount()).isEqualTo(2)

    // when
    projection.keepOnlyFirstHierarchyLevel()

    // then
    checkCurrentHierarchyLevelIs(projection, 0)
    then(projection.clusters().containsKey("10")).isTrue
  }

  private fun initProjectionWith(events: List<Event>) {
    projection.initFromHistory(events)
  }

  @Test
  fun `on ClusterCenterMovedToNextHierarchyLevel then create new hierarchy level if it does not exist`() {
    // when
    simulateEventToCreateANewHierarchy()

    // then
    checkCurrentHierarchyLevelIs(projection, 1)
  }

  @Test
  fun `on ClusterCenterMovedToNextHierarchyLevel then DO NOT create new hierarchy level if it exists yet`() {
    // given
    val aggregateId2 = "8890"
    val nextHierarchyLevel = projection.nextHierarchyLevelIndex()
    projection.onEvent(
      MultisetElementMovedToNextHierarchyLevel(
        aggregateId,
        nextHierarchyLevel
      )
    )

    // when
    projection.onEvent(
      MultisetElementMovedToNextHierarchyLevel(
        aggregateId2,
        nextHierarchyLevel
      )
    )

    // then
    checkCurrentHierarchyLevelIs(projection, 1)
  }

  @Test
  fun `on ClusterCenterMovedToNextHierarchyLevel then file is not in cluster in the new hierarchy`() {
    // when
    simulateEventToCreateANewHierarchy()

    // then
    then(projection.filesNotInClusters()).containsExactly(aggregateId)
  }

  private fun simulateEventToCreateANewHierarchy() {
    projection.onEvent(
      MultisetElementMovedToNextHierarchyLevel(
        aggregateId,
        projection.nextHierarchyLevelIndex()
      )
    )
  }

  private fun checkCurrentHierarchyLevelIs(projection: ClustersProjection, currentHierarchyLevelId: Int) {
    then(projection.currentHierarchyLevelIndex()).isEqualTo(currentHierarchyLevelId)
    then(projection.hierarchyLevelCount()).isEqualTo(currentHierarchyLevelId + 1)
    then(projection.nextHierarchyLevelIndex()).isEqualTo(currentHierarchyLevelId + 1)
  }
}
