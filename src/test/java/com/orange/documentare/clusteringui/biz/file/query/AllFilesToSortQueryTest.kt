package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.ClusterModified
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.event.FileDuplicated
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.queries.QueryResponse
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class AllFilesToSortQueryTest {
  var projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  var handler = AllFilesToSortQueryHandler(projections)

  @Test
  fun retrieve_no_files_to_sort() {
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "0", "1", true),
      FileCreated(aggregateIdExample(1), "1", "1", false),
      FileCreated(aggregateIdExample(2), "2", "1", false),
      FileCreated(aggregateIdExample(3), "3", "2", true),
      FileCreated(aggregateIdExample(4), "4", "2", false)
    )
    projections.testPurposeInitFromHistory(events)
    // when
    val response: QueryResponse<List<DomainFile>> = handler.execute(AllFilesToSortQuery())
    // then
    then(response.success()).isTrue()
    then(response.value()).hasSize(0)
  }

  @Test
  fun retrieve_all_files_to_sort() {
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "0", "1", true),
      ClusterModified(aggregateIdExample(0), "1"),
      FileCreated(aggregateIdExample(1), "1", "1", false),
      FileCreated(aggregateIdExample(2), "2", "1", false),
      FileCreated(aggregateIdExample(3), "3", "2", true),
      FileCreated(aggregateIdExample(4), "4", "2", false),
      FileCreated(aggregateIdExample(5), "5"),
      FileCreated(aggregateIdExample(6), "6"),
      FileDuplicated(aggregateIdExample(6), null),
      FileCreated(aggregateIdExample(7), "7")
    )
    projections.testPurposeInitFromHistory(events)
    // when
    val response: QueryResponse<List<DomainFile>> = handler.execute(AllFilesToSortQuery())
    // then
    then(response.success()).isTrue()
    then(response.value()).hasSize(2)
    then(response.value().map { it.aggregateId }).contains(aggregateIdExample(5), aggregateIdExample(7))
  }
}
