package com.orange.documentare.clusteringui.biz.file.event

import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster
import com.orange.documentare.clusteringui.biz.file.domain.DomainFileHistory
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.tophe.ddd.infrastructure.event.Event
import org.assertj.core.api.BDDAssertions.then
import org.assertj.core.util.Lists
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Build DomainFile from history")
class DomainFileHistoryTest {
  @Test
  fun build_domain_file_from_FileCreated() {
    // when
    val (aggregateId, filename) = DomainFileHistory.from(
      setOf(
        FileCreated("1234", "titi")
      )
    )
    val domainFile = DomainFileHistory.from(
      setOf(
        FileCreated("1234", "titi", "10", true)
      )
    )

    // then
    then(aggregateId).isEqualTo("1234")
    then(domainFile.aggregateId).isEqualTo("1234")
    then(filename).isEqualTo("titi")
    then(domainFile.filename).isEqualTo("titi")
    then(domainFile.clusterId).isEqualTo("10")
    then(domainFile.clusterCenter).isTrue
    then(domainFile.clusterModified).isNull()
  }

  @Test
  fun mark_as_duplicate_with_FileDuplicated() {
    // when
    val events = buildFileWithAllFieldsToBeReset()
    events.add(FileDuplicated("1234", null))
    val f = DomainFileHistory.from(events)

    // then
    then(f.duplicated()).isTrue
    then(f.clusterId).isNull()
    then(f.clusterCenter()).isFalse
    then(f.clusterModified()).isFalse
    then(f.distancesToClustersCenters).isNull()
    then(f.distanceToCenter).isNull()
  }

  @Test
  fun build_domain_file_with_FileAddedToCluster() {
    // when
    val domainFile = DomainFileHistory.from(
      Lists.newArrayList(
        FileCreated("1234", "titi"),
        FileAddedToCluster.builder()
          .aggregateId("1234")
          .clusterId("10").clusterCenter(true)
          .distanceToCenter(1000)
          .automaticInclude(true)
          .inMultiset(true)
          .build()
      )
    )

    // then
    then(domainFile.aggregateId).isEqualTo("1234")
    then(domainFile.filename).isEqualTo("titi")
    then(domainFile.clusterId).isEqualTo("10")
    then(domainFile.clusterCenter).isTrue
    then(domainFile.distanceToCenter).isEqualTo(1000)
    then(domainFile.automaticInclude).isTrue
    then(domainFile.inMultiset()).isTrue
    then(domainFile.clusterModified).isNull()
  }

  @Test
  fun create_file_in_cluster_then_remove_it_with_FileRemovedFromCluster() {
    // when
    val domainFile = DomainFileHistory.from(
      Lists.newArrayList(
        FileCreated("1234", "titi"),
        FileAddedToCluster.builder().aggregateId("1234")
          .clusterId("10")
          .clusterCenter(true)
          .distanceToCenter(1000)
          .automaticInclude(true)
          .inMultiset(true)
          .build(),
        ClusterModified("1234", "10"),
        FileRemovedFromCluster("1234", "10", null)
      )
    )

    // then
    then(domainFile.clusterId).isNull()
    then(domainFile.clusterCenter).isNull()
    then(domainFile.clusterModified).isNull()
    then(domainFile.automaticInclude).isNull()
    then(domainFile.inMultiset()).isFalse
  }

  @Test
  fun add_distances_to_centers_with_DistancesToCenterAdded() {
    // when
    val (_, _, _, _, distancesToClustersCenters) = DomainFileHistory.from(
      Lists.newArrayList(
        FileCreated("1234", "titi"),
        FileAddedToCluster.builder().aggregateId("1234").clusterId("10").clusterCenter(true).build(),
        DistancesToClusterCenterAdded("1234", listOf(DistanceToCluster(100, "10")))
      )
    )

    // then
    then(distancesToClustersCenters).containsExactly(
      DistanceToCluster(100, "10")
    )
  }

  @Test
  fun cluster_center_tagged_with_ClusterModified() {
    // when
    val domainFile = DomainFileHistory.from(
      Lists.newArrayList(
        FileCreated("1234", "titi"),
        FileAddedToCluster.builder().aggregateId("1234").clusterId("10").build(),
        ClusterModified("1234", "10")
      )
    )

    // then
    then(domainFile.clusterModified).isTrue
  }

  @Test
  fun reset_file_info_with_FileInfoReset() {
    // when
    val events = buildFileWithAllFieldsToBeReset()
    events.add(FileInfoReset("1234", "10"))
    val f = DomainFileHistory.from(events)

    // then
    then(f.clusterId).isNull()
    then(f.clusterCenter()).isFalse
    then(f.inMultiset()).isFalse
    then(f.clusterModified()).isFalse
    then(f.duplicated()).isFalse
    then(f.distancesToClustersCenters).isNull()
    then(f.distanceToCenter).isNull()
  }

  @Test
  fun reset_file_info_when_moving_to_new_hierarchy_with_ClusterCenterMovedToNextHierarchyLevel() {
    // when
    val events = buildClusterCenter()
    events.add(
      MultisetElementMovedToNextHierarchyLevel(
        "1234",
        2
      )
    )
    val f = DomainFileHistory.from(events)

    // then
    then(f.clusterId).isNull()
    then(f.clusterCenter()).isFalse
    then(f.inMultiset()).isFalse
    then(f.clusterModified()).isFalse
    then(f.duplicated()).isFalse
    then(f.distancesToClustersCenters).isNull()
    then(f.distanceToCenter).isNull()
  }

  @Test
  fun reset_file_info_with_FileInfoReset_but_keep_deleted_attribute() {
    // when
    val events = buildFileWithAllFieldsToBeReset()
    events.addAll(
      Lists.newArrayList(
        FileInfoReset("1234", "10"),
        FileDeleted("1234", "10")
      )
    )
    val f = DomainFileHistory.from(events)

    // then
    then(f.deleted()).isTrue
  }

  @Test
  fun reset_file_info_with_FileInfoReset_but_keep_named_class() {
    // when
    val events = listOf(
      FileCreated("1234", "titi"),
      FilePutInNamedClass("1234", arrayOf("named class")),
      FileInfoReset("1234", "10")
    )
    val f = DomainFileHistory.from(events)

    // then
    then(f.fileNamedClass).isNotEmpty
  }

  @Test
  fun file_in_waiting_room() {
    // when
    val f = DomainFileHistory.from(
      setOf(
        FileCreated("1234", "titi", true)
      )
    )

    // then
    then(f.isInWaitingRoom()).isTrue
  }

  @Test
  fun `file put in named class`() {
    // when
    val fileAggregateId = "1234"
    val fileNamedClass = arrayOf("class name")
    val f = DomainFileHistory.from(
      setOf(
        FileCreated(fileAggregateId, "titi"),
        FilePutInNamedClass(fileAggregateId, fileNamedClass)
      )
    )

    // then
    then(f.fileNamedClass).isEqualTo(fileNamedClass)
  }

  @Test
  fun file_in_waiting_room_moved_to_sort_room_and_keep_file_named_class() {
    // when
    val f = DomainFileHistory.from(
      Lists.newArrayList(
        FileCreated(aggregateIdExample(0), "titi", true),
        FilePutInNamedClass(aggregateIdExample(0), arrayOf("classe a")),
        FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(0))
      )
    )

    // then
    then(f.isInWaitingRoom()).isFalse
    then(f.fileNamedClass).isEqualTo(arrayOf("classe a"))
  }

  @Test
  fun file_moved_to_next_hierarchy_level_and_keep_file_named_class() {
    // when
    val f = DomainFileHistory.from(
      Lists.newArrayList(
        FileCreated(aggregateIdExample(0), "titi", "100", true),
        FilePutInNamedClass(aggregateIdExample(0), arrayOf("classe a")),
        MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(0), 1)
      )
    )

    // then
    then(f.fileNamedClass).isEqualTo(arrayOf("classe a"))
  }

  private fun buildFileWithAllFieldsToBeReset(): MutableList<Event> {
    return Lists.newArrayList(
      FileCreated("1234", "titi"),
      FilePutInNamedClass("1234", arrayOf("named class")),
      FileAddedToCluster.builder().aggregateId("1234").clusterId("10").clusterCenter(true).inMultiset(true).distanceToCenter(100).build(),
      FileDuplicated("1234", "10"),
      ClusterModified("1234", "10"),
      DistancesToClusterCenterAdded("1234", emptyList())
    )
  }

  private fun buildClusterCenter(): MutableList<Event> {
    return Lists.newArrayList(
      FileCreated("1234", "titi"),
      FileAddedToCluster.builder().aggregateId("1234").clusterId("10").clusterCenter(true).inMultiset(true).distanceToCenter(100).build(),
      ClusterModified("1234", "10")
    )
  }
}
