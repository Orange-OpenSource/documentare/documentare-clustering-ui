package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

internal class ClusterUIViewTest {
  @Test
  fun `clusterId first 5 characters is display name`() {
    val clusterUIView = ClusterUIView("3f2cb7bb-dc2d-4700-b3e8-2c55c417ddeb", emptyList())
    then(clusterUIView.displayName()).isEqualTo("3f2cb")
  }

  @Test
  fun `display clusterId and file named class statistics in descending order`() {
    // given
    val domainFile1 = DomainFile.builder().aggregateId("id1").filename("filename 1").fileNamedClass(arrayOf("c1")).build()
    val domainFile2 = DomainFile.builder().aggregateId("id2").filename("filename 2").fileNamedClass(arrayOf("c2")).build()
    val domainFile3 = DomainFile.builder().aggregateId("id3").filename("filename 3").fileNamedClass(arrayOf("c3")).build()
    val fileview1 = FileUIView.from(domainFile1)
    val fileview2 = FileUIView.from(domainFile2)
    val fileview3 = FileUIView.from(domainFile3)
    val clusterUIView = ClusterUIView("3f2cb7bb-dc2d-4700-b3e8-2c55c417ddeb", listOf(fileview1, fileview2, fileview2, fileview3))

    // when
    val displayName = clusterUIView.displayName()

    // then
    then(displayName).isEqualTo("3f2cb - c2 50% / c3 25% / c1 25%")
  }

  @Test
  fun `display clusterId and file named class statistics with unknown named class`() {
    // given
    val domainFile1 = DomainFile.builder().aggregateId("id1").filename("filename 1").fileNamedClass(arrayOf("c1")).build()
    val domainFile2 = DomainFile.builder().aggregateId("id2").filename("filename 2").build()
    val fileview1 = FileUIView.from(domainFile1)
    val fileview2 = FileUIView.from(domainFile2)
    val clusterUIView = ClusterUIView("3f2cb7bb-dc2d-4700-b3e8-2c55c417ddeb", listOf(fileview1, fileview1, fileview2))

    // when
    val displayName = clusterUIView.displayName()

    // then
    then(displayName).isEqualTo("3f2cb - c1 66% / unknown class 33%")
  }
}
