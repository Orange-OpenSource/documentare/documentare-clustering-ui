package com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats

import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class StatsTest {

  data class Value(val value: Int) : StatValue {
    override fun value(): Int {
      return value
    }
  }

  private val classRoom1With3Values = listOf(
        Value(10),
        Value(15),
        Value(18),
    )

    private val classRoom2With5Values = listOf(
        Value(8),
        Value(8),
        Value(10),
        Value(12),
        Value(12),
    )

    @Test
    fun `mean for a classroom`() {
        then(Stats.mean(classRoom1With3Values)).isEqualTo(14.333333333333334)
        then(Stats.mean(classRoom2With5Values)).isEqualTo(10.0)
    }

    @Test
    fun `standard deviation for a classroom`() {
        then(Stats.standardDeviation(classRoom1With3Values)).isEqualTo(3.2998316455372216)
        then(Stats.standardDeviation(classRoom2With5Values)).isEqualTo(1.7888543819998317)
    }

    @Test
    fun `weighted mean of two classrooms`() {
        then(Stats.weightedMeanRaw(listOf(classRoom1With3Values, classRoom2With5Values))).isEqualTo(11.625)
        then(Stats.weightedMean(listOf(Pair(3, 14.333333333333334), Pair(5, 10.0)))).isEqualTo(11.625)
    }

    @Test
    fun `weighted standard deviation of two classrooms`() {
        then(Stats.weightedStandardDeviationRaw(listOf(classRoom1With3Values, classRoom2With5Values))).isEqualTo(2.0978659791956846)
        then(Stats.weightedStandardDeviation(listOf(Pair(3, 14.333333333333334), Pair(5, 10.0)))).isEqualTo(2.0978659791956846)
    }
}
