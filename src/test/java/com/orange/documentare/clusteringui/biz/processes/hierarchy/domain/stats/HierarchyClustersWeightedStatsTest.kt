package com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats

import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.event.FileMovedFromWaitingRoomToSortRoom
import com.orange.documentare.clusteringui.biz.file.event.MultisetElementMovedToNextHierarchyLevel
import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class HierarchyClustersWeightedStatsTest {

  //
  // Test Hierarchy for stats
  //
  //                                      333333
  // Level 3                               |F0|
  //                                       ----
  //                                        |
  //                                      22222
  // Level 2                           |F0,F4,F8,F14|
  //                                   -------------
  //                                       |
  //                         1111                      2222                        3333                                   4444
  // Level 1            |F0,F2,F5,F7|                  |F4|                      |F8,F9|                              |F14,F15,F16|
  //                    ------------                   ---                       -------                              ------------
  //                         |                          |                           |                                       |
  //             111        222    444                 333                     555               666             777       888      999
  // Level 0    |F0,F1|  |F2,F3|  |F7|          |F4,F5multiset,F6|        |F8,F10,F11|         |F9,F12,F13|     |F14|     |F15|    |F16|
  //            ------   ------   ---           -----------------         -----------          ------------     ----      ----     ----


  private fun projectionTestHierarchyForStats(): Projections {
    val projection = Projections(DBEventInMemoryRepositoryForTestPurpose())

    val events = listOf(
      //
      // Level 0
      //
      FileCreated(aggregateIdExample(0), "f0"),
      FileCreated(aggregateIdExample(1), "f1"),
      FileCreated(aggregateIdExample(2), "f2"),
      FileCreated(aggregateIdExample(3), "f3"),
      FileCreated(aggregateIdExample(4), "f4"),
      FileCreated(aggregateIdExample(5), "f5"),
      FileCreated(aggregateIdExample(6), "f6"),
      FileCreated(aggregateIdExample(7), "f7"),
      FileCreated(aggregateIdExample(8), "f8"),
      FileCreated(aggregateIdExample(9), "f9"),
      FileCreated(aggregateIdExample(10), "f10"),
      FileCreated(aggregateIdExample(11), "f11"),
      FileCreated(aggregateIdExample(12), "f12"),
      FileCreated(aggregateIdExample(13), "f13"),
      FileCreated(aggregateIdExample(14), "f14"),
      FileCreated(aggregateIdExample(15), "f15"),
      FileCreated(aggregateIdExample(16), "f16"),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("111").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("111").distanceToCenter(112).build(),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("222").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("222").distanceToCenter(225).build(),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(4)).clusterId("333").clusterCenter(true).build(),
      // ⚠️ interesting to test client hierarchy export: at level 0, only cluster centers + multiset files are exported
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(5)).clusterId("333").inMultiset(true).distanceToCenter(338).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(6)).clusterId("333").distanceToCenter(438).build(),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(7)).clusterId("444").clusterCenter(true).build(),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(8)).clusterId("555").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(10)).clusterId("555").distanceToCenter(500).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(11)).clusterId("555").distanceToCenter(530).build(),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(9)).clusterId("666").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(12)).clusterId("666").distanceToCenter(600).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(13)).clusterId("666").distanceToCenter(900).build(),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(14)).clusterId("777").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(15)).clusterId("888").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(16)).clusterId("999").clusterCenter(true).build(),

      //
      // Level 1
      //
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(0), 1),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(2), 1),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(4), 1),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(5), 1),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(7), 1),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(8), 1),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(9), 1),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(14), 1),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(15), 1),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(16), 1),

      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(0)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(2)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(4)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(5)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(7)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(8)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(9)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(14)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(15)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(16)),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("1111").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("1111").build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(5)).clusterId("1111").build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(7)).clusterId("1111").build(),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(4)).clusterId("2222").clusterCenter(true).build(),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(8)).clusterId("3333").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(9)).clusterId("3333").build(),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(14)).clusterId("4444").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(15)).clusterId("4444").distanceToCenter(1000).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(16)).clusterId("4444").distanceToCenter(1400).build(),

      //
      // Level 2
      //
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(0), 2),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(4), 2),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(8), 2),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(14), 2),

      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(0)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(4)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(8)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(14)),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("22222").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(4)).clusterId("22222").build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(8)).clusterId("22222").build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(14)).clusterId("22222").build(),

      //
      // Level 3
      //
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(0), 3),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(0)),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("333333").clusterCenter(true).build(),
    )

    projection.testPurposeInitFromHistory(events)

    return projection
  }

  @Test
  // 4444: built from level 0 singletons
  //       => we can not use weighted stats
  //       => we retrieve the cluster 4444 stats computed during hierarchy building
  //
  // 444/2222: singleton clusters => not used for upper level weighted stats computation
  fun `3 levels one root, cluster 4444 stats retrieved from hierarchy cluster directly, singleton cluster 444 & 2222 skipped for stats`() {
    // given
    val projections = projectionTestHierarchyForStats()
    val hierarchyClustersWeightedStats = HierarchyClustersWeightedStats(
      HierarchyExamples.hierarchyViewFor(projections)
    ) { levelIndex, clusterId -> HierarchyExamples.retrieveLevelClusterStatsFor(levelIndex, clusterId, projections) }

    then(hierarchyClustersWeightedStats.toString()).isEqualTo(
      """HierarchiesView:
	Level 0:
		Cluster '111' (mean: 112.0, stdDev: 0.0):	f0*, f1
		Cluster '222' (mean: 225.0, stdDev: 0.0):	f2*, f3
		Cluster '333' (mean: 438.0, stdDev: 0.0):	f4*, f5**, f6
		Cluster '444' (singleton, not used for upper level stats):	f7*
		Cluster '555' (mean: 515.0, stdDev: 21.213203435596427):	f8*, f10, f11
		Cluster '666' (mean: 750.0, stdDev: 212.13203435596427):	f9*, f12, f13
		Cluster '777' (singleton, not used for upper level stats):	f14*
		Cluster '888' (singleton, not used for upper level stats):	f15*
		Cluster '999' (singleton, not used for upper level stats):	f16*
	Level 1:
		Cluster '1111' (mean: 284.0, stdDev: 140.03979026384914):	f0*, f2, f5, f7
		Cluster '2222' (mean: 438.0, stdDev: 0.0) (singleton, not used for upper level stats):	f4*
		Cluster '3333' (mean: 632.5, stdDev: 117.5):	f8*, f9
		Cluster '4444' (mean: 1200.0, stdDev: 282.842712474619):	f14*, f15, f16
	Level 2:
		Cluster '22222' (mean: 666.7777777777778, stdDev: 400.19482138294643):	f0*, f4, f8, f14
	Level 3:
		Cluster '333333' (mean: 666.7777777777778, stdDev: 0.0) (singleton, not used for upper level stats):	f0*
"""
    )
  }


  @Test
  fun `4 levels one root`() {
    // given
    val projections = HierarchyExamples.projectionWith4LevelsOneRoot()
    val hierarchyClustersWeightedStats = HierarchyClustersWeightedStats(
      HierarchyExamples.hierarchyViewFor(projections)
    ) { levelIndex, clusterId -> HierarchyExamples.retrieveLevelClusterStatsFor(levelIndex, clusterId, projections) }

    then(hierarchyClustersWeightedStats.toString()).isEqualTo(
      """HierarchiesView:
	Level 0:
		Cluster '111' (mean: 112.0, stdDev: 0.0):	f0*, f1
		Cluster '222' (mean: 225.0, stdDev: 0.0):	f2*, f3
		Cluster '333' (mean: 438.0, stdDev: 0.0):	f4*, f5**, f6
		Cluster '444' (singleton, not used for upper level stats):	f7*
	Level 1:
		Cluster '1111' (mean: 284.0, stdDev: 140.03979026384914):	f0*, f2, f5, f7
		Cluster '2222' (mean: 438.0, stdDev: 0.0) (singleton, not used for upper level stats):	f4*
	Level 2:
		Cluster '11111' (mean: 284.0, stdDev: 0.0):	f0*, f4
	Level 3:
		Cluster '111111' (mean: 284.0, stdDev: 0.0) (singleton, not used for upper level stats):	f0*
"""
    )
  }
}
