package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.event.ClusterLocked
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.io.IOException

@DisplayName("Command LockClusters")
class LockClustersCommandTest {
  /**
   * init eventBus with event repository so that events will be persisted automatically
   */
  var eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  var eventBus = EventBus(eventRepo)
  var handler = LockClustersCommandHandler(eventRepo, eventBus)

  @Test
  @Throws(IOException::class)
  fun a_lock_clusters_command_generates_a_ClusterLocked_event() {
    // given
    val initialEvents = mutableListOf<Event>(
      FileCreated(aggregateIdExample(0), "f0", "10", true)
    )
    eventRepo.insert(initialEvents)
    val cmd = LockClustersCommand(listOf(aggregateIdExample(0)))

    // when
    val response: CommandResponse<*> = handler.execute(cmd)

    // then
    then(response.success()).isTrue()
    initialEvents.apply {
      this.add(ClusterLocked(aggregateIdExample(0), "10", true))
      then(eventRepo.findAll()).containsAll(this)
    }
  }
}
