package com.orange.documentare.clusteringui.biz.remoteservices.clustering.transport

import com.nhaarman.mockitokotlin2.*
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileDuplicated
import com.orange.documentare.clusteringui.biz.file.query.AllFilesQueryHandler
import com.orange.documentare.clusteringui.biz.file.query.AllFilesToSortQueryHandler
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQuery
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.ClusteringServiceImpl
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`

class ClusteringControllerTest {

  private val projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  private val allFilesToSortHandler = AllFilesToSortQueryHandler(projections)
  private val allFilesHandler = AllFilesQueryHandler(projections)
  private val queryBus = spy<QueryBus>().apply {
    register(allFilesToSortHandler, allFilesHandler)
  }

  private val clusteringService = mock<ClusteringServiceImpl>()
  private val clustering = ClusteringController(queryBus, clusteringService)

  @Test
  fun `doGlobalClustering, redirect to parameters settings page if parameters do not exist yet`() {
    // when
    val route = clustering.doGlobalClustering()

    // then
    then(route).isEqualTo("redirect:/parameters")
  }

  @Test
  fun `doGlobalClustering, can launch a global or partial clustering successfully, excluding files in Bin`() {
    // given
    doReturnParametersWhenRequested()

    val domainFile = DomainFile.builder().aggregateId("fileId").filename("filename").build()
    val domainFileInBin = DomainFile.builder().aggregateId("fileIdInBin").filename("filenameInBin").duplicate(true).build()
    projections.testPurposeInitFromHistory(
      listOf(
        FileCreated(domainFile.aggregateId, domainFile.filename),
        FileCreated(domainFileInBin.aggregateId, domainFileInBin.filename),
        FileDuplicated(domainFileInBin.aggregateId, null)
      )
    )

    // when
    val route = clustering.doGlobalClustering()

    // then
    verify(clusteringService).globalOrPartialClusteringAsync(
      Parameters.defaults(),
      listOf(domainFile),
      listOf(domainFileInBin.aggregateId)
    )
    then(route).isEqualTo("redirect:/")
  }


  @Test
  fun `globalOrPartialClusteringAsync, redirect to parameters settings page if parameters do not exist yet`() {
    // when
    val route = clustering.partialCreateClustering()

    // then
    then(route).isEqualTo("redirect:/parameters")
  }

  @Test
  fun `globalOrPartialClusteringAsync, can launch a global or partial clustering successfully`() {
    // given
    doReturnParametersWhenRequested()

    val domainFile = DomainFile.builder().aggregateId("fileId").filename("filename").build()
    projections.testPurposeInitFromHistory(listOf(FileCreated(domainFile.aggregateId, domainFile.filename)))

    // when
    val route = clustering.partialCreateClustering()

    // then
    verify(clusteringService).globalOrPartialClusteringAsync(
      Parameters.defaults(),
      listOf(domainFile),
      emptyList()
    )
    then(route).isEqualTo("redirect:/")
  }

  @Test
  fun `globalOrPartialClusteringAsync, do nothing of no files are present in Sort room`() {
    // given
    doReturnParametersWhenRequested()

    // when
    val route = clustering.partialCreateClustering()

    // then
    verify(clusteringService, never()).globalOrPartialClusteringAsync(anyOrNull(), anyOrNull(), anyOrNull())
    then(route).isEqualTo("redirect:/")
  }

  private fun doReturnParametersWhenRequested() {
    `when`(queryBus.dispatch<QueryResponse<Parameters?>>(ParametersQuery())).thenReturn(QueryResponse(TryOrCatch.of {
      Parameters.defaults()
    }))
  }
}
