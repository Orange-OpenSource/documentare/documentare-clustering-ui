package com.orange.documentare.clusteringui.biz.parameters.domain

import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource

internal class ParametersTest {
  @Test
  fun `default suffix array algorithm is DivSufSort`() {
    val p = Parameters.defaults()
    then(p.getRawConverter()).isEqualTo(true)
    then(p.getpCount()).isEqualTo(150000)
    then(p.getSuffixArrayAlgorithm()).isEqualTo(SuffixArrayAlgorithm.LIBDIVSUFSORT)
    then(p.getAcutSdFactor()).isEqualTo(2f)
    then(p.getQcutSdFactor()).isEqualTo(2f)
    then(p.getScutSdFactor()).isEqualTo(2f)
    then(p.getCcutPercentile()).isEqualTo(75)
    then(p.getKnnThreshold()).isEqualTo(10)
    then(p.getSloop()).isEqualTo(true)
    then(p.getEnroll()).isEqualTo(true)
    then(p.getEnrollAcutSdFactor()).isEqualTo(2f)
    then(p.getEnrollQcutSdFactor()).isEqualTo(2f)
    then(p.getEnrollScutSdFactor()).isEqualTo(2f)
    then(p.getEnrollCcutPercentile()).isEqualTo(75)
    then(p.getEnrollKnnThreshold()).isEqualTo(10)
    then(p.getThresholdFactor()).isEqualTo(1.0f)
    then(p.getFeed()).isEqualTo(false)
    then(p.getFeedThreshold()).isEqualTo(10)
    then(p.getMinMultisetThreshold()).isEqualTo(2)
    then(p.getMaxMultisetThreshold()).isEqualTo(5)
  }


  @ParameterizedTest
  @EnumSource(SuffixArrayAlgorithm::class)
  fun `consolidate cluster parameters preserves the suffix array algorithm`(saAlgorithm: SuffixArrayAlgorithm) {
    // when
    val parameters = Parameters.consolidateCluster(100000, saAlgorithm)

    // then
    val expectedParameters = Parameters.builder().pCount(100000).consolidateCluster(true).suffixArrayAlgorithm(saAlgorithm).build()
    then(parameters).isEqualTo(expectedParameters)
  }
}
