package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.Event
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class AllFilesQueryTest {
  var projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  var handler = AllFilesQueryHandler(projections)

  @Test
  fun retrieve_all_files() {
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "0"),
      FileCreated(aggregateIdExample(1), "1")
    )
    projections.testPurposeInitFromHistory(events)

    // when
    val response = handler.execute(AllFilesQuery())

    // then
    then(response.success()).isTrue()
    then(response.value()).hasSize(2)
  }
}
