package com.orange.documentare.clusteringui.biz.file.transport

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.file.query.ClassesFilenamesView
import com.orange.documentare.clusteringui.biz.file.query.DisplayHierarchyLevelFileTransform.Companion.CLASS_NO_NAME
import com.orange.documentare.clusteringui.biz.file.transport.api.FileRestController
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`


class ClassesFilenamesTest {

  @Test
  fun `retrieve classes filenames`() {
    // given
    val expectedClassesFilenames = mutableMapOf<String, List<String>>()
    expectedClassesFilenames["ca1"] = listOf("a1")
    expectedClassesFilenames["ca2"] = listOf("a2")
    expectedClassesFilenames["ce1"] = listOf("e1", "e2", "e3")
    expectedClassesFilenames[CLASS_NO_NAME] = listOf("z")

    val queryBus = mock<QueryBus>()
    `when`(queryBus.dispatch<QueryResponse<ClassesFilenamesView>>(any()))
      .thenReturn(QueryResponse(TryOrCatch.success(ClassesFilenamesView(expectedClassesFilenames))))

    val controller = FileRestController(queryBus)

    // when
    val classesFilenamesView: ClassesFilenamesView = controller.classesFilenames()

    // then
    then(classesFilenamesView.classesFilenames).hasSize(4)
  }
}
