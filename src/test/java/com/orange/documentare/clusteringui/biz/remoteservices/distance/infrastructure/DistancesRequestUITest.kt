package com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.remoteservices.infrastructure.BytesDataUI
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.orange.documentare.core.computationserver.biz.distances.api.DistancesRequest
import com.orange.documentare.core.computationserver.infrastructure.transport.BytesDataDTO
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

internal class DistancesRequestUITest {
  @Test
  fun `prepare distances requests`(@TempDir tmpDir: File) {
    // given
    val params = Parameters.defaults()
    val file1 = createFile(tmpDir, "titi")
    val file2 = createFile(tmpDir, "toto")

    val file3 = createFile(tmpDir, "tata")
    val file4 = createFile(tmpDir, "tyty")

    val file1BytesDataUI = BytesDataUI(
      ArrayList(listOf(file1.aggregateId)),
      ArrayList(listOf(FileIO.filePathOf(file1)))
    )
    BytesDataUI(
      ArrayList(listOf(file2.aggregateId)),
      ArrayList(listOf(FileIO.filePathOf(file2)))
    )
    val file2BytesDataUI = BytesDataUI(
      ArrayList(listOf(file3.aggregateId)),
      ArrayList(listOf(FileIO.filePathOf(file3)))
    )
    val file3BytesDataUI = BytesDataUI(
      ArrayList(listOf(file4.aggregateId)),
      ArrayList(listOf(FileIO.filePathOf(file4)))
    )
    val distancesRequestUI = DistancesRequestUI(
      arrayOf(file1BytesDataUI),
      arrayOf(file2BytesDataUI, file3BytesDataUI),
      params.rawConverter,
      params.pCount,
      params.suffixArrayAlgorithm
    )

    // when
    val distanceRequest: DistancesRequest = distancesRequestUI.toComputationServerDistanceRequest()

    // then
    val elementDTO: Array<BytesDataDTO> = arrayOf(file1BytesDataUI.toBytesDataDTO())
    val elementsDTO: Array<BytesDataDTO> = arrayOf(file2BytesDataUI.toBytesDataDTO(), file3BytesDataUI.toBytesDataDTO())
    val expectedDistanceRequest = DistancesRequest(elementDTO, elementsDTO, distanceRequest.rawConverter, distanceRequest.pCount, distanceRequest.suffixArrayAlgorithm)
    then(distanceRequest).isEqualTo(expectedDistanceRequest)
  }

  private fun createFile(tmpDir: File, filename: String): DomainFile {
    FileIO.initFilesDirectory(tmpDir.absolutePath)
    val filepath = tmpDir.absoluteFile.absolutePath + File.separator + filename
    File(filepath).writeText("hello")
    return DomainFile.builder().aggregateId(filename).filename(filepath).build()
  }
}
