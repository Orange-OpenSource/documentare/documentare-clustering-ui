package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.nhaarman.mockitokotlin2.*
import com.orange.documentare.clusteringui.Couple
import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.appnotifications.AppNotification
import com.orange.documentare.clusteringui.biz.appnotifications.AppNotificationsService
import com.orange.documentare.clusteringui.biz.file.command.TagClusterNotModifiedCommand
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.ClusterFilesQuery
import com.orange.documentare.clusteringui.biz.file.query.SelectedFilesQuery
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure.ClusteringRemoteApi
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure.PreviousClusterId
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceServiceImpl
import com.orange.documentare.core.model.ref.clustering.Cluster
import com.orange.documentare.core.model.ref.clustering.Clustering
import com.orange.documentare.core.model.ref.clustering.ClusteringElement
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.`when`

class ComputeClusteringOnModifiedClustersTest {

  private val computeClusteringOnModifiedClusters: ComputeClusteringOnModifiedClusters = mock()
  private val mockedRemoteApi = mock<ClusteringRemoteApi>()
  private val commandBus: CommandBus = mock()
  private val queryBus: QueryBus = mock()
  private val appNotifications = AppNotificationsService()
  private val infra = ClusteringInfra(
    commandBus, queryBus, mockedRemoteApi, DistanceServiceImpl(
      appNotifications,
      EmbeddedComputationServer.disabled()
    ), ClusteringInProgress(), appNotifications
  )
  private val parameters = Parameters.defaults()

  @BeforeEach
  fun setup() {
    doCallRealMethod().`when`(computeClusteringOnModifiedClusters).doSync(any(), any(), any())
    doCallRealMethod().`when`(computeClusteringOnModifiedClusters).recalculateClusters(any(), any(), any())
    doCallRealMethod().`when`(computeClusteringOnModifiedClusters).tagClusterNotModified(any(), any())
  }

  @Test
  fun `do nothing if there is no modified cluster`() {
    // when
    computeClusteringOnModifiedClusters.doSync(infra, parameters, java.util.HashMap<String, List<DomainFile>>().toMap())

    // then
    verify(computeClusteringOnModifiedClusters, never()).recalculateClusters(any(), any(), any())
  }

  @Test
  fun `recalculate non empty clusters`() {
    // given
    `when`(mockedRemoteApi.recalculateClusters(any(), any())).thenReturn(mutableMapOf())

    val files = listOf(DomainFile.builder().build())
    val clustersModified = mapOf(
      Pair("clEmpty", emptyList()),
      Pair("clNotEmpty", files)
    )

    // when
    computeClusteringOnModifiedClusters.doSync(infra, parameters, clustersModified)

    // then
    verify(mockedRemoteApi).recalculateClusters(
      parameters,
      listOf(Couple(PreviousClusterId("clNotEmpty"), files))
    )
  }

  @Test
  fun `recalculate a cluster then save clustering and notify it is done`() {
    // given
    val clusterCenter = DomainFile.builder().aggregateId(aggregateIdExample(1)).clusterId("10").clusterCenter(true).build()

    val previousClusterId = PreviousClusterId(clusterCenter.clusterId!!)
    val cluster = buildClusteringWithMultiset(1)
    val map = mutableMapOf(Pair(previousClusterId, TryOrCatch.of { cluster }))

    `when`(mockedRemoteApi.recalculateClusters(any(), any())).thenReturn(map)

    mockQueryClusterFilesWith(clusterCenter)
    mockQueryDeletedFilesWith()

    val clusterFiles = listOf(clusterCenter)
    val clustersModified = mapOf(
      Pair(previousClusterId.id, clusterFiles)
    )

    var actualAppNotification: AppNotification? = null
    appNotifications.register {
      actualAppNotification = it
    }

    // when
    computeClusteringOnModifiedClusters.doSync(infra, parameters, clustersModified)

    // then
    then(actualAppNotification).isEqualTo(AppNotification.RECALCULATE_CLUSTERS_DONE)
    verify(computeClusteringOnModifiedClusters).saveClustering(infra, cluster, previousClusterId.id)
  }

  @Test
  fun `recalculate a cluster then save clustering IF cluster is locked BUT multiset count is below threshold`() {
    // given
    val clusterCenter = DomainFile.builder().aggregateId(aggregateIdExample(1)).clusterId("10").clusterCenter(true).lock(true).build()

    val previousClusterId = PreviousClusterId(clusterCenter.clusterId!!)
    val cluster = buildClusteringWithMultiset(parameters.maxMultisetThreshold - 1)
    val map = mutableMapOf(Pair(previousClusterId, TryOrCatch.of { cluster }))

    `when`(mockedRemoteApi.recalculateClusters(any(), any())).thenReturn(map)

    mockQueryClusterFilesWith(clusterCenter)
    mockQueryDeletedFilesWith()

    val clusterFiles = listOf(clusterCenter)
    val clustersModified = mapOf(
      Pair(previousClusterId.id, clusterFiles)
    )

    // when
    computeClusteringOnModifiedClusters.doSync(infra, parameters, clustersModified)

    // then
    verify(computeClusteringOnModifiedClusters).saveClustering(infra, cluster, previousClusterId.id)
  }

  @Test
  fun `recalculate a cluster then DO NOT save clustering AND DO tag cluster as not modified IF cluster is locked AND multiset count is above threshold`() {
    // given
    val clusterCenter = DomainFile.builder().aggregateId(aggregateIdExample(1)).clusterId("10").clusterCenter(true).lock(true).build()

    val previousClusterId = PreviousClusterId(clusterCenter.clusterId!!)
    val cluster = buildClusteringWithMultiset(parameters.maxMultisetThreshold + 1)
    val map = mutableMapOf(Pair(previousClusterId, TryOrCatch.of { cluster }))

    `when`(mockedRemoteApi.recalculateClusters(any(), any())).thenReturn(map)

    mockQueryClusterFilesWith(clusterCenter)
    mockQueryDeletedFilesWith()

    val clusterFiles = listOf(clusterCenter)
    val clustersModified = mapOf(
      Pair(previousClusterId.id, clusterFiles)
    )
    `when`(commandBus.dispatch<CommandResponse<Void>>(any())).thenReturn(CommandResponse<Void>(TryOrCatch.success(null)))

    // when
    computeClusteringOnModifiedClusters.doSync(infra, parameters, clustersModified)

    // then
    verify(computeClusteringOnModifiedClusters, never()).saveClustering(any(), any(), any())
    verify(computeClusteringOnModifiedClusters).tagClusterNotModified(infra, previousClusterId.id)
    verify(commandBus).dispatch<CommandResponse<Void>>(TagClusterNotModifiedCommand(aggregateIdExample(1)))
  }

  @Test
  fun `recalculate a cluster then DO NOT save clustering IF a file was deleted from cluster`() {
    // given
    val clusterCenter = DomainFile.builder().aggregateId(aggregateIdExample(1)).clusterId("10").clusterCenter(true).build()
    val deletedFile = DomainFile.builder().aggregateId(aggregateIdExample(2)).delete(true).build()

    val previousClusterId = PreviousClusterId(clusterCenter.clusterId!!)
    val cluster = buildClusteringWithMultiset(0)
    val map = mutableMapOf(Pair(previousClusterId, TryOrCatch.of { cluster }))

    `when`(mockedRemoteApi.recalculateClusters(any(), any())).thenReturn(map)

    mockQueryClusterFilesWith(clusterCenter, deletedFile)
    mockQueryDeletedFilesWith(deletedFile)

    val clusterFiles = listOf(clusterCenter, deletedFile)
    val clustersModified = mapOf(
      Pair(previousClusterId.id, clusterFiles)
    )

    // when
    computeClusteringOnModifiedClusters.doSync(infra, parameters, clustersModified)

    // then
    verify(computeClusteringOnModifiedClusters, never()).saveClustering(any(), any(), any())
  }

  private fun buildClusteringWithMultiset(multisetSize: Int): Clustering {
    val elements = listOf(
      ClusteringElement(aggregateIdExample(1), 20, true, null, null, null)
    )
    val multisetIndices = if (multisetSize == 0) null else IntRange(0, multisetSize).distinct()
    val cluster = Cluster(emptyList(), multisetIndices)
    val clusters = listOf(cluster)
    return Clustering(elements, emptyList(), clusters, ClusteringParameters.builder().build(), null, null)
  }

  private fun mockQueryClusterFilesWith(vararg files: DomainFile) {
    val clusterFilesQueryResponse = QueryResponse(TryOrCatch.of { files.asList() })
    `when`(queryBus.dispatch<QueryResponse<List<DomainFile>>>(ArgumentMatchers.isA(ClusterFilesQuery::class.java))).thenReturn(clusterFilesQueryResponse)
  }

  private fun mockQueryDeletedFilesWith(vararg files: DomainFile) {
    val deletedFilesQueryResponse = QueryResponse(TryOrCatch.of { files.asList() })
    `when`(queryBus.dispatch<QueryResponse<List<DomainFile>>>(ArgumentMatchers.isA(SelectedFilesQuery::class.java))).thenReturn(deletedFilesQueryResponse)
  }
}
