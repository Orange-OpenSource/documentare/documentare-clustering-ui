package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.DisplayHierarchyLevelFileTransform.Companion.CLASS_NO_NAME
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.Event
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class DisplayApiFilesQueryTest {
  var projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  var handler = DisplayApiFilesQueryHandler(projections)

  @Test
  fun no_files_to_display() {
    // when
    val response = handler.execute(DisplayApiFilesQuery())
    // then
    then(response.success()).isTrue()
    then(response.value().notInClusters).isEmpty()
    then(response.value().mapClassViews[CLASS_NO_NAME]).isEmpty()
  }

  @Test
  fun one_file_not_in_cluster_to_display() {
    // given
    val events: Iterable<Event> = listOf<Event>(
      FileCreated(aggregateIdExample(0), "titi")
    )
    projections.testPurposeInitFromHistory(events)
    // when
    val response = handler.execute(DisplayApiFilesQuery())
    // then
    then(response.success()).isTrue()
    then(response.value().notInClusters).hasSize(1)
    then(response.value().mapClassViews[CLASS_NO_NAME]).isEmpty()
  }

  @Test
  fun one_file_in_cluster_to_display() {
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "titi"),
      FileAddedToCluster(aggregateIdExample(0), "10", true, null, null, true, null, null)
    )
    projections.testPurposeInitFromHistory(events)
    // when
    val response = handler.execute(DisplayApiFilesQuery())
    // then
    then(response.success()).isTrue()
    val (_, notInClusters, mapClassViews) = response.value()
    then(notInClusters).isEmpty()
    then(mapClassViews).hasSize(1)
    then((mapClassViews[CLASS_NO_NAME] ?: error(""))["10"]).hasSize(1)
    val file = ((mapClassViews[CLASS_NO_NAME] ?: error(""))["10"] ?: error(""))[0]
    then(file.filename).isEqualTo("titi")
    then(file.clusterCenter).isTrue()
    then(file.inMultiset()).isTrue()
  }

  @Test
  fun one_file_in_cluster_10_in_className_test_to_display_and_no_cluster_in_className_zzzz_No_Named() {
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "titi"),
      FileAddedToCluster(aggregateIdExample(0), "10", true, null, null, true, null, "test")
    )
    projections.testPurposeInitFromHistory(events)
    // when
    val response = handler.execute(DisplayApiFilesQuery())
    // then
    then(response.success()).isTrue()
    val (_, notInClusters, mapClassViews) = response.value()
    then(notInClusters).isEmpty()
    then(mapClassViews).hasSize(2)
    then((mapClassViews["test"] ?: error(""))["10"]).hasSize(1)
    then(response.value().mapClassViews[CLASS_NO_NAME]).isEmpty()
    val file = ((mapClassViews["test"] ?: error(""))["10"] ?: error(""))[0]
    then(file.filename).isEqualTo("titi")
    then(file.clusterCenter).isTrue()
    then(file.inMultiset()).isTrue()
  }

  @Test
  fun `classes are built with clusters`() {
    // given
    val events = listOf(
      FileCreated(aggregateIdExample(0), "filename-0"),
      FileCreated(aggregateIdExample(1), "filename-1"),
      FileCreated(aggregateIdExample(2), "filename-2"),
      FileCreated(aggregateIdExample(3), "filename-3"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("100").className("a").inMultiset(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("100").className("a").build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("100").className("a").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("100").className("a").automaticInclude(true).build(),

      FileCreated(aggregateIdExample(4), "filename-4"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(4)).clusterId("200").className("a").clusterCenter(true).build(),

      FileCreated(aggregateIdExample(5), "filename-5"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(5)).clusterId("300").className("b").clusterCenter(true).build(),

      FileCreated(aggregateIdExample(10), "filename-10"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(10)).clusterId("400").clusterCenter(true).build()
    )
    projections.testPurposeInitFromHistory(events)

    // when
    val response = handler.execute(DisplayApiFilesQuery())

    // then
    then(response.success()).isTrue()
    val view = response.value()
    then(view.mapClassViews.keys).containsExactlyInAnyOrder("a", "b", CLASS_NO_NAME)
    then(view.mapClassViews["a"]?.keys).containsExactlyInAnyOrder("100", "200")
    then(view.mapClassViews["b"]?.keys).containsExactlyInAnyOrder("300")
    then(view.mapClassViews[CLASS_NO_NAME]?.keys).containsExactlyInAnyOrder("400")
  }

}
