package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.appnotifications.AppNotificationsService
import com.orange.documentare.clusteringui.biz.file.command.*
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.*
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.AllFilesQueryHandler
import com.orange.documentare.clusteringui.biz.file.query.ClusterFilesQueryHandler
import com.orange.documentare.clusteringui.biz.file.query.SelectedFilesQueryHandler
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.SaveClustering.Companion.testUUIDSupplier
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure.ClusteringRemoteApi
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure.PreviousClusterId
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceService
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.orange.documentare.core.model.ref.clustering.Cluster
import com.orange.documentare.core.model.ref.clustering.Clustering
import com.orange.documentare.core.model.ref.clustering.ClusteringElement
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.event.TestEventHandler
import com.tophe.ddd.queries.QueryBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`

internal class RecalculateClustersThenComputeDistancesTest {
  private val testEventHandler = TestEventHandler()

  private var clusteringService = ClusteringServiceImpl(
    DistanceSupplier().get(), AppNotificationsService(),
    EmbeddedComputationServer.disabled()
  )
  private val eventRepo = DBEventInMemoryRepositoryForTestPurpose()

  private val eventBus = EventBus(eventRepo)
  private val projections = Projections(eventRepo)

  private val clusteringRemoteApi = mock<ClusteringRemoteApi>()

  @BeforeEach
  fun setUp() {
    initClusteringService(clusteringService)

    eventBus.register(testEventHandler)
    eventBus.register(projections)

    val uuidSupplier = UUIDSupplier()
    testUUIDSupplier { uuidSupplier.getAsString() }

    verifyCalculateAllClustersIsNotCalled()
  }

  private fun initClusteringService(clusteringService: ClusteringServiceImpl) {
    val commandBus = CommandBus()
    commandBus.register(
      BinPurgeCommandHandler(eventRepo, eventBus),
      AddToClusterCommandHandler(eventRepo, eventBus),
      DuplicateFilesCommandHandler(eventRepo, eventBus),
      ResetFileInfoCommandHandler(eventRepo, eventBus),
      RemoveSelectedFilesFromClustersCommandHandler(eventRepo, eventBus)
    )
    val queryBus = QueryBus()
    queryBus.register(AllFilesQueryHandler(projections), SelectedFilesQueryHandler(projections), ClusterFilesQueryHandler(projections))

    clusteringService.setCommandBus(commandBus)
    clusteringService.setQueryBus(queryBus)
    clusteringService.testClusteringRemote(clusteringRemoteApi)
  }

  @Test
  fun `recalculate cluster with a new cluster center and multiset`() {
    // given
    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("10").clusterCenter(true).build(),
      ClusterModified(aggregateIdExample(0), "10"),
      FileCreated(aggregateIdExample(1), "f1"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("10").build(),
      FileCreated(aggregateIdExample(2), "f2"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("10").build(),
      FileCreated(aggregateIdExample(3), "f3"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("10").build()
    )
    val files = prepRepos(events, aggregateIdExample(0), aggregateIdExample(1), aggregateIdExample(2), aggregateIdExample(3))
    val clustersModified = mutableMapOf(Pair("10", files))

    stubRecalculateClustersReturnedValue(PreviousClusterId("10"), clusteringOfFourElementsInSameClusterWithAMultiset(false))

    // when
    startThenWaitForRecalculate(clustersModified, NO_FILES_TO_SORT)

    // then
    then(testEventHandler.events()).containsExactly(
      FileRemovedFromCluster(aggregateIdExample(0), "10", null),
      FileRemovedFromCluster(aggregateIdExample(1), "10", null),
      FileRemovedFromCluster(aggregateIdExample(2), "10", null),
      FileRemovedFromCluster(aggregateIdExample(3), "10", null),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId(FIRST_UUID).distanceToCenter(10).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId(FIRST_UUID).inMultiset(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId(FIRST_UUID).clusterCenter(true).inMultiset(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId(FIRST_UUID).distanceToCenter(10).build()
    )

    verifyCalculateAllClustersIsNotCalled()
  }

  @Test
  fun `recalculate cluster with a new cluster center and multiset, then compute distances of file to sort`() {
    // given
    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("10").clusterCenter(true).build(),
      ClusterModified(aggregateIdExample(0), "10"),
      FileCreated(aggregateIdExample(1), "f1"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("10").build(),
      FileCreated(aggregateIdExample(2), "f2"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("10").build(),
      FileCreated(aggregateIdExample(3), "f3"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("10").build(),
      FileCreated(aggregateIdExample(4), "f4")
    )
    val files = prepRepos(events, aggregateIdExample(0), aggregateIdExample(1), aggregateIdExample(2), aggregateIdExample(3), aggregateIdExample(4))
    val clustersModified = mutableMapOf(Pair("10", files))
    val filesIdsToSort = listOf(aggregateIdExample(4))

    stubRecalculateClustersReturnedValue(PreviousClusterId("10"), clusteringOfFourElementsInSameClusterWithAMultiset(false))

    val distanceSupplier = DistanceSupplier()
    val distancesComputedForTheseIds = mutableListOf<String>()
    distanceSupplier.computeDistancesOf = { _, fileIdsToSort ->
      distancesComputedForTheseIds.addAll(fileIdsToSort)
    }
    clusteringService = ClusteringServiceImpl(
      distanceSupplier.get(), AppNotificationsService(),
      EmbeddedComputationServer.disabled()
    )
    initClusteringService(clusteringService)

    // when
    startThenWaitForRecalculate(clustersModified, filesIdsToSort)

    // then
    then(testEventHandler.events()).containsExactly(
      FileRemovedFromCluster(aggregateIdExample(0), "10", null),
      FileRemovedFromCluster(aggregateIdExample(1), "10", null),
      FileRemovedFromCluster(aggregateIdExample(2), "10", null),
      FileRemovedFromCluster(aggregateIdExample(3), "10", null),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId(FIRST_UUID).distanceToCenter(10).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId(FIRST_UUID).inMultiset(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId(FIRST_UUID).clusterCenter(true).inMultiset(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId(FIRST_UUID).distanceToCenter(10).build(),
      FileInfoReset(aggregateIdExample(4), null)
    )

    then(distancesComputedForTheseIds).isEqualTo(filesIdsToSort)

    verifyCalculateAllClustersIsNotCalled()
  }

  @Test
  fun `a file is deleted, result of recalculate cluster is not saved`() {
    // given
    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("10").clusterCenter(true).build(),
      ClusterModified(aggregateIdExample(0), "10"),
      FileCreated(aggregateIdExample(1), "f1"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("10").build(),
      FileCreated(aggregateIdExample(2), "f2"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("10").build(),
      FileCreated(aggregateIdExample(3), "f3"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("10").build()
    )
    val files = prepRepos(events, aggregateIdExample(0), aggregateIdExample(1), aggregateIdExample(2), aggregateIdExample(3))
    val clustersModified = mutableMapOf(Pair("10", files))

    stubRecalculateClustersReturnedValue(PreviousClusterId("10"), clusteringOfFourElementsInSameClusterWithAMultiset(false), aggregateIdExample(1))

    // when
    startThenWaitForRecalculate(clustersModified, NO_FILES_TO_SORT)

    // then
    then(testEventHandler.events()).isEmpty()

    verifyCalculateAllClustersIsNotCalled()
  }

  @Test
  fun `duplicate file is saved as a duplicate`() {
    // given
    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("10").clusterCenter(true).build(),
      ClusterModified(aggregateIdExample(0), "10"),
      FileCreated(aggregateIdExample(1), "f1"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("10").build(),
      FileCreated(aggregateIdExample(2), "f2"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("10").build(),
      FileCreated(aggregateIdExample(3), "f3"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("10").build()
    )
    val files = prepRepos(events, aggregateIdExample(0), aggregateIdExample(1), aggregateIdExample(2), aggregateIdExample(3))
    val clustersModified = mutableMapOf(Pair("10", files))

    stubRecalculateClustersReturnedValue(PreviousClusterId("10"), clusteringOfFourElementsInSameClusterWithAMultiset(true))

    // when
    startThenWaitForRecalculate(clustersModified, NO_FILES_TO_SORT)

    // then
    then(testEventHandler.events()).containsExactly(
      FileRemovedFromCluster(aggregateIdExample(0), "10", null),
      FileRemovedFromCluster(aggregateIdExample(1), "10", null),
      FileRemovedFromCluster(aggregateIdExample(2), "10", null),
      FileRemovedFromCluster(aggregateIdExample(3), "10", null),
      FileDuplicated(aggregateIdExample(3), null),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId(FIRST_UUID).distanceToCenter(10).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId(FIRST_UUID).inMultiset(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId(FIRST_UUID).clusterCenter(true).inMultiset(true).build()
    )

    verifyCalculateAllClustersIsNotCalled()
  }

  @Test
  fun `when no clusters are modified, just compute distance of file to sort to clusters`() {
    // given
    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0"),
      FileCreated(aggregateIdExample(1), "f1", "10", true)
    )
    prepRepos(events, aggregateIdExample(0), aggregateIdExample(1))
    val filesIdsToSort = listOf(aggregateIdExample(0))
    val distanceService = mock<DistanceService>()
    clusteringService.testSetDistanceService(distanceService)

    // when
    startThenWaitForRecalculate(NO_MODIFIED_CLUSTER, filesIdsToSort)

    // then
    then(testEventHandler.events()).containsExactly(
      FileInfoReset(aggregateIdExample(0), null)
    )
    verify(distanceService).computeDistancesOf(PARAMETERS, listOf(aggregateIdExample(0)))

    verifyCalculateAllClustersIsNotCalled()
  }

  @Test
  fun `recalculate a cluster locked with a new cluster center and multiset keep the cluster locked`() {
    // given
    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("10").clusterCenter(true).build(),
      ClusterModified(aggregateIdExample(0), "10"),
      FileCreated(aggregateIdExample(1), "f1"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("10").build(),
      FileCreated(aggregateIdExample(2), "f2"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("10").build(),
      FileCreated(aggregateIdExample(3), "f3"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("10").build(),
      ClusterLocked(aggregateIdExample(0), "10", true)
    )
    val files = prepRepos(events, aggregateIdExample(0), aggregateIdExample(1), aggregateIdExample(2), aggregateIdExample(3))
    val clustersModified = mutableMapOf(Pair("10", files))

    stubRecalculateClustersReturnedValue(PreviousClusterId("10"), clusteringOfFourElementsInSameClusterWithAMultiset(false))

    // when
    startThenWaitForRecalculate(clustersModified, NO_FILES_TO_SORT)

    // then
    then(testEventHandler.events()).containsExactly(
      FileRemovedFromCluster(aggregateIdExample(0), "10", null),
      FileRemovedFromCluster(aggregateIdExample(1), "10", null),
      FileRemovedFromCluster(aggregateIdExample(2), "10", null),
      FileRemovedFromCluster(aggregateIdExample(3), "10", null),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId(FIRST_UUID).distanceToCenter(10).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId(FIRST_UUID).inMultiset(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId(FIRST_UUID).clusterCenter(true).inMultiset(true).lock(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId(FIRST_UUID).distanceToCenter(10).build()
    )

    verifyCalculateAllClustersIsNotCalled()
  }

  @Test
  fun `recalculate a cluster locked and named with a new cluster center and multiset keep the cluster locked and named`() {
    // given
    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("10").clusterCenter(true).build(),
      ClusterModified(aggregateIdExample(0), "10"),
      FileCreated(aggregateIdExample(1), "f1"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("10").build(),
      FileCreated(aggregateIdExample(2), "f2"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("10").build(),
      FileCreated(aggregateIdExample(3), "f3"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("10").build(),
      ClusterLocked(aggregateIdExample(0), "10", true),
      ClusterPutInNamedClass(aggregateIdExample(0), "10", "test")
    )
    val files = prepRepos(events, aggregateIdExample(0), aggregateIdExample(1), aggregateIdExample(2), aggregateIdExample(3))
    val clustersModified = mutableMapOf(Pair("10", files))

    stubRecalculateClustersReturnedValue(PreviousClusterId("10"), clusteringOfFourElementsInSameClusterWithAMultiset(false))

    // when
    startThenWaitForRecalculate(clustersModified, NO_FILES_TO_SORT)

    // then
    then(testEventHandler.events()).containsExactly(
      FileRemovedFromCluster(aggregateIdExample(0), "10", null),
      FileRemovedFromCluster(aggregateIdExample(1), "10", null),
      FileRemovedFromCluster(aggregateIdExample(2), "10", null),
      FileRemovedFromCluster(aggregateIdExample(3), "10", null),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId(FIRST_UUID).distanceToCenter(10).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId(FIRST_UUID).inMultiset(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId(FIRST_UUID).clusterCenter(true).inMultiset(true).lock(true).className("test").build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId(FIRST_UUID).distanceToCenter(10).build()
    )

    verifyCalculateAllClustersIsNotCalled()
  }

  private fun clusteringOfFourElementsInSameClusterWithAMultiset(lastIsADuplicate: Boolean): Clustering {
    val elements = listOf(
      ClusteringElement(aggregateIdExample(0), 0, false, null, null, 10),
      ClusteringElement(aggregateIdExample(1), 0, false, null, null, null),
      ClusteringElement(aggregateIdExample(2), 0, true, null, null, null),
      ClusteringElement(aggregateIdExample(3), 0, false, null, if (lastIsADuplicate) 0 else null, 10)
    )

    val elementsIndices = listOf(0, 1, 2, 3)
    val multisetElementsIndices = listOf(1, 2)
    val cluster = Cluster(elementsIndices, multisetElementsIndices)

    return Clustering(elements, null, listOf(cluster), null, null, null)
  }

  private fun startThenWaitForRecalculate(clustersModified: Map<String, List<DomainFile>>, filesIdsToSort: List<String>) {
    clusteringService.recalculateClustersThenComputeDistancesAsync(
      PARAMETERS, clustersModified, filesIdsToSort
    )
    do {
      Thread.sleep(10)
    } while (clusteringService.isRunning())
  }

  private fun stubRecalculateClustersReturnedValue(previousClusterId: PreviousClusterId, result: Clustering, deletedFileId: String? = null) {
    deletedFileId?.let {
      markFileAsDeleted(it)
    }
    `when`(clusteringRemoteApi.recalculateClusters(any(), any())).thenReturn(mutableMapOf(Pair(previousClusterId, TryOrCatch.success(result))))
  }

  private fun markFileAsDeleted(id: String) {
    CreateNewClusterTest.markFileAsDeleted(id, eventRepo, projections)
  }

  private fun verifyCalculateAllClustersIsNotCalled() {
    verify(clusteringRemoteApi, never()).calculateClustering(any(), any())
  }

  private fun prepRepos(events: List<Event>, vararg fileIds: String): List<DomainFile> {
    eventRepo.insert(events)
    projections.testPurposeInitFromHistory(events)
    return fileIds.map { eventRepo.load(it)!! }
  }

  companion object {
    internal val PARAMETERS = Parameters.builder().pCount(1).consolidateCluster(true).maxMultisetThreshold(3).build()
    internal val NO_FILES_TO_SORT = emptyList<String>()
    internal val NO_MODIFIED_CLUSTER = mutableMapOf<String, List<DomainFile>>()
    internal const val FIRST_UUID: String = UUIDSupplier.FIRST_UUID.toString()
  }
}
