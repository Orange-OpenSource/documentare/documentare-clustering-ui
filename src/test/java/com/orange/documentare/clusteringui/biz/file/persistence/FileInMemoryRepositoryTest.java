/*
package com.orange.documentare.clusteringui.biz.file.persistence;

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile;
import org.assertj.core.api.Assertions;
import org.junit.Ignore;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

@DisplayName("In memory file repository")
public class FileInMemoryRepositoryTest {

  FileInMemoryRepository repo = new FileInMemoryRepository();

  @Test
  void retrieve_cluster_files_by_clusterId() {
    // given
    repo.save(DomainFile.builder().build());
    DomainFile fileInCluster = DomainFile.builder().clusterId("10").build();
    fileInCluster = repo.save(fileInCluster);

    // when
    List<DomainFile> filesInCluster = repo.findByClusterId("10");

    // then
    then(filesInCluster).containsExactly(
      fileInCluster
    );
  }
}
*/
