package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.DisplayHierarchyLevelFileTransform.Companion.CLASS_NO_NAME
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.queries.QueryResponse
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class ClassesFilenamesQueryTest {
  var projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  var handler = ClassesFilenamesQueryHandler(projections)

  @Test
  fun `retrieve classes with file names, no files so no named class`() {
    // when
    val response: QueryResponse<ClassesFilenamesView> = handler.execute(ClassesFilenamesQuery())

    // then
    then(response.success()).isTrue()

    val classesFilenames = mutableMapOf<String, List<String>>()
    classesFilenames[CLASS_NO_NAME] = emptyList()
    then(response.value()).isEqualTo(ClassesFilenamesView(classesFilenames))
  }

  @Test
  fun `retrieve classes with file names`() {
    // given
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "a1"),
      FileCreated(aggregateIdExample(1), "a2"),
      FileCreated(aggregateIdExample(2), "e1"),
      FileCreated(aggregateIdExample(3), "e2"),
      FileCreated(aggregateIdExample(4), "e3"),
      FileCreated(aggregateIdExample(5), "z"),
      FileAddedToCluster(aggregateIdExample(0), "10", true, null, null, true, null, "ca1"),
      FileAddedToCluster(aggregateIdExample(1), "11", true, null, null, true, null, "ca2"),
      FileAddedToCluster(aggregateIdExample(2), "20", true, null, null, true, null, "ce1"),
      FileAddedToCluster(aggregateIdExample(3), "21", true, null, null, true, null, "ce1"),
      FileAddedToCluster(aggregateIdExample(4), "21", true, null, null, true, null, "ce1"),
      FileAddedToCluster(aggregateIdExample(5), "30", true, null, null, true, null, null)
    )
    projections.testPurposeInitFromHistory(events)

    // when
    val response: QueryResponse<ClassesFilenamesView> = handler.execute(ClassesFilenamesQuery())

    // then
    then(response.success()).isTrue()

    val expectedClassesFilenames = mutableMapOf<String, List<String>>()
    expectedClassesFilenames["ca1"] = listOf("a1")
    expectedClassesFilenames["ca2"] = listOf("a2")
    expectedClassesFilenames["ce1"] = listOf("e1", "e2", "e3")
    expectedClassesFilenames[CLASS_NO_NAME] = listOf("z")
    val classesFilenamesView = response.value().classesFilenames
    then(classesFilenamesView).hasSize(4)
    classesFilenamesView.forEach {
      then(it.value).containsExactlyInAnyOrder(*expectedClassesFilenames[it.key]!!.toTypedArray())
    }
  }
}
