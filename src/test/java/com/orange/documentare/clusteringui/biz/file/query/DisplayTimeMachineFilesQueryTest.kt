package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.DisplayHierarchyLevelFileTransform.Companion.CLASS_NO_NAME
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.persistence.EventRepository
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class DisplayTimeMachineFilesQueryTest {
  private val eventRepo: EventRepository = DBEventInMemoryRepositoryForTestPurpose()
  var handler = DisplayTimeMachineFilesQueryHandler(eventRepo)

  @Test
  fun no_files_to_display() {
    // when
    val response = handler.execute(DisplayTimeMachineFilesQuery(""))

    // then
    then(response.success()).isTrue
    then(response.value().noFiles).isTrue
    then(response.value().notInClusters).isEmpty()
    then(response.value().mapClassViews[CLASS_NO_NAME]).isEmpty()
  }

  @Test
  fun one_file_not_in_cluster_to_display() {
    // given
    eventRepo.insert(listOf(FileCreated(aggregateIdExample(0), "titi")))

    // when
    val response = handler.execute(DisplayTimeMachineFilesQuery(""))

    // then
    then(response.success()).isTrue
    then(response.value().noFiles).isFalse
    then(response.value().notInClusters).hasSize(1)
    then(response.value().mapClassViews[CLASS_NO_NAME]).isEmpty()
  }

  @Test
  fun one_file_in_cluster_to_display() {
    // given
    eventRepo.insert(
      listOf(
        FileCreated(aggregateIdExample(0), "titi"),
        FileAddedToCluster(aggregateIdExample(0), "10", true, null, false, true, false, null)
      )
    )

    // when
    val response = handler.execute(DisplayTimeMachineFilesQuery(""))

    // then
    then(response.success()).isTrue
    val (noFiles, _, notInClusters, _, mapClassViews) = response.value()
    then(noFiles).isFalse
    then(notInClusters).isEmpty()
    then(mapClassViews).hasSize(1)
    then((mapClassViews[CLASS_NO_NAME] ?: error("no value"))["10"]?.fileUIViews).hasSize(1)
    val fileView = ((mapClassViews[CLASS_NO_NAME] ?: error("no value"))["10"]?.fileUIViews ?: error("no value"))[0]
    then(fileView.filename).isEqualTo("titi")
    then(fileView.clusterCenter).isTrue
    then(fileView.inMultiset).isTrue
  }

  @Test
  fun one_file_in_cluster_10_in_className_test_to_display_and_no_cluster_in_className_zzzz_No_Named() {
    // given
    eventRepo.insert(
      listOf(
        FileCreated(aggregateIdExample(0), "titi"),
        FileAddedToCluster(aggregateIdExample(0), "10", true, null, false, true, false, "test")
      )
    )

    // when
    val response = handler.execute(DisplayTimeMachineFilesQuery(""))

    // then
    then(response.success()).isTrue
    val (noFiles, _, notInClusters, _, mapClassViews) = response.value()
    then(noFiles).isFalse
    then(notInClusters).isEmpty()
    then(mapClassViews).hasSize(2)
    then((mapClassViews["test"] ?: error("no value"))["10"]?.fileUIViews).hasSize(1)
    then(response.value().mapClassViews[CLASS_NO_NAME]).isEmpty()
    val fileView = ((mapClassViews["test"] ?: error("no value"))["10"]?.fileUIViews ?: error("no value"))[0]
    then(fileView.filename).isEqualTo("titi")
    then(fileView.clusterCenter).isTrue
    then(fileView.inMultiset).isTrue
  }
}
