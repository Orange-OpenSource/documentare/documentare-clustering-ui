package com.orange.documentare.clusteringui.biz.parameters.query

import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.persistence.ParametersRepository
import com.orange.documentare.clusteringui.infrastructure.persistence.ParametersInMemoryRepository
import com.tophe.ddd.queries.QueryResponse
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class ParametersQueryTest {
  private var repo: ParametersRepository = ParametersInMemoryRepository()
  private var handler = ParametersQueryHandler(repo)

  @Test
  fun parameters_do_not_exist_in_the_database() {
    // given
    val query = ParametersQuery()

    // when
    val response: QueryResponse<Parameters?> = handler.execute(query)

    // then
    then(response.success()).isTrue()
    then(response.value()).isNull()
  }

  @Test
  fun get_modified_parameters() {
    // given
    repo.insert(listOf(Parameters.builder().pCount(1234).build()))
    val query = ParametersQuery()

    // when
    val response: QueryResponse<Parameters?> = handler.execute(query)

    // then
    then(response.success()).isTrue()
    then(response.value()?.pCount).isEqualTo(1234)
  }
}
