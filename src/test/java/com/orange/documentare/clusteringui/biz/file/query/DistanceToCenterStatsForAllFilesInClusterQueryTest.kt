package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.event.FileMovedFromWaitingRoomToSortRoom
import com.orange.documentare.clusteringui.biz.file.event.MultisetElementMovedToNextHierarchyLevel
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class DistanceToCenterStatsForAllFilesInClusterQueryTest {
  var projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  var handler = DistanceToCenterStatsForAllFilesInClusterQueryHandler(projections)

  @Test
  fun retrieve_stats_distance_to_center_in_cluster_in_hierarchy_level_0() {
    // given
    projections.testPurposeInitFromHistory(listOf(soleHierarchyLevelEvents(), secondLevelEvents()).flatten())

    // when
    val responseCluster111Singleton = handler.execute(DistanceToCenterStatsForAllFilesInClusterQuery("111-singleton", 0))
    val responseCluster222ThreeFiles = handler.execute(DistanceToCenterStatsForAllFilesInClusterQuery("222-3-files", 0))
    val responseCluster333TwoFiles = handler.execute(DistanceToCenterStatsForAllFilesInClusterQuery("333-2-files", 0))

    // then
    then(responseCluster111Singleton.success()).isTrue
    then(responseCluster111Singleton.value().mean).isNaN
    then(responseCluster111Singleton.value().standardDeviation).isNaN
    then(responseCluster111Singleton.value().singleton).isTrue

    then(responseCluster222ThreeFiles.success()).isTrue
    then(responseCluster222ThreeFiles.value().mean).isEqualTo(15.0)
    then(responseCluster222ThreeFiles.value().standardDeviation).isEqualTo(7.0710678118654755)

    then(responseCluster333TwoFiles.success()).isTrue
    then(responseCluster333TwoFiles.value().mean).isEqualTo(30.0)
    then(responseCluster333TwoFiles.value().standardDeviation).isEqualTo(0.0)
  }

  @Test
  fun retrieve_stats_distance_to_centroid_in_multiset_cluster() {
    // given
    projections.testPurposeInitFromHistory(listOf(soleHierarchyLevelEvents(), secondLevelEvents()).flatten())

    // when
    val responseCluster444Multiset = handler.execute(DistanceToCenterStatsForAllFilesInClusterQuery("444-4-files-multiset", 0))

    // then
    then(responseCluster444Multiset.success()).isTrue
    then(responseCluster444Multiset.value().mean).isEqualTo(55.0)
    then(responseCluster444Multiset.value().standardDeviation).isEqualTo(7.0710678118654755)
  }

  @Test
  fun retrieve_stats_distance_to_center_in_cluster_in_hierarchy_level_1() {
    // given
    projections.testPurposeInitFromHistory(listOf(soleHierarchyLevelEvents(), secondLevelEvents()).flatten())

    // when
    val responseCluster11111 = handler.execute(DistanceToCenterStatsForAllFilesInClusterQuery("11111-3-files", 1))

    // then
    then(responseCluster11111.success()).isTrue
    then(responseCluster11111.value().mean).isEqualTo(150.0)
  }

  @Test
  fun try_to_retrieve_a_cluster_statistics_but_not_in_proper_hierarchy_level() {
    // given
    projections.testPurposeInitFromHistory(listOf(soleHierarchyLevelEvents(), secondLevelEvents()).flatten())

    // when
    val responseCluster11111 = handler.execute(DistanceToCenterStatsForAllFilesInClusterQuery("11111-3-files", 0))

    // then
    then(responseCluster11111.failure()).isTrue
    then(responseCluster11111.failureCause()).contains("[DistanceToCenterStatsForAllFilesInClusterQuery] ❌ cluster '11111-3-files' was expected")
  }

  private fun soleHierarchyLevelEvents() = listOf(
    // sole hierarchy level, singleton, cluster 111
    FileCreated(aggregateIdExample(0), "0", "111-singleton", true),

    // sole hierarchy level, cluster 222
    FileCreated(aggregateIdExample(1), "1", "222-3-files", true),
    FileCreated(aggregateIdExample(2), "2"),
    FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("222-3-files").distanceToCenter(10).build(),
    FileCreated(aggregateIdExample(3), "3"),
    FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("222-3-files").distanceToCenter(20).build(),

    // sole hierarchy level, cluster 333
    FileCreated(aggregateIdExample(4), "4", "333-2-files", true),
    FileCreated(aggregateIdExample(5), "5"),
    FileAddedToCluster.builder().aggregateId(aggregateIdExample(5)).clusterId("333-2-files").distanceToCenter(30).build(),

    // sole hierarchy level, cluster 444 with multiset
    FileCreated(aggregateIdExample(6), "6", "444-4-files-multiset", true),
    FileCreated(aggregateIdExample(6), "6"),
    FileAddedToCluster.builder().aggregateId(aggregateIdExample(6)).clusterId("444-4-files-multiset").inMultiset(true).distanceToCenter(10).build(),
    FileCreated(aggregateIdExample(7), "7"),
    FileAddedToCluster.builder().aggregateId(aggregateIdExample(7)).clusterId("444-4-files-multiset").distanceToCenter(50).build(),
    FileCreated(aggregateIdExample(8), "8"),
    FileAddedToCluster.builder().aggregateId(aggregateIdExample(8)).clusterId("444-4-files-multiset").distanceToCenter(60).build(),
  )

  private fun secondLevelEvents() = listOf(
    // move files to second hierarchy level
    MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(0), 1),
    MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(1), 1),
    MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(4), 1),

    FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(0)),
    FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(1)),
    FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(4)),

    // second hierarchy level, cluster 11111
    FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("11111-3-files").clusterCenter(true).build(),
    FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("11111-3-files").distanceToCenter(100).build(),
    FileAddedToCluster.builder().aggregateId(aggregateIdExample(4)).clusterId("11111-3-files").distanceToCenter(200).build(),
  )
}
