package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.biz.processes.feed.domain.SingletonsChanges.Companion.computeSingletonsChanges
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

internal class SingletonsChangesTest {
  @Test
  fun `no singletons in previous loop and now one, so singletons changed with 100 change rate`() {
    // when
    val singletonsChanges = computeSingletonsChanges(emptyList(), listOf("1"))

    // then
    then(singletonsChanges).isEqualTo(
      SingletonsChanges(
        true,
        emptyList(),
        listOf("1"),
        100f
      )
    )
  }

  @Test
  fun `no singletons in previous loop and still no one, so singletons did not changed with 0 change rate`() {
    // when
    val singletonsChanges = computeSingletonsChanges(emptyList(), emptyList())

    // then
    then(singletonsChanges).isEqualTo(
      SingletonsChanges(
        false,
        emptyList(),
        emptyList(),
        0f
      )
    )
  }

  @Test
  fun `singletons in previous loop but nothing changed, so singletons did not changed with 0 change rate`() {
    // when
    val singletonsChanges = computeSingletonsChanges(listOf("1", "2"), listOf("1", "2"))

    // then
    then(singletonsChanges).isEqualTo(
      SingletonsChanges(
        false,
        emptyList(),
        emptyList(),
        0f
      )
    )
  }

  @Test
  fun `singletons in previous loop but nothing changed except singletons order, so singletons did not changed with 0 change rate`() {
    // when
    val singletonsChanges = computeSingletonsChanges(listOf("1", "2"), listOf("2", "1"))

    // then
    then(singletonsChanges).isEqualTo(
      SingletonsChanges(
        false,
        emptyList(),
        emptyList(),
        0f
      )
    )
  }

  @Test
  fun `singletons in previous loop and some removed and other added, so singletons did changed with non 0 change rate`() {
    // when
    val singletonsChanges = computeSingletonsChanges(listOf("0", "1", "2", "3"), listOf("2", "1", "4", "5", "6"))

    // then
    then(singletonsChanges).isEqualTo(
      SingletonsChanges(
        true,
        listOf("0", "3"),
        listOf("4", "5", "6"),
        125f
      )
    )
  }
}
