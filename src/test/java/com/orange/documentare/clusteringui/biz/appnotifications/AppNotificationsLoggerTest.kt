package com.orange.documentare.clusteringui.biz.appnotifications

import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

internal class AppNotificationsLoggerTest {
  @Test
  fun `on new app notification, then log it`() {
    // given
    val appNotificationsService = AppNotificationsService()
    val notificationsLogger = AppNotificationsLogger(appNotificationsService)
    var actualLogMessageReceived = ""
    notificationsLogger.testPurposeOverrideLogger { actualLogMessageReceived = it }

    // done by spring in the app
    notificationsLogger.registerToListenAppNotificationsAtStartup()

    // when
    appNotificationsService.notify(AppNotification.COMPUTE_DISTANCES_TO_CLUSTERS_DONE)

    // then
    then(actualLogMessageReceived).isEqualTo("[App Notification] COMPUTE_DISTANCES_TO_CLUSTERS_DONE")
  }
}
