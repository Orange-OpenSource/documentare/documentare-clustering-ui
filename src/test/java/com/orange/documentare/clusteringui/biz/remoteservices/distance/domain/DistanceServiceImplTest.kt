package com.orange.documentare.clusteringui.biz.remoteservices.distance.domain

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure.DistancesRequestResult
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

internal class DistanceServiceImplTest {
  @Test
  fun `failed to retrieve file distances to clusters, then build exception context`() {
    // given
    val file = DomainFile.builder().aggregateId("1234").filename("f1").build()
    val file2 = DomainFile.builder().aggregateId("5678").filename("f222").build()
    val result = mutableMapOf(Pair(file, DistancesRequestResult.with(intArrayOf(0, 10))), Pair(file2, DistancesRequestResult.with(intArrayOf(5, 15))))
    val files = listOf(file, file2)

    // when
    val buildExceptionContext = DistanceServiceImpl.buildExceptionContext(file, result, files).message

    // then
    then(buildExceptionContext).isEqualTo(
      "File distances to clusters is not available for file: filename(f1) - id(1234)\n" +
        "\n" +
        "distance computation results are present for files with id: '[1234, 5678]'\n" +
        "\n" +
        "initial distances computation request was for files with id: '[1234, 5678]'"
    )
  }
}
