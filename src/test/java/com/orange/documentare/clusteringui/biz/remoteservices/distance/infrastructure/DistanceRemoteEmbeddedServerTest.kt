package com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure

import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.parameters.domain.DistanceParameters
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import java.util.*

class DistanceRemoteEmbeddedServerTest {

  private lateinit var tmpDir: File
  private lateinit var rawCacheDir: File
  private lateinit var distancesPrepDir: File

  @BeforeEach
  fun setup() {
    // do not use @TmpDir since as our code delete files in it at the same time junit try to delete the dir, we got strange exceptions...
    tmpDir = File("/tmp/" + UUID.randomUUID())
    rawCacheDir = File("/tmp/" + UUID.randomUUID())
    distancesPrepDir = File("/tmp/" + UUID.randomUUID())
    tmpDir.mkdirs()
    rawCacheDir.mkdirs()
    distancesPrepDir.mkdirs()
  }

  @AfterEach
  fun cleanup() {
    // sometimes it may be seen as not empty just after the test
    tmpDir.deleteRecursively()
    rawCacheDir.deleteRecursively()
    distancesPrepDir.deleteRecursively()
  }


  @Test
  fun `on distances requests results received from embedded computation server, then return distances per file map`() {
    // given
    val file1: DomainFile = createFile(tmpDir, "titi", "I am titi")
    val file2: DomainFile = createFile(tmpDir, "toto", "I am toto")
    val file3: DomainFile = createFile(tmpDir, "tata", "I am tata")
    val file4: DomainFile = createFile(tmpDir, "tutu", "I am tutu")

    val embeddedComputationServer = EmbeddedComputationServer.with(distancesPrepDir.absolutePath, rawCacheDir.absolutePath)
    val distanceRemote = DistanceRemote(embeddedComputationServer, RemoteDistance(null, embeddedComputationServer))
    val mapClusterMultiset: Map<String, List<DomainFile>> = mapOf(Pair("clusterId1", listOf(file2)), Pair("clusterId2", listOf(file3, file4)))

    // when
    val result: TryOrCatch<Map<DomainFile, DistancesRequestResult>> = distanceRemote.doTheJob(DistanceParameters.Examples.defaults(), arrayListOf(file1), mapClusterMultiset.values)

    // then
    then(result.isSuccess).isTrue
    val resultMap = result.get()
    then(resultMap.size).isEqualTo(1)
    then(resultMap[file1]).isEqualTo(DistancesRequestResult.with(intArrayOf(312500, 450000)))
  }

  @Test
  fun `on distances requests succeeded even if we saturate computation server (503 not available)`() {
    // given
    val enoughFilesToSaturateComputationServer = Runtime.getRuntime().availableProcessors() + 2
    val files: List<DomainFile> = (1..enoughFilesToSaturateComputationServer).map { createFile(tmpDir, "titi$it", "1") }

    val file2: DomainFile = createFile(tmpDir, "toto", "2")
    val mapClusterMultiset: Map<String, List<DomainFile>> = mapOf(Pair("clusterId1", listOf(file2)))

    val embeddedComputationServer = EmbeddedComputationServer(true, distancesPrepDir.absolutePath, rawCacheDir.absolutePath, "")
    val distanceRemote = DistanceRemote(embeddedComputationServer, RemoteDistance(null, embeddedComputationServer))

    // when
    val result: TryOrCatch<Map<DomainFile, DistancesRequestResult>> = distanceRemote.doTheJob(DistanceParameters.Examples.defaults(), files, mapClusterMultiset.values)

    // then
    then(result.isSuccess).isTrue
    then(result.get()).hasSize(enoughFilesToSaturateComputationServer)
    result.get().forEach { then(it.value.distances).isEqualTo(intArrayOf(125000)) }
  }


  private fun createFile(tmpDir: File, filename: String, content: String): DomainFile {
    FileIO.initFilesDirectory(tmpDir.absolutePath)
    val filepath = tmpDir.absoluteFile.absolutePath + File.separator + filename
    File(filepath).writeText(content)
    return DomainFile.builder().aggregateId(filename).filename(filepath).build()
  }
}
