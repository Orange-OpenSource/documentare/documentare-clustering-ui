package com.orange.documentare.clusteringui.biz.file.domain

import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.event.FileRemovedFromCluster
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class DomainFileRemoveFilesFromClusterTest {

  @Test
  fun remove_cluster_center_destroys_cluster() {
    // given
    val clusterFiles = listOf(
      b().aggregateId(aggregateIdExample(0)).clusterId("10").clusterCenter(true).build(),
      b().aggregateId(aggregateIdExample(1)).clusterId("10").build()
    )
    val filesToRemove = listOf(b().aggregateId(aggregateIdExample(0)).clusterId("10").clusterCenter(true).build())
    // when
    val removedFilesFromCluster = DomainFile.removeFilesFromCluster("10", clusterFiles, filesToRemove, Room.SORT_ROOM)
    // then
    then(removedFilesFromCluster.destroyedCluster).isNotNull()
    then(removedFilesFromCluster.destroyedCluster).isEqualTo("10")
    then(removedFilesFromCluster.events).containsExactly(
      FileRemovedFromCluster(aggregateIdExample(0), "10", null), FileRemovedFromCluster(aggregateIdExample(1), "10", null)
    )
  }

  @Test
  fun not_destroy_cluster_if_cluster_center_stays_alone() {
    // given
    val clusterFiles = listOf(
      b().aggregateId(aggregateIdExample(0)).clusterId("10").clusterCenter(true).build(),
      b().aggregateId(aggregateIdExample(1)).clusterId("10").build()
    )
    val filesToRemove = listOf(b().aggregateId(aggregateIdExample(1)).clusterId("10").build())
    // when
    val removedFilesFromCluster = DomainFile.removeFilesFromCluster("10", clusterFiles, filesToRemove, Room.SORT_ROOM)
    // then
    then(removedFilesFromCluster.destroyedCluster).isNull()
    then(removedFilesFromCluster.events).containsExactly(
      FileRemovedFromCluster(aggregateIdExample(1), "10", null)
    )
  }

  @Test
  fun remove_file_from_cluster() {
    // given
    val clusterFiles = listOf(
      b().aggregateId(aggregateIdExample(0)).clusterId("10").clusterCenter(true).build(),
      b().aggregateId(aggregateIdExample(1)).clusterId("10").build(),
      b().aggregateId(aggregateIdExample(2)).clusterId("10").build()
    )
    val filesToRemove = listOf(b().aggregateId(aggregateIdExample(2)).clusterId("10").build())
    // when
    val removedFilesFromCluster = DomainFile.removeFilesFromCluster("10", clusterFiles, filesToRemove, Room.SORT_ROOM)
    // then
    then(removedFilesFromCluster.destroyedCluster).isNull()
    then(removedFilesFromCluster.events).containsExactly(
      FileRemovedFromCluster(aggregateIdExample(2), "10", null)
    )
  }

  private fun b(): DomainFileBuilder {
    return DomainFile.builder()
  }
}
