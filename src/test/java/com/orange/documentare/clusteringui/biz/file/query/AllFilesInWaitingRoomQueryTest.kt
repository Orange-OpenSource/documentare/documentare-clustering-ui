package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.queries.QueryResponse
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class AllFilesInWaitingRoomQueryTest {
  var projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  var handler = AllFilesInWaitingRoomQueryHandler(projections)

  @Test
  fun retrieve_no_files_in_waiting_room() {
    // given
    val events: Iterable<Event> = listOf<Event>(
      FileCreated(aggregateIdExample(0), "0"),
      FileCreated(aggregateIdExample(1), "1")
    )
    projections.testPurposeInitFromHistory(events)
    // when
    val response: QueryResponse<List<DomainFile>> = handler.execute(AllFilesInWaitingRoomQuery())
    // then
    then(response.success()).isTrue()
    then(response.value()).hasSize(0)
  }

  @Test
  fun retrieve_all_files_in_waiting_room() {
    // given
    val events: Iterable<Event> = listOf<Event>(
      FileCreated(aggregateIdExample(0), "0"),
      FileCreated(aggregateIdExample(1), "1", true)
    )
    projections.testPurposeInitFromHistory(events)
    // when
    val response: QueryResponse<List<DomainFile>> = handler.execute(AllFilesInWaitingRoomQuery())
    // then
    then(response.success()).isTrue
    then(response.value()).hasSize(1)
    then(response.value().map { f: DomainFile -> f.aggregateId }).contains(aggregateIdExample(1))
  }
}
