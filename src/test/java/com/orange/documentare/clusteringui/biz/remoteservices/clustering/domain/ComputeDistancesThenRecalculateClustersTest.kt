package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.orange.documentare.clusteringui.Couple
import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.appnotifications.AppNotificationsService
import com.orange.documentare.clusteringui.biz.file.command.AddToClusterCommandHandler
import com.orange.documentare.clusteringui.biz.file.command.RemoveSelectedFilesFromClustersCommandHandler
import com.orange.documentare.clusteringui.biz.file.command.ResetFileInfoCommandHandler
import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster
import com.orange.documentare.clusteringui.biz.file.event.*
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.*
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.SaveClustering.Companion.testUUIDSupplier
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.UUIDSupplier.Companion.FIRST_UUID
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure.ClusteringRemoteApi
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure.PreviousClusterId
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.orange.documentare.core.model.ref.clustering.Cluster
import com.orange.documentare.core.model.ref.clustering.Clustering
import com.orange.documentare.core.model.ref.clustering.ClusteringElement
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.event.TestEventHandler
import com.tophe.ddd.queries.QueryBus
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`

class ComputeDistancesThenRecalculateClustersTest {

  private val testEventHandler = TestEventHandler()

  private val eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  private val eventBus = EventBus(eventRepo)
  private val projections = Projections(eventRepo)

  private val clusteringRemoteApi = mock(ClusteringRemoteApi::class.java)
  private val parameters = Parameters.defaults()

  private val distanceSupplier = DistanceSupplier()
  private val clusteringService = ClusteringServiceImpl(
    distanceSupplier.get(), AppNotificationsService(),
    EmbeddedComputationServer.disabled()
  )

  @BeforeEach
  fun setUp() {
    val commandBus = CommandBus()
    val queryBus = QueryBus()
    commandBus.register(
      ResetFileInfoCommandHandler(eventRepo, eventBus),
      RemoveSelectedFilesFromClustersCommandHandler(eventRepo, eventBus),
      AddToClusterCommandHandler(eventRepo, eventBus)
    )
    queryBus.register(
      AllFilesToSortQueryHandler(projections),
      AllClustersQueryHandler(projections),
      AllFilesInModifiedClustersQueryHandler(projections),
      SelectedFilesQueryHandler(projections),
      ClusterFilesQueryHandler(projections)
    )

    clusteringService.setCommandBus(commandBus)
    clusteringService.setQueryBus(queryBus)
    clusteringService.testClusteringRemote(clusteringRemoteApi)

    eventBus.register(testEventHandler)
    eventBus.register(projections)

    val uuidSupplier = UUIDSupplier()
    testUUIDSupplier { uuidSupplier.getAsString() }
  }

  @Test
  fun `do nothing but without any errors or crash`() {
    clusteringService.computeDistancesThenRecalculateClustersSync(parameters)
  }

  @Test
  fun `compute distance to cluster, distance is computed but cluster is not modified`() {
    // given
    val clusterId = "10"
    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId(clusterId).clusterCenter(true).build(),
      FileCreated(aggregateIdExample(1), "f1")
    )
    eventRepo.insert(events)
    projections.testPurposeInitFromHistory(events)

    eventRepo.load(aggregateIdExample(0))
    eventRepo.load(aggregateIdExample(1))


    distanceSupplier.computeDistancesOfInSameThread = { _: Parameters, fileIds: List<String> ->
      addDistancesToClusters(fileIds)
    }

    // when
    clusteringService.computeDistancesThenRecalculateClustersSync(parameters)

    // then
    then(testEventHandler.events()).containsExactly(
      FileInfoReset(aggregateIdExample(1), null),
      DistancesToClusterCenterAdded(aggregateIdExample(1), listOf(DistanceToCluster(10, clusterId)))
    )
  }

  @Test
  fun `compute distance to cluster, distance is computed and a modified cluster is recalculated`() {
    // given
    val clusterId = "10"
    val events = listOf(
      FileCreated(aggregateIdExample(0), "f0"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId(clusterId).clusterCenter(true).build(),
      FileCreated(aggregateIdExample(1), "f1"),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId(clusterId).build(),
      ClusterModified(aggregateIdExample(0), clusterId),

      FileCreated(aggregateIdExample(2), "f2")
    )
    eventRepo.insert(events)
    projections.testPurposeInitFromHistory(events)

    val f0 = eventRepo.load(aggregateIdExample(0))!!
    val f1 = eventRepo.load(aggregateIdExample(1))!!
    eventRepo.load(aggregateIdExample(2))


    distanceSupplier.computeDistancesOfInSameThread = { _: Parameters, fileIds: List<String> ->
      addDistancesToClusters(fileIds)
    }

    val previousClusterId = PreviousClusterId(clusterId)
    val clustersFiles = listOf(Couple(previousClusterId, listOf(f0, f1)))
    `when`(clusteringRemoteApi.recalculateClusters(parameters, clustersFiles)).thenReturn(
      mutableMapOf(Pair(previousClusterId, TryOrCatch.success(clusteringOfTwoElementsWithoutMultiset()))).toMap()
    )

    // when
    clusteringService.computeDistancesThenRecalculateClustersSync(parameters)

    // then
    then(testEventHandler.events()).containsExactly(
      FileInfoReset(aggregateIdExample(2), null),
      DistancesToClusterCenterAdded(aggregateIdExample(2), listOf(DistanceToCluster(10, clusterId))),
      FileRemovedFromCluster(aggregateIdExample(0), clusterId, null),
      FileRemovedFromCluster(aggregateIdExample(1), clusterId, null),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId(FIRST_UUID.toString()).clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId(FIRST_UUID.toString()).build()
    )
  }

  private fun addDistancesToClusters(fileIds: List<String>) {
    val distancesToCluster = listOf(DistanceToCluster(10, "10"))
    val events: List<Event> = fileIds.map { fileId -> DistancesToClusterCenterAdded(fileId, distancesToCluster) }
    eventBus.dispatch(events)
  }

  private fun clusteringOfTwoElementsWithoutMultiset(): Clustering {
    val elements = listOf(
      ClusteringElement(aggregateIdExample(0), 10, true, false, null, null),
      ClusteringElement(aggregateIdExample(1), 10, false, true, null, null)
    )
    val clusterWithoutMultiset = listOf(Cluster(listOf(0, 1)))
    return Clustering(elements, null, clusterWithoutMultiset, null, null, null)
  }
}
