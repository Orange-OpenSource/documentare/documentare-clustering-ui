package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.command.AddToClusterCommand.AddToClusterAssociation
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.event.TestEventHandler
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Command AddToCluster")
class AddToClusterCommandTest {
  var testEventHandler = TestEventHandler()
  var eventBus = EventBus(DBEventInMemoryRepositoryForTestPurpose())
  var eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  var handler = AddToClusterCommandHandler(eventRepo, eventBus)

  @BeforeEach
  fun setUp() {
    eventBus.register(testEventHandler)
  }

  @Test
  fun add_file_to_cluster() {
    // given
    val id = DomainFile.builder().aggregateId(aggregateIdExample(0)).filename("filename").build().aggregateId
    eventRepo.insert(listOf(FileCreated(id, "filename")))
    val cmd = AddToClusterCommand(
      listOf(
        AddToClusterAssociation(id, "10", true, 1, true, null, null)
      )
    )
    // when
    val response = handler.execute(cmd)
    // then
    then(response.success()).isTrue()
    then(testEventHandler.events()).containsExactly(
      FileAddedToCluster.builder().aggregateId(id).clusterId("10").clusterCenter(true).distanceToCenter(null).inMultiset(true).build()
    )
  }
}
