package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.nhaarman.mockitokotlin2.*
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.query.AllFilesInWaitingRoomQuery
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQuery
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.ClusteringServiceImpl
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceService
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`

class FeederServiceLaunchDecisionTest {

  private var distanceService = mock<DistanceService>()
  private var clusteringService = mock<ClusteringServiceImpl>()

  private var feederService = spy(FeederServiceImpl(distanceService, clusteringService))
  private var commandBus: CommandBus? = mock()
  private var queryBus: QueryBus? = mock()

  @BeforeEach
  fun setup() {
    feederService.commandBus = commandBus!!
    feederService.queryBus = queryBus!!
    `when`(clusteringService.isRunning()).thenReturn(false)
    `when`(distanceService.isRunning()).thenReturn(false)

    mockParameters()
  }

  @Test
  fun `should do nothing on file created event if feeder is already running`() {
    // given
    val filesToSort = moreFilesThanTriggerThreshold()
    mockAllFilesInWaitingRoomQuery(filesToSort)

    feederService.forceFeedRunningForUnitTestPurposeOnly()

    // when
    feederService.onEvent(FileCreated.example())

    // then
    verify(feederService, never()).launchFeederInBackground(any(), any())
  }

  @Test
  fun `should do nothing on file created event if distance service is already running`() {
    // given
    val filesToSort = moreFilesThanTriggerThreshold()
    mockAllFilesInWaitingRoomQuery(filesToSort)
    `when`(distanceService.isRunning()).thenReturn(true)

    // when
    feederService.onEvent(FileCreated.example())

    // then
    verify(feederService, never()).launchFeederInBackground(any(), any())
  }

  @Test
  fun `should do nothing on file created event if clustering service is already running`() {
    // given
    val filesToSort = moreFilesThanTriggerThreshold()
    mockAllFilesInWaitingRoomQuery(filesToSort)
    `when`(clusteringService.isRunning()).thenReturn(true)

    // when
    feederService.onEvent(FileCreated.example())

    // then
    verify(feederService, never()).launchFeederInBackground(any(), eq(null))
  }

  @Test
  fun `should do nothing if parameters are not available`() {
    // given
    mockParametersAsNotAvailable()

    // when
    feederService.onEvent(FileCreated.example())

    // then
    verify(feederService, never()).launchFeederInBackground(any(), any())
  }

  @Test
  fun `should do nothing on file created event if feed parameter is not set`() {
    // given
    val filesToSort = moreFilesThanTriggerThreshold()
    mockAllFilesInWaitingRoomQuery(filesToSort)
    mockParametersWithoutFeed()

    // when
    feederService.onEvent(FileCreated.example())

    // then
    verify(feederService, never()).launchFeederInBackground(any(), any())
  }

  @Test
  fun `should do nothing if the number of files in waiting room is lower than the trigger threshold`() {
    // given
    val filesInWaitingRoom = emptyList<DomainFile>()
    mockAllFilesInWaitingRoomQuery(filesInWaitingRoom)

    // when
    feederService.onEvent(FileCreated.example())

    // then
    verify(feederService, never()).launchFeederInBackground(any(), any())
  }

  @Test
  fun `should launch the feedLoopAsync function if the number of files in waiting room is greater than the trigger threshold, on New File Created`() {
    // given
    val filesInWaitingRoom = moreFilesThanTriggerThreshold()
    mockAllFilesInWaitingRoomQuery(filesInWaitingRoom)
    doReturn(null).`when`(feederService).launchFeederInBackground(any(), eq(null))

    // when
    feederService.onEvent(FileCreated.example())

    // then
    verify(feederService).launchFeederInBackground(any(), eq(null))
  }

  @Test
  fun `should launch the feedLoopAsync function if the number of files in waiting room is greater than the trigger threshold`() {
    // given
    val filesInWaitingRoom = moreFilesThanTriggerThreshold()
    mockAllFilesInWaitingRoomQuery(filesInWaitingRoom)
    doReturn(null).`when`(feederService).launchFeederInBackground(any(), eq(null))

    // when
    feederService.launch()

    // then
    verify(feederService).launchFeederInBackground(any(), eq(null))
  }

  @Test
  fun `should not launch the feedLoopAsync function if waiting room is empty`() {
    // given
    val filesInWaitingRoom = emptyList<DomainFile>()
    mockAllFilesInWaitingRoomQuery(filesInWaitingRoom)

    doReturn(null).`when`(feederService).launchFeederInBackground(any(), any())

    // when
    feederService.launch()

    // then
    verify(feederService, never()).launchFeederInBackground(any(), any())
  }


  private fun mockAllFilesInWaitingRoomQuery(filesInWaitingRoom: List<DomainFile>) {
    val response = QueryResponse(TryOrCatch.success(filesInWaitingRoom))
    `when`<Any>(queryBus?.dispatch(AllFilesInWaitingRoomQuery())).thenReturn(response)
  }

  private fun moreFilesThanTriggerThreshold(): List<DomainFile> {
    return (0..TRIGGER_THRESHOLD * 2).map { DomainFile.builder().build() }
  }

  private fun mockParameters() {
    val response = QueryResponse(TryOrCatch.success(Parameters.builder().feed(true).feedThreshold(TRIGGER_THRESHOLD).minMultisetThreshold(2).maxMultisetThreshold(5).build()))
    `when`<Any>(queryBus?.dispatch(ParametersQuery())).thenReturn(response)
  }

  private fun mockParametersWithoutFeed() {
    val response = QueryResponse(TryOrCatch.success(Parameters.builder().feedThreshold(TRIGGER_THRESHOLD).minMultisetThreshold(2).maxMultisetThreshold(5).build()))
    `when`<Any>(queryBus?.dispatch(ParametersQuery())).thenReturn(response)
  }

  private fun mockParametersAsNotAvailable() {
    val response = QueryResponse(TryOrCatch.success(null))
    `when`<Any>(queryBus?.dispatch(ParametersQuery())).thenReturn(response)
  }

  companion object {
    private const val TRIGGER_THRESHOLD = 5
  }
}
