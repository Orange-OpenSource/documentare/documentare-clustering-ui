package com.orange.documentare.clusteringui.biz.remoteservices.distance.domain

import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.appnotifications.AppNotification
import com.orange.documentare.clusteringui.biz.appnotifications.AppNotificationsService
import com.orange.documentare.clusteringui.biz.file.command.AddDistancesToClustersCommandHandler
import com.orange.documentare.clusteringui.biz.file.command.AutoAddFileToClosestClusterCommandHandler
import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.event.MultisetElementMovedToNextHierarchyLevel
import com.orange.documentare.clusteringui.biz.file.query.*
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.biz.parameters.domain.DistanceParameters
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.persistence.ParametersRepository
import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQueryHandler
import com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure.DistanceRemoteApi
import com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure.DistancesRequestResult
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.orange.documentare.clusteringui.infrastructure.persistence.ParametersInMemoryRepository
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.queries.QueryBus
import org.assertj.core.api.BDDAssertions.catchException
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

class DistanceServiceTest {
  private val appNotifications = AppNotificationsService()
  private var distanceService = DistanceServiceImpl(appNotifications, EmbeddedComputationServer.disabled())
  private var eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  private var parametersRepository: ParametersRepository = ParametersInMemoryRepository().apply { insert(listOf(Parameters.defaults())) }
  private var projections = Projections(eventRepo)
  private var queryBus = QueryBus()
  private var commandBus = CommandBus()

  private val parameters = Parameters.defaults()

  @BeforeEach
  fun setup() {
    val eventBus = EventBus(eventRepo)
    eventBus.register(projections)

    commandBus.register(AddDistancesToClustersCommandHandler(eventRepo, eventBus), AutoAddFileToClosestClusterCommandHandler(eventRepo, eventBus))
    queryBus.register(
      ComputeDistancesQueryHandler(projections),
      DistanceToCenterStatsForAllFilesInClusterQueryHandler(projections),
      ParametersQueryHandler(parametersRepository),
      ClusterFilesQueryHandler(projections),
      SelectedFilesQueryHandler(projections),
      SelectedFilesAtLevel0QueryHandler(projections)
    )

    distanceService.setCommandBus(commandBus)
    distanceService.setQueryBus(queryBus)
  }

  @Test
  fun `do nothing when there are no files`() {
    // when
    distanceService.testComputeDistancesOfAllFilesNotInCluster()
  }

  @Test
  fun `do nothing and do not crash when there are no cluster centers`() {
    // given
    val events: Iterable<Event> = listOf<Event>(
      FileCreated(aggregateIdExample(0), "0")
    )
    projections.testPurposeInitFromHistory(events)

    // when
    distanceService.testComputeDistancesOfAllFilesNotInCluster()
  }

  @Test
  fun `do nothing and do not crash when there are cluster centers but no files`() {
    // given
    val events: Iterable<Event> = listOf<Event>(
      FileCreated(aggregateIdExample(0), "0", "10", true)
    )
    projections.testPurposeInitFromHistory(events)

    // when
    distanceService.testComputeDistancesOfAllFilesNotInCluster()
  }

  @Test
  fun `notify that distance computation is finished`() {
    // given
    setupFileEventsAndDistancesToClusters(
      eventList = listOf(
        FileCreated(aggregateIdExample(0), "f0"),
        FileCreated(aggregateIdExample(1), "f1", "10", true),
        FileCreated(aggregateIdExample(2), "f2"),
        FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("10").distanceToCenter(100).build()
      ),
      distancesToClusters = intArrayOf(9999) /* file 0 not included in cluster */
    )

    var actualAppNotification: AppNotification? = null
    appNotifications.register { actualAppNotification = it }

    // when
    distanceService.computeDistancesOf(parameters, listOf(aggregateIdExample(0)))

    // then
    do Thread.sleep(10)
    while (actualAppNotification != AppNotification.COMPUTE_DISTANCES_TO_CLUSTERS_DONE)
  }

  @Test
  fun `file 0 should be auto included when its distance to cluster center is below cluster threshold`() {
    // given
    setupFileEventsAndDistancesToClusters(
      eventList = listOf(
        FileCreated(aggregateIdExample(0), "f0"),
        FileCreated(aggregateIdExample(1), "f1", "10", true),
        FileCreated(aggregateIdExample(2), "f2"),
        FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("10").distanceToCenter(100).build()
      ),
      distancesToClusters = intArrayOf(99) /* file 0 is included in cluster */
    )

    // when
    distanceService.computeDistancesOf(parameters, listOf(aggregateIdExample(0)))

    // then
    val domainFile = waitForFileToBeIncludedInCluster(aggregateIdExample(0))
    then(domainFile).isEqualTo(
      DomainFile.builder()
        .aggregateId(aggregateIdExample(0)).filename("f0")
        .clusterId("10")
        .distanceToCenter(99)
        .automaticInclude(true)
        .build()
    )
  }

  @Test
  fun `file 0 should be auto included whatever its distance to singleton`() {
    // given
    setupFileEventsAndDistancesToClusters(
      eventList = listOf(
        FileCreated(aggregateIdExample(0), "f0"),
        FileCreated(aggregateIdExample(1), "f1", "10", true)
      ), distancesToClusters = intArrayOf(Int.MAX_VALUE - 1) /* file 0 should not be included in cluster if we use the distance, not considering the singleton information */
    )

    // when
    distanceService.computeDistancesOf(parameters, listOf(aggregateIdExample(0)))
    val domainFile = waitForFileToBeIncludedInCluster(aggregateIdExample(0))

    // then
    then(domainFile).isEqualTo(
      DomainFile.builder()
        .aggregateId(aggregateIdExample(0)).filename("f0")
        .clusterId("10")
        .distanceToCenter(Int.MAX_VALUE - 1)
        .automaticInclude(true)
        .build()
    )
  }

  @Test
  // useful to make sure distances to clusters are sorted... we can miss this case with only one distance to cluster
  fun `file 0 should be auto included when its distance to cluster center is below cluster threshold and other distances to clusters are present`() {
    // given
    setupFileEventsAndDistancesToClusters(
      eventList = listOf(
        FileCreated(aggregateIdExample(0), "f0"),
        FileCreated(aggregateIdExample(11), "f11", "11", true),
        FileCreated(aggregateIdExample(12), "f12", "12", true),
        FileCreated(aggregateIdExample(13), "f13", "13", true),
        FileCreated(aggregateIdExample(2), "f2"),
        FileCreated(aggregateIdExample(3), "f3"),
        FileCreated(aggregateIdExample(4), "f4"),
        FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("11").distanceToCenter(100).build(),
        FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("12").distanceToCenter(100).build(),
        FileAddedToCluster.builder().aggregateId(aggregateIdExample(4)).clusterId("13").distanceToCenter(100).build()
      ), distancesToClusters = intArrayOf(9999, 99, 9999) /* file 0 is included in cluster  12 */
    )

    // when
    distanceService.computeDistancesOf(parameters, listOf(aggregateIdExample(0)))
    val domainFile = waitForFileToBeIncludedInCluster(aggregateIdExample(0))

    // then
    then(domainFile).isEqualTo(
      DomainFile.builder()
        .aggregateId(aggregateIdExample(0)).filename("f0")
        .clusterId("12")
        .distanceToCenter(99)
        .automaticInclude(true)
        .build()
    )
  }

  @Test
  fun `file 0 should not be auto included when its distance to cluster center is above cluster threshold`() {
    // given
    setupFileEventsAndDistancesToClusters(
      eventList = listOf(
        FileCreated(aggregateIdExample(0), "f0"),
        FileCreated(aggregateIdExample(1), "f1", "10", true),
        FileCreated(aggregateIdExample(2), "f2"),
        FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("10").distanceToCenter(100).build()
      ), distancesToClusters = intArrayOf(101) /* file 0 not included in cluster */
    )

    // when
    distanceService.computeDistancesOf(parameters, listOf(aggregateIdExample(0)))
    val domainFile = waitForFileToHaveDistancesToClusters(aggregateIdExample(0))

    // then
    then(domainFile).isEqualTo(
      DomainFile.builder()
        .aggregateId(aggregateIdExample(0)).filename("f0")
        .distancesToClustersCenters(listOf(DistanceToCluster(101, "10")))
        .build()
    )
  }

  @Test
  fun `do not crash if distance remote process throws an exception`() {
    // given
    parametersRepository.insert(listOf(parameters))
    val distanceRemoteApi = DistanceRemoteApi { _: DistanceParameters, _: List<DomainFile>, _: Collection<List<DomainFile>> -> TryOrCatch.failure(IllegalStateException()) }
    distanceService.testSetDistanceRemote(distanceRemoteApi)
    val parameters = parametersRepository.findAll()

    // when
    distanceService.computeDistancesOf(parameters.first(), listOf("0"))
    Thread.sleep(100)
  }


  //======================================== COMPUTE DISTANCES OF FILES TO CENTROID ========================================
  @Test
  fun `compute distances of a file to centroid, throw error since file does not exist on disk`(@TempDir distancesPrepDir: File, @TempDir rawImageCacheDir: File) {
    // given
    val distanceService = DistanceServiceImpl(appNotifications, EmbeddedComputationServer.with(distancesPrepDir.absolutePath, rawImageCacheDir.absolutePath))
    distanceService.setQueryBus(queryBus)

    val aggregateId0 = aggregateIdExample(0)
    val aggregateId1 = aggregateIdExample(1)
    projections.testPurposeInitFromHistory(
      listOf(
        FileCreated(aggregateId0, "0"),
        FileCreated(aggregateId1, "1", "1", true)
      )
    )

    // when
    val exception = catchException { distanceService.computeDistancesToCentroidAnywhereInHierarchyInSameThread(listOf(aggregateId0), listOf(aggregateId1)) }

    // then
    then(exception.message).contains("❌ Failed to compute distance: java.lang.IllegalStateException: File is not a file")
  }

  @Test
  fun `compute distances of a file to centroid (not multiset)`(@TempDir filesDir: File, @TempDir distancesPrepDir: File, @TempDir rawImageCacheDir: File) {
    // given
    FileIO.initFilesDirectory(filesDir.absolutePath)
    val distanceService = DistanceServiceImpl(appNotifications, EmbeddedComputationServer.with(distancesPrepDir.absolutePath, rawImageCacheDir.absolutePath))
    distanceService.setQueryBus(queryBus)

    val aggregateId0 = aggregateIdExample(0)
    val aggregateId1 = aggregateIdExample(1)
    projections.testPurposeInitFromHistory(
      listOf(
        DomainFile.create(aggregateId0, "f0", null, false, "0".toByteArray()),
        DomainFile.create(aggregateId1, "f1", "1", true, "1".toByteArray())
      )
    )

    // when
    val distances = distanceService.computeDistancesToCentroidAnywhereInHierarchyInSameThread(listOf(aggregateId0), listOf(aggregateId1))

    // then
    then(distances).containsExactly(Pair(aggregateId0, 125000))
  }

  @Test
  fun `compute distances of a file to centroid (not multiset), files are in a lower hierarchy level`(
    @TempDir filesDir: File,
    @TempDir distancesPrepDir: File,
    @TempDir rawImageCacheDir: File
  ) {
    // given
    FileIO.initFilesDirectory(filesDir.absolutePath)
    val distanceService = DistanceServiceImpl(appNotifications, EmbeddedComputationServer.with(distancesPrepDir.absolutePath, rawImageCacheDir.absolutePath))
    distanceService.setQueryBus(queryBus)

    val aggregateId0 = aggregateIdExample(0)
    val aggregateId1 = aggregateIdExample(1)
    val aggregateId2 = aggregateIdExample(2)
    projections.testPurposeInitFromHistory(
      listOf(
        DomainFile.create(aggregateId0, "f0", "2", false, "0".toByteArray()),
        DomainFile.create(aggregateId1, "f1", "2", false, "1".toByteArray()),
        DomainFile.create(aggregateId2, "f2", "2", true, "1".toByteArray()),
        MultisetElementMovedToNextHierarchyLevel(aggregateId2, 1)
      )
    )

    // when
    val distances = distanceService.computeDistancesToCentroidAnywhereInHierarchyInSameThread(listOf(aggregateId0), listOf(aggregateId1))

    // then
    then(distances).containsExactly(Pair(aggregateId0, 125000))
  }

  @Test
  fun `compute distances of several files to centroid with multiset`(@TempDir filesDir: File, @TempDir distancesPrepDir: File, @TempDir rawImageCacheDir: File) {
    // given
    FileIO.initFilesDirectory(filesDir.absolutePath)
    val distanceService = DistanceServiceImpl(appNotifications, EmbeddedComputationServer.with(distancesPrepDir.absolutePath, rawImageCacheDir.absolutePath))
    distanceService.setQueryBus(queryBus)

    val aggregateId0 = aggregateIdExample(0)
    val aggregateId1 = aggregateIdExample(1)
    val aggregateId2 = aggregateIdExample(2)
    val aggregateId3 = aggregateIdExample(3)
    val file0CreationEvent = DomainFile.create(aggregateId0, "f0", null, false, "0".toByteArray())
    val file1CreationEvent = DomainFile.create(aggregateId1, "f1", null, false, "23".toByteArray())
    val file2CreationEvent = DomainFile.create(aggregateId2, "f2", "2", true, "2".toByteArray())
    val file3CreationEvent = DomainFile.create(aggregateId3, "f3", "2", false, "3".toByteArray())
    projections.testPurposeInitFromHistory(listOf(file0CreationEvent, file1CreationEvent, file2CreationEvent, file3CreationEvent))

    // when
    val distances = distanceService.computeDistancesToCentroidAnywhereInHierarchyInSameThread(listOf(aggregateId0, aggregateId1), listOf(aggregateId2, aggregateId3))

    // then
    then(distances).containsExactlyInAnyOrder(
      Pair(aggregateId0, 222222),
      Pair(aggregateId1, 0)
    )
  }

  //======================================================================================================================


  private fun setupFileEventsAndDistancesToClusters(eventList: List<Event>, vararg distancesToClusters: Int) {
    val events: Iterable<Event> = eventList
    eventRepo.insert(events)
    projections.testPurposeInitFromHistory(events)
    distanceService.testSetDistanceRemote(distancesOfFileToClusters(*distancesToClusters))
  }

  private fun distancesOfFileToClusters(vararg distances: Int) = DistanceRemoteApi { _: DistanceParameters, files: List<DomainFile>, _: Collection<List<DomainFile>> ->
    val map = mutableMapOf<DomainFile, DistancesRequestResult>()
    files.forEach {
      map[it] = DistancesRequestResult(distances, false, null, false)
    }
    TryOrCatch.of { map }
  }

  private fun waitForFileToHaveDistancesToClusters(aggregateId: String): DomainFile {
    var domainFile: DomainFile
    do {
      Thread.sleep(10)
      domainFile = projections.files[aggregateId]
    } while (domainFile.distancesToClustersCenters == null || domainFile.distancesToClustersCenters!!.isEmpty())
    return domainFile
  }

  private fun waitForFileToBeIncludedInCluster(aggregateId: String): DomainFile {
    var waitingTimes = 0
    var domainFile: DomainFile
    do {
      Thread.sleep(10)
      waitingTimes++
      domainFile = projections.files[aggregateId]
    } while (domainFile.clusterId == null && waitingTimes < 500)
    return domainFile
  }
}
