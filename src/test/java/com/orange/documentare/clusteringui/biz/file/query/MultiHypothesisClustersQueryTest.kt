package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.queries.QueryResponse
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class MultiHypothesisClustersQueryTest {

  private val eventRepository = DBEventInMemoryRepositoryForTestPurpose()
  private var projections = Projections(eventRepository)
  private val eventBus = EventBus(eventRepository).apply { register(projections) }
  private var handler = MultiHypothesisClustersQueryHandler(projections)

  @Test
  fun `retrieve only multi-hypothesis clusters`() {
    // given
    MultiHypothesisClustersView.Examples.oneMultiHypothesisClusterWithTwoFilesInit(eventBus)

    // when
    val response: QueryResponse<MultiHypothesisClustersView> = handler.execute(MultiHypothesisClustersQuery())

    // then
    then(response.success()).isTrue
    then(response.value()).isEqualTo(
      MultiHypothesisClustersView.Examples.oneMultiHypothesisClusterWithTwoFiles()
    )
  }
}
