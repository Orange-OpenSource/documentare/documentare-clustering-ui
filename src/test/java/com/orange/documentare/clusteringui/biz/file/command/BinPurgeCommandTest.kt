package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.event.FileDeleted
import com.orange.documentare.clusteringui.biz.file.event.FileDuplicated
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.event.TestEventHandler
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Command BinPurge")
class BinPurgeCommandTest {
  var testEventHandler = TestEventHandler()
  var eventBus = EventBus(DBEventInMemoryRepositoryForTestPurpose())
  var eventRepo = DBEventInMemoryRepositoryForTestPurpose()
  var handler = BinPurgeCommandHandler(eventRepo, eventBus)

  @BeforeEach
  fun setup() {
    eventBus.register(testEventHandler)
  }

  @Test
  fun bin_purge_command_will_delete_duplicates() {
    // given
    val file = DomainFile.builder()
      .aggregateId(aggregateIdExample(0))
      .filename("f0")
      .duplicate(true)
      .build()
    val initialEvents = listOf(
      FileCreated(aggregateIdExample(0), "f0"),
      FileDuplicated(aggregateIdExample(0), null)
    )
    eventRepo.insert(initialEvents)

    // when
    val response = handler.execute(BinPurgeCommand(listOf(aggregateIdExample(0))))

    // then
    then(response.success()).isTrue()
    then(testEventHandler.events()).containsExactly(
      FileDeleted(file.aggregateId, null)
    )
  }
}
