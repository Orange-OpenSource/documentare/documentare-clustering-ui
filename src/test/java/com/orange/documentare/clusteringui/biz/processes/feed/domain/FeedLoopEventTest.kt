package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeedLoopEvent
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import java.time.Instant


class FeedLoopEventTest {

  @Test
  fun `prefix with date`() {
    then(FeedLoopEvent.htmlTimestamp(Instant.parse("2013-08-12T14:15:59.479Z"))).isEqualTo("16:15:59.479&nbsp;&nbsp;&nbsp;")
  }
}
