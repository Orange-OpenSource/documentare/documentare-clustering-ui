package com.orange.documentare.clusteringui.biz.remoteservices.distance.domain

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile.Companion.builder
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import kotlin.concurrent.thread

class DistancesComputationInProgressTest {
  private var distancesComputationInProgress = DistancesComputationInProgress()

  @Test
  @Throws(InterruptedException::class)
  fun list_is_thread_safe() {
    // when
    (0..1000)
      .map { i: Int -> i.toString() }
      .map { fileId: String? -> builder().aggregateId(fileId).build() }
      .map { file: DomainFile -> thread { addThenRemoveFileId(file) } }
      .forEach { it.join() }

    // then
    then(distancesComputationInProgress.size()).isEqualTo(0)
  }

  private fun addThenRemoveFileId(file: DomainFile) {
    distancesComputationInProgress.add(file)
    then(distancesComputationInProgress.contains(file.aggregateId)).isTrue()
    distancesComputationInProgress.remove(file)
  }
}
