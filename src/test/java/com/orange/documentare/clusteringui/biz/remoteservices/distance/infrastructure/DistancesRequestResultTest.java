package com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure;

import static org.assertj.core.api.BDDAssertions.then;

import java.io.IOException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;



public class DistancesRequestResultTest {

  @DisplayName("can deserialize the distance request result")
  @Test
  void can_deserialize_the_distance_request_rest() throws IOException {
    // given
    String json = "{\n" +
      "  \"distances\" : [ 10, 20 ],\n" +
      "  \"error\" : false\n" +
      "}";

    ObjectMapper mapper = new ObjectMapper();

    // when
    DistancesRequestResult result = mapper.readValue(json, DistancesRequestResult.class);

    // then
    then(result).isEqualTo(DistancesRequestResult.with(new int[] { 10, 20 }));
  }
}
