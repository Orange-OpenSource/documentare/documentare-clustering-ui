package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.event.MultisetElementMovedToNextHierarchyLevel
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.event.TestEventHandler
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Command MoveClusterCentersToNextHierarchyLevelCommand")
class MoveMultisetElementToNextHierarchyLevelCommandTest {
  var eventBus = EventBus(DBEventInMemoryRepositoryForTestPurpose())
  var testEventHandler = TestEventHandler()
  var handler = MoveMultisetElementToNextHierarchyLevelCommandHandler(eventBus)

  @BeforeEach
  fun setup() {
    eventBus.register(testEventHandler)
  }

  @Test
  fun `cluster centers are moved to next hierarchy level`() {
    // given
    val aggregateId1 = aggregateIdExample(0)
    val aggregateId2 = aggregateIdExample(2)
    val nextHierarchyLevel = 2
    val cmd = MoveMultisetElementToNextHierarchyLevelCommand(listOf(aggregateId1, aggregateId2), nextHierarchyLevel)

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isTrue
    then(testEventHandler.events()).containsExactly(
      MultisetElementMovedToNextHierarchyLevel(
        aggregateId1,
        nextHierarchyLevel
      ),
      MultisetElementMovedToNextHierarchyLevel(
        aggregateId2,
        nextHierarchyLevel
      )
    )
  }
}
