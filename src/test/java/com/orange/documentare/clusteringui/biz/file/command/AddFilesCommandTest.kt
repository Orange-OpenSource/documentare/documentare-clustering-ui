package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.event.TestEventHandler
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.io.File

@DisplayName("Command AddFiles")
class AddFilesCommandTest {
  private val testEventHandler = TestEventHandler()
  private val eventBus = EventBus(DBEventInMemoryRepositoryForTestPurpose())
  private val handler = AddFilesCommandHandler(eventBus)
  private val uuidSupplierDomainFile = UUIDSupplierDomainFile()

  @BeforeEach
  fun setup() {
    File(FILES_DIRECTORY).deleteRecursively()
    FileIO.initFilesDirectory(FILES_DIRECTORY)
    eventBus.register(testEventHandler)
    handler.testPurposeSetUuidSuplier { uuidSupplierDomainFile.asString }
  }

  @Test
  fun add_file_named_titi_then_check_it_exists_in_db() {
    // given
    val image = File(javaClass.getResource("/image.png")!!.file)
    val bytes = image.readBytes()
    val filename = image.name
    val file = AddFilesCommand.File(filename, bytes)
    val cmd = AddFilesCommand(listOf(file))

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isTrue
    val id = uuidSupplierDomainFile.get(0)
    then(File("$FILES_DIRECTORY$id.png")).exists()
    then(File(FILES_DIRECTORY + "thumbnails/" + id)).exists()
    then(events()).containsExactly(FileCreated(id, filename))
  }

  @Test
  fun `added files aggregate ids are returned`() {
    // given
    val image = File(javaClass.getResource("/image.png")!!.file)
    val bytes = image.readBytes()
    val filename = image.name
    val file1 = AddFilesCommand.File(filename, bytes)
    val file2 = AddFilesCommand.File("another", bytes)
    val cmd = AddFilesCommand(listOf(file1, file2))

    // when
    val response: CommandResponse<List<String>> = handler.execute(cmd)

    // then
    then(response.success()).isTrue
    then(response.value()).containsExactly(uuidSupplierDomainFile.get(0), uuidSupplierDomainFile.get(1))
  }

  @Test
  fun add_file_with_cluster_details_then_check_it_exists_in_db() {
    // given
    val clusterId = "10"
    val clusterCenter = true
    val image = File(javaClass.getResource("/image.png")!!.file)
    val bytes = image.readBytes()
    val filename = image.name
    val file = AddFilesCommand.File(filename, bytes, clusterId, clusterCenter)
    val cmd = AddFilesCommand(listOf(file))

    // when
    val response = handler.execute(cmd)

    // then
    then(response.success()).isTrue
    val id = uuidSupplierDomainFile.get(0)
    then(File("$FILES_DIRECTORY$id.png")).exists()
    then(File(FILES_DIRECTORY + "thumbnails/" + id)).exists()
    then(events()).containsExactly(FileCreated(id, filename, clusterId, clusterCenter))
  }

  private fun events(): List<Event> {
    return testEventHandler.events()
  }

  companion object {
    const val FILES_DIRECTORY = "/tmp/"
  }
}
