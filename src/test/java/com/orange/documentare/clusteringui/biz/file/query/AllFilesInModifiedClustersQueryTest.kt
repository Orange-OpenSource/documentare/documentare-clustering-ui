package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.ClusterModified
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.queries.QueryResponse
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

class AllFilesInModifiedClustersQueryTest {
  var projections = Projections(DBEventInMemoryRepositoryForTestPurpose())
  var handler = AllFilesInModifiedClustersQueryHandler(projections)

  @Test
  fun retrieve_no_modified_clusters() {
    // given
    val events: Iterable<Event> = listOf<Event>(
      FileCreated(aggregateIdExample(0), "0", "1", true),
      FileCreated(aggregateIdExample(1), "1", "1", false),
      FileCreated(aggregateIdExample(2), "2", "1", false),
      FileCreated(aggregateIdExample(3), "3", "2", true),
      FileCreated(aggregateIdExample(4), "4", "2", false)
    )
    projections.testPurposeInitFromHistory(events)

    // when
    val response: QueryResponse<Map<String, List<DomainFile>>> = handler.execute(AllFilesInModifiedClustersQuery())

    // then
    then(response.success()).isTrue()
    then(response.value()).hasSize(0)
  }

  @Test
  fun retrieve_all_files_in_modified_clusters() {
    // given
    val events: Iterable<Event> = listOf(
      FileCreated(aggregateIdExample(0), "0", "1", true),
      ClusterModified(aggregateIdExample(0), "1"),
      FileCreated(aggregateIdExample(1), "1", "1", false),
      FileCreated(aggregateIdExample(2), "2", "1", false),
      FileCreated(aggregateIdExample(3), "3", "2", true),
      FileCreated(aggregateIdExample(4), "4", "2", false)
    )
    projections.testPurposeInitFromHistory(events)


    // when
    val response: QueryResponse<Map<String, List<DomainFile>>> = handler.execute(AllFilesInModifiedClustersQuery())

    // then
    then(response.success()).isTrue()
    then(response.value()).hasSize(1)
  }
}
