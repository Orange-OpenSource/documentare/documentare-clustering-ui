/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure

import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.remoteservices.infrastructure.BytesDataUI
import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import com.orange.documentare.tasksdispatcher.AvailableResultsTasksIdDTO
import com.orange.documentare.tasksdispatcher.TasksDispatcher
import jakarta.json.bind.JsonbBuilder
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File

internal class RemoteDistanceTest {
  private var server = MockWebServer()
  private val url = listOf(server.url("/").toString())
  private val remoteDistance = RemoteDistance(DefaultChangeListener(server), EmbeddedComputationServer.disabled())

  @TempDir
  lateinit var tmpDir: File

  private lateinit var fakeFile1: File
  private lateinit var fakeFile2: File

  @BeforeEach
  fun setup() {
    fakeFile1 = File(tmpDir.absolutePath + File.separator + "test_distance_request_1")
    fakeFile2 = File(tmpDir.absolutePath + File.separator + "test_distance_request_2")
  }

  @Test
  fun `receives task id, then distances results`() {
    // given
    val distancesRequests = buildDistancesRequests()

    // when
    val results = remoteDistance.request(url, distancesRequests)

    // then
    then(results.isSuccess).isTrue()
    val map = results.get()
    then(map[distancesRequests[0]]).isEqualTo(DistancesRequestResult.with(intArrayOf(11, 21)))
    then(map[distancesRequests[1]]).isEqualTo(DistancesRequestResult.with(intArrayOf(12, 22)))
  }

  private fun buildDistancesRequests(): List<DistancesRequestUI> {
    val fakeFiles: MutableList<File> = ArrayList()
    fakeFiles.add(fakeFile1)
    fakeFiles.add(fakeFile2)
    fakeFiles.forEach { file: File? -> TryOrCatch.run { file?.writeText("") } }
    val bytesData1 = BytesDataUI(listOf("1234"), listOf(fakeFile1.absolutePath))
    val bytesData2 = BytesDataUI(listOf("5678"), listOf(fakeFile2.absolutePath))
    val element1 = arrayOf(bytesData1)
    val element2 = arrayOf(bytesData2)
    val elements = arrayOf(bytesData1, bytesData2)
    val rawConverter = true
    val pCount = 10000
    val suffixArrayAlgorithm = SuffixArrayAlgorithm.LIBDIVSUFSORT

    return listOf(
      DistancesRequestUI(
        element1,
        elements,
        rawConverter,
        pCount,
        suffixArrayAlgorithm
      ),
      DistancesRequestUI(
        element2,
        elements,
        rawConverter,
        pCount,
        suffixArrayAlgorithm
      )
    )
  }

  open class DefaultChangeListener(private val server: MockWebServer) : TasksDispatcher.ChangeListener {
    private var tasksOnGoing = mutableListOf<String>()
    private var taskIdCount = 1
    private var resultCount = 1

    override fun willRetrieveAvailableResultsTasksId(url: String) {
      if (tasksOnGoing.isNotEmpty()) {
        mockAvailableResultsTasksIdWith(tasksOnGoing)
      } else {
        mockAvailableResultsTasksIdWith(emptyList())
      }
    }

    override fun willRetrieveATaskId(url: String) {
      val taskId = taskIdCount.toString()
      taskIdCount++
      mockRetrieveTaskId(taskId)
    }

    override fun willRetrieveResult(forTaskId: String, url: String) {
      mockRetrieveResult()
    }

    override fun receivedTaskId(taskId: String, url: String) {
      tasksOnGoing.add(taskId)
    }

    override fun receivedResultForTaskId(taskId: String, url: String) {
      tasksOnGoing.remove(taskId)
    }

    private fun mockAvailableResultsTasksIdWith(tasksIdList: List<String>) {
      val availableResultsTasksIdDTO = AvailableResultsTasksIdDTO(tasksIdList)
      val json = JsonbBuilder.create().toJson(availableResultsTasksIdDTO)
      server.enqueue(MockResponse().setBody(json))
    }

    private fun mockRetrieveTaskId(taskId: String) {
      server.enqueue(MockResponse().setBody("{\"id\": \"$taskId\"}"))
    }

    private fun mockRetrieveResult() {
      server.enqueue(MockResponse().setBody("{\"distances\":[${10 + resultCount}, ${20 + resultCount}],\"error\":false}"))
      resultCount++
    }
  }
}
