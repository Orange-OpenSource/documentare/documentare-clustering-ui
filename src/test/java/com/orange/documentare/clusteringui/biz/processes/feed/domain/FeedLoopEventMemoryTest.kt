package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederPhases.Phase
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederPhases.Phase.MetadataBalancingStep
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederPhases.UpdateType
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeedLoopStarted
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeederPhaseUpdated
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test

internal class FeedLoopEventMemoryTest {


  @Test
  fun `can get events we saved`() {
    // given
    val feedLoopEventMemory = FeedLoopEventMemory()
    feedLoopEventMemory.add(FeedLoopStarted(feedLoopCount = 2))
    feedLoopEventMemory.add(FeederPhaseUpdated(UpdateType.AT_FEED_LOOP_START, Phase.METADATA_BALANCING, MetadataBalancingStep.INCREMENTAL_CLUSTERING))

    // when
    val events = feedLoopEventMemory.events()

    // then
    then(events).containsExactly(
      FeedLoopStarted(feedLoopCount = 2),
      FeederPhaseUpdated(UpdateType.AT_FEED_LOOP_START, Phase.METADATA_BALANCING, MetadataBalancingStep.INCREMENTAL_CLUSTERING),
    )
  }

  @Test
  fun `keep only last 300 events`() {
    // given
    val feedLoopEventMemory = FeedLoopEventMemory()
    repeat(500) { feedLoopEventMemory.add(FeedLoopStarted(feedLoopCount = it)) }

    // when
    val events = feedLoopEventMemory.events()

    // then
    then(events.size).isEqualTo(300)
    then(events.last()).isEqualTo(FeedLoopStarted(feedLoopCount = 499))
  }
}
