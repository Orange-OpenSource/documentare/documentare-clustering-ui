package com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure

import com.orange.documentare.clusteringui.Couple
import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.core.model.ref.clustering.Clustering
import com.orange.documentare.core.model.ref.clustering.ClusteringParameters
import com.orange.documentare.core.model.ref.clustering.infra.network.dto.ClusteringDTO
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test


internal class RemoteClusteringTest {

  @Test
  internal fun `dispatch requests and provide clustering results with previous cluster id`() {
    // given
    val remoteClustering = RemoteClustering(EmbeddedComputationServer.disabled())
    val urls = listOf("http://machine")
    val clusteringRequestUI1 = ClusteringRequestUI.builder().outputDirectory("dir1").build()
    val clusteringRequestUI2 = ClusteringRequestUI.builder().outputDirectory("dir2").build()
    val clusteringRequests = listOf(
      Couple(PreviousClusterId("cl1"), clusteringRequestUI1),
      Couple(PreviousClusterId("cl2"), clusteringRequestUI2)
    )

    val clustering1 = Clustering(emptyList(), emptyList(), emptyList(), ClusteringParameters.builder().build(), null, "an error", false)
    val clustering2 = Clustering(emptyList(), emptyList(), emptyList(), ClusteringParameters.builder().build(), null, null, true)
    val testDispatcher = RemoteClustering.Dispatch { urlList, apiWrappers, resultObserver ->
      then(urlList).isEqualTo(listOf("http://machine"))
      then(apiWrappers).hasSize(2)
      then(apiWrappers[0].clusteringRequestUI).isEqualTo(clusteringRequestUI1)
      then(apiWrappers[1].clusteringRequestUI).isEqualTo(clusteringRequestUI2)

      resultObserver.accept(apiWrappers[0], ClusteringDTO.fromBusinessObject(clustering1))
      resultObserver.accept(apiWrappers[1], ClusteringDTO.fromBusinessObject(clustering2))
    }
    remoteClustering.setDispatchRequests(testDispatcher)

    // when
    val results = remoteClustering.request(urls, clusteringRequests)

    // then
    then(results).isEqualTo(
      mutableMapOf(
        Pair(PreviousClusterId("cl1"), TryOrCatch.success(clustering1)),
        Pair(PreviousClusterId("cl2"), TryOrCatch.success(clustering2))
      )
    )
  }
}
