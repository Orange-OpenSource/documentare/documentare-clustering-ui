package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.nhaarman.mockitokotlin2.mock
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class FeedLoopPeekSomeFilesInWaitingRoomTest {

  private lateinit var feedLoop: FeedLoop

  private val peekFilesInOrder: (List<DomainFile>, Int) -> List<String> = { files, count -> files.take(count).map { it.aggregateId } }
  private val filesInSortRoom = tenFilesInSortRoom()
  private val filesInWaitingRoom = sixtyFilesInWaitingRoom()
  private val feedThreshold = 3

  @BeforeEach
  fun setup() {
    feedLoop = FeedLoop(mock(), mock(), { Parameters.defaults() }, mock(), peekFilesInOrder, mock())
  }

  @Test
  fun `peek files in waiting room`() {
    // given
    val feedLoopCount = 2

    // when
    val filesIdsToMoveToSortRoom = feedLoop.peekSomeFilesInWaitingRoom(feedLoopCount, filesInSortRoom, filesInWaitingRoom, feedThreshold)

    // then
    then(filesIdsToMoveToSortRoom).hasSize(feedThreshold)
  }

  @Test
  fun `peek NO files in waiting room on first loop if sort room already contains files`() {
    // given
    val feedLoopCount = 0

    // when
    val filesIdsToMoveToSortRoom = feedLoop.peekSomeFilesInWaitingRoom(feedLoopCount, filesInSortRoom, filesInWaitingRoom, feedThreshold)

    // then
    then(filesIdsToMoveToSortRoom).hasSize(0)
  }

  private fun tenFilesInSortRoom() =
    (0..9).map {
      DomainFile.builder().aggregateId(it.toString()).build()
    }

  private fun sixtyFilesInWaitingRoom() =
    (0..59).map {
      DomainFile.builder().aggregateId((it + 100).toString()).build()
    }
}
