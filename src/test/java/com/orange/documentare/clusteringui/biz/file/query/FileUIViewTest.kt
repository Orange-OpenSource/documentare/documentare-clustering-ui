package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

internal class FileUIViewTest {
  @Test
  fun `build FileView from DomainFile`() {
    // when
    val fileUIView = FileUIView.from(
      DomainFile.builder()
        .aggregateId("id").filename("filename")
        .build()
    )

    // then
    then(fileUIView).isEqualTo(FileUIView("id", "filename", false, "/img/no_image.png", "/id", emptyList(),
      computeDistanceInProgress = false,
      automaticInclude = false,
      clusterModified = false,
      inMultiset = false,
      lock = false,
      fileNamedClass = null,
      distanceToCenter = null,
      viewStyleBackgroundColor = "white",
      viewStyleTextColor = "black"
    ))
  }

  @Test
  fun `build FileView from DomainFile with non null values`() {
    // when
    val distancesToClustersCenters = listOf(DistanceToCluster(100, "clusterId"))
    val fileUIView = FileUIView.from(
      DomainFile.builder()
        .aggregateId("id").filename("filename")
        .clusterCenter(true)
        .distancesToClustersCenters(distancesToClustersCenters)
        .distanceToCenter(100)
        .automaticInclude(true)
        .inMultiset(true)
        .clusterModified(true)
        .fileNamedClass(arrayOf("file named class"))
        .lock(true)
        .build()
    )

    // then
    val expected = FileUIView(
      "id",
      "filename",
      true,
      "/img/no_image.png",
      "/id",
      distancesToClustersCenters,
      computeDistanceInProgress = false,
      automaticInclude = true,
      clusterModified = true,
      inMultiset = true,
      lock = true,
      fileNamedClass = listOf("file named class"),
      distanceToCenter = 100,
      viewStyleBackgroundColor = "blueviolet",
      viewStyleTextColor = "white"
    )
    then(fileUIView).isEqualTo(expected)
  }

  @Test
  fun `display name is filename if file class name is null`() {
    // given
    val fileUIView = FileUIView.from(
      DomainFile.builder()
        .aggregateId("id").filename("filename")
        .build()
    )

    // when / then
    then(fileUIView.displayName()).isEqualTo("filename")
  }

  @Test
  fun `display name is filename if file named class is blank`() {
    // given
    val fileUIView = FileUIView.from(
      DomainFile.builder()
        .aggregateId("id").filename("filename")
        .fileNamedClass(arrayOf("  "))
        .build()
    )

    // when / then
    then(fileUIView.displayName()).isEqualTo("filename")
  }

  @Test
  fun `display name is filename if file named class is blank for all class names`() {
    // given
    val fileUIView = FileUIView.from(
      DomainFile.builder()
        .aggregateId("id").filename("filename")
        .fileNamedClass(arrayOf("  ", " ", ""))
        .build()
    )

    // when / then
    then(fileUIView.displayName()).isEqualTo("filename")
  }

  @Test
  fun `display name is filename followed by file named class if not blank`() {
    // given
    val fileUIView = FileUIView.from(
      DomainFile.builder()
        .aggregateId("id").filename("filename")
        .fileNamedClass(arrayOf("xyz"))
        .build()
    )

    // when / then
    then(fileUIView.displayName()).isEqualTo("filename ∈ xyz")
  }

  @Test
  fun `display name is filename followed by all file named class if not blank`() {
    // given
    val fileUIView = FileUIView.from(
      DomainFile.builder()
        .aggregateId("id").filename("filename")
        .fileNamedClass(arrayOf("bird", "eagle"))
        .build()
    )

    // when / then
    then(fileUIView.displayName()).isEqualTo("filename ∈ bird | eagle")
  }


  @MethodSource("viewColors")
  @ParameterizedTest
  fun `view color`(clusterId: String?, clusterCenter: Boolean, inMultiset: Boolean, automaticInclude: Boolean, clusterModified: Boolean, lock: Boolean, distancesToSortedClusters: List<DistanceToCluster>?, expectedBackgroundColor: String, expectedTextColor: String) {
    // given
    val fileUIView = FileUIView.from(
      DomainFile.builder()
        .clusterId(clusterId)
        .aggregateId("id").filename("filename")
        .clusterCenter(clusterCenter)
        .inMultiset(inMultiset)
        .automaticInclude(automaticInclude)
        .clusterModified(clusterModified)
        .lock(lock)
        .distancesToClustersCenters(distancesToSortedClusters)
        .build()
    )

    // when / then
    then(fileUIView.viewStyleBackgroundColor).isEqualTo(expectedBackgroundColor)
    then(fileUIView.viewStyleTextColor).isEqualTo(expectedTextColor)
  }

  companion object {
    @JvmStatic
    fun viewColors(): Stream<Arguments> {
      return Stream.of(
        // nothing special
        Arguments.of(/* cluster id */ "cluster-id-0", /* cluster center */ false, /* in multiset */ false, /* automatic include */ false, /* cluster modified */ false, /* lock */ false, /* distances to sorted clusters */ null, /* expected background color */ "white", /* expected text color */ "black"),
        // cluster center
        Arguments.of(/* cluster id */ "cluster-id-0", /* cluster center */ true, /* in multiset */ false, /* automatic include */ false, /* cluster modified */ false, /* lock */ false, /* distances to sorted clusters */ null, /* expected background color */ "green", /* expected text color */ "white"),
        // cluster center + lock
        Arguments.of(/* cluster id */ "cluster-id-0", /* cluster center */ true, /* in multiset */ false, /* automatic include */ false, /* cluster modified */ false, /* lock */ true, /* distances to sorted clusters */ null, /* expected background color */ "lightseagreen", /* expected text color */ "black"),
        // cluster center + cluster modified
        Arguments.of(/* cluster id */ "cluster-id-0", /* cluster center */ true, /* in multiset */ false, /* automatic include */ false, /* cluster modified */ true, /* lock */ false, /* distances to sorted clusters */ null, /* expected background color */ "aqua", /* expected text color */ "black"),
        // in multiset
        Arguments.of(/* cluster id */ "cluster-id-0", /* cluster center */ false, /* in multiset */ true, /* automatic include */ false, /* cluster modified */ false, /* lock */ false, /* distances to sorted clusters */ null, /* expected background color */ "chartreuse", /* expected text color */ "black"),
        // automatic include
        Arguments.of(/* cluster id */ "cluster-id-0", /* cluster center */ false, /* in multiset */ false, /* automatic include */ true, /* cluster modified */ false, /* lock */ false, /* distances to sorted clusters */ null, /* expected background color */ "orangered", /* expected text color */ "black"),

        // NOT in cluster - have distances to sorted clusters
        Arguments.of(/* cluster id */ null, /* cluster center */ false, /* in multiset */ false, /* automatic include */ false, /* cluster modified */ false, /* lock */ false, /* distances to sorted clusters */ listOf(DistanceToCluster(10, "a cluster id")), /* expected background color */ "blueviolet", /* expected text color */ "white"),
        // NOT in cluster
        Arguments.of(/* cluster id */ null, /* cluster center */ false, /* in multiset */ false, /* automatic include */ false, /* cluster modified */ false, /* lock */ false, /* distances to sorted clusters */ null, /* expected background color */ "white", /* expected text color */ "black")
      )
    }
  }
}
