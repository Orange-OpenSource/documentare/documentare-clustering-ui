package com.tophe.ddd.pad.query;

import static org.assertj.core.api.BDDAssertions.then;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose;
import com.tophe.ddd.infrastructure.event.EventBus;
import com.tophe.ddd.pad.command.CreatePadCommand;
import com.tophe.ddd.pad.command.CreatePadCommandHandler;
import com.tophe.ddd.pad.domain.Pad;
import com.tophe.ddd.pad.infrastructure.PadInMemoryRepository;
import com.tophe.ddd.pad.infrastructure.persistence.PadRepository;
import com.tophe.ddd.queries.QueryResponse;



public class GetPadQueryHandlerTest {

  @Test
  public void query_returns_empty_result_for_an_unknown_pad_id() {
    // given
    Long padId = null;
    GetPadQuery getPadQuery = new GetPadQuery(padId);
    GetPadQueryHandler queryHandler = new GetPadQueryHandler(new PadInMemoryRepository());

    // when
    QueryResponse<Optional<Pad>> response = queryHandler.execute(getPadQuery);

    // then
    then(response.success()).isTrue();
    then(response.value().isPresent()).isFalse();
  }

  @Test
  public void create_an_empty_pad_and_then_retrieve_it() {
    // given
    PadRepository padRepository = new PadInMemoryRepository();
    CreatePadCommandHandler cmdHandler = new CreatePadCommandHandler(padRepository, new EventBus(new DBEventInMemoryRepositoryForTestPurpose()));
    cmdHandler.execute(new CreatePadCommand());

    GetPadQuery getPadQuery = new GetPadQuery(0L);
    GetPadQueryHandler queryHandler = new GetPadQueryHandler(padRepository);

    // when
    QueryResponse<Optional<Pad>> response = queryHandler.execute(getPadQuery);

    // then
    then(response.success()).isTrue();
    then(response.value().isPresent()).isTrue();
  }
}
