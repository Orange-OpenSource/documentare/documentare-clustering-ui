package com.tophe.ddd.pad.domain;

import static org.assertj.core.api.BDDAssertions.then;

import org.junit.jupiter.api.Test;

public class PadTest {

  @Test
  public void create_empty_pad() {
    // when
    Pad pad = Pad.createEmptyPad();

    // then
    then(pad.id).isNull();
    then(pad.text).isEmpty();
  }

  @Test
  public void add_text_to_pad() {
    // when
    Pad pad = Pad.createEmptyPad();
    pad = pad.updateText("hi ddd");

    // then
    then(pad.text).isEqualTo("hi ddd");
  }
}
