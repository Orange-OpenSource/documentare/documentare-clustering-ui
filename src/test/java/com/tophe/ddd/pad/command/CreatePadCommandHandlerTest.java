package com.tophe.ddd.pad.command;

import static org.assertj.core.api.BDDAssertions.then;

import org.junit.jupiter.api.Test;

import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose;
import com.tophe.ddd.commands.CommandResponse;
import com.tophe.ddd.infrastructure.event.EventBus;
import com.tophe.ddd.pad.domain.Pad;
import com.tophe.ddd.pad.infrastructure.PadInMemoryRepository;
import com.tophe.ddd.pad.infrastructure.persistence.PadRepository;

public class CreatePadCommandHandlerTest {

  @Test
  public void create_an_empty_pad() {
    // given
    PadRepository padRepository = new PadInMemoryRepository();
    CreatePadCommandHandler handler = new CreatePadCommandHandler(padRepository, new EventBus(new DBEventInMemoryRepositoryForTestPurpose()));
    CreatePadCommand command = new CreatePadCommand();

    // when
    CommandResponse<Void> response = handler.execute(command);

    // then
    then(response.success()).isTrue();
    then(padRepository.findAll()).hasSize(1);
    Pad pad = padRepository.findAll().iterator().next();
  }
}
