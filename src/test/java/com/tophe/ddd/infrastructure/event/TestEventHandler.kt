package com.tophe.ddd.infrastructure.event

class TestEventHandler : EventHandler<Event>() {

  private val events: MutableList<Event> = mutableListOf()

  override fun onEvent(event: Event) {
    events.add(event)
  }

  override fun supports(event: Event): Boolean {
    return true
  }

  fun events(): List<Event> {
    return events
  }
}