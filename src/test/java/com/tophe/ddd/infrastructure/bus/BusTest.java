package com.tophe.ddd.infrastructure.bus;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.orange.documentare.clusteringui.TryOrCatch;

;

public class BusTest {

  TestBus bus;

  @BeforeEach
  public void setUp() {
    bus = new TestBus();
  }

  @Test
  public void dispatch_elem_to_registered_handler() {
    // given
    FakeBusElem1 fakeBusElem1 = new FakeBusElem1();
    FakeBusHandler1 handler = new FakeBusHandler1();

    // when
    bus.register(handler);
    bus.dispatch(fakeBusElem1);

    // then
    then(handler.executed).isTrue();
  }

  @Test
  public void dispatch_elem_to_correct_handler() {
    // given
    FakeBusElem1 fakeBusElem1 = new FakeBusElem1();
    FakeBusElem2 fakeBusElem2 = new FakeBusElem2();
    FakeBusHandler1 handler1 = new FakeBusHandler1();
    FakeBusHandler2 handler2 = new FakeBusHandler2();
    bus.register(handler1, handler2);

    // when
    bus.dispatch(fakeBusElem1);
    // then
    then(handler1.executed).isTrue();
    then(handler2.executed).isFalse();

    // when
    handler1.executed = false;
    bus.dispatch(fakeBusElem2);
    // then
    then(handler1.executed).isFalse();
    then(handler2.executed).isTrue();
  }

  @Test
  public void dispatch_elem_and_receives_response() {
    // given
    FakeBusElem1 fakeBusElem1 = new FakeBusElem1();
    FakeBusHandler1 handler = new FakeBusHandler1();

    // when
    bus.register(handler);
    BusResponse<String, TryOrCatch<String>> response = bus.dispatch(fakeBusElem1);

    // then
    then(response.success()).isTrue();
    then(response.value()).isEqualTo("fake response 1");
  }

  @Test
  public void dispatch_Null_and_receives_response() {
    // given
    FakeBusElem3 fakeBusElem3 = new FakeBusElem3();
    FakeBusHandler3 handler = new FakeBusHandler3();

    // when
    bus.register(handler);
    BusResponse<String, TryOrCatch<String>> response = bus.dispatch(fakeBusElem3);

    // then
    then(response.failure()).isTrue();
    Throwable throwable = catchThrowable(response::value);
    then(throwable).isNotNull();
    then(throwable.getMessage()).isEqualTo("oops");
  }

  @Test
  public void dispatch_without_registered_handlers_and_receives_failure_response() {
    FakeBusElem1 fakeBusElem1 = new FakeBusElem1();

    // when
    BusResponse response = bus.dispatch(fakeBusElem1);

    // then
    then(response.success()).isFalse();
    then(response.failureCause()).contains(NoBusHandlerFound.class.getName());
  }


  private class FakeBusHandler1 extends BusHandler<FakeBusElem1, String, BusResponse<String, TryOrCatch<String>>> {

    public boolean executed;

    @Override
    public BusResponse<String, TryOrCatch<String>> execute(FakeBusElem1 busElem) {
      executed = true;
      return new BusResponse<>(TryOrCatch.of(() -> "fake response 1"));
    }
  }

  private class FakeBusHandler2 extends BusHandler<FakeBusElem2, String, BusResponse<String, TryOrCatch<String>>> {

    public boolean executed;

    @Override
    public BusResponse<String, TryOrCatch<String>> execute(FakeBusElem2 busElem) {
      executed = true;
      return new BusResponse<>(TryOrCatch.of(() -> "fake response 2"));
    }
  }

  private class FakeBusHandler3 extends BusHandler<FakeBusElem3, String, BusResponse<String, TryOrCatch<String>>> {

    @Override
    public BusResponse<String, TryOrCatch<String>> execute(FakeBusElem3 busElem) {
      return new BusResponse(TryOrCatch.failure(new IllegalStateException("oops")));
    }
  }

  private class FakeBusElem1 implements BusElem {
  }

  private class FakeBusElem2 implements BusElem {
  }

  private class FakeBusElem3 implements BusElem {
  }

  private class TestBus extends Bus<BusHandler, BusElem> {

    @Override
    protected BusResponse failed(RuntimeException e) {
      return new BusResponse(TryOrCatch.failure(e));
    }
  }
}
