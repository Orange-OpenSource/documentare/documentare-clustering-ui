package com.tophe.ddd.infrastructure.persistence

import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class InMemoryRepositoryTest {
  private lateinit var repo: InMemoryRepository<Pojo, Long?>

  internal inner class Pojo {
    @Suppress("PropertyName")
    var id: Long? = null
  }

  @BeforeEach
  fun setUp() {
    repo = InMemoryRepository()
  }

  @Test
  fun two_added_elements_have_distinct_ids() {
    // given
    repo.insert(listOf(Pojo(), Pojo()))
    // when
    val list = repo.findAll().toList()

    // then
    then(list).hasSize(2)
    then(list[0].id).isNotEqualTo(list[1].id)
  }

  @Test
  fun find_all_elements() {
    // when
    repo.insert(listOf(Pojo()))
    val foundPojo = repo.findAll()
    // then
    then(foundPojo).hasSize(1)
    then(foundPojo.iterator().next().id).isNotNull
  }

  @Test
  fun save_two_elements_at_a_time() {
    // given
    val pojo1 = Pojo()
    val pojo2 = Pojo()
    // when
    repo.insert(listOf(pojo1, pojo2))
    // then
    then(repo.findAll()).hasSize(2)
  }

  @Test
  fun insert_two_elements_at_a_time() {
    // given
    val pojo1 = Pojo()
    val pojo2 = Pojo()
    // when
    repo.insert(listOf(pojo1, pojo2))
    // then
    then(repo.findAll()).hasSize(2)
  }

  @Test
  fun delete_all() {
    // given
    val pojo1 = Pojo()
    val pojo2 = Pojo()
    repo.insert(listOf(pojo1, pojo2))
    // when
    repo.deleteAll()
    // then
    then(repo.findAll()).hasSize(0)
  }

  @Test
  fun find_all_should_return_elements_in_insertion_order_for_String_id() {
    // given
    val ids0To100 = (0..100).toList()
    repo.insert(
      ids0To100.map { Pojo() }
    )
    // when
    val ids = repo.findAll().map { pojo: Pojo -> pojo.id!!.toInt() }
    // then
    then(ids).isEqualTo(ids0To100)
  }


  @Test
  fun `insertAll should raise exception if id not null`() {
    // given
    val entity = Pojo().apply { id = 123 }

    // when
    val result = runCatching { repo.insert(listOf(entity)) }

    // then
    var throwable: Throwable? = null
    then(result.isFailure).isTrue
    result.onFailure { throwable = it }
    then(throwable).isInstanceOf(IllegalStateException::class.java)
    then(throwable!!.message).isEqualTo("Entity should not already exists for insertion")
  }
}
