console.log('🔵 script client-clear-results loaded')

function homeButtonClicked () {
  console.log('🔵 reset classification results')
  fetch("/client/clear", {
    method: "POST"
  })
    .then(response => {
      if (response.status === 201) {
        console.log("✅ reset")
        window.location = '/client/'
      } else {
        alert("❌ reset failed: " + response.status)
      }
    })
}

document.getElementById('home-button').onclick = homeButtonClicked
