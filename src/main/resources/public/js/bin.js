console.log("Cool, bin.js is loaded :)");
const listFileIds = [];

function PurgeBin() {
  console.log("PurgeBin");
  ListFileIds();

  $.ajax({
    url: "/api/bin/purge",
    type: 'post',
    data: JSON.stringify(listFileIds),
    contentType: "application/json",
    success: function(_response) {
      window.location = "/";
    },
    error: function(response) {
    }
  })
}


function ListFileIds() {
  let i = 1;
  $(document.getElementsByClassName("card")).each(function () {
    i = i + 1;
    const id = this.getAttribute("id");
    console.log("id "+id);
    listFileIds.push(id);
  });
}
