console.log("Cool, clustering.js is loaded :)");

const selectedFileIdsSorted = [];
const selectedFileIdsToSort = [];

function SelectAll () {
  console.log("Select All");
  const selectedButton = document.getElementById("selectedButton");
  if (selectedButton.value === "") {
    selectedButton.value = "Select All";
  }

  let i = 1;

  console.log("count child " + document.getElementsByName("idToSort").length);
  $(document.getElementsByName("idToSort")).each(function () {
    if (document.getElementById("idToSort" + i) != null) {
      const idToSort = document.getElementById("idToSort" + i);
      console.log("idToSort " + i + " " + idToSort.value);
      idToSort.checked = selectedButton.value === "Select All";
    }
    i = i + 1;
  });

  if (selectedButton.value === "Select All") {
    selectedButton.innerHTML = "Deselect All";
    selectedButton.value = "Deselect All";
  } else {
    selectedButton.innerHTML = "Select All";
    selectedButton.value = "Select All";
  }

}

function updateHomePage (sourceMessage) {
  const url = new URL(location.href);
  url.searchParams.set('homeNotSortedShown', '' + homeNotSortedShown());
  url.searchParams.set('homeSortedShown', '' + homeSortedShown());

  console.log(Date.now() + ' ♻️ Reloading home page - ' + sourceMessage + ' - ' + url.href);
  location.replace(url.href);
}

function checkBackgroundOperation () {
  let backgroundOperationInProgress;
  let t;
  let globalClustering = document.getElementById("globalClustering");
  let partialClustering = document.getElementById("partialClustering");
  let createCluster = document.getElementById("createCluster");
  let createClusterLink = document.getElementById("createClusterLink");
  let computeDistance = document.getElementById("computeDistance");
  let computeDistanceLink = document.getElementById("computeDistanceLink");
  let addToCluster = document.getElementById("addToCluster");
  let addToClusterLink = document.getElementById("addToClusterLink");
  let timeMachine = document.getElementById("timeMachine");
  let feed = document.getElementById("feed");
  let hierarchy = document.getElementById("hierarchy");
  let hierarchyLevel = document.getElementById("hierarchy-level");
  let errorOccured = false;

  globalClustering.style.display = "none";
  partialClustering.style.display = "none";
  if (createCluster != null) {
    createCluster.style.display = "none";
  }
  createClusterLink.style.display = "none";
  if (computeDistance != null) {
    computeDistance.style.display = "none";
  }
  computeDistanceLink.style.display = "none";
  if (addToCluster != null) {
    addToCluster.style.display = "none";
  }
  addToClusterLink.style.display = "none";
  if (timeMachine != null) {
    timeMachine.style.display = "none";
  }
  if (feed != null) {
    feed.style.display = "none";
  }
  if (hierarchy != null) {
    hierarchy.style.display = "none";
  }
  if (hierarchyLevel != null) {
    hierarchyLevel.style.display = "none";
  }

  $.ajax({
    url: "/api/check-state-background-operation",
    success: function (response) {
      backgroundOperationInProgress = response;
      if (backgroundOperationInProgress === "yes") {
        console.log("Operation In Progress");
        computeDistance = document.getElementById("computeDistance");
        if (computeDistance != null) {
          computeDistance.style.display = "none";
        }
        computeDistanceLink = document.getElementById("computeDistanceLink");
        computeDistanceLink.style.display = "none";
      } else {
        console.log("Operation Terminated");
        clearTimeout(t);
      }
    },
    error: function (response) {
      errorOccured = true;
      console.log("[Check background operation] error occurred: " + JSON.stringify(response));
      clearTimeout(t);
    }
  })
  if (!errorOccured) {
    t = setTimeout(function () {
      checkBackgroundOperation()
    }, 10000);
  }
}

function stopFeed () {
  console.log("stopFeed");
  $.ajax({
    url: "/api/stop-feed",
    success: function () {
      console.log("stopFeed success");
      updateHomePage('[Stop feed]');
    },
    error: function () {
      console.log("stopFeed error");
    }
  })

}

function accordionNotSortedButtonClicked () {
  let shownBeforeToggle = $("#notSortedDisplayArea").length > 0;
  let shownAfterToggle = !shownBeforeToggle;
  localStorage.setItem('collapseElementNotSortedShown', "" + shownAfterToggle)
  updateHomePage('[Accordion NOT Sorted Button Clicked]');
}

function showAllSorted () {
  localStorage.setItem('collapseElementSortedShown', "" + true)
  updateHomePage('[Show All Sorted]');
}

function showOnlySortedClassNames () {
  localStorage.setItem('collapseElementSortedShown', "" + false)
  updateHomePage('[Show Only Sorted Class Names]');
}

function homeNotSortedShown () {
  return localStorage.getItem('collapseElementNotSortedShown') === 'true';
}

function homeSortedShown () {
  return localStorage.getItem('collapseElementSortedShown') === 'true';
}

function updateExpandAllSorted () {
  if (homeSortedShown() && localStorage.getItem('expandAllSorted') === 'true') {
    expandAllSorted()
  }
}

function expandAllSorted () {
  localStorage.setItem('expandAllSorted', 'true')
  $('.collapse').collapse('show');
}

function collapseAllSorted () {
  localStorage.setItem('expandAllSorted', 'false')
  $('.collapse.show').collapse('hide');
}


function Naming () {
  window.location = "/naming";
}


$(document).ready(function () {
  $(window).bind('beforeunload', function () {
    localStorage['scrollTop'] = $(window).scrollTop();
  })
  if (localStorage['scrollTop'] !== "undefined") {
    $(window).scrollTop(localStorage['scrollTop'])
  }
  updateExpandAllSorted()
})

function Remove () {
  console.log("Remove");
  listSelectedFileIdsSorted();

  $.ajax({
    url: "/api/remove-file",
    type: 'post',
    data: JSON.stringify(selectedFileIdsSorted),
    contentType: "application/json",
    success: function () {
      window.location = "/";
    },
    error: function (response) {
    }
  })
}

function LockCluster () {
  console.log("LockCluster");
  listSelectedFileIdsSorted();

  $.ajax({
    url: "/api/lock-cluster",
    type: 'post',
    data: JSON.stringify(selectedFileIdsSorted),
    contentType: "application/json",
    success: function () {
      window.location = "/";
    },
    error: function (response) {
    }
  })
}

function UnLockCluster () {
  console.log("LockCluster");
  listSelectedFileIdsSorted();

  $.ajax({
    url: "/api/unlock-cluster",
    type: 'post',
    data: JSON.stringify(selectedFileIdsSorted),
    contentType: "application/json",
    success: function () {
      window.location = "/";
    },
    error: function (response) {
    }
  })
}

function listSelectedFileIdsSorted () {
  const t0 = performance.now();

  $(document.getElementsByClassName("form-check-input")).each(function (_i, element) {
    if (element.checked) {
      const selectedFileId = element.value;
      if (selectedFileIdsSorted.indexOf(selectedFileId) === -1) {
        console.log("selectedFileId " + selectedFileId);
        selectedFileIdsSorted.push(selectedFileId);
      }
    }
  });
  const t1 = performance.now();
  console.log("listSelectedFileIdsSorted took " + (t1 - t0) + " milliseconds.")
}

function listSelectedFileIdsToSort () {
  const t0 = performance.now();

  $(document.getElementsByClassName("form-check-input")).each(function (_i, element) {
    if (element.checked) {
      const selectedFileId = element.value;
      if (selectedFileIdsToSort.indexOf(selectedFileId) === -1) {
        console.log("selectedFileId " + selectedFileId);
        selectedFileIdsToSort.push(selectedFileId);
      }
    }
  });

  const t1 = performance.now();
  console.log("listSelectedFileIdsToSort took " + (t1 - t0) + " milliseconds.")
}

function Delete () {
  console.log("Delete");
  listSelectedFileIdsToSort();

  $.ajax({
    url: "/api/delete-file",
    type: 'post',
    data: JSON.stringify(selectedFileIdsToSort),
    contentType: "application/json",
    success: function () {
      window.location = "/";
    },
    error: function (response) {
    }
  })
}

function ComputeDistances () {
  console.log("ComputeDistances");
  listSelectedFileIdsToSort();

  $.ajax({
    url: "/api/computeDistances",
    type: 'post',
    data: JSON.stringify(selectedFileIdsToSort),
    contentType: "application/json",
    success: function () {
      window.location = "/";
    },
    error: function (response) {
    }
  })
}

function CreateCluster () {
  console.log("CreateCluster");
  listSelectedFileIdsToSort();

  $.ajax({
    url: "/api/createCluster",
    type: 'post',
    data: JSON.stringify(selectedFileIdsToSort),
    contentType: "application/json",
    success: function () {
      window.location = "/";
    },
    error: function (response) {
    }
  })
}
