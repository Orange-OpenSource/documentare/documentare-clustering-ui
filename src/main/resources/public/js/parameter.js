const submit = document.getElementById('submit');

function checkMinMultiSetThreshold () {
  const min = parseInt(document.getElementById('minMultisetThreshold').value);
  const max = parseInt(document.getElementById('maxMultisetThreshold').value);

  if (min > '' && max < min) {
    alert("Min invalide");
    submit.disabled = true;
  } else {
    submit.disabled = false;
  }
}

function checkMaxMultiSetThreshold () {
  const min = parseInt(document.getElementById('minMultisetThreshold').value);
  const max = parseInt(document.getElementById('maxMultisetThreshold').value);
  if (max > '' && max < min) {
    alert("Max invalide");
    submit.disabled = true;
  } else {
    submit.disabled = false;
  }
}
