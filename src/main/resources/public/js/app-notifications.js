console.log("Cool, app-notifications.js is loaded :)");

let eventSource = undefined;
initAppNotificationsListener()

function initAppNotificationsListener() {
  eventSource = new EventSource('/app-notifications')

  eventSource.addEventListener("message", (event) => {
    const message = event.data;
    console.log('[APP Notifications] received new message: ' + message);
    $('.toast-body').text('🐞 ' + message)
    $('.toast').toast('show');
    setTimeout(() => {
      updateHomePage('[APP Notifications] received new message: ' + message);
    }, 5000)
  })

  eventSource.onerror = (error) => {
    console.log("[APP Notifications] error occurred: " + JSON.stringify(error, ["message", "arguments", "type", "name"]));
    eventSource.close()
    //alert("An error occurred while connecting to the server. Please refresh the page.")
  };
}

