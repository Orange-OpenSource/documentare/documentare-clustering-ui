console.log("Cool, naming.js is loaded :)");
let selectedFileIdsToName = [];
let classe = {};
const $formNameClasse = $('#formNameClasse');

$formNameClasse.on('submit', function(event) {
  console.log("NameClasse");
  selectedFileIdsToName = [];
  event.preventDefault();
  ListSelectedFileIdsToNameClasse();
  classe = {
    name : $('#classeName').val(),
    selectedFileIdsToName : selectedFileIdsToName
  };

  $.ajax({
    url: $formNameClasse.attr('action'),
    type: 'post',
    data: JSON.stringify(classe),
    contentType: "application/json",
    success: function(response) {
      var url = "/naming";
      window.location = url;
    },
    error: function(response) {
    }
  })
});



function ListSelectedFileIdsToNameClasse() {
  console.log("ListSelectedFileIdsToNameClasse");

  $(document.getElementsByClassName("form-check-input")).each(function (_i, element) {
    if (element.checked) {
      const selectedFileId = element.value;
      if (selectedFileIdsToName.indexOf(selectedFileId) === -1) {
        console.log("selectedFileId " + selectedFileId);
        selectedFileIdsToName.push(selectedFileId);
      }
    }
  });
}
