console.log("Cool, associate.js is loaded :)");
var selectedFileIdsToSort = [];
var selectedFileIdsSorted = [];
var listAssociateFileIds = [];

function AddFilesToCluster() {
  console.log("AddFilesToCluster");
  ListSelectedFileIds();

  $.ajax({
    url: "/api/add-files-to-cluster",
    type: 'post',
    data: JSON.stringify(listAssociateFileIds),
    contentType: "application/json",
    success: function(response) {
      var url = "/";
      window.location = url;
    },
    error: function(response) {
    }
  })
}

function CreateNewCluster() {
  console.log("CreateNewCluster");
  ListSelectedFileIdsToCreateNewCluster();

  $.ajax({
    url: "/api/create-new-cluster",
    type: 'post',
    data: JSON.stringify(selectedFileIdsToSort),
    contentType: "application/json",
    success: function(response) {
      var url = "/";
      window.location = url;
    },
    error: function(response) {
    }
  })
}

function RemoveCluster() {
  console.log("RemoveCluster");
  ListselectedFileIdsSortedCluster();

  $.ajax({
    url: "/api/remove-file",
    type: 'post',
    data: JSON.stringify(selectedFileIdsSorted),
    contentType: "application/json",
    success: function(response) {
      var url = "/";
      window.location = url;
    },
    error: function(response) {
    }
  })
}

function ListSelectedFileIds() {
  i= 1;
  $(document.getElementsByClassName("card")).children("label").each(function () {
    if (document.getElementById("idToSort"+i) != null) {
      if (document.getElementById("idToSort" + i).checked) {
        var elementToSort = document.getElementById("idToSort"+i);
        var selectedFileIdToSort = document.getElementById("idToSort" + i).value;
        console.log("i "+i+ " selectedFileIdToSort " + selectedFileIdToSort);
        selectedFileIdsToSort.push(selectedFileIdToSort);
        var parent = elementToSort;
        do {
          parent = parent.parentNode;
          console.log("parent "+parent.getAttribute('class'));
        } while (parent.getAttribute('class') != 'row');
        console.log("nb child "+parent.getElementsByClassName("col-lg-10").length);
        if (parent.getElementsByClassName("col-lg-10").length == 1) {
          var child = parent.getElementsByClassName("col-lg-10")[0];
          $(child.getElementsByClassName("form-check-label")).children("input").each(function () {
            if ($(this).prop('checked') && this.value != selectedFileIdToSort) {
              var selectedFileidSorted = this.value;
              console.log("a trier " + selectedFileIdToSort + " a associer à " + selectedFileidSorted);
              var associateFileIds = {
                idFileToSort : selectedFileIdToSort,
                idClusterCenter : selectedFileidSorted
              }
              listAssociateFileIds.push(associateFileIds);
              return;
            }
          });
        }
      }

    }
    i = i + 1;

  });
}

function ListSelectedFileIdsToCreateNewCluster() {
  i= 1;
  $(document.getElementsByClassName("card")).children("label").each(function () {
    if (document.getElementById("idToSort"+i) != null) {
      if (document.getElementById("idToSort" + i).checked) {
        var selectedFileIdToSort = document.getElementById("idToSort" + i).value;
        console.log("selectedFileIdToSort " + selectedFileIdToSort);
        selectedFileIdsToSort.push(selectedFileIdToSort);
      }
    }
    i = i + 1;
  });
}


function ListselectedFileIdsSortedCluster() {
  i= 1;
  $(document.getElementsByClassName("card")).children("label").each(function () {
    if (document.getElementById("idSorted"+i) != null) {
      if (document.getElementById("idSorted" + i).checked) {
        var selectedFileidSorted = document.getElementById("idSorted" + i).value;
        if (selectedFileIdsSorted.indexOf(selectedFileidSorted) == -1) {
          console.log("selectedFileidSorted " + selectedFileidSorted);
          selectedFileIdsSorted.push(selectedFileidSorted);
        }
      }
    }
    i = i + 1;
  });
}

function Check(element) {
  var parent = element.parentNode;
  do {
    parent = parent.parentNode;
    console.log("parent "+parent.getAttribute('class'));
  } while (parent.getAttribute('class') != 'col-lg-10');
  i= 1;
  $(parent.getElementsByClassName("form-check-label")).children("input").not(element).prop("checked",false);

}