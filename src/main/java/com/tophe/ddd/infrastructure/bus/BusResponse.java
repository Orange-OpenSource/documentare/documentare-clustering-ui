package com.tophe.ddd.infrastructure.bus;

import java.util.function.Consumer;

import com.orange.documentare.clusteringui.TryOrCatch;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BusResponse<V, T extends TryOrCatch<V>> {

  private final T value;

  public BusResponse(T t) {
    this.value = t;
  }

  public boolean success() {
    return value.isSuccess();
  }

  public boolean failure() {
    return value.isFailure();
  }

  public V value() {
    V value = this.value.get();
    if (value == null) {
      log.warn("[{}] Trying to retrieve a null value", this.getClass().getSimpleName());
    }
    return value;
  }

  public String failureCause() {
    Throwable cause = value.getCause();
    String message = cause.getClass().getName() + " : " + cause.getMessage();
    cause.printStackTrace();
    return message;
  }

  public TryOrCatch<V> onSuccess(Consumer<V> f) {
    return value.onSuccess(f);
  }

  public TryOrCatch<V> onFailure(Consumer<Throwable> t) {
    return value.onFailure(t);
  }
}
