package com.tophe.ddd.infrastructure.persistence;

import java.util.List;

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile;
import com.tophe.ddd.infrastructure.event.Event;
import com.tophe.ddd.infrastructure.event.PersistencePauseAndResume;

public interface EventRepository extends Repository<Event>, PersistencePauseAndResume {

  DomainFile load(String aggregateId);

  List<DomainFile> load(List<String> aggregateIds);
}
