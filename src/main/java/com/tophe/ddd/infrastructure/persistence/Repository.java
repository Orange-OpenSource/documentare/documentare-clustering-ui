package com.tophe.ddd.infrastructure.persistence;

import java.util.OptionalLong;

public interface Repository<AggregateRoot> {

  <S extends AggregateRoot> void insert(Iterable<S> entities);

  Iterable<AggregateRoot> findAll();

  RepoPage<AggregateRoot> findAllInPage(RepoPageable repoPageable);

  long count();

  void deleteAll();

  default int applyOnAll(RepositoryApplyOnAll<AggregateRoot> applyOnAll, int pageSize) {
    int calledCount = 0;
    RepoPageable repoPageable = new RepoPageable(0, pageSize, OptionalLong.empty());
    while (repoPageable != null) {
      RepoPage<AggregateRoot> repoPage = findAllInPage(repoPageable);
      applyOnAll.applyOnAll(repoPage.content());
      repoPageable = repoPage.nextRepoPageable();
      calledCount++;
    }
    return calledCount;
  }
}
