package com.tophe.ddd.infrastructure.persistence;

import java.util.OptionalLong;

public record RepoPageable(int pageNumber, int pageSize, OptionalLong lastPageIndex) {

}
