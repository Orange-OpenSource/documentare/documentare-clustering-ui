package com.tophe.ddd.infrastructure.persistence;

import java.lang.reflect.Field;
import java.util.*;

/**
 * In memory repository useful for tests purpose
 * Should support Long AggregateId but it is not completely tested in this case
 */
public class InMemoryRepository<AggregateRoot, AggregateId> implements Repository<AggregateRoot> {

  private final SortedMap<AggregateId, AggregateRoot> map = new TreeMap<>(new KeyComparator<>());

  @Override
  public <S extends AggregateRoot> void insert(Iterable<S> entities) {
    for (S entity : entities) {
      Optional<AggregateId> field = retrieveEntityIdField(entity);
      AggregateId id = field.orElseThrow(() -> new IllegalStateException("Failed to retrieve the AggregateId field in aggregate: " + entity));
      if (!entityDoesNotExistYet(id, entity)) {
        throw new IllegalStateException("Entity should not already exists for insertion");
      }
    }
    entities.forEach(this::save);
  }

  @Override
  public Iterable<AggregateRoot> findAll() {
    return map.values();
  }

  @Override
  public RepoPage<AggregateRoot> findAllInPage(RepoPageable repoPageable) {
    int count = map.size();
    int pageSize = repoPageable.pageSize();
    int pageCount = pageSize >= count ? 1 : count / pageSize + 1;
    List<AggregateRoot> content = new ArrayList<>();
    RepoPageable nextRepoPageable = null;
    int pageNumber = repoPageable.pageNumber();
    if (pageNumber < pageCount - 1) {
      nextRepoPageable = new RepoPageable(pageNumber + 1, pageSize, OptionalLong.empty());
    }
    if (pageNumber <= pageCount - 1) {
      List<AggregateRoot> all = new ArrayList<>(map.values());
      int lastIndex = Math.min((pageNumber + 1) * pageSize, map.size());
      content = all.subList(pageNumber * pageSize, lastIndex);
    }
    return new RepoPage<>(content, nextRepoPageable);
  }

  @Override
  public long count() {
    return map.size();
  }

  @Override
  public void deleteAll() {
    map.clear();
  }


  private <S extends AggregateRoot> void save(S aggregate) {
    Optional<AggregateId> field = retrieveEntityIdField(aggregate);
    field
      .flatMap(id -> doSaveAggregate(id, aggregate))
      .orElseThrow(() -> new IllegalStateException("Failed to retrieve the AggregateId field in aggregate: " + aggregate));
  }

  private <S extends AggregateRoot> Optional<S> doSaveAggregate(AggregateId id, S entity) {
    AggregateId saveId = id;
    if (entityDoesNotExistYet(id, entity)) {
      saveId = buildNewId(id);
      forceUpdateEntityId(saveId, entity);
    }
    map.put(saveId, entity);
    return Optional.of(entity);
  }

  private <S extends AggregateRoot> boolean entityDoesNotExistYet(AggregateId id, S entity) {
    return !map.containsKey(id) && isEmptyId(id, entity);
  }


  private <S extends AggregateRoot> Optional<AggregateId> retrieveEntityIdField(S entity) {
    Field f = null;
    try {
      Class<?> clazz = entity.getClass();
      f = retrieveField(f, clazz);
      if (f == null) {
        throw new NoSuchFieldException();
      }

      f.setAccessible(true);
      AggregateId id = (AggregateId) f.get(entity);
      id = id == null ? emptyID(f.getType()) : id;
      return Optional.of(id);
    }
    catch (NoSuchFieldException | IllegalAccessException e) {
      return Optional.empty();
    }
  }

  private Field retrieveField(Field f, Class<?> clazz) {
    while (clazz != null && f == null) {
      try {
        f = clazz.getDeclaredField("id");
      }
      catch (Exception ignored) {
      }
      clazz = clazz.getSuperclass();
    }
    return f;
  }

  private <S extends AggregateRoot> void forceUpdateEntityId(AggregateId id, S entity) {
    Field f;
    try {
      f = entity.getClass().getDeclaredField("id");
      f.setAccessible(true);
      f.set(entity, id);
    }
    catch (NoSuchFieldException | IllegalAccessException ignored) {
    }
  }

  private AggregateId buildNewId(AggregateId id) {
    if (id instanceof Long) {
      return (AggregateId) Long.valueOf(map.size());
    }
    throw new IllegalStateException("Unsupported AggregateId type: " + id.getClass().toString());
  }

  private AggregateId emptyID(Class<?> clazz) {
    if (clazz.getSimpleName().equals("Long")) {
      return (AggregateId) Long.valueOf(-1);
    }
    throw new IllegalStateException("Unsupported AggregateId type: " + clazz.getName());
  }

  private <S extends AggregateRoot> boolean isEmptyId(AggregateId id, S entity) {
    try {
      Field field;
      field = entity.getClass().getDeclaredField("id");
      return id.equals(emptyID(field.getType()));
    }
    catch (NoSuchFieldException e) {
      return true;
    }
  }
}
