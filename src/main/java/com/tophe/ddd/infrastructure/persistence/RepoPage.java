package com.tophe.ddd.infrastructure.persistence;

import java.util.List;

public record RepoPage<AggregateRoot>(List<AggregateRoot> content, RepoPageable nextRepoPageable) {
}
