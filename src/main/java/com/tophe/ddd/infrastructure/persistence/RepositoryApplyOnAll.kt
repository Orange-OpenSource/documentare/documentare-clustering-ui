package com.tophe.ddd.infrastructure.persistence

interface RepositoryApplyOnAll<AggregateRoot> {
  fun applyOnAll(aggregateRoots: List<AggregateRoot>)
}