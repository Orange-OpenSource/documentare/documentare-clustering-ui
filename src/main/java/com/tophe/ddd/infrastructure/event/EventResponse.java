package com.tophe.ddd.infrastructure.event;

import com.orange.documentare.clusteringui.TryOrCatch;
import com.tophe.ddd.infrastructure.bus.BusResponse;

public class EventResponse<V> extends BusResponse<V, TryOrCatch<V>> {

  public EventResponse(TryOrCatch<V> t) {
    super(t);
  }
}
