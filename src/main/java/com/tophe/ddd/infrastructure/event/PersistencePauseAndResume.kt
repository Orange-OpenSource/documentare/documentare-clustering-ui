package com.tophe.ddd.infrastructure.event

interface PersistencePauseAndResume {
  fun pauseHardPersistenceAndAppendToVolatilePersistence()
  fun stopHardAndVolatilePersistence()
  fun resumeHardPersistenceAndDropVolatilePersistence()
}
