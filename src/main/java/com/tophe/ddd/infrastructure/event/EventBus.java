package com.tophe.ddd.infrastructure.event;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.tophe.ddd.infrastructure.persistence.EventRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class EventBus {

  private final List<EventHandler> handlers = new ArrayList<>();
  private final EventRepository eventRepository;

  public void register(EventHandler... handlers) {
    this.handlers.addAll(Arrays.asList(handlers));
  }

  public void dispatch(Collection<Event> events) {
    eventRepository.insert(events);
    events.forEach(event -> {
      handlers.stream()
        .filter(h -> h.supports(event))
        .forEach(h -> h.onEvent(event));
    });
  }
}
