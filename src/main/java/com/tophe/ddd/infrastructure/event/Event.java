package com.tophe.ddd.infrastructure.event;

import java.time.Instant;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode
@ToString
public abstract class Event {

  public final String aggregateId;

  // we could avoid excluding this field with shared DATE Examples, but it would clutter tests with test date setup
  @EqualsAndHashCode.Exclude
  public final Instant date;

  protected Event(String aggregateId) {
    this(aggregateId, getCurrentLocalDateTimeStamp());
  }

  protected Event(String aggregateId, Instant date) {
    this.aggregateId = aggregateId;
    this.date = date;
  }

  static Instant getCurrentLocalDateTimeStamp() {
    return Instant.now();
  }
}
