package com.tophe.ddd.commands;

import java.util.Collection;

import com.orange.documentare.clusteringui.AggregateAndEvents;
import com.orange.documentare.clusteringui.TryOrCatch;
import com.tophe.ddd.infrastructure.bus.BusHandler;
import com.tophe.ddd.infrastructure.event.Event;
import com.tophe.ddd.infrastructure.event.EventBus;

import lombok.NoArgsConstructor;

;

@NoArgsConstructor
public abstract class CommandHandler<T extends Command, R> extends BusHandler<T, R, CommandResponse<R>> {

  private EventBus eventBus;

  public CommandHandler(EventBus eventBus) {
    this.eventBus = eventBus;
  }

  @Override
  public CommandResponse<R> execute(T command) {
    return new CommandResponse<>(TryOrCatch.of(() -> {
      AggregateAndEvents<R, Collection<Event>> tuple = doExecute(command);
      dispatchEvents(tuple.events);
      return tuple.aggregate;
    }));
  }

  private void dispatchEvents(Collection<Event> events) {
    if (eventBus != null) {
      eventBus.dispatch(events);
    }
  }

  protected abstract AggregateAndEvents<R, Collection<Event>> doExecute(T command);
}
