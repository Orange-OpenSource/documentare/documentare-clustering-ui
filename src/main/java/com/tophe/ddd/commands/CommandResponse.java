package com.tophe.ddd.commands;

import com.orange.documentare.clusteringui.TryOrCatch;
import com.tophe.ddd.infrastructure.bus.BusResponse;

public class CommandResponse<V> extends BusResponse<V, TryOrCatch<V>> {

  public CommandResponse(TryOrCatch<V> t) {
    super(t);
  }
}
