package com.tophe.ddd.commands;

import com.orange.documentare.clusteringui.TryOrCatch;
import com.tophe.ddd.infrastructure.bus.Bus;
import com.tophe.ddd.infrastructure.bus.BusResponse;

public class CommandBus extends Bus<CommandHandler, Command> {

  @Override
  protected BusResponse failed(RuntimeException e) {
    return new CommandResponse(TryOrCatch.failure(e));
  }
}

