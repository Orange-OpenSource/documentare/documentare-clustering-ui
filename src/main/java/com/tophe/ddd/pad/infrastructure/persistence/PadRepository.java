package com.tophe.ddd.pad.infrastructure.persistence;

import com.tophe.ddd.infrastructure.persistence.Repository;
import com.tophe.ddd.pad.domain.Pad;

public interface PadRepository extends Repository<Pad> {

}
