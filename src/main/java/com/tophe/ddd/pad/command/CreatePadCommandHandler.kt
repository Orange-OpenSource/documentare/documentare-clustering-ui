package com.tophe.ddd.pad.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.pad.domain.Pad
import com.tophe.ddd.pad.infrastructure.persistence.PadRepository


class CreatePadCommandHandler(private val padRepository: PadRepository, eventBus: EventBus) : CommandHandler<CreatePadCommand, Void>(eventBus) {

  override fun doExecute(command: CreatePadCommand): AggregateAndEvents<Void?, Collection<Event>> {
    val pad = Pad.createEmptyPad()
    padRepository.insert(listOf(pad))
    return AggregateAndEvents.withOnlyEvents(emptyList())
  }

}
