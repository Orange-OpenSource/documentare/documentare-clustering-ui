package com.tophe.ddd.pad.query;

import com.tophe.ddd.queries.Query;

public class GetPadQuery implements Query {

  public final Long padId;

  public GetPadQuery(Long padId) {
    this.padId = padId;
  }
}
