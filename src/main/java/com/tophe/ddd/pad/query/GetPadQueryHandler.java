package com.tophe.ddd.pad.query;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import com.tophe.ddd.pad.domain.Pad;
import com.tophe.ddd.pad.infrastructure.persistence.PadRepository;
import com.tophe.ddd.queries.QueryHandler;

public class GetPadQueryHandler extends QueryHandler<GetPadQuery, Optional<Pad>> {

  private final PadRepository padRepository;

  public GetPadQueryHandler(PadRepository padRepository) {
    this.padRepository = padRepository;
  }

  @Override
  public Optional<Pad> doExecute(GetPadQuery query) {
    AtomicReference<Optional<Pad>> padOptional = new AtomicReference<>(Optional.empty());
    padRepository.findAll().forEach(pad -> {
      if (pad.id == query.padId) {
        padOptional.set(Optional.of(pad));
      }
    });
    return padOptional.get();
  }
}
