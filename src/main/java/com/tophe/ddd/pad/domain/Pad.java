package com.tophe.ddd.pad.domain;

public class Pad {

  public final Long id;
  public final String text;

  private Pad(Long id, String text) {
    this.text = text;
    this.id = id;
  }

  public static Pad createEmptyPad() {
    return new Pad(null, "");
  }

  public Pad updateText(String text) {
    return new Pad(id, text);
  }

  public Pad updateId(Long id) {
    return new Pad(id, text);
  }
}
