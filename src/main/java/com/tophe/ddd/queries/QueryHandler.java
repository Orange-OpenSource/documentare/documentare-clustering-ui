package com.tophe.ddd.queries;

import com.orange.documentare.clusteringui.TryOrCatch;
import com.tophe.ddd.infrastructure.bus.BusHandler;

public abstract class QueryHandler<T extends Query, R> extends BusHandler<T, R, QueryResponse<R>> {

  @Override
  public QueryResponse<R> execute(T command) {
    return new QueryResponse<>(TryOrCatch.of(() -> doExecute(command)));
  }

  protected abstract R doExecute(T command);
}
