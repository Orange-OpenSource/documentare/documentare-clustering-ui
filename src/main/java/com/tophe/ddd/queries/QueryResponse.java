package com.tophe.ddd.queries;

import com.orange.documentare.clusteringui.TryOrCatch;
import com.tophe.ddd.infrastructure.bus.BusResponse;

public class QueryResponse<T> extends BusResponse<T, TryOrCatch<T>> {

  public QueryResponse(TryOrCatch<T> t) {
    super(t);
  }
}
