package com.tophe.ddd.queries;

import com.orange.documentare.clusteringui.TryOrCatch;
import com.tophe.ddd.infrastructure.bus.Bus;
import com.tophe.ddd.infrastructure.bus.BusResponse;

public class QueryBus extends Bus<QueryHandler, Query> {

  @Override
  protected BusResponse failed(RuntimeException e) {
    return new QueryResponse(TryOrCatch.failure(e));
  }
}
