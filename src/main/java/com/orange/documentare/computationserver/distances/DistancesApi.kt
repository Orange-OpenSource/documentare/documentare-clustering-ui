package com.orange.documentare.computationserver.distances

import com.orange.documentare.computationserver.ComputationServerResponse
import com.orange.documentare.computationserver.ComputationServerResponse.Companion.error
import com.orange.documentare.computationserver.ComputationServerResponse.Companion.with
import com.orange.documentare.core.computationserver.biz.RemoteTaskDTO
import com.orange.documentare.core.computationserver.biz.distances.DistancesController
import com.orange.documentare.core.computationserver.biz.distances.DistancesController.DistanceRequestState.*
import com.orange.documentare.core.computationserver.biz.distances.api.DistancesRequest

class DistancesApi(private val distancesController: DistancesController) {

  fun computeDistances(distanceRequest: DistancesRequest): ComputationServerResponse<RemoteTaskDTO> {
    val status = distancesController.launchDistancesComputation(distanceRequest)
    return when (status.state) {
      BAD_REQUEST -> error(400, status.error)
      CAN_NOT_ACCEPT_A_NEW_TASK -> error(503, status.error)
      TASK_LAUNCHED -> with(RemoteTaskDTO(status.taskId ?: ""), 200)
      else -> {
        throw IllegalStateException("${status.state.name} is not handled")
      }
    }
  }

}
