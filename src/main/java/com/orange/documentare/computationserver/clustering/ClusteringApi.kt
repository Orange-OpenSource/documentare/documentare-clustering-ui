package com.orange.documentare.computationserver.clustering

import com.orange.documentare.computationserver.ComputationServerResponse
import com.orange.documentare.computationserver.ComputationServerResponse.Companion.error
import com.orange.documentare.core.computationserver.biz.RemoteTaskDTO
import com.orange.documentare.core.computationserver.biz.clustering.ClusteringController
import com.orange.documentare.core.computationserver.biz.clustering.ClusteringController.ClusteringRequestState.*
import com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest

class ClusteringApi(private val clusteringController: ClusteringController) {

  fun computeClustering(clusteringRequest: ClusteringRequest): ComputationServerResponse<RemoteTaskDTO> {
    val requestStatus = clusteringController.launchClustering(clusteringRequest)
    return when (requestStatus.state) {
      BAD_REQUEST -> error(400, requestStatus.error)
      CAN_NOT_ACCEPT_A_NEW_TASK -> error(503, requestStatus.error)
      TASK_LAUNCHED -> ComputationServerResponse.with(RemoteTaskDTO(requestStatus.taskId ?: ""), 200)
      else -> {
        throw IllegalStateException("${requestStatus.state.name} is not handled")
      }
    }
  }
}
