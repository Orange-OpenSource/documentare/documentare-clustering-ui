package com.orange.documentare.computationserver

data class ComputationServerResponse<DTO> private constructor(
  private val dto: DTO?,
  private val callResponseCode: Int?,
  private val errorMessage: String?,
  private val throwable: Throwable?
) {
  fun isSuccessful() = dto != null
  fun hasThrownAnException() = throwable != null
  fun dto() = dto!!
  fun code(): Int = callResponseCode!!
  fun throwable(): Throwable = throwable!!
  fun errorMessage(): String = errorMessage!!

  companion object {
    // TODO: with devrait tout le temps vouloir dire 200, et on devrait utiliser error pour un message d'erreur ?
    @JvmStatic
    fun <DTO> with(dto: DTO?, code: Int): ComputationServerResponse<DTO> = ComputationServerResponse(dto, code, null, null)

    @JvmStatic
    fun <DTO> hasThrown(throwable: Throwable): ComputationServerResponse<DTO> = ComputationServerResponse(null, null, null, throwable)

    @JvmStatic
    fun <DTO> error(code: Int, errorMessage: String? = null): ComputationServerResponse<DTO> = ComputationServerResponse(null, code, errorMessage, null)
  }
}
