package com.orange.documentare.computationserver.tasks

import com.orange.documentare.computationserver.ComputationServerResponse
import com.orange.documentare.computationserver.ComputationServerResponse.Companion.error
import com.orange.documentare.computationserver.ComputationServerResponse.Companion.with
import com.orange.documentare.core.computationserver.biz.task.TaskController
import org.slf4j.LoggerFactory

class GetTaskResultApi(private val taskController: TaskController) {

  private val log = LoggerFactory.getLogger(GetTaskResultApi::class.java)

  fun retrieveTaskResult(taskId: String): ComputationServerResponse<Any?> {
    log.debug("retrieve task '$taskId'")

    val (taskState, result) = taskController.retrieveTaskResult(taskId)

    return when (taskState) {
      TaskController.TaskState.NOT_FOUND -> error(404)
      TaskController.TaskState.NOT_FINISHED -> error(204)
      TaskController.TaskState.ERROR -> {
        log.info("retrieve DONE WITH ERROR task '$taskId'")
        with(result, 207)
      }

      TaskController.TaskState.DONE -> {
        log.debug("retrieve DONE task '$taskId'")
        with(result!!, 200)
      }
    }
  }

}
