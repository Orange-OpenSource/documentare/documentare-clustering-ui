package com.orange.documentare.computationserver.tasks

import com.orange.documentare.computationserver.ComputationServerResponse
import com.orange.documentare.core.computationserver.biz.task.AvailableResultsTasksIdDTO
import com.orange.documentare.core.computationserver.biz.task.TaskController
import org.slf4j.LoggerFactory

class GetAvailableTasksIdApi(private val taskController: TaskController) {

  private val log = LoggerFactory.getLogger(GetAvailableTasksIdApi::class.java)
  fun retrieveAvailableResultsTasksId(): ComputationServerResponse<AvailableResultsTasksIdDTO> {
    log.debug("retrieve available results tasks id")

    val availableResultsTasksId = taskController.retrieveAvailableResultsTasksId()

    return ComputationServerResponse.with(availableResultsTasksId, 200)
  }

}
