package com.orange.documentare.clientui.biz.domain.classifier

data class ClassifierParameters(val clusterDistancesThresholdStandardDeviationFactor: Float) {
  fun valid(): Boolean {
    return clusterDistancesThresholdStandardDeviationFactor in 0.0F..10.0F
  }

  companion object {
    fun defaults() = ClassifierParameters(2.0F)
  }
}
