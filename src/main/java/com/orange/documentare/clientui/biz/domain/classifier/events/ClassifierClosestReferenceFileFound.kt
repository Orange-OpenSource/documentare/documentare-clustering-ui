package com.orange.documentare.clientui.biz.domain.classifier.events

import com.orange.documentare.clusteringui.biz.file.query.ClusterDistancesStats

data class ClassifierClosestReferenceFileFound(
  val fileToClassifyName: String,
  val closestReferenceFilename: String,
  val clusterDistancesStats: ClusterDistancesStats,
  val clusterDistanceThreshold: Double
) : ClassifierEvent {
  /*override fun toString(): String {
    return "🏁 RESULT  '$fileToClassifyName'  closest reference  ➡️  '$closestReferenceFilename'"
  }*/
}
