package com.orange.documentare.clientui.biz.domain.classifier

data class ClassifierResult(
  val classifiedFile: ClassifiedFile,
  val closestSortedReferenceFiles: ClassifierClosestSortedReferenceFiles,
  val classificationHierarchyPath: ClassificationHierarchyPath
)
