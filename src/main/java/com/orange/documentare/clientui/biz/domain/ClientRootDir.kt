package com.orange.documentare.clientui.biz.domain

import java.io.File
import java.io.FileOutputStream
import java.util.*
import kotlin.io.path.Path

class ClientRootDir(private val clientRootDirPath: String, private val fake: Boolean = false) {

  var clearFakeStorageCalled = false

  fun createDirectoriesIfNeeded() {
    if (fake) {
      return
    }
    doCreateDirectoriesIfNeeded()
  }

  private fun doCreateDirectoriesIfNeeded() {
    val dir = Path(clientRootDirPath, INPUT_FILES_DIR).toFile()
    if (dir.isDirectory) {
      return
    }
    if (!dir.mkdirs()) {
      throw IllegalStateException("❌ Failed to create directory " + dir.absolutePath)
    }
  }

  fun store(filename: String, bytes: ByteArray) {
    val fileOutputStream = FileOutputStream(filePath(filename, clientRootDirPath))
    fileOutputStream.write(bytes)
    fileOutputStream.close()
  }

  fun clear() {
    File(inputDirectoryPath(clientRootDirPath)).listFiles()?.forEach { it.delete() }
    clearFakeStorageCalled = true
  }

  fun files(filenames: List<String>): List<File> {
    return filenames.map { File(inputDirectoryPath(clientRootDirPath) + it) }
  }

  fun isEmpty(): Boolean =
    File(inputDirectoryPath(clientRootDirPath)).listFiles()?.isEmpty()
      ?: throw IllegalStateException("unexpected error, '${inputDirectoryPath(clientRootDirPath)}' directory should exist and be listable")

  companion object {
    const val INPUT_FILES_DIR = "input"

    // ⚠️ Shared with clustering-ui...
    const val NO_IMAGE = "/img/no_image.png"

    fun fakeForTestNoIO() = ClientRootDir("", true)

    fun imageUri(filename: String): String {
      return if (isImage(filename)) {
        webPath(filename)
      } else {
        NO_IMAGE
      }
    }

    fun isImage(filename: String): Boolean {
      val extension = getExtensionByStringHandling(filename)
      return extension.isPresent && imageExtensions.contains(extension.get().lowercase())
    }

    private fun webPath(filename: String): String = filePath(filename, "")

    private fun getExtensionByStringHandling(filename: String): Optional<String> {
      return Optional.ofNullable(filename)
        .filter { f -> f.contains(".") }
        .map { f -> f.substring(filename.lastIndexOf(".") + 1) }
    }

    fun inputDirectoryPath(clientRootDirPath: String) = clientRootDirPath + File.separator + INPUT_FILES_DIR + File.separator
    private fun filePath(filename: String, clientRootDirPath: String) = inputDirectoryPath(clientRootDirPath) + filename

    private val imageExtensions: List<String> = listOf(
      "png",  // null strings should be considered blank
      "jpg",  // null strings should be considered blank
      "jpeg",  // null strings should be considered blank
      "bmp"
    )
  }
}
