package com.orange.documentare.clientui.biz.domain.classifier.events

data class ClassifierDistancesComputed(val fileToClassifyName: String, val toReferenceFileNames: List<String>, val distances: List<Int>, val inLevel: Int) : ClassifierEvent {
  override fun toString(): String {
    return "    🔵 DISTANCES at level  '$inLevel'  FOR  '$fileToClassifyName'  TO  [$toReferenceFileNames]  ➡️ '$distances'"
  }
}
