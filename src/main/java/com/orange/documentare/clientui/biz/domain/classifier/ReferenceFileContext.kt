package com.orange.documentare.clientui.biz.domain.classifier

import com.orange.documentare.clusteringui.biz.file.query.ClusterDistancesStats
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelFile

data class ReferenceFileContext(
  val referenceFile: ClientHierarchyReferenceModelFile,
  val multiset: List<ClientHierarchyReferenceModelFile>,
  val clusterStats: ClusterDistancesStats
)
