package com.orange.documentare.clientui.biz.domain

data class FileToClassify(val filename: String, val imageUri: String? = null)
