package com.orange.documentare.clientui.biz.domain.classifier

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.util.*

data class ClassifierQueue(private val queue: LinkedList<File> = LinkedList()) {

  @Synchronized
  fun add(files: List<File>) {
    files.forEach {
      queue.add(it)
      log.info("🐞 Added file to queue: ${it.name}")
    }
  }

  @Synchronized
  fun dequeue(): File? = queue.poll()?.apply { log.info("🐞 Dequeued file from queue: $name") }

  @Synchronized
  fun isEmpty() = queue.isEmpty()

  @Synchronized
  fun clear() {
    queue.clear()
  }

  @Synchronized
  fun filesWaitingInQueue() = queue.toList()

  companion object {
    val log: Logger = LoggerFactory.getLogger(ClassifierQueue::class.java)
  }
}
