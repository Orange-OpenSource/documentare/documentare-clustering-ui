package com.orange.documentare.clientui.biz.transport

import com.orange.documentare.clientui.biz.domain.ClientUIController
import com.orange.documentare.clientui.biz.domain.classifier.events.ClassifierEvent
import com.orange.documentare.clientui.biz.domain.classifier.events.ClassifierListener
import jakarta.servlet.http.HttpServletRequest
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest

@Suppress("SpringMVCViewInspection")
@Controller
@RequestMapping("/client")
class ClientUI(private val controller: ClientUIController) {

  @Secured("ROLE_USER")
  @RequestMapping("/")
  fun clientHome(request: HttpServletRequest): String {
    val classificationRunning = controller.classificationRunning()
    val resultsAvailable = controller.resultsAvailable()
    return if (classificationRunning)
      "redirect:/client/client-classification-running"
    else if (resultsAvailable)
      "redirect:/client/client-classification-results"
    else "client-home"
  }

  @Secured("ROLE_USER")
  @RequestMapping("/client-classification-running")
  fun clientClassificationRunning(request: HttpServletRequest): String {
    val classificationRunning = controller.classificationRunning()
    val resultsAvailable = controller.resultsAvailable()
    return if (resultsAvailable)
      "redirect:/client/client-classification-results"
    else if (!classificationRunning)
      "redirect:/client/"
    else
      "client-classification-running"
  }

  @Secured("ROLE_USER")
  @RequestMapping("/client-classification-results")
  fun clientClassificationResults(model: Model): String {

    val classificationRunning = controller.classificationRunning()
    val resultsAvailable = controller.resultsAvailable()
    return if (classificationRunning)
      "redirect:/client/client-classification-running"
    else if (!resultsAvailable)
      "redirect:/client/"
    else {
      model.addAttribute("classificationResults", controller.results())
      return "client-classification-results"
    }
  }


  //
  // PARAMETERS
  //

  @Secured("ROLE_USER")
  @RequestMapping("/client-parameters")
  fun clientParameters(model: Model): String {
    model.addAttribute("parameters", controller.classifierParameters())
    return "client-parameters"
  }

  @Secured("ROLE_USER")
  @RequestMapping(value = ["/parameters/update"], method = [RequestMethod.POST])
  fun updateParameters(@ModelAttribute updateParametersDTO: UpdateParametersDTO): String {
    controller.updateClassifierParameters(updateParametersDTO.toClassifierParameters())
    return "client-home"
  }

  //
  // UPLOAD
  //

  @Secured("ROLE_USER")
  @RequestMapping(value = ["/upload"], method = [RequestMethod.POST])
  fun upload(request: MultipartHttpServletRequest): ResponseEntity<String> {
    val result = controller.upload(retrieveFilenamesAndContent(request))

    return ResponseEntity.status(result.statusCode).body(result.message)
  }


  //
  // CLEAR
  //

  @Secured("ROLE_USER")
  @RequestMapping(value = ["/clear"], method = [RequestMethod.POST])
  fun clear(): ResponseEntity<String> {
    val result = controller.clear()

    return ResponseEntity.status(result.statusCode).body(result.message)
  }


  //
  // EVENTS
  //

  @Secured("ROLE_USER")
  @RequestMapping("/events")
  fun events(): ResponseEntity<String> {
    val body = events.joinToString("\n") { it.toString() }
    return ResponseEntity.ok(body)
  }


  private fun retrieveFilenamesAndContent(request: MultipartHttpServletRequest): List<Pair<String, ByteArray>> =
    request.fileNames.asSequence().toList()
      .mapNotNull { filename: String -> request.getFile(filename) }
      .filter { file: MultipartFile -> file.originalFilename != null }
      .map { file: MultipartFile -> Pair(file.originalFilename!!, file.bytes) }


  companion object {
    private val log = LoggerFactory.getLogger(ClientUIController::class.java)

    private var events = mutableListOf<ClassifierEvent>()

    val classifierListener = object : ClassifierListener {
      override fun onEvent(event: ClassifierEvent) {
        events.add(event)
        events = events.takeLast(50).toMutableList()
        logEvent(event)
      }
    }

    private fun logEvent(event: ClassifierEvent) {
      log.info("[⚡️ CLIENT] classifier event: $event")
    }
  }
}
