package com.orange.documentare.clientui.biz.domain

data class FrontClassificationResult(val fileToClassify: FileToClassify, val bestCandidates: List<FrontBestCandidate>)
