package com.orange.documentare.clientui.biz.domain.classifier

data class ClassifierClosestSortedReferenceFiles(private val distanceReferenceFiles: List<ClassifierDistanceToReferenceFile>) {
  fun takeFirst(count: Int): ClassifierClosestSortedReferenceFiles = ClassifierClosestSortedReferenceFiles(distanceReferenceFiles.take(count))
  fun first(): ClassifierDistanceToReferenceFile = distanceReferenceFiles.first()
  fun all(): List<ClassifierDistanceToReferenceFile> = distanceReferenceFiles
}
