package com.orange.documentare.clientui.biz.domain.classifier

import com.orange.documentare.clientui.biz.domain.ClientRootDir

data class ClassifiedFile(val filename: String, val imageUri: String = ClientRootDir.NO_IMAGE)
