package com.orange.documentare.clientui.biz.transport

import com.orange.documentare.clientui.biz.domain.classifier.ClassifierParameters

data class UpdateParametersDTO(val clusterDistancesThresholdStandardDeviationFactor: Float) {
  fun toClassifierParameters() = ClassifierParameters(clusterDistancesThresholdStandardDeviationFactor)
}
