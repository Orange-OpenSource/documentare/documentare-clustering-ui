package com.orange.documentare.clientui.biz.domain.classifier.events

data class ClassifierStarted(val fileToClassifyName: String) : ClassifierEvent {
  override fun toString(): String {
    return "🟢 START classification for file '$fileToClassifyName'"
  }
}
