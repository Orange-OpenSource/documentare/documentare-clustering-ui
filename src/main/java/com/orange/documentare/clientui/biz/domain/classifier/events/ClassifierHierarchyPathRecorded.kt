package com.orange.documentare.clientui.biz.domain.classifier.events

import com.orange.documentare.clientui.biz.domain.classifier.ClassificationHierarchyPath
import com.orange.documentare.clientui.biz.domain.classifier.HierarchyLevelClosestClusterCenterMatchingInclusionCriteria

data class ClassifierHierarchyPathRecorded(val filename: String, private val classificationHierarchyPath: ClassificationHierarchyPath) : ClassifierEvent {
  override fun toString(): String {
    var log = "🏁 RECORDED hierarchy path for '$filename'\n"
    classificationHierarchyPath.path().forEach { clustersMatchingInclusionCriteria: List<HierarchyLevelClosestClusterCenterMatchingInclusionCriteria> ->
      if (clustersMatchingInclusionCriteria.isEmpty()) {
        log += "    🔵 No cluster matching inclusion criteria at this level\n"
      } else {
        log += "    🔵 Level ${clustersMatchingInclusionCriteria.first().level}"
        clustersMatchingInclusionCriteria.forEach {
          log += " - ${it.filename}" +
            " - SingletonAutoInclusion=${if (it.singletonAutoInclusion) "⚠️" else "NO"}" + "" +
            " - DistanceUnderOrEqualThreshold=${if (it.distanceUnderOrEqualThreshold) "✅" else "❌"}" +
            " - Distance=${it.distance}" + "" +
            " - Threshold=${it.clusterDistanceThreshold}" + "" +
            " - Multihypothesis=${it.clusterMultiHypothesisName}"
        }
        log += "\n"
      }
    }

    log += "    🔵 TrustWorthyNess=${classificationHierarchyPath.trustWorthiness(true)}\n"
    return log
  }
}
