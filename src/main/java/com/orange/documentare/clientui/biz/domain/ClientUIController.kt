package com.orange.documentare.clientui.biz.domain

import com.orange.documentare.clientui.biz.domain.classifier.ClassificationTrustWorthiness
import com.orange.documentare.clientui.biz.domain.classifier.ClassificationTrustWorthinessType.DOUBT
import com.orange.documentare.clientui.biz.domain.classifier.Classifier
import com.orange.documentare.clientui.biz.domain.classifier.ClassifierParameters
import com.orange.documentare.clientui.biz.domain.classifier.events.ClassifierListener
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport
import org.slf4j.LoggerFactory

class ClientUIController(
  clientHierarchyExport: ClientHierarchyExport,
  private val clientStorageDirectory: ClientRootDir,
  private val distancePrepDir: String,
  private val rawImageCacheDir: String,
  private val listener: ClassifierListener? = null,
  private val classifier: Classifier = Classifier(distancePrepDir, rawImageCacheDir, clientHierarchyExport, listener)
) {
  init {
    clientStorageDirectory.createDirectoriesIfNeeded()
  }

  fun classificationRunning() = classifier.running()
  fun resultsAvailable() = classifier.resultsAvailable()
  fun results(): List<FrontClassificationResult> = FrontClassificationResults.from(classifier.results()).results

  class ClientUploadResult private constructor(val statusCode: Int = 201, val message: String = "") {
    companion object {
      fun ok(): ClientUploadResult = ClientUploadResult()
      fun error(statusCode: Int, message: String): ClientUploadResult = ClientUploadResult(statusCode, message)
    }
  }

  fun upload(listOfFilenameAndBytes: List<Pair<String, ByteArray>>): ClientUploadResult {
    if (listOfFilenameAndBytes.isEmpty()) {
      return ClientUploadResult.error(400, "no files provided")
    }

    listOfFilenameAndBytes.forEach {
      clientStorageDirectory.store(it.first, it.second)
      logUploadedFile(it.first, it.second.size)
    }

    classifier.runAsync(clientStorageDirectory.files(listOfFilenameAndBytes.map { it.first }))

    return ClientUploadResult.ok()
  }

  fun clear(): ClientUploadResult {
    clientStorageDirectory.clear()
    classifier.clear()
    return ClientUploadResult.ok()
  }

  private fun logUploadedFile(filename: String, fileSize: Int) {
    log.info("[⚡️ CLIENT] web controller:  ⬆️  file '$filename' uploaded ('$fileSize' bytes)")
  }

  fun classifierParameters() = classifier.classifierParameters()
  fun updateClassifierParameters(classifierParameters: ClassifierParameters) = classifier.updateClassifierParameters(classifierParameters)

  companion object {
    fun withFakeResultsForTestPurpose() =
      ClientUIController(ClientHierarchyExport.fakeForTestClientHierarchyExport(), ClientRootDir.fakeForTestNoIO(), "", "", null, Classifier.withFakeResultsForTestPurpose())

    private val log = LoggerFactory.getLogger(ClientUIController::class.java)
  }

  object Examples {

    val CONTROLLER_FILES_TO_UPLOAD_EXAMPLE = Classifier.Examples.CLASSIFIER_FILES_TO_CLASSIFY.map {
      Pair(it.name, it.name.toByteArray())
    }

    val CONTROLLER_NO_FILES_TO_UPLOAD_EXAMPLE = emptyList<Pair<String, ByteArray>>()

    val CONTROLLER_RESULTS = listOf(
      FrontClassificationResult(
        FileToClassify("titi", ClientRootDir.NO_IMAGE),
        listOf(
          FrontBestCandidate("f0", "class A 100%", ClassificationTrustWorthiness(DOUBT, 2), ClientRootDir.NO_IMAGE, ClientRootDir.NO_IMAGE),
          FrontBestCandidate("f2", "class B 100%", null, ClientRootDir.NO_IMAGE, ClientRootDir.NO_IMAGE),
          FrontBestCandidate("f4", "class C 100%", null, ClientRootDir.NO_IMAGE, ClientRootDir.NO_IMAGE)
        )
      ),
      FrontClassificationResult(
        FileToClassify("alf", ClientRootDir.NO_IMAGE),
        listOf(
          FrontBestCandidate("f0", "class A 100%", ClassificationTrustWorthiness(DOUBT, 2), ClientRootDir.NO_IMAGE, ClientRootDir.NO_IMAGE),
          FrontBestCandidate("f2", "class B 100%", null, ClientRootDir.NO_IMAGE, ClientRootDir.NO_IMAGE),
          FrontBestCandidate("f4", "class C 100%", null, ClientRootDir.NO_IMAGE, ClientRootDir.NO_IMAGE)
        )
      )
    )
  }
}
