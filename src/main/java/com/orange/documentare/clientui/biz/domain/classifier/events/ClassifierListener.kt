package com.orange.documentare.clientui.biz.domain.classifier.events

interface ClassifierListener {
  fun onEvent(event: ClassifierEvent)
}
