package com.orange.documentare.clientui.biz.domain.classifier

import com.orange.documentare.clientui.biz.domain.classifier.ClassificationTrustWorthinessType.*
import com.orange.documentare.clusteringui.biz.file.query.ClusterUIView
import org.slf4j.Logger
import org.slf4j.LoggerFactory

data class HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
  val filename: String,
  val singletonAutoInclusion: Boolean,
  val distanceUnderOrEqualThreshold: Boolean,
  val clusterMultiHypothesisName: String?,
  val level: Int,
  val distance: Int,
  val clusterDistanceThreshold: Double
)

data class ClassificationTrustWorthiness(val trustWorthinessType: ClassificationTrustWorthinessType, val bonusMalusScore: Int) {
  fun description(): String = "${trustWorthinessType.name} $bonusMalusScore"
}

enum class ClassificationTrustWorthinessType {
  RELIABLE,
  DOUBT,
  UNRELIABLE
}

data class ClassificationHierarchyPath(private val path: MutableList<List<HierarchyLevelClosestClusterCenterMatchingInclusionCriteria>> = mutableListOf()) {

  fun add(closestReferenceFilesMatchingInclusionCriteria: List<ClassifierDistanceToReferenceFile>, inLevel: Int) {
    val clustersNotMathingInclusionCriteria =
      closestReferenceFilesMatchingInclusionCriteria.isNotEmpty() && closestReferenceFilesMatchingInclusionCriteria.none { it.distanceUnderOrEqualThreshold() || it.singletonAutoInclusion() }
    if (clustersNotMathingInclusionCriteria) {
      throw IllegalStateException("cluster not matching inclusion criteria should not be added to hierarchy path")
    }

    path.add(closestReferenceFilesMatchingInclusionCriteria.map {
      HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
        it.referenceFile.filename,
        it.singletonAutoInclusion() && !it.distanceUnderOrEqualThreshold(),
        it.distanceUnderOrEqualThreshold(),
        it.referenceFile.clusterMultiHypothesisName,
        inLevel,
        it.distance,
        it.clusterDistanceThreshold
      )
    })
  }

  fun path(): List<List<HierarchyLevelClosestClusterCenterMatchingInclusionCriteria>> = path.toList()

  private fun trustWorthiness(
    level: Int,
    hierarchyLevelClosestClusterCentersMatchingInclusionCriteria: List<HierarchyLevelClosestClusterCenterMatchingInclusionCriteria>,
    logDecision: Boolean
  ): ClassificationTrustWorthinessType {
    if (hierarchyLevelClosestClusterCentersMatchingInclusionCriteria.isEmpty()) {
      if (logDecision) {
        log.warn("[⚠️ level $level] UNRELIABLE since no cluster matching inclusion criteria")
      }
      return UNRELIABLE
    }
    if (hierarchyLevelClosestClusterCentersMatchingInclusionCriteria.size > 1) {
      if (logDecision) {
        log.warn("[⚠️ level $level] DOUBT since more than one cluster matching inclusion criteria: ${hierarchyLevelClosestClusterCentersMatchingInclusionCriteria.map { it.filename }}")
      }
      return DOUBT
    }
    val closest = hierarchyLevelClosestClusterCentersMatchingInclusionCriteria.first()
    return if (closest.distanceUnderOrEqualThreshold) {
      if (isALeafMultihypothesisCluster(closest)) {
        if (logDecision) {
          log.warn("[⚠️ level $level] DOUBT since closest cluster (${closest.filename}) is a leaf multihypothesis cluster: ${closest.clusterMultiHypothesisName}")
        }
        DOUBT
      } else {
        // TODO CLIENT if distance > 1 => UNRELIABLE
        RELIABLE
      }
    } else if (closest.singletonAutoInclusion) {
      if (logDecision) {
        log.warn("[⚠️ level $level] DOUBT since closest cluster (${closest.filename}) is a singleton")
      }
      // TODO CLIENT if distance > 1 => UNRELIABLE
      DOUBT
    } else {
      UNRELIABLE
    }
  }

  private fun isALeafMultihypothesisCluster(
    cluster: HierarchyLevelClosestClusterCenterMatchingInclusionCriteria,
    // TODO CLIENT +++++ level == 0 needs to be tested with multihierarchy test
  ) = cluster.level == 0 && cluster.clusterMultiHypothesisName != null && cluster.clusterMultiHypothesisName.contains(ClusterUIView.MULTIHYPOTHESIS_CLASS_SEPARATOR)

  fun trustWorthiness(logDecision: Boolean = false): ClassificationTrustWorthiness {
    return computePathTrustWorthiness(path.mapIndexed { index, it -> trustWorthiness(path.size - 1 - index, it, logDecision) })
  }

  companion object {
    val log: Logger = LoggerFactory.getLogger(ClassificationHierarchyPath::class.java)

    fun computePathTrustWorthiness(pathTrustWorthinesses: List<ClassificationTrustWorthinessType>): ClassificationTrustWorthiness {
      val levelsCount = pathTrustWorthinesses.size
      val bonusMalusScore = pathTrustWorthinesses.mapIndexed { levelIndex, worthinessType ->
        val score: Int = when (worthinessType) {
          RELIABLE -> 2 * (levelIndex + 1) // +1 because levelIndex starts from 0
          DOUBT -> 0
          UNRELIABLE -> -1 * (levelsCount - levelIndex)
        }
        score
      }.sum()

      val trustWorthinessOfLeaf = pathTrustWorthinesses.last()
      if (trustWorthinessOfLeaf == RELIABLE) {
        return ClassificationTrustWorthiness(RELIABLE, bonusMalusScore)
      } else if (trustWorthinessOfLeaf == UNRELIABLE) {
        return ClassificationTrustWorthiness(UNRELIABLE, bonusMalusScore)
      }

      if (bonusMalusScore < 0) {
        return ClassificationTrustWorthiness(UNRELIABLE, bonusMalusScore)
      }

      return ClassificationTrustWorthiness(DOUBT, bonusMalusScore)
    }
  }
}

