package com.orange.documentare.clientui.biz.domain.classifier

import com.orange.documentare.clientui.biz.domain.ClientRootDir
import com.orange.documentare.clientui.biz.domain.ClientRootDir.Companion.NO_IMAGE
import com.orange.documentare.clientui.biz.domain.classifier.events.*
import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples
import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples.clientExportRefFileLevel0F0NotSingleton
import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples.clientExportRefFileLevel0F2NotSingleton
import com.orange.documentare.clusteringui.biz.file.query.hierarchy.HierarchyExamples.clientExportRefFileLevel0F4NotSingleton
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.AggregateId
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport.Companion.fakeForTestClientHierarchyExport
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModel
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelFile
import com.orange.documentare.core.comp.distance.Distance
import com.orange.documentare.core.comp.distance.bytesdistances.BytesData
import com.orange.documentare.core.computationserver.biz.SharedDirectory
import com.orange.documentare.core.computationserver.biz.distances.DistancesController
import com.orange.documentare.core.computationserver.biz.distances.DistancesRequestResultDTO
import com.orange.documentare.core.computationserver.biz.distances.api.DistancesRequest
import com.orange.documentare.core.computationserver.biz.prepdata.AppPrepData
import com.orange.documentare.core.computationserver.biz.task.Task
import com.orange.documentare.core.computationserver.biz.task.Tasks
import org.slf4j.LoggerFactory
import java.io.File
import kotlin.concurrent.thread

typealias FindClosestReferenceFilesSignature = (
  fileToClassify: File,
  inReferenceFilesAggregateId: List<AggregateId>,
  hierarchyReferenceModel: ClientHierarchyReferenceModel,
  inLevel: Int,
  classificationHierarchyPath: ClassificationHierarchyPath
)
-> ClassifierClosestSortedReferenceFiles

data class Classifier(
  private val distancePrepDir: String,
  private val rawImageCacheDir: String,
  private val clientHierarchyExport: ClientHierarchyExport,
  private val listener: ClassifierListener? = null
) {
  private var classifierParameters = ClassifierParameters.defaults()

  private var classifierThread: Thread? = null
  private val appPrepData = AppPrepData(distancePrepDir, rawImageCacheDir, buildSharedDirectory())
  private val tasks = Tasks()
  private val distancesController = DistancesController(tasks, appPrepData)

  private var hierarchyReferenceModel: ClientHierarchyReferenceModel? = null

  private var computeDistance: (File, List<ReferenceFileContext>) -> DistancesRequestResultDTO = ::prodComputeDistances
  private val results = mutableListOf<ClassifierResult>()

  private var running = false
  private var resultsAvailable = false

  private val queue = ClassifierQueue()

  fun classifierParameters(): ClassifierParameters {
    return classifierParameters
  }
  fun updateClassifierParameters(parameters: ClassifierParameters) {
    if (parameters.valid()) {
      log.info("🔵  client classifier parameters updated: $parameters")
      classifierParameters = parameters
    } else {
      val errorMessage = "🔴 invalid classifier parameters: $parameters"
      log.error(errorMessage)
      throw IllegalStateException(errorMessage)
    }
  }

  fun results(): List<ClassifierResult> {
    return results
  }

  fun testPurposeSetComputeDistance(lambda: (fileToClassify: File, referenceFilesContext: List<ReferenceFileContext>) -> DistancesRequestResultDTO) {
    computeDistance = lambda
  }

  fun testPurposeWaitComputationFinished() {
    var maxTimes = 0
    while (running() && maxTimes < 500) {
      Thread.sleep(10)
      maxTimes++
    }
  }

  fun running(): Boolean {
    checkCoherentState()
    return running
  }

  fun resultsAvailable(): Boolean {
    checkCoherentState()
    return resultsAvailable
  }

  private fun checkCoherentState() {
    if (running && resultsAvailable) {
      val errorMessage = "🔴  both running with available results..."
      log.error(errorMessage)
      throw IllegalStateException(errorMessage)
    }
    if (!running && !queue.isEmpty()) {
      val errorMessage = "🔴  NOT running but queue is NOT empty..."
      log.error(errorMessage)
      throw IllegalStateException(errorMessage)
    }
  }

  fun runAsync(filesToClassify: List<File>) {
    if (!queueIsEmpty()) {
      queue.add(filesToClassify)
      return
    }

    clear(false)
    running = true

    // if we update the hierarchy reference model, we need to force a refresh in client to avoid a restart
    forceHierarchyReferenceModelRefresh()

    // not before clear which would clear the queue...
    // not in the thread since it may fail unit test
    queue.add(filesToClassify)

    classifierThread = thread {
      try {
        log.info("⚙️ Classifier parameters: ${hierarchyReferenceModel().distanceParameters}")
        doRun(queue)
      } catch (e: Exception) {
        log.error("\n❌ classifier crashed, Jo will be hungry 😱\n", e)
        clear()
      }
    }
  }

  private fun forceHierarchyReferenceModelRefresh() {
    hierarchyReferenceModel = null
  }

  private fun doRun(queue: ClassifierQueue) {
    var fileToClassify = queue.dequeue()

    while (fileToClassify != null) {
      doRunOn(fileToClassify, hierarchyReferenceModel())
      fileToClassify = queue.dequeue()
    }

    running = false
    resultsAvailable = true
  }

  private fun doRunOn(file: File, hierarchyReferenceModel: ClientHierarchyReferenceModel) {
    val filename = file.name
    listener?.onEvent(ClassifierStarted(filename))
    val classificationHierarchyPath = ClassificationHierarchyPath()
    val closestReferenceFiles =
      findClosestReferenceFiles(file, emptyList(), hierarchyReferenceModel, hierarchyReferenceModel.hierarchyLevels.size - 1, classificationHierarchyPath)
    val closestReferenceFile = closestReferenceFiles.first()
    listener?.onEvent(
      ClassifierClosestReferenceFileFound(
        filename,
        closestReferenceFile.referenceFile.filename,
        closestReferenceFile.referenceFile.clusterDistancesStats!!,
        closestReferenceFile.clusterDistanceThreshold
      )
    )
    listener?.onEvent(ClassifierHierarchyPathRecorded(filename, classificationHierarchyPath))
    results.add(ClassifierResult(ClassifiedFile(filename, ClientRootDir.imageUri(filename)), closestReferenceFiles, classificationHierarchyPath))
  }

  fun findClosestReferenceFiles(
    fileToClassify: File,
    inReferenceFilesAggregateId: List<AggregateId>,
    hierarchyReferenceModel: ClientHierarchyReferenceModel,
    inLevel: Int,
    classificationHierarchyPath: ClassificationHierarchyPath,
    // for test purpose: to capture the next call arguments and stop the recursion
    testCaptureNextCall: (FindClosestReferenceFilesSignature)? = null
  ): ClassifierClosestSortedReferenceFiles {
    val next = testCaptureNextCall ?: prodFindClosestReferenceFiles()
    val allLevelRefFiles: List<List<ClientHierarchyReferenceModelFile>> = hierarchyReferenceModel.hierarchyLevels[inLevel].referenceFiles
    val inLevelRefFiles = currentLevelReferenceFilesToCompareTo(inReferenceFilesAggregateId, allLevelRefFiles, inLevel)

    // Level 0
    // - only cluster centers + multiset files are present in this level (thanks to client export behaviour)
    // - we keep only first BEST_CANDIDATES_MAX_COUNT best candidates, no need to go further
    if (inLevel == 0) {
      val closestSortedReferenceFiles = closestSortedReferenceFiles(fileToClassify, inLevelRefFiles, inLevel)
      val (clustersMatchingInclusionCriteria, _) = findClustersMatchingInclusionCriteria(closestSortedReferenceFiles, classificationHierarchyPath, inLevel)
      return clustersMatchingInclusionCriteria.takeFirst(BEST_CANDIDATES_MAX_COUNT)
    }

    //
    // Root/Intermediate level
    //
    val closestClustersNonSingletons = closestSortedReferenceFiles(fileToClassify, inLevelRefFiles.filter { !it.clusterStats.singleton }, inLevel).all()
    val closestClustersPlusSingletons = closestClustersPlusSingletons(inLevelRefFiles.filter { it.clusterStats.singleton }, closestClustersNonSingletons)

    val (clustersMatchingInclusionCriteria, stop) = findClustersMatchingInclusionCriteria(closestClustersPlusSingletons, classificationHierarchyPath, inLevel)
    if (stop) {
      return clustersMatchingInclusionCriteria.takeFirst(BEST_CANDIDATES_MAX_COUNT)
    }

    return next.invoke(
      fileToClassify,
      retrieveMatchingClustersId(allLevelRefFiles, clustersMatchingInclusionCriteria),
      hierarchyReferenceModel,
      inLevel - 1,
      classificationHierarchyPath
    )
  }

  private fun retrieveMatchingClustersId(
    levelReferenceFiles: List<List<ClientHierarchyReferenceModelFile>>,
    clustersMatchingInclusionCriteria: ClassifierClosestSortedReferenceFiles
  ): List<AggregateId> {
    val closestClustersCenterOrMultisetId: List<String> = clustersMatchingInclusionCriteria.all().map { it.referenceFile.aggregateId }

    val closestClusters: List<List<ClientHierarchyReferenceModelFile>> =
      levelReferenceFiles.filter { cluster -> cluster.any { closestClustersCenterOrMultisetId.contains(it.aggregateId) } }
    val closestClustersRefFiles: List<AggregateId> = closestClusters.flatten().map { it.aggregateId }
    return closestClustersRefFiles
  }

  private fun closestClustersPlusSingletons(
    singletonsInLevelReferenceFiles: List<ReferenceFileContext>,
    closestNonSingletons: List<ClassifierDistanceToReferenceFile>
  ): ClassifierClosestSortedReferenceFiles {
    val singletons = singletonsInLevelReferenceFiles.map {
      ClassifierDistanceToReferenceFile(it.referenceFile, it.clusterStats, Int.MAX_VALUE, Double.NaN)
    }
    val closestClustersPlusSingletons = mutableListOf<ClassifierDistanceToReferenceFile>()
    closestClustersPlusSingletons.addAll(closestNonSingletons)
    closestClustersPlusSingletons.addAll(singletons)
    return ClassifierClosestSortedReferenceFiles(closestClustersPlusSingletons)
  }

  private fun prodFindClosestReferenceFiles(): FindClosestReferenceFilesSignature = { fileToClassify: File,
                                                                                      inReferenceFilesAggregateId: List<AggregateId>,
                                                                                      hierarchyReferenceModel: ClientHierarchyReferenceModel,
                                                                                      inLevel: Int,
                                                                                      classificationHierarchyPath: ClassificationHierarchyPath
    ->
    findClosestReferenceFiles(fileToClassify, inReferenceFilesAggregateId, hierarchyReferenceModel, inLevel, classificationHierarchyPath)
  }

  private fun closestSortedReferenceFiles(fileToClassify: File, referenceFilesContext: List<ReferenceFileContext>, inLevel: Int): ClassifierClosestSortedReferenceFiles {
    val distances: IntArray = doComputeDistance(fileToClassify, referenceFilesContext, inLevel).distances
    val distancesReferenceFiles = mutableListOf<ClassifierDistanceToReferenceFile>()
    val thresholdStandardDeviationFactor = classifierParameters.clusterDistancesThresholdStandardDeviationFactor

    log.info("🐞  thresholdStandardDeviationFactor: $thresholdStandardDeviationFactor")

    referenceFilesContext.forEachIndexed { index, referenceFileContext ->
      val distance = distances[index]
      val clusterDistanceThreshold = referenceFileContext.clusterStats.threshold(thresholdStandardDeviationFactor)
      distancesReferenceFiles.add(ClassifierDistanceToReferenceFile(referenceFileContext.referenceFile, referenceFileContext.clusterStats, distance, clusterDistanceThreshold))
    }
    return ClassifierClosestSortedReferenceFiles(distancesReferenceFiles.sortedBy { it.distance })
  }

  private fun doComputeDistance(fileToClassify: File, referenceFilesContext: List<ReferenceFileContext>, inLevel: Int): DistancesRequestResultDTO {
    val requestResultDTO = computeDistance(fileToClassify, referenceFilesContext)
    listener?.onEvent(ClassifierDistancesComputed(fileToClassify.name, referenceFilesContext.map { it.referenceFile.filename }, requestResultDTO.distances.toList(), inLevel))
    return requestResultDTO
  }

  fun prodComputeDistances(fileToClassify: File, referenceFilesContext: List<ReferenceFileContext>): DistancesRequestResultDTO {
    val element = arrayOf(BytesData(listOf(fileToClassify.name), listOf(fileToClassify.absolutePath)))
    val compareTo = referenceFilesContext.map { mapReferenceFileContextToBytesData(it) }.toTypedArray()

    val clientParameters = hierarchyReferenceModel().distanceParameters
    val distancesRequest = DistancesRequest.builder()
      .element(element)
      .compareTo(compareTo)
      .suffixArrayAlgorithm(clientParameters.suffixArrayAlgorithm).rawConverter(clientParameters.rawConverterEnabled).pCount(clientParameters.rawConverterPixelCount).build()

    val status = distancesController.launchDistancesComputation(distancesRequest)

    // TODO CLIENT error cases...
    val result = waitForResult(status.taskId!!)
    return result.result.get() as DistancesRequestResultDTO
  }

  private fun hierarchyReferenceModel(): ClientHierarchyReferenceModel {
    if (hierarchyReferenceModel == null) {
      hierarchyReferenceModel = clientHierarchyExport.exportClientHierarchy()
    }
    return hierarchyReferenceModel!!
  }

  private fun mapReferenceFileContextToBytesData(referenceFileContext: ReferenceFileContext): BytesData {
    val multisetFilesName = referenceFileContext.multiset.map { it.filename }
    val multisetFilesPath = referenceFileContext.multiset.map { it.absolutePath }
    return BytesData(multisetFilesName, multisetFilesPath)
  }

  private fun waitForResult(taskId: String): Task {
    do {
      Thread.sleep(10)
    } while (!tasks.isDone(taskId))
    return tasks.pop(taskId)
  }

  fun clear(notifyListener: Boolean = true) {
    classifierThread?.interrupt()
    classifierThread = null
    running = false
    resultsAvailable = false
    results.clear()
    queue.clear()
    if (notifyListener) {
      listener?.onEvent(ClassifierCleared())
    }
  }

  fun queueIsEmpty(): Boolean {
    checkCoherentState()
    return queue.isEmpty()
  }

  fun filesWaitingInQueue() = queue.filesWaitingInQueue()

  companion object {
    private const val BEST_CANDIDATES_MAX_COUNT = 3

    private val log = LoggerFactory.getLogger(Classifier::class.java)

    fun currentLevelReferenceFilesToCompareTo(
      inReferenceFilesAggregateId: List<AggregateId>, levelReferenceFiles: List<List<ClientHierarchyReferenceModelFile>>, inLevel: Int
    ): List<ReferenceFileContext> {
      val inLevelReferenceFiles = mutableListOf<ReferenceFileContext>()
      return if (inReferenceFilesAggregateId.isEmpty()) {
        levelReferenceFiles.forEach { cluster ->
          val clusterCenter = cluster.first { it.clusterCenter }
          val multisetFiles = cluster.filter { it.multiset || it.clusterCenter }
          inLevelReferenceFiles.add(ReferenceFileContext(clusterCenter, multisetFiles, clusterCenter.clusterDistancesStats!!))
        }
        inLevelReferenceFiles
      } else {
        levelReferenceFiles.forEach { cluster ->
          val clusterCenter = cluster.first { it.clusterCenter }
          val multisetFiles = cluster.filter { it.multiset || it.clusterCenter }
          val matchingMultisetFiles = multisetFiles.filter { inReferenceFilesAggregateId.contains(it.aggregateId) }
          if (inReferenceFilesAggregateId.contains(clusterCenter.aggregateId)) {
            inLevelReferenceFiles.add(ReferenceFileContext(clusterCenter, multisetFiles, clusterCenter.clusterDistancesStats!!))
            val matchingMultisetFilesNotClusterCenter = matchingMultisetFiles.filter { !it.clusterCenter }
            if (matchingMultisetFilesNotClusterCenter.isNotEmpty()) {
              log.info("[level $inLevel] 🗑️ skip multiset files '${matchingMultisetFilesNotClusterCenter.map { it.filename }}' since cluster center '${clusterCenter.filename}' is 'inReferenceFiles'")
            }
          } else if (matchingMultisetFiles.isNotEmpty()) {
            inLevelReferenceFiles.add(ReferenceFileContext(clusterCenter, multisetFiles, clusterCenter.clusterDistancesStats!!))
            log.info("[level $inLevel] 🔵 matching multiset files '${matchingMultisetFiles.map { it.filename }}' replaced by cluster center '${clusterCenter.filename}'")
          }
        }
        inLevelReferenceFiles
      }
    }

    fun findClustersMatchingInclusionCriteria(
      closestSortedReferenceFiles: ClassifierClosestSortedReferenceFiles,
      classificationHierarchyPath: ClassificationHierarchyPath,
      inLevel: Int
    ): Pair<ClassifierClosestSortedReferenceFiles, Boolean> {
      val closestReferenceFilesData = closestSortedReferenceFiles.all().map { ClosestReferenceFileData.from(it) }

      // input/output types are simpler for test purpose; mapping done before/after
      val (stop, closestReferenceFilesIdMatchingInclusionCriteria) =
        doFindClustersMatchingInclusionCriteria(closestReferenceFilesData, "level $inLevel")

      val closestReferenceFilesMatchingInclusionCriteria = closestSortedReferenceFiles.all()
        .filter { closestReferenceFilesIdMatchingInclusionCriteria.contains(it.referenceFile.aggregateId) }

      classificationHierarchyPath.add(
        closestReferenceFilesMatchingInclusionCriteria
          // TODO check doFindClustersMatchingInclusionCriteria, we should remove this line after new evolutions...
          .filter { it.distanceUnderOrEqualThreshold() || it.singletonAutoInclusion() },
        inLevel
      )
      return Pair(ClassifierClosestSortedReferenceFiles(closestReferenceFilesMatchingInclusionCriteria), stop)
    }

    fun doFindClustersMatchingInclusionCriteria(
      closestReferenceFilesData: List<ClosestReferenceFileData>,
      describeLevel: String
    ): Pair<Boolean, MutableList<AggregateId>> {
      val closestReferenceFilesIdMatchingInclusionCriteria = mutableListOf<AggregateId>()
      for (refFileData in closestReferenceFilesData) {
        val (aggregateId, distanceUnderOrEqualThreshold, singletonAutoInclusion, filename, distance, clusterDistanceThreshold) = refFileData
        val distanceUnderOne = distance < Distance.DISTANCE_INT_CONV_FACTOR
        if (singletonAutoInclusion || (distanceUnderOrEqualThreshold && distanceUnderOne)) {
          closestReferenceFilesIdMatchingInclusionCriteria.add(aggregateId)
        } else {
          val reason = if (distanceUnderOne) "threshold $clusterDistanceThreshold" else "1 (${Distance.DISTANCE_INT_CONV_FACTOR})"
          if (closestReferenceFilesIdMatchingInclusionCriteria.isEmpty()) {
            log.info("⚠️ [$describeLevel]️ skip nearest neighbor '$filename' since distance $distance > $reason")
          } else {
            log.info("⚠️ [$describeLevel]️ skip '$filename' since distance $distance > $reason")
          }
        }
      }

      if (closestReferenceFilesIdMatchingInclusionCriteria.isEmpty()) {
        // provide the closest cluster to display something
        val closest = closestReferenceFilesData.first()
        closestReferenceFilesIdMatchingInclusionCriteria.add(closest.aggregateId)
        log.warn("⚠️ [$describeLevel] no clusters matching inclusion criteria, we STOP at once (retain '${closest.filename}' for display purpose)")
        return Pair(true, closestReferenceFilesIdMatchingInclusionCriteria)
      }

      log.info(
        "💡 [$describeLevel] clusters retained for lower level:\n${
          closestReferenceFilesData
            .filter { closestReferenceFilesIdMatchingInclusionCriteria.contains(it.aggregateId) }
            .joinToString(separator = "\n") { if (it.singletonAutoInclusion) "\t🔵 ${it.filename} (singleton)" else "\t✅ ${it.filename} (${it.distance} < ${it.clusterDistanceThreshold})" }
        }\n"
      )
      return Pair(false, closestReferenceFilesIdMatchingInclusionCriteria)
    }

    fun fakingAlreadyRunningForTestPurpose(): Classifier {
      return Classifier("/tmp", "/tmp", fakeForTestClientHierarchyExport()).apply { running = true }
    }

    fun withFakeResultsForTestPurpose(): Classifier {
      return Classifier("/tmp", "/tmp", fakeForTestClientHierarchyExport()).apply {
        resultsAvailable = true
        results.clear()
        results.addAll(Examples.classifierResults().toMutableList())
      }
    }

    private fun buildSharedDirectory() = object : SharedDirectory {
      override fun sharedDirectoryAvailable() = false

      override fun sharedDirectoryRootPath(): String? {
        TODO("Not yet implemented")
      }
    }

    data class ClosestReferenceFileData(
      val aggregateId: String,
      val distanceUnderOrEqualThreshold: Boolean,
      val singletonAutoInclusion: Boolean,
      val filename: String,
      val distance: Int,
      val clusterDistanceThreshold: Double
    ) {
      companion object {
        fun from(refFile: ClassifierDistanceToReferenceFile): ClosestReferenceFileData {
          return ClosestReferenceFileData(
            refFile.referenceFile.aggregateId,
            refFile.distanceUnderOrEqualThreshold(),
            refFile.singletonAutoInclusion(),
            refFile.referenceFile.filename,
            refFile.distance,
            refFile.clusterDistanceThreshold
          )
        }

        fun testExampleDistanceUnderClusterThreshold(aggregateId: String): ClosestReferenceFileData {
          return ClosestReferenceFileData(
            aggregateId = aggregateId,
            distanceUnderOrEqualThreshold = true,
            singletonAutoInclusion = false,
            filename = "filename_$aggregateId",
            distance = 90,
            clusterDistanceThreshold = 100.0
          )
        }

        fun testExampleDistanceUnderClusterThresholdButAbove1(aggregateId: String): ClosestReferenceFileData {
          return ClosestReferenceFileData(
            aggregateId = aggregateId,
            distanceUnderOrEqualThreshold = true,
            singletonAutoInclusion = false,
            filename = "filename_$aggregateId",
            distance = Distance.DISTANCE_INT_CONV_FACTOR + 1,
            clusterDistanceThreshold = Distance.DISTANCE_INT_CONV_FACTOR + 10.0
          )
        }

        fun testExampleNotMatchingAnyCriteria(aggregateId: String): ClosestReferenceFileData {
          return ClosestReferenceFileData(
            aggregateId = aggregateId,
            distanceUnderOrEqualThreshold = false,
            singletonAutoInclusion = false,
            filename = "filename_$aggregateId",
            distance = 110,
            clusterDistanceThreshold = 100.0
          )
        }

        fun testExampleSingletonAutoInclusion(aggregateId: String): ClosestReferenceFileData {
          return ClosestReferenceFileData(
            aggregateId = aggregateId,
            distanceUnderOrEqualThreshold = false,
            singletonAutoInclusion = true,
            filename = "filename_$aggregateId",
            distance = 110,
            clusterDistanceThreshold = 100.0
          )
        }
      }
    }
  }

  object Examples {
    val CLASSIFIER_FILES_TO_CLASSIFY = listOf(File("titi"), File("alf"))

    private val recordedPath = mutableListOf(
      listOf(
        HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
          "f0",
          singletonAutoInclusion = false,
          distanceUnderOrEqualThreshold = true,

          "class C 50% / class A 50%",
          level = 2,
          100,
          11115.0
        )
      ),
      listOf(
        HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
          "f0",
          singletonAutoInclusion = false,
          distanceUnderOrEqualThreshold = true,
          "class D 25% / class C 25% / class B 25% / class A 25%",
          level = 1,
          100,
          1857.0481111399554
        ), HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
          "f4",
          singletonAutoInclusion = true,
          distanceUnderOrEqualThreshold = false,
          "class C 100%",
          level = 1,
          101,
          0.0
        )
      ),
      listOf(
        HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
          "f0",
          singletonAutoInclusion = false,
          distanceUnderOrEqualThreshold = true,
          "class A 100%",
          level = 0,
          100,
          112.0
        ),
        HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
          "f2",
          singletonAutoInclusion = false,
          distanceUnderOrEqualThreshold = true,
          "class B 100%",
          level = 0,
          101,
          225.0
        ),
        HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
          "f4",
          singletonAutoInclusion = false,
          distanceUnderOrEqualThreshold = true,
          "class C 100%",
          level = 0,
          102,
          438.0
        ),
        HierarchyLevelClosestClusterCenterMatchingInclusionCriteria(
          "f7",
          singletonAutoInclusion = true,
          distanceUnderOrEqualThreshold = false,
          "class D 100%",
          level = 0,
          103,
          0.0
        )
      )
    )

    fun classifierResults(): List<ClassifierResult> {
      val clientExportRefFileLevel0F0 = clientExportRefFileLevel0F0NotSingleton()
      val clientExportRefFileLevel0F2 = clientExportRefFileLevel0F2NotSingleton()
      val clientExportRefFileLevel0F4 = clientExportRefFileLevel0F4NotSingleton()
      return listOf(
        ClassifierResult(
          ClassifiedFile("titi", NO_IMAGE), ClassifierClosestSortedReferenceFiles(
            listOf(
              ClassifierDistanceToReferenceFile(
                clientExportRefFileLevel0F0, clientExportRefFileLevel0F0.clusterDistancesStats!!, 100, 112.0
              ), ClassifierDistanceToReferenceFile(
                clientExportRefFileLevel0F2, clientExportRefFileLevel0F2.clusterDistancesStats!!, 101, 225.0
              ), ClassifierDistanceToReferenceFile(
                clientExportRefFileLevel0F4, clientExportRefFileLevel0F4.clusterDistancesStats!!, 102, 438.0
              )
            )
          ), ClassificationHierarchyPath(
            recordedPath
          )
        ), ClassifierResult(
          ClassifiedFile("alf", NO_IMAGE), ClassifierClosestSortedReferenceFiles(
            listOf(
              ClassifierDistanceToReferenceFile(
                clientExportRefFileLevel0F0, clientExportRefFileLevel0F0.clusterDistancesStats, 100, 112.0
              ), ClassifierDistanceToReferenceFile(
                clientExportRefFileLevel0F2, clientExportRefFileLevel0F2.clusterDistancesStats, 101, 225.0
              ), ClassifierDistanceToReferenceFile(
                clientExportRefFileLevel0F4, clientExportRefFileLevel0F4.clusterDistancesStats, 102, 438.0
              )
            )
          ), ClassificationHierarchyPath(
            recordedPath
          )
        )
      )
    }

    val CLASSIFIER_EVENTS = listOf(
      ClassifierStarted("titi"),
      ClassifierDistancesComputed("titi", listOf("f0"), listOf(100), 2),
      ClassifierDistancesComputed("titi", listOf("f0", "f4"), listOf(100, 101), 1),
      ClassifierDistancesComputed("titi", listOf("f0", "f2", "f4", "f7"), listOf(100, 101, 102, 103), 0),
      ClassifierClosestReferenceFileFound("titi", "f0", HierarchyExamples.clusterStatsFor("111"), 112.0),
      ClassifierHierarchyPathRecorded(
        "titi",
        ClassificationHierarchyPath(
          recordedPath
        )
      ),

      ClassifierStarted("alf"),
      ClassifierDistancesComputed("alf", listOf("f0"), listOf(100), 2),
      ClassifierDistancesComputed("alf", listOf("f0", "f4"), listOf(100, 101), 1),
      ClassifierDistancesComputed("alf", listOf("f0", "f2", "f4", "f7"), listOf(100, 101, 102, 103), 0),
      ClassifierClosestReferenceFileFound("alf", "f0", HierarchyExamples.clusterStatsFor("111"), 112.0),
      ClassifierHierarchyPathRecorded(
        "alf",
        ClassificationHierarchyPath(
          recordedPath
        )
      )
    )
  }
}

