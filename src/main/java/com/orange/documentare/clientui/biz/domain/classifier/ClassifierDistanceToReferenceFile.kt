package com.orange.documentare.clientui.biz.domain.classifier

import com.orange.documentare.clusteringui.biz.file.query.ClusterDistancesStats
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelFile

// referenceFile may be a multiset element, so we provide cluster stats separately
data class ClassifierDistanceToReferenceFile(
  val referenceFile: ClientHierarchyReferenceModelFile,
  val clusterDistancesStats: ClusterDistancesStats,
  val distance: Int,
  val clusterDistanceThreshold: Double
) {

  // ⚠️ Equal case is interesting for instance for a cluster of two elements 'Center1' & 'File2':
  // if we try to classify 'File2', we need equality since distance == clusterDistanceThreshold
  fun distanceUnderOrEqualThreshold() = distance <= clusterDistanceThreshold

  fun singletonAutoInclusion() = clusterDistancesStats.singleton
}
