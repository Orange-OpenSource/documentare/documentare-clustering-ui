package com.orange.documentare.clientui.biz.domain

import com.orange.documentare.clientui.biz.domain.classifier.ClassificationTrustWorthiness

data class FrontBestCandidate(
  val filename: String, val displayName: String?, val trustWorthiness: ClassificationTrustWorthiness?, val thumbnailUri: String? = null, val originalImageUri: String?
)
