package com.orange.documentare.clientui.biz.domain.classifier.events

data class ClassifierCleared(val resultsCleared: Boolean = true) : ClassifierEvent {
  override fun toString(): String {
    return "🗑️ CLEAR classification results"
  }
}
