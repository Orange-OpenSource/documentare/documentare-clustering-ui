package com.orange.documentare.clientui.biz.transport

import com.orange.documentare.clientui.biz.domain.ClientRootDir
import com.orange.documentare.clientui.biz.domain.ClientUIController
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ClientUIConfig {
  @Bean
  fun getClientUIController(
    clientHierarchyExport: ClientHierarchyExport,
    @Value("\${client.root.dir.path}") clientRootDirPath: String,
    @Value("\${client.embedded-computation-server-distance-prep-directory}") distancePrepDir: String,
    @Value("\${app.embedded-computation-server-raw-image-cache-directory}") rawImageCacheDir: String,
  ): ClientUIController {
    return ClientUIController(clientHierarchyExport, ClientRootDir(clientRootDirPath), distancePrepDir, rawImageCacheDir, ClientUI.classifierListener)
  }
}
