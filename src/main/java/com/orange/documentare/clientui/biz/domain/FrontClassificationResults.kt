package com.orange.documentare.clientui.biz.domain

import com.orange.documentare.clientui.biz.domain.classifier.ClassifierDistanceToReferenceFile
import com.orange.documentare.clientui.biz.domain.classifier.ClassifierResult
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelFile

data class FrontClassificationResults(val results: List<FrontClassificationResult>) {
  companion object {
    fun from(classifierResults: List<ClassifierResult>): FrontClassificationResults {
      return FrontClassificationResults(
        classifierResults.map { classifierResult ->
          FrontClassificationResult(
            FileToClassify(classifierResult.classifiedFile.filename, classifierResult.classifiedFile.imageUri),
            classifierResult.closestSortedReferenceFiles.all().mapIndexed { index, it: ClassifierDistanceToReferenceFile ->
              FrontBestCandidate(
                it.referenceFile.filename,
                displayName(it.referenceFile.className, it.referenceFile.fileNamedClass, it.referenceFile.clusterMultiHypothesisName),
                if (index == 0) classifierResult.classificationHierarchyPath.trustWorthiness() else null,
                it.referenceFile.webThumbnailPath,
                refModelImageWebPath(it.referenceFile)
              )
            }
          )
        }
      )
    }

    fun refModelImageWebPath(modelFile: ClientHierarchyReferenceModelFile): String {
      return if (ClientRootDir.isImage(modelFile.filename)) {
        modelFile.webPath
      } else {
        ClientRootDir.NO_IMAGE
      }
    }

    fun displayName(className: String?, fileNamedClass: List<String>?, multiHypothesisName: String?): String? {
      @Suppress("IfThenToElvis")
      return if (className != null) {
        className
      } else if (multiHypothesisName != null) {
        multiHypothesisName
      } else if (!fileNamedClass.isNullOrEmpty()) {
        fileNamedClass.joinToString(" / ")
      } else null
    }
  }
}
