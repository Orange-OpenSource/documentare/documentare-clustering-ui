package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.orange.documentare.clusteringui.biz.file.command.AddToClusterCommand.AddToClusterAssociation
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.persistence.EventRepository


class AddToClusterCommandHandler(private val eventRepository: EventRepository, eventBus: EventBus) : CommandHandler<AddToClusterCommand, Void>(eventBus) {

  override fun doExecute(command: AddToClusterCommand): AggregateAndEvents<Void?, Collection<Event>> {
    val events: List<Event> = command.associations.mapNotNull { addToCluster(it) }
    return AggregateAndEvents.withOnlyEvents(events)
  }

  private fun addToCluster(addToClusterAssociation: AddToClusterAssociation): FileAddedToCluster? {
    val file = loadFile(addToClusterAssociation.fileId)
    return if (file == null) {
      null
    } else {
      val fileAddedToCluster: FileAddedToCluster =
        if (addToClusterAssociation.inMultiset != null && addToClusterAssociation.inMultiset) {
          file.moveToCluster(
            addToClusterAssociation.clusterId,
            addToClusterAssociation.clusterCenter,
            null,
            null,
            true,
            addToClusterAssociation.lock,
            addToClusterAssociation.className
          )
        } else {
          file.moveToCluster(
            addToClusterAssociation.clusterId,
            addToClusterAssociation.clusterCenter,
            addToClusterAssociation.distanceToCenter,
            null,
            addToClusterAssociation.inMultiset,
            addToClusterAssociation.lock,
            addToClusterAssociation.className
          )
        }
      fileAddedToCluster
    }
  }

  private fun loadFile(fileId: String): DomainFile? {
    return eventRepository.load(fileId)
  }
}
