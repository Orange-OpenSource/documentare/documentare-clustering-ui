package com.orange.documentare.clusteringui.biz.file.transport

import com.orange.documentare.clusteringui.Couple
import com.orange.documentare.clusteringui.biz.file.query.DisplayHomeFilesQuery
import com.orange.documentare.clusteringui.biz.file.query.DisplayHomeFilesView
import com.orange.documentare.clusteringui.biz.file.query.FileUIView
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import jakarta.servlet.http.HttpServletResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.FileSystemResource
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.ResponseBody
import java.io.FileOutputStream
import java.io.IOException

@Controller
class Export(@Autowired private val queryBus: QueryBus) {

  @Secured("ROLE_USER")
  @ResponseBody
  @RequestMapping("/export-classes.csv", method = [GET])
  fun exportClasses(response: HttpServletResponse): FileSystemResource {
    val prefix = "GET [/export-classes.csv]"
    log.info(prefix)

    val path = createExportClassesFile()

    response.contentType = "application/csv"
    response.setHeader("Content-Disposition", "attachment; filename=export-classes.csv")

    return FileSystemResource(path)
  }

  private fun createExportClassesFile(): String {
    var exportFilename = ""
    queryBus.dispatch<QueryResponse<DisplayHomeFilesView>>(DisplayHomeFilesQuery())
      .onSuccess { homeView ->
        try {
          exportFilename = createExportClassesFile(homeView)
        } catch (throwable: IOException) {
          onError(throwable)
        }
      }
      .onFailure { throwable ->
        onError(throwable)
      }
    return exportFilename
  }

  private fun onError(throwable: Throwable) {
    log.error("GET [/] error: '${throwable.localizedMessage}", throwable)
    throw throwable
  }

  internal fun createExportClassesFile(homeView: DisplayHomeFilesView): String {
    val couple: Couple<String, FileOutputStream> = FileIO.createExportClassesFile()
    val filename = couple.first
    val outputStream = couple.second
    homeView.mapClassViews?.onEach { (className, classClusters) ->
      classClusters.forEach { (clusterId, clusterFiles) ->
        clusterFiles.fileUIViews.forEach { clusterFile ->
          writeLine(className, clusterId, clusterFile, outputStream)
        }
      }
    }
    outputStream.close()
    return filename
  }

  private fun writeLine(className: String, clusterId: String, clusterFile: FileUIView, outputStream: FileOutputStream) {
    val distanceToCenter = (if (clusterFile.clusterCenter || clusterFile.inMultiset) 0 else clusterFile.distanceToCenter)
      ?: throw IllegalStateException("[🛑 CSV EXPORT ERROR] file ${clusterFile.filename} is not a cluster center nor in multiset but have no distance to cluster center: $clusterFile")

    val fileNamedClass = if (clusterFile.fileNamedClass.isNullOrEmpty()) "" else clusterFile.fileNamedClass[0]
    val lineContent = "$className;$clusterId;${clusterFile.filename};$distanceToCenter;${fileNamedClass}\n"
    val strToBytes = lineContent.toByteArray()
    try {
      outputStream.write(strToBytes)
      log.debug("[Export] $lineContent")
    } catch (e: IOException) {
      log.error("[Export] failed to write line '$lineContent'", e)
    }
  }

  companion object {
    private val log = LoggerFactory.getLogger(Export::class.java)
  }
}
