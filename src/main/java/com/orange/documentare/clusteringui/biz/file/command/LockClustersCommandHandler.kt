package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.persistence.EventRepository


class LockClustersCommandHandler(private val eventRepository: EventRepository, eventBus: EventBus) : CommandHandler<LockClustersCommand, Void>(eventBus) {

  override fun doExecute(command: LockClustersCommand): AggregateAndEvents<Void?, Collection<Event>> {
    val files = eventRepository.load(command.filesIds)
    val lockClusters = files
      .filter { it.clusterCenter() }
      .map { it.tagClusterLocked() }

    return AggregateAndEvents.withOnlyEvents(lockClusters)
  }
}