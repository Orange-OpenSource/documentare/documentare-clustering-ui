package com.orange.documentare.clusteringui.biz.file.query.projection

import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventHandler
import com.tophe.ddd.infrastructure.persistence.EventRepository
import com.tophe.ddd.infrastructure.persistence.RepositoryApplyOnAll
import org.slf4j.LoggerFactory
import java.util.*

class Projections : EventHandler<Event> {
  private val eventRepository: EventRepository
  val files = FilesProjection()
  val clusters = ClustersProjection()

  constructor(eventRepository: EventRepository) {
    this.eventRepository = eventRepository
    init()
  }

  constructor(eventRepository: EventRepository, dateTimeMachine: Date) {
    this.eventRepository = eventRepository
    initBeforeDate(dateTimeMachine)
  }

  fun init() {
    log.warn("[PROJECTIONS] Start to rebuild projections by pages, {} events", eventRepository.count())
    val t0 = System.currentTimeMillis()
    eventRepository.applyOnAll(object : RepositoryApplyOnAll<Event> {
      override fun applyOnAll(aggregateRoots: List<Event>) {
        aggregateRoots.forEach { event: Event -> onEvent(event) }
      }
    }, PAGINATION_SIZE)
    log.warn("[PROJECTIONS] Time to rebuild projections by pages: {} ms", System.currentTimeMillis() - t0)
  }

  // FIXME: TODO with pageable!
  private fun initBeforeDate(dateTimeMachine: Date) {
    val t0 = System.currentTimeMillis()
    val events = eventRepository.findAll()
    val eventsToKeep = events.filterNot {
      Date.from(it.date).after(dateTimeMachine)
    }
    val eventsCount = eventsToKeep.size
    val t1 = System.currentTimeMillis()
    log.warn("[PROJECTIONS] Time to load events ({}) from db: {} ms", eventsCount, t1 - t0)
    testPurposeInitFromHistory(eventsToKeep)
  }


  fun testPurposeInitFromHistory(events: Iterable<Event>) {
    var t0 = System.currentTimeMillis()
    files.initFromHistory(events)
    log.warn("[FILES PROJECTIONS] Time to rebuild projection for {} files: {} ms", files.filesCount(), System.currentTimeMillis() - t0)
    t0 = System.currentTimeMillis()
    clusters.initFromHistory(events)
    log.warn("[CLUSTERS PROJECTIONS] Time to rebuild projection: {} ms", System.currentTimeMillis() - t0)
  }

  @Synchronized
  public override fun onEvent(event: Event) {
    files.onEvent(event)
    clusters.onEvent(event)
  }

  override fun supports(event: Event): Boolean {
    return true
  }

  fun reset() {
    files.reset()
    clusters.reset()
  }

  fun hierarchyLevelCount(): Int {
    val filesHierarchyLevelCount = files.hierarchyLevelCount()
    val clustersHierarchyLevelCount = clusters.hierarchyLevelCount()
    if (filesHierarchyLevelCount != clustersHierarchyLevelCount) {
      throw IllegalStateException("files hierarchy level count ($filesHierarchyLevelCount) != clusters hierarchy level count ($clustersHierarchyLevelCount)")
    }
    return filesHierarchyLevelCount
  }

  fun keepOnlyFirstHierarchyLevel() {
    files.keepOnlyFirstHierarchyLevel()
    clusters.keepOnlyFirstHierarchyLevel()
  }

  companion object {
    private const val PAGINATION_SIZE = 200000
    private val log = LoggerFactory.getLogger(Projections::class.java)
  }
}
