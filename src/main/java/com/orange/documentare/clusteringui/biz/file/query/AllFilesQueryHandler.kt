package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler

class AllFilesQueryHandler(private val projections: Projections) : QueryHandler<AllFilesQuery, List<DomainFile>>() {

  override fun doExecute(query: AllFilesQuery) =
    projections.files.ids()
      .map { projections.files[it] }
      .filter { !it.deleted() }
}