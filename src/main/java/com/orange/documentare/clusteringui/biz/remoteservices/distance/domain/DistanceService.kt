package com.orange.documentare.clusteringui.biz.remoteservices.distance.domain

import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.AggregateId

interface DistanceService {
  fun isRunning(): Boolean
  fun isDistanceBeingComputedFor(id: String): Boolean

  fun computeDistancesOf(parameters: Parameters, fileIds: List<String>)
  fun computeDistancesOfInSameThread(parameters: Parameters, fileIds: List<String>)
  fun computeDistancesToCentroidAnywhereInHierarchyInSameThread(files: List<AggregateId>, centroidMultisetFiles: List<AggregateId>): List<Pair<AggregateId, Int>>
}
