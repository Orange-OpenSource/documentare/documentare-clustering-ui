package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory

data class DisplayHierarchyLevelFilesQueryHandler(
  private val projections: Projections
) : QueryHandler<DisplayHierarchyLevelFilesQuery, DisplayHierarchyLevelFilesView>() {

  public override fun doExecute(query: DisplayHierarchyLevelFilesQuery): DisplayHierarchyLevelFilesView {
    val filesHierarchyLevel = projections.files.hierarchyLevel(query.hierarchyLevelIndex)
    val clustersHierarchyLevel = projections.clusters.hierarchyLevel(query.hierarchyLevelIndex)
    val transform = DisplayHierarchyLevelFileTransform(filesHierarchyLevel, clustersHierarchyLevel) { toFileView(it) }

    val t0 = System.currentTimeMillis()
    val notDeleted = filesHierarchyLevel.ids().mapNotNull { fileId -> filesHierarchyLevel[fileId] }
      .filter { file -> !file.deleted() }
      .map { file -> toFileView(file) }

    val notInClusters = transform.filesNotInClusters()
    val mapClassViews: Map<String, Map<String, ClusterUIView>> = transform.mapClasses()
    val sortedMap: Map<String, Map<String, ClusterUIView>> = mapClassViews.toSortedMap()
    val hierarchyLevelCount = projections.hierarchyLevelCount()
    val currentHierarchyLevel = query.hierarchyLevelIndex

    val viewES = DisplayHierarchyLevelFilesView(
      hierarchyLevelCount, currentHierarchyLevel,
      filesHierarchyLevel.filesCount() == 0, notDeleted, notInClusters, sortedMap
    )

    val dtMs = System.currentTimeMillis() - t0
    if (dtMs > 10) {
      log.info("ES VIEW LOAD = $dtMs")
    }

    return viewES
  }

  private fun toFileView(file: DomainFile): FileUIView {
    return FileUIView.from(file)
  }


  companion object {
    private val log: Logger = LoggerFactory.getLogger(DisplayHierarchyLevelFilesQueryHandler::class.java)
  }
}
