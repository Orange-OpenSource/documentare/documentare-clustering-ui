package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.orange.documentare.clusteringui.Couple
import com.orange.documentare.clusteringui.biz.appnotifications.AppNotification
import com.orange.documentare.clusteringui.biz.file.command.TagClusterNotModifiedCommand
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.ClusterFilesQuery
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.FileDeletedInCluster.aFileWasDeletedInTheCluster
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure.PreviousClusterId
import com.orange.documentare.core.model.ref.clustering.Clustering
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory

internal open class ComputeClusteringOnModifiedClusters {

  open fun doSync(infra: ClusteringInfra, parameters: Parameters, clustersModified: Map<String, List<DomainFile>>): Int? {
    val modifiedClustersNotEmpty = clustersModified.keys
      .filter { (clustersModified[it] ?: error("modified cluster ($it) should not contain a null list")).isNotEmpty() }

    if (modifiedClustersNotEmpty.isEmpty()) {
      log.info("\t[Recalculate] no modified cluster")
      return null
    }

    log.info("\t[Recalculate] recalculate clusters")
    log.debug("\t[Recalculate] recalculate clusters, clusters count '${modifiedClustersNotEmpty.size}'")

    val clustersFiles = modifiedClustersNotEmpty
      .map { clusterId ->
        Couple(
          PreviousClusterId(clusterId), clustersModified[clusterId]
            ?: error("clustersModified of $clusterId should not be null")
        )
      }

    return recalculateClusters(infra, clustersFiles, parameters)
  }

  open fun recalculateClusters(infra: ClusteringInfra, clustersFiles: List<Couple<PreviousClusterId, List<DomainFile>>>, parameters: Parameters): Int {
    var t0 = System.currentTimeMillis()
    val results = infra.clusteringRemote.recalculateClusters(parameters, clustersFiles)
    log.info("\t--> {} ms [recalculate clusters]", System.currentTimeMillis() - t0)

    t0 = System.currentTimeMillis()
    results.forEach { previousClusterIdAndClusteringResultMapEntry ->
      previousClusterIdAndClusteringResultMapEntry.value
        .onFailure { log.error("recalculate clusters error occurred", it) }
        .onSuccess { clustering ->
          var clusterLocked = false
          val previousClusterId = previousClusterIdAndClusteringResultMapEntry.key.id
          val clusterFilesQuery = infra.queryBus.dispatch<QueryResponse<List<DomainFile>>>(ClusterFilesQuery(previousClusterId))
          if (clusterFilesQuery.value().isNotEmpty()) {
            for (file in clusterFilesQuery.value()) {
              if (file.clusterCenter() && file.locked()) {
                clusterLocked = true
                break
              }
            }
          }

          val multisetIndices = clustering.clusters
            .filter { it?.multisetElementsIndices != null }
            .map { it.multisetElementsIndices }
            .firstOrNull()
          val multisetCount = multisetIndices?.size ?: 0

          if ((clusterLocked) && (multisetCount > parameters.maxMultisetThreshold)) {
            log.info("Don't save Cluster '$previousClusterId' because Cluster Locked and Multiset '$multisetCount' is greater than maxMultisetThreshold '${parameters.maxMultisetThreshold}'")
            tagClusterNotModified(infra, previousClusterId)
          } else {
            if (aFileWasDeletedInTheCluster(infra.queryBus, clustering.elements.map { it.id })) {
              log.info("A file was deleted during recalculateOneCluster, drop recalculate result")
            } else {
              saveClustering(infra, clustering, previousClusterId)
            }
          }
        }
    }
    log.info("\t--> {} ms [compute distances to multiset center]", System.currentTimeMillis() - t0)

    infra.appNotifications.notify(AppNotification.RECALCULATE_CLUSTERS_DONE)

    return results.size
  }

  open fun saveClustering(infra: ClusteringInfra, cl: Clustering, clusterIdModified: String) =
    SaveClustering.instance(infra).saveClustering(cl, clusterIdModified)

  open fun tagClusterNotModified(infra: ClusteringInfra, clusterIdModified: String) {
    val clusterFilesQuery = infra.queryBus.dispatch<QueryResponse<List<DomainFile>>>(ClusterFilesQuery(clusterIdModified))
    val clusterCenterId = clusterFilesQuery.value()
      .filter { f -> f.clusterCenter() }
      .map { f -> f.aggregateId }
      .first()
    (infra.commandBus.dispatch<CommandResponse<Void>>(TagClusterNotModifiedCommand(clusterCenterId)))
      .onFailure { throwable -> log.error(" error3: $throwable") }
  }

  companion object {
    private val log: Logger = LoggerFactory.getLogger(ComputeClusteringOnModifiedClusters::class.java)
  }
}
