package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.orange.documentare.clusteringui.biz.appnotifications.AppNotification
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.FileDeletedInCluster.aFileWasDeletedInTheCluster
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.PurgeBin.purgeBin
import org.slf4j.LoggerFactory

internal class CreateNewCluster {
  private val log = LoggerFactory.getLogger(CreateNewCluster::class.java)

  fun doAsync(clusteringInfra: ClusteringInfra, parameters: Parameters, files: List<DomainFile>, filesIdsInBinToPurge: List<String>) {
    purgeBin(clusteringInfra.commandBus, filesIdsInBinToPurge)

    ClusteringAsyncTreatment(clusteringInfra.inProgress).start {
      recalculateOneCluster(clusteringInfra, files, parameters)
    }
  }

  private fun recalculateOneCluster(infra: ClusteringInfra, files: List<DomainFile>, parameters: Parameters) {
    infra.clusteringRemote.createCluster(parameters, files)
      .onFailure { log.error("create cluster error occurred", it) }
      .onSuccess { cluster ->
        if (!aFileWasDeletedInTheCluster(infra.queryBus, DomainFile.idsOf(files))) {
          SaveClustering.instance(infra).saveClustering(cluster, files)
        }
      }
    infra.appNotifications.notify(AppNotification.CREATE_CLUSTER_DONE)
  }
}
