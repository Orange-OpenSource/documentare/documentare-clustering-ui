package com.orange.documentare.clusteringui.biz.file.domain

class DomainFileBuilder internal constructor() {
  private var aggregateId: String? = FILE_NOT_CREATED_YET
  private var filename: String? = FILE_NOT_CREATED_YET
  private var clusterId: String? = null
  private var clusterCenter: Boolean? = null
  private var distancesToClustersCenters: List<DistanceToCluster>? = null
  private var duplicate: Boolean? = null
  private var inMultiset: Boolean? = null
  private var distanceToCenter: Int? = null
  private var automaticInclude: Boolean? = null
  private var clusterModified: Boolean? = null
  private var delete: Boolean? = null
  private var inWaitingRoom: Boolean? = null
  private var lock: Boolean? = null
  private var className: String? = null
  private var fileNamedClass: Array<String>? = null

  /**
   * db ID by name convention
   *
   * @return `this`.
   */
  fun aggregateId(aggregateId: String?): DomainFileBuilder {
    this.aggregateId = aggregateId
    return this
  }

  fun filename(filename: String?): DomainFileBuilder {
    this.filename = filename
    return this
  }

  fun clusterId(clusterId: String?): DomainFileBuilder {
    this.clusterId = clusterId
    return this
  }

  fun clusterCenter(clusterCenter: Boolean?): DomainFileBuilder {
    this.clusterCenter = clusterCenter
    return this
  }

  fun distancesToClustersCenters(distancesToClustersCenters: List<DistanceToCluster>?): DomainFileBuilder {
    this.distancesToClustersCenters = distancesToClustersCenters
    return this
  }

  fun duplicate(duplicate: Boolean?): DomainFileBuilder {
    this.duplicate = duplicate
    return this
  }

  fun inMultiset(inMultiset: Boolean?): DomainFileBuilder {
    this.inMultiset = inMultiset
    return this
  }

  fun distanceToCenter(distanceToCenter: Int?): DomainFileBuilder {
    this.distanceToCenter = distanceToCenter
    return this
  }

  fun automaticInclude(automaticInclude: Boolean?): DomainFileBuilder {
    this.automaticInclude = automaticInclude
    return this
  }

  fun clusterModified(clusterModified: Boolean?): DomainFileBuilder {
    this.clusterModified = clusterModified
    return this
  }

  fun delete(delete: Boolean?): DomainFileBuilder {
    this.delete = delete
    return this
  }

  fun inWaitingRoom(inWaitingRoom: Boolean?): DomainFileBuilder {
    this.inWaitingRoom = inWaitingRoom
    return this
  }

  fun lock(lock: Boolean?): DomainFileBuilder {
    this.lock = lock
    return this
  }

  fun className(className: String?): DomainFileBuilder {
    this.className = className
    return this
  }

  fun fileNamedClass(fileNamedClass: Array<String>?): DomainFileBuilder {
    this.fileNamedClass = fileNamedClass
    return this
  }

  fun build(): DomainFile =
    DomainFile(
      aggregateId!!,
      filename!!,
      clusterId,
      clusterCenter,
      distancesToClustersCenters,
      duplicate,
      inMultiset,
      distanceToCenter,
      automaticInclude,
      clusterModified,
      delete,
      inWaitingRoom,
      lock,
      className,
      fileNamedClass
    )

  companion object {
    private const val FILE_NOT_CREATED_YET = "file not created yet"
  }
}
