package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster

class AssociateFileView(
  @JvmField val fileUIView: FileUIView,
  @JvmField val clustersCenterViews: Map<String, List<FileUIView>>?,
  @JvmField val fileDistanceToCluster: Map<String, List<DistanceToCluster>>?
)
