package com.orange.documentare.clusteringui.biz.file.query

import com.tophe.ddd.queries.Query

data class SelectedFilesQuery(val fileIds: List<String>) : Query