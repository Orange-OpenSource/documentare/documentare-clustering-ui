package com.orange.documentare.clusteringui.biz.services.impl

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.*
import com.orange.documentare.clusteringui.biz.file.query.ClusterDistancesStats.ClusterDistancesElement
import com.orange.documentare.clusteringui.biz.parameters.domain.DistanceParameters
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.AggregateId
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.DistanceToAggregate
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.HierarchyClustersWeightedStats
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.StatisticsConsolidation.Aggregate
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.StatisticsConsolidation.ClusterCenterAggregate
import com.orange.documentare.clusteringui.biz.processes.hierarchy.transport.HierarchyStatisticsConsolidation
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModel
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelFile
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelLevel
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class ClientHierarchyExportImpl(val hierarchyStatisticsConsolidation: HierarchyStatisticsConsolidation) : ClientHierarchyExport {
  private lateinit var queryBus: QueryBus

  fun setQueryBus(queryBus: QueryBus) {
    this.queryBus = queryBus
  }

  override fun exportClientHierarchy(): ClientHierarchyReferenceModel {
    return ClientHierarchyExport.cleanLevel0(doExportClientHierarchyWithWholeLevel0())
  }

  override fun exportClientHierarchyForStatisticsConsolidation(): ClientHierarchyReferenceModel {
    return doExportClientHierarchyWithWholeLevel0(false)
  }

  private fun doExportClientHierarchyWithWholeLevel0(withClusterStats: Boolean = true): ClientHierarchyReferenceModel {
    val hierarchiesView: HierarchiesView = HierarchiesView.get(queryBus)
    val clustersWeightedStats = HierarchyClustersWeightedStats(hierarchiesView) { levelIndex, clusterId -> clusterStatisticsForLevel(levelIndex, clusterId) }
    var currentLevelIndex = hierarchiesView.topHierarchyLevel()
    val levels = mutableListOf<ClientHierarchyReferenceModelLevel>()
    do {
      mapLevel(clustersWeightedStats, currentLevelIndex, hierarchiesView.level(currentLevelIndex), withClusterStats)
        ?.let { levels.add(it) }
      currentLevelIndex--
    } while (currentLevelIndex >= 0)

    return ClientHierarchyReferenceModel(levels.reversed(), DistanceParameters.get(queryBus))
  }

  private fun mapLevel(
    clustersWeightedStats: HierarchyClustersWeightedStats,
    levelIndex: Int,
    level: Map<ClusterId, List<DomainFile>>,
    withClusterStats: Boolean
  ): ClientHierarchyReferenceModelLevel? {
    if (levelIndex > 0 && levelContainsOnlyASingleton(level)) {
      return skipLevel()
    }

    return completeLevel(clustersWeightedStats, levelIndex, level, withClusterStats)
  }

  private fun completeLevel(
    clustersWeightedStats: HierarchyClustersWeightedStats,
    levelIndex: Int,
    level: Map<ClusterId, List<DomainFile>>,
    withClusterStats: Boolean
  ): ClientHierarchyReferenceModelLevel {
    val clientHierarchyReferenceModelLevel = ClientHierarchyReferenceModelLevel(
      level.map { mapEntry: Map.Entry<ClusterId, List<DomainFile>> ->
        val clusterId: ClusterId = mapEntry.key
        val clusterFiles: List<DomainFile> = mapEntry.value
        clusterFiles.map {
          ClientHierarchyReferenceModelFile(
            it.aggregateId,
            it.filename,
            it.clusterCenter(),
            it.inMultiset(),
            if (it.clusterCenter() && withClusterStats) retrieveClusterStatistics(clustersWeightedStats, levelIndex, clusterId) else null,
            if (it.clusterCenter()) retrieveClusterMultiHypothesisName(levelIndex, clusterId) else null,
            it.className,
            it.fileNamedClass?.toList(),
            FileIO.filePathOf(it),
            FileIO.fileWebPathOf(it),
            FileIO.thumbnailWebPathOf(it)
          )
        }
      }
    )
    displayStats(clientHierarchyReferenceModelLevel, levelIndex)
    return clientHierarchyReferenceModelLevel
  }

  private fun displayStats(clientHierarchyReferenceModelLevel: ClientHierarchyReferenceModelLevel, levelIndex: Int) {
    clientHierarchyReferenceModelLevel.referenceFiles
      .flatten()
      .mapNotNull { it.clusterDistancesStats }
      .forEach {
        log.info("🐞 L$levelIndex - ${it.description}")
      }
  }

  private fun levelContainsOnlyASingleton(level: Map<ClusterId, List<DomainFile>>): Boolean {
    val allHierarchyFiles = level.values.flatten()
    return allHierarchyFiles.size == 1
  }

  private fun skipLevel() = null

  private fun retrieveClusterStatistics(
    clustersWeightedStats: HierarchyClustersWeightedStats,
    hierarchyLevelIndex: Int,
    clusterId: ClusterId
  ): ClusterDistancesStats {
    return clustersWeightedStats.clusterStats(hierarchyLevelIndex, clusterId)
  }

  private fun retrieveClusterMultiHypothesisName(hierarchyLevel: Int, clusterId: String): String? {
    val response = queryBus.dispatch<QueryResponse<List<DomainFile>>>(ClusterFilesQuery(clusterId, hierarchyLevel))
    return if (response.success()) {
      val clusterFiles: List<DomainFile> = response.value()
      ClusterUIView.multiHypothesisNamedClass(clusterFiles.map { it.fileNamedClass?.asList() })
    } else {
      throw IllegalStateException("❌  Failed to retrieve multi-hypothesis name of cluster '$clusterId' at level $hierarchyLevel")
    }
  }

  private fun clusterStatisticsForLevel(levelIndex: Int, clusterId: ClusterId): ClusterDistancesStats {
    return queryBus.dispatch<QueryResponse<ClusterDistancesStats>>(DistanceToCenterStatsForAllFilesInClusterQuery(clusterId, levelIndex)).value()
      ?: throw IllegalStateException("❌  Failed to retrieve statistics of cluster '$clusterId' at level '$levelIndex'")
  }

  fun clusterStatisticsInUpperHierarchyLevels(clusterCenterAggregateId: AggregateId, hierarchyLevelIndex: Int): ClusterDistancesStats {
    if (!hierarchyStatisticsConsolidation.computedDistancesAvailable()) {
      throw IllegalStateException("❌  hierarchy distances statistics not available yet")
    }

    val clusterDistances: Pair<ClusterCenterAggregate, List<Pair<Aggregate, DistanceToAggregate>>> =
      hierarchyStatisticsConsolidation.computedDistances().forCluster(hierarchyLevelIndex, clusterCenterAggregateId)
    val clusterCenterAggregate: ClusterCenterAggregate = clusterDistances.first
    val distances: List<Pair<Aggregate, DistanceToAggregate>> = clusterDistances.second
    val clusterDistancesStats = ClusterDistancesStats.buildWith(
      distances.map {
        val filename = it.first.filename
        val inMultiset = clusterCenterAggregate.centroid.contains(it.first.aggregateId)
        val distanceToCentroid = it.second
        ClusterDistancesElement(filename, inMultiset, distanceToCentroid)
      }
    )
    return clusterDistancesStats
  }

  companion object {
    private val log = LoggerFactory.getLogger(ClientHierarchyExportImpl::class.java)
  }
}
