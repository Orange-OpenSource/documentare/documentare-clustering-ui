package com.orange.documentare.clusteringui.biz.file.domain

import com.tophe.ddd.infrastructure.event.Event

class DestroyedClustersAndUpdatedFiles(val destroyedClusters: List<String>, val events: List<Event>)