package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler

class SelectedFilesAtLevel0QueryHandler(private val projections: Projections) : QueryHandler<SelectedFilesAtLevel0Query, List<DomainFile>>() {

  override fun doExecute(query: SelectedFilesAtLevel0Query): List<DomainFile> =
    query.aggregatesId.map { projections.files.get(id = it, hierarchyLevelIndex = 0) }
}
