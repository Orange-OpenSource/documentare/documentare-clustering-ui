package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FilePutInNamedClass
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.persistence.EventRepository


class PutFileInNamedClassCommandHandler(private val eventRepository: EventRepository, eventBus: EventBus) : CommandHandler<PutFileInNamedClassCommand, Void>(eventBus) {

  override fun doExecute(command: PutFileInNamedClassCommand): AggregateAndEvents<Void?, Collection<Event>> {
    val events = mutableListOf<FilePutInNamedClass>()
    val domainFile = loadFile(command.fileAggregateId)
    events.add(domainFile.putInNamedClass(command.fileNamedClass))
    return AggregateAndEvents.withOnlyEvents(events)
  }

  private fun loadFile(fileId: String): DomainFile = eventRepository.load(fileId)
}
