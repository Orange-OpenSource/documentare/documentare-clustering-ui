package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler

class SelectedFilesQueryHandler(private val projections: Projections) : QueryHandler<SelectedFilesQuery, List<DomainFile>>() {

  override fun doExecute(query: SelectedFilesQuery): List<DomainFile> =
    query.fileIds.map { projections.files[it] }
}