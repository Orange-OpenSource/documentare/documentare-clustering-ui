package com.orange.documentare.clusteringui.biz.file.event;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;

import java.time.Instant;

import com.tophe.ddd.infrastructure.event.Event;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FileDuplicated extends Event {

  public final String clusterId;

  /* clusterId: here only to speed up initial projection */
  public FileDuplicated(String aggregateId, String clusterId) {
    super(aggregateId);
    this.clusterId = clusterId;
  }

  private FileDuplicated(Instant date, String aggregateId, String clusterId) {
    super(aggregateId, date);
    this.clusterId = clusterId;
  }

  public static FileDuplicated buildFromDB(Instant date, String aggregateId, String clusterId) {
    return new FileDuplicated(date, aggregateId, clusterId);
  }


  //
  // FOR TESTS
  //
  public static FileDuplicated example() {
    return new FileDuplicated(TEST_DATE, AGGREGATE_ID_EXAMPLE.toString(), exampleClusterId(AGGREGATE_ID_EXAMPLE));
  }

}
