package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler

class AllFilesInWaitingRoomQueryHandler(private val projections: Projections) : QueryHandler<AllFilesInWaitingRoomQuery, List<DomainFile>>() {

  override fun doExecute(query: AllFilesInWaitingRoomQuery): List<DomainFile> =
    projections.clusters.filesNotInClusters()
      .map { fileId: String -> projections.files[fileId] }
      .filter { file: DomainFile -> file.isInWaitingRoom() }
}