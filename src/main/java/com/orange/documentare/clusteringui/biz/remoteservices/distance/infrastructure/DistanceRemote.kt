package com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure

import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile.Companion.idsOf
import com.orange.documentare.clusteringui.biz.parameters.domain.DistanceParameters
import com.orange.documentare.clusteringui.biz.remoteservices.infrastructure.BytesDataUI
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import org.slf4j.LoggerFactory

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

class DistanceRemote(embeddedComputationServer: EmbeddedComputationServer, private val remoteDistance: RemoteDistancePort = RemoteDistance(null, embeddedComputationServer)) :
  DistanceRemoteApi {

  init {
    if (embeddedComputationServer.enabled) {
      initRemoteSimdocServerForDistance(listOf("embedded computation server"))
    }
  }

  override fun doTheJob(
    distanceParameters: DistanceParameters,
    files: List<DomainFile>,
    clustersMultisetFiles: Collection<List<DomainFile>>
  ): TryOrCatch<Map<DomainFile, DistancesRequestResult>> {
    val fileToDistanceResultMap: MutableMap<DomainFile, DistancesRequestResult> = HashMap()
    try {
      val distancesRequests = prepareDistancesRequests(files, distanceParameters, clustersMultisetFiles)
      val results =
        remoteDistance.request(remoteSimdocServersUrls!!, distancesRequests)

      if (results.isFailure) {
        return TryOrCatch.failure(results.cause)
      }
      for (i in files.indices) {
        val result = results.get()[distancesRequests[i]]
        fileToDistanceResultMap[files[i]] = result!!
      }
    } catch (e: Exception) {
      return TryOrCatch.failure(e)
    }
    return TryOrCatch.of { fileToDistanceResultMap.toMap() }
  }

  companion object {
    private val log = LoggerFactory.getLogger(DistanceRemote::class.java)
    private var remoteSimdocServersUrls: List<String>? = null

    fun prepareDistancesRequests(
      files: List<DomainFile>,
      distanceParameters: DistanceParameters,
      clustersMultisetFiles: Collection<List<DomainFile>>
    ): List<DistancesRequestUI> {
      val bytesDataElements = clustersMultisetFiles
        .map { multisetFiles: List<DomainFile> ->
          BytesDataUI(
            idsOf(multisetFiles),
            FileIO.filesPathOf(multisetFiles)
          )
        }
        .toTypedArray()

      log.info("\tPrepare {} files distances computation requests, to {} cluster (multiset) files", files.size, bytesDataElements.size)

      return files.map { prepareDistancesRequest(distanceParameters, it, bytesDataElements) }
    }

    fun prepareDistancesRequest(distanceParameters: DistanceParameters, element: DomainFile, bytesDataElements: Array<BytesDataUI>): DistancesRequestUI {
      val bytesDataElement = BytesDataUI(
        ArrayList(listOf(element.aggregateId)),
        ArrayList(listOf(FileIO.filePathOf(element)))
      )
      val bytesDataElementAsArray = arrayOf(bytesDataElement)

      return buildDistancesRequest(bytesDataElementAsArray, bytesDataElements, distanceParameters)
    }

    fun buildDistancesRequest(
      bytesDataElements: Array<BytesDataUI>,
      bytesData: Array<BytesDataUI>,
      distanceParameters: DistanceParameters
    ) = DistancesRequestUI(
      bytesDataElements,
      bytesData,
      distanceParameters.rawConverterEnabled,
      distanceParameters.rawConverterPixelCount,
      distanceParameters.suffixArrayAlgorithm
    )

    @JvmStatic
    fun initRemoteSimdocServerForDistance(remoteSimdocServersUrls: List<String>?) {
      Companion.remoteSimdocServersUrls = remoteSimdocServersUrls
    }
  }
}
