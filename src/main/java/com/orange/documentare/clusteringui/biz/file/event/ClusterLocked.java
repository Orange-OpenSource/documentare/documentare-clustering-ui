package com.orange.documentare.clusteringui.biz.file.event;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;

import java.time.Instant;

import com.tophe.ddd.infrastructure.event.Event;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ClusterLocked extends Event {

  public final String clusterId;
  public final boolean lock;

  public ClusterLocked(String aggregateId, String clusterId, boolean lock) {
    super(aggregateId);
    this.clusterId = clusterId;
    this.lock = lock;
  }

  private ClusterLocked(Instant date, String aggregateId, String clusterId, boolean lock) {
    super(aggregateId, date);
    this.clusterId = clusterId;
    this.lock = lock;
  }

  public static ClusterLocked buildFromDB(Instant date, String aggregateId, String clusterId, boolean lock) {
    return new ClusterLocked(date, aggregateId, clusterId, lock);
  }

  //
  // FOR TESTS
  //
  public static ClusterLocked example() {
    return new ClusterLocked(TEST_DATE, AGGREGATE_ID_EXAMPLE.toString(), exampleClusterId(AGGREGATE_ID_EXAMPLE), true);
  }
}
