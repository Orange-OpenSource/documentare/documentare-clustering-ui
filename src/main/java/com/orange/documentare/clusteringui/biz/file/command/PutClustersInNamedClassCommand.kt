package com.orange.documentare.clusteringui.biz.file.command

import com.tophe.ddd.commands.Command

class PutClustersInNamedClassCommand(val name: String, val selectedFileIdsToName: List<String>) : Command