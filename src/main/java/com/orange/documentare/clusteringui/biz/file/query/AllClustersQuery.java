package com.orange.documentare.clusteringui.biz.file.query;

import com.tophe.ddd.queries.Query;

public record AllClustersQuery() implements Query {
}
