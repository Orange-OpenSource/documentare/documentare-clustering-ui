package com.orange.documentare.clusteringui.biz.parameters.domain;

import org.jetbrains.annotations.NotNull;

import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Builder(toBuilder = true)
@ToString
@EqualsAndHashCode
public class Parameters {

  private static final boolean RAW_CONVERTER = true;
  private static final int P_COUNT = 150000;
  private static final float SD_FACTOR = 2f;
  private static final int CCUT_PERCENTILE = 75;
  private static final int KNN_THRESHOLD = 10;
  private static final boolean SLOOP = true;
  private static final boolean ENROLL = true;
  private static final boolean FEED = false;
  private static final float THRESHOLD_FACTOR = 1;
  private static final int FEED_THRESHOLD = 10;
  private static final int FEED_THRESHOLD_SMOKE_TESTS = 34;
  private static final int MIN_MULTISET_THRESHOLD = 2;
  private static final int MAX_MULTISET_THRESHOLD = 5;

  /**
   * db ID by name convention
   */
  public final Long id;

  public final Boolean rawConverter;
  public final Integer pCount;
  public final Float acutSdFactor;
  public final Float qcutSdFactor;
  public final Float scutSdFactor;
  public final Integer ccutPercentile;
  public final Integer knnThreshold;
  public final Boolean sloop;
  public final Boolean consolidateCluster;
  public final Boolean enroll;
  public final Float enrollAcutSdFactor;
  public final Float enrollQcutSdFactor;
  public final Float enrollScutSdFactor;
  public final Integer enrollCcutPercentile;
  public final Integer enrollKnnThreshold;

  public final Float thresholdFactor;
  public final Boolean feed;
  public final Integer feedThreshold;
  public final Integer minMultisetThreshold;
  public final Integer maxMultisetThreshold;

  public final SuffixArrayAlgorithm suffixArrayAlgorithm;

  public static Parameters defaults() {
    return builder()
      .rawConverter(RAW_CONVERTER)
      .pCount(P_COUNT)
      .suffixArrayAlgorithm(SuffixArrayAlgorithm.LIBDIVSUFSORT)
      .acutSdFactor(SD_FACTOR)
      .qcutSdFactor(SD_FACTOR)
      .scutSdFactor(SD_FACTOR)
      .ccutPercentile(CCUT_PERCENTILE)
      .knnThreshold(KNN_THRESHOLD)
      .sloop(SLOOP)
      .enroll(ENROLL)
      .enrollAcutSdFactor(SD_FACTOR)
      .enrollQcutSdFactor(SD_FACTOR)
      .enrollScutSdFactor(SD_FACTOR)
      .enrollCcutPercentile(CCUT_PERCENTILE)
      .enrollKnnThreshold(KNN_THRESHOLD)
      .thresholdFactor(THRESHOLD_FACTOR)
      .feed(FEED)
      .feedThreshold(FEED_THRESHOLD)
      .minMultisetThreshold(MIN_MULTISET_THRESHOLD)
      .maxMultisetThreshold(MAX_MULTISET_THRESHOLD)
      .build();
  }

  public static Parameters smokeTests() {
    return builder()
      .rawConverter(RAW_CONVERTER)
      .pCount(P_COUNT)
      .suffixArrayAlgorithm(SuffixArrayAlgorithm.LIBDIVSUFSORT)
      .scutSdFactor(SD_FACTOR)
      .ccutPercentile(CCUT_PERCENTILE)
      .knnThreshold(KNN_THRESHOLD)
      .sloop(SLOOP)
      .enroll(ENROLL)
      .thresholdFactor(THRESHOLD_FACTOR)
      .feed(true)
      .feedThreshold(FEED_THRESHOLD_SMOKE_TESTS)
      .minMultisetThreshold(MIN_MULTISET_THRESHOLD)
      .maxMultisetThreshold(MAX_MULTISET_THRESHOLD)
      .build();
  }

  public static Parameters consolidateCluster(Integer pCount, SuffixArrayAlgorithm saAlgorithm) {
    return builder()
      .pCount(pCount)
      .consolidateCluster(true)
      .suffixArrayAlgorithm(saAlgorithm)
      .build();
  }

  public boolean isFeed() {
    return feed != null && feed;
  }

  public Boolean isRawConverter() {
    return rawConverter != null && rawConverter;
  }

  public Boolean getRawConverter() {
    return rawConverter;
  }

  public Integer getpCount() {
    return pCount;
  }

  public SuffixArrayAlgorithm getSuffixArrayAlgorithm() {
    return suffixArrayAlgorithm;
  }

  public Float getAcutSdFactor() {
    return acutSdFactor;
  }

  public Float getQcutSdFactor() {
    return qcutSdFactor;
  }

  public Float getScutSdFactor() {
    return scutSdFactor;
  }

  public Integer getCcutPercentile() {
    return ccutPercentile;
  }

  public Integer getKnnThreshold() {
    return knnThreshold;
  }

  public Boolean getSloop() {
    return sloop;
  }

  public Boolean getEnroll() {
    return enroll;
  }

  public Float getEnrollAcutSdFactor() {
    return enrollAcutSdFactor;
  }

  public Float getEnrollQcutSdFactor() {
    return enrollQcutSdFactor;
  }

  public Float getEnrollScutSdFactor() {
    return enrollScutSdFactor;
  }

  public Integer getEnrollCcutPercentile() {
    return enrollCcutPercentile;
  }

  public Integer getEnrollKnnThreshold() {
    return enrollKnnThreshold;
  }

  public Float getThresholdFactor() {
    return thresholdFactor;
  }

  public Boolean getFeed() {
    return feed;
  }

  public Integer getFeedThreshold() {
    return feedThreshold;
  }

  public Integer getMaxMultisetThreshold() {
    return maxMultisetThreshold;
  }

  public Integer getMinMultisetThreshold() {
    return minMultisetThreshold;
  }

  @NotNull
  public static ParametersBuilder validExample() {
    ParametersBuilder builder = new ParametersBuilder();
    builder.suffixArrayAlgorithm = SuffixArrayAlgorithm.LIBDIVSUFSORT;
    builder.thresholdFactor = 1f;
    builder.minMultisetThreshold = 1;
    builder.maxMultisetThreshold = 2;
    return builder;
  }
}
