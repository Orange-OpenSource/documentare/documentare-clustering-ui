package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.persistence.EventRepository


class MoveFileFromWaitingRoomToSortRoomCommandHandler(private val eventRepository: EventRepository, eventBus: EventBus) :
  CommandHandler<MoveFileFromWaitingRoomToSortRoomCommand, Void>(eventBus) {

  override fun doExecute(command: MoveFileFromWaitingRoomToSortRoomCommand): AggregateAndEvents<Void?, Collection<Event>> {
    val files = eventRepository.load(command.filesIds)

    val moveToSortRoom = files
      .filter { !it.deleted() }
      .map { it.moveFromWaitingRoomToSortRoom() }

    return AggregateAndEvents.withOnlyEvents(moveToSortRoom)
  }
}
