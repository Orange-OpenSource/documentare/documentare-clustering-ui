package com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure

import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.parameters.domain.DistanceParameters

fun interface DistanceRemoteApi {
  fun doTheJob(
    distanceParameters: DistanceParameters,
    files: List<DomainFile>,
    clustersMultisetFiles: Collection<List<DomainFile>>
  ): TryOrCatch<Map<DomainFile, DistancesRequestResult>>
}
