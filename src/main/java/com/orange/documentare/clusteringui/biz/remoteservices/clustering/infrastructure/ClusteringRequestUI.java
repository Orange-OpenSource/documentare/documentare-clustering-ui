package com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure;

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import java.util.Arrays;

import com.orange.documentare.clusteringui.biz.remoteservices.infrastructure.BytesDataUI;
import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm;
import com.orange.documentare.core.computationserver.biz.clustering.api.ClusteringRequest;
import com.orange.documentare.core.computationserver.infrastructure.transport.BytesDataDTO;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class ClusteringRequestUI {

  public final BytesDataUI[] bytesData;
  public final String outputDirectory;
  public final Boolean rawConverter;
  public final Integer pCount;
  public final Boolean debug;
  public final Float acutSdFactor;
  public final Float qcutSdFactor;
  public final Float scutSdFactor;
  public final Integer ccutPercentile;
  public final Integer knnThreshold;
  public final Boolean sloop;
  public final Boolean consolidateCluster;
  public final Boolean enroll;
  public final Float enrollAcutSdFactor;
  public final Float enrollQcutSdFactor;
  public final Float enrollScutSdFactor;
  public final Integer enrollCcutPercentile;
  public final Integer enrollKnnThreshold;
  public final SuffixArrayAlgorithm suffixArrayAlgorithm;

  // not used but required by simdoc-server REST API
  public final String inputDirectory = null;


  public static ClusteringRequestBuilder builder() {
    return new ClusteringRequestBuilder();
  }

  public ClusteringRequest toComputationServerClusteringRequest() {
    return ClusteringRequest.builder()
      .bytesData(Arrays.stream(bytesData).map(bytesDataUI -> bytesDataUI.toBytesDataDTO()).toArray(BytesDataDTO[]::new))
      .outputDirectory(outputDirectory)
      .rawConverter(rawConverter)
      .pCount(pCount)
      .acutSdFactor(acutSdFactor)
      .qcutSdFactor(qcutSdFactor)
      .scutSdFactor(scutSdFactor)
      .ccutPercentile(ccutPercentile)
      .knnThreshold(knnThreshold)
      .sloop(sloop)
      .consolidateCluster(consolidateCluster)
      .enroll(enroll)
      .enrollAcutSdFactor(enrollAcutSdFactor)
      .enrollQcutSdFactor(enrollQcutSdFactor)
      .enrollScutSdFactor(enrollScutSdFactor)
      .enrollCcutPercentile(enrollCcutPercentile)
      .enrollKnnThreshold(enrollKnnThreshold)
      .suffixArrayAlgorithm(suffixArrayAlgorithm)
      .build();
  }

  public static class ClusteringRequestBuilder {

    public String url;
    private BytesDataUI[] bytesData;
    private String outputDirectory;
    private Boolean rawConverter;
    private Integer pCount;
    private Boolean debug;
    private Float acutSdFactor;
    private Float qcutSdFactor;
    private Float scutSdFactor;
    private Integer ccutPercentile;
    private Integer knnThreshold;
    private Boolean sloop;
    private Boolean consolidateCluster;
    private Boolean enroll;
    private Float enrollAcutSdFactor;
    private Float enrollQcutSdFactor;
    private Float enrollScutSdFactor;
    private Integer enrollCcutPercentile;
    private Integer enrollKnnThreshold;
    private SuffixArrayAlgorithm suffixArrayAlgorithm;

    private ClusteringRequestBuilder() {
    }

    public ClusteringRequestUI build() {
      return new ClusteringRequestUI(bytesData, outputDirectory, rawConverter, pCount, debug, acutSdFactor, qcutSdFactor, scutSdFactor, ccutPercentile, knnThreshold, sloop,
        consolidateCluster, enroll, enrollAcutSdFactor, enrollQcutSdFactor, enrollScutSdFactor, enrollCcutPercentile, enrollKnnThreshold, suffixArrayAlgorithm);
    }

    public ClusteringRequestBuilder url(String url) {
      this.url = url;
      return this;
    }

    public ClusteringRequestBuilder bytesData(BytesDataUI[] bytesDatumUIs) {
      this.bytesData = bytesDatumUIs;
      return this;
    }

    public ClusteringRequestBuilder outputDirectory(String outputDirectory) {
      this.outputDirectory = outputDirectory;
      return this;
    }

    public ClusteringRequestBuilder rawConverter(boolean isRawConverter) {
      rawConverter = isRawConverter;
      return this;
    }

    public ClusteringRequestBuilder pCount(int pCount) {
      this.pCount = pCount;
      return this;
    }

    public ClusteringRequestBuilder debug() {
      debug = false;
      return this;
    }

    public ClusteringRequestBuilder acut(float acutSdFactor) {
      this.acutSdFactor = acutSdFactor;
      return this;
    }

    public ClusteringRequestBuilder qcut(float qcutSdFactor) {
      this.qcutSdFactor = qcutSdFactor;
      return this;
    }

    public ClusteringRequestBuilder scut(float scutSdFactor) {
      this.scutSdFactor = scutSdFactor;
      return this;
    }

    public ClusteringRequestBuilder ccut(int ccutPercentile) {
      this.ccutPercentile = ccutPercentile;
      return this;
    }

    public ClusteringRequestBuilder knnThreshold(int knnThreshold) {
      this.knnThreshold = knnThreshold;
      return this;
    }

    public ClusteringRequestBuilder sloop() {
      sloop = true;
      return this;
    }

    public ClusteringRequestBuilder consolidateCluster() {
      consolidateCluster = true;
      return this;
    }

    public ClusteringRequestBuilder enroll() {
      enroll = true;
      return this;
    }

    public ClusteringRequestBuilder enrollAcut(float enrollAcutSdFactor) {
      this.enrollAcutSdFactor = enrollAcutSdFactor;
      return this;
    }

    public ClusteringRequestBuilder enrollQcut(float enrollQcutSdFactor) {
      this.enrollQcutSdFactor = enrollQcutSdFactor;
      return this;
    }

    public ClusteringRequestBuilder enrollScut(float enrollScutSdFactor) {
      this.enrollScutSdFactor = enrollScutSdFactor;
      return this;
    }

    public ClusteringRequestBuilder enrollCcut(int enrollCcutPercentile) {
      this.enrollCcutPercentile = enrollCcutPercentile;
      return this;
    }

    public ClusteringRequestBuilder enrollK(int enrollKnnThreshold) {
      this.enrollKnnThreshold = enrollKnnThreshold;
      return this;
    }

    public ClusteringRequestBuilder suffixArrayAlgorithm(SuffixArrayAlgorithm suffixArrayAlgorithm) {
      this.suffixArrayAlgorithm = suffixArrayAlgorithm;
      return this;
    }
  }
}
