package com.orange.documentare.clusteringui.biz.processes.feed.domain.events

data class SingletonsNotLockedMovedToSortRoom(val filesId: List<String>) : FeedLoopEvent() {
  override fun displayInternalHtml() = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ${filesId.size} not locked singletons moved to sort room"
  override fun displayInternalRaw() = "      ${filesId.size} not locked singletons moved to sort room"

}
