package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.query.AllFilesInWaitingRoomQuery
import com.orange.documentare.clusteringui.biz.parameters.command.UpdateParametersCommand
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQuery
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeedLoopEvent
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeedLoopEventListener
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.ClusteringService
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceService
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventHandler
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import kotlin.concurrent.thread

@Service
class FeederServiceImpl(
  private val distanceService: DistanceService,
  private val clusteringService: ClusteringService
) : FeederService, EventHandler<Event>(), FeedLoopEventListener {

  private val feedLoopEventMemory = FeedLoopEventMemory()

  lateinit var queryBus: QueryBus
  lateinit var commandBus: CommandBus

  private var onFeederServiceEvents: FeederServiceEvents? = null
  private var feedLoopMock: FeedLoop? = null
  private var dataUpdated: Boolean = false

  final var isRunning: Boolean = false
    private set
  final var stopFeederWhenPossible: Boolean = false
    private set

  private var takeFilesRandomly =
    { filesInWaitingRoom: List<DomainFile>, triggerThreshold: Int -> filesInWaitingRoom.shuffled().take(triggerThreshold).map { file -> file.aggregateId } }

  fun getThenClearDataUpdated(): Boolean {
    val dataWasUpdated = dataUpdated
    dataUpdated = false
    return dataWasUpdated
  }

  fun stopFeederWhenPossible() {
    log.info("[FeedLoop]: request to stop feeder loop when possible")
    onFeederServiceEvents?.onStopFeederWhenPossible()
    stopFeederWhenPossible = true
  }

  @Synchronized
  private fun onNewFileCreated(fileCreated: FileCreated) {
    log.info("[Feeder, onNewFileCreated] ${fileCreated.filename}")

    onFeederServiceEvents?.onNewFileCreated()
    tryToLaunchFeeder(false)
  }

  @Scheduled(fixedRate = PERIODIC_FEEDER_MS)
  fun periodicLaunch() {
    launch()
  }

  @Synchronized
  override fun launch(feederProcessFinished: FeederProcessFinished?) {
    log.debug("[Feeder, launched]")
    tryToLaunchFeeder(true, feederProcessFinished)
  }

  override fun events() = feedLoopEventMemory.events()

  override fun onFeedLoopEvent(event: FeedLoopEvent) {
    feedLoopEventMemory.add(event)
    dataUpdated = true
    log.info("-> ${event.displayInternalRaw()}")
  }

  fun tryToLaunchFeeder(forceLaunch: Boolean, feederProcessFinished: FeederProcessFinished? = null): Thread? =
    if (shouldLaunchFeeder(forceLaunch)) {
      launchFeederInBackground(buildFeedLoop(), feederProcessFinished)
    } else {
      null
    }

  private fun buildFeedLoop(): FeedLoop {
    val feedLoop = if (feedLoopMock != null) {
      feedLoopMock as FeedLoop
    } else {
      FeedLoop(queryBus, commandBus, { parameters()!! }, clusteringService, takeFilesRandomly, this)
    }
    return feedLoop.also { onFeederServiceEvents = it }
  }

  private fun shouldLaunchFeeder(forceLaunch: Boolean): Boolean {
    if (feedLoopMock != null && !isRunning) {
      return true
    }

    if (isRunning || distanceService.isRunning() || clusteringService.isRunning()) {
      return false
    }

    val p: Parameters? = parameters()
    if (p == null) {
      log.error("[Feeder] failed to build context: parameters 'null'")
      return false
    } else if (!p.isFeed) {
      log.debug("[Feeder] not activated, exit")
      return false
    }

    val filesInWaitingRoomCount = filesInWaitingRoom().size
    if (forceLaunch && filesInWaitingRoomCount > 0 || filesInWaitingRoomCount >= p.getFeedThreshold()) {
      return true
    }
    log.debug("[Feeder] no files to handle, exit")
    return false
  }

  fun launchFeederInBackground(feedLoop: FeedLoop, feederProcessFinished: FeederProcessFinished? = null): Thread {
    isRunning = true
    return thread {
      try {
        run(feedLoop)
        isRunning = false
        log.info("\n[Feeder] feed loop finished successfully")
        feederProcessFinished?.apply {
          log.info("[Feeder] notify process finished listener")
          onFeederProcessFinished()
        }
      } catch (it: Throwable) {
        log.error("[Feeder] feed loop failed: '${it.message}'", it)
        isRunning = false
      }
    }
  }

  private fun run(feedLoop: FeedLoop) {
    feedLoop.loop()
    if (stopFeederWhenPossible) {
      disableFeedModeInParameters()
      stopFeederWhenPossible = false
    }
  }

  private fun disableFeedModeInParameters() {
    UpdateParametersCommand.initBuilderWith(parameters())
      .feed(false)
      .build().let { command ->
        commandBus.dispatch<CommandResponse<UpdateParametersCommand>>(command)
          .onSuccess { log.info(" parameters updated: $it") }
          .onFailure { throwable -> log.error(" error: $throwable") }
      }
  }

  private fun filesInWaitingRoom() = queryBus.dispatch<QueryResponse<List<DomainFile>>>(AllFilesInWaitingRoomQuery()).value()

  private fun parameters() = queryBus.dispatch<QueryResponse<Parameters?>>(ParametersQuery()).value()

  @Synchronized
  public override fun onEvent(event: Event) {
    if (supports(event)) {
      onNewFileCreated(event as FileCreated)
    }
  }

  override fun supports(event: Event) = event is FileCreated

  /**
   * ONLY FOR TEST PURPOSE
   */
  fun forceFeedRunningForUnitTestPurposeOnly() {
    isRunning = true
  }

  fun forceFeedLoopMockForUnitTestPurposeOnly(feedLoop: FeedLoop) {
    feedLoopMock = feedLoop
  }


  companion object {
    private val log = LoggerFactory.getLogger(FeederServiceImpl::class.java)
    private const val PERIODIC_FEEDER_MS = 60000L
  }
}
