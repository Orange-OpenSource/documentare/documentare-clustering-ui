package com.orange.documentare.clusteringui.biz.file.event;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.ClusterPutInNamedClassDTO.exampleClassName;
import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileAddedToClusterDTO.EXAMPLE_DISTANCE_TO_CENTER;
import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;

import java.time.Instant;

import com.tophe.ddd.infrastructure.event.Event;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FileAddedToCluster extends Event {

  public final String clusterId;
  public final Boolean clusterCenter;
  public final Integer distanceToCenter;
  public final Boolean automaticInclude;
  public final Boolean inMultiset;
  public final Boolean lock;
  public final String className;

  public FileAddedToCluster(String aggregateId, String clusterId, Boolean clusterCenter, Integer distanceToCenter, Boolean automaticInclude, Boolean inMultiset, Boolean lock,
    String className)
  {
    super(aggregateId);
    this.clusterId = clusterId;
    this.clusterCenter = clusterCenter;
    this.distanceToCenter = distanceToCenter;
    this.automaticInclude = automaticInclude;
    this.inMultiset = inMultiset;
    this.lock = lock;
    this.className = className;
  }

  private FileAddedToCluster(Instant date, String aggregateId, String clusterId, Boolean clusterCenter, Integer distanceToCenter, Boolean automaticInclude,
    Boolean inMultiset, Boolean lock, String className)
  {
    super(aggregateId, date);
    this.clusterId = clusterId;
    this.clusterCenter = clusterCenter;
    this.distanceToCenter = distanceToCenter;
    this.automaticInclude = automaticInclude;
    this.inMultiset = inMultiset;
    this.lock = lock;
    this.className = className;
  }

  public static FileAddedToCluster buildFromDB(Instant date, String aggregateId, String clusterId, Boolean clusterCenter, Integer distanceToCenter,
    Boolean automaticInclude, Boolean inMultiset, Boolean lock, String className)
  {
    return new FileAddedToCluster(date, aggregateId, clusterId, clusterCenter, distanceToCenter, automaticInclude, inMultiset, lock, className);
  }

  public boolean clusterCenter() {
    return clusterCenter != null && clusterCenter;
  }

  public static FileAddedToClusterBuilder builder() {
    return new FileAddedToClusterBuilder();
  }

  public static class FileAddedToClusterBuilder {

    private String aggregateId;
    private String clusterId;
    private Boolean clusterCenter;
    private Integer distanceToCenter;
    private Boolean automaticInclude;
    private Boolean inMultiset;
    private Boolean lock;
    private String className;

    public FileAddedToCluster build() {
      return new FileAddedToCluster(aggregateId, clusterId, clusterCenter, distanceToCenter, automaticInclude, inMultiset, lock, className);
    }

    public FileAddedToClusterBuilder aggregateId(String aggregateId) {
      this.aggregateId = aggregateId;
      return this;
    }

    public FileAddedToClusterBuilder clusterId(String clusterId) {
      this.clusterId = clusterId;
      return this;
    }

    public FileAddedToClusterBuilder clusterCenter(Boolean clusterCenter) {
      this.clusterCenter = clusterCenter;
      return this;
    }

    public FileAddedToClusterBuilder automaticInclude(Boolean automaticInclude) {
      this.automaticInclude = automaticInclude;
      return this;
    }

    public FileAddedToClusterBuilder inMultiset(Boolean inMultiset) {
      this.inMultiset = inMultiset;
      return this;
    }

    public FileAddedToClusterBuilder lock(Boolean lock) {
      this.lock = lock;
      return this;
    }

    public FileAddedToClusterBuilder distanceToCenter(Integer distanceToCenter) {
      this.distanceToCenter = distanceToCenter;
      return this;
    }

    public FileAddedToClusterBuilder className(String className) {
      this.className = className;
      return this;
    }
  }


  //
  // FOR TESTS
  //
  public static FileAddedToCluster example() {
    return new FileAddedToCluster(TEST_DATE, AGGREGATE_ID_EXAMPLE.toString(), exampleClusterId(AGGREGATE_ID_EXAMPLE), true,
      EXAMPLE_DISTANCE_TO_CENTER, true, true, true, exampleClassName(AGGREGATE_ID_EXAMPLE));
  }
}
