package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import java.util.*

class AddFilesInWaitingRoomCommandHandler(
  eventBus: EventBus,
  private val uuidSupplier: () -> String = defaultUUIDSupplier()
) : CommandHandler<AddFilesInWaitingRoomCommand, List<String>>(eventBus) {

  override fun doExecute(command: AddFilesInWaitingRoomCommand): AggregateAndEvents<List<String>, Collection<Event>> {
    val events: List<Event> = command.files.map { persistFile(it) }
    return AggregateAndEvents(events.map { it.aggregateId }, events)
  }

  private fun persistFile(f: AddFilesInWaitingRoomCommand.File): Event {
    val id = uuidSupplier.invoke()
    return DomainFile.createInWaitingRoom(id, f.filename, f.bytes, true)
  }

  companion object {
    @JvmStatic
    fun defaultUUIDSupplier(): () -> String = { UUID.randomUUID().toString() }
  }
}
