package com.orange.documentare.clusteringui.biz.services

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.orange.documentare.clusteringui.biz.file.query.ClusterDistancesStats
import com.orange.documentare.clusteringui.biz.parameters.domain.DistanceParameters

data class ClientHierarchyReferenceModel(val hierarchyLevels: List<ClientHierarchyReferenceModelLevel>, val distanceParameters: DistanceParameters) {
  fun serialize(): String = ObjectMapper()
    .setSerializationInclusion(JsonInclude.Include.NON_NULL)
    .writerWithDefaultPrettyPrinter()
    .writeValueAsString(this)
}

data class ClientHierarchyReferenceModelLevel(val referenceFiles: List<List<ClientHierarchyReferenceModelFile>>)

data class ClientHierarchyReferenceModelFile(
  val aggregateId: String,
  val filename: String,
  val clusterCenter: Boolean,
  val multiset: Boolean,
  val clusterDistancesStats: ClusterDistancesStats?,
  val clusterMultiHypothesisName: String?,
  val className: String?,
  val fileNamedClass: List<String>?,
  val absolutePath: String,
  val webPath: String,
  val webThumbnailPath: String
)
