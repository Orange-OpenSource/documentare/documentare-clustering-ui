package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler
import java.util.Comparator.comparing

class ComputeDistancesQueryHandler(private val projections: Projections) : QueryHandler<ComputeDistancesQuery, ComputeDistancesQueryView>() {

  override fun doExecute(query: ComputeDistancesQuery): ComputeDistancesQueryView {

    val filesProjection = projections.files

    val multiSets = filesProjection.ids()
      .asSequence()
      .filter { filesProjection[it].inMultiset() }
      .map { filesProjection[it] }

    val filesGroupedByClusterId = multiSets
      .sortedWith(comparing(DomainFile::clusterCenter).reversed())
      .sortedWith(comparing { obj: DomainFile -> obj.clusterId!! })
      .distinct()
      .groupBy { it.clusterId!! }

    val files = query.fileIds.map { filesProjection[it] }

    return ComputeDistancesQueryView(filesGroupedByClusterId, files)
  }
}
