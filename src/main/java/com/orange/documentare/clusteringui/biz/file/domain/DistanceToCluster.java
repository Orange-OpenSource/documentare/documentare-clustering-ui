package com.orange.documentare.clusteringui.biz.file.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

// TODO: add DistanceToClusterDTO to avoid infra in domain (jackson annotation)
public record DistanceToCluster(@JsonProperty("distance") int distance, @JsonProperty("clusterId") String clusterId) {
}
