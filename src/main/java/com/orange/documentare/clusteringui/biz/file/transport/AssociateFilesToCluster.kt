package com.orange.documentare.clusteringui.biz.file.transport

import com.orange.documentare.clusteringui.biz.file.command.AddDistancesToClustersCommand
import com.orange.documentare.clusteringui.biz.file.command.AddFilesToClustersCommand
import com.orange.documentare.clusteringui.biz.file.domain.AssociateFile
import com.orange.documentare.clusteringui.biz.file.query.AssociateFileQuery
import com.orange.documentare.clusteringui.biz.file.query.AssociateFileView
import com.orange.documentare.clusteringui.biz.file.query.SelectedFileView
import com.orange.documentare.clusteringui.infrastructure.security.Authent
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import jakarta.servlet.http.HttpServletRequest
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody
import java.security.Principal

@Controller
class AssociateFilesToCluster {
  private val log = LoggerFactory.getLogger(AssociateFilesToCluster::class.java)

  @Autowired
  lateinit var commandBus: CommandBus

  @Autowired
  lateinit var queryBus: QueryBus

  @RequestMapping(value = ["/files"], params = ["action=associateToCluster"])
  fun associateFilesToCluster(req: HttpServletRequest, principal: Principal?, model: Model): String {
    val prefix = "GET [/files/ action=associateToCluster]"
    model.addAttribute("title", "Associate Files To Cluster")
    Authent.addAuthenticatedModel(model, principal)

    val fileIds: Array<String> = req.parameterMap["idToSort"] as Array<String>

    log.info("$prefix $fileIds")

    val associateFileViews = listOf(*fileIds).map { queryBus.dispatch<QueryResponse<AssociateFileView>>(AssociateFileQuery(it)).value() }

    cleanupRemovedClustersDistances(associateFileViews)

    val selectedFileView = SelectedFileView(null, null, null)

    model.addAttribute("associateFileViews", associateFileViews)
    model.addAttribute("selectedFileView", selectedFileView)
    return "associate-files-to-cluster"
  }

  private fun cleanupRemovedClustersDistances(associateFileViews: List<AssociateFileView>) {
    for (a in associateFileViews) {
      if (a.fileDistanceToCluster != null) {
        for (fileKey in a.fileDistanceToCluster.keys) {
          val distancesToClustersCenters = a.fileDistanceToCluster[fileKey]
          if (distancesToClustersCenters!!.isNotEmpty()) {
            commandBus.dispatch<CommandResponse<Void>>(AddDistancesToClustersCommand(fileKey, distancesToClustersCenters))
              .onFailure { throwable: Any? -> log.error(String.format("Failed to save distances to file id '%s': '%s'"), fileKey, distancesToClustersCenters, throwable) }
          } else {
            commandBus.dispatch<CommandResponse<Void>>(AddDistancesToClustersCommand(fileKey, emptyList()))
              .onFailure { throwable: Any? -> log.error(String.format("Failed to save distances to file id '%s': '%s'"), fileKey, distancesToClustersCenters, throwable) }
          }
        }
      }
    }
  }

  @Secured("ROLE_USER")
  @ResponseBody
  @RequestMapping(value = ["/api/add-files-to-cluster"], method = [RequestMethod.POST])
  fun apiAddFilesToCluster(@RequestBody associateFiles: List<AssociateFile>?): String {
    if (associateFiles == null) {
      return "redirect:/"
    }
    val prefix = "ADD_TO_CLUSTER [/add-files-to-cluster/]"
    commandBus.dispatch<CommandResponse<Void>>(AddFilesToClustersCommand(associateFiles))
      .onSuccess { log.info(prefix) }
      .onFailure { throwable: Any -> log.error("$prefix error: $throwable") }
    return "redirect:/"
  }
}
