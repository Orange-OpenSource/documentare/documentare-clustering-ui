package com.orange.documentare.clusteringui.biz.file.transport

import com.orange.documentare.clusteringui.biz.file.query.DisplayHomeFilesQuery
import com.orange.documentare.clusteringui.biz.file.query.DisplayHomeFilesView
import com.orange.documentare.clusteringui.biz.file.query.SelectedFileView
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQuery
import com.orange.documentare.clusteringui.infrastructure.security.Authent
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import java.security.Principal

@Controller
class WaitingRoom(private val queryBus: QueryBus) {
  private val log = LoggerFactory.getLogger(WaitingRoom::class.java)

  @RequestMapping("/waitingRoom")
  fun index(principal: Principal?, model: Model): String {
    log.info("GET [/WaitingRoom]")
    model.addAttribute("title", "Waiting Room")
    Authent.addAuthenticatedModel(model, principal)
    val responseParameters = queryBus.dispatch<QueryResponse<Parameters?>>(ParametersQuery())
    val parameters: Parameters
    parameters = if (responseParameters.failure() || responseParameters.value() == null) {
      Parameters.defaults()
    } else {
      responseParameters.value()!!
    }
    val response = queryBus.dispatch<QueryResponse<DisplayHomeFilesView>>(DisplayHomeFilesQuery())
    response
      .onSuccess { filesView: DisplayHomeFilesView -> fillModel(filesView, parameters.isFeed, model) }
      .onFailure { throwable: Throwable -> log.error("GET [/WaitingRoom] error: " + throwable.localizedMessage) }
    return "waiting-room"
  }

  private fun fillModel(filesView: DisplayHomeFilesView, feed: Boolean, model: Model) {
    val numberFilesInWaitingRoom = "Salle d'attente : " + ' ' + filesView.inWaitingRoom!!.size
    model.addAttribute("nofiles", filesView.noFiles)
    model.addAttribute("filesInWaitingRoom", filesView.inWaitingRoom)
    model.addAttribute("numberFilesInWaitingRoom", numberFilesInWaitingRoom)
    model.addAttribute("filesWithoutClusterId", filesView.notInClusters)
    model.addAttribute("waitingForClusteringResult", filesView.clusteringInProgress)
    model.addAttribute("computeDistanceInProgress", filesView.computeDistancesInProgress)
    model.addAttribute("waitingForFeedResult", filesView.feedInProgress)
    val selectedFileViewInCluster = SelectedFileView(null, null, null)
    model.addAttribute("selectedFileViewInCluster", selectedFileViewInCluster)
    val selectedFileView = SelectedFileView(null, null, null)
    model.addAttribute("selectedFileView", selectedFileView)
    if (filesView.computeDistancesInProgress || filesView.clusteringInProgress || filesView.feedInProgress) {
      model.addAttribute("classAlbum", "album-red-ligth text-muted")
    } else {
      model.addAttribute("classAlbum", "album text-muted")
    }
    model.addAttribute("commandTM", commandTM())
    model.addAttribute("feedActive", feed)
    model.addAttribute("askFeedToStop", filesView.feedStop)
  }

}