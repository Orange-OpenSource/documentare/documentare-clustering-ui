package com.orange.documentare.clusteringui.biz.parameters.command;

import static java.util.Collections.emptyList;

import java.util.Collection;
import java.util.List;

import com.orange.documentare.clusteringui.AggregateAndEvents;
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters;
import com.orange.documentare.clusteringui.biz.parameters.persistence.ParametersRepository;
import com.tophe.ddd.commands.CommandHandler;
import com.tophe.ddd.infrastructure.event.Event;


public class UpdateParametersCommandHandler extends CommandHandler<UpdateParametersCommand, Void> {

  private final ParametersRepository parametersRepository;

  public UpdateParametersCommandHandler(ParametersRepository parametersRepository) {
    this.parametersRepository = parametersRepository;
  }

  @Override
  public AggregateAndEvents<Void, Collection<Event>> doExecute(UpdateParametersCommand cmd) {
    cmd.validate();

    Parameters parameters = Parameters.builder()
      .rawConverter(cmd.rawConverter())
      .pCount(cmd.pCount())
      .suffixArrayAlgorithm(cmd.suffixArrayAlgorithm())
      .acutSdFactor(cmd.acutSdFactor())
      .qcutSdFactor(cmd.qcutSdFactor())
      .scutSdFactor(cmd.scutSdFactor())
      .ccutPercentile(cmd.ccutPercentile())
      .knnThreshold(cmd.knnThreshold())
      .sloop(cmd.sloop())
      .enroll(cmd.enroll())
      .enrollAcutSdFactor(cmd.enrollAcutSdFactor())
      .enrollQcutSdFactor(cmd.enrollQcutSdFactor())
      .enrollScutSdFactor(cmd.enrollScutSdFactor())
      .enrollCcutPercentile(cmd.enrollCcutPercentile())
      .enrollKnnThreshold(cmd.enrollKnnThreshold())
      .thresholdFactor(cmd.thresholdFactor())
      .feed(cmd.feed())
      .feedThreshold(cmd.feedThreshold())
      .minMultisetThreshold(cmd.minMultisetThreshold())
      .maxMultisetThreshold(cmd.maxMultisetThreshold())
      .build();

    parametersRepository.deleteAll();
    parametersRepository.insert(List.of(parameters));

    return AggregateAndEvents.withOnlyEvents(emptyList());
  }
}
