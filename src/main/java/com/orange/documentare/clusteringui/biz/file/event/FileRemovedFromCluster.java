package com.orange.documentare.clusteringui.biz.file.event;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;

import java.time.Instant;

import com.tophe.ddd.infrastructure.event.Event;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FileRemovedFromCluster extends Event {

  public final String clusterId;
  public final Boolean inWaitingRoom;

  public FileRemovedFromCluster(String aggregateId, String clusterId, Boolean inWaitingRoom) {
    super(aggregateId);
    this.clusterId = clusterId;
    this.inWaitingRoom = inWaitingRoom;
  }

  private FileRemovedFromCluster(Instant date, String aggregateId, String clusterId, Boolean inWaitingRoom) {
    super(aggregateId, date);
    this.clusterId = clusterId;
    this.inWaitingRoom = inWaitingRoom;
  }

  public static FileRemovedFromCluster buildFromDB(Instant date, String aggregateId, String clusterId, Boolean inWaitingRoom) {
    return new FileRemovedFromCluster(date, aggregateId, clusterId, inWaitingRoom);
  }


  //
  // FOR TESTS
  //
  public static FileRemovedFromCluster example() {
    return new FileRemovedFromCluster(TEST_DATE, AGGREGATE_ID_EXAMPLE.toString(), exampleClusterId(AGGREGATE_ID_EXAMPLE), true);
  }
}
