package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler

class AllClustersQueryHandler(private val projections: Projections) : QueryHandler<AllClustersQuery, Map<String, List<String>>>() {
  override fun doExecute(query: AllClustersQuery) = projections.clusters.clusters()
}