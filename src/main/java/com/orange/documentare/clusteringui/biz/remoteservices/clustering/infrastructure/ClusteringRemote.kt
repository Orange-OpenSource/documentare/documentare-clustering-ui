package com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure

/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import com.orange.documentare.clusteringui.Couple
import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.remoteservices.infrastructure.BytesDataUI
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.orange.documentare.core.model.ref.clustering.Clustering

open class ClusteringRemote(private val embeddedComputationServer: EmbeddedComputationServer) : ClusteringRemoteApi {

  init {
    if (embeddedComputationServer.enabled) {
      remoteSimdocServersUrls = listOf("embedded computation server")
    }
  }

  override fun createCluster(parameters: Parameters, files: List<DomainFile>): TryOrCatch<Clustering> =
    genericCalculateClustering(consolidateClusterParams(parameters), wrapFiles(files)).values.first()

  override fun recalculateClusters(parameters: Parameters, clustersFiles: List<Couple<PreviousClusterId, List<DomainFile>>>): Map<PreviousClusterId, TryOrCatch<Clustering>> =
    genericCalculateClustering(consolidateClusterParams(parameters), clustersFiles)

  override fun calculateClustering(parameters: Parameters, files: List<DomainFile>): TryOrCatch<Clustering> =
    genericCalculateClustering(parameters, wrapFiles(files)).values.first()

  private fun wrapFiles(files: List<DomainFile>) =
    listOf(Couple(PreviousClusterId.none(), files))

  open fun genericCalculateClustering(parameters: Parameters, clustersFiles: List<Couple<PreviousClusterId, List<DomainFile>>>): Map<PreviousClusterId, TryOrCatch<Clustering>> {
    remoteSimdocServersUrls?.let { urls ->
      val clusteringRequestsUI: List<Couple<PreviousClusterId, ClusteringRequestUI>> = clustersFiles.map {
        Couple(it.first, buildClusteringRequest(parameters, it.second))
      }
      return doRequest(urls, parameters, clusteringRequestsUI)
    }
    throw IllegalStateException("remote simdoc servers urls are not set")
  }

  open fun doRequest(
    remoteSimdocServersUrls: List<String>,
    parameters: Parameters,
    clusteringRequestsUI: List<Couple<PreviousClusterId, ClusteringRequestUI>>
  ): Map<PreviousClusterId, TryOrCatch<Clustering>> =
    RemoteClustering(embeddedComputationServer).request(remoteSimdocServersUrls, clusteringRequestsUI)

  open fun bytesDataOf(files: List<DomainFile>): Array<BytesDataUI> =
    files
      .map { file -> BytesDataUI(listOf(file.aggregateId), listOf(FileIO.filePathOf(file))) }
      .toTypedArray()


  fun consolidateClusterParams(parameters: Parameters): Parameters = Parameters.consolidateCluster(parameters.pCount, parameters.suffixArrayAlgorithm)


  fun buildClusteringRequest(params: Parameters, files: List<DomainFile>): ClusteringRequestUI {

    val bytesData = bytesDataOf(files)

    val clusteringRequestUIBuilder = ClusteringRequestUI.builder()
      .bytesData(bytesData)
      .outputDirectory(FileIO.outputDirectory())

    clusteringRequestUIBuilder.rawConverter(params.isRawConverter)

    if (params.pCount != null) {
      clusteringRequestUIBuilder.pCount(params.pCount)
    }

    if (params.acutSdFactor != null) {
      clusteringRequestUIBuilder.acut(params.acutSdFactor)
    }

    if (params.ccutPercentile != null) {
      clusteringRequestUIBuilder.ccut(params.ccutPercentile)
    }

    if (params.qcutSdFactor != null) {
      clusteringRequestUIBuilder.qcut(params.qcutSdFactor)
    }

    if (params.scutSdFactor != null) {
      clusteringRequestUIBuilder.scut(params.scutSdFactor)
    }

    if (params.knnThreshold != null) {
      clusteringRequestUIBuilder.knnThreshold(params.knnThreshold)
    }

    if (params.sloop != null && params.sloop == true) {
      clusteringRequestUIBuilder.sloop()
    }

    if (params.consolidateCluster != null && params.consolidateCluster == true) {
      clusteringRequestUIBuilder.consolidateCluster()
    }

    if (params.suffixArrayAlgorithm != null) {
      clusteringRequestUIBuilder.suffixArrayAlgorithm(params.suffixArrayAlgorithm)
    }

    if (params.enroll != null && params.enroll == true) {
      buildClusteringRequestAddEnrollParameters(clusteringRequestUIBuilder, params)
    }

    return clusteringRequestUIBuilder.build()
  }

  private fun buildClusteringRequestAddEnrollParameters(
    clusteringRequestUIBuilder: ClusteringRequestUI.ClusteringRequestBuilder,
    params: Parameters
  ) {
    clusteringRequestUIBuilder.enroll()

    if (params.enrollAcutSdFactor != null) {
      clusteringRequestUIBuilder.enrollAcut(params.enrollAcutSdFactor)
    }

    if (params.enrollCcutPercentile != null) {
      clusteringRequestUIBuilder.enrollCcut(params.enrollCcutPercentile)
    }

    if (params.enrollQcutSdFactor != null) {
      clusteringRequestUIBuilder.enrollQcut(params.enrollQcutSdFactor)
    }

    if (params.enrollScutSdFactor != null) {
      clusteringRequestUIBuilder.enrollScut(params.enrollScutSdFactor)
    }

    if (params.enrollKnnThreshold != null) {
      clusteringRequestUIBuilder.enrollK(params.enrollKnnThreshold)
    }
  }


  companion object {
    private var remoteSimdocServersUrls: List<String>? = null

    fun initRemoteSimdocServerForClustering(remoteSimdocServersUrls: List<String>) {
      ClusteringRemote.remoteSimdocServersUrls = remoteSimdocServersUrls
    }
  }
}
