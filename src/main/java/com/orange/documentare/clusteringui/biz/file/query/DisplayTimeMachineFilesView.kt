package com.orange.documentare.clusteringui.biz.file.query

data class DisplayTimeMachineFilesView(
  @JvmField
  val noFiles: Boolean,
  @JvmField
  val inWaitingRoom: List<FileUIView>,
  @JvmField
  val notInClusters: List<FileUIView>,
  @JvmField
  val orderedByClusterId: Map<String, ClusterUIView>,
  @JvmField
  val mapClassViews: Map<String, Map<String, ClusterUIView>>,
  @JvmField
  val duplicate: List<FileUIView>
)
