package com.orange.documentare.clusteringui.biz.parameters.query;

import com.tophe.ddd.queries.Query;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class ParametersQuery implements Query {
}
