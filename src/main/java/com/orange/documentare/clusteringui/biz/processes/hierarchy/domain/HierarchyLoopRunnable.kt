package com.orange.documentare.clusteringui.biz.processes.hierarchy.domain

interface HierarchyLoopRunnable {
  fun run()
}
