package com.orange.documentare.clusteringui.biz.processes.feed.domain.events

import com.orange.documentare.clusteringui.biz.file.domain.FileId

data class MultiHypothesisClustersBroke(val filesIdMovedToWaitingRoom: List<FileId>, val metadataBalancingFinished: Boolean) : FeedLoopEvent() {
  override fun displayInternalHtml() =
    if (filesIdMovedToWaitingRoom.isEmpty()) {
      "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; no files moved from multi-hypothesis clusters to waiting room"
    } else {
      "&nbsp;🔨&nbsp;&nbsp; <strong>${filesIdMovedToWaitingRoom.size}</strong> files moved from multi-hypothesis clusters to waiting room"
    }

  override fun displayInternalRaw() =
    if (filesIdMovedToWaitingRoom.isEmpty()) {
      "      no files moved from multi-hypothesis clusters to waiting room"
    } else {
      " 🔨   ${filesIdMovedToWaitingRoom.size} files moved from multi-hypothesis clusters to waiting room"
    }
}
