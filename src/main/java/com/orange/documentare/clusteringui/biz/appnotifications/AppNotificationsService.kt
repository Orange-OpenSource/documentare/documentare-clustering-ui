package com.orange.documentare.clusteringui.biz.appnotifications

import org.springframework.stereotype.Service

typealias AppNotificationListener = (appNotifification: AppNotification) -> Unit

@Service
class AppNotificationsService : AppNotifications {
  private val listeners = mutableListOf<AppNotificationListener>()

  @Synchronized
  override fun notify(appNotification: AppNotification) {
    listeners.forEach { it(appNotification) }
  }

  @Synchronized
  override fun register(appNotificationsListener: AppNotificationListener) {
    listeners.add(appNotificationsListener)
  }

  @Synchronized
  override fun unregister(appNotificationsListener: (appNotifification: AppNotification) -> Unit) {
    listeners.remove(appNotificationsListener)
  }
}
