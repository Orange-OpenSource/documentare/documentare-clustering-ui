package com.orange.documentare.clusteringui.biz.file.transport;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.orange.documentare.clusteringui.infrastructure.security.Authent;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class AddFiles {

  @RequestMapping("/add-files")
  public String addFiles(Principal principal, Model model) {
    log.info("GET [/add-files]");

    model.addAttribute("title", "AddFile");
    Authent.addAuthenticatedModel(model, principal);

    return "add-files";
  }
}
