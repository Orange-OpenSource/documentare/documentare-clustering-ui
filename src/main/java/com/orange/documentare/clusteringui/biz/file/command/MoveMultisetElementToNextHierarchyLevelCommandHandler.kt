package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.orange.documentare.clusteringui.biz.file.event.MultisetElementMovedToNextHierarchyLevel
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus

class MoveMultisetElementToNextHierarchyLevelCommandHandler(eventBus: EventBus) :
  CommandHandler<MoveMultisetElementToNextHierarchyLevelCommand, Void>(eventBus) {

  override fun doExecute(command: MoveMultisetElementToNextHierarchyLevelCommand): AggregateAndEvents<Void?, Collection<Event>> {
    val events: List<Event> = command.clusterCentersAndMultiSetsIds
      .map {
        MultisetElementMovedToNextHierarchyLevel(
          it,
          command.nextHierarchyLevel
        )
      }
    return AggregateAndEvents.withOnlyEvents(events)
  }
}
