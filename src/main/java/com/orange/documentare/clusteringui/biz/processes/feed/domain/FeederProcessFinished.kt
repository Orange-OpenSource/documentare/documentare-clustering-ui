package com.orange.documentare.clusteringui.biz.processes.feed.domain

interface FeederProcessFinished {
  fun onFeederProcessFinished()
}
