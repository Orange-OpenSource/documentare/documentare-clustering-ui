package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.persistence.EventRepository

class ResetFileInfoCommandHandler(private val eventRepository: EventRepository, eventBus: EventBus) : CommandHandler<ResetFileInfoCommand, Void>(eventBus) {

  override fun doExecute(command: ResetFileInfoCommand): AggregateAndEvents<Void?, Collection<Event>> {
    val files: List<DomainFile> = command.filesIds
      .chunked(100)
      .mapNotNull { eventRepository.load(it) }
      .flatten()

    val reset = files
      .filter { !it.deleted() }
      .map { it.resetFileInfo() }

    return AggregateAndEvents.withOnlyEvents(reset)
  }
}