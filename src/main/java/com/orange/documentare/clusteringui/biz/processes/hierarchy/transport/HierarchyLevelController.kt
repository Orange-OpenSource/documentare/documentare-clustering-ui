package com.orange.documentare.clusteringui.biz.processes.hierarchy.transport

import com.orange.documentare.clusteringui.biz.file.query.DisplayHierarchyLevelFilesQuery
import com.orange.documentare.clusteringui.biz.file.query.DisplayHierarchyLevelFilesView
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederServiceImpl
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.ClusteringServiceImpl
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceServiceImpl
import com.orange.documentare.clusteringui.biz.services.MessageByLocaleService
import com.orange.documentare.clusteringui.infrastructure.security.Authent
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import java.security.Principal

@Controller
class HierarchyLevelController {
  private val log = LoggerFactory.getLogger(HierarchyLevelController::class.java)

  @Autowired
  lateinit var commandBus: CommandBus

  @Autowired
  lateinit var queryBus: QueryBus

  @Autowired
  lateinit var clusteringService: ClusteringServiceImpl

  @Autowired
  lateinit var distanceService: DistanceServiceImpl

  @Autowired
  lateinit var feederService: FeederServiceImpl

  @Autowired
  lateinit var messageByLocaleService: MessageByLocaleService

  @RequestMapping("/hierarchy-level/{hierarchyLevelIndex}")
  fun index(principal: Principal, model: Model, @PathVariable("hierarchyLevelIndex") hierarchyLevelIndex: Int): String {
    log.debug("GET [/hierarchy-level]")
    model.addAttribute("title", "HierarchyLevel")
    Authent.addAuthenticatedModel(model, principal)

    val response = queryBus.dispatch<QueryResponse<DisplayHierarchyLevelFilesView>>(DisplayHierarchyLevelFilesQuery(hierarchyLevelIndex))
    response
      .onSuccess { filesView: DisplayHierarchyLevelFilesView -> fillModel(filesView, hierarchyLevelIndex, model) }
      .onFailure { throwable: Throwable -> log.error("GET [/] error: " + throwable.localizedMessage) }
    return "hierarchy-level"
  }

  private fun fillModel(filesView: DisplayHierarchyLevelFilesView, hierarchyLevelIndex: Int, model: Model) {
    model.addAttribute("levelCount", filesView.hierarchyLevelCount)
    model.addAttribute("currentLevel", hierarchyLevelIndex)
    model.addAttribute("nofiles", filesView.noFiles)
    model.addAttribute("filesWithoutClusterId", filesView.notInClusters)
    model.addAttribute("mapClassViews", filesView.mapClassViews)
    model.addAttribute("classAlbum", "album text-muted")
  }

}
