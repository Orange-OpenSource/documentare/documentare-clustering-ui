package com.orange.documentare.clusteringui.biz.parameters.persistence;

import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters;
import com.tophe.ddd.infrastructure.persistence.RepoPage;
import com.tophe.ddd.infrastructure.persistence.RepoPageable;
import com.tophe.ddd.infrastructure.persistence.Repository;

import kotlin.NotImplementedError;

public interface ParametersRepository extends Repository<Parameters> {

  default RepoPage<Parameters> findAllInPage(RepoPageable repoPageable) {
    throw new NotImplementedError();
  }
}
