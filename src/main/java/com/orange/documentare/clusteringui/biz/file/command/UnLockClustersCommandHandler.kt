package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.persistence.EventRepository


class UnLockClustersCommandHandler(private val eventRepository: EventRepository, eventBus: EventBus) : CommandHandler<UnLockClustersCommand, Void>(eventBus) {

  override fun doExecute(command: UnLockClustersCommand): AggregateAndEvents<Void?, Collection<Event>> {
    val files: List<DomainFile> = command.filesIds.mapNotNull { loadFile(it) }
    val unlockClusters = files
      .filter { it.clusterCenter() && it.locked() }
      .map { it.tagClusterUnLocked() }

    return AggregateAndEvents.withOnlyEvents(unlockClusters)
  }

  private fun loadFile(fileId: String): DomainFile? = eventRepository.load(fileId)
}