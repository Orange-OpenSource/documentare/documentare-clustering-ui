package com.orange.documentare.clusteringui.biz.file.command

import com.tophe.ddd.commands.Command

data class DuplicateFilesCommand(val filesIds: List<String>) : Command