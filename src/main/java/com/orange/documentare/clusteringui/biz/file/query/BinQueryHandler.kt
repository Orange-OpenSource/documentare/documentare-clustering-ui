package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler

class BinQueryHandler(private val projections: Projections) : QueryHandler<BinQuery, BinView>() {

  override fun doExecute(query: BinQuery): BinView {
    val fileUIViews = projections.files.ids()
      .filter { projections.files[it].duplicated() }
      .map { projections.files[it] }
      .map { FileUIView.from(it) }

    return BinView(fileUIViews)
  }
}
