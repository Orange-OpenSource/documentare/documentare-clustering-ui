package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.MultisetClustersBroke
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.MultisetThresholdReset
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.queries.QueryBus

open class DecrementMultisetThresholdAndBreakClusters(
  commandBus: CommandBus,
  queryBus: QueryBus,
  private var minMultiSetThreshold: Int,
  private val breakClusters: BreakOutOfThresholdNotLockedClusters = BreakOutOfThresholdNotLockedClusters(commandBus, queryBus),
  private val notLockedClusters: NotLockedClusters = NotLockedClusters(queryBus)
) {

  private var currentMultiSetThreshold = MULTISET_THRESHOLD_MAX_VALUE

  open fun checkIfWeNeedToBreakClusters(): MultisetClustersBroke {
    val maxMultisetOnAllClustersNotLocked = notLockedClusters.countMaxMultiset()
    currentMultiSetThreshold = maxMultisetOnAllClustersNotLocked - 1
    return if (!hasReachedMinThreshold()) {
      val brokeClustersCount = breakClusters.breakIfNumberOfMultisetIsGreaterThanMultisetThreshold(currentMultiSetThreshold)
      MultisetClustersBroke(brokeClustersCount, currentMultiSetThreshold, maxMultisetOnAllClustersNotLocked)
    } else {
      MultisetClustersBroke(0, currentMultiSetThreshold, maxMultisetOnAllClustersNotLocked)
    }
  }

  open fun reset(): MultisetThresholdReset {
    currentMultiSetThreshold = MULTISET_THRESHOLD_MAX_VALUE
    return MultisetThresholdReset(currentMultiSetThreshold)
  }

  open fun hasReachedMinThreshold() = currentMultiSetThreshold < minMultiSetThreshold

  open fun updateMinMultisetThresholdAtLoopStart(minThreshold: Int): Int? {
    if (minMultiSetThreshold != minThreshold) {
      minMultiSetThreshold = minThreshold
      return minMultiSetThreshold
    }
    return null
  }

  companion object {
    private const val MULTISET_THRESHOLD_MAX_VALUE = 9999
  }
}

