package com.orange.documentare.clusteringui.biz.processes.feed.domain.events

import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederPhases
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederPhases.Phase.MetadataBalancingStep

data class FeederPhaseUpdated(val updateType: FeederPhases.UpdateType, val phase: FeederPhases.Phase, val metadataBalancingStep: MetadataBalancingStep? = null) : FeedLoopEvent() {
  override fun displayInternalHtml() =
    "❗ [$updateType] Feeder phase is now <strong>$phase</strong>" + if (metadataBalancingStep == null) "" else " (step <strong>$metadataBalancingStep</strong>)"

  override fun displayInternalRaw() = "❗ [$updateType] Feeder phase is now $phase" + if (metadataBalancingStep == null) "" else " (step $metadataBalancingStep)"
}
