package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.orange.documentare.clusteringui.biz.file.command.ResetFileInfoCommand
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.SelectedFilesQuery
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory

internal data class RecalculateClustersThenComputeDistances(
  val clusteringInfra: ClusteringInfra
) {
  private val log: Logger = LoggerFactory.getLogger(javaClass)

  fun doAsync(parameters: Parameters, clustersModified: Map<String, List<DomainFile>>, fileIdsToSort: List<String>) {
    ClusteringAsyncTreatment(clusteringInfra.inProgress).start {
      recalculateClustersThenComputeDistancesSync(parameters, clustersModified, fileIdsToSort)
    }
  }

  private fun recalculateClustersThenComputeDistancesSync(parameters: Parameters, clustersModified: Map<String, List<DomainFile>>, fileIdsToSort: List<String>) {
    if (clustersModified.isEmpty()) {
      computeDistancesWithUpdatedClusters(clusteringInfra, parameters, fileIdsToSort)
      return
    }

    ComputeClusteringOnModifiedClusters().doSync(clusteringInfra, parameters, clustersModified)

    val query = clusteringInfra.queryBus.dispatch<QueryResponse<List<DomainFile>>>(SelectedFilesQuery(fileIdsToSort))
    if (query.value().isNotEmpty()) {
      val file = query.value().filter { f -> !f.deleted() }
      val fileIds = DomainFile.idsOf(file)
      if (fileIds.isNotEmpty()) {
        computeDistancesWithUpdatedClusters(clusteringInfra, parameters, fileIds)
      }
    }
  }

  private fun computeDistancesWithUpdatedClusters(clusteringInfra: ClusteringInfra, parameters: Parameters, fileIdsToSort: List<String>) {
    log.info("\t[Recalculate] compute distances with updated clusters, files ids '$fileIdsToSort'")
    (clusteringInfra.commandBus.dispatch<CommandResponse<Unit>>(ResetFileInfoCommand(fileIdsToSort)))
      .onFailure { log.error("Failed to reset file info '$fileIdsToSort' before computing distance", it) }
      .onSuccess { clusteringInfra.distanceService.computeDistancesOf(parameters, fileIdsToSort) }
  }
}