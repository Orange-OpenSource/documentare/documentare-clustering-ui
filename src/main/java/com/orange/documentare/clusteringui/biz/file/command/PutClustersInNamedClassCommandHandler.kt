package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.persistence.EventRepository


class PutClustersInNamedClassCommandHandler(private val eventRepository: EventRepository, eventBus: EventBus) : CommandHandler<PutClustersInNamedClassCommand, Void>(eventBus) {

  override fun doExecute(command: PutClustersInNamedClassCommand): AggregateAndEvents<Void?, Collection<Event>> {
    val events = command.selectedFileIdsToName
      .mapNotNull { fileId -> loadFile(fileId) }
      .map { it.clusterNamed(command.name) }

    return AggregateAndEvents.withOnlyEvents(events)
  }

  private fun loadFile(fileId: String): DomainFile? = eventRepository.load(fileId)
}