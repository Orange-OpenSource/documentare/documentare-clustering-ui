package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler

class AllFilesToSortQueryHandler(private val projections: Projections) : QueryHandler<AllFilesToSortQuery, List<DomainFile>>() {

  override fun doExecute(query: AllFilesToSortQuery): List<DomainFile> =
    projections.clusters.filesNotInClusters()
      .map { id: String -> projections.files[id] }
      .filter { file: DomainFile -> !file.isInWaitingRoom() }
}