/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure

import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.tasksdispatcher.ApiWrapper
import com.orange.documentare.tasksdispatcher.TasksDispatcher
import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicReference
import java.util.function.BiConsumer

class RemoteDistance @JvmOverloads constructor(
  private val changeListener: TasksDispatcher.ChangeListener? = null,
  private val embeddedComputationServer: EmbeddedComputationServer
) : RemoteDistancePort {
  private val log = LoggerFactory.getLogger(RemoteDistance::class.java)

  override fun request(urls: List<String>, distancesRequestUIS: List<DistancesRequestUI>): TryOrCatch<Map<DistancesRequestUI, DistancesRequestResult>> =
    TryOrCatch.of { doRequest(urls, distancesRequestUIS) }

  private fun doRequest(urls: List<String>, distancesRequestUIS: List<DistancesRequestUI>): Map<DistancesRequestUI, DistancesRequestResult> {
    val apiWrappers = distancesRequestUIS.map { DistanceRequestWrapper(it, embeddedComputationServer) }

    val results = AtomicReference<MutableMap<DistancesRequestUI, DistancesRequestResult>>()
    results.set(HashMap())

    val resultObserver = BiConsumer { apiWrapper: ApiWrapper<DistanceRequestWrapperNetworkApi, DistancesRequestResult>, res: DistancesRequestResult ->
      val updater = ResultUpdate((apiWrapper as DistanceRequestWrapper).distancesRequestUI, res)
      results.getAndUpdate { currentResults: MutableMap<DistancesRequestUI, DistancesRequestResult> -> updater.update(currentResults) }
    }

    log.info("\tDispatch {} distances computation requests", distancesRequestUIS.size)

    TasksDispatcher(apiWrappers, urls, resultObserver, changeListener, if (embeddedComputationServer.enabled) embeddedComputationServer.tasksApiClient else null)
      .distributeTasks()

    return results.get()
  }

  private class ResultUpdate(
    private val distancesRequestUI: DistancesRequestUI,
    private val res: DistancesRequestResult
  ) {

    fun update(currentResults: MutableMap<DistancesRequestUI, DistancesRequestResult>): MutableMap<DistancesRequestUI, DistancesRequestResult> =
      currentResults.apply { this[distancesRequestUI] = res }
  }
}
