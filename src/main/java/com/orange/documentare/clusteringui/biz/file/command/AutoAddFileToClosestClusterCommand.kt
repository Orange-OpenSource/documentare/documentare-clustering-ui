package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster
import com.tophe.ddd.commands.Command

data class AutoAddFileToClosestClusterCommand(
  val fileId: String,
  val distanceToCluster: DistanceToCluster,
  val closestClusterFileIds: List<String>,
  val maxMultisetThreshold: Int
) : Command
