package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler

class AllFilesToRemoveInClusterQueryHandler(private val projections: Projections) : QueryHandler<AllFilesToRemoveInClusterQuery, Map<String, List<DomainFile>>>() {

  override fun doExecute(query: AllFilesToRemoveInClusterQuery): Map<String, List<DomainFile>> {
    val files = query.fileIds.map { projections.files[it] }
    val clusterIds = files.map { it.clusterId!! }
    return clusterIds
      .associateWith { clusterId ->
        (projections.clusters.clusters()[clusterId]
          ?: error("cluster $clusterId was expected"))
          .map { fileId: String -> projections.files.get(fileId) }
      }
  }
}