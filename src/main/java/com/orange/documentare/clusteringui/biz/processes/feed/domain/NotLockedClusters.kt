package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.AllClustersQuery
import com.orange.documentare.clusteringui.biz.file.query.SelectedFilesQuery
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory

open class NotLockedClusters(private val queryBus: QueryBus) {

  open fun countMaxMultiset(): Int {
    var maxMultiset = 0
    queryAllClusters()
      .onFailure { log.error("Failed to retrieve all clusters", it) }
      .onSuccess { allClusters ->
        allClusters.forEach { (_, filesIdsInCluster) ->
          maxMultiset = countMaxMultiset(filesIdsInCluster, maxMultiset)
        }
      }
    return maxMultiset
  }

  private fun countMaxMultiset(filesIdsInCluster: List<String>, multisetThreshold: Int): Int {
    var maxMultiset = multisetThreshold
    querySelectedFiles(filesIdsInCluster)
      .onFailure { log.error("Failed to retrieve selected files '$filesIdsInCluster'", it) }
      .onSuccess { filesInCluster ->
        val multisetSize = filesInCluster.count { it.inMultiset() }
        val clusterIsLocked = filesInCluster.any { it.locked() }

        if (multisetSize > maxMultiset && !clusterIsLocked) {
          maxMultiset = multisetSize
        }
      }
    return maxMultiset
  }

  private fun queryAllClusters() = queryBus.dispatch<QueryResponse<Map<String, List<String>>>>(AllClustersQuery())

  private fun querySelectedFiles(filesIdsInCluster: List<String>) =
    queryBus.dispatch<QueryResponse<List<DomainFile>>>(SelectedFilesQuery(filesIdsInCluster))

  companion object {
    private val log: Logger = LoggerFactory.getLogger(NotLockedClusters::class.java)
  }
}
