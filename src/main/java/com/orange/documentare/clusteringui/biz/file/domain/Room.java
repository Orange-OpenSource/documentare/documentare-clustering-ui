package com.orange.documentare.clusteringui.biz.file.domain;

public enum Room {
  WAITING_ROOM,
  SORT_ROOM
}
