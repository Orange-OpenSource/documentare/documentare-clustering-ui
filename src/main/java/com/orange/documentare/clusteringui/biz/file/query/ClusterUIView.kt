package com.orange.documentare.clusteringui.biz.file.query

data class ClusterUIView(val clusterId: String, val fileUIViews: List<FileUIView>, val size: Int = fileUIViews.size) {
  fun displayName(): String {
    val abbrevClusterId = clusterId.substring(0, 5)
    val multiHypothesisNamedClass = multiHypothesisNamedClass()
    return if (multiHypothesisNamedClass == null) {
      abbrevClusterId
    } else {
      "$abbrevClusterId - $multiHypothesisNamedClass"
    }
  }

  fun multiHypothesisNamedClass(): String? {
    val clusterFileNamedClass = fileUIViews.map { it.fileNamedClass }
    return multiHypothesisNamedClass(clusterFileNamedClass)
  }

  companion object {
    const val MULTIHYPOTHESIS_CLASS_SEPARATOR = " / "

    fun multiHypothesisNamedClass(clusterFileNamedClass: List<List<String>?>): String? {
      val filesWithoutNamedClassCount = clusterFileNamedClass.count { it.isNullOrEmpty() }
      if (filesWithoutNamedClassCount == clusterFileNamedClass.size) {
        return null
      }

      val allNamedClass: MutableList<String> = clusterFileNamedClass.mapNotNull { it?.toList() }.flatten().toMutableList()
      repeat(IntRange(0, filesWithoutNamedClassCount).count() - 1) {
        allNamedClass.add("unknown class")
      }
      val namedClassFrequency: Map<String, Int> = allNamedClass.groupingBy { it }.eachCount()
      val sortedNamedClassFrequency = namedClassFrequency.toList().sortedBy { (_, value) -> value }.reversed().toMap()
      return sortedNamedClassFrequency.map { entry: Map.Entry<String, Int> -> "${entry.key} ${(entry.value * 100).div(allNamedClass.size)}%" }
        .joinToString(MULTIHYPOTHESIS_CLASS_SEPARATOR)
    }
  }
}
