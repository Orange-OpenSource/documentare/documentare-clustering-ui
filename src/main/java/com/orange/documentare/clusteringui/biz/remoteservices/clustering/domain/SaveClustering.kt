package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.orange.documentare.clusteringui.biz.file.command.AddToClusterCommand
import com.orange.documentare.clusteringui.biz.file.command.DuplicateFilesCommand
import com.orange.documentare.clusteringui.biz.file.command.RemoveSelectedFilesFromClustersCommand
import com.orange.documentare.clusteringui.biz.file.command.ResetFileInfoCommand
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.domain.Room
import com.orange.documentare.clusteringui.biz.file.query.ClusterFilesQuery
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.FileDeletedInCluster.aFileWasDeletedInTheCluster
import com.orange.documentare.core.model.ref.clustering.Clustering
import com.orange.documentare.core.model.ref.clustering.ClusteringElement
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*
import java.util.Comparator.comparing


data class SaveClustering(
  val commandBus: CommandBus,
  val queryBus: QueryBus
) {

  private class ClusteringSubgroups(clustering: Clustering) {
    val inMultisetElements = extractInMultisetElements(clustering)
    val duplicateElements = extractDuplicateElements(clustering.elements)
    val notDuplicateElements = extractNotDuplicatedElements(clustering)
    val elementsGroupByClusterId = extractElementsGroupByClusterId(clustering)

    private fun extractInMultisetElements(clustering: Clustering) =
      clustering.clusters
        .filter { it.multisetElementsIndices != null }
        .flatMap { it.multisetElementsIndices }
        .map { clustering.elements[it] }

    private fun extractDuplicateElements(elements: List<ClusteringElement>) =
      elements.filter { it.duplicateOf().isPresent }

    private fun extractNotDuplicatedElements(clustering: Clustering) =
      clustering.elements.toMutableList().apply { removeAll(duplicateElements) }

    private fun extractElementsGroupByClusterId(clustering: Clustering) =
      clustering.elements.toMutableList()
        .sortedWith(comparing { el: ClusteringElement -> el.getClusterId() })
        .groupBy { it.getClusterId() }
  }

  private val log: Logger = LoggerFactory.getLogger(javaClass)

  fun saveClustering(clustering: Clustering, files: List<DomainFile>): String {
    val fileIds = files.map { it.aggregateId }

    (commandBus.dispatch<CommandResponse<Unit>>(ResetFileInfoCommand(fileIds)))
      .onFailure { throwable -> log.error("Error saving clustering: '{}'", throwable.toString(), throwable) }

    val clusteringSubgroups = ClusteringSubgroups(clustering)

    saveDuplicateElements(clusteringSubgroups.duplicateElements)
    return saveNotDuplicateElements(clusteringSubgroups, null, null)
  }

  fun saveClustering(clustering: Clustering, clusterIdModified: String): String {

    var clusterLocked: Boolean? = null
    var className: String? = null
    val clusterFilesQuery = queryBus.dispatch<QueryResponse<List<DomainFile>>>(ClusterFilesQuery(clusterIdModified))
    if (clusterFilesQuery.value().isNotEmpty()) {
      for (file in clusterFilesQuery.value()) {
        if (file.clusterCenter() && file.locked()) {
          clusterLocked = true
          className = file.className
          break
        }
      }
      val clusterModified: MutableMap<String, List<DomainFile>> = mutableMapOf()
      clusterModified[clusterIdModified] = clusterFilesQuery.value()

      (commandBus.dispatch<CommandResponse<Void>>(RemoveSelectedFilesFromClustersCommand(clusterFilesQuery.value().map { f -> f.aggregateId }, clusterModified, Room.SORT_ROOM)))
        .onFailure { throwable -> log.error("saveClustering error: $throwable") }
    }

    val clusteringSubgroups = ClusteringSubgroups(clustering)

    saveDuplicateElements(clusteringSubgroups.duplicateElements)
    return saveNotDuplicateElements(clusteringSubgroups, clusterLocked, className)
  }

  private fun saveDuplicateElements(duplicateElements: List<ClusteringElement>) {
    if (duplicateElements.isEmpty()) {
      return
    }
    (commandBus.dispatch<CommandResponse<Unit>>(DuplicateFilesCommand(duplicateElements.map { el -> el.id })))
      .onFailure { log.error("duplicate files command error occurred", it) }
  }

  private fun saveNotDuplicateElements(clusteringSubgroups: ClusteringSubgroups, clusterLocked: Boolean?, className: String?): String {
    var clusterId = "error"
    val notDuplicateElementsGroupByClusterId = clusteringSubgroups.notDuplicateElements
      .sortedWith(comparing { el: ClusteringElement -> el.getClusterId() })
      .groupBy { it.getClusterId() }

    val associations = mutableListOf<AddToClusterCommand.AddToClusterAssociation>()

    for (key in notDuplicateElementsGroupByClusterId.keys) {
      val filesId = clusteringSubgroups.elementsGroupByClusterId[key]?.map { it.id }
      if (aFileWasDeletedInTheCluster(queryBus, filesId!!)) {
        break
      }
      clusterId = uuidSupplier.invoke()

      for (clusteringElement in notDuplicateElementsGroupByClusterId[key] ?: error("list should not be null")) {
        associations.add(clusterAssociationFor(clusteringElement, clusterId, clusteringSubgroups.inMultisetElements.contains(clusteringElement), clusterLocked, className))
      }
    }

    (commandBus.dispatch<CommandResponse<Unit>>(AddToClusterCommand(associations)))
      .onFailure { log.error("add to cluster command error occurred", it) }

    return clusterId
  }

  private fun clusterAssociationFor(
    el: ClusteringElement,
    clusterId: String?,
    inMultiset: Boolean,
    clusterLocked: Boolean?,
    className: String?
  ): AddToClusterCommand.AddToClusterAssociation {
    val clusterCenter = if (el.clusterCenter == null || !el.clusterCenter) null else true
    val inMulNullable = if (!inMultiset) null else true
    val lock = if (el.clusterCenter == null || !el.clusterCenter) null else clusterLocked
    val classNameForClusterCenter = if (el.clusterCenter == null || !el.clusterCenter) null else className
    return AddToClusterCommand.AddToClusterAssociation(
      el.id, clusterId, clusterCenter, el.distanceToCenter, inMulNullable, lock, classNameForClusterCenter
    )
  }

  companion object {
    private var uuidSupplier = { UUID.randomUUID().toString() }

    /** setter for test purpose */
    @JvmStatic
    fun testUUIDSupplier(supplier: () -> String) {
      uuidSupplier = supplier
    }

    internal fun instance(infra: ClusteringInfra): SaveClustering =
      SaveClustering(infra.commandBus, infra.queryBus)
  }
}
