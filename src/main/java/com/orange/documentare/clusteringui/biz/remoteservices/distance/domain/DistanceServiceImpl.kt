package com.orange.documentare.clusteringui.biz.remoteservices.distance.domain

import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.appnotifications.AppNotification
import com.orange.documentare.clusteringui.biz.appnotifications.AppNotifications
import com.orange.documentare.clusteringui.biz.file.command.AddDistancesToClustersCommand
import com.orange.documentare.clusteringui.biz.file.command.AutoAddFileToClosestClusterCommand
import com.orange.documentare.clusteringui.biz.file.command.DuplicateFilesCommand
import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile.Companion.idsOf
import com.orange.documentare.clusteringui.biz.file.query.*
import com.orange.documentare.clusteringui.biz.parameters.domain.DistanceParameters
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.AggregateId
import com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure.DistanceRemote
import com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure.DistanceRemoteApi
import com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure.DistancesRequestResult
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.infrastructure.bus.BusResponse
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.util.Comparator.comparing
import java.util.function.Consumer
import java.util.stream.Collectors

@Service
class DistanceServiceImpl(private val appNotifications: AppNotifications, embeddedComputationServer: EmbeddedComputationServer) : DistanceService {
  private val log = LoggerFactory.getLogger(DistanceServiceImpl::class.java)
  private lateinit var queryBus: QueryBus
  private lateinit var commandBus: CommandBus
  private var distanceRemote: DistanceRemoteApi = DistanceRemote(embeddedComputationServer)
  private val distanceComputationInProgress = DistancesComputationInProgress()
  override fun isDistanceBeingComputedFor(id: String): Boolean {
    return distanceComputationInProgress.contains(id)
  }

  fun setCommandBus(commandBus: CommandBus) {
    this.commandBus = commandBus
  }

  fun setQueryBus(queryBus: QueryBus) {
    this.queryBus = queryBus
  }

  override fun isRunning(): Boolean {
    return distanceComputationInProgress.notEmpty()
  }

  override fun computeDistancesOf(parameters: Parameters, fileIds: List<String>) {
    val view = queryBus.dispatch<QueryResponse<ComputeDistancesQueryView>>(ComputeDistancesQuery(fileIds))
    if (nothingToDo(view)) {
      return
    }
    val computeDistancesQueryView = view.value()
    computeDistanceInDetachedThread(parameters, computeDistancesQueryView.files, computeDistancesQueryView.mapClusterMultiset)
  }

  override fun computeDistancesOfInSameThread(parameters: Parameters, fileIds: List<String>) {
    val view = queryBus.dispatch<QueryResponse<ComputeDistancesQueryView>>(ComputeDistancesQuery(fileIds))
    if (nothingToDo(view)) {
      return
    }
    val computeDistancesQueryView = view.value()
    computeDistances(parameters, computeDistancesQueryView.files, computeDistancesQueryView.mapClusterMultiset)
  }

  private fun computeDistancesToCentroidInSameThread(
    distanceParameters: DistanceParameters,
    files: List<DomainFile>,
    centroidMultisetFiles: List<DomainFile>
  ): List<Pair<DomainFile, Int>> {
    val res: TryOrCatch<Map<DomainFile, DistancesRequestResult>> = doComputeDistances(distanceParameters, files, listOf(centroidMultisetFiles))
    if (res.isFailure) {
      throw IllegalStateException("❌ Failed to compute distance: " + res.cause)
    }

    val distanceResults = res.get().entries
    return distanceResults.map { entry: Map.Entry<DomainFile, DistancesRequestResult> ->
      Pair(entry.key, entry.value.distances[0])
    }
  }

  override fun computeDistancesToCentroidAnywhereInHierarchyInSameThread(files: List<AggregateId>, centroidMultisetFiles: List<AggregateId>): List<Pair<AggregateId, Int>> {
    val filesQuery = queryBus.dispatch<QueryResponse<List<DomainFile>>>(SelectedFilesAtLevel0Query(files))
    val centroidFilesQuery = queryBus.dispatch<QueryResponse<List<DomainFile>>>(SelectedFilesAtLevel0Query(centroidMultisetFiles))
    if (filesQuery.failure()) {
      throw IllegalStateException("❌ Failed to get files: " + filesQuery.failureCause())
    }
    if (centroidFilesQuery.failure()) {
      throw IllegalStateException("❌ Failed to get centroid files: " + centroidFilesQuery.failureCause())
    }

    val distanceParameters = DistanceParameters.get(queryBus)
    val domainFiles = filesQuery.value()
    val centroidFiles = centroidFilesQuery.value()

    val distances: List<Pair<DomainFile, Int>> = computeDistancesToCentroidInSameThread(distanceParameters, domainFiles, centroidFiles)
    return distances.map { (file, distance) -> Pair(file.aggregateId, distance) }
  }

  private fun nothingToDo(view: QueryResponse<ComputeDistancesQueryView>): Boolean {
    if (view.failure()) {
      log.error("Failed to build compute distances query view: " + view.failureCause())
      return true
    }
    if (view.value().files.isEmpty()) {
      log.warn("No files need distances to be computed")
      return true
    }
    val mapClusterMultiset = view.value().mapClusterMultiset
    if (mapClusterMultiset.isEmpty()) {
      log.warn("Can not Compute distances since there are no cluster centers")
      return true
    }
    return false
  }

  private fun computeDistanceInDetachedThread(parameters: Parameters, files: List<DomainFile>, mapClusterMultiset: Map<String, List<DomainFile>>) {
    Thread {
      TryOrCatch.run {
        files.forEach(Consumer { file: DomainFile? -> distanceComputationInProgress.add(file!!) })
        computeDistances(parameters, files, mapClusterMultiset)
      }.onFailure { throwable: Throwable -> log.error("Failed to compute distance: $throwable", throwable) }
        .andFinally { files.forEach(Consumer { file: DomainFile? -> distanceComputationInProgress.remove(file!!) }) }
    }.start()
  }

  private fun computeDistances(parameters: Parameters, files: List<DomainFile>, mapClusterMultiset: Map<String, List<DomainFile>>) {
    doComputeDistances(DistanceParameters.from(parameters), files, mapClusterMultiset.values)
      .onSuccess { result: Map<DomainFile, DistancesRequestResult> ->
        val query = queryBus.dispatch<QueryResponse<List<DomainFile>>>(SelectedFilesQuery(idsOf(files)))
        if (query.value().isEmpty()) {
          return@onSuccess
        }
        query.value().stream().filter { selectedFile: DomainFile -> !selectedFile.deleted() }.forEach { selectedFile: DomainFile ->
          val fileDistances = result[selectedFile] ?: throw buildExceptionContext(selectedFile, result, files)
          saveDistances(parameters, selectedFile, fileDistances, mapClusterMultiset)
        }
      }.onFailure { throwable: Throwable -> log.warn("Failed to compute distance of {} files: {}", files.size, throwable.localizedMessage) }

    appNotifications.notify(AppNotification.COMPUTE_DISTANCES_TO_CLUSTERS_DONE)
  }

  private fun doComputeDistances(
    distanceParameters: DistanceParameters,
    files: List<DomainFile>,
    clustersMultisetFiles: Collection<List<DomainFile>>
  ): TryOrCatch<Map<DomainFile, DistancesRequestResult>> =
    distanceRemote.doTheJob(distanceParameters, files, clustersMultisetFiles)

  private fun saveDistances(parameters: Parameters, domainFile: DomainFile, distancesRequestResult: DistancesRequestResult, mapClusterMultiset: Map<String, List<DomainFile>>) {
    if (distancesRequestResult.error) {
      log.warn("Failed to compute distance of {}: {}", domainFile.filename, distancesRequestResult.errorMessage)
      return
    }
    val distances: MutableList<DistanceToCluster> = mutableListOf()
    val keySet = mapClusterMultiset.keys
    for ((i, key) in keySet.withIndex()) {
      distances.add(DistanceToCluster(distancesRequestResult.distances[i], key))
    }

    if (distances.isEmpty()) {
      return
    }

    val distancesSorted = distances.sortedWith(comparing { distanceToCluster: DistanceToCluster -> distanceToCluster.distance })

    val nearestCluster = distancesSorted[0]
    val duplicate = nearestCluster.distance == 0

    if (duplicate) {
      commandBus.dispatch<BusResponse<*, *>>(DuplicateFilesCommand(listOf(domainFile.aggregateId)))
        .onFailure { throwable: Any? -> log.error("Failed to save duplicate file {}: {}", domainFile, throwable) }
      return
    }

    if (shouldAutoInclude(nearestCluster, parameters.thresholdFactor)) {
      val closestClusterFileIds: List<String>
      val response = queryBus.dispatch<QueryResponse<List<DomainFile>>>(ClusterFilesQuery(nearestCluster.clusterId))
      if (response.value().isNotEmpty()) {
        closestClusterFileIds = response.value().stream().map { file: DomainFile -> file.aggregateId }.collect(Collectors.toList())
        commandBus.dispatch<BusResponse<*, *>>(AutoAddFileToClosestClusterCommand(domainFile.aggregateId, nearestCluster, closestClusterFileIds, parameters.maxMultisetThreshold))
          .onFailure { throwable: Any -> log.error("Failed to add file to Cluster : $throwable") }
      }
    } else {
      commandBus.dispatch<BusResponse<*, *>>(AddDistancesToClustersCommand(domainFile.aggregateId, distancesSorted))
        .onFailure { throwable: Any? -> log.error(String.format("Failed to save distances to file %s", domainFile.aggregateId), throwable) }
    }
  }

  private fun shouldAutoInclude(nearestCluster: DistanceToCluster, thresholdFactor: Float): Boolean {
    val clusterDistancesStats = clusterDistancesStats(nearestCluster)
    return if (clusterDistancesStats == null) false
    else clusterDistancesStats.singleton || nearestCluster.distance <= clusterDistancesStats.threshold(thresholdFactor)
  }

  private fun clusterDistancesStats(nearestCluster: DistanceToCluster): ClusterDistancesStats? {
    val responseThreshold = queryBus.dispatch<QueryResponse<ClusterDistancesStats>>(DistanceToCenterStatsForAllFilesInClusterQuery(nearestCluster.clusterId))
    return responseThreshold.value()
  }

  /**
   * ONLY FOR TEST PURPOSE
   */
  fun testSimulateDistanceInProgress(file: DomainFile?) {
    distanceComputationInProgress.add(file!!)
  }

  fun testComputeDistancesOfAllFilesNotInCluster() {
    computeDistancesOf(Parameters.defaults(), emptyList())
  }

  fun testSetDistanceRemote(distanceRemoteApi: DistanceRemoteApi) {
    distanceRemote = distanceRemoteApi
  }

  companion object {
    fun buildExceptionContext(file: DomainFile, result: Map<DomainFile, DistancesRequestResult>, files: List<DomainFile>): IllegalStateException {
      val errorMessage =
        "File distances to clusters is not available for file: filename(${file.filename}) - id(${file.aggregateId})\n\n" + "distance computation results are present for files with id: '${result.keys.map { it.aggregateId }}'\n\n" + "initial distances computation request was for files with id: '${files.map { it.aggregateId }}'"
      return IllegalStateException(errorMessage)
    }
  }
}
