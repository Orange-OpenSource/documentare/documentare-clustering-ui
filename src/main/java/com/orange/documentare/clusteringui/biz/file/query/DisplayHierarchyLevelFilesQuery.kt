package com.orange.documentare.clusteringui.biz.file.query

import com.tophe.ddd.queries.Query

class DisplayHierarchyLevelFilesQuery(val hierarchyLevelIndex: Int) : Query
