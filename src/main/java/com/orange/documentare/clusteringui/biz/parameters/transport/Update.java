package com.orange.documentare.clusteringui.biz.parameters.transport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.orange.documentare.clusteringui.biz.parameters.command.UpdateParametersCommand;
import com.tophe.ddd.commands.CommandBus;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class Update {

  @Autowired
  CommandBus commandBus;

  @RequestMapping(value = "/parameters/create", method = RequestMethod.POST)
  public String updateParameters(@ModelAttribute UpdateParametersCommand command) {
    String prefix = "POST [/parameters/create]";

    commandBus.dispatch(command)
      .onSuccess(res -> log.info(prefix + " parameters updated: " + command.toString()))
      .onFailure(throwable -> log.error(prefix + " error: " + throwable));

    return "redirect:/";
  }
}
