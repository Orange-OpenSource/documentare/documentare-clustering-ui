package com.orange.documentare.clusteringui.biz.processes.feed.domain.events

data class MultisetThresholdReset(val multisetThreshold: Int) : FeedLoopEvent() {
  override fun displayInternalHtml() = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; multiset threshold reset to initial value"
  override fun displayInternalRaw() = "      multiset threshold reset to initial value"
}
