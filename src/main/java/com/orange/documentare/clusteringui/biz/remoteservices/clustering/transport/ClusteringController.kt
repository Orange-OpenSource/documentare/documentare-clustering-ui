package com.orange.documentare.clusteringui.biz.remoteservices.clustering.transport

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.AllFilesQuery
import com.orange.documentare.clusteringui.biz.file.query.AllFilesToSortQuery
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQuery
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.ClusteringServiceImpl
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import java.util.stream.Collectors

@Controller
class ClusteringController(private val queryBus: QueryBus, private val clusteringService: ClusteringServiceImpl) {

  private val log: Logger = LoggerFactory.getLogger(ClusteringController::class.java)

  @RequestMapping(value = ["/global-clustering/create"])
  fun doGlobalClustering(): String {
    val prefix = "GET [/global-clustering/create]"
    log.debug(prefix)
    val parameters = parameters()
    if (parameters == null) {
      log.warn("$prefix failed to find parameters, redirect user to form page")
      return "redirect:/parameters"
    }
    val response = queryBus.dispatch<QueryResponse<MutableList<DomainFile>>>(AllFilesQuery())
    response
      .onFailure { throwable: Throwable -> logError(throwable) }
      .onSuccess { allFiles: MutableList<DomainFile> ->
        val filesInBin = allFiles.stream().filter { obj: DomainFile -> obj.duplicated() }.collect(Collectors.toList())
        allFiles.removeAll(filesInBin)
        val filesIdsInBinToPurge = filesInBin.stream().map { file: DomainFile -> file.aggregateId }.collect(Collectors.toList())
        clusteringService.globalOrPartialClusteringAsync(parameters, allFiles, filesIdsInBinToPurge)
      }
    return "redirect:/"
  }

  @RequestMapping(value = ["/partial-clustering/create"])
  fun partialCreateClustering(): String {
    val prefix = "GET [/partial-clustering/create]"
    log.info(prefix)
    val parameters = parameters()
    if (parameters == null) {
      log.warn("$prefix failed to find parameters, redirect user to form page")
      return "redirect:/parameters"
    }
    val responseFileIdsToSort = queryBus.dispatch<QueryResponse<List<DomainFile>>>(AllFilesToSortQuery())
    responseFileIdsToSort
      .onFailure { throwable: Throwable -> logError(throwable) }
    if (responseFileIdsToSort.value().isNotEmpty()) {
      val allFilesToSort = responseFileIdsToSort.value()
      clusteringService.globalOrPartialClusteringAsync(parameters, allFilesToSort, emptyList())
    }
    return "redirect:/"
  }

  private fun parameters(): Parameters? {
    val response = queryBus.dispatch<QueryResponse<Parameters>>(ParametersQuery())
    return if (response.failure()) null else response.value()
  }

  private fun logError(throwable: Any) {
    log.error(ClusteringServiceImpl::class.java.simpleName + "clustering failed: " + throwable, throwable)
  }

}
