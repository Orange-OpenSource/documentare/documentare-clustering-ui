package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.DisplayHierarchyLevelFileTransform.Companion.CLASS_NO_NAME
import com.orange.documentare.clusteringui.biz.file.query.DisplayHierarchyLevelFileTransform.Companion.orderFilesInClusterByBizCriteria
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler

// TODO MULTI-HYPOTHESIS
// should rely on DisplayHierarchyLevelFileTransform; problematic duplication
class DisplayApiFilesQueryHandler(private val projections: Projections) : QueryHandler<DisplayApiFilesQuery, DisplayApiFilesView>() {

  override fun doExecute(query: DisplayApiFilesQuery): DisplayApiFilesView {

    val inWaitingRoom = filesInWaitingRoom()
    val notInClusters = filesNotInClusters()
    val mapClassViews = mapClasses()
    val sortedMap = mapClassViews.toSortedMap()

    return DisplayApiFilesView(inWaitingRoom, notInClusters, sortedMap)
  }

  private fun filesInWaitingRoom(): List<DomainFile> {
    return projections.clusters.filesNotInClusters()
      .map { fileId -> projections.files[fileId] }
      .filter { file -> file.isInWaitingRoom() }
      .sortedWith(Comparator.comparing { domainFile: DomainFile -> domainFile.aggregateId })
  }

  private fun filesNotInClusters(): List<DomainFile> {
    return projections.clusters.filesNotInClusters()
      .map { fileId -> projections.files[fileId] }
      .filter { file -> !file.isInWaitingRoom() }
  }

  private fun mapClasses(): MutableMap<String, Map<String, List<DomainFile>>> {
    val classes = projections.clusters.classes()
    val filesOrderedInClusters = orderFilesInClusterByBizCriteria(projections.clusters.clusters(), projections.files.currentHierarchyLevel())

    val mapClassViews: MutableMap<String, Map<String, List<DomainFile>>> = mutableMapOf()
    for (tuple in classes) {
      val className = tuple.key
      val classClusterIDs = tuple.value
      mapClassViews[className] = retrieveClassClusters(classClusterIDs, filesOrderedInClusters)
    }

    val clustersNotInClasses = orderClustersWithFileViewsBySize(filesOrderedInClusters)
    mapClassViews[CLASS_NO_NAME] = clustersNotInClasses

    return mapClassViews
  }

  private fun retrieveClassClusters(
    classClusterIDs: List<String>,
    clustersWithOrderedFileViews: MutableMap<String, List<DomainFile>>
  ): Map<String, List<DomainFile>> {
    val mapClusterIdClusterFiles: MutableMap<String, List<DomainFile>> = mutableMapOf()
    classClusterIDs.forEach { aClassClusterId ->
      val clusterFileViews = clustersWithOrderedFileViews[aClassClusterId]!!
      mapClusterIdClusterFiles[aClassClusterId] = clusterFileViews
      clustersWithOrderedFileViews.remove(aClassClusterId)
    }
    return orderClustersWithFileViewsBySize(mapClusterIdClusterFiles)
  }

  private fun orderClustersWithFileViewsBySize(clustersWithFileUIViews: Map<String, List<DomainFile>>): MutableMap<String, List<DomainFile>> {
    return clustersWithFileUIViews
      .toList()
      .sortedBy { (_, clusterUIView) -> clusterUIView.size }
      .reversed()
      .toMap()
      .toMutableMap()
  }
}
