package com.orange.documentare.clusteringui.biz.processes.feed.transport

import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeedLoopEvent

object FeedEventsPrinter {
  fun print(events: List<FeedLoopEvent>, prefix: String? = null): String {
    val stringBuilder = StringBuilder()
    stringBuilder.append("<html><head><meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"></head><body>")
    events.reversed().forEach {
      stringBuilder.append("${it.display(prefix)}<br>")
    }
    stringBuilder.append("</body> <script>setTimeout(() => { document.location.reload(); }, 30000);</script> </html>")
    return stringBuilder.toString()
  }
}
