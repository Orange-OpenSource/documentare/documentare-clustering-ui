package com.orange.documentare.clusteringui.biz.file.transport

import com.orange.documentare.clusteringui.biz.file.command.DeleteFilesCommand
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.infrastructure.bus.BusResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.util.*

@Controller
class Delete {

  @Autowired
  lateinit var commandBus: CommandBus

  private val log: Logger = LoggerFactory.getLogger(Delete::class.java)

  @RequestMapping("/delete/{fileId}")
  fun deleteFile(@PathVariable fileId: String?): String {
    if (fileId == null) {
      log.warn("DELETE [/delete/fileId]" + " fileId is NULL")
      return "redirect:/"
    }

    val deletePrefix = "DELETE [/delete/$fileId]"
    commandBus.dispatch<BusResponse<*, *>>(DeleteFilesCommand(listOf(fileId)))
      .onSuccess { log.info(deletePrefix) }
      .onFailure { throwable: Any -> log.error("$deletePrefix error: $throwable") }

    return "redirect:/"
  }


  @Secured("ROLE_USER")
  @ResponseBody
  @RequestMapping(value = ["/api/delete-file"], method = [RequestMethod.POST])
  fun deleteFiles(@RequestBody selectedFileIdsToSort: Array<String>?): String {
    val prefix = "DELETE [/api/delete-file]"
    if (selectedFileIdsToSort == null) {
      log.warn("$prefix selectedFileIdsToSort is NULL")
      return "redirect:/"
    }

    val prefixWithFileIds = prefix + " " + Arrays.toString(selectedFileIdsToSort)
    commandBus.dispatch<BusResponse<*, *>>(DeleteFilesCommand(listOf(*selectedFileIdsToSort)))
      .onSuccess { log.info(prefixWithFileIds) }
      .onFailure { log.error("$prefixWithFileIds error: $it") }

    return "redirect:/"
  }

}
