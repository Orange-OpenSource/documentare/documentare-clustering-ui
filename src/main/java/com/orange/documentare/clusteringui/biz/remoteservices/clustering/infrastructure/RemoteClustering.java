package com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import com.orange.documentare.clusteringui.Couple;
import com.orange.documentare.clusteringui.EmbeddedComputationServer;
import com.orange.documentare.clusteringui.TryOrCatch;
import com.orange.documentare.core.model.ref.clustering.Clustering;
import com.orange.documentare.core.model.ref.clustering.infra.network.dto.ClusteringDTO;
import com.orange.documentare.tasksdispatcher.TasksDispatcher;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

public class RemoteClustering {

  private EmbeddedComputationServer embeddedComputationServer;

  public RemoteClustering(EmbeddedComputationServer embeddedComputationServer) {
    this.embeddedComputationServer = embeddedComputationServer;
  }

  interface Dispatch {

    void dispatch(List<String> urls, List<ClusteringRequestWrapper> apiWrappers, BiConsumer<ClusteringRequestWrapper, ClusteringDTO> resultObserver);
  }

  @Setter(AccessLevel.PACKAGE)
  private Dispatch dispatchRequests = (urls, apiWrappers, resultObserver) ->
    (new TasksDispatcher(apiWrappers, urls, resultObserver, null, embeddedComputationServer.getEnabled() ? embeddedComputationServer.getTasksApiClient() : null))
      .distributeTasks();

  // FIXME: Try is not necessary
  public Map<PreviousClusterId, TryOrCatch<Clustering>> request(List<String> urls, List<Couple<PreviousClusterId, ClusteringRequestUI>> clusteringRequests) {
    return doRequest(urls, clusteringRequests);
  }

  private Map<PreviousClusterId, TryOrCatch<Clustering>> doRequest(List<String> urls, List<Couple<PreviousClusterId, ClusteringRequestUI>> clusteringRequests) {
    List<ClusteringRequestWrapper> apiWrappers =
      clusteringRequests.stream().map(tuple -> new ClusteringRequestWrapper(tuple.second, embeddedComputationServer)).collect(Collectors.toList());

    AtomicReference<Map<ClusteringRequestUI, Clustering>> results = new AtomicReference<>();
    results.set(new HashMap<>());
    BiConsumer<ClusteringRequestWrapper, ClusteringDTO> resultObserver =
      (apiWrapper, res) -> {
        Clustering clustering = res.toBusinessObject();
        ResultUpdate updater = new ResultUpdate(apiWrapper.clusteringRequestUI, clustering);
        results.getAndUpdate(updater::update);
      };

    dispatchRequests.dispatch(urls, apiWrappers, resultObserver);

    Map<ClusteringRequestUI, Clustering> resMap = results.get();
    List<Couple<PreviousClusterId, TryOrCatch<Clustering>>> tuples =
      clusteringRequests.stream().map(tuple -> new Couple<PreviousClusterId, TryOrCatch<Clustering>>(tuple.first, TryOrCatch.success(resMap.get(tuple.second))))
        .collect(Collectors.toList());

    HashMap<PreviousClusterId, TryOrCatch<Clustering>> map = new HashMap<>();
    tuples.forEach(mapEntry -> map.put(mapEntry.first, mapEntry.second));

    return map;
  }

  @RequiredArgsConstructor
  private static class ResultUpdate {

    private final ClusteringRequestUI clusteringRequestUI;
    private final Clustering res;

    Map<ClusteringRequestUI, Clustering> update(Map<ClusteringRequestUI, Clustering> currentResults) {
      currentResults.put(clusteringRequestUI, res);
      return currentResults;
    }
  }
}
