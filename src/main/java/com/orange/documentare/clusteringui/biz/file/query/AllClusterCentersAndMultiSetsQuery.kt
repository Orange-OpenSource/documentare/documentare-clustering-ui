package com.orange.documentare.clusteringui.biz.file.query

import com.tophe.ddd.queries.Query

class AllClusterCentersAndMultiSetsQuery : Query {
  data class AllClusterCentersAndMultiSets(val clusterCentersAndMultiSetsIds: List<String>, val hierarchyLevel: Int)
}
