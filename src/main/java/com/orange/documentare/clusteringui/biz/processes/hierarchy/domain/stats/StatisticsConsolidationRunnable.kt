package com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats

import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport

interface StatisticsConsolidationRunnable {
  fun run(clientHierarchyExport: ClientHierarchyExport)
}
