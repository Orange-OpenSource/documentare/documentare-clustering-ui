package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.DisplayHierarchyLevelFileTransform.Companion.orderClustersWithFileViewsBySize
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.infrastructure.persistence.EventRepository
import com.tophe.ddd.queries.QueryHandler
import org.slf4j.LoggerFactory
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.Comparator.comparing

class DisplayTimeMachineFilesQueryHandler(private val eventRepository: EventRepository) : QueryHandler<DisplayTimeMachineFilesQuery, DisplayTimeMachineFilesView>() {

  private val log = LoggerFactory.getLogger(DisplayTimeMachineFilesQueryHandler::class.java)

  override fun doExecute(query: DisplayTimeMachineFilesQuery): DisplayTimeMachineFilesView {
    val strDate = query.dateTime
    var date = Date()
    try {
      // create SimpleDateFormat object with source string date format
      val sdfSource = SimpleDateFormat(
        "yyyy-MM-dd'T'hh:mm"
      )

      // parse the string into Date object
      date = sdfSource.parse(strDate)
      log.info("Date is converted from yyyy-MM-dd'T'hh:mm format to dd/MM/yyyy, ha")
    } catch (pe: ParseException) {
      log.error("Parse Exception", pe)
    }

    val projectionsTM = Projections(eventRepository, date)
    val transform = DisplayHierarchyLevelFileTransform.with(projectionsTM) { toFileView(it) }
    val inWaitingRoom = transform.filesInWaitingRoom()
    val notInClusters = transform.filesNotInClusters()
    val orderedByClusterId: Map<String, ClusterUIView> = transform.orderFileViewsInClusterByBizCriteria()
    val sortedMapByListSize1: Map<String, ClusterUIView> = orderClustersWithFileViewsBySize(orderedByClusterId)

    val duplicates = projectionsTM.files.ids()
      .map { projectionsTM.files[it] }
      .filter { it.duplicated() }
      .map { toFileView(it) }
      .sortedWith(comparing { fileUIView: FileUIView -> fileUIView.aggregateId })

    val mapClassViews: Map<String, Map<String, ClusterUIView>> = transform.mapClasses()
    val sortedMapClassViews: Map<String, Map<String, ClusterUIView>> = TreeMap(mapClassViews)
    return DisplayTimeMachineFilesView(
      projectionsTM.files.filesCount() == 0, inWaitingRoom, notInClusters, sortedMapByListSize1, sortedMapClassViews, duplicates
    )
  }

  private fun toFileView(file: DomainFile): FileUIView = FileUIView.from(file)
}
