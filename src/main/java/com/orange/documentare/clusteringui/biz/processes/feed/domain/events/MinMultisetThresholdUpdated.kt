package com.orange.documentare.clusteringui.biz.processes.feed.domain.events

data class MinMultisetThresholdUpdated(val minMultisetThreshold: Int) : FeedLoopEvent() {
  override fun displayInternalHtml() = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; min multiset threshold updated to ${minMultisetThreshold}"
  override fun displayInternalRaw() = "      min multiset threshold updated to ${minMultisetThreshold}"
}
