package com.orange.documentare.clusteringui.biz.file.command;

import java.util.List;
import java.util.Map;

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile;
import com.orange.documentare.clusteringui.biz.file.domain.Room;
import com.tophe.ddd.commands.Command;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class RemoveSelectedFilesFromClustersCommand implements Command {

  public final List<String> filesIds;
  public final Map<String, List<DomainFile>> clusters;
  public final Room fileInRoom;

  public RemoveSelectedFilesFromClustersCommand(List<String> filesIds, Map<String, List<DomainFile>> clusters, Room fileInRoom) {
    this.filesIds = filesIds;
    this.clusters = clusters;
    this.fileInRoom = fileInRoom;
  }
}
