package com.orange.documentare.clusteringui.biz.file.transport

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.DisplayHomeFilesQuery
import com.orange.documentare.clusteringui.biz.file.query.DisplayHomeFilesView
import com.orange.documentare.clusteringui.biz.file.query.SelectedFileView
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQuery
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederServiceImpl
import com.orange.documentare.clusteringui.biz.processes.hierarchy.transport.HierarchyService
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.ClusteringServiceImpl
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceServiceImpl
import com.orange.documentare.clusteringui.biz.services.MessageByLocaleService
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.orange.documentare.clusteringui.infrastructure.security.Authent
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import jakarta.servlet.http.HttpServletResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import java.security.Principal

@Controller
class Home {
  private val log = LoggerFactory.getLogger(Home::class.java)

  @Autowired
  lateinit var commandBus: CommandBus

  @Autowired
  lateinit var queryBus: QueryBus

  @Autowired
  lateinit var clusteringService: ClusteringServiceImpl

  @Autowired
  lateinit var distanceService: DistanceServiceImpl

  @Autowired
  lateinit var feederService: FeederServiceImpl

  @Autowired
  lateinit var hierarchyService: HierarchyService

  @Autowired
  lateinit var messageByLocaleService: MessageByLocaleService

  @RequestMapping("/")
  fun home(
    principal: Principal,
    @RequestParam(value = "homeSortedShown", defaultValue = "false") sortedShown: String,
    @RequestParam(value = "homeNotSortedShown", defaultValue = "false") notSortedShown: String,
    model: Model
  ): String {
    log.debug("🐞 GET [/] with sortedShown=$sortedShown and notSortedShown=$notSortedShown")
    model.addAttribute("title", "Home")
    Authent.addAuthenticatedModel(model, principal)
    val responseParameters = queryBus.dispatch<QueryResponse<Parameters?>>(ParametersQuery())
    val parameters: Parameters = if (responseParameters.failure() || responseParameters.value() == null) {
      Parameters.defaults()
    } else {
      responseParameters.value()!!
    }
    val response = queryBus.dispatch<QueryResponse<DisplayHomeFilesView>>(DisplayHomeFilesQuery())
    response
      .onSuccess { filesView: DisplayHomeFilesView -> fillModel(filesView, sortedShown == "true", notSortedShown == "true", parameters.isFeed, model) }
      .onFailure { throwable: Throwable -> log.error("GET [/] error: " + throwable.localizedMessage) }
    return "index"
  }

  private fun fillModel(filesView: DisplayHomeFilesView, sortedShown: Boolean, notSortedShown: Boolean, feed: Boolean, model: Model) {
    model.addAttribute("sortedShown", sortedShown)
    model.addAttribute("notSortedShown", notSortedShown)
    model.addAttribute("currentLevel", filesView.currentHierarchyLevel)
    model.addAttribute("nofiles", filesView.noFiles)
    model.addAttribute("filesInWaitingRoom", filesView.inWaitingRoom)
    model.addAttribute("numberFilesInWaitingRoom", messageByLocaleService.getMessage("Files_In_Waiting_Room", arrayOf<Any>(filesView.inWaitingRoom!!.size)))
    model.addAttribute("filesWithoutClusterId", filesView.notInClusters)
    model.addAttribute("mapClassViews", filesView.mapClassViews)
    model.addAttribute("waitingForClusteringResult", filesView.clusteringInProgress)
    model.addAttribute("computeDistanceInProgress", filesView.computeDistancesInProgress)
    model.addAttribute("waitingForFeedResult", filesView.feedInProgress)
    val selectedFileViewInCluster = SelectedFileView(null, null, null)
    model.addAttribute("selectedFileViewInCluster", selectedFileViewInCluster)
    val selectedFileView = SelectedFileView(null, null, null)
    model.addAttribute("selectedFileView", selectedFileView)
    if (filesView.computeDistancesInProgress || filesView.clusteringInProgress || filesView.feedInProgress) {
      model.addAttribute("classAlbum", "album-red-ligth text-muted")
    } else {
      model.addAttribute("classAlbum", "album text-muted")
    }
    model.addAttribute("commandTM", commandTM())
    model.addAttribute("feedActive", feed)
    model.addAttribute("askFeedToStop", filesView.feedStop)
  }

  @Secured("ROLE_USER")
  @ResponseBody
  @RequestMapping(value = ["/api/check-state-background-operation"], method = [RequestMethod.GET])
  fun checkStateBackgroundOperation(response: HttpServletResponse?): String {
    val prefix = "GET [/api/check-state-background-operation]"
    log.debug(prefix)
    var backgroundOperationInProgress = "no"
    if (distanceService.isRunning() || clusteringService.isRunning() || feederService.isRunning || hierarchyService.isStatisticsConsolidationRunning()) {
      backgroundOperationInProgress = "yes"
      if (feederService.isRunning && feederService.getThenClearDataUpdated()) {
        backgroundOperationInProgress = "reload"
      }
    }
    return backgroundOperationInProgress
  }

  @Secured("ROLE_USER")
  @ResponseBody
  @RequestMapping(value = ["/api/check-feed-in_progress"], method = [RequestMethod.GET])
  fun checkFeedInProgress(response: HttpServletResponse?): String {
    val prefix = "GET [/api/check-feed-in_progress]"
    log.debug(prefix)
    var feedInProgress = "no"
    if (feederService.isRunning && feederService.getThenClearDataUpdated()) {
      feedInProgress = "reload"
    }
    return feedInProgress
  }

  @Secured("ROLE_USER")
  @ResponseBody
  @RequestMapping(value = ["/api/stop-feed"], method = [RequestMethod.GET])
  fun stopFeed(response: HttpServletResponse?): String {
    val prefix = "GET [/api/stop-feed]"
    log.debug(prefix)
    if (feederService.isRunning) {
      feederService.stopFeederWhenPossible()
    }
    return "true"
  }

  companion object {
    fun buildThumbnailsModel(files: List<DomainFile>): List<String> =
      files.map { FileIO.thumbnailWebPathOf(it) }
  }
}
