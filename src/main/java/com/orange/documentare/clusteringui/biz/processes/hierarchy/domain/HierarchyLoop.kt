package com.orange.documentare.clusteringui.biz.processes.hierarchy.domain

import com.orange.documentare.clusteringui.biz.file.command.MoveMultisetElementToNextHierarchyLevelCommand
import com.orange.documentare.clusteringui.biz.file.query.AllClusterCentersAndMultiSetsQuery
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederProcessFinished
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederService
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.infrastructure.event.PersistencePauseAndResume
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.LoggerFactory

class HierarchyLoop(
  private val commandBus: CommandBus,
  private val queryBus: QueryBus,
  private val feederService: FeederService,
  private val persistencePauseAndResume: PersistencePauseAndResume,
  private val getClusterCentersAndMultiset: () -> Pair<List<String>, Int> = { getClusterCentersAndMultiset(queryBus) },
  private val moveClusterCentersAndMultisetToNextHierarchy: (clusterCentersAndMultiSetsIds: List<String>, nextHierarchyLevel: Int) -> Unit = { clusterCentersAndMultiSetsIds: List<String>, nextHierarchyLevel: Int ->
    moveClusterCentersAndMultisetToNextHierarchy(
      commandBus,
      clusterCentersAndMultiSetsIds,
      nextHierarchyLevel
    )
  }
) : HierarchyLoopRunnable, FeederProcessFinished {
  private var loopCount = 0
  private var previousClusterCentersAndMultiSetsIds = listOf<String>()

  override fun run() {
    buildStarts()

    loopCount = 0
    previousClusterCentersAndMultiSetsIds = mutableListOf()
    loop()
  }

  private fun loop() {
    loopCount++
    log.info("\n\n[HierarchyLoop - LOOP $loopCount]: create new hierarchy and launch feeder\n")

    createNewHierarchy()
  }

  private fun createNewHierarchy() {
    val (clusterCentersAndMultiSetsIds, hierarchyLevel) = getClusterCentersAndMultiset()
    if (previousClusterCentersAndMultiSetsIds == clusterCentersAndMultiSetsIds) {
      buildIsFinished()
      // appNotifications.notify(AppNotification.HIERARCHY_BUILD_FINISHED)
      return
    }
    previousClusterCentersAndMultiSetsIds = clusterCentersAndMultiSetsIds

    moveClusterCentersAndMultisetToNextHierarchy(clusterCentersAndMultiSetsIds, hierarchyLevel + 1)

    feederService.launch(this)
  }

  private fun buildStarts() {
    log.info("\n\n[HierarchyLoop - START]: start creating new hierarchies\n")
    persistencePauseAndResume.pauseHardPersistenceAndAppendToVolatilePersistence()
  }

  private fun buildIsFinished() {
    log.info("\n\n[HierarchyLoop - END]: hierarchy build is finished\n")
    persistencePauseAndResume.stopHardAndVolatilePersistence()
  }

  override fun onFeederProcessFinished() {
    log.info("\n\n[HierarchyLoop - FEEDER DONE]: feeder has finished\n")
    loop()
  }

  companion object {
    private val log = LoggerFactory.getLogger(HierarchyLoop::class.java)

    private fun moveClusterCentersAndMultisetToNextHierarchy(commandBus: CommandBus, clusterCentersAndMultiSetsIds: List<String>, nextHierarchyLevel: Int) {
      val response = commandBus.dispatch<CommandResponse<Void>>(MoveMultisetElementToNextHierarchyLevelCommand(clusterCentersAndMultiSetsIds, nextHierarchyLevel))
      response?.onFailure { log.error("[HierarchyLoop]: $it") }
    }

    private fun getClusterCentersAndMultiset(queryBus: QueryBus): Pair<List<String>, Int> {
      val response = queryBus.dispatch<QueryResponse<AllClusterCentersAndMultiSetsQuery.AllClusterCentersAndMultiSets>>(
        AllClusterCentersAndMultiSetsQuery()
      )
      response.onFailure { log.error("[HierarchyLoop]: $it") }

      return Pair(response.value().clusterCentersAndMultiSetsIds, response.value().hierarchyLevel)
    }
  }
}
