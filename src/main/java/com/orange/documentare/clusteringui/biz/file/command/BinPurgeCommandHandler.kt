package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.persistence.EventRepository


class BinPurgeCommandHandler(private val eventRepository: EventRepository, eventBus: EventBus) : CommandHandler<BinPurgeCommand, Void>(eventBus) {

  override fun doExecute(command: BinPurgeCommand): AggregateAndEvents<Void?, Collection<Event>> {
    val events = command.filesIdsInBin
      .mapNotNull { deleteFile(it) }

    return AggregateAndEvents.withOnlyEvents(events)
  }

  private fun deleteFile(id: String): Event? =
    loadFile(id)?.delete()

  private fun loadFile(fileId: String): DomainFile? = eventRepository.load(fileId)
}