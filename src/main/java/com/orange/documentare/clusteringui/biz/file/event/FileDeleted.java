package com.orange.documentare.clusteringui.biz.file.event;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;

import java.time.Instant;

import com.tophe.ddd.infrastructure.event.Event;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FileDeleted extends Event {

  public final String clusterId;

  public FileDeleted(String aggregateId, String clusterId) {
    super(aggregateId);
    this.clusterId = clusterId;
  }

  private FileDeleted(Instant date, String aggregateId, String clusterId) {
    super(aggregateId, date);
    this.clusterId = clusterId;
  }

  public static FileDeleted buildFromDB(Instant date, String aggregateId, String clusterId) {
    return new FileDeleted(date, aggregateId, clusterId);
  }


  //
  // FOR TESTS
  //
  public static FileDeleted example() {
    return new FileDeleted(TEST_DATE, AGGREGATE_ID_EXAMPLE.toString(), exampleClusterId(AGGREGATE_ID_EXAMPLE));
  }
}
