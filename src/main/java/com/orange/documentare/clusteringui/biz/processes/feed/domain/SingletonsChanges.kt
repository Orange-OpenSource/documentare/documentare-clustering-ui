package com.orange.documentare.clusteringui.biz.processes.feed.domain

data class SingletonsChanges(val singletonsChanged: Boolean, val removedSingletons: List<String>, val addedSingletons: List<String>, val changeRatePercent: Float) {

  companion object {
    internal fun computeSingletonsChanges(
      singletonsAtLoopStart: List<String>,
      singletonsAtLoopEnd: List<String>
    ): SingletonsChanges {
      val removedSingletonsIds = singletonsAtLoopStart.toMutableList()
      removedSingletonsIds.removeAll(singletonsAtLoopEnd)
      val newSingletonsIds = singletonsAtLoopEnd.toMutableList()
      newSingletonsIds.removeAll(singletonsAtLoopStart)

      val changeRatePercent: Float = if (removedSingletonsIds.size == 0 && newSingletonsIds.size == 0)
        0f
      else if (singletonsAtLoopStart.isEmpty())
        100f
      else
        100f * (removedSingletonsIds.size + newSingletonsIds.size) / singletonsAtLoopStart.size

      val singletonsChanged = removedSingletonsIds.size > 0 || newSingletonsIds.size > 0

      return SingletonsChanges(singletonsChanged, removedSingletonsIds, newSingletonsIds, changeRatePercent)
    }
  }
}
