package com.orange.documentare.clusteringui.biz.file.query.hierarchy

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.*
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.query.*
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.biz.parameters.domain.DistanceParameters
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.StatisticsConsolidation
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModel
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelFile
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelLevel
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventInMemoryRepositoryForTestPurpose
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse

object HierarchyExamples {

  //
  // Hierarchy with 4 levels and one root
  //
  //                                      111111
  // Level 3                              |F0|
  //                                      ---
  //                                       |
  //                                     11111
  // Level 2                            |F0,F4|
  //                                    ------
  //                                      |
  //                         1111                      2222
  // Level 1            |F0,F2,F5,F7|                  |F4|
  //                    ------------                   ---
  //                         |                          |
  //             111        222    444                 333
  // Level 0    |F0,F1|  |F2,F3|  |F7|          |F4,F5multiset,F6|
  //            ------   ------   ---           -----------------


  fun projectionWith4LevelsOneRoot(): Projections {
    val projection = Projections(DBEventInMemoryRepositoryForTestPurpose())

    val events = listOf(
      //
      // Level 0
      //
      FileCreated(aggregateIdExample(0), "f0"),
      FilePutInNamedClass(aggregateIdExample(0), arrayOf("class A")),
      FileCreated(aggregateIdExample(1), "f1"),
      FilePutInNamedClass(aggregateIdExample(1), arrayOf("class A")),

      FileCreated(aggregateIdExample(2), "f2"),
      FilePutInNamedClass(aggregateIdExample(2), arrayOf("class B")),
      FileCreated(aggregateIdExample(3), "f3"),
      FilePutInNamedClass(aggregateIdExample(3), arrayOf("class B")),

      FileCreated(aggregateIdExample(4), "f4"),
      FilePutInNamedClass(aggregateIdExample(4), arrayOf("class C")),
      FileCreated(aggregateIdExample(5), "f5"),
      FilePutInNamedClass(aggregateIdExample(5), arrayOf("class C")),
      FileCreated(aggregateIdExample(6), "f6"),
      FilePutInNamedClass(aggregateIdExample(6), arrayOf("class C")),

      FileCreated(aggregateIdExample(7), "f7"),
      FilePutInNamedClass(aggregateIdExample(7), arrayOf("class D")),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("111").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(1)).clusterId("111").distanceToCenter(112).build(),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("222").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(3)).clusterId("222").distanceToCenter(225).build(),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(4)).clusterId("333").clusterCenter(true).build(),
      // ⚠️ interesting to test client hierarchy export: at level 0, only cluster centers + multiset files are exported
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(5)).clusterId("333").inMultiset(true).distanceToCenter(338).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(6)).clusterId("333").distanceToCenter(438).build(),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(7)).clusterId("444").clusterCenter(true).build(),

      //
      // Level 1
      //
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(0), 1),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(2), 1),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(4), 1),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(5), 1),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(7), 1),

      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(0)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(2)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(4)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(5)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(7)),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("1111").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(2)).clusterId("1111").distanceToCenter(1113).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(5)).clusterId("1111").distanceToCenter(338).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(7)).clusterId("1111").distanceToCenter(1224).build(),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(4)).clusterId("2222").clusterCenter(true).build(),

      //
      // Level 2
      //
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(0), 2),
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(4), 2),

      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(0)),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(4)),

      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("11111").clusterCenter(true).build(),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(4)).clusterId("11111").distanceToCenter(11115).build(),

      //
      // Level 3
      //
      MultisetElementMovedToNextHierarchyLevel(aggregateIdExample(0), 3),
      FileMovedFromWaitingRoomToSortRoom(aggregateIdExample(0)),
      FileAddedToCluster.builder().aggregateId(aggregateIdExample(0)).clusterId("111111").clusterCenter(true).build()
    )

    projection.testPurposeInitFromHistory(events)

    return projection
  }

  fun hierarchyViewFor(projections: Projections): HierarchiesView {
    val queryBus = QueryBus().apply {
      register(
        HierarchiesQueryHandler(projections)
      )
    }

    return HierarchiesView.get(queryBus)
  }

  fun retrieveLevelClusterStatsFor(levelIndex: Int, clusterId: String, projections: Projections): ClusterDistancesStats {
    val queryBus = QueryBus().apply {
      register(
        DistanceToCenterStatsForAllFilesInClusterQueryHandler(projections)
      )
    }

    val response = queryBus.dispatch<QueryResponse<ClusterDistancesStats>>(DistanceToCenterStatsForAllFilesInClusterQuery(clusterId, levelIndex))
    if (response.failure()) {
      throw IllegalStateException(response.failureCause())
    }
    return response.value()
  }

  //
  // LEVEL 0
  //
  // Cluster 100
  private val level0F0CenterNotSingleton =
    b().aggregateId(aggregateIdExample(0)).filename("f0").clusterId("111").fileNamedClass(arrayOf("class A")).clusterCenter(true).inWaitingRoom(null).build()
  private val level0F1 = b().aggregateId(aggregateIdExample(1)).filename("f1").clusterId("111").fileNamedClass(arrayOf("class A")).distanceToCenter(112).inWaitingRoom(null).build()

  // Cluster 200
  private val level0F2CenterNotSingleton =
    b().aggregateId(aggregateIdExample(2)).filename("f2").clusterId("222").fileNamedClass(arrayOf("class B")).clusterCenter(true).inWaitingRoom(null).build()
  private val level0F3 = b().aggregateId(aggregateIdExample(3)).filename("f3").clusterId("222").fileNamedClass(arrayOf("class B")).distanceToCenter(225).inWaitingRoom(null).build()

  // Cluster 300
  private val level0F4CenterNotSingleton =
    b().aggregateId(aggregateIdExample(4)).filename("f4").clusterId("333").fileNamedClass(arrayOf("class C")).clusterCenter(true).inWaitingRoom(null).build()
  private val level0F5Multiset =
    b().aggregateId(aggregateIdExample(5)).filename("f5").clusterId("333").inMultiset(true).fileNamedClass(arrayOf("class C")).distanceToCenter(338).inWaitingRoom(null).build()
  private val level0F6 = b().aggregateId(aggregateIdExample(6)).filename("f6").clusterId("333").fileNamedClass(arrayOf("class C")).distanceToCenter(438).inWaitingRoom(null).build()

  // Cluster 400
  private val level0F7CenterSingleton =
    b().aggregateId(aggregateIdExample(7)).filename("f7").clusterId("444").fileNamedClass(arrayOf("class D")).clusterCenter(true).inWaitingRoom(null).build()

  //
  // LEVEL 1
  //
  // Cluster 1000
  private val level1F0CenterNotSingleton =
    b().aggregateId(aggregateIdExample(0)).filename("f0").clusterId("1111").fileNamedClass(arrayOf("class A")).clusterCenter(true).inWaitingRoom(false).build()
  private val level1F2 =
    b().aggregateId(aggregateIdExample(2)).filename("f2").clusterId("1111").fileNamedClass(arrayOf("class B")).distanceToCenter(1113).inWaitingRoom(false).build()
  private val level1F5 =
    b().aggregateId(aggregateIdExample(5)).filename("f5").clusterId("1111").fileNamedClass(arrayOf("class C")).distanceToCenter(338).inWaitingRoom(false).build()
  private val level1F7 =
    b().aggregateId(aggregateIdExample(7)).filename("f7").clusterId("1111").fileNamedClass(arrayOf("class D")).distanceToCenter(1224).inWaitingRoom(false).build()

  // Cluster 2222
  private val level1F4CenterSingleton =
    b().aggregateId(aggregateIdExample(4)).filename("f4").clusterId("2222").fileNamedClass(arrayOf("class C")).clusterCenter(true).inWaitingRoom(false).build()

  //
  // LEVEL 2
  //
  // Cluster 10000
  private val level2F0CenterNotSingleton =
    b().aggregateId(aggregateIdExample(0)).filename("f0").clusterId("11111").fileNamedClass(arrayOf("class A")).clusterCenter(true).inWaitingRoom(false).build()
  private val level2F4 =
    b().aggregateId(aggregateIdExample(4)).filename("f4").clusterId("11111").fileNamedClass(arrayOf("class C")).distanceToCenter(11115).inWaitingRoom(false).build()

  //
  // LEVEL 3
  //
  // Cluster 100000
  private val level3F0CenterSingleton =
    b().aggregateId(aggregateIdExample(0)).filename("f0").clusterId("111111").fileNamedClass(arrayOf("class A")).clusterCenter(true).inWaitingRoom(false).build()


  fun hierarchiesWith4LevelsOneRoot(): Map<HierarchyLevel, Map<ClusterId, List<DomainFile>>> {
    val expectedHierarchies = mutableMapOf<HierarchyLevel, Map<ClusterId, List<DomainFile>>>()

    expectedHierarchies[0] = mutableMapOf(
      Pair("111", listOf(level0F0CenterNotSingleton, level0F1)),
      Pair("222", listOf(level0F2CenterNotSingleton, level0F3)),
      Pair("333", listOf(level0F4CenterNotSingleton, level0F5Multiset, level0F6)),
      Pair("444", listOf(level0F7CenterSingleton))
    )

    expectedHierarchies[1] = mutableMapOf(
      Pair("1111", listOf(level1F0CenterNotSingleton, level1F2, level1F5, level1F7)), Pair("2222", listOf(level1F4CenterSingleton))
    )

    expectedHierarchies[2] = mutableMapOf(
      Pair("11111", listOf(level2F0CenterNotSingleton, level2F4))
    )

    expectedHierarchies[3] = mutableMapOf(
      Pair("111111", listOf(level3F0CenterSingleton))
    )

    return expectedHierarchies
  }

  private fun toReferenceModelFile(
    domainFile: DomainFile,
    clusterDistancesStats: ClusterDistancesStats? = null,
    clusterMultiHypothesisName: String? = null /* TODO CLIENT +++++ */
  ): ClientHierarchyReferenceModelFile {
    return ClientHierarchyReferenceModelFile(
      domainFile.aggregateId,
      domainFile.filename,
      domainFile.clusterCenter(),
      domainFile.inMultiset(),
      clusterDistancesStats,
      clusterMultiHypothesisName,
      domainFile.className,
      domainFile.fileNamedClass?.toList(),
      FileIO.filePathOf(domainFile),
      FileIO.fileWebPathOf(domainFile),
      FileIO.thumbnailWebPathOf(domainFile)
    )
  }

  private const val CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_0_F0_CLUSTER_111 = "class A 100%"
  private const val CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_0_F2_CLUSTER_222 = "class B 100%"
  private const val CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_0_F4_CLUSTER_333 = "class C 100%"
  private const val CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_0_F7_CLUSTER_444 = "class D 100%"
  private const val CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_1_F0_CLUSTER_1111 = "class D 25% / class C 25% / class B 25% / class A 25%"
  private const val CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_1_F4_CLUSTER_2222 = "class C 100%"
  private const val CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_2_F0_CLUSTER_11111 = "class C 50% / class A 50%"


  private val clusterDistancesStatsLevel0F0Cluster111 = ClusterDistancesStats.buildWithDomainFiles(listOf(level0F0CenterNotSingleton, level0F1))
  private val clusterDistancesStatsLevel0F2Cluster222 = ClusterDistancesStats.buildWithDomainFiles(listOf(level0F2CenterNotSingleton, level0F3))
  private val clusterDistancesStatsLevel0F4Cluster333 = ClusterDistancesStats.buildWithDomainFiles(listOf(level0F4CenterNotSingleton, level0F5Multiset, level0F6))
  private val clusterDistancesStatsLevel0F7Cluster444 = ClusterDistancesStats.buildWithDomainFiles(listOf(level0F7CenterSingleton))

  private val clusterDistancesStatsLevel1F0Cluster1111 = ClusterDistancesStats.buildWithDomainFiles(listOf(level1F0CenterNotSingleton, level1F2, level1F5, level1F7))
  private val clusterDistancesStatsLevel1F0Cluster1111New = ClusterDistancesStats.buildWith(4, 284.0, 140.03979026384914, listOf("f0"))
  private val clusterDistancesStatsLevel1F4Cluster2222 = ClusterDistancesStats.buildWithDomainFiles(listOf(level1F4CenterSingleton))
  private val clusterDistancesStatsLevel1F4Cluster2222New = ClusterDistancesStats.buildWith(1, 438.0, 0.0, listOf("f4"))

  private val clusterDistancesStatsLevel2F0Cluster11111 = ClusterDistancesStats.buildWithDomainFiles(listOf(level2F0CenterNotSingleton, level2F4))
  private val clusterDistancesStatsLevel2F0Cluster11111New = ClusterDistancesStats.buildWith(2, 284.0, 0.0, listOf("f0"))

  private val clusterDistancesStatsLevel3F0Cluster111111 = ClusterDistancesStats.buildWithDomainFiles(listOf(level3F0CenterSingleton))

  fun clusterStatsFor(clusterId: String): ClusterDistancesStats {
    return when (clusterId) {
      "111" -> clusterDistancesStatsLevel0F0Cluster111
      "222" -> clusterDistancesStatsLevel0F2Cluster222
      "333" -> clusterDistancesStatsLevel0F4Cluster333
      "444" -> clusterDistancesStatsLevel0F7Cluster444
      "1111" -> clusterDistancesStatsLevel1F0Cluster1111
      "2222" -> clusterDistancesStatsLevel1F4Cluster2222
      "11111" -> clusterDistancesStatsLevel2F0Cluster11111

      else -> {
        throw IllegalStateException("missing test case for clusterId '$clusterId'")
      }
    }
  }

  val computedDistancesHierarchiesWith4LevelsOneRoot: StatisticsConsolidation.ComputedDistances = StatisticsConsolidation.ComputedDistances(
    listOf(
      // Level 0
      Pair(
        0, mapOf(/* TODO - not required for test */)
      ),
      // Level 1
      Pair(
        1, mapOf(
          // cluster 1
          Pair(
            StatisticsConsolidation.ComputedDistancesExamples.cca_(0, 1),
            listOf(
              // TODO - test bias: we should have the distances from the previous level as well
              Pair(StatisticsConsolidation.ComputedDistancesExamples.a_(0), 0),
              Pair(StatisticsConsolidation.ComputedDistancesExamples.a_(2), 1113),
              Pair(StatisticsConsolidation.ComputedDistancesExamples.a_(5), 338),
              Pair(StatisticsConsolidation.ComputedDistancesExamples.a_(7), 1224)
            )
          ),
          // cluster 2
          Pair(
            StatisticsConsolidation.ComputedDistancesExamples.cca_(4, 1),
            listOf(
              Pair(StatisticsConsolidation.ComputedDistancesExamples.a_(4), 0)
            )
          )
        )
      ),
      // Level 2
      Pair(
        2, mapOf(
          // cluster 1
          Pair(
            StatisticsConsolidation.ComputedDistancesExamples.cca_(0, 2),
            listOf(
              Pair(StatisticsConsolidation.ComputedDistancesExamples.a_(0), 0),
              Pair(StatisticsConsolidation.ComputedDistancesExamples.a_(4), 11115)
            )
          )
        )
      )
    )
  )

  fun clientExportRefFileLevel0F0NotSingleton() =
    toReferenceModelFile(level0F0CenterNotSingleton, clusterDistancesStatsLevel0F0Cluster111, CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_0_F0_CLUSTER_111)

  fun clientExportRefFileLevel0F2NotSingleton() =
    toReferenceModelFile(level0F2CenterNotSingleton, clusterDistancesStatsLevel0F2Cluster222, CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_0_F2_CLUSTER_222)

  fun clientExportRefFileLevel0F4NotSingleton() =
    toReferenceModelFile(level0F4CenterNotSingleton, clusterDistancesStatsLevel0F4Cluster333, CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_0_F4_CLUSTER_333)

  fun clientExportRefFileLevel0F7Singleton() =
    toReferenceModelFile(level0F7CenterSingleton, clusterDistancesStatsLevel0F7Cluster444, CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_0_F7_CLUSTER_444)

  fun clientExportRefFileLevel1F0NotSingleton() =
    toReferenceModelFile(level1F0CenterNotSingleton, clusterDistancesStatsLevel1F0Cluster1111, CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_1_F0_CLUSTER_1111)
  fun clientExportRefFileLevel1F0NotSingletonNew() =
    toReferenceModelFile(level1F0CenterNotSingleton, clusterDistancesStatsLevel1F0Cluster1111New, CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_1_F0_CLUSTER_1111)

  fun clientExportRefFileLevel2F0NotSingleton() =
    toReferenceModelFile(level2F0CenterNotSingleton, clusterDistancesStatsLevel2F0Cluster11111, CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_2_F0_CLUSTER_11111)
  fun clientExportRefFileLevel2F0NotSingletonNew() =
    toReferenceModelFile(level2F0CenterNotSingleton, clusterDistancesStatsLevel2F0Cluster11111New, CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_2_F0_CLUSTER_11111)

  fun clientExportRefFileLevel3F0Singleton() = toReferenceModelFile(level3F0CenterSingleton, clusterDistancesStatsLevel3F0Cluster111111)


  fun exportedClientHierarchiesWith4LevelsOneRootParameters() = DistanceParameters.Examples.withNonDefaultPixelCount()

  // Full with level 0 with all files WITHOUT cluster stats
  fun exportedClientHierarchiesForStatisticsConsolidation(distanceParameters: DistanceParameters = exportedClientHierarchiesWith4LevelsOneRootParameters()): ClientHierarchyReferenceModel {
    return ClientHierarchyReferenceModel(
      listOf(
        ClientHierarchyReferenceModelLevel(
          listOf(
            // level 0
            listOf(
              toReferenceModelFile(level0F0CenterNotSingleton, clusterMultiHypothesisName = CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_0_F0_CLUSTER_111),
              toReferenceModelFile(level0F1)
            ),
            listOf(
              toReferenceModelFile(level0F2CenterNotSingleton, clusterMultiHypothesisName = CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_0_F2_CLUSTER_222),
              toReferenceModelFile(level0F3)
            ),
            listOf(
              toReferenceModelFile(level0F4CenterNotSingleton, clusterMultiHypothesisName = CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_0_F4_CLUSTER_333),
              toReferenceModelFile(level0F5Multiset),
              toReferenceModelFile(level0F6)
            ),
            listOf(toReferenceModelFile(level0F7CenterSingleton, clusterMultiHypothesisName = CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_0_F7_CLUSTER_444))
          )
        ), ClientHierarchyReferenceModelLevel(
          // level 1
          listOf(
            // contains all clusters with all files
            listOf(
              toReferenceModelFile(level1F0CenterNotSingleton, clusterMultiHypothesisName = CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_1_F0_CLUSTER_1111),
              toReferenceModelFile(level1F2),
              toReferenceModelFile(level1F5),
              toReferenceModelFile(level1F7)
            ),
            listOf(toReferenceModelFile(level1F4CenterSingleton, clusterMultiHypothesisName = CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_1_F4_CLUSTER_2222))
          )
        ), ClientHierarchyReferenceModelLevel(
          // level 2
          listOf(
            // contains all clusters with all files
            listOf(
              toReferenceModelFile(level2F0CenterNotSingleton, clusterMultiHypothesisName = CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_2_F0_CLUSTER_11111),
              toReferenceModelFile(level2F4)
            )
          )
        )
        // level 3 is skipped since it contains only a singleton
      ), distanceParameters
    )
  }


  // Full: level 0 with all files; useful for statistics consolidation step
  private fun exportedClientFullHierarchiesWith4LevelsOneRoot(distanceParameters: DistanceParameters = exportedClientHierarchiesWith4LevelsOneRootParameters()): ClientHierarchyReferenceModel {
    return ClientHierarchyReferenceModel(
      listOf(
        ClientHierarchyReferenceModelLevel(
          listOf(
            // level 0
            listOf(clientExportRefFileLevel0F0NotSingleton(), toReferenceModelFile(level0F1)),
            listOf(clientExportRefFileLevel0F2NotSingleton(), toReferenceModelFile(level0F3)),
            listOf(clientExportRefFileLevel0F4NotSingleton(), toReferenceModelFile(level0F5Multiset), toReferenceModelFile(level0F6)),
            listOf(clientExportRefFileLevel0F7Singleton())
          )
        ), ClientHierarchyReferenceModelLevel(
          // level 1
          listOf(
            // contains all clusters with all files
            listOf(
              clientExportRefFileLevel1F0NotSingleton(),
              toReferenceModelFile(level1F2),
              toReferenceModelFile(level1F5),
              toReferenceModelFile(level1F7)
            ),
            listOf(
              toReferenceModelFile(level1F4CenterSingleton, clusterDistancesStatsLevel1F4Cluster2222, CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_1_F4_CLUSTER_2222)
            )
          )
        ), ClientHierarchyReferenceModelLevel(
          // level 2
          listOf(
            // contains all clusters with all files
            listOf(clientExportRefFileLevel2F0NotSingleton(), toReferenceModelFile(level2F4))
          )
        )
        // level 3 is skipped since it contains only a singleton
      ), distanceParameters
    )
  }
  private fun exportedClientFullHierarchiesWith4LevelsOneRootNew(distanceParameters: DistanceParameters = exportedClientHierarchiesWith4LevelsOneRootParameters()): ClientHierarchyReferenceModel {
    return ClientHierarchyReferenceModel(
      listOf(
        ClientHierarchyReferenceModelLevel(
          listOf(
            // level 0
            listOf(clientExportRefFileLevel0F0NotSingleton(), toReferenceModelFile(level0F1)),
            listOf(clientExportRefFileLevel0F2NotSingleton(), toReferenceModelFile(level0F3)),
            listOf(clientExportRefFileLevel0F4NotSingleton(), toReferenceModelFile(level0F5Multiset), toReferenceModelFile(level0F6)),
            listOf(clientExportRefFileLevel0F7Singleton())
          )
        ), ClientHierarchyReferenceModelLevel(
          // level 1
          listOf(
            // contains all clusters with all files
            listOf(
              clientExportRefFileLevel1F0NotSingletonNew(),
              toReferenceModelFile(level1F2),
              toReferenceModelFile(level1F5),
              toReferenceModelFile(level1F7)
            ),
            listOf(
              toReferenceModelFile(level1F4CenterSingleton, clusterDistancesStatsLevel1F4Cluster2222New, CLUSTER_MULTI_HYPOTHESIS_NAME_LEVEL_1_F4_CLUSTER_2222)
            )
          )
        ), ClientHierarchyReferenceModelLevel(
          // level 2
          listOf(
            // contains all clusters with all files
            listOf(clientExportRefFileLevel2F0NotSingletonNew(), toReferenceModelFile(level2F4))
          )
        )
        // level 3 is skipped since it contains only a singleton
      ), distanceParameters
    )
  }

  fun exportedClientHierarchiesWith4LevelsOneRoot(distanceParameters: DistanceParameters = exportedClientHierarchiesWith4LevelsOneRootParameters()): ClientHierarchyReferenceModel {
    return ClientHierarchyExport.cleanLevel0(exportedClientFullHierarchiesWith4LevelsOneRoot(distanceParameters))
  }
  fun exportedClientHierarchiesWith4LevelsOneRootNew(distanceParameters: DistanceParameters = exportedClientHierarchiesWith4LevelsOneRootParameters()): ClientHierarchyReferenceModel {
    return ClientHierarchyExport.cleanLevel0(exportedClientFullHierarchiesWith4LevelsOneRootNew(distanceParameters))
  }

  fun clientHierarchyExport(distanceParameters: DistanceParameters = exportedClientHierarchiesWith4LevelsOneRootParameters()) = object : ClientHierarchyExport {
    override fun exportClientHierarchy(): ClientHierarchyReferenceModel {
      return exportedClientHierarchiesWith4LevelsOneRoot(distanceParameters)
    }

    override fun exportClientHierarchyForStatisticsConsolidation(): ClientHierarchyReferenceModel {
      return exportedClientHierarchiesForStatisticsConsolidation(distanceParameters)
    }
  }

  fun treantModelWith4LevelsOneRoot() = """{
  "text" : {
    "name" : "Root"
  },
  "children" : [ {
    "text" : {
      "name" : "f0"
    },
    "children" : [ {
      "children" : [ {
        "children" : [ {
          "text" : {
            "name" : "f1"
          },
          "children" : [ ],
          "link" : {
            "href" : "/01000000-eee5-4abd-b85b-161928ae3cd3"
          }
        } ]
      }, {
        "text" : {
          "name" : "f2"
        },
        "children" : [ {
          "text" : {
            "name" : "f3"
          },
          "children" : [ ],
          "link" : {
            "href" : "/03000000-eee5-4abd-b85b-161928ae3cd3"
          }
        } ],
        "link" : {
          "href" : "/02000000-eee5-4abd-b85b-161928ae3cd3"
        }
      }, {
        "text" : {
          "name" : "f5"
        },
        "children" : [ {
          "text" : {
            "name" : "f4"
          },
          "children" : [ ],
          "link" : {
            "href" : "/04000000-eee5-4abd-b85b-161928ae3cd3"
          }
        }, {
          "text" : {
            "name" : "f6"
          },
          "children" : [ ],
          "link" : {
            "href" : "/06000000-eee5-4abd-b85b-161928ae3cd3"
          }
        } ],
        "link" : {
          "href" : "/05000000-eee5-4abd-b85b-161928ae3cd3"
        }
      }, {
        "children" : [ {
          "text" : {
            "name" : "f7"
          },
          "children" : [ ],
          "link" : {
            "href" : "/07000000-eee5-4abd-b85b-161928ae3cd3"
          }
        } ]
      } ]
    }, {
      "children" : [ {
        "text" : {
          "name" : "f4"
        },
        "children" : [ {
          "text" : {
            "name" : "f5"
          },
          "children" : [ ],
          "link" : {
            "href" : "/05000000-eee5-4abd-b85b-161928ae3cd3"
          }
        }, {
          "text" : {
            "name" : "f6"
          },
          "children" : [ ],
          "link" : {
            "href" : "/06000000-eee5-4abd-b85b-161928ae3cd3"
          }
        } ],
        "link" : {
          "href" : "/04000000-eee5-4abd-b85b-161928ae3cd3"
        }
      } ]
    } ],
    "link" : {
      "href" : "/00000000-eee5-4abd-b85b-161928ae3cd3"
    }
  } ]
}"""


  fun hierarchiesWith2LevelsTwoRootsFilesIds() = listOf(aggregateIdExample(0), aggregateIdExample(1), aggregateIdExample(2))

  fun hierarchiesWith2LevelsTwoRoots(): Map<HierarchyLevel, Map<ClusterId, List<DomainFile>>> {
    val expectedHierarchies = mutableMapOf<HierarchyLevel, Map<ClusterId, List<DomainFile>>>()

    expectedHierarchies[0] = mutableMapOf(
      Pair(
        "111", listOf(
          b().aggregateId(aggregateIdExample(0)).filename("f0").clusterId("111").clusterCenter(true).inWaitingRoom(false).build(),
          b().aggregateId(aggregateIdExample(1)).filename("f1").clusterId("111").inWaitingRoom(false).build()
        )
      ), Pair(
        "222", listOf(
          b().aggregateId(aggregateIdExample(2)).filename("f2").clusterId("222").clusterCenter(true).inWaitingRoom(false).build(),
        )
      )
    )

    expectedHierarchies[1] = mutableMapOf(
      Pair(
        "1111", listOf(
          b().aggregateId(aggregateIdExample(0)).filename("f0").clusterId("1111").clusterCenter(true).inWaitingRoom(false).build(),
        )
      ), Pair(
        "2222", listOf(
          b().aggregateId(aggregateIdExample(2)).filename("f2").clusterId("2222").clusterCenter(true).inWaitingRoom(false).build(),
        )
      )
    )

    return expectedHierarchies
  }

  private fun b() = DomainFile.builder()

  fun treantModelWith2LevelsTwoRoots() = """
{
  "text" : {
    "name" : "Root"
  },
  "children" : [ {
    "text" : {
      "name" : "f0"
    },
    "children" : [ {
      "text" : {
        "name" : "f1"
      },
      "children" : [ ],
      "image" : "/thumbnails/${aggregateIdExample(1)}",
      "link" : {
        "href" : "/${aggregateIdExample(1)}"
      }
    } ],
    "image" : "/thumbnails/${aggregateIdExample(0)}",
    "link" : {
      "href" : "/${aggregateIdExample(0)}"
    }
  }, {
    "children" : [ {
      "text" : {
        "name" : "f2"
      },
      "children" : [ ],
      "image" : "/thumbnails/${aggregateIdExample(2)}",
      "link" : {
        "href" : "/${aggregateIdExample(2)}"
      }
    } ]
  } ]
}""".trimIndent()
}
