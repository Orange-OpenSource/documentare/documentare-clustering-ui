package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.persistence.EventRepository


class AutoAddFileToClosestClusterCommandHandler(private val eventRepository: EventRepository, eventBus: EventBus) :
  CommandHandler<AutoAddFileToClosestClusterCommand, Void>(eventBus) {

  override fun doExecute(command: AutoAddFileToClosestClusterCommand): AggregateAndEvents<Void?, Collection<Event>> {
    val domainFile = loadFile(command.fileId) ?: return AggregateAndEvents.withOnlyEvents(emptyList())

    val closestClusterFiles = command.closestClusterFileIds.mapNotNull { loadFile(it) }
    val multisetFiles = closestClusterFiles.filter { it.inMultiset() }
    val clusterLock = closestClusterFiles.filter { it.clusterCenter() && it.locked() }
    val clusterIsLocked = clusterLock.isNotEmpty()

    var updateClusterCenter: Event? = null
    if (clusterIsLocked) {
      if (multisetFiles.size <= command.maxMultisetThreshold) {
        updateClusterCenter = tagClusterModified(closestClusterFiles)
      }
    } else {
      updateClusterCenter = tagClusterModified(closestClusterFiles)
    }

    val updateFile = domainFile.moveToCluster(
      command.distanceToCluster.clusterId, null, command.distanceToCluster.distance, true, null, null, null
    )

    return if (updateClusterCenter != null) {
      AggregateAndEvents.withOnlyEvents(listOf(updateFile, updateClusterCenter))
    } else {
      AggregateAndEvents.withOnlyEvents(listOf<Event>(updateFile))
    }
  }

  private fun tagClusterModified(closestClusterFiles: List<DomainFile>) = closestClusterFiles
    .filter { it.clusterCenter() }
    .map { it.tagClusterModified() }
    .first()

  private fun loadFile(fileId: String) = eventRepository.load(fileId)
}
