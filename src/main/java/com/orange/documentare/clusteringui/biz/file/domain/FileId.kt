package com.orange.documentare.clusteringui.biz.file.domain

data class FileId(val fileId: String)
