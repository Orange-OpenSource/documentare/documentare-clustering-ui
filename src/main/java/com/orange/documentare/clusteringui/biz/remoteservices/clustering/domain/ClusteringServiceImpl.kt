package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.orange.documentare.clusteringui.EmbeddedComputationServer
import com.orange.documentare.clusteringui.biz.appnotifications.AppNotifications
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.DistancesComputedAndClustersRecalculated
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.GlobalOrPartialClusteringComputed
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure.ClusteringRemote
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure.ClusteringRemoteApi
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceService
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.queries.QueryBus
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class ClusteringServiceImpl(private var distanceService: DistanceService, private val appNotifications: AppNotifications, embeddedComputationServer: EmbeddedComputationServer) :
  ClusteringService {
  private val log = LoggerFactory.getLogger(ClusteringServiceImpl::class.java)
  private val inProgress = ClusteringInProgress()

  private lateinit var queryBus: QueryBus
  private lateinit var commandBus: CommandBus

  private var clusteringRemote: ClusteringRemoteApi = ClusteringRemote(embeddedComputationServer)

  override fun isRunning() = inProgress.inProgress()

  fun globalOrPartialClusteringAsync(parameters: Parameters, files: List<DomainFile>, filesIdsInBinToPurge: List<String>) {
    log.info("[Async] Global or partial clustering")
    inProgress.raiseExceptionIfAlreadyRunning()
    GlobalOrPartialClustering()
      .doAsync(clusteringInfra(), parameters, files, filesIdsInBinToPurge)
  }

  override fun globalOrPartialClusteringSync(parameters: Parameters, allFilesToSort: List<DomainFile>): GlobalOrPartialClusteringComputed? {
    log.info("[Sync] Global or partial clustering")
    inProgress.raiseExceptionIfAlreadyRunning()
    return GlobalOrPartialClustering()
      .doSync(clusteringInfra(), parameters, allFilesToSort)
  }

  fun createNewClusterAsync(parameters: Parameters, files: List<DomainFile>, filesIdsInBinToPurge: List<String>) {
    log.info("[Async] Create a new cluster")
    inProgress.raiseExceptionIfAlreadyRunning()
    CreateNewCluster()
      .doAsync(clusteringInfra(), parameters, files, filesIdsInBinToPurge)
  }

  fun recalculateClustersThenComputeDistancesAsync(parameters: Parameters, clustersModified: Map<String, List<DomainFile>>, fileIdsToSort: List<String>) {
    log.info("[Async] Recalculate clusters then Compute distances")
    inProgress.raiseExceptionIfAlreadyRunning()
    RecalculateClustersThenComputeDistances(clusteringInfra())
      .doAsync(parameters, clustersModified, fileIdsToSort)
  }

  override fun computeDistancesThenRecalculateClustersSync(parameters: Parameters): DistancesComputedAndClustersRecalculated {
    log.info("[Sync] Compute distances then Recalculate clusters")
    inProgress.raiseExceptionIfAlreadyRunning()
    return ComputeDistancesThenRecalculateClusters(clusteringInfra())
      .doSync(parameters)
  }

  private fun clusteringInfra(): ClusteringInfra {
    return ClusteringInfra(commandBus, queryBus, clusteringRemote, distanceService, inProgress, appNotifications)
  }

  fun setCommandBus(commandBus: CommandBus) {
    this.commandBus = commandBus
  }

  fun setQueryBus(queryBus: QueryBus) {
    this.queryBus = queryBus
  }

  fun testAlreadyInProgress() {
    inProgress.setInProgress(true)
  }

  fun testSetDistanceService(distanceService: DistanceService) {
    this.distanceService = distanceService
  }

  fun testClusteringRemote(clusteringRemote: ClusteringRemoteApi) {
    this.clusteringRemote = clusteringRemote
  }
}
