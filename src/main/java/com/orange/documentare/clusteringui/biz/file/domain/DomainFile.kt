package com.orange.documentare.clusteringui.biz.file.domain

import com.orange.documentare.clusteringui.biz.file.event.*
import com.orange.documentare.clusteringui.infrastructure.io.FileIO
import com.tophe.ddd.infrastructure.event.Event
import java.util.Comparator.comparing
import java.util.stream.Collectors

data class DomainFile(
  val aggregateId: String,
  val filename: String,
  val clusterId: String?,
  val clusterCenter: Boolean?,
  val distancesToClustersCenters: List<DistanceToCluster>?,
  val duplicate: Boolean?,
  private val inMultiset: Boolean?,
  val distanceToCenter: Int?,
  val automaticInclude: Boolean?,
  val clusterModified: Boolean?,
  val delete: Boolean?,
  val inWaitingRoom: Boolean?,
  val lock: Boolean?,
  val className: String?,
  // TODO refactor to List to avoid Array compare and equals issues
  val fileNamedClass: Array<String>?
) {
  class Examples {
    companion object {
      fun addedFile() = builder().aggregateId("id").filename("filename").build()
    }

  }

  fun hasClusterId(): Boolean = clusterId != null

  fun clusterCenter(): Boolean = clusterCenter ?: false

  // A cluster center is also considered as a multiset file
  // A cluster built with a global or partial clustering will only contain a cluster center, and NO other multiset files  ('inMultiset' is left null in the clustering result for cluster centers)
  // A consolidated cluster (recalculated after a file auto-inclusion) may contain multiset files  ('inMultiset' is true also for the cluster center)
  fun inMultiset(): Boolean = inMultiset ?: (clusterCenter ?: false)

  fun automaticInclude(): Boolean = automaticInclude ?: false

  fun clusterModified(): Boolean = clusterModified ?: false

  fun duplicated(): Boolean = duplicate ?: false

  fun deleted(): Boolean = delete ?: false

  fun notInCentroidNorIncludedAutomatically(): Boolean = !inMultiset() && !automaticInclude()

  fun isInWaitingRoom(): Boolean = inWaitingRoom ?: false

  fun locked(): Boolean = lock ?: false

  fun hasClassName(): Boolean = className != null

  fun tagClusterModified(): Event {
    checkDuplicateModification()
    return ClusterModified(aggregateId, clusterId)
  }

  fun tagClusterNotModified(): Event {
    checkDuplicateModification()
    return ClusterNotModified(aggregateId, clusterId)
  }

  fun tagClusterLocked(): Event {
    checkDuplicateModification()
    return ClusterLocked(aggregateId, clusterId, true)
  }

  fun clusterNamed(className: String?): Event {
    checkDuplicateModification()
    return ClusterPutInNamedClass(aggregateId, clusterId, className)
  }

  fun putInNamedClass(fileNamedClass: Array<String>): FilePutInNamedClass {
    checkDuplicateModification()
    return FilePutInNamedClass(aggregateId, fileNamedClass)
  }

  fun tagClusterUnLocked(): Event {
    checkDuplicateModification()
    return ClusterUnlocked(
      aggregateId,
      clusterId,
      false,
      null
    )
  }

  fun moveToCluster(
    clusterId: String?,
    clusterCenter: Boolean?,
    distanceToCenter: Int?,
    automaticInclude: Boolean?,
    inMultiset: Boolean?,
    lock: Boolean?,
    className: String?
  ): FileAddedToCluster {
    checkDuplicateModification()
    return FileAddedToCluster.builder().aggregateId(aggregateId).clusterId(clusterId).clusterCenter(clusterCenter).distanceToCenter(distanceToCenter)
      .automaticInclude(automaticInclude)
      .inMultiset(inMultiset).lock(lock).className(className).build()
  }

  fun addDistancesToClustersCenters(distancesToClustersCenters: List<DistanceToCluster>): Event? {
    checkDuplicateModification()
    return if (this.distancesToClustersCenters != null && this.distancesToClustersCenters == distancesToClustersCenters) null else DistancesToClusterCenterAdded(
      aggregateId,
      first5MinDistances(distancesToClustersCenters)
    )
  }

  private fun first5MinDistances(distancesToClustersCenters: List<DistanceToCluster>): List<DistanceToCluster> =
    distancesToClustersCenters.sortedWith(comparing { dist: DistanceToCluster -> dist.distance }).take(5)

  private fun removeFromClusterAndPutInWaitingRoom(): FileRemovedFromCluster {
    checkDuplicateModification()
    return FileRemovedFromCluster(aggregateId, clusterId, true)
  }

  private fun removeFromClusterAndPutInSortRoom(): FileRemovedFromCluster {
    checkDuplicateModification()
    return FileRemovedFromCluster(aggregateId, clusterId, null)
  }

  private fun writeOnFilesystem(bytes: ByteArray) {
    FileIO.write(this, bytes)
    FileIO.createThumbnail(this)
  }

  fun duplicate(): Event? {
    return if (deleted()) {
      null
    } else FileDuplicated(aggregateId, clusterId)
  }

  fun delete(): Event = FileDeleted(aggregateId, clusterId)

  fun resetFileInfo(): Event {
    checkDuplicateModification()
    return FileInfoReset(aggregateId, clusterId)
  }

  fun moveFromWaitingRoomToSortRoom(): Event {
    checkDuplicateModification()
    return FileMovedFromWaitingRoomToSortRoom(aggregateId)
  }

  fun apply(event: Event): DomainFile = DomainFileHistory.apply(this, event)

  private fun checkDuplicateModification() {
    if (duplicated()) {
      val e: RuntimeException = IllegalStateException("Try to modify a duplicate: $this")
      e.printStackTrace()
      throw e
    }
  }

  fun toBuilder(): DomainFileBuilder =
    DomainFileBuilder().aggregateId(aggregateId).filename(filename).clusterId(clusterId).clusterCenter(clusterCenter).distancesToClustersCenters(distancesToClustersCenters)
      .duplicate(duplicate)
      .inMultiset(inMultiset).distanceToCenter(distanceToCenter).automaticInclude(automaticInclude).clusterModified(clusterModified).delete(delete).inWaitingRoom(inWaitingRoom)
      .lock(lock).className(className).fileNamedClass(fileNamedClass)


  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false

    other as DomainFile

    if (aggregateId != other.aggregateId) return false
    if (filename != other.filename) return false
    if (clusterId != other.clusterId) return false
    if (clusterCenter != other.clusterCenter) return false
    if (distancesToClustersCenters != other.distancesToClustersCenters) return false
    if (duplicate != other.duplicate) return false
    if (inMultiset != other.inMultiset) return false
    if (distanceToCenter != other.distanceToCenter) return false
    if (automaticInclude != other.automaticInclude) return false
    if (clusterModified != other.clusterModified) return false
    if (delete != other.delete) return false
    if (inWaitingRoom != other.inWaitingRoom) return false
    if (lock != other.lock) return false
    if (className != other.className) return false
    if (fileNamedClass != null) {
      if (other.fileNamedClass == null) return false
      if (!fileNamedClass.contentEquals(other.fileNamedClass)) return false
    } else if (other.fileNamedClass != null) return false

    return true
  }

  override fun hashCode(): Int {
    var result = aggregateId.hashCode()
    result = 31 * result + filename.hashCode()
    result = 31 * result + (clusterId?.hashCode() ?: 0)
    result = 31 * result + (clusterCenter?.hashCode() ?: 0)
    result = 31 * result + (distancesToClustersCenters?.hashCode() ?: 0)
    result = 31 * result + (duplicate?.hashCode() ?: 0)
    result = 31 * result + (inMultiset?.hashCode() ?: 0)
    result = 31 * result + (distanceToCenter ?: 0)
    result = 31 * result + (automaticInclude?.hashCode() ?: 0)
    result = 31 * result + (clusterModified?.hashCode() ?: 0)
    result = 31 * result + (delete?.hashCode() ?: 0)
    result = 31 * result + (inWaitingRoom?.hashCode() ?: 0)
    result = 31 * result + (lock?.hashCode() ?: 0)
    result = 31 * result + (className?.hashCode() ?: 0)
    result = 31 * result + (fileNamedClass?.contentHashCode() ?: 0)
    return result
  }


  companion object {
    fun create(id: String?, filename: String?, clusterId: String?, clusterCenter: Boolean?, bytes: ByteArray): Event {
      val domainFile = builder().aggregateId(id).filename(filename).clusterId(clusterId).clusterCenter(clusterCenter).build()
      val fileCreated = FileCreated(id, filename, clusterId, clusterCenter)
      domainFile.writeOnFilesystem(bytes)
      return fileCreated
    }

    fun createInWaitingRoom(id: String?, filename: String?, bytes: ByteArray, inWaitingRoom: Boolean?): Event {
      val domainFile = builder().aggregateId(id).filename(filename).inWaitingRoom(inWaitingRoom).build()
      val fileCreated = FileCreated(id, filename, inWaitingRoom)
      domainFile.writeOnFilesystem(bytes)
      return fileCreated
    }

    fun idsOf(files: List<DomainFile>): List<String> = files.map { it.aggregateId }

    fun removeFilesFromClusters(files: List<DomainFile>, clusters: Map<String, List<DomainFile>>, fileInRoom: Room): DestroyedClustersAndUpdatedFiles {
      val destroyedClusters: MutableList<String> = mutableListOf()
      val events: MutableList<Event> = ArrayList()
      clusters.forEach { (clusterId: String, clusterFiles: List<DomainFile>) ->
        val toRemove = removeFilesFromCluster(
          clusterId,
          clusterFiles,
          files.stream().filter { f: DomainFile -> f.hasClusterId() && f.clusterId == clusterId }.collect(Collectors.toList()),
          fileInRoom
        )
        events.addAll(toRemove.events)
        if (toRemove.destroyedCluster != null) {
          destroyedClusters.add(toRemove.destroyedCluster)
        }
      }
      return DestroyedClustersAndUpdatedFiles(destroyedClusters, events)
    }

    fun removeFilesFromCluster(clusterId: String?, clusterFiles: List<DomainFile>, filesToRemoveFromCluster: List<DomainFile>, fileInRoom: Room): RemovedFilesFromCluster {
      val removeClusterCenter = filesToRemoveFromCluster.stream().anyMatch { obj: DomainFile -> obj.clusterCenter() }
      if (removeClusterCenter) {
        return if (fileInRoom == Room.WAITING_ROOM) {
          RemovedFilesFromCluster(clusterId, clusterFiles.stream().map { obj: DomainFile -> obj.removeFromClusterAndPutInWaitingRoom() }.collect(Collectors.toList()))
        } else {
          RemovedFilesFromCluster(clusterId, clusterFiles.stream().map { obj: DomainFile -> obj.removeFromClusterAndPutInSortRoom() }.collect(Collectors.toList()))
        }
      }
      return if (fileInRoom == Room.WAITING_ROOM) {
        RemovedFilesFromCluster(null, filesToRemoveFromCluster.stream().map { obj: DomainFile -> obj.removeFromClusterAndPutInWaitingRoom() }.collect(Collectors.toList()))
      } else {
        RemovedFilesFromCluster(null, filesToRemoveFromCluster.stream().map { obj: DomainFile -> obj.removeFromClusterAndPutInSortRoom() }.collect(Collectors.toList()))
      }
    }

    fun builder(): DomainFileBuilder = DomainFileBuilder()
  }
}
