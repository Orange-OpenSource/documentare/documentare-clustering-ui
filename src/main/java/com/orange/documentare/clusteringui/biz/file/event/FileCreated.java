package com.orange.documentare.clusteringui.biz.file.event;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;

import java.time.Instant;

import com.tophe.ddd.infrastructure.event.Event;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FileCreated extends Event {

  public final String filename;
  public final String clusterId;
  public final Boolean clusterCenter;
  public final Boolean waitingRoom;

  public FileCreated(String aggregateId, String filename) {
    this(aggregateId, filename, null, null, null);
  }

  public FileCreated(String aggregateId, String filename, String clusterId, Boolean clusterCenter) {
    this(aggregateId, filename, clusterId, clusterCenter, null);
  }

  public FileCreated(String aggregateId, String filename, Boolean waitingRoom) {
    this(aggregateId, filename, null, null, waitingRoom);
  }

  private FileCreated(String aggregateId, String filename, String clusterId, Boolean clusterCenter, Boolean waitingRoom) {
    super(aggregateId);
    this.filename = filename;
    this.clusterId = clusterId;
    this.clusterCenter = clusterCenter;
    this.waitingRoom = waitingRoom;
  }

  public boolean clusterCenter() {
    return clusterCenter != null && clusterCenter;
  }

  private FileCreated(Instant date, String aggregateId, String filename, String clusterId, Boolean clusterCenter, Boolean waitingRoom) {
    super(aggregateId, date);

    this.filename = filename;
    this.clusterId = clusterId;
    this.clusterCenter = clusterCenter;
    this.waitingRoom = waitingRoom;
  }

  public static FileCreated buildFromDB(Instant date, String aggregateId, String filename, String clusterId, Boolean clusterCenter, Boolean waitingRoom) {
    return new FileCreated(date, aggregateId, filename, clusterId, clusterCenter, waitingRoom);
  }


  //
  // FOR TESTS
  //
  public static String aggregateIdExample(int index) {
    return switch (index) {
      case 0 -> "00000000-eee5-4abd-b85b-161928ae3cd3";
      case 1 -> "01000000-eee5-4abd-b85b-161928ae3cd3";
      case 2 -> "02000000-eee5-4abd-b85b-161928ae3cd3";
      case 3 -> "03000000-eee5-4abd-b85b-161928ae3cd3";
      case 4 -> "04000000-eee5-4abd-b85b-161928ae3cd3";
      case 5 -> "05000000-eee5-4abd-b85b-161928ae3cd3";
      case 6 -> "06000000-eee5-4abd-b85b-161928ae3cd3";
      case 7 -> "07000000-eee5-4abd-b85b-161928ae3cd3";
      case 8 -> "08000000-eee5-4abd-b85b-161928ae3cd3";
      case 9 -> "09000000-eee5-4abd-b85b-161928ae3cd3";
      case 10 -> "10000000-eee5-4abd-b85b-161928ae3cd3";
      case 11 -> "11000000-eee5-4abd-b85b-161928ae3cd3";
      case 12 -> "12000000-eee5-4abd-b85b-161928ae3cd3";
      case 13 -> "13000000-eee5-4abd-b85b-161928ae3cd3";
      case 14 -> "14000000-eee5-4abd-b85b-161928ae3cd3";
      case 15 -> "15000000-eee5-4abd-b85b-161928ae3cd3";
      case 16 -> "16000000-eee5-4abd-b85b-161928ae3cd3";
      default -> throw new IllegalStateException("add more index here please");
    };
  }

  public static int aggregateExampleIndex(String aggregateIdExample) {
    return Integer.parseInt(aggregateIdExample.substring(0, 2));
  }

  public static FileCreated example() {
    return new FileCreated(TEST_DATE, AGGREGATE_ID_EXAMPLE.toString(), exampleFilename(AGGREGATE_ID_EXAMPLE),
      exampleClusterId(AGGREGATE_ID_EXAMPLE), true, true);
  }
}
