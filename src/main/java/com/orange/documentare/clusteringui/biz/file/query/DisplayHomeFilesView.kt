package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample

data class DisplayHomeFilesView(
  val currentHierarchyLevel: Int,
  val noFiles: Boolean = false,
  val notDeleted: List<FileUIView>? = null,
  val inWaitingRoom: List<FileUIView>? = null,
  val notInClusters: List<FileUIView>? = null,
  val mapClassViews: Map<String, Map<String, ClusterUIView>>? = null,
  val computeDistancesInProgress: Boolean = false,
  val clusteringInProgress: Boolean = false,
  val feedInProgress: Boolean = false,
  val feedStop: Boolean = false
) {
  class Examples {
    companion object {
      fun clustersInClasses(): DisplayHomeFilesView {
        // Class A
        // Cluster C1
        val f1InClusterC1InClassA = DomainFile.builder().aggregateId(aggregateIdExample(1)).filename("F1").clusterCenter(true).clusterId("C1").className("A").build()
        val f2InClusterC1InClassA = DomainFile.builder().aggregateId(aggregateIdExample(2)).filename("F2").inMultiset(true).clusterId("C1").build()
        val f3InClusterC1InClassA = DomainFile.builder().aggregateId(aggregateIdExample(3)).filename("F3").clusterId("C1").distanceToCenter(300).build()

        // Cluster C2
        val clusterCenterF4InClusterC2InClassA = DomainFile.builder().aggregateId(aggregateIdExample(4)).filename("F4").clusterCenter(true).clusterId("C2").className("A").build()
        val fileF5InClusterC2InClassA = DomainFile.builder().aggregateId(aggregateIdExample(5)).filename("F5").clusterId("C2").distanceToCenter(500).build()

        // No classname
        // Cluster C3
        val clusterCenterF6InClusterC3NoClassName = DomainFile.builder().aggregateId("6").filename("F6").clusterCenter(true).clusterId("C3").build()


        val mapClasses = mapOf(
          "A" to mapOf(
            "C1" to ClusterUIView("C1", listOf(FileUIView.from(f1InClusterC1InClassA), FileUIView.from(f2InClusterC1InClassA), FileUIView.from(f3InClusterC1InClassA))),
            "C2" to ClusterUIView("C2", listOf(FileUIView.from(clusterCenterF4InClusterC2InClassA), FileUIView.from(fileF5InClusterC2InClassA)))
          ),
          DisplayHierarchyLevelFileTransform.CLASS_NO_NAME to mapOf("C3" to ClusterUIView("C3", listOf(FileUIView.from(clusterCenterF6InClusterC3NoClassName)))),
        )
        return DisplayHomeFilesView(
          0,
          noFiles = false,
          notDeleted = emptyList(),
          notInClusters = emptyList(),
          inWaitingRoom = emptyList(),
          mapClassViews = mapClasses
        )
      }

      fun clustersInClassesWithFileNamedClass(): DisplayHomeFilesView {
        // Class A
        // Cluster C1
        val f1InClusterC1InClassA =
          DomainFile.builder().aggregateId(aggregateIdExample(1)).filename("F1").clusterCenter(true).clusterId("C1").className("A").fileNamedClass(arrayOf("titi")).build()
        val f2InClusterC1InClassA = DomainFile.builder().aggregateId(aggregateIdExample(2)).filename("F2").inMultiset(true).clusterId("C1").fileNamedClass(arrayOf("toto")).build()
        val f3InClusterC1InClassA =
          DomainFile.builder().aggregateId(aggregateIdExample(3)).filename("F3").clusterId("C1").distanceToCenter(300).fileNamedClass(arrayOf("tutu")).build()

        // Cluster C2
        val clusterCenterF4InClusterC2InClassA =
          DomainFile.builder().aggregateId(aggregateIdExample(4)).filename("F4").clusterCenter(true).clusterId("C2").className("A").fileNamedClass(arrayOf("titi")).build()
        val fileF5InClusterC2InClassA =
          DomainFile.builder().aggregateId(aggregateIdExample(5)).filename("F5").clusterId("C2").distanceToCenter(500).fileNamedClass(arrayOf("toto")).build()

        // No classname
        // Cluster C3
        val clusterCenterF6InClusterC3NoClassName = DomainFile.builder().aggregateId("6").filename("F6").clusterCenter(true).clusterId("C3").fileNamedClass(arrayOf("tutu")).build()


        val mapClasses = mapOf(
          "A" to mapOf(
            "C1" to ClusterUIView("C1", listOf(FileUIView.from(f1InClusterC1InClassA), FileUIView.from(f2InClusterC1InClassA), FileUIView.from(f3InClusterC1InClassA))),
            "C2" to ClusterUIView("C2", listOf(FileUIView.from(clusterCenterF4InClusterC2InClassA), FileUIView.from(fileF5InClusterC2InClassA)))
          ),
          DisplayHierarchyLevelFileTransform.CLASS_NO_NAME to mapOf("C3" to ClusterUIView("C3", listOf(FileUIView.from(clusterCenterF6InClusterC3NoClassName)))),
        )
        return DisplayHomeFilesView(
          0,
          noFiles = false,
          notDeleted = emptyList(),
          notInClusters = emptyList(),
          inWaitingRoom = emptyList(),
          mapClassViews = mapClasses
        )
      }

      fun invalidStateWithFileWithoutDistanceToCenter(): DisplayHomeFilesView {
        // Class A
        // Cluster C1
        val clusterCenterF1InClusterC1InClassA = DomainFile.builder().aggregateId(aggregateIdExample(1)).filename("F1").clusterCenter(true).clusterId("C1").className("A").build()
        val fileF2InClusterC1InClassA = DomainFile.builder().aggregateId(aggregateIdExample(2)).filename("F2").clusterId("C1").build()

        val mapClasses = mapOf(
          "A" to mapOf(
            "C1" to ClusterUIView("C1", listOf(FileUIView.from(clusterCenterF1InClusterC1InClassA), FileUIView.from(fileF2InClusterC1InClassA)))
          )
        )
        return DisplayHomeFilesView(
          0,
          noFiles = false,
          notDeleted = emptyList(),
          notInClusters = emptyList(),
          inWaitingRoom = emptyList(),
          mapClassViews = mapClasses
        )
      }
    }
  }
}
