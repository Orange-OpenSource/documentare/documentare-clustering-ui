package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile

interface MultisetBalance {
  fun isFinished(): Boolean

  companion object {
    fun get(filesInWaitingRoom: List<DomainFile>, multisetMinThresholdReached: Boolean, singletonsChangedSinceLastLoop: Boolean): MultisetBalance = object : MultisetBalance {
      override fun isFinished() = filesInWaitingRoom.isEmpty() && multisetMinThresholdReached && !singletonsChangedSinceLastLoop
    }
  }
}
