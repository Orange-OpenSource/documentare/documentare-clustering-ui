package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.ClusterId
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.domain.FileId
import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.file.event.FileCreated.aggregateIdExample
import com.orange.documentare.clusteringui.biz.file.event.FilePutInNamedClass
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus

data class MultiHypothesisClustersView(val multiHypothesisClustersView: Map<ClusterId, List<DomainFile>>) {
  class Examples {
    companion object {
      private val f3 =
        DomainFile.builder().aggregateId(aggregateIdExample(10)).filename("filename 10").clusterId("cluster 10, multi-hypothesis").clusterCenter(true)
          .fileNamedClass(arrayOf("name 10")).build()
      private val f4DistinctFromF3 =
        DomainFile.builder().aggregateId(aggregateIdExample(11)).filename("filename 11").clusterId(f3.clusterId).fileNamedClass(arrayOf("another name")).build()

      private val f5LikeF3 = DomainFile.builder().aggregateId(aggregateIdExample(12)).filename("filename 12").clusterId(f3.clusterId).fileNamedClass(f3.fileNamedClass).build()

      fun oneMultiHypothesisClusterWithTwoFiles() =
        MultiHypothesisClustersView(mapOf(Pair(ClusterId(f3.clusterId!!), listOf(f3, f4DistinctFromF3))))

      fun oneMultiHypothesisClusterWithTwoFilesInit(eventBus: EventBus) {
        val events: List<Event> = listOf(
          FileCreated(aggregateIdExample(0), "not in a cluster"),
          FilePutInNamedClass(aggregateIdExample(0), arrayOf("name 0")),

          FileCreated(aggregateIdExample(1), "1", "cluster 1, no multi-hypothesis", true),
          FilePutInNamedClass(aggregateIdExample(1), arrayOf("name 1")),
          FileCreated(aggregateIdExample(2), "2", "cluster 1, no multi-hypothesis", false),
          FilePutInNamedClass(aggregateIdExample(2), arrayOf("name 1")),

          FileCreated(f3.aggregateId, f3.filename, f3.clusterId, f3.clusterCenter),
          FilePutInNamedClass(f3.aggregateId, f3.fileNamedClass),
          FileCreated(f4DistinctFromF3.aggregateId, f4DistinctFromF3.filename, f4DistinctFromF3.clusterId, f4DistinctFromF3.clusterCenter),
          FilePutInNamedClass(f4DistinctFromF3.aggregateId, f4DistinctFromF3.fileNamedClass),
        )
        eventBus.dispatch(events)
      }

      fun oneMultiHypothesisClusterWithTwoFilesFilesIdToRemove() = listOf(FileId(f3.aggregateId), FileId(f4DistinctFromF3.aggregateId))
      fun oneMultiHypothesisClusterWithTwoFilesMap() = mapOf(Pair(ClusterId(f3.clusterId!!), listOf(f3, f4DistinctFromF3)))

      fun multiHypothesisClusterWithOnlyOneDistinctFileInit(eventBus: EventBus) {
        val events: List<Event> = listOf(
          FileCreated(aggregateIdExample(0), "not in a cluster"),
          FilePutInNamedClass(aggregateIdExample(0), arrayOf("name 0")),

          FileCreated(aggregateIdExample(1), "1", "cluster 1, no multi-hypothesis", true),
          FilePutInNamedClass(aggregateIdExample(1), arrayOf("name 1")),
          FileCreated(aggregateIdExample(2), "2", "cluster 1, no multi-hypothesis", false),
          FilePutInNamedClass(aggregateIdExample(2), arrayOf("name 1")),

          FileCreated(f3.aggregateId, f3.filename, f3.clusterId, f3.clusterCenter),
          FilePutInNamedClass(f3.aggregateId, f3.fileNamedClass),
          FileCreated(f4DistinctFromF3.aggregateId, f4DistinctFromF3.filename, f4DistinctFromF3.clusterId, f4DistinctFromF3.clusterCenter),
          FilePutInNamedClass(f4DistinctFromF3.aggregateId, f4DistinctFromF3.fileNamedClass),
          FileCreated(f5LikeF3.aggregateId, f5LikeF3.filename, f5LikeF3.clusterId, f5LikeF3.clusterCenter),
          FilePutInNamedClass(f5LikeF3.aggregateId, f5LikeF3.fileNamedClass),
        )
        eventBus.dispatch(events)
      }

      fun multiHypothesisClusterWithOnlyOneDistinctFileFilesIdToRemove() = listOf(FileId(f4DistinctFromF3.aggregateId))
    }
  }
}
