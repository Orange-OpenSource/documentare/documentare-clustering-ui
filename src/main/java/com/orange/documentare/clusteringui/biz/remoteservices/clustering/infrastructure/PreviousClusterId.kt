package com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure

/** mockito prevents us from using a data class... */
open class PreviousClusterId(val id: String) {

  companion object {
    fun none() = PreviousClusterId("")
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (other !is PreviousClusterId) return false

    if (id != other.id) return false

    return true
  }

  override fun hashCode(): Int {
    return id.hashCode()
  }

  override fun toString(): String {
    return "PreviousClusterId(id='$id')"
  }
}