package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler

class AllFilesInModifiedClustersQueryHandler(private val projections: Projections) : QueryHandler<AllFilesInModifiedClustersQuery, Map<String, List<DomainFile>>>() {

  override fun doExecute(query: AllFilesInModifiedClustersQuery): Map<String, List<DomainFile>> =
    projections.files.ids()
      .map { projections.files[it] }
      .filter { it.clusterCenter() && it.clusterModified() }
      .map { it.clusterId!! }
      .associateWith { clusterId ->
        (projections.clusters.clusters()[clusterId] ?: error("cluster $clusterId was expected"))
          .map { projections.files[it] }
      }
}