package com.orange.documentare.clusteringui.biz.file.query;

import com.tophe.ddd.queries.Query;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@EqualsAndHashCode
@RequiredArgsConstructor
public class AllSingletonsNotLockedQuery implements Query {
}
