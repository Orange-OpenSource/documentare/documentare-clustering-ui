package com.orange.documentare.clusteringui.biz.processes.hierarchy.transport

import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.StatisticsConsolidation.ComputedDistances

interface HierarchyStatisticsConsolidation {
  fun computedDistancesAvailable(): Boolean
  fun computedDistances(): ComputedDistances
}
