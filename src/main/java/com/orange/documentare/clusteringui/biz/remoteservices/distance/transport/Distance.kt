package com.orange.documentare.clusteringui.biz.remoteservices.distance.transport

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.AllFilesInModifiedClustersQuery
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQuery
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.ClusteringServiceImpl
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceServiceImpl
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.util.*

@Controller
class Distance {
  private val log = LoggerFactory.getLogger(Distance::class.java)

  @Autowired
  lateinit var distanceService: DistanceServiceImpl

  @Autowired
  lateinit var clusteringService: ClusteringServiceImpl

  @Autowired
  lateinit var queryBus: QueryBus

  @RequestMapping("/compute-distance/{fileId}")
  fun computeDistance(@PathVariable fileId: String?): String {
    val prefix = "GET [/compute-distance/"
    if (fileId == null) {
      log.error(prefix + "fileId] fileId is NULL")
      return "redirect:/"
    }
    val parameters = parameters()
    if (parameters == null) {
      log.warn("$prefix failed to find parameters, redirect user to form page")
      return "redirect:/parameters"
    }
    log.info("$prefix$fileId]")
    distanceService.computeDistancesOf(parameters, listOf(fileId))
    return "redirect:/"
  }

  private fun parameters(): Parameters? {
    val response = queryBus.dispatch<QueryResponse<Parameters?>>(ParametersQuery())
    return if (response.failure()) null else response.value()
  }

  /*  @RequestMapping(value = ["/files"], params = ["action=computeDistances"])
    fun computeDistances(req: HttpServletRequest): String {
      val prefix = "GET [/files/ action=computeDistances]"
      val fileIds = req.parameterMap["idToSort"]
      var filesIds = emptyList<String>()
      if (fileIds != null) {
        filesIds = listOf(*fileIds)
      }
      val parameters = parameters()
      if (parameters == null) {
        log.warn("$prefix failed to find parameters, redirect user to form page")
        return "redirect:/parameters"
      }
      val prefixWithFileIds = prefix + " " + Arrays.toString(fileIds)
      log.info(prefixWithFileIds)
      val fIds = filesIds
      (queryBus.dispatch<QueryResponse<Map<String, List<DomainFile>>>>(AllFilesInModifiedClustersQuery()))
        .onFailure { throwable: Throwable -> log.error("Failed to get AllFilesInModifiedClusters : $throwable") }
        .onSuccess { clustersModified: Map<String, List<DomainFile>> -> clusteringService.recalculateClustersThenComputeDistancesAsync(parameters()!!, clustersModified, fIds) }
      return "redirect:/"
    }*/

  @Secured("ROLE_USER")
  @ResponseBody
  @RequestMapping(value = ["/api/computeDistances"], method = [RequestMethod.POST])
  fun computeDistances(@RequestBody selectedFileIdsToSort: Array<String>?): String {
    val prefix = "GET [/api/computeDistances]"
    var filesIds = emptyList<String>()
    if (selectedFileIdsToSort != null) {
      filesIds = listOf(*selectedFileIdsToSort)
    }
    val parameters = parameters()
    if (parameters == null) {
      log.warn("$prefix failed to find parameters, redirect user to form page")
      return "redirect:/parameters"
    }
    val prefixWithselectedFileIdsToSort = prefix + " " + Arrays.toString(selectedFileIdsToSort)
    log.info(prefixWithselectedFileIdsToSort)
    val fIds = filesIds
    (queryBus.dispatch<QueryResponse<Map<String, List<DomainFile>>>>(AllFilesInModifiedClustersQuery()))
      .onFailure { throwable: Throwable -> log.error("Failed to get AllFilesInModifiedClusters : $throwable") }
      .onSuccess { clustersModified: Map<String, List<DomainFile>> -> clusteringService.recalculateClustersThenComputeDistancesAsync(parameters()!!, clustersModified, fIds) }
    return "redirect:/"
  }
}
