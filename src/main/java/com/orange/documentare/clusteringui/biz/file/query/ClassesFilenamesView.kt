package com.orange.documentare.clusteringui.biz.file.query

data class ClassesFilenamesView(val classesFilenames: Map<String, List<String>>)
