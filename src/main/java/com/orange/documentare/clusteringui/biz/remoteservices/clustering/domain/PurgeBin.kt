package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.orange.documentare.clusteringui.biz.file.command.BinPurgeCommand
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.commands.CommandResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory

internal object PurgeBin {

  private val log: Logger = LoggerFactory.getLogger(PurgeBin::class.java)

  fun purgeBin(commandBus: CommandBus, filesIdsInBinToPurge: List<String>) {
    if (filesIdsInBinToPurge.isNotEmpty()) {
      doBinPurge(commandBus, filesIdsInBinToPurge)
    }
  }

  private fun doBinPurge(commandBus: CommandBus, filesIdsInBinToPurge: List<String>) {
    commandBus.dispatch<CommandResponse<Unit>>(BinPurgeCommand(filesIdsInBinToPurge))
      .onSuccess { log.info("Bin purged") }
      .onFailure { log.error("Bin purge error occurred", it) }
  }
}