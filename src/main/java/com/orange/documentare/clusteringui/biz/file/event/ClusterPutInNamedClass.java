package com.orange.documentare.clusteringui.biz.file.event;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.ClusterPutInNamedClassDTO.exampleClassName;
import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;

import java.time.Instant;

import com.tophe.ddd.infrastructure.event.Event;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ClusterPutInNamedClass extends Event {

  public final String clusterId;
  public final String className;

  public ClusterPutInNamedClass(String aggregateId, String clusterId, String className) {
    super(aggregateId);
    this.clusterId = clusterId;
    this.className = className;
  }

  private ClusterPutInNamedClass(Instant date, String aggregateId, String clusterId, String className) {
    super(aggregateId, date);
    this.clusterId = clusterId;
    this.className = className;
  }

  public static ClusterPutInNamedClass buildFromDB(Instant date, String aggregateId, String clusterId, String className) {
    return new ClusterPutInNamedClass(date, aggregateId, clusterId, className);
  }

  //
  // FOR TESTS
  //
  public static ClusterPutInNamedClass example() {
    return new ClusterPutInNamedClass(TEST_DATE, AGGREGATE_ID_EXAMPLE.toString(), exampleClusterId(AGGREGATE_ID_EXAMPLE),
      exampleClassName(AGGREGATE_ID_EXAMPLE));
  }
}
