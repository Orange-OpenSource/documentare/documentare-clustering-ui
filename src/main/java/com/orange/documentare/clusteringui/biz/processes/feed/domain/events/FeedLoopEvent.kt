package com.orange.documentare.clusteringui.biz.processes.feed.domain.events

import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter


abstract class FeedLoopEvent(private val date: Instant = Instant.now()) {
  fun display(prefix: String? = null): String {
    val printPrefix = prefix ?: htmlTimestamp(date)
    return "${printPrefix}${displayInternalHtml()}"
  }

  abstract fun displayInternalHtml(): String
  abstract fun displayInternalRaw(): String

  companion object {
    fun htmlTimestamp(instant: Instant): String {
      val pattern = "HH:mm:ss.SSS"
      val str = DateTimeFormatter.ofPattern(pattern).format(ZonedDateTime.ofInstant(instant, ZoneId.of("Europe/Paris")))
      return "${str}&nbsp;&nbsp;&nbsp;"
    }
  }
}
