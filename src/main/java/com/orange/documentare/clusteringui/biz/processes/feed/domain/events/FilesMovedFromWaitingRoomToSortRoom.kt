package com.orange.documentare.clusteringui.biz.processes.feed.domain.events

data class FilesMovedFromWaitingRoomToSortRoom(val filesId: List<String>) : FeedLoopEvent() {
  override fun displayInternalHtml() = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ${filesId.size} files moved from waiting room to sort room"
  override fun displayInternalRaw() = "      ${filesId.size} files moved from waiting room to sort room"
}
