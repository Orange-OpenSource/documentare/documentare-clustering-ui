package com.orange.documentare.clusteringui.biz.file.transport

import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.file.command.AddFilesCommand
import com.orange.documentare.clusteringui.biz.file.command.AddFilesInWaitingRoomCommand
import com.orange.documentare.clusteringui.biz.file.command.PutFileInNamedClassCommand
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQuery
import com.tophe.ddd.commands.Command
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import java.security.Principal

@Controller
class Upload(var commandBus: CommandBus, var queryBus: QueryBus) {

  // For test purpose to avoid dealing with real command/query buses in tests
  var commandBusDispatch: (Command) -> Any = { commandBus.dispatch(it) }
  var queryParameters: () -> Parameters = { parameters() }

  @Secured("ROLE_USER")
  @RequestMapping(value = ["/upload"], method = [RequestMethod.POST])
  fun upload(request: MultipartHttpServletRequest, principal: Principal): ResponseEntity<Void> {
    return apiUpload(request, null, principal)
  }

  @Secured("ROLE_USER")
  @RequestMapping(value = ["/api/upload"], method = [RequestMethod.POST])
  fun apiUpload(request: MultipartHttpServletRequest, @RequestParam("file-named-class") fileNamedClass: Array<String>?, principal: Principal): ResponseEntity<Void> {
    val filesAddedSuccessfully = addFiles(retrieveFilenamesAndContent(request), fileNamedClass).isNotEmpty()

    return if (filesAddedSuccessfully) ResponseEntity.ok().build() else ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
  }

  fun addFiles(filenamesAndContent: List<Pair<String, ByteArray>>, fileNamedClass: Array<String>?): List<String> {
    fileNamedClass?.let {
      if (filenamesAndContent.size > 1) {
        throw IllegalStateException("can not upload multiple files in the same request if clustering class name is provided")
      }
    }

    val filesToAddCommands: Pair<Command, Int> = if (isFeederEnabled()) {
      val commandFilesInWaitingRoom: List<AddFilesInWaitingRoomCommand.File> = filenamesAndContent
        .map { filenameAndContent: Pair<String, ByteArray> -> TryOrCatch.of { AddFilesInWaitingRoomCommand.File(filenameAndContent.first, filenameAndContent.second) } }
        .map { obj: TryOrCatch<AddFilesInWaitingRoomCommand.File> -> obj.get() }
      Pair(AddFilesInWaitingRoomCommand(commandFilesInWaitingRoom), commandFilesInWaitingRoom.size)
    } else {
      val commandFiles: List<AddFilesCommand.File> = filenamesAndContent
        .map { filenameAndContent: Pair<String, ByteArray> -> TryOrCatch.of { AddFilesCommand.File(filenameAndContent.first, filenameAndContent.second) } }
        .map { obj: TryOrCatch<AddFilesCommand.File> -> obj.get() }
      Pair(AddFilesCommand(commandFiles), commandFiles.size)
    }

    val addedFilesAggregatesIds = doAddFiles(commandBusDispatch.invoke(filesToAddCommands.first), filesToAddCommands.second)

    fileNamedClass?.let {
      addFileToNamedClass(addedFilesAggregatesIds.first(), it)
    }

    return addedFilesAggregatesIds
  }

  private fun retrieveFilenamesAndContent(request: MultipartHttpServletRequest): List<Pair<String, ByteArray>> =
    request.fileNames.asSequence().toList()
      .mapNotNull { filename: String -> request.getFile(filename) }
      .filter { file: MultipartFile -> file.originalFilename != null }
      .map { file: MultipartFile -> Pair(file.originalFilename!!, file.bytes) }

  private fun doAddFiles(dispatch: Any, size: Int): List<String> {
    val value = dispatch as CommandResponse<List<String>>
    return if (value.success()) {
      log.debug("GET [/upload] received files count: $size")
      value.value()
    } else {
      value.onFailure { throwable: Throwable -> log.error("GET [/upload] error: $throwable") }
      emptyList()
    }
  }

  private fun addFileToNamedClass(fileAggregateId: String, fileNamedClass: Array<String>): Boolean {
    val response = commandBusDispatch.invoke(PutFileInNamedClassCommand(fileAggregateId, fileNamedClass)) as CommandResponse<*>
    response
      .onSuccess { log.debug("file '{}' added to named class '{}'", fileAggregateId, fileNamedClass) }
      .onFailure { throwable: Throwable -> log.error("GET [/upload] error while putting file to named class: $throwable") }

    return response.failure()
  }

  private fun isFeederEnabled() = queryParameters.invoke().isFeed

  private fun parameters(): Parameters {
    val response = queryBus.dispatch<QueryResponse<Parameters>>(ParametersQuery())
    return if (response.failure() || response.value() == null) {
      Parameters.defaults()
    } else {
      response.value()
    }
  }

  companion object {
    private val log: Logger = LoggerFactory.getLogger(Upload::class.java)
  }

  /*
@Secured("ROLE_USER")
@ResponseBody
@RequestMapping(value = ["/api/upload-file"], method = [RequestMethod.POST])
@Throws(IOException::class)
fun uploadFile(
  @RequestParam("file") file: MultipartFile?
): ResponseEntity<Any> {
  val bytes = file?.bytes
  val originalFilename = file?.originalFilename
  if (bytes == null || bytes.isEmpty()) {
    log.error("POST [/api/upload-file] error: no bytes or clusteringId for : $originalFilename")
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
  }
  val addFilesCommands = listOf(AddFilesInWaitingRoomCommand.File(originalFilename, bytes))
  val filesAddedSuccessfully = addFiles(AddFilesInWaitingRoomCommand(addFilesCommands), addFilesCommands.size)
  return if (filesAddedSuccessfully) ResponseEntity.ok().build() else ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
}
*/
  /*
  @Secured("ROLE_USER")
  @ResponseBody
  @RequestMapping(value = ["/api/upload-clustering-file"], method = [RequestMethod.POST])
  @Throws(IOException::class)
  fun importFileWithClustering(
    @RequestParam("file") file: MultipartFile?,
    @RequestParam("clusterId") clusterId: String?,
    @RequestParam("clusterCenter") clusterCenter: Boolean?
  ): ResponseEntity<Any> {
    val bytes = file?.bytes
    val originalFilename = file?.originalFilename
    if (bytes == null || bytes.isEmpty() || clusterId == null) {
      log.error("POST [/api/upload-clustering-file] error: no bytes or clusteringId for : $originalFilename")
      return ResponseEntity<Any>(HttpStatus.BAD_REQUEST)
    }
    val addFilesCommands = listOf(AddFilesCommand.File(originalFilename!!, bytes, clusterId, clusterCenter))
    val filesAddedSuccessfully = addFiles(AddFilesCommand(addFilesCommands), addFilesCommands.size)
    return if (filesAddedSuccessfully) ResponseEntity.ok().build() else ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
  }
*/
}
