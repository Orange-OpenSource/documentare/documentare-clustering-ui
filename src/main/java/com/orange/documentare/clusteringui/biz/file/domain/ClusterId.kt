package com.orange.documentare.clusteringui.biz.file.domain

data class ClusterId(val clusterId: String)
