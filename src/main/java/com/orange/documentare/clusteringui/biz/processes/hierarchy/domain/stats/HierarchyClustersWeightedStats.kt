package com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.ClusterDistancesStats
import com.orange.documentare.clusteringui.biz.file.query.ClusterId
import com.orange.documentare.clusteringui.biz.file.query.HierarchiesView

data class HierarchyClustersWeightedStats(
  private val hierarchiesView: HierarchiesView,
  private val retrieveLevelClusterStats: (levelIndex: Int, clusterId: String) -> ClusterDistancesStats
) {
  override fun toString(): String {
    // return a string representation of the object
    return StringBuilder().apply {
      append("HierarchiesView:\n")
      hierarchiesView.hierarchies.forEach { (level, clusters) ->
        append("\tLevel $level:\n")
        clusters.forEach { (clusterId, files) ->
          append(
            "\t\tCluster '${clusterId.substring(0, kotlin.math.min(8, clusterId.length))}' ${
              displayClusterStats(
                level,
                clusterId
              )
            }:\t${files.joinToString { displayFile(it) }}\n"
          )
        }
      }
    }.toString()
  }

  private fun displayFile(domainFile: DomainFile): String {
    val extra = if (domainFile.clusterCenter()) "*" else if (domainFile.inMultiset()) "**" else ""
    return domainFile.filename + extra
  }

  private fun displayClusterStats(level: Int, clusterId: String): String {
    val clusterStats = clusterStats(level, clusterId)
    return if (level == 0 && clusterStats.singleton) {
      "(singleton, not used for upper level stats)"
    } else {
      "(mean: ${clusterStats.mean}, stdDev: ${clusterStats.standardDeviation})" + if (clusterStats.singleton) " (singleton, not used for upper level stats)" else ""
    }
  }

  fun clusterStats(level: Int, clusterId: String): ClusterDistancesStats {
    if (level == 0) {
      return retrieveLevelClusterStats(0, clusterId)
    }
    return clusterStatsForHigherLevels(level, clusterId)
  }

  private fun clusterStatsForHigherLevels(level: Int, clusterId: String): ClusterDistancesStats {
    val clusterFilesAtCurrentLevel: List<DomainFile> = hierarchiesView.level(level)[clusterId]!!
    val clusterFilesIdsAtCurrentLevel = clusterFilesAtCurrentLevel.map { domainFile -> domainFile.aggregateId }
    val associatedNonSingletonClusterIdsAtLowerLevel: List<ClusterId> = hierarchiesView.level(level - 1)
      .filter { entry: Map.Entry<ClusterId, List<DomainFile>> ->
        val domainFiles = entry.value
        // not a singleton
        domainFiles.size > 1 && domainFiles.any { clusterFilesIdsAtCurrentLevel.contains(it.aggregateId) }
      }
      .map { it.key }

    // TODO CLIENT RESTART HERE test me
    // edge case: this "level 1" cluster is only made of "level 0" singletons
    // => 💡 we can use directly the cluster stats computed during hierarchy construction
    if (level == 1 && associatedNonSingletonClusterIdsAtLowerLevel.isEmpty()) {
      return retrieveLevelClusterStats(1, clusterId)
    }

    // recursive call
    val associatedClustersStatsAtLowerLevel: List<ClusterDistancesStats> = associatedNonSingletonClusterIdsAtLowerLevel.map { clusterStats(level - 1, it) }

    val associatedClustersNbElementsAndMeanAtLowerLevel = associatedClustersStatsAtLowerLevel.map { Pair(it.nbElements, it.mean) }
    val weightedMean: Double = Stats.weightedMean(associatedClustersNbElementsAndMeanAtLowerLevel)
    val weightedStandardDeviation: Double = Stats.weightedStandardDeviation(associatedClustersNbElementsAndMeanAtLowerLevel)

    return ClusterDistancesStats.buildWith(
      clusterFilesAtCurrentLevel.size,
      weightedMean,
      weightedStandardDeviation,
      clusterFilesAtCurrentLevel.filter { it.inMultiset() }.map { it.filename })
  }
}
