package com.orange.documentare.clusteringui.biz.file.command;

import java.util.List;

import com.tophe.ddd.commands.Command;

public class AddToClusterCommand implements Command {

  public AddToClusterCommand(List<AddToClusterAssociation> associations) {
    this.associations = associations;
  }

  public static class AddToClusterAssociation {

    public final String fileId;
    public final String clusterId;
    public final Boolean clusterCenter;
    public final Integer distanceToCenter;
    public final Boolean inMultiset;
    public final Boolean lock;
    public final String className;

    public AddToClusterAssociation(String fileId, String clusterId, Boolean clusterCenter, Integer distanceToCenter, Boolean inMultiset, Boolean lock, String className) {
      this.fileId = fileId;
      this.clusterId = clusterId;
      this.clusterCenter = clusterCenter;
      this.distanceToCenter = distanceToCenter;
      this.inMultiset = inMultiset;
      this.lock = lock;
      this.className = className;
    }
  }

  public final List<AddToClusterAssociation> associations;
}
