package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile

data class ClusterView(val clusterId: String, val files: List<DomainFile>)
