package com.orange.documentare.clusteringui.biz.file.event;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;

import java.time.Instant;

import com.tophe.ddd.infrastructure.event.Event;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FileInfoReset extends Event {

  public final String clusterId;

  public FileInfoReset(String aggregateId, String clusterId) {
    super(aggregateId);
    this.clusterId = clusterId;
  }

  private FileInfoReset(Instant date, String aggregateId, String clusterId) {
    super(aggregateId, date);
    this.clusterId = clusterId;
  }

  public static FileInfoReset buildFromDB(Instant date, String aggregateId, String clusterId) {
    return new FileInfoReset(date, aggregateId, clusterId);
  }


  //
  // FOR TESTS
  //
  public static FileInfoReset example() {
    return new FileInfoReset(TEST_DATE, AGGREGATE_ID_EXAMPLE.toString(), exampleClusterId(AGGREGATE_ID_EXAMPLE));
  }
}
