package com.orange.documentare.clusteringui.biz.parameters.transport

import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQuery
import com.orange.documentare.clusteringui.infrastructure.security.Authent
import com.orange.documentare.core.comp.ncd.cache.NcdCache
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import java.security.Principal

@Controller
class Display(private val queryBus: QueryBus) {
  private val log = LoggerFactory.getLogger(Display::class.java)

  @RequestMapping("/parameters")
  fun getParameters(principal: Principal?, model: Model): String {

    // hook to display cache stats at will
    log.info(NcdCache.stats())

    model.addAttribute("title", "Parameters")
    Authent.addAuthenticatedModel(model, principal)

    val response = queryBus.dispatch<QueryResponse<Parameters?>>(ParametersQuery())
    val prefix = "GET [/parameters]"
    response
      .onFailure { throwable: Throwable -> log.error("$prefix error: $throwable") }

    val parameters = if (response.failure() || response.value() == null) {
      Parameters.defaults()
    } else {
      response.value()
    }

    model.addAttribute("parameters", parameters)
    return "parameters"
  }
}
