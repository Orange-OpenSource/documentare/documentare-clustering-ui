package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.orange.documentare.clusteringui.biz.file.command.ResetFileInfoCommand
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.AllFilesInModifiedClustersQuery
import com.orange.documentare.clusteringui.biz.file.query.AllFilesToSortQuery
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.processes.feed.domain.BreakOutOfThresholdNotLockedClusters
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.DistancesComputedAndClustersRecalculated
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.IOException

internal data class ComputeDistancesThenRecalculateClusters(
  val infra: ClusteringInfra
) {

  private val log: Logger = LoggerFactory.getLogger(javaClass)

  fun doSync(parameters: Parameters): DistancesComputedAndClustersRecalculated {
    var computedDistancesForFilesId: List<String>? = null
    var recalculatedClustersCount: Int? = null

    val query = infra.queryBus.dispatch<QueryResponse<List<DomainFile>>>(AllFilesToSortQuery())
    if (query.value().isNotEmpty()) {
      val files = query.value().filter { !it.deleted() }
      val fileIds = files.map { it.aggregateId }
      if (fileIds.isNotEmpty()) {
        computedDistancesForFilesId = computeDistancesWithUpdatedClustersSync(parameters, fileIds)
      }
    }

    (infra.queryBus.dispatch<QueryResponse<Map<String, List<DomainFile>>>>(AllFilesInModifiedClustersQuery()))
      .onFailure { throwable -> log.error("Failed to get AllFilesInModifiedClusters : $throwable") }
      .onSuccess { clustersModified ->
        recalculatedClustersCount = ComputeClusteringOnModifiedClusters().doSync(infra, parameters, clustersModified)
      }

    val brokeClustersCount = BreakOutOfThresholdNotLockedClusters(infra.commandBus, infra.queryBus)
      .breakIfNumberOfMultisetIsGreaterThanMultisetThreshold(parameters.maxMultisetThreshold)

    return DistancesComputedAndClustersRecalculated(computedDistancesForFilesId?.size, recalculatedClustersCount, brokeClustersCount)
  }

  private fun computeDistancesWithUpdatedClustersSync(parameters: Parameters, fileIdsToSort: List<String>): List<String>? {
    log.info("\t[Recalculate] compute distances with updated clusters of '${fileIdsToSort.size}' files")
    var computedDistanceForFilesId: List<String>? = null
    (infra.commandBus.dispatch<CommandResponse<Unit>>(ResetFileInfoCommand(fileIdsToSort)))
      .onFailure { throwable -> log.error("ResetFileInfoCommand: $throwable") }
      .onSuccess {
        try {
          log.debug("Reset done on '${fileIdsToSort.size}' files info")
          val t0 = System.currentTimeMillis()
          infra.distanceService.computeDistancesOfInSameThread(parameters, fileIdsToSort)
          computedDistanceForFilesId = fileIdsToSort
          log.info("\t--> {} ms [compute distances with updated clusters]", System.currentTimeMillis() - t0)
        } catch (e: IOException) {
          e.printStackTrace()
        }
      }
    return computedDistanceForFilesId
  }
}
