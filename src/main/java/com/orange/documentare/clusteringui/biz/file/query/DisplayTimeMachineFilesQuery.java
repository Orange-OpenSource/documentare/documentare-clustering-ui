package com.orange.documentare.clusteringui.biz.file.query;

import com.tophe.ddd.queries.Query;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DisplayTimeMachineFilesQuery implements Query {

  public final String dateTime;
}
