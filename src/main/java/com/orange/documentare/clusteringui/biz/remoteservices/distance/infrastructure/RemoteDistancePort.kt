package com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure

import com.orange.documentare.clusteringui.TryOrCatch

interface RemoteDistancePort {
  fun request(urls: List<String>, distancesRequestUIS: List<DistancesRequestUI>): TryOrCatch<Map<DistancesRequestUI, DistancesRequestResult>>
}
