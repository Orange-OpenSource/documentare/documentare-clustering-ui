package com.orange.documentare.clusteringui.biz.file.domain

import com.orange.documentare.clusteringui.biz.file.event.FileRemovedFromCluster

class RemovedFilesFromCluster(destroyedCluster: String?, updatedFilesAndEvents: List<FileRemovedFromCluster>) {
  @JvmField
  val destroyedCluster: String? = destroyedCluster

  @JvmField
  val events: List<FileRemovedFromCluster> = updatedFilesAndEvents
}