package com.orange.documentare.clusteringui.biz.file.event;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;

import java.time.Instant;

import com.tophe.ddd.infrastructure.event.Event;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FileMovedFromWaitingRoomToSortRoom extends Event {

  public FileMovedFromWaitingRoomToSortRoom(String aggregateId) {
    super(aggregateId);
  }

  private FileMovedFromWaitingRoomToSortRoom(Instant date, String aggregateId) {
    super(aggregateId, date);
  }

  public static FileMovedFromWaitingRoomToSortRoom buildFromDB(Instant date, String aggregateId) {
    return new FileMovedFromWaitingRoomToSortRoom(date, aggregateId);
  }


  //
  // FOR TESTS
  //
  public static FileMovedFromWaitingRoomToSortRoom example() {
    return new FileMovedFromWaitingRoomToSortRoom(TEST_DATE, AGGREGATE_ID_EXAMPLE.toString());
  }
}
