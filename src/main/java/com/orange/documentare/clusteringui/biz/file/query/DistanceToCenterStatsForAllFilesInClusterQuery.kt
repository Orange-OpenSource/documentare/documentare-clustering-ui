package com.orange.documentare.clusteringui.biz.file.query

import com.tophe.ddd.queries.Query

data class DistanceToCenterStatsForAllFilesInClusterQuery(val clusterId: String, val hierarchyLevelIndex: Int? = null) : Query
