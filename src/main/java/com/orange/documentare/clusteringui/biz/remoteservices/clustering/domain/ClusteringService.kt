package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.DistancesComputedAndClustersRecalculated
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.GlobalOrPartialClusteringComputed

interface ClusteringService {
  fun globalOrPartialClusteringSync(parameters: Parameters, allFilesToSort: List<DomainFile>): GlobalOrPartialClusteringComputed?
  fun computeDistancesThenRecalculateClustersSync(parameters: Parameters): DistancesComputedAndClustersRecalculated
  fun isRunning(): Boolean
}
