package com.orange.documentare.clusteringui.biz.file.transport

import com.orange.documentare.clusteringui.biz.file.command.PutClustersInNamedClassCommand
import com.orange.documentare.clusteringui.biz.file.domain.Classe
import com.orange.documentare.clusteringui.biz.file.query.DisplayNamingFilesQuery
import com.orange.documentare.clusteringui.biz.file.query.DisplayNamingFilesView
import com.orange.documentare.clusteringui.biz.file.query.SelectedFileView
import com.orange.documentare.clusteringui.infrastructure.security.Authent
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody
import java.security.Principal
import java.util.*

@Controller
class Naming {
  private val log = LoggerFactory.getLogger(Naming::class.java)

  @Autowired
  lateinit var commandBus: CommandBus

  @Autowired
  lateinit var queryBus: QueryBus

  @RequestMapping("/naming")
  fun naming(principal: Principal?, model: Model): String {
    log.info("GET [/naming]")
    model.addAttribute("title", "Naming")
    model.addAttribute("classe", Classe(null, null))
    model.addAttribute("classAlbum", "album text-muted")

    Authent.addAuthenticatedModel(model, principal)
    val response = queryBus.dispatch<QueryResponse<DisplayNamingFilesView>>(DisplayNamingFilesQuery())
    response
      .onSuccess { filesView: DisplayNamingFilesView -> fillModel(filesView, model) }
      .onFailure { throwable: Throwable -> log.error("GET [/] error: " + throwable.localizedMessage) }
    return "naming"
  }

  private fun fillModel(filesView: DisplayNamingFilesView, model: Model) {
    model.addAttribute("noClusters", filesView.noClusters)
    model.addAttribute("filesByClusterId", filesView.sortedMapClusterByListSize)
    val selectedFileViewInCluster = SelectedFileView(null, null, null)
    model.addAttribute("selectedFileViewInCluster", selectedFileViewInCluster)
    val selectedFileView = SelectedFileView(null, null, null)
    model.addAttribute("selectedFileView", selectedFileView)
    model.addAttribute("classNames", filesView.classNameArrays)
  }

  @ResponseBody
  @RequestMapping(value = ["/api/name_classe"], method = [RequestMethod.POST])
  fun nameClasse(@RequestBody classe: Classe): String {
    log.info("nameClasse")
    if (classe.selectedFileIdsToName == null || classe.name == null) {
      return "redirect:/naming"
    }
    val prefix = "NAME [/api/name_classe/] " + Arrays.toString(classe.selectedFileIdsToName)
    commandBus.dispatch<CommandResponse<Void>>(PutClustersInNamedClassCommand(classe.name, listOf(*classe.selectedFileIdsToName)))
      .onSuccess { log.info(prefix) }
      .onFailure { throwable: Any -> log.error("$prefix error: $throwable") }
    return "redirect:/naming"
  }
}
