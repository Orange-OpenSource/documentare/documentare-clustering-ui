package com.orange.documentare.clusteringui.biz.file.event;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;
import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.MultisetMovedToNextHierarchyLevelDTO.EXAMPLE_NEXT_HIERARCHY_LEVEL;

import java.time.Instant;

import com.tophe.ddd.infrastructure.event.Event;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class MultisetElementMovedToNextHierarchyLevel extends Event {

  public final int nextHierarchyLevel;

  public MultisetElementMovedToNextHierarchyLevel(String aggregateId, int nextHierarchyLevel) {
    super(aggregateId);
    this.nextHierarchyLevel = nextHierarchyLevel;
  }

  private MultisetElementMovedToNextHierarchyLevel(Instant date, String aggregateId, int nextHierarchyLevel) {
    super(aggregateId, date);
    this.nextHierarchyLevel = nextHierarchyLevel;
  }

  public static MultisetElementMovedToNextHierarchyLevel buildFromDB(Instant date, String aggregateId, int nextHierarchyLevel) {
    return new MultisetElementMovedToNextHierarchyLevel(date, aggregateId, nextHierarchyLevel);
  }


  //
  // FOR TESTS
  //
  public static MultisetElementMovedToNextHierarchyLevel example() {
    return new MultisetElementMovedToNextHierarchyLevel(TEST_DATE, AGGREGATE_ID_EXAMPLE.toString(), EXAMPLE_NEXT_HIERARCHY_LEVEL);
  }
}
