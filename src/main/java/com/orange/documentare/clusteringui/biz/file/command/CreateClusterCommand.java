package com.orange.documentare.clusteringui.biz.file.command;

import java.util.List;

import com.tophe.ddd.commands.Command;

public class CreateClusterCommand implements Command {

  public final List<String> filesIds;
  public final String clusterId;

  public CreateClusterCommand(List<String> filesIds, String clusterId) {
    this.filesIds = filesIds;
    this.clusterId = clusterId;
  }
}
