package com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure

import com.orange.documentare.clusteringui.Couple
import com.orange.documentare.clusteringui.TryOrCatch
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.core.model.ref.clustering.Clustering

interface ClusteringRemoteApi {
  fun calculateClustering(parameters: Parameters, files: List<DomainFile>): TryOrCatch<Clustering>
  fun recalculateClusters(parameters: Parameters, clustersFiles: List<Couple<PreviousClusterId, List<DomainFile>>>): Map<PreviousClusterId, TryOrCatch<Clustering>>
  fun createCluster(parameters: Parameters, files: List<DomainFile>): TryOrCatch<Clustering>
}
