package com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import java.io.IOException;
import java.util.List;

import org.jetbrains.annotations.NotNull;

import com.orange.documentare.clusteringui.EmbeddedComputationServer;
import com.orange.documentare.computationserver.ComputationServerResponse;
import com.orange.documentare.core.computationserver.biz.distances.DistancesRequestResultDTO;
import com.orange.documentare.core.computationserver.biz.task.TaskController;
import com.orange.documentare.tasksdispatcher.ApiWrapper;
import com.orange.documentare.tasksdispatcher.RemoteTaskDTO;

import kotlin.Pair;
import retrofit2.Response;

public class DistanceRequestWrapper extends ApiWrapper<DistanceRequestWrapperNetworkApi, DistancesRequestResult> {

  public final DistancesRequestUI distancesRequestUI;
  private final EmbeddedComputationServer embeddedComputationServer;


  public DistanceRequestWrapper(DistancesRequestUI distancesRequestUI, EmbeddedComputationServer embeddedComputationServer) {
    this.distancesRequestUI = distancesRequestUI;
    this.embeddedComputationServer = embeddedComputationServer;
  }

  @Override
  public ComputationServerResponse<RemoteTaskDTO> callComputationApi(String url) {
    if (embeddedComputationServer.getEnabled()) {
      return embeddedCallComputationServer();
    }
    else
      return remoteCallComputationApi(url);

  }

  @Override
  public ComputationServerResponse<DistancesRequestResult> callTaskApi(String taskId, String url) {
    if (embeddedComputationServer.getEnabled())
      return embeddedCallTasks(taskId);
    else
      return remoteCallTaskApi(taskId, url);
  }

  @Override
  public String requestId() {
    List<String> ids = distancesRequestUI.element[0].ids;
    String requestId = ids.get(0);
    for (int i = 1; i < ids.size(); i++) {
      requestId += "_" + ids.get(i);
    }
    return requestId;
  }

  private DistanceRequestWrapperNetworkApi api(String url) {
    return buildNetworkApi(DistanceRequestWrapperNetworkApi.class, url);
  }

  @NotNull
  private ComputationServerResponse<RemoteTaskDTO> remoteCallComputationApi(String url) {
    Response<RemoteTaskDTO> remoteTaskDTOResponse;
    try {
      remoteTaskDTOResponse = api(url).computationApi(distancesRequestUI).execute();
      return ComputationServerResponse.with(remoteTaskDTOResponse.body(), remoteTaskDTOResponse.code());
    }
    catch (IOException e) {
      return ComputationServerResponse.hasThrown(e);
    }
  }

  @NotNull
  private ComputationServerResponse<DistancesRequestResult> remoteCallTaskApi(String taskId, String url) {
    Response<DistancesRequestResult> distancesRequestResultResponse;
    try {
      distancesRequestResultResponse = api(url).taskApi(taskId).execute();
      return ComputationServerResponse.with(distancesRequestResultResponse.body(), distancesRequestResultResponse.code());
    }
    catch (IOException e) {
      return ComputationServerResponse.hasThrown(e);
    }
  }

  private ComputationServerResponse<RemoteTaskDTO> embeddedCallComputationServer() {
    ComputationServerResponse<com.orange.documentare.core.computationserver.biz.RemoteTaskDTO> remoteTaskDTOComputationServerResponse =
      embeddedComputationServer.getDistancesApi().computeDistances(distancesRequestUI.toComputationServerDistanceRequest());
    if (remoteTaskDTOComputationServerResponse.isSuccessful()) {
      return ComputationServerResponse.with(new RemoteTaskDTO(remoteTaskDTOComputationServerResponse.dto().id), remoteTaskDTOComputationServerResponse.code());
    }
    else {
      return ComputationServerResponse.error(remoteTaskDTOComputationServerResponse.code(), remoteTaskDTOComputationServerResponse.errorMessage());
    }
  }

  private ComputationServerResponse<DistancesRequestResult> embeddedCallTasks(String taskId) {
    Pair<TaskController.TaskState, Object> taskResult = embeddedComputationServer.getTaskApiClient().getTaskResult(taskId);
    TaskController.TaskState taskState = taskResult.getFirst();
    if (taskState == TaskController.TaskState.DONE) {
      DistancesRequestResultDTO distancesRequestResultDTO = (DistancesRequestResultDTO) taskResult.component2();
      return ComputationServerResponse.with(DistancesRequestResult.with(distancesRequestResultDTO.distances), 200);
    }
    else {
      // TODO: error cases not tested
      return ComputationServerResponse.error(418, "Internal error, try to retrieve task which is not finished, current state is $taskState");
    }
  }
}
