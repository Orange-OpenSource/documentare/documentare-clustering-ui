package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler
import java.util.Comparator.comparing

class AssociateFileQueryHandler(private val projections: Projections) : QueryHandler<AssociateFileQuery, AssociateFileView>() {

  override fun doExecute(query: AssociateFileQuery): AssociateFileView {
    val file = projections.files[query.fileId]
    return buildAssociateFileView(file)
  }

  private fun buildAssociateFileView(file: DomainFile): AssociateFileView {
    val fileUIView = FileUIView.from(file)
    var distancesToCluster: List<DistanceToCluster> = file.distancesToClustersCenters
      ?: return AssociateFileView(fileUIView, null, null)

    distancesToCluster = distancesToCluster.sortedWith(comparing { dist: DistanceToCluster ->
      dist.distance
    })

    val clustersCenterViews: MutableMap<String, List<FileUIView>> = mutableMapOf()
    var i = 0
    for (distanceToCluster in distancesToCluster) {
      val clusterFilesIds = projections.clusters.clusters()[distanceToCluster.clusterId]
        ?: error("cluster ${distanceToCluster.clusterId} was expected")
      if (clusterFilesIds.isNotEmpty()) {
        var fileUIViews: List<FileUIView>
        if (i > 4) {
          break
        }
        fileUIViews = clusterFilesIds.map { id: String -> projections.files[id] }
          .filter { it.clusterCenter() }
          .map { FileUIView.from(it) }
        clustersCenterViews[distanceToCluster.clusterId] = fileUIViews
        i++
      }
    }
    val fileDistanceToCluster: MutableMap<String, List<DistanceToCluster>> = mutableMapOf()
    val distanceToClusters = distancesToCluster
      .filter { distance: DistanceToCluster ->
        (projections.clusters.clusters()[distance.clusterId]
          ?: error("cluster ${distance.clusterId} was expected")).isNotEmpty()
      }
    if (distanceToClusters != distancesToCluster) {
      fileDistanceToCluster[file.aggregateId] = distanceToClusters
    }
    return AssociateFileView(fileUIView, clustersCenterViews, fileDistanceToCluster)
  }

}
