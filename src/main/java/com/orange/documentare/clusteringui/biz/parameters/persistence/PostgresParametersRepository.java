package com.orange.documentare.clusteringui.biz.parameters.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters;
import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm;

@Service
public class PostgresParametersRepository implements ParametersRepository {

  private final JdbcTemplate jdbcTemplate;

  public PostgresParametersRepository(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public <S extends Parameters> void insert(Iterable<S> entities) {
    entities.forEach(parameters -> {
      if (jdbcTemplate.update(
        """
          INSERT INTO PARAMETERS(
          RAW_CONVERTER,
          P_COUNT,
          ACUT_SD_FACTOR,
          QCUT_SD_FACTOR,
          SCUT_SD_FACTOR,
          CCUT_PERCENTILE,
          KNN_THRESHOLD,
          SLOOP,
          CONSOLIDATE_CLUSTER,
          ENROLL,
          ENROLL_ACUT_SD_FACTOR,
          ENROLL_QCUT_SD_FACTOR,
          ENROLL_SCUT_SD_FACTOR,
          ENROLL_CCUT_PERCENTILE,
          ENROLL_KNN_THRESHOLD,
          THRESHOLD_FACTOR,
          FEED,
          FEED_THRESHOLD,
          MIN_MULTISET_THRESHOLD,
          MAX_MULTISET_THRESHOLD,
          SUFFIX_ARRAY_ALGORITHM)
          VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
          """,
        ps -> persistParameters(ps, (Parameters) parameters)) <= 0)
      {
        throw new IllegalStateException(String.format("Failed to add parameters %s", parameters.toString()));
      }
    });
  }

  private void persistParameters(PreparedStatement ps, Parameters p) throws SQLException {
    ps.setObject(1, p.rawConverter);
    ps.setObject(2, p.pCount);
    ps.setObject(3, p.acutSdFactor);
    ps.setObject(4, p.qcutSdFactor);
    ps.setObject(5, p.scutSdFactor);
    ps.setObject(6, p.ccutPercentile);
    ps.setObject(7, p.knnThreshold);
    ps.setObject(8, p.sloop);
    ps.setObject(9, p.consolidateCluster);
    ps.setObject(10, p.enroll);
    ps.setObject(11, p.enrollAcutSdFactor);
    ps.setObject(12, p.enrollQcutSdFactor);
    ps.setObject(13, p.enrollScutSdFactor);
    ps.setObject(14, p.enrollCcutPercentile);
    ps.setObject(15, p.enrollKnnThreshold);
    ps.setObject(16, p.thresholdFactor);
    ps.setObject(17, p.feed);
    ps.setObject(18, p.feedThreshold);
    ps.setObject(19, p.minMultisetThreshold);
    ps.setObject(20, p.maxMultisetThreshold);
    ps.setObject(21, p.suffixArrayAlgorithm.toString());
  }

  @Override
  public Iterable<Parameters> findAll() {
    return jdbcTemplate.query(
      "SELECT * FROM PARAMETERS",
      (rs, rowNum) -> toParameters(rs));
  }

  private Parameters toParameters(ResultSet rs) throws SQLException {
    return Parameters.builder()
      .id(rs.getLong("ID"))
      .rawConverter(rs.getBoolean("RAW_CONVERTER"))
      .pCount(rs.getInt("P_COUNT"))
      .acutSdFactor(rs.getFloat("ACUT_SD_FACTOR"))
      .qcutSdFactor(rs.getFloat("QCUT_SD_FACTOR"))
      .scutSdFactor(rs.getFloat("SCUT_SD_FACTOR"))
      .ccutPercentile(rs.getInt("CCUT_PERCENTILE"))
      .knnThreshold(rs.getInt("KNN_THRESHOLD"))
      .sloop(rs.getBoolean("SLOOP"))
      .consolidateCluster(rs.getBoolean("CONSOLIDATE_CLUSTER"))
      .enroll(rs.getBoolean("ENROLL"))
      .enrollAcutSdFactor(rs.getFloat("ENROLL_ACUT_SD_FACTOR"))
      .enrollQcutSdFactor(rs.getFloat("ENROLL_QCUT_SD_FACTOR"))
      .enrollScutSdFactor(rs.getFloat("ENROLL_SCUT_SD_FACTOR"))
      .enrollCcutPercentile(rs.getInt("ENROLL_CCUT_PERCENTILE"))
      .enrollKnnThreshold(rs.getInt("ENROLL_KNN_THRESHOLD"))
      .thresholdFactor(rs.getFloat("THRESHOLD_FACTOR"))
      .feed(rs.getBoolean("FEED"))
      .feedThreshold(rs.getInt("FEED_THRESHOLD"))
      .minMultisetThreshold(rs.getInt("MIN_MULTISET_THRESHOLD"))
      .maxMultisetThreshold(rs.getInt("MAX_MULTISET_THRESHOLD"))
      .suffixArrayAlgorithm(SuffixArrayAlgorithm.valueOf(rs.getString("SUFFIX_ARRAY_ALGORITHM")))
      .build();
  }

  @Override
  public long count() {
    return jdbcTemplate.queryForObject(
      "SELECT COUNT(*) FROM PARAMETERS;",
      Integer.class);
  }

  @Override
  public void deleteAll() {
    jdbcTemplate.update("DELETE FROM PARAMETERS;");
  }
}
