package com.orange.documentare.clusteringui.biz.file.event;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.ClusterPutInNamedClassDTO.exampleClassName;
import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;

import java.time.Instant;

import com.tophe.ddd.infrastructure.event.Event;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ClusterUnlocked extends Event {

  public final String clusterId;
  public final boolean lock;
  public final String className;

  // NB: if Cluster was a real aggregate it will not be necessary to have a fileId...
  public ClusterUnlocked(String aggregateId, String clusterId, boolean lock, String className) {
    super(aggregateId);
    this.clusterId = clusterId;
    this.lock = lock;
    this.className = className;
  }

  private ClusterUnlocked(Instant date, String aggregateId, String clusterId, boolean lock, String className) {
    super(aggregateId, date);
    this.clusterId = clusterId;
    this.lock = lock;
    this.className = className;
  }

  public static ClusterUnlocked buildFromDB(Instant date, String aggregateId, String clusterId, boolean lock, String className) {
    return new ClusterUnlocked(date, aggregateId, clusterId, lock, className);
  }


  //
  // FOR TESTS
  //
  public static ClusterUnlocked example() {
    return new ClusterUnlocked(TEST_DATE, AGGREGATE_ID_EXAMPLE.toString(), exampleClusterId(AGGREGATE_ID_EXAMPLE), true,
      exampleClassName(AGGREGATE_ID_EXAMPLE));
  }
}
