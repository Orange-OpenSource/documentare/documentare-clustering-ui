package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import java.util.*

class AddFilesCommandHandler(eventBus: EventBus) : CommandHandler<AddFilesCommand, List<String>>(eventBus) {
  /* for test purpose */
  private var uuidSupplier = { UUID.randomUUID().toString() }

  override fun doExecute(command: AddFilesCommand): AggregateAndEvents<List<String>, Collection<Event>> {
    val events = command.files.map { persistFile(it) }
    return AggregateAndEvents(events.map { it.aggregateId }, events)
  }

  private fun persistFile(f: AddFilesCommand.File) =
    DomainFile.create(uuidSupplier(), f.filename, f.clusterId, f.clusterCenter, f.bytes)

  fun testPurposeSetUuidSuplier(testUUIDSupplier: () -> String) {
    uuidSupplier = testUUIDSupplier
  }
}
