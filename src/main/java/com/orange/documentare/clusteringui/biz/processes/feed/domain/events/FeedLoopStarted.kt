package com.orange.documentare.clusteringui.biz.processes.feed.domain.events

data class FeedLoopStarted(val feedLoopCount: Int) : FeedLoopEvent() {
  override fun displayInternalHtml() = "♻️ Feed Loop number <strong>$feedLoopCount</strong> has just started<br>"
  override fun displayInternalRaw() = "♻️ Feed Loop number $feedLoopCount has just started"
}
