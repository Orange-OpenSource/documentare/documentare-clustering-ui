package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

internal class ClusteringInProgress {
  private var inProgress = false

  fun inProgress(): Boolean = inProgress

  fun raiseExceptionIfAlreadyRunning() {
    if (inProgress) {
      throw IllegalStateException("clustering is running, please check running state first")
    }
  }

  fun setInProgress(inProgress: Boolean) {
    this.inProgress = inProgress
  }
}