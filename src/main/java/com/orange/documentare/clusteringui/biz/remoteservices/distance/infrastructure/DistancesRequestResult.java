package com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
public class DistancesRequestResult {

  public final int[] distances;
  public final boolean error;
  public final String errorMessage;
  public final boolean serviceUnavailable;

  public static DistancesRequestResult with(int[] distances) {
    return new DistancesRequestResult(distances, false, null, false);
  }
}
