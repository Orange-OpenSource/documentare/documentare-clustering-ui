package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler

open class AllSingletonsNotLockedQueryHandler(private val projections: Projections) : QueryHandler<AllSingletonsNotLockedQuery, List<String>>() {

  override fun doExecute(query: AllSingletonsNotLockedQuery): List<String> {
    val singletons = mutableListOf<String>()
    val clusters = projections.clusters.clusters()
    for (key in clusters.keys) {
      val clusterFilesId = clusters[key] ?: error("cluster $key was expected")
      if (clusterFilesId.size == 1 && !projections.files[clusterFilesId[0]].locked()) {
        singletons.addAll(clusterFilesId)
      }
    }
    return singletons
  }
}