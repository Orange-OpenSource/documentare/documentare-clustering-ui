package com.orange.documentare.clusteringui.biz.parameters.command;

import org.jetbrains.annotations.NotNull;

import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters;
import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm;
import com.tophe.ddd.commands.Command;

public record UpdateParametersCommand(
  Boolean rawConverter,
  Integer pCount,
  SuffixArrayAlgorithm suffixArrayAlgorithm,
  Float acutSdFactor,
  Float qcutSdFactor,
  Float scutSdFactor,
  Integer ccutPercentile,
  Integer knnThreshold,
  Boolean sloop,
  Boolean enroll,
  Float enrollAcutSdFactor,
  Float enrollQcutSdFactor,
  Float enrollScutSdFactor,
  Integer enrollCcutPercentile,
  Integer enrollKnnThreshold,
  Float thresholdFactor,
  Boolean feed,
  Integer feedThreshold,
  Integer minMultisetThreshold,
  Integer maxMultisetThreshold
) implements Command
{

  public static UpdateParametersCommandBuilder builder() {
    return new UpdateParametersCommandBuilder();
  }

  public static UpdateParametersCommandBuilder initBuilderWith(Parameters p) {
    return (new UpdateParametersCommandBuilder())
      .rawConverter(p.rawConverter)
      .pCount(p.pCount)
      .suffixArrayAlgorithm(p.suffixArrayAlgorithm)
      .acutSdFactor(p.acutSdFactor)
      .qcutSdFactor(p.qcutSdFactor)
      .scutSdFactor(p.scutSdFactor)
      .ccutPercentile(p.ccutPercentile)
      .knnThreshold(p.knnThreshold)
      .sloop(p.sloop)
      .enroll(p.enroll)
      .enrollAcutSdFactor(p.enrollAcutSdFactor)
      .enrollQcutSdFactor(p.enrollQcutSdFactor)
      .enrollScutSdFactor(p.enrollScutSdFactor)
      .enrollCcutPercentile(p.enrollCcutPercentile)
      .enrollKnnThreshold(p.enrollKnnThreshold)
      .thresholdFactor(p.thresholdFactor)
      .feed(p.feed)
      .feedThreshold(p.feedThreshold)
      .minMultisetThreshold(p.minMultisetThreshold)
      .maxMultisetThreshold(p.maxMultisetThreshold);
  }

  public void validate() {
    if (rawConverter != null && rawConverter && pCount == null) {
      throw new ParametersValidationException("raw converter is enabled but pCount is not valid: null");
    }
    if (feed != null && feed && feedThreshold == null) {
      throw new ParametersValidationException("feed is enabled but feedThreshold is not valid: null");
    }
    if (suffixArrayAlgorithm == null) {
      throw new ParametersValidationException("suffixArrayAlgorithm is not valid: null");
    }
    if (thresholdFactor == null) {
      throw new ParametersValidationException("thresholdFactor is not valid: null");
    }
    if (minMultisetThreshold == null) {
      throw new ParametersValidationException("minMultisetThreshold is not valid: null");
    }
    if (maxMultisetThreshold == null) {
      throw new ParametersValidationException("maxMultisetThreshold is not valid: null");
    }
    if (minMultisetThreshold > maxMultisetThreshold) {
      throw new ParametersValidationException(
        String.format("minMultisetThreshold(%s) should not be lower than maxMultisetThreshold(%s)", minMultisetThreshold, maxMultisetThreshold));
    }
  }

  public static class UpdateParametersCommandBuilder {

    private Boolean rawConverter;
    private Integer pCount;
    private SuffixArrayAlgorithm suffixArrayAlgorithm;
    private Float acutSdFactor;
    private Float qcutSdFactor;
    private Float scutSdFactor;
    private Integer ccutPercentile;
    private Integer knnThreshold;
    private Boolean sloop;
    private Boolean enroll;
    private Float enrollAcutSdFactor;
    private Float enrollQcutSdFactor;
    private Float enrollScutSdFactor;
    private Integer enrollCcutPercentile;
    private Integer enrollKnnThreshold;
    private Float thresholdFactor;
    private Boolean feed;
    private Integer feedThreshold;
    private Integer minMultisetThreshold;
    private Integer maxMultisetThreshold;

    public UpdateParametersCommand build() {
      return new UpdateParametersCommand(
        rawConverter,
        pCount,
        suffixArrayAlgorithm,
        acutSdFactor,
        qcutSdFactor,
        scutSdFactor,
        ccutPercentile,
        knnThreshold,
        sloop,
        enroll,
        enrollAcutSdFactor,
        enrollQcutSdFactor,
        enrollScutSdFactor,
        enrollCcutPercentile,
        enrollKnnThreshold,
        thresholdFactor,
        feed,
        feedThreshold,
        minMultisetThreshold,
        maxMultisetThreshold
      );
    }

    public UpdateParametersCommandBuilder rawConverter(Boolean rawConverter) {
      this.rawConverter = rawConverter;
      return this;
    }

    public UpdateParametersCommandBuilder pCount(Integer pCount) {
      this.pCount = pCount;
      return this;
    }

    public UpdateParametersCommandBuilder suffixArrayAlgorithm(SuffixArrayAlgorithm suffixArrayAlgorithm) {
      this.suffixArrayAlgorithm = suffixArrayAlgorithm;
      return this;
    }

    public UpdateParametersCommandBuilder acutSdFactor(Float acutSdFactor) {
      this.acutSdFactor = acutSdFactor;
      return this;
    }

    public UpdateParametersCommandBuilder qcutSdFactor(Float qcutSdFactor) {
      this.qcutSdFactor = qcutSdFactor;
      return this;
    }

    public UpdateParametersCommandBuilder scutSdFactor(Float scutSdFactor) {
      this.scutSdFactor = scutSdFactor;
      return this;
    }

    public UpdateParametersCommandBuilder ccutPercentile(Integer ccutPercentile) {
      this.ccutPercentile = ccutPercentile;
      return this;
    }

    public UpdateParametersCommandBuilder knnThreshold(Integer knnThreshold) {
      this.knnThreshold = knnThreshold;
      return this;
    }

    public UpdateParametersCommandBuilder sloop(Boolean sloop) {
      this.sloop = sloop;
      return this;
    }

    public UpdateParametersCommandBuilder enroll(Boolean enroll) {
      this.enroll = enroll;
      return this;
    }

    public UpdateParametersCommandBuilder enrollAcutSdFactor(Float enrollAcutSdFactor) {
      this.enrollAcutSdFactor = enrollAcutSdFactor;
      return this;
    }

    public UpdateParametersCommandBuilder enrollQcutSdFactor(Float enrollQcutSdFactor) {
      this.enrollQcutSdFactor = enrollQcutSdFactor;
      return this;
    }

    public UpdateParametersCommandBuilder enrollScutSdFactor(Float enrollScutSdFactor) {
      this.enrollScutSdFactor = enrollScutSdFactor;
      return this;
    }

    public UpdateParametersCommandBuilder enrollCcutPercentile(Integer enrollCcutPercentile) {
      this.enrollCcutPercentile = enrollCcutPercentile;
      return this;
    }

    public UpdateParametersCommandBuilder enrollKnnThreshold(Integer enrollKnnThreshold) {
      this.enrollKnnThreshold = enrollKnnThreshold;
      return this;
    }

    public UpdateParametersCommandBuilder thresholdFactor(Float thresholdFactor) {
      this.thresholdFactor = thresholdFactor;
      return this;
    }

    public UpdateParametersCommandBuilder feed(Boolean feed) {
      this.feed = feed;
      return this;
    }

    public UpdateParametersCommandBuilder feedThreshold(Integer feedThreshold) {
      this.feedThreshold = feedThreshold;
      return this;
    }

    public UpdateParametersCommandBuilder minMultisetThreshold(Integer minMultisetThreshold) {
      this.minMultisetThreshold = minMultisetThreshold;
      return this;
    }

    public UpdateParametersCommandBuilder maxMultisetThreshold(Integer maxMultisetThreshold) {
      this.maxMultisetThreshold = maxMultisetThreshold;
      return this;
    }
  }

  public static class ParametersValidationException extends RuntimeException {

    public ParametersValidationException(@NotNull String errorMessage) {
      super(errorMessage);
    }
  }

  public static UpdateParametersCommandBuilder validExample() {
    return UpdateParametersCommand.builder()
      .suffixArrayAlgorithm(SuffixArrayAlgorithm.LIBDIVSUFSORT)
      .thresholdFactor(1f)
      .minMultisetThreshold(1)
      .maxMultisetThreshold(2);
  }

  public static UpdateParametersCommandBuilder invalidExample() {
    return UpdateParametersCommand.builder();
  }
}
