package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.projection.ClustersProjection.ClustersHierarchyLevel
import com.orange.documentare.clusteringui.biz.file.query.projection.FilesProjection.FilesHierarchyLevel
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import java.util.Comparator.comparing

internal class DisplayHierarchyLevelFileTransform(
  private val filesHierarchyLevel: FilesHierarchyLevel,
  private val clustersHierarchyLevel: ClustersHierarchyLevel,
  private val toFileUIView: (DomainFile) -> FileUIView
) {

  fun filesInWaitingRoom(): List<FileUIView> {
    return clustersHierarchyLevel.filesNotInClusters()
      .map { fileId -> filesHierarchyLevel[fileId] }
      .filter { file -> file != null && file.isInWaitingRoom() }
      .map { file -> toFileUIView(file!!) }
      .sortedWith(comparing { fileUIView: FileUIView -> fileUIView.aggregateId })
  }

  fun filesNotInClusters(): List<FileUIView> {
    return clustersHierarchyLevel.filesNotInClusters()
      .map { fileId -> filesHierarchyLevel[fileId] }
      .filter { file -> file != null && !file.isInWaitingRoom() }
      .map { file -> toFileUIView(file!!) }
      .sortedWith(comparing { fileUIView: FileUIView -> fileUIView.aggregateId })
  }

  fun orderFileViewsInClusterByBizCriteria(): MutableMap<String, ClusterUIView> {
    return orderFilesInClusterByBizCriteria(clustersHierarchyLevel.clusters(), filesHierarchyLevel)
      .map { Pair(it.key, ClusterUIView(it.key, it.value.map { file -> toFileUIView(file) })) }
      .associate { Pair(it.first, it.second) }
      .toMutableMap()
  }

  fun mapClasses(): Map<String, Map<String, ClusterUIView>> {
    val clustersWithOrderedFileViews: MutableMap<String, ClusterUIView> = orderFileViewsInClusterByBizCriteria()
    val classesWithClusterIDs = retrieveClassesWithClusterIDs(clustersHierarchyLevel.classes(), clustersWithOrderedFileViews.values)

    val mapClassClusters: MutableMap<String, Map<String, ClusterUIView>> = mutableMapOf()
    for (mapEntry in classesWithClusterIDs) {
      val className = mapEntry.key
      val classClusterIDs = mapEntry.value
      mapClassClusters[className] = retrieveClassClusters(classClusterIDs, clustersWithOrderedFileViews)
    }

    val clustersNotInClasses: MutableMap<String, ClusterUIView> = orderClustersWithFileViewsBySize(clustersWithOrderedFileViews)
    mapClassClusters[CLASS_NO_NAME] = clustersNotInClasses

    return mapClassClusters
  }

  private fun retrieveClassClusters(
    classClusterIDs: List<String>,
    clustersWithOrderedFileViews: MutableMap<String, ClusterUIView>
  ): Map<String, ClusterUIView> {
    val mapClusterIdClusterFiles: MutableMap<String, ClusterUIView> = mutableMapOf()
    classClusterIDs.forEach { aClassClusterId ->
      val clusterFileViews = clustersWithOrderedFileViews[aClassClusterId]!!
      mapClusterIdClusterFiles[aClassClusterId] = clusterFileViews
      clustersWithOrderedFileViews.remove(aClassClusterId)
    }
    return orderClustersWithFileViewsBySize(mapClusterIdClusterFiles)
  }


  companion object {

    const val CLASS_NO_NAME = "zzzz_No_Named"

    fun retrieveClassesWithClusterIDs(knownClassesWithClusterIDs: Map<String, List<String>>, clusters: Collection<ClusterUIView>): Map<String, List<String>> {
      if (knownClassesWithClusterIDs.isNotEmpty()) return knownClassesWithClusterIDs

      val classesWithMultiHypothesisNamedClass: Map<String, List<String>> = clusters
        .filter { it.multiHypothesisNamedClass() != null }
        .groupBy({ it.multiHypothesisNamedClass()!! }, { it.clusterId })

      return classesWithMultiHypothesisNamedClass
    }

    fun orderFilesInClusterByBizCriteria(clusters: Map<String, List<String>>, filesHierarchyLevel: FilesHierarchyLevel): MutableMap<String, List<DomainFile>> {
      return clusters.map { clusterEntry ->
        val list = clusterEntry.value.mapNotNull { fileId -> filesHierarchyLevel[fileId] }
          .sortedWith(
            comparing { file: DomainFile -> file.clusterCenter() }
              .thenComparing(comparing { file: DomainFile -> file.inMultiset() })
              .thenComparing(comparing { file: DomainFile -> file.notInCentroidNorIncludedAutomatically() })
              .thenComparing(comparing { file: DomainFile -> file.automaticInclude() })
          )
          .reversed()
        Pair(clusterEntry.key, list)
      }.associate { Pair(it.first, it.second) }
        .toMutableMap()
    }

    // FIXME: add unit test
    fun orderClustersWithFileViewsBySize(clustersWithFileUIViews: Map<String, ClusterUIView>): MutableMap<String, ClusterUIView> {
      return clustersWithFileUIViews
        .toList()
        .sortedBy { (_, clusterUIView) -> clusterUIView.size }
        .reversed()
        .toMap()
        .toMutableMap()
    }

    fun with(projections: Projections, toFileUIView: (DomainFile) -> FileUIView): DisplayHierarchyLevelFileTransform {
      val currentHierarchyLevel: Int = projections.files.currentHierarchyLevelIndex()
      val filesHierarchyLevel: FilesHierarchyLevel = projections.files.hierarchyLevel(currentHierarchyLevel)
      val clustersHierarchyLevel: ClustersHierarchyLevel = projections.clusters.hierarchyLevel(currentHierarchyLevel)
      return DisplayHierarchyLevelFileTransform(filesHierarchyLevel, clustersHierarchyLevel, toFileUIView)
    }
  }
}
