package com.orange.documentare.clusteringui.biz.services

import com.orange.documentare.clusteringui.biz.parameters.domain.DistanceParameters


interface ClientHierarchyExport {
  fun exportClientHierarchy(): ClientHierarchyReferenceModel
  fun exportClientHierarchyForStatisticsConsolidation(): ClientHierarchyReferenceModel

  companion object {
    fun cleanLevel0(fullModelWithAllLevel0Files: ClientHierarchyReferenceModel): ClientHierarchyReferenceModel {
      val withLevel0Cleaned = fullModelWithAllLevel0Files.hierarchyLevels.mapIndexed { levelIndex, level: ClientHierarchyReferenceModelLevel ->
        if (levelIndex == 0) {
          // keep only cluster centers and multiset files in level 0
          ClientHierarchyReferenceModelLevel(level.referenceFiles.map { files -> files.filter { it.clusterCenter || it.multiset } })
        } else {
          level
        }
      }
      return ClientHierarchyReferenceModel(withLevel0Cleaned, fullModelWithAllLevel0Files.distanceParameters)
    }

    fun fakeForTestClientHierarchyExport(): ClientHierarchyExport {
      return object : ClientHierarchyExport {
        override fun exportClientHierarchy(): ClientHierarchyReferenceModel {
          return ClientHierarchyReferenceModel(emptyList(), DistanceParameters.Examples.defaults())
        }

        override fun exportClientHierarchyForStatisticsConsolidation(): ClientHierarchyReferenceModel {
          TODO("Not yet implemented")
        }
      }
    }
  }
}
