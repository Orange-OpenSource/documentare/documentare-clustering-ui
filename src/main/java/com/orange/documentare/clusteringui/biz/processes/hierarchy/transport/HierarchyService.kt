package com.orange.documentare.clusteringui.biz.processes.hierarchy.transport

import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederService
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.HierarchyLoop
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.HierarchyLoopRunnable
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.StatisticsConsolidation
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceService
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.infrastructure.event.PersistencePauseAndResume
import com.tophe.ddd.queries.QueryBus
import org.springframework.stereotype.Service

@Service
class HierarchyService : HierarchyStatisticsConsolidation {

  private var hierarchyLoop: HierarchyLoopRunnable? = null

  private lateinit var projections: Projections
  private lateinit var commandBus: CommandBus
  private lateinit var queryBus: QueryBus
  private lateinit var feederService: FeederService
  private lateinit var distanceService: DistanceService
  private lateinit var clientHierarchyExport: ClientHierarchyExport
  private lateinit var persistencePauseAndResume: PersistencePauseAndResume
  private lateinit var statisticsConsolidation: StatisticsConsolidation

  override fun computedDistancesAvailable() = getStatisticsConsolidation().computedDistancesAvailable()

  override fun computedDistances() = getStatisticsConsolidation().computedDistances()

  fun launchHierarchyBuildLoop() {
    if (hierarchyLoop == null) {
      hierarchyLoop = HierarchyLoop(commandBus, queryBus, feederService, persistencePauseAndResume)
    }
    hierarchyLoop!!.run()
  }

  fun launchHierarchyStatisticsConsolidation() {
    getStatisticsConsolidation().run(clientHierarchyExport)
  }

  fun isStatisticsConsolidationRunning() = getStatisticsConsolidation().isRunning()

  fun setCommandBus(commandBus: CommandBus) {
    this.commandBus = commandBus
  }

  fun setQueryBus(queryBus: QueryBus) {
    this.queryBus = queryBus
  }

  fun setFeederService(feederService: FeederService) {
    this.feederService = feederService
  }

  fun setDistanceService(distanceService: DistanceService) {
    this.distanceService = distanceService
  }

  fun setClientHierarchyExport(clientHierarchyExport: ClientHierarchyExport) {
    this.clientHierarchyExport = clientHierarchyExport
  }

  fun setPersistencePauseAndResume(persistencePauseAndResume: PersistencePauseAndResume) {
    this.persistencePauseAndResume = persistencePauseAndResume
  }

  fun setProjections(projections: Projections) {
    this.projections = projections
  }

  fun keepOnlyFirstHierarchyLevel() {
    persistencePauseAndResume.resumeHardPersistenceAndDropVolatilePersistence()
    projections.keepOnlyFirstHierarchyLevel()
  }

  fun testPurposeOverrideHierarchyLoop(hierarchyLoop: HierarchyLoopRunnable) {
    this.hierarchyLoop = hierarchyLoop
  }

  private fun getStatisticsConsolidation() = if (this::statisticsConsolidation.isInitialized) {
    statisticsConsolidation
  } else {
    statisticsConsolidation = StatisticsConsolidation(distanceService)
    statisticsConsolidation
  }
}
