package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.Filename
import com.orange.documentare.core.comp.clustering.stats.DescriptiveStats

data class ClusterDistancesStats(val nbElements: Int, val mean: Double, val standardDeviation: Double, val singleton: Boolean, val description: String = "") {
  data class ClusterDistancesElement(val filename: Filename, val inMultiset: Boolean, val distanceToCentroid: Int?)

  fun threshold(thresholdStandardDeviationFactor: Float): Double {
    if (singleton) {
      return 0.0
    }
    // ⚠️ if only one distance: 'mean = this distance' and 'std = 0' => 'threshold = mean = this distance'
    return (mean + thresholdStandardDeviationFactor * standardDeviation)
  }

  companion object {

    fun buildWithDomainFiles(allFilesInCluster: List<DomainFile>): ClusterDistancesStats {
      return buildWith(allFilesInCluster.map { ClusterDistancesElement(it.filename, it.inMultiset(), it.distanceToCenter) })
    }

    fun buildWith(clusterDistancesElements: List<ClusterDistancesElement>): ClusterDistancesStats {
      if (clusterDistancesElements.isEmpty()) {
        throw IllegalStateException("\n[${ClusterDistancesStats::class.java.simpleName}] no files in cluster")
      }

      if (clusterDistancesElements.size == 1) {
        return singletonStats(clusterDistancesElements.first().filename)
      }

      val clusterDistancesElementsLessMultiset = clusterDistancesElements
        .filter { !it.inMultiset }

      val distances = clusterDistancesElementsLessMultiset.mapNotNull { it.distanceToCentroid }

      // ⚠️ If only one distance: mean = this distance and std = 0
      val statistics = DescriptiveStats().statistics(distances)

      val description = "centroid${
        clusterDistancesElements.filter { it.inMultiset }.map { it.filename }
      } distances(used ${distances.size}, skipped ${clusterDistancesElements.size - clusterDistancesElementsLessMultiset.size}) $statistics ${clusterDistancesElementsLessMultiset.map { "${it.filename} -> ${it.distanceToCentroid}" }}"

      return ClusterDistancesStats(clusterDistancesElements.size, statistics.mean, statistics.standardDeviation, false, description)
    }

    fun buildWith(nbElements: Int, mean: Double, standardDeviation: Double, centroidFileNames: List<String>): ClusterDistancesStats {
      val desc = "centroid${centroidFileNames} nbElements($nbElements) mean($mean) standardDeviation($standardDeviation)"
      return ClusterDistancesStats(nbElements, mean, standardDeviation, nbElements == 1, desc)
    }

    private fun singletonStats(filename: Filename) = ClusterDistancesStats(1, Double.NaN, Double.NaN, true, "centroid[$filename]")

    fun testExample(): ClusterDistancesStats = ClusterDistancesStats(3, 100.0, 10.0, false)
    fun testExampleSingleton(filename: String): ClusterDistancesStats = singletonStats(filename)
    fun testExampleWithThreshold(distanceThreshold: Double) = ClusterDistancesStats(4, distanceThreshold, 0.0, false)
  }
}
