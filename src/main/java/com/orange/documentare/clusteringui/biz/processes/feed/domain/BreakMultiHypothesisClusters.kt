package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.biz.file.command.RemoveSelectedFilesFromClustersCommand
import com.orange.documentare.clusteringui.biz.file.domain.ClusterId
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.domain.FileId
import com.orange.documentare.clusteringui.biz.file.domain.Room
import com.orange.documentare.clusteringui.biz.file.query.AllFilesToRemoveInClusterQuery
import com.orange.documentare.clusteringui.biz.file.query.MultiHypothesisClustersQuery
import com.orange.documentare.clusteringui.biz.file.query.MultiHypothesisClustersView
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.MultiHypothesisClustersBroke
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.infrastructure.bus.BusResponse
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.LoggerFactory

class BreakMultiHypothesisClusters(private val queryBus: QueryBus, private val commandBus: CommandBus) {

  private var previousMultiHypothesisClustersMap: Map<ClusterId, List<DomainFile>> = hashMapOf()

  fun breakClusters(): MultiHypothesisClustersBroke? {
    val queryResponse = queryBus.dispatch<QueryResponse<MultiHypothesisClustersView>>(MultiHypothesisClustersQuery())
    if (queryResponse.failure()) {
      log.error("Failed to retrieve multi-hypothesis clusters: ${queryResponse.failureCause()}")
      return null
    }

    val multiHypothesisClustersMap: Map<ClusterId, List<DomainFile>> = queryResponse.value().multiHypothesisClustersView
    val clustersFilesToRemove = multiHypothesisClustersMap.map { clusterEntry: Map.Entry<ClusterId, List<DomainFile>> ->
      val clusterFiles = clusterEntry.value
      val clusterCenter: DomainFile = clusterFiles.first { (it.clusterCenter != null) && it.clusterCenter }
      val distinctFiles: List<DomainFile> = clusterFiles.filter { !it.fileNamedClass.contentEquals(clusterCenter.fileNamedClass) }
      if (distinctFiles.size == clusterFiles.size - 1) {
        clusterFiles.map { FileId(it.aggregateId) }
      } else {
        distinctFiles.map { FileId(it.aggregateId) }
      }
    }
    val filesIdToMoveToWaitingRoom: List<FileId> = clustersFilesToRemove.flatten()

    val metadataBalancingFinished = isMetadataBalancingFinished(previousMultiHypothesisClustersMap, multiHypothesisClustersMap)
    previousMultiHypothesisClustersMap = multiHypothesisClustersMap

    if (metadataBalancingFinished) {
      return MultiHypothesisClustersBroke(emptyList(), true)
    }

    val ok = removeFilesFromClusters(filesIdToMoveToWaitingRoom)
    return if (ok) {
      MultiHypothesisClustersBroke(filesIdToMoveToWaitingRoom, false)
    } else {
      null
    }
  }

  private fun removeFilesFromClusters(filesIdToMoveToWaitingRoom: List<FileId>): Boolean {
    var ok = false
    val fileIds = filesIdToMoveToWaitingRoom.map { it.fileId }
    queryBus.dispatch<QueryResponse<Map<String, List<DomainFile>>>>(AllFilesToRemoveInClusterQuery(fileIds))
      .onFailure { throwable: Throwable ->
        log.error("Failed to retrieved all files to remove in clusters: $throwable")
      }
      .onSuccess { clusters: Map<String, List<DomainFile>> ->
        commandBus.dispatch<BusResponse<*, *>>(RemoveSelectedFilesFromClustersCommand(fileIds, clusters, Room.WAITING_ROOM))
          .onSuccess { ok = true }
          .onFailure { throwable: Throwable ->
            log.error("Failed to remove selected files from clusters: $throwable")
          }
      }
    return ok
  }

  fun testPurposeUpdatePreviousMultiHypothesisClustersMap(previousMultiHypothesisClustersMap: Map<ClusterId, List<DomainFile>>) {
    this.previousMultiHypothesisClustersMap = previousMultiHypothesisClustersMap
  }

  companion object {
    private val log = LoggerFactory.getLogger(BreakMultiHypothesisClusters::class.java)

    fun isMetadataBalancingFinished(previousMultiHypothesisClustersMap: Map<ClusterId, List<DomainFile>>, multiHypothesisClustersMap: Map<ClusterId, List<DomainFile>>): Boolean {
      val previousFilesInMultiHypothesisClusters: List<DomainFile> = previousMultiHypothesisClustersMap.values.flatten()
      val filesInMultiHypothesisClusters: List<DomainFile> = multiHypothesisClustersMap.values.flatten()
      return previousFilesInMultiHypothesisClusters.size == filesInMultiHypothesisClusters.size
    }
  }
}
