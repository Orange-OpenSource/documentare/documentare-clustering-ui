package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.biz.file.command.MoveFileFromWaitingRoomToSortRoomCommand
import com.orange.documentare.clusteringui.biz.file.command.ResetFileInfoCommand
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.AllFilesInWaitingRoomQuery
import com.orange.documentare.clusteringui.biz.file.query.AllFilesToSortQuery
import com.orange.documentare.clusteringui.biz.file.query.AllSingletonsNotLockedQuery
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.*
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.ClusteringService
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse


open class FeedLoop(
  private val queryBus: QueryBus,
  private val commandBus: CommandBus,
  private var parameters: () -> Parameters,
  private val clusteringService: ClusteringService,
  private val takeFilesRandomly: (List<DomainFile>, Int) -> List<String>,
  private val listener: FeedLoopEventListener = FeedLoopEventListener.noOpListener(),
  private val decrementMultisetThresholdAndBreakClusters: DecrementMultisetThresholdAndBreakClusters = DecrementMultisetThresholdAndBreakClusters(
    commandBus, queryBus, parameters().minMultisetThreshold
  )
) : FeederServiceEvents {

  private val feederPhases = FeederPhases.idle()
  private val breakMultiHypothesisClusters = BreakMultiHypothesisClusters(queryBus, commandBus)

  private var singletonsNotLockedAtLoopStart: List<String> = emptyList()

  var stopFeederWhenPossible: Boolean = false
    private set

  override fun onNewFileCreated() {
    decrementMultisetThresholdAndBreakClusters.reset()
  }

  override fun onStopFeederWhenPossible() {
    stopFeederWhenPossible = true
  }

  open fun loop() {
    var feedLoopCount = 0
    var filesInWaitingRoom = filesInWaitingRoom()

    while (shouldRunLoop(filesInWaitingRoom, singletonsNotLockedAtLoopStart, retrieveAllSingletonsNotLockedIds(), listener)) {
      listener.onFeedLoopEvent(FeedLoopStarted(feedLoopCount))

      val params = parameters()
      decrementMultisetThresholdAndBreakClusters.updateMinMultisetThresholdAtLoopStart(params.minMultisetThreshold)?.let {
        listener.onFeedLoopEvent(MinMultisetThresholdUpdated(it))
      }

      var multiHypothesisClustersBroke: MultiHypothesisClustersBroke? = null
      if (feederPhases.current() == FeederPhases.Phase.METADATA_BALANCING && feederPhases.metadataBalancingStep() == FeederPhases.Phase.MetadataBalancingStep.BREAK_CLUSTERS) {
        multiHypothesisClustersBroke = breakMultiHypothesisClusters.breakClusters()?.apply {
          listener.onFeedLoopEvent(this)
        }
        decrementMultisetThresholdAndBreakClusters.reset().let {
          listener.onFeedLoopEvent(it)
        }
      }

      feederPhases.updateAfterFeedLoopInitialisation(multiHypothesisClustersBroke?.metadataBalancingFinished ?: false)?.let {
        listener.onFeedLoopEvent(it)
        if (feederPhases.current() == FeederPhases.Phase.IDLE) {
          return
        }
      }

      // TODO: si on met simplement 'filesInWaitingRoom' les tests sont verts...
      moveSomeFilesFromWaitingRoomToSortRoom(feedLoopCount, filesInWaitingRoom(), params).let {
        listener.onFeedLoopEvent(FilesMovedFromWaitingRoomToSortRoom(it))
      }

      feedWithSortRoom(feedLoopCount, params, listener)

      filesInWaitingRoom = filesInWaitingRoom()

      feederPhases.updateBeforeLoopFinalisation(filesInWaitingRoom)?.let {
        listener.onFeedLoopEvent(it)
      }

      if (filesInWaitingRoom.isEmpty()) {
        decrementMultisetThresholdAndBreakClusters.checkIfWeNeedToBreakClusters().let {
          listener.onFeedLoopEvent(it)
        }

        filesInWaitingRoom = filesInWaitingRoom()
      }
      feedLoopCount++
    }
  }

  private fun moveSomeFilesFromWaitingRoomToSortRoom(
    feedLoopCount: Int, filesInWaitingRoom: List<DomainFile>, params: Parameters
  ): List<String> {
    val filesIdsToMoveToSortRoom = peekSomeFilesInWaitingRoom(feedLoopCount, getFilesInSortRoom(), filesInWaitingRoom, params.getFeedThreshold())
    return moveFilesFromWaitingRoomToSortRoom(filesIdsToMoveToSortRoom)
  }

  open fun peekSomeFilesInWaitingRoom(feedLoopCount: Int, filesInSortRoom: List<DomainFile>, filesInWaitingRoom: List<DomainFile>, feedThreshold: Int): List<String> {
    if (feedLoopCount == 0 && filesInSortRoom.isNotEmpty()) {
      return emptyList()
    }
    return takeFilesRandomly.invoke(filesInWaitingRoom, feedThreshold)
  }

  open fun shouldRunLoop(
    filesInWaitingRoom: List<DomainFile>, singletonsNotLockedAtPreviousLoopStart: List<String>, singletonsNotLockedAtPreviousLoopEnd: List<String>, listener: FeedLoopEventListener
  ): Boolean {
    val singletonsChanges: SingletonsChanges = SingletonsChanges.computeSingletonsChanges(singletonsNotLockedAtPreviousLoopStart, singletonsNotLockedAtPreviousLoopEnd)
    val hasReachedMinMultisetThreshold = decrementMultisetThresholdAndBreakClusters.hasReachedMinThreshold()

    val multisetBalance = MultisetBalance.get(filesInWaitingRoom, hasReachedMinMultisetThreshold, singletonsChanges.singletonsChanged)

    feederPhases.updateAtFeedLoopStart(filesInWaitingRoom, multisetBalance)?.let {
      listener.onFeedLoopEvent(it)
    }

    if (stopFeederWhenPossible) {
      return false
    }
    return feederPhases.current() != FeederPhases.Phase.IDLE
  }


  open fun feedWithSortRoom(feedLoopCount: Int, params: Parameters, listener: FeedLoopEventListener) {
    if (stopFeederWhenPossible) {
      return
    }

    moveSingletonsNotLockedToSortRoom().let {
      listener.onFeedLoopEvent(SingletonsNotLockedMovedToSortRoom(it))
    }
    if (stopFeederWhenPossible) {
      return
    }

    computeDistancesThenRecalculateModifiedClusters(params).let {
      listener.onFeedLoopEvent(it)
    }
    if (stopFeederWhenPossible) {
      return
    }
    partialClustering(params)?.let {
      listener.onFeedLoopEvent(it)
    }
  }

  open fun moveSingletonsNotLockedToSortRoom(): List<String> {
    singletonsNotLockedAtLoopStart = retrieveAllSingletonsNotLockedIds()
    return moveSingletonsNotLockedToSortRoom(singletonsNotLockedAtLoopStart)
  }

  // TODO: cas d'erreur ?
  private fun retrieveAllSingletonsNotLockedIds() = queryBus.dispatch<QueryResponse<List<String>>>(AllSingletonsNotLockedQuery()).value()

  private fun moveSingletonsNotLockedToSortRoom(singletonsIds: List<String>): List<String> {
    val response = commandBus.dispatch<CommandResponse<ResetFileInfoCommand>>(ResetFileInfoCommand(singletonsIds))
    if (response.failure()) {
      // TODO: test
      throw IllegalStateException(response.failureCause())
    } else {
      return singletonsIds
    }
  }

  open fun computeDistancesThenRecalculateModifiedClusters(params: Parameters): DistancesComputedAndClustersRecalculated {
    return clusteringService.computeDistancesThenRecalculateClustersSync(params)
  }

  open fun partialClustering(params: Parameters): GlobalOrPartialClusteringComputed? {
    val allFilesToSort = getFilesInSortRoom()
    return if (allFilesToSort.isNotEmpty()) {
      clusteringService.globalOrPartialClusteringSync(params, allFilesToSort)
    } else {
      null
    }
  }

  private fun getFilesInSortRoom() = queryBus.dispatch<QueryResponse<List<DomainFile>>>(AllFilesToSortQuery()).value()
  private fun filesInWaitingRoom() = queryBus.dispatch<QueryResponse<List<DomainFile>>>(AllFilesInWaitingRoomQuery()).value()

  private fun moveFilesFromWaitingRoomToSortRoom(filesIds: List<String>): List<String> {
    val response = commandBus.dispatch<CommandResponse<MoveFileFromWaitingRoomToSortRoomCommand>>(MoveFileFromWaitingRoomToSortRoomCommand(filesIds))
    if (response.failure()) {
      // TODO: test
      throw IllegalStateException(response.failureCause())
    }
    return filesIds
  }
}
