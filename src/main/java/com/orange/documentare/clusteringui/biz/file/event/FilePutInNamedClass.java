package com.orange.documentare.clusteringui.biz.file.event;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;
import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FilePutInNamedClassDTO.exampleNamedClass;

import java.time.Instant;

import com.tophe.ddd.infrastructure.event.Event;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FilePutInNamedClass extends Event {

  public final String[] fileNamedClass;

  public FilePutInNamedClass(String aggregateId, String[] fileNamedClass) {
    super(aggregateId);
    this.fileNamedClass = fileNamedClass;
  }

  private FilePutInNamedClass(Instant date, String aggregateId, String[] fileNamedClass) {
    super(aggregateId, date);
    this.fileNamedClass = fileNamedClass;
  }

  public static FilePutInNamedClass buildFromDB(Instant date, String aggregateId, String[] fileNamedClass) {
    return new FilePutInNamedClass(date, aggregateId, fileNamedClass);
  }


  //
  // FOR TESTS
  //
  public static FilePutInNamedClass example() {
    return new FilePutInNamedClass(TEST_DATE, AGGREGATE_ID_EXAMPLE.toString(), exampleNamedClass(AGGREGATE_ID_EXAMPLE));
  }
}
