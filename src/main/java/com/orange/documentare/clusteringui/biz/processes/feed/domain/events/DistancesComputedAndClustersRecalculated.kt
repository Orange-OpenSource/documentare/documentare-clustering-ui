package com.orange.documentare.clusteringui.biz.processes.feed.domain.events

data class DistancesComputedAndClustersRecalculated(val computedDistancesCount: Int?, val recalculatedClustersCount: Int?, val brokeClustersCount: Int) : FeedLoopEvent() {

  override fun displayInternalHtml() =
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ${computedDistancesCount ?: 0} distances computed and ${recalculatedClustersCount ?: 0} clusters recalculated"

  override fun displayInternalRaw() = "      ${computedDistancesCount ?: 0} distances computed and ${recalculatedClustersCount ?: 0} clusters recalculated"

}
