package com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure;


import java.util.Arrays;

import com.orange.documentare.clusteringui.biz.remoteservices.infrastructure.BytesDataUI;
import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm;
import com.orange.documentare.core.computationserver.biz.distances.api.DistancesRequest;
import com.orange.documentare.core.computationserver.infrastructure.transport.BytesDataDTO;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class DistancesRequestUI {

  public final BytesDataUI[] element;
  public final BytesDataUI[] elements;
  public final Boolean rawConverter;
  public final Integer pCount;
  public final SuffixArrayAlgorithm suffixArrayAlgorithm;

  public DistancesRequestUI(BytesDataUI[] element, BytesDataUI[] elements, Boolean rawConverter, Integer pCount, SuffixArrayAlgorithm suffixArrayAlgorithm) {
    this.element = element;
    this.elements = elements;
    this.rawConverter = rawConverter;
    this.pCount = pCount;
    this.suffixArrayAlgorithm = suffixArrayAlgorithm;
  }

  public DistancesRequest toComputationServerDistanceRequest() {
    DistancesRequest distancesRequest = new DistancesRequest();
    distancesRequest.elementDTO = Arrays.stream(element).map(BytesDataUI::toBytesDataDTO).toArray(BytesDataDTO[]::new);
    distancesRequest.elementsDTOs = Arrays.stream(elements).map(BytesDataUI::toBytesDataDTO).toArray(BytesDataDTO[]::new);
    distancesRequest.rawConverter = rawConverter;
    distancesRequest.pCount = pCount;
    distancesRequest.suffixArrayAlgorithm = suffixArrayAlgorithm;
    return distancesRequest;
  }
}

