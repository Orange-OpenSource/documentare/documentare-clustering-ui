package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.orange.documentare.clusteringui.biz.appnotifications.AppNotifications
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure.ClusteringRemoteApi
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceService
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.queries.QueryBus

internal data class ClusteringInfra(
  @JvmField
  val commandBus: CommandBus,
  @JvmField
  val queryBus: QueryBus,
  val clusteringRemote: ClusteringRemoteApi,
  val distanceService: DistanceService,
  val inProgress: ClusteringInProgress,
  val appNotifications: AppNotifications
)
