package com.orange.documentare.clusteringui.biz.appnotifications

interface AppNotifications {
  fun notify(appNotification: AppNotification)
  fun register(appNotificationsListener: AppNotificationListener)
  fun unregister(appNotificationsListener: (appNotifification: AppNotification) -> Unit)
}
