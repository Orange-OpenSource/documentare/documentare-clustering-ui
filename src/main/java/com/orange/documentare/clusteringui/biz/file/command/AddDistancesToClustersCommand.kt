package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster
import com.tophe.ddd.commands.Command

data class AddDistancesToClustersCommand(val fileId: String, val distancesToClustersCenters: List<DistanceToCluster>) : Command