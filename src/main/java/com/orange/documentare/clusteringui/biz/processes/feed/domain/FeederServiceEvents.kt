package com.orange.documentare.clusteringui.biz.processes.feed.domain

interface FeederServiceEvents {
  fun onNewFileCreated()
  fun onStopFeederWhenPossible()
}
