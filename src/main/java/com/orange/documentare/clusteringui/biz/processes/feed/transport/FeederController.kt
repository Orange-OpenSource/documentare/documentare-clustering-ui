package com.orange.documentare.clusteringui.biz.processes.feed.transport

import com.orange.documentare.clusteringui.biz.parameters.command.UpdateParametersCommand
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQuery
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederService
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.infrastructure.bus.BusResponse
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

@Controller
class FeederController(private val commandBus: CommandBus, private val queryBus: QueryBus, private val feederService: FeederService) {
  private val log = LoggerFactory.getLogger(FeederController::class.java)

  @RequestMapping(value = ["/feed"])
  fun feed(): String {
    val prefix = "GET [/feed]"
    log.info(prefix)
    val parameters = parameters()
    if (parameters == null) {
      log.warn("$prefix failed to find parameters, redirect user to form page")
      return "redirect:/parameters"
    }

    val command = UpdateParametersCommand.initBuilderWith(parameters)
      .feed(true)
      .build()

    commandBus.dispatch<BusResponse<*, *>>(command)
      .onSuccess { log.info("$prefix parameters updated: $command") }
      .onFailure { throwable: Any -> log.error("$prefix error: $throwable") }

    feederService.launch()

    return "redirect:/"
  }

  @RequestMapping(value = ["/feed-events"])
  @ResponseBody
  fun feedEvents(): String {
    return FeedEventsPrinter.print(feederService.events())
  }

  private fun parameters(): Parameters? {
    val response = queryBus.dispatch<QueryResponse<Parameters?>>(ParametersQuery())
    return if (response.failure()) null else response.value()
  }
}
