package com.orange.documentare.clusteringui.biz.parameters.domain

import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQuery
import com.orange.documentare.core.comp.bwt.SuffixArrayAlgorithm
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse

data class DistanceParameters(val suffixArrayAlgorithm: SuffixArrayAlgorithm, val rawConverterEnabled: Boolean, val rawConverterPixelCount: Int) {

  object Examples {

    fun defaults() = from(Parameters.defaults())

    fun withNonDefaultPixelCount() = DistanceParameters(defaults().suffixArrayAlgorithm, defaults().rawConverterEnabled, 123456789)

    fun withoutRawConverter() = DistanceParameters(defaults().suffixArrayAlgorithm, false, -1)
  }

  companion object {
    fun from(parameters: Parameters) = DistanceParameters(parameters.suffixArrayAlgorithm, parameters.rawConverter, parameters.pCount)

    fun get(queryBus: QueryBus): DistanceParameters {
      val response = queryBus.dispatch<QueryResponse<Parameters>>(ParametersQuery())
      return if (response.failure() || response.value() == null) {
        throw IllegalStateException("❌  Failed to retrieve parameters: ${response.failureCause()}")
      } else {
        from(response.value())
      }
    }
  }
}
