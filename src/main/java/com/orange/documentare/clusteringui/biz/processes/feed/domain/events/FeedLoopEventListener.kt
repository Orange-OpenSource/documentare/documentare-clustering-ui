package com.orange.documentare.clusteringui.biz.processes.feed.domain.events

interface FeedLoopEventListener {
  fun onFeedLoopEvent(event: FeedLoopEvent)

  companion object {
    fun noOpListener() = object : FeedLoopEventListener {
      override fun onFeedLoopEvent(event: FeedLoopEvent) {
        // do nothing
      }
    }
  }

}
