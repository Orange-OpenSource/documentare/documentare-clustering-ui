package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeedLoopEvent

interface FeederService {
  fun launch(feederProcessFinished: FeederProcessFinished? = null)
  fun events(): List<FeedLoopEvent> = emptyList()
}
