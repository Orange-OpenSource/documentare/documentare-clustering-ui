package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler

class HierarchiesQueryHandler(private val projections: Projections) : QueryHandler<HierarchiesQuery, HierarchiesView>() {

  override fun doExecute(_query: HierarchiesQuery): HierarchiesView {
    val hierarchies = mutableMapOf<Int, Map<String, List<DomainFile>>>()
    val hierarchyLevelCount = projections.hierarchyLevelCount()
    for (i in 0 until hierarchyLevelCount) {
      val clustersHierarchyLevel = projections.clusters.hierarchyLevel(i)
      val filesHierarchyLevel = projections.files.hierarchyLevel(i)
      val clustersOfDomainFiles: Map<String, List<DomainFile>> = clustersHierarchyLevel.clusters().mapValues { it.value.map { fileId -> filesHierarchyLevel[fileId]!! } }
      hierarchies[i] = clustersOfDomainFiles
    }
    return HierarchiesView(hierarchies)
  }
}
