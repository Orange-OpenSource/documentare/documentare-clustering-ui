package com.orange.documentare.clusteringui.biz.file.transport

import com.orange.documentare.clusteringui.biz.file.command.BinPurgeCommand
import com.orange.documentare.clusteringui.biz.file.query.BinQuery
import com.orange.documentare.clusteringui.biz.file.query.BinView
import com.orange.documentare.clusteringui.infrastructure.security.Authent
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.LoggerFactory
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody
import java.security.Principal
import java.util.*

@Controller
class Bin(private val commandBus: CommandBus, private val queryBus: QueryBus) {
  private val log = LoggerFactory.getLogger(Bin::class.java)

  @RequestMapping("/bin")
  fun index(principal: Principal?, model: Model): String {
    Authent.addAuthenticatedModel(model, principal)
    model.addAttribute("title", "Bin")

    val response = queryBus.dispatch<QueryResponse<BinView>>(BinQuery())
    response
      .onSuccess { log.info("GET [/bin]") }
      .onFailure { throwable: Throwable -> log.error("GET [/bin] error: $throwable") }

    model.addAttribute("binView", response.value())

    return "bin"
  }

  @Secured("ROLE_USER")
  @ResponseBody
  @RequestMapping(value = ["api/bin/purge"], method = [RequestMethod.POST])
  fun mvcBinPurge(@RequestBody filesIds: Array<String>?): String {
    if (filesIds == null) {
      return "redirect:/bin"
    }
    val binPurgePrefix = "DELETE [/bin/purge]" + Arrays.toString(filesIds)
    commandBus.dispatch<CommandResponse<Void>>(BinPurgeCommand(listOf(*filesIds)))
      .onSuccess { log.info(binPurgePrefix) }
      .onFailure { log.error("$binPurgePrefix error: $it") }

    return "redirect:/bin"
  }

}