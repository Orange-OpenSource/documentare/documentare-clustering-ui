package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.persistence.EventRepository

class AddFilesToClustersCommandHandler(private val eventRepository: EventRepository, eventBus: EventBus) : CommandHandler<AddFilesToClustersCommand, Void>(eventBus) {

  override fun doExecute(command: AddFilesToClustersCommand): AggregateAndEvents<Void?, Collection<Event>> {
    val events: List<Event> = command.associateFiles
      .flatMap { addFileToCluster(it.idFileToSort, it.idClusterCenter) }
      .map { it!! }
    return AggregateAndEvents.withOnlyEvents(events)
  }

  private fun addFileToCluster(idFileToAddToCluster: String, idClusterCenter: String): List<Event?> {
    val events: MutableList<Event?> = ArrayList()
    val fileToAddToCluster = loadFile(idFileToAddToCluster) ?: return events
    val clusterCenter = loadFile(idClusterCenter) ?: return events
    val distancesToCluster = fileToAddToCluster.distancesToClustersCenters ?: return events

    val distanceToCluster =
      distancesToCluster.find { d: DistanceToCluster -> d.clusterId == clusterCenter.clusterId }
        ?: return events

    events.add(
      moveFileToCluster(fileToAddToCluster, distanceToCluster)
    )
    events.addAll(
      tagClusterAsModified(clusterCenter)
    )
    return events
  }

  private fun moveFileToCluster(file: DomainFile, cluster: DistanceToCluster) =
    file.moveToCluster(cluster.clusterId, null, cluster.distance, null, null, null, null)

  private fun tagClusterAsModified(clusterCenter: DomainFile) = listOf(clusterCenter.tagClusterModified())

  private fun loadFile(fileId: String) = eventRepository.load(fileId)
}
