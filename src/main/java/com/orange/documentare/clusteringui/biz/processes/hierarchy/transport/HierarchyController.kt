package com.orange.documentare.clusteringui.biz.processes.hierarchy.transport

import com.orange.documentare.clusteringui.Couple
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport
import com.orange.documentare.clusteringui.infrastructure.io.FileIO.createExportClientFile
import jakarta.servlet.http.HttpServletResponse
import org.slf4j.LoggerFactory
import org.springframework.core.io.FileSystemResource
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod.GET
import org.springframework.web.bind.annotation.ResponseBody
import java.io.FileOutputStream

@Controller
@RequestMapping("/hierarchy")
class HierarchyController(private val hierarchyService: HierarchyService, private val clientHierarchyExport: ClientHierarchyExport) {
  private val log = LoggerFactory.getLogger(HierarchyController::class.java)


  @RequestMapping("/build")
  fun buildHierarchy(): String {
    val prefix = "GET [/hierarchy]"
    log.info(prefix)

    hierarchyService.launchHierarchyBuildLoop()

    return "redirect:/"
  }

  @RequestMapping("/consolidate-with-statistics")
  fun consolidateWithStatistics(): String {
    val prefix = "GET [/consolidate-with-statistics]"
    log.info(prefix)

    hierarchyService.launchHierarchyStatisticsConsolidation()

    return "redirect:/"
  }

  @RequestMapping("/keep-only-first-level")
  fun keepOnlyFirstHierarchyLevel(): String {
    val prefix = "GET [/keep-only-first-level]"
    log.info(prefix)

    hierarchyService.keepOnlyFirstHierarchyLevel()

    return "redirect:/"
  }

  @Secured("ROLE_USER")
  @ResponseBody
  @RequestMapping("/client-export.json", method = [GET])
  fun clientExport(response: HttpServletResponse): FileSystemResource {
    val prefix = "GET [/client-export.json]"
    log.info(prefix)

    val couple: Couple<String, FileOutputStream> = createExportClientFile()
    val filePath = couple.first
    val outputStream = couple.second
    outputStream.write(clientHierarchyExport.exportClientHierarchy().serialize().toByteArray())
    outputStream.close()

    response.contentType = "application/json"
    response.setHeader("Content-Disposition", "attachment; filename=client-export.json")

    return FileSystemResource(filePath)
  }
}
