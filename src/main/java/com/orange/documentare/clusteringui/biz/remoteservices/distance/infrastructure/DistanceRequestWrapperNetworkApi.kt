package com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure

import com.orange.documentare.tasksdispatcher.RemoteTaskDTO
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface DistanceRequestWrapperNetworkApi {
  @POST("/distances")
  fun computationApi(@Body distancesRequestUI: DistancesRequestUI): Call<RemoteTaskDTO>

  @GET("/task/{taskId}")
  fun taskApi(@Path("taskId") taskId: String): Call<DistancesRequestResult>
}
