package com.orange.documentare.clusteringui.biz.remoteservices.infrastructure;
/*
 * Copyright (c) 2016 Orange
 *
 * Authors: Denis Boisset & Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import java.io.File;
import java.util.List;

import org.jetbrains.annotations.NotNull;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.orange.documentare.core.computationserver.infrastructure.transport.BytesDataDTO;

import lombok.EqualsAndHashCode;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@EqualsAndHashCode
public final class BytesDataUI {

  public final List<String> ids;
  public final List<String> filepaths;
  public final byte[] bytes;

  public BytesDataUI(List<String> ids, List<String> filepaths) {
    this(ids, filepaths, null);
    for (String filepath : filepaths) {
      if (!(new File(filepath)).isFile()) {
        throw new IllegalStateException("File is not a file: " + filepath);
      }
    }
  }

  private BytesDataUI(List<String> ids, List<String> filepaths, byte[] bytes) {
    this.ids = ids;
    this.filepaths = filepaths;
    this.bytes = bytes;
  }

  @NotNull
  public BytesDataDTO toBytesDataDTO() {
    return new BytesDataDTO(ids, filepaths, bytes);
  }
}
