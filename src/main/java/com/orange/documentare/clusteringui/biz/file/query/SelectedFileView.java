package com.orange.documentare.clusteringui.biz.file.query;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class SelectedFileView {

  public List<String> idToSort;
  public List<String> idSorted;
  public List<String> idToName;

  public SelectedFileView(List<String> idToSort, List<String> idSorted, List<String> idToName) {
    this.idToSort = idToSort;
    this.idSorted = idSorted;
    this.idToName = idToName;
  }
}
