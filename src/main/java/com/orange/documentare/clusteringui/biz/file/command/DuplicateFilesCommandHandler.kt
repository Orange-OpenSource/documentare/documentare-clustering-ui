package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.persistence.EventRepository


class DuplicateFilesCommandHandler(private val eventRepository: EventRepository, eventBus: EventBus) : CommandHandler<DuplicateFilesCommand, Void>(eventBus) {

  override fun doExecute(command: DuplicateFilesCommand): AggregateAndEvents<Void?, Collection<Event>> {
    val events: List<Event> = command.filesIds
      .mapNotNull { duplicateFile(it) }

    return AggregateAndEvents.withOnlyEvents(events)
  }

  private fun duplicateFile(id: String): Event? = loadFile(id)?.duplicate()

  private fun loadFile(fileId: String): DomainFile? = eventRepository.load(fileId)
}