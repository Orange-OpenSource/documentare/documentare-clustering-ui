package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.orange.documentare.clusteringui.TryOrCatch
import org.slf4j.Logger
import org.slf4j.LoggerFactory

internal data class ClusteringAsyncTreatment(
  val clusteringInProgress: ClusteringInProgress
) {
  private val log: Logger = LoggerFactory.getLogger(ClusteringAsyncTreatment::class.java)

  fun start(runnable: () -> Unit) {
    Thread {
      TryOrCatch
        .run {
          clusteringInProgress.setInProgress(true)
          runnable.invoke()
        }
        .onFailure { log.error("clustering error occurred", it) }
        .andFinally { clusteringInProgress.setInProgress(false) }
    }.start()
  }
}
