package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.persistence.EventRepository


class AddDistancesToClustersCommandHandler(private val eventRepository: EventRepository, eventBus: EventBus) : CommandHandler<AddDistancesToClustersCommand, Void>(eventBus) {

  override fun doExecute(command: AddDistancesToClustersCommand): AggregateAndEvents<Void?, Collection<Event>> {
    val domainFile = loadFile(command.fileId)
      ?: throw IllegalStateException("file with following aggregateId not found: " + command.fileId)

    val tuple: Event? = domainFile.addDistancesToClustersCenters(command.distancesToClustersCenters)
    return AggregateAndEvents.withOnlyEvents(if (tuple != null) listOf(tuple) else emptyList())
  }

  private fun loadFile(fileId: String): DomainFile? {
    return eventRepository.load(fileId)
  }
}
