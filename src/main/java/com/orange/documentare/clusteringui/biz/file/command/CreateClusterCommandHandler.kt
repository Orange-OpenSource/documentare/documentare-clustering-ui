package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.persistence.EventRepository


class CreateClusterCommandHandler(private val eventRepository: EventRepository, eventBus: EventBus) : CommandHandler<CreateClusterCommand, Void>(eventBus) {

  override fun doExecute(command: CreateClusterCommand): AggregateAndEvents<Void?, Collection<Event>> {
    val clusterId = command.clusterId
    val files: MutableList<DomainFile> = command.filesIds.mapNotNull { fileId -> loadFile(fileId) }.toMutableList()
    val events = mutableListOf<Event>()
    val lastFile = files[files.size - 1]
    files.remove(lastFile)

    // Cluster center
    events.add(
      addFileToCluster(lastFile, clusterId, true)
    )
    // other files
    events.addAll(
      files.map { file: DomainFile -> addFileToCluster(file, clusterId, null) }
    )

    return AggregateAndEvents.withOnlyEvents(events)
  }

  private fun addFileToCluster(lastFile: DomainFile, clusterId: String, clusterCenter: Boolean?): Event =
    lastFile.moveToCluster(clusterId, clusterCenter, null, null, null, null, null)

  private fun loadFile(fileId: String): DomainFile? =
    eventRepository.load(fileId)
}