package com.orange.documentare.clusteringui.biz.appnotifications.transport

import com.orange.documentare.clusteringui.biz.appnotifications.AppNotification
import com.orange.documentare.clusteringui.biz.appnotifications.AppNotifications
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter

@Controller
class NotificationsEmitter(private val appNotifications: AppNotifications) {

  private val log = LoggerFactory.getLogger(NotificationsEmitter::class.java)

  @GetMapping("/app-notifications")
  fun eventEmitter(): SseEmitter {
    val emitter = SseEmitter(-1)

    val listener: (appNotification: AppNotification) -> Unit = { listener(it, emitter) }

    emitter.onCompletion {
      log.info("unregister app notifications listener")
      appNotifications.unregister(listener)
    }
    emitter.onTimeout {
      log.warn("SSE emitter timeout")
    }
    log.info("register new app notifications listener")
    appNotifications.register(listener)

    return emitter
  }

  fun listener(appNotification: AppNotification, emitter: SseEmitter) {
    try {
      emitter.send(appNotification.name)
    } catch (e: Exception) {
      emitter.completeWithError(e)
    }
  }
}


