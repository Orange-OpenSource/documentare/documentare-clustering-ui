package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.ClusterId
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler

class MultiHypothesisClustersQueryHandler(private val projections: Projections) : QueryHandler<MultiHypothesisClustersQuery, MultiHypothesisClustersView>() {

  override fun doExecute(_query: MultiHypothesisClustersQuery): MultiHypothesisClustersView {
    val clusters = projections.clusters.currentHierarchyLevel().clusters()
    val multiHypothesisClusters = clusters.filter { cluster: Map.Entry<String, MutableList<String>> ->
      val clusterFilesId: MutableList<String> = cluster.value
      val clusterDomainFiles: List<DomainFile> = clusterFilesId.map { fileId -> projections.files[fileId] }
      val clusterDistinctFileNamedClasses: List<String> = clusterDomainFiles.mapNotNull { it.fileNamedClass?.toList() }.flatten().distinct()
      clusterDistinctFileNamedClasses.size > 1
    }
    return MultiHypothesisClustersView(multiHypothesisClusters.map { clusterEntry ->
      Pair(
        ClusterId(clusterEntry.key),
        clusterEntry.value.map { projections.files.currentHierarchyLevel()[it]!! })
    }.toMap())
  }
}
