package com.orange.documentare.clusteringui.biz.file.event;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;

import java.time.Instant;

import com.tophe.ddd.infrastructure.event.Event;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ClusterNotModified extends Event {

  public final String clusterId;

  public ClusterNotModified(String aggregateId, String clusterId) {
    super(aggregateId);
    this.clusterId = clusterId;
  }

  private ClusterNotModified(Instant date, String aggregateId, String clusterId) {
    super(aggregateId, date);
    this.clusterId = clusterId;
  }

  public static ClusterNotModified buildFromDB(Instant date, String aggregateId, String clusterId) {
    return new ClusterNotModified(date, aggregateId, clusterId);
  }

  //
  // FOR TESTS
  //
  public static ClusterNotModified example() {
    return new ClusterNotModified(TEST_DATE, AGGREGATE_ID_EXAMPLE.toString(), exampleClusterId(AGGREGATE_ID_EXAMPLE));
  }
}
