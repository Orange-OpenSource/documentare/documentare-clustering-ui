package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeedLoopEvent

class FeedLoopEventMemory(private val events: MutableList<FeedLoopEvent> = mutableListOf()) {
  fun events(): List<FeedLoopEvent> {
    return events.toList()
  }

  fun add(event: FeedLoopEvent) {
    if (events.size == MEMORY_LIMIT) {
      events.removeAt(0)
    }
    events.add(event)
  }

  companion object {
    private const val MEMORY_LIMIT = 300
  }
}
