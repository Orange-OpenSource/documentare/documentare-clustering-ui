package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.orange.documentare.clusteringui.biz.appnotifications.AppNotification
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.GlobalOrPartialClusteringComputed
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.PurgeBin.purgeBin
import org.slf4j.LoggerFactory

internal class GlobalOrPartialClustering {
  private val log = LoggerFactory.getLogger(GlobalOrPartialClustering::class.java)

  fun doAsync(infra: ClusteringInfra, parameters: Parameters, files: List<DomainFile>, filesIdsInBinToPurge: List<String>) {
    purgeBin(infra.commandBus, filesIdsInBinToPurge)

    ClusteringAsyncTreatment(infra.inProgress).start {
      doSync(infra, parameters, files)
    }
  }

  fun doSync(infra: ClusteringInfra, parameters: Parameters, files: List<DomainFile>): GlobalOrPartialClusteringComputed? {
    var globalOrPartialClusteringComputed: GlobalOrPartialClusteringComputed? = null
    infra.clusteringRemote.calculateClustering(parameters, files)
      .onFailure { log.error("calculate clustering error occurred", it) }
      .onSuccess {
        SaveClustering.instance(infra).saveClustering(it, files)
        globalOrPartialClusteringComputed = GlobalOrPartialClusteringComputed(files.size)
      }
    infra.appNotifications.notify(AppNotification.GLOBAL_OR_PARTIAL_CLUSTERING_DONE)
    return globalOrPartialClusteringComputed
  }
}
