package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.infrastructure.io.FileIO

data class FileUIView(
  val aggregateId: String,
  val filename: String,
  val clusterCenter: Boolean,
  val thumbnailPath: String,
  val filePath: String,
  val distancesToCluster: List<DistanceToCluster?>,
  val computeDistanceInProgress: Boolean,
  val automaticInclude: Boolean,
  val clusterModified: Boolean,
  val inMultiset: Boolean,
  val lock: Boolean,
  val fileNamedClass: List<String>?,
  val distanceToCenter: Int?,
  val viewStyleBackgroundColor: String,
  val viewStyleTextColor: String
) {

  fun displayName(): String {
    val concatClasses: String? = fileNamedClass?.joinToString("")
    val concatClassesWithSeparator: String? = fileNamedClass?.joinToString(" | ")
    return filename + if (concatClasses.isNullOrBlank()) "" else " ∈ $concatClassesWithSeparator"
  }

  fun withDistanceComputationInProgress(distanceComputationInProgress: Boolean): FileUIView {
    return FileUIView(
      aggregateId,
      filename,
      clusterCenter,
      thumbnailPath,
      filePath,
      distancesToCluster,
      distanceComputationInProgress,
      automaticInclude,
      clusterModified,
      inMultiset,
      lock,
      fileNamedClass,
      distanceToCenter,
      viewStyleBackgroundColor,
      viewStyleTextColor
    )
  }

  companion object {
    @JvmStatic
    fun from(f: DomainFile): FileUIView {
      val (backgroundColor, textColor) = viewStyleColor(
        f.clusterCenter(),
        f.inMultiset(),
        f.automaticInclude(),
        f.clusterModified(),
        f.locked(),
        f.clusterId,
        f.distancesToClustersCenters
      )
      return FileUIView(
        f.aggregateId,
        f.filename,
        f.clusterCenter(),
        FileIO.thumbnailWebPathOf(f),
        FileIO.fileWebPathOf(f),
        f.distancesToClustersCenters ?: emptyList(),
        false,
        f.automaticInclude(),
        f.clusterModified(),
        f.inMultiset(),
        f.locked(),
        f.fileNamedClass?.asList(),
        f.distanceToCenter,
        backgroundColor,
        textColor
      )
    }

    private fun viewStyleColor(
      clusterCenter: Boolean,
      inMultiset: Boolean,
      automaticInclude: Boolean,
      clusterModified: Boolean,
      locked: Boolean,
      clusterId: String?,
      distancesToClustersCenters: List<DistanceToCluster>?
    ): Pair<String, String> {

      // if the file is in a cluster -> SORTED
      if (clusterId != null) {
        if (clusterCenter) {
          if (locked) {
            return Pair("lightseagreen", "black")
          }
          if (clusterModified) {
            return Pair("aqua", "black")
          }
          return Pair("green", "white")
        }

        if (inMultiset) {
          return Pair("chartreuse", "black")
        }

        if (automaticInclude) {
          return Pair("orangered", "black")
        }

      } else {
        // if the file is not in a cluster -> NOT SORTED

        if (!distancesToClustersCenters.isNullOrEmpty()) {
          return Pair("blueviolet", "white")
        }
      }

      // nothing special
      return Pair("white", "black")
    }
  }
}
