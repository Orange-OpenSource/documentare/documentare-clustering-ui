package com.orange.documentare.clusteringui.biz.file.transport

import com.orange.documentare.clusteringui.biz.file.command.CreateClusterCommand
import com.orange.documentare.clusteringui.biz.file.command.LockClustersCommand
import com.orange.documentare.clusteringui.biz.file.command.RemoveSelectedFilesFromClustersCommand
import com.orange.documentare.clusteringui.biz.file.command.UnLockClustersCommand
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.domain.Room
import com.orange.documentare.clusteringui.biz.file.query.AllFilesToRemoveInClusterQuery
import com.orange.documentare.clusteringui.biz.file.query.SelectedFilesQuery
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQuery
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederServiceImpl
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.ClusteringServiceImpl
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceServiceImpl
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.infrastructure.bus.BusResponse
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import lombok.extern.slf4j.Slf4j
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.annotation.Secured
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody
import java.util.*

@Slf4j
@Controller
class Cluster {

  private val log: Logger = LoggerFactory.getLogger(Cluster::class.java)

  @Autowired
  lateinit var commandBus: CommandBus

  @Autowired
  lateinit var queryBus: QueryBus

  @Autowired
  lateinit var clusteringService: ClusteringServiceImpl

  @Autowired
  lateinit var distanceService: DistanceServiceImpl

  @Autowired
  lateinit var feederService: FeederServiceImpl


  @Secured("ROLE_USER")
  @ResponseBody
  @RequestMapping(value = ["/api/lock-cluster"], method = [RequestMethod.POST])
  fun apiLockCluster(@RequestBody selectedFileIdsSorted: Array<String>?): String {
    if (selectedFileIdsSorted == null) {
      return "redirect:/"
    }
    val prefix = "LOCK [/clustering-files/] " + Arrays.toString(selectedFileIdsSorted)
    commandBus.dispatch<CommandResponse<Void>>(LockClustersCommand(listOf(*selectedFileIdsSorted)))
      .onSuccess { log.info(prefix) }
      .onFailure { throwable: Any -> log.error("$prefix error: $throwable") }
    return "redirect:/"
  }


  @Secured("ROLE_USER")
  @ResponseBody
  @RequestMapping(value = ["/api/unlock-cluster"], method = [RequestMethod.POST])
  fun apiUnLockCluster(@RequestBody selectedFileIdsSorted: Array<String>?): String {
    if (selectedFileIdsSorted == null) {
      return "redirect:/"
    }
    val prefix = "UNLOCK [/clustering-files/] " + Arrays.toString(selectedFileIdsSorted)
    commandBus.dispatch<CommandResponse<Void>>(UnLockClustersCommand(listOf(*selectedFileIdsSorted)))
      .onSuccess { log.info(prefix) }
      .onFailure { throwable: Any? -> log.error("$prefix error: ", throwable) }
    return "redirect:/"
  }

  @Secured("ROLE_USER")
  @ResponseBody
  @RequestMapping(value = ["/api/create-new-cluster"], method = [RequestMethod.POST])
  fun createNewCluster(@RequestBody selectedFileIdsToSort: Array<String>?): String {
    if (selectedFileIdsToSort == null) {
      return "redirect:/"
    }
    val prefix = "POST [/api/create-new-cluster]" + Arrays.toString(selectedFileIdsToSort)
    log.info(prefix)
    val parameters = parameters()
    if (parameters == null) {
      log.warn("$prefix failed to find parameters, redirect user to form page")
      return "redirect:/parameters"
    }
    if (selectedFileIdsToSort.size > 1) {
      (queryBus.dispatch<QueryResponse<List<DomainFile>>>(SelectedFilesQuery(listOf(*selectedFileIdsToSort))))
        .onFailure { throwable: Throwable -> logError(throwable) }
        .onSuccess { selectedFiles: List<DomainFile> ->
          clusteringService.createNewClusterAsync(
            parameters, selectedFiles, emptyList()
          )
        }
    } else {
      commandBus.dispatch<CommandResponse<Void>>(CreateClusterCommand(listOf(*selectedFileIdsToSort), UUID.randomUUID().toString()))
    }
    return "redirect:/"
  }

  private fun parameters(): Parameters? {
    val response = queryBus.dispatch<QueryResponse<Parameters?>>(ParametersQuery())
    return if (response.failure()) null else response.value()
  }

  @Secured("ROLE_USER")
  @ResponseBody
  @RequestMapping(value = ["/api/remove-file"], method = [RequestMethod.POST])
  fun apiRemoveFiles(@RequestBody selectedFileIdsSorted: Array<String>?): String {
    if (selectedFileIdsSorted == null) {
      return "redirect:/"
    }
    val fileInRoom: Room
    val parameters = parameters()
    fileInRoom = if (parameters!!.isFeed && !feederService.isRunning) {
      Room.WAITING_ROOM
    } else {
      Room.SORT_ROOM
    }
    val prefix = "POST [/api/remove-file]" + Arrays.toString(selectedFileIdsSorted)
    log.info(prefix)
    (queryBus.dispatch<QueryResponse<Map<String, List<DomainFile>>>>(AllFilesToRemoveInClusterQuery(listOf(*selectedFileIdsSorted))))
      .onFailure { throwable: Throwable -> logError(throwable) }
      .onSuccess { clusters: Map<String, List<DomainFile>> ->
        commandBus.dispatch<BusResponse<*, *>>(RemoveSelectedFilesFromClustersCommand(listOf(*selectedFileIdsSorted), clusters, fileInRoom))
          .onSuccess { log.info(prefix) }
          .onFailure { throwable: Any -> log.error("$prefix error: $throwable") }
      }
    return "redirect:/"
  }


  private fun logError(throwable: Any) {
    log.error(ClusteringServiceImpl::class.java.simpleName + "clustering failed: " + throwable, throwable)
  }

  @Secured("ROLE_USER")
  @ResponseBody
  @RequestMapping(value = ["/api/createCluster"], method = [RequestMethod.POST])
  fun createCluster(@RequestBody selectedFileIdsToSort: Array<String>?): String {
    if (selectedFileIdsToSort == null) {
      log.warn("/api/createCluster: selectedFileIdsToSort is NULL")
      return "redirect:/"
    }
    val prefix = "POST [/api/createCluster]" + Arrays.toString(selectedFileIdsToSort)
    log.info(prefix)
    val parameters = parameters()
    if (parameters == null) {
      log.warn("$prefix failed to find parameters, redirect user to form page")
      return "redirect:/parameters"
    }
    if (selectedFileIdsToSort.size > 1) {
      (queryBus.dispatch<QueryResponse<List<DomainFile>>>(SelectedFilesQuery(listOf(*selectedFileIdsToSort))))
        .onFailure { throwable: Throwable -> logError(throwable) }
        .onSuccess { selectedFiles: List<DomainFile> ->
          clusteringService.createNewClusterAsync(
            parameters, selectedFiles, emptyList()
          )
        }
    } else {
      commandBus.dispatch<CommandResponse<Void>>(CreateClusterCommand(listOf(*selectedFileIdsToSort), UUID.randomUUID().toString()))
    }
    return "redirect:/"
  }
}
