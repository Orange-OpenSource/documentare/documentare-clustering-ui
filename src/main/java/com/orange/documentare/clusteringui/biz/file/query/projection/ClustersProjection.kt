package com.orange.documentare.clusteringui.biz.file.query.projection

import com.orange.documentare.clusteringui.biz.file.event.*
import com.tophe.ddd.infrastructure.event.Event

class ClustersProjection {
  class ClustersHierarchyLevel {
    private val filesNotInClusters = mutableListOf<String>()
    private val clusters = mutableMapOf<String, MutableList<String>>()
    private val classes = mutableMapOf<String, MutableList<String>>()

    fun filesNotInClusters() = filesNotInClusters.toList()
    fun clusters() = clusters.toMap()
    fun classes(): Map<String, List<String>> = classes.toMap()

    private fun removeFromFilesNotInClusters(aggregateId: String?) {
      filesNotInClusters.remove(aggregateId)
    }

    private fun clusterWithId(clusterId: String) = clusters.getOrDefault(clusterId, mutableListOf())
    private fun classWithName(className: String) = classes.getOrDefault(className, mutableListOf())

    fun addClusterToNamedClass(clusterId: String, className: String) {
      val clusterIdsInClass = classWithName(className)
      if (!clusterIdsInClass.contains(clusterId)) {
        clusterIdsInClass.add(clusterId)
      }
      classes[className] = clusterIdsInClass
    }

    fun removeClusterFromNamedClass(clusterId: String) {
      // do not modify classes inplace, to avoid ConcurrentModificationException
      val classNamesToRemove = mutableListOf<String>()
      classes.keys.forEach { className ->
        val clusterIds = classWithName(className)
        clusterIds.contains(clusterId).let {
          clusterIds.remove(clusterId)
          if (clusterIds.isEmpty()) {
            classNamesToRemove.add(className)
          }
        }
      }
      classNamesToRemove.forEach { classes.remove(it) }
    }

    private fun removeFromClusters(aggregateId: String, clusterId: String?) {
      if (clusterId == null || !clusters.containsKey(clusterId)) {
        return
      }
      val filesInCluster = clusterWithId(clusterId)
      filesInCluster.apply {
        remove(aggregateId)
        if (isEmpty()) {
          clusters.remove(clusterId)
          removeClusterFromNamedClass(clusterId)
        }
      }
    }

    fun addToCluster(aggregateId: String, clusterId: String, clusterCenter: Boolean) {
      val filesInCluster = clusterWithId(clusterId)
      if (clusterCenter) {
        filesInCluster.add(0, aggregateId)
      } else {
        filesInCluster.add(aggregateId)
      }
      clusters[clusterId] = filesInCluster
    }

    fun addToFilesNotInACluster(aggregateId: String, clusterId: String? = null) {
      if (aggregateId !in filesNotInClusters) {
        filesNotInClusters.add(aggregateId)
        clusterId?.let { removeFromClusters(aggregateId, it) }
      }
    }

    fun addFileToCluster(aggregateId: String, clusterId: String, clusterCenter: Boolean, className: String?) {
      removeFromFilesNotInClusters(aggregateId)
      addToCluster(aggregateId, clusterId, clusterCenter)
      className?.let { addClusterToNamedClass(clusterId, it) }
    }

    fun removeFileFromProjection(aggregateId: String, clusterId: String?) {
      removeFromFilesNotInClusters(aggregateId)
      removeFromClusters(aggregateId, clusterId)
    }
  }

  private class HierarchyLevels {

    private val hierarchyLevels: MutableList<ClustersHierarchyLevel> = emptyOneHierarchyLevel()

    private fun emptyOneHierarchyLevel() = mutableListOf(ClustersHierarchyLevel())

    fun get() = hierarchyLevels.last()
    fun hierarchyLevelsCount() = hierarchyLevels.size
    fun createNextHierarchyLevel() {
      hierarchyLevels.add(ClustersHierarchyLevel())
    }

    operator fun get(hierarchyLevel: Int) = hierarchyLevels[hierarchyLevel]

    fun nextHierarchyLevelIndex() = currentHierarchyLevelIndex() + 1

    fun currentHierarchyLevelIndex() = hierarchyLevelsCount() - 1

    fun reset() {
      hierarchyLevels.clear()
      hierarchyLevels.addAll(emptyOneHierarchyLevel())
    }

    fun keepOnlyFirstHierarchyLevel() {
      val firstLevel = get(0)
      hierarchyLevels.clear()
      hierarchyLevels.add(firstLevel)
    }
  }

  private val hierarchyLevels = HierarchyLevels()

  fun currentHierarchyLevel() = hierarchyLevels.get()

  @Synchronized
  fun filesNotInClusters() = currentHierarchyLevel().filesNotInClusters()

  @Synchronized
  fun clusters(): Map<String, List<String>> = currentHierarchyLevel().clusters()

  @Synchronized
  fun classes(): Map<String, List<String>> = currentHierarchyLevel().classes()

  fun nextHierarchyLevelIndex(): Int = hierarchyLevels.nextHierarchyLevelIndex()

  fun hierarchyLevelCount(): Int = hierarchyLevels.hierarchyLevelsCount()

  fun currentHierarchyLevelIndex(): Int = hierarchyLevels.currentHierarchyLevelIndex()

  private fun createHierarchyLevelIfNecessary(event: MultisetElementMovedToNextHierarchyLevel) {
    if (currentHierarchyLevelIndex() < event.nextHierarchyLevel) {
      hierarchyLevels.createNextHierarchyLevel()
    }
  }

  fun hierarchyLevel(hierarchyLevelIndex: Int) = hierarchyLevels[hierarchyLevelIndex]

  @Synchronized
  fun reset() {
    hierarchyLevels.reset()
  }

  @Synchronized
  fun keepOnlyFirstHierarchyLevel() {
    hierarchyLevels.keepOnlyFirstHierarchyLevel()
  }

  @Synchronized
  fun initFromHistory(events: Iterable<Event>) = events.forEach { onEvent(it) }

  @Synchronized
  fun onEvent(e: Event) = when (e) {
    is FileCreated -> onFileCreated(e)
    is FileAddedToCluster -> onFileAddedToCluster(e)
    is FileRemovedFromCluster -> onFileRemovedFromClusterOrReset(e, e.clusterId)
    is FileInfoReset -> onFileRemovedFromClusterOrReset(e, e.clusterId)
    is FileDeleted -> onFileDuplicatedOrDeleted(e, e.clusterId)
    is FileDuplicated -> onFileDuplicatedOrDeleted(e, e.clusterId)
    is ClusterPutInNamedClass -> onClusterAddedToNamedClass(e)
    is ClusterUnlocked -> onClusterRemoveFromNamedClass(e)
    is MultisetElementMovedToNextHierarchyLevel -> onMultisetMovedToNextHierarchyLevel(e)
    else -> {
    }
  }

  private fun onMultisetMovedToNextHierarchyLevel(event: MultisetElementMovedToNextHierarchyLevel) {
    createHierarchyLevelIfNecessary(event)
    currentHierarchyLevel().addToFilesNotInACluster(event.aggregateId)
  }

  private fun onClusterAddedToNamedClass(e: ClusterPutInNamedClass) =
    e.clusterId?.let { currentHierarchyLevel().addClusterToNamedClass(e.clusterId, e.className) }

  private fun onClusterRemoveFromNamedClass(e: ClusterUnlocked) =
    e.clusterId?.let { currentHierarchyLevel().removeClusterFromNamedClass(e.clusterId) }

  private fun onFileCreated(e: FileCreated) {
    if (e.clusterId == null) {
      currentHierarchyLevel().addToFilesNotInACluster(e.aggregateId, null)
    } else {
      currentHierarchyLevel().addToCluster(e.aggregateId, e.clusterId, e.clusterCenter())
    }
  }

  private fun onFileAddedToCluster(e: FileAddedToCluster) {
    if (e.clusterId != null) {
      currentHierarchyLevel().addFileToCluster(e.aggregateId, e.clusterId, e.clusterCenter(), e.className)
    }
  }

  private fun onFileRemovedFromClusterOrReset(e: Event, clusterId: String?) =
    currentHierarchyLevel().addToFilesNotInACluster(e.aggregateId, clusterId)

  private fun onFileDuplicatedOrDeleted(e: Event, clusterId: String?) =
    currentHierarchyLevel().removeFileFromProjection(e.aggregateId, clusterId)
}
