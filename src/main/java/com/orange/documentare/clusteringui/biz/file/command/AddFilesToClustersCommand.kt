package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.biz.file.domain.AssociateFile
import com.tophe.ddd.commands.Command

class AddFilesToClustersCommand(val associateFiles: List<AssociateFile>) : Command