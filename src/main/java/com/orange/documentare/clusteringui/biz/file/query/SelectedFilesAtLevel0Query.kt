package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats.AggregateId
import com.tophe.ddd.queries.Query

data class SelectedFilesAtLevel0Query(val aggregatesId: List<AggregateId>) : Query
