package com.orange.documentare.clusteringui.biz.file.transport

import com.orange.documentare.clusteringui.biz.file.hierarchyvisu.TreantHierarchyDTO
import com.orange.documentare.clusteringui.biz.file.query.HierarchiesView
import com.orange.documentare.clusteringui.infrastructure.security.Authent
import com.tophe.ddd.queries.QueryBus
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import java.security.Principal

@Controller
class Tree(private val queryBus: QueryBus) {
  @RequestMapping("/tree")
  fun tree(principal: Principal?, model: Model): String {
    Authent.addAuthenticatedModel(model, principal)
    model.addAttribute("title", "Tree")
    model.addAttribute("nodeStructure", TreantHierarchyDTO.from(HierarchiesView.get(queryBus)))

    return "tree"
  }
}
