package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse

typealias HierarchyLevel = Int
typealias ClusterId = String

class HierarchiesView(val hierarchies: Map<HierarchyLevel, Map<ClusterId, List<DomainFile>>>) {
  fun topHierarchyLevel() = hierarchies.size - 1
  fun level(hierarchyLevel: Int) = hierarchies[hierarchyLevel]!!
  fun topNodes(): List<DomainFile> {
    // top nodes are singletons => can take only first
    return level(topHierarchyLevel()).values.map { it.first() }
  }

  override fun toString(): String {
    // return a string representation of the object
    return StringBuilder().apply {
      append("HierarchiesView:\n")
      hierarchies.forEach { (level, clusters) ->
        append("\tLevel $level:\n")
        clusters.forEach { (clusterId, files) ->
          append("\t\tCluster '$clusterId':\t${files.joinToString { displayFile(it) }}\n")
        }
      }
    }.toString()
  }

  private fun displayFile(domainFile: DomainFile): String {
    val extra = if (domainFile.clusterCenter()) "*" else if (domainFile.inMultiset()) "**" else ""
    return domainFile.filename + extra
  }

  companion object {
    fun get(queryBus: QueryBus): HierarchiesView {
      val response = queryBus.dispatch<QueryResponse<HierarchiesView>>(HierarchiesQuery())
      if (response.failure()) {
        throw IllegalStateException(response.failureCause())
      }
      return response.value()
    }
  }
}
