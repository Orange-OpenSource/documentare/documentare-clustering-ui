package com.orange.documentare.clusteringui.biz.file.query

import com.tophe.ddd.queries.Query

data class ClusterFilesQuery(val clusterId: String, val hierarchyLevel: Int? = null) : Query
