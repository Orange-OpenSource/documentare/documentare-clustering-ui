package com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.SelectedFilesQuery
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse

internal object FileDeletedInCluster {

  @JvmStatic
  fun aFileWasDeletedInTheCluster(queryBus: QueryBus, filesId: List<String>): Boolean {
    val query = queryBus.dispatch<QueryResponse<List<DomainFile>>>(SelectedFilesQuery(filesId))
    if (query.value().isNotEmpty()) {
      for (file in query.value()) {
        if (file.deleted()) {
          return true
        }
      }
    }
    return false
  }
}
