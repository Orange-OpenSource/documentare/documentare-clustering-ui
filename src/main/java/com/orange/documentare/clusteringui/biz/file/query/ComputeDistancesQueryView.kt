package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile

class ComputeDistancesQueryView(val mapClusterMultiset: Map<String, List<DomainFile>>, val files: List<DomainFile>)