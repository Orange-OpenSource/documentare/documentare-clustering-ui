package com.orange.documentare.clusteringui.biz.file.command

import com.tophe.ddd.commands.Command

class DeleteFilesCommand(val filesIds: List<String>) : Command