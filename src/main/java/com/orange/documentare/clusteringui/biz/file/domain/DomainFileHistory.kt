package com.orange.documentare.clusteringui.biz.file.domain

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile.Companion.builder
import com.orange.documentare.clusteringui.biz.file.event.*
import com.tophe.ddd.infrastructure.event.Event

object DomainFileHistory {

  fun apply(file: DomainFile, event: Event): DomainFile = apply(file.toBuilder(), event).build()

  @JvmStatic
  fun from(events: Iterable<Event>): DomainFile {
    var builder = builder()
    for (event in events) {
      builder = apply(builder, event)
    }
    return builder.build()
  }

  private fun applyFileCreated(builder: DomainFileBuilder, e: FileCreated): DomainFileBuilder = builder
    .aggregateId(e.aggregateId)
    .filename(e.filename)
    .clusterId(e.clusterId)
    .clusterCenter(e.clusterCenter)
    .inWaitingRoom(e.waitingRoom)

  private fun applyFileAddedToCluster(builder: DomainFileBuilder, e: FileAddedToCluster): DomainFileBuilder = builder
    .clusterId(e.clusterId)
    .clusterCenter(e.clusterCenter)
    .distanceToCenter(e.distanceToCenter)
    .automaticInclude(e.automaticInclude)
    .inMultiset(e.inMultiset)
    .lock(e.lock)
    .className(e.className)

  private fun applyFileRemovedFromCluster(builder: DomainFileBuilder, e: FileRemovedFromCluster): DomainFileBuilder =
    builder
      .clusterId(null)
      .clusterCenter(null)
      .clusterModified(null)
      .automaticInclude(null)
      .inMultiset(null)
      .distanceToCenter(null)
      .lock(null)
      .className(null)
      .inWaitingRoom(e.inWaitingRoom)

  private fun applyDistancesToClusterCenterAdded(builder: DomainFileBuilder, e: DistancesToClusterCenterAdded): DomainFileBuilder =
    builder.distancesToClustersCenters(e.distancesToClustersCenters)

  private fun applyClusterModified(builder: DomainFileBuilder): DomainFileBuilder = builder.clusterModified(true)

  private fun applyClusterNotModified(builder: DomainFileBuilder): DomainFileBuilder = builder.clusterModified(false)

  private fun applyClusterLocked(builder: DomainFileBuilder, e: ClusterLocked): DomainFileBuilder = builder.lock(e.lock)

  private fun applyClusterUnLocked(builder: DomainFileBuilder, e: ClusterUnlocked): DomainFileBuilder =
    builder.lock(e.lock).className(null)

  private fun applyClusterPutInNamedClass(builder: DomainFileBuilder, e: ClusterPutInNamedClass): DomainFileBuilder =
    builder.className(e.className)

  private fun applyFilePutInNamedClass(builder: DomainFileBuilder, e: FilePutInNamedClass): DomainFileBuilder =
    builder.fileNamedClass(e.fileNamedClass)

  private fun applyFileDuplicated(builder: DomainFileBuilder): DomainFileBuilder =
    applyFileInfoReset(builder).duplicate(true)

  private fun applyFileInfoReset(builder: DomainFileBuilder): DomainFileBuilder {
    val file = builder.build()
    return builder()
      .aggregateId(file.aggregateId)
      .filename(file.filename)
      .delete(file.delete)
      .fileNamedClass(file.fileNamedClass)
  }

  private fun applyMultisetMovedToNextHierarchyLevel(builder: DomainFileBuilder): DomainFileBuilder {
    val file = builder.build()
    return builder()
      .aggregateId(file.aggregateId)
      .filename(file.filename)
      .fileNamedClass(file.fileNamedClass)
      .inWaitingRoom(true)
  }

  private fun applyFileMovedFromWaitingRoomToSortRoom(builder: DomainFileBuilder): DomainFileBuilder {
    val file = builder.build()
    return builder()
      .aggregateId(file.aggregateId)
      .filename(file.filename)
      .inWaitingRoom(false)
      .fileNamedClass(file.fileNamedClass)
  }

  private fun applyFileDeleted(builder: DomainFileBuilder): DomainFileBuilder =
    applyFileInfoReset(builder).delete(true)

  private fun apply(builder: DomainFileBuilder, e: Event): DomainFileBuilder =
    when (e) {
      is FileCreated -> applyFileCreated(builder, e)
      is FileAddedToCluster -> applyFileAddedToCluster(builder, e)
      is FileRemovedFromCluster -> applyFileRemovedFromCluster(builder, e)
      is DistancesToClusterCenterAdded -> applyDistancesToClusterCenterAdded(builder, e)
      is ClusterModified -> applyClusterModified(builder)
      is ClusterNotModified -> applyClusterNotModified(builder)
      is FileDuplicated -> applyFileDuplicated(builder)
      is FileInfoReset -> applyFileInfoReset(builder)
      is FileDeleted -> applyFileDeleted(builder)
      is FileMovedFromWaitingRoomToSortRoom -> applyFileMovedFromWaitingRoomToSortRoom(builder)
      is ClusterLocked -> applyClusterLocked(builder, e)
      is ClusterUnlocked -> applyClusterUnLocked(builder, e)
      is ClusterPutInNamedClass -> applyClusterPutInNamedClass(builder, e)
      is FilePutInNamedClass -> applyFilePutInNamedClass(builder, e)
      is MultisetElementMovedToNextHierarchyLevel -> applyMultisetMovedToNextHierarchyLevel(builder)
      else -> throw IllegalStateException("No Matcher for event: " + e.javaClass)
    }
}
