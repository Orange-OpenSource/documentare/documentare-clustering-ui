package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederPhases.Phase.*
import com.orange.documentare.clusteringui.biz.processes.feed.domain.events.FeederPhaseUpdated

class FeederPhases private constructor(private var current: Phase, private var metadataBalancingStep: MetadataBalancingStep? = null) {
  fun updateAtFeedLoopStart(filesInWaitingRoom: List<DomainFile>, multisetBalance: MultisetBalance): FeederPhaseUpdated? {
    val updateType = UpdateType.AT_FEED_LOOP_START

    if (current == IDLE && filesInWaitingRoom.isNotEmpty()) {
      current = INCREMENTAL_CLUSTERING
      return FeederPhaseUpdated(updateType, current)
    }
    if (current == MULTISET_BALANCING && multisetBalance.isFinished()) {
      metadataBalancingStep = MetadataBalancingStep.BREAK_CLUSTERS
      current = METADATA_BALANCING
      FeederPhaseUpdated(updateType, current, metadataBalancingStep)
    }
    if (current == METADATA_BALANCING && multisetBalance.isFinished()) {
      metadataBalancingStep = MetadataBalancingStep.BREAK_CLUSTERS
      return FeederPhaseUpdated(updateType, current, metadataBalancingStep)
    }
    return null // no update
  }

  fun updateBeforeLoopFinalisation(filesInWaitingRoom: List<DomainFile>): FeederPhaseUpdated? {
    val updateType = UpdateType.BEFORE_LOOP_FINALISATION

    if (current == INCREMENTAL_CLUSTERING && filesInWaitingRoom.isEmpty()) {
      current = MULTISET_BALANCING
      return FeederPhaseUpdated(updateType, current)
    }
    if (current == METADATA_BALANCING && filesInWaitingRoom.isEmpty()) {
      metadataBalancingStep = MetadataBalancingStep.MULTISET_BALANCING
      return FeederPhaseUpdated(updateType, current, metadataBalancingStep)
    }
    return null // no update
  }

  fun updateAfterFeedLoopInitialisation(metadataBalancingFinished: Boolean): FeederPhaseUpdated? {
    val updateType = UpdateType.AFTER_FEED_LOOP_INITIALISATION

    if (current == METADATA_BALANCING) {
      return if (metadataBalancingFinished) {
        current = IDLE
        FeederPhaseUpdated(updateType, current)
      } else {
        metadataBalancingStep = MetadataBalancingStep.INCREMENTAL_CLUSTERING
        FeederPhaseUpdated(updateType, METADATA_BALANCING, MetadataBalancingStep.INCREMENTAL_CLUSTERING)
      }
    }
    return null
  }

  fun current() = current
  fun metadataBalancingStep() = metadataBalancingStep

  enum class Phase {
    IDLE,
    INCREMENTAL_CLUSTERING,
    MULTISET_BALANCING,
    METADATA_BALANCING;

    enum class MetadataBalancingStep {
      BREAK_CLUSTERS,
      INCREMENTAL_CLUSTERING,
      MULTISET_BALANCING
    }
  }

  enum class UpdateType {
    AT_FEED_LOOP_START,
    AFTER_FEED_LOOP_INITIALISATION,
    BEFORE_LOOP_FINALISATION
  }

  companion object {
    fun idle() = FeederPhases(IDLE)

    fun testPurposeCreateWithPhase(phase: Phase, metadataBalancingStep: MetadataBalancingStep? = null) = FeederPhases(phase, metadataBalancingStep)
  }
}
