package com.orange.documentare.clusteringui.biz.file.event;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.DistancesToClusterCenterAddedDTO.exampleDistancesToCenters;
import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;

import java.time.Instant;
import java.util.List;

import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster;
import com.tophe.ddd.infrastructure.event.Event;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DistancesToClusterCenterAdded extends Event {

  public final List<DistanceToCluster> distancesToClustersCenters;

  public DistancesToClusterCenterAdded(String aggregateId, List<DistanceToCluster> distancesToClustersCenters) {
    super(aggregateId);
    this.distancesToClustersCenters = distancesToClustersCenters;
  }

  private DistancesToClusterCenterAdded(Instant date, String aggregateId, List<DistanceToCluster> distancesToClustersCenters) {
    super(aggregateId, date);
    this.distancesToClustersCenters = distancesToClustersCenters;
  }

  public static DistancesToClusterCenterAdded buildFromDB(Instant date, String aggregateId, List<DistanceToCluster> distancesToClustersCenters) {
    return new DistancesToClusterCenterAdded(date, aggregateId, distancesToClustersCenters);
  }


  //
  // FOR TESTS
  //
  public static DistancesToClusterCenterAdded example() {
    return new DistancesToClusterCenterAdded(TEST_DATE, AGGREGATE_ID_EXAMPLE.toString(), exampleDistancesToCenters(AGGREGATE_ID_EXAMPLE));
  }
}
