package com.orange.documentare.clusteringui.biz.file.query

data class DisplayHierarchyLevelFilesView(
  val hierarchyLevelCount: Int,
  val currentHierarchyLevel: Int,
  val noFiles: Boolean = false,
  val notDeleted: List<FileUIView>? = null,
  val notInClusters: List<FileUIView>? = null,
  val mapClassViews: Map<String, Map<String, ClusterUIView>>? = null,
)
