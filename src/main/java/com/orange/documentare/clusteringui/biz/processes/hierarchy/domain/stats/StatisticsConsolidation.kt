package com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats

import com.orange.documentare.clusteringui.biz.file.event.FileCreated
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceService
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyExport
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModel
import com.orange.documentare.clusteringui.biz.services.ClientHierarchyReferenceModelFile
import org.slf4j.LoggerFactory
import java.util.*
import kotlin.concurrent.thread

typealias AggregateId = String
typealias Filename = String
typealias LevelIndex = Int
typealias DistanceToAggregate = Int
typealias Cluster = List<StatisticsConsolidation.AggregateWithCentroidInfo>
typealias Clusters = List<Cluster>

class StatisticsConsolidation(private val distanceService: DistanceService) : StatisticsConsolidationRunnable {

  data class ClusterCenterAggregate(val aggregateId: AggregateId, val filename: Filename, val centroid: List<AggregateId>, val levelIndex: Int) {
    override fun toString() = "$filename[$levelIndex]"
  }

  data class AggregateWithCentroidInfo(val aggregateId: AggregateId, val filename: Filename, val clusterCenter: Boolean, val multiset: Boolean) {
    override fun toString() = filename

    fun toAggregate() = Aggregate(aggregateId, filename)
  }

  data class Aggregate(val aggregateId: AggregateId, val filename: Filename) {
    override fun toString() = filename
  }

  data class DistancesToCompute(val distancesToCompute: List<Pair<LevelIndex, Map<ClusterCenterAggregate, List<Aggregate>>>>) {
    override fun toString(): String {
      val stringBuilder = StringBuilder()
      stringBuilder.append("\n\nDistances to compute for cluster statistics:\n")
      distancesToCompute.forEach { (levelIndex, distancesToComputesAtLevel) ->
        stringBuilder.append("\n\tLevel $levelIndex:\n")
        distancesToComputesAtLevel.forEach { (clusterCenterAggregate, aggregates) ->
          stringBuilder.append("\t\tcentroid of cluster '${clusterCenterAggregate.filename}'\t->\t${aggregates.map { it.filename }}\n")
        }
      }
      return stringBuilder.toString()
    }
  }

  data class ComputedDistances(val computedDistances: List<Pair<LevelIndex, Map<ClusterCenterAggregate, List<Pair<Aggregate, DistanceToAggregate>>>>>) {

    override fun toString(): String {
      val stringBuilder = StringBuilder()
      stringBuilder.append("\n\nComputed distances for cluster statistics:\n")
      computedDistances.forEach { (levelIndex, computedDistancesAtLevel) ->
        stringBuilder.append("\n\tLevel $levelIndex:\n")
        computedDistancesAtLevel.forEach { (clusterCenterAggregate: ClusterCenterAggregate, distances: List<Pair<Aggregate, DistanceToAggregate>>) ->
          stringBuilder.append("\t\tdistances to centroid '${clusterCenterAggregate.filename}'\t->\t${distances}\n")
        }
      }
      return stringBuilder.toString()
    }

    fun forCluster(hierarchyLevelIndex: LevelIndex, clusterCenterAggregateId: AggregateId): Pair<ClusterCenterAggregate, List<Pair<Aggregate, DistanceToAggregate>>> {
      val levelClustersDistances: Pair<LevelIndex, Map<ClusterCenterAggregate, List<Pair<Aggregate, DistanceToAggregate>>>>? =
        computedDistances.find { it.first == hierarchyLevelIndex }
      if (levelClustersDistances == null) {
        throw IllegalStateException("❌  No computed distances available for level $hierarchyLevelIndex")
      }

      val levelClustersMap: Map<ClusterCenterAggregate, List<Pair<Aggregate, DistanceToAggregate>>> = levelClustersDistances.second
      val levelClustersMapWithClusterAggregateId: List<Pair<ClusterCenterAggregate, List<Pair<Aggregate, DistanceToAggregate>>>> =
        levelClustersMap.map { entry: Map.Entry<ClusterCenterAggregate, List<Pair<Aggregate, DistanceToAggregate>>> ->
          entry.key to entry.value
        }
      val cluster: Pair<ClusterCenterAggregate, List<Pair<Aggregate, DistanceToAggregate>>>? =
        levelClustersMapWithClusterAggregateId.find { it.first.aggregateId == clusterCenterAggregateId }
      val clusterDistances: List<Pair<Aggregate, DistanceToAggregate>> = cluster?.second
        ?: throw IllegalStateException("❌  No computed distances available for cluster '$clusterCenterAggregateId' at level $hierarchyLevelIndex")
      val clusterCenterAggregate: ClusterCenterAggregate = cluster.first

      // TODO CLIENT ++++  could be empty if we eliminate previously multiset files distance computation
      if (clusterDistances.isEmpty()) {
        throw IllegalStateException("❌  cluster distances (without cluster center) is an empty list")
      }
      return Pair(
        clusterCenterAggregate,
        clusterDistances
      )
    }
  }

  private var isRunning = false
  private var computedDistances: ComputedDistances? = null

  fun isRunning() = isRunning

  fun computedDistancesAvailable() = computedDistances != null

  fun computedDistances(): ComputedDistances {
    return if (computedDistancesAvailable()) {
      computedDistances!!
    } else {
      throw IllegalStateException("Computed Distances not available yet, call computedDistancesAvailable() to check first")
    }
  }

  override fun run(clientHierarchyExport: ClientHierarchyExport) {
    isRunning = true
    computedDistances = null
    thread {
      try {
        computedDistances = doRun(clientHierarchyExport.exportClientHierarchyForStatisticsConsolidation())
        log.info("\n[StatisticsConsolidation] finished successfully")
      } catch (it: Throwable) {
        log.error("[StatisticsConsolidation] failed: '${it.message}'", it)
      } finally {
        isRunning = false
      }
    }
  }

  private fun doRun(clientHierarchyReferenceModel: ClientHierarchyReferenceModel): ComputedDistances {
    val distancesToCompute: DistancesToCompute = collectDistancesToCompute(clientHierarchyReferenceModel)
    return computeDistances(distancesToCompute)
  }

  private fun computeDistances(distancesToCompute: DistancesToCompute): ComputedDistances {
    val computedDistances: MutableList<Pair<LevelIndex, Map<ClusterCenterAggregate, List<Pair<Aggregate, DistanceToAggregate>>>>> = mutableListOf()
    distancesToCompute.distancesToCompute.forEach { (levelIndex, distancesToComputeAtLevel: Map<ClusterCenterAggregate, List<Aggregate>>) ->
      val computedDistancesAtLevel = distancesToComputeAtLevel.map { (clusterCenterAggregate, aggregates) ->
        val distances: List<Pair<Aggregate, DistanceToAggregate>> = doComputeDistances(clusterCenterAggregate, aggregates)
        Pair(clusterCenterAggregate, distances)
      }
      computedDistances.add(Pair(levelIndex, computedDistancesAtLevel.toMap()))
    }
    return ComputedDistances(computedDistances.toList())
  }

  private fun doComputeDistances(
    clusterCenterAggregate: ClusterCenterAggregate,
    aggregates: List<Aggregate>
  ): List<Pair<Aggregate, DistanceToAggregate>> {
    val distances = distanceService.computeDistancesToCentroidAnywhereInHierarchyInSameThread(aggregates.map { it.aggregateId }, clusterCenterAggregate.centroid)
    return distances.map { (aggregateId, distance) ->
      val aggregate = aggregates.find { it.aggregateId == aggregateId }!!
      Pair(aggregate, distance)
    }
  }

  companion object {

    fun collectDistancesToCompute(clientHierarchyReferenceModel: ClientHierarchyReferenceModel): DistancesToCompute {
      // each list element is a level in the hierarchy, containing a list of clusters
      val aggregatesHierarchy: List<Clusters> = clientHierarchyReferenceModel.hierarchyLevels
        .map { it.referenceFiles }
        .map { referenceFilesList: List<List<ClientHierarchyReferenceModelFile>> ->
          referenceFilesList.map { referenceFiles: List<ClientHierarchyReferenceModelFile> ->
            referenceFiles.map { AggregateWithCentroidInfo(it.aggregateId, it.filename, it.clusterCenter, it.multiset) }
          }
        }

      return doCollectDistancesToCompute(aggregatesHierarchy)
    }

    // each list element is a level in the hierarchy, containing a list of clusters
    private fun doCollectDistancesToCompute(aggregatesHierarchyLevels: List<Clusters>): DistancesToCompute {

      val distancesToCompute = mutableListOf<Pair<LevelIndex, Map<ClusterCenterAggregate, List<Aggregate>>>>()

      aggregatesHierarchyLevels.forEachIndexed { levelIndex, clusters ->
        // clusters at index 0 (leafs) already contain cluster statistics
        if (levelIndex == 0) {
          return@forEachIndexed
        }
        val distancesToComputeAtCurrentLevel = mutableMapOf<ClusterCenterAggregate, List<Aggregate>>()
        clusters.forEach { cluster: Cluster ->
          val centroid = cluster.filter { aggregateWithCentroidInfo -> aggregateWithCentroidInfo.clusterCenter || aggregateWithCentroidInfo.multiset }.map { it.aggregateId }
          val clusterCenterAggregate: ClusterCenterAggregate =
            cluster.filter { it.clusterCenter }.map { ClusterCenterAggregate(it.aggregateId, it.filename, centroid, levelIndex) }.first()
          distancesToComputeAtCurrentLevel[clusterCenterAggregate] = findChildrenInLowerLevels(cluster.map { it.toAggregate() }, aggregatesHierarchyLevels, levelIndex - 1)
        }
        distancesToCompute.add(Pair(levelIndex, distancesToComputeAtCurrentLevel.toMap()))
      }

      return DistancesToCompute(distancesToCompute.toList())
    }

    private fun findChildrenInLowerLevels(parents: List<Aggregate>, aggregatesHierarchyLevels: List<Clusters>, levelIndex: Int): List<Aggregate> {
      val levelClusters = aggregatesHierarchyLevels[levelIndex]
      val clustersContainingAtLeastOneParent = findClustersContainingParents(parents, levelClusters.map { cluster -> cluster.map { it.toAggregate() } })
      val newParents = clustersContainingAtLeastOneParent.flatten()
      if (levelIndex == 0) {
        return newParents
      }
      return findChildrenInLowerLevels(newParents, aggregatesHierarchyLevels, levelIndex - 1)
    }

    private fun findClustersContainingParents(parents: List<Aggregate>, levelClusters: List<List<Aggregate>>): List<List<Aggregate>> {
      return levelClusters.filter { levelCluster ->
        // disjoint: true if the two collections have no elements in common
        !Collections.disjoint(
          // map to aggregateId: do not compare objects since as there are in distinct levels
          // the clusterCenter/multiset flags may differ
          levelCluster.map { it.aggregateId },
          parents.map { it.aggregateId }
        )
      }
    }

    private val log = LoggerFactory.getLogger(StatisticsConsolidation::class.java)
  }


  object ComputedDistancesExamples {

    private const val CLUSTER_CENTER_AGGREGATE_INDEX_LEVEL_1 = 100
    private const val CLUSTER_MULTISET_AGGREGATE_INDEX_LEVEL_1 = 101
    private val CLUSTER_MULTISET_AGGREGATE_INDEX_LEVEL_1_ID = aId(101)
    private const val CLUSTER_CENTER_AGGREGATE_INDEX_LEVEL_2 = 200
    private val clusterCenterAggregateLevel1 = cca(CLUSTER_CENTER_AGGREGATE_INDEX_LEVEL_1, 1, CLUSTER_MULTISET_AGGREGATE_INDEX_LEVEL_1_ID)
    private val clusterCenterAggregateLevel2 = cca(CLUSTER_CENTER_AGGREGATE_INDEX_LEVEL_2, 1)
    val clusterCenterLevel1AggregateId = clusterCenterAggregateLevel1.aggregateId
    val clusterCenterLevel2AggregateId = clusterCenterAggregateLevel2.aggregateId

    val computedDistancesWith3Levels: ComputedDistances = ComputedDistances(
      listOf(
        // Level 0
        Pair(
          0, mapOf(
            // cluster 1
            Pair(
              cca(0, 0),
              listOf(
                Pair(a(0), 0)
              )
            )
          )
        ),
        // Level 1
        Pair(
          1, mapOf(
            // cluster 1
            Pair(
              cca(10, 1),
              listOf(
                Pair(a(10), 0),
                Pair(a(11), 1),
                Pair(a(12), 3),
                Pair(a(13), 5)
              )
            ),
            // cluster 2
            Pair(
              clusterCenterAggregateLevel1,
              listOf(
                Pair(a(CLUSTER_CENTER_AGGREGATE_INDEX_LEVEL_1), 0),
                Pair(a(CLUSTER_MULTISET_AGGREGATE_INDEX_LEVEL_1), 11),
                Pair(a(102), 20),
                Pair(a(103), 30)
              )
            ),
            // cluster 3
            Pair(
              cca(110, 1),
              listOf(
                Pair(a(110), 0),
                Pair(a(111), 21),
                Pair(a(112), 23),
                Pair(a(113), 25)
              )
            )
          )
        ),
        // Level 2
        Pair(
          2, mapOf(
            // cluster 1
            Pair(
              clusterCenterAggregateLevel2,
              listOf(
                Pair(a(CLUSTER_CENTER_AGGREGATE_INDEX_LEVEL_2), 0)
              )
            )
          )
        )
      )
    )


    // TODO CLIENT +++ we can leave distance from cluster center to cluster center to handle multiset centroid case?
    val computedDistancesWithTwoLevels = ComputedDistances(
      listOf(
        // level 1
        Pair(
          1, mapOf(
            Pair(cca_(0, 1), listOf(a_(0) to 0, a_(1) to 1, a_(2) to 2, a_(3) to 3, a_(4) to 4, a_(5) to 5, a_(6) to 6, a_(7) to 7)),
            Pair(cca_(4, 1), listOf(a_(4) to 4, a_(5) to 5, a_(6) to 6))
          )
        ),
        // level 2
        Pair(
          2, mapOf(
            Pair(cca_(0, 2), listOf(a_(0) to 0, a_(1) to 1, a_(2) to 2, a_(3) to 3, a_(4) to 4, a_(5) to 5, a_(6) to 6, a_(7) to 7))
          )
        )
      )
    )

    // cluster center aggregate
    fun cca(aggregateIndex: Int, levelIndex: Int, multisetAggregateIndex: AggregateId? = null): ClusterCenterAggregate {
      val centroid = mutableListOf(aId(aggregateIndex))
      if (multisetAggregateIndex != null) {
        centroid.add(multisetAggregateIndex)
      }
      return ClusterCenterAggregate(aId(aggregateIndex), "f$aggregateIndex", centroid, levelIndex)
    }

    // aggregate
    fun aId(aggregateIndex: Int) = "aggregate-id-$aggregateIndex"
    fun a(aggregateIndex: Int) = Aggregate(aId(aggregateIndex), "f$aggregateIndex")

    // cluster center aggregate with real UUID
    fun cca_(aggregateIndex: Int, levelIndex: Int) =
      ClusterCenterAggregate(FileCreated.aggregateIdExample(aggregateIndex), "f$aggregateIndex", listOf(FileCreated.aggregateIdExample(aggregateIndex)), levelIndex)

    // aggregate with real UUID
    fun a_(aggregateIndex: Int) = Aggregate(FileCreated.aggregateIdExample(aggregateIndex), "f$aggregateIndex")


  }
}
