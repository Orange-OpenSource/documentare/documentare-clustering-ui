package com.orange.documentare.clusteringui.biz.processes.hierarchy.domain.stats

interface StatValue {
    fun value(): Int
}
