package com.orange.documentare.clusteringui.biz.file.command

import com.tophe.ddd.commands.Command

data class PutFileInNamedClassCommand(val fileAggregateId: String, val fileNamedClass: Array<String>) : Command {
  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false

    other as PutFileInNamedClassCommand

    if (fileAggregateId != other.fileAggregateId) return false
    if (!fileNamedClass.contentEquals(other.fileNamedClass)) return false

    return true
  }

  override fun hashCode(): Int {
    var result = fileAggregateId.hashCode()
    result = 31 * result + fileNamedClass.contentHashCode()
    return result
  }
}
