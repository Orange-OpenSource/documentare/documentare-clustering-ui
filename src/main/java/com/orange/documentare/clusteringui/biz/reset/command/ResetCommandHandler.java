package com.orange.documentare.clusteringui.biz.reset.command;

import static java.util.Collections.emptyList;

import java.util.Collection;

import com.orange.documentare.clusteringui.AggregateAndEvents;
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections;
import com.orange.documentare.clusteringui.infrastructure.io.FileIO;
import com.tophe.ddd.commands.CommandHandler;
import com.tophe.ddd.infrastructure.event.Event;
import com.tophe.ddd.infrastructure.persistence.EventRepository;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ResetCommandHandler extends CommandHandler<ResetCommand, Void> {

  private final EventRepository eventRepository;
  private final Projections projections;

  @Override
  public AggregateAndEvents<Void, Collection<Event>> doExecute(ResetCommand command) {
    FileIO.reset();

    eventRepository.resumeHardPersistenceAndDropVolatilePersistence();
    eventRepository.deleteAll();
    if (projections != null) {
      projections.reset();
      projections.init();
    }
    return AggregateAndEvents.withOnlyEvents(emptyList());
  }
}
