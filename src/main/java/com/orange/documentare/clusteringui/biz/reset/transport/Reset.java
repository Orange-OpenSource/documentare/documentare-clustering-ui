package com.orange.documentare.clusteringui.biz.reset.transport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.orange.documentare.clusteringui.biz.reset.command.ResetCommand;
import com.tophe.ddd.commands.CommandBus;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class Reset {

  @Autowired
  CommandBus commandBus;

  @Secured("ROLE_USER")
  @RequestMapping("/reset")
  public String mvcReset() {
    doReset();
    return "redirect:/";
  }

  @Secured("ROLE_USER")
  @ResponseBody
  @RequestMapping(value = "/api/reset", method = RequestMethod.DELETE)
  public void reset() {
    doReset();
  }

  private void doReset() {
    String resetPrefix = "DELETE [/reset]";
    commandBus.dispatch(new ResetCommand())
      .onSuccess(res -> log.info(resetPrefix))
      .onFailure(throwable -> log.error(resetPrefix + " error: " + throwable));
  }
}
