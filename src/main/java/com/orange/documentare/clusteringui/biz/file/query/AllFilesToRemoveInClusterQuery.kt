package com.orange.documentare.clusteringui.biz.file.query

import com.tophe.ddd.queries.Query

class AllFilesToRemoveInClusterQuery(val fileIds: List<String>) : Query