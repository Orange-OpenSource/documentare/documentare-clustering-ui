package com.orange.documentare.clusteringui.biz.remoteservices.distance.domain

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile

class DistancesComputationInProgress {
  private val fileIds: MutableList<String> = mutableListOf()

  @Synchronized
  operator fun contains(fileId: String): Boolean {
    return fileIds.contains(fileId)
  }

  @Synchronized
  fun notEmpty() = fileIds.isNotEmpty()

  @Synchronized
  fun add(file: DomainFile) {
    fileIds.add(file.aggregateId)
  }

  @Synchronized
  fun remove(file: DomainFile) {
    val removeItems = fileIds.filter { it == file.aggregateId }
    fileIds.removeAll(removeItems)
  }

  fun size() = fileIds.size
}
