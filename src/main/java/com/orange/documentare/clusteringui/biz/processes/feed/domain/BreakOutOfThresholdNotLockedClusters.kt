package com.orange.documentare.clusteringui.biz.processes.feed.domain

import com.orange.documentare.clusteringui.biz.file.command.RemoveSelectedFilesFromClustersCommand
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.domain.Room
import com.orange.documentare.clusteringui.biz.file.query.AllClustersQuery
import com.orange.documentare.clusteringui.biz.file.query.SelectedFilesQuery
import com.tophe.ddd.commands.CommandBus
import com.tophe.ddd.commands.CommandResponse
import com.tophe.ddd.queries.QueryBus
import com.tophe.ddd.queries.QueryResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory

open class BreakOutOfThresholdNotLockedClusters(
  private val commandBus: CommandBus,
  private val queryBus: QueryBus
) {

  open fun breakIfNumberOfMultisetIsGreaterThanMultisetThreshold(multisetThreshold: Int): Int {
    var brokeClustersCount = 0
    queryAllClusters()
      .onFailure { log.error("Failed to retrieve all clusters", it) }
      .onSuccess { allClusters ->
        allClusters.forEach { (key, filesIdsInCluster) ->
          val clusterWasBreak = breakIfNumberOfMultisetIsGreaterThanMultisetThreshold(key, filesIdsInCluster, multisetThreshold)
          brokeClustersCount += if (clusterWasBreak) 1 else 0
        }
      }
    return brokeClustersCount
  }

  private fun breakIfNumberOfMultisetIsGreaterThanMultisetThreshold(key: String, filesIdsInCluster: List<String>, multisetThreshold: Int): Boolean {
    var clusterWasBreak = false
    querySelectedFiles(filesIdsInCluster)
      .onFailure { log.error("Failed to retrieve selected files '$filesIdsInCluster'", it) }
      .onSuccess { filesInCluster ->
        val multisetSize = filesInCluster.count { it.inMultiset() }
        val clusterIsLocked = filesInCluster.any { it.locked() }

        if (multisetSize > multisetThreshold && !clusterIsLocked) {
          log.info("\t[Delete] break cluster since number of multiset is greater than MultisetThreshold  '$multisetThreshold, clusterId '$key', files '$filesIdsInCluster'")
          val clusterModified = mapOf(Pair(key, filesInCluster))
          val fileIdsInCluster = filesInCluster.map { file -> file.aggregateId }
          removeSelectedFilesFromCluster(fileIdsInCluster, clusterModified, key)
          clusterWasBreak = true
        }
      }
    return clusterWasBreak
  }

  private fun queryAllClusters() = queryBus.dispatch<QueryResponse<Map<String, List<String>>>>(AllClustersQuery())

  private fun querySelectedFiles(filesIdsInCluster: List<String>) =
    queryBus.dispatch<QueryResponse<List<DomainFile>>>(SelectedFilesQuery(filesIdsInCluster))

  private fun removeSelectedFilesFromCluster(fileIdsInCluster: List<String>, clusterModified: Map<String, List<DomainFile>>, key: String) {
    (commandBus.dispatch<CommandResponse<Unit>>(RemoveSelectedFilesFromClustersCommand(fileIdsInCluster, clusterModified, Room.WAITING_ROOM)))
      .onFailure { throwable -> log.error("Failed to delete cluster '$key' with files '$fileIdsInCluster'", throwable) }
  }

  companion object {
    private val log: Logger = LoggerFactory.getLogger(BreakOutOfThresholdNotLockedClusters::class.java)
  }
}
