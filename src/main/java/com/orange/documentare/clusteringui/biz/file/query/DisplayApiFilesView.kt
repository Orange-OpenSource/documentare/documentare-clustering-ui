package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile

data class DisplayApiFilesView(
  val inWaitingRoom: List<DomainFile>,
  val notInClusters: List<DomainFile>,
  @JvmField
  val mapClassViews: Map<String, Map<String, List<DomainFile>>>
)
