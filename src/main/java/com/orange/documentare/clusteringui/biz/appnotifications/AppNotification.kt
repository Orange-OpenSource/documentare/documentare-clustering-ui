package com.orange.documentare.clusteringui.biz.appnotifications

enum class AppNotification {
  COMPUTE_DISTANCES_TO_CLUSTERS_DONE,
  GLOBAL_OR_PARTIAL_CLUSTERING_DONE,
  RECALCULATE_CLUSTERS_DONE,
  CREATE_CLUSTER_DONE
}
