package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.persistence.EventRepository


class TagClusterNotModifiedCommandHandler(private val eventRepository: EventRepository, eventBus: EventBus) : CommandHandler<TagClusterNotModifiedCommand, Void>(eventBus) {

  override fun doExecute(command: TagClusterNotModifiedCommand): AggregateAndEvents<Void?, Collection<Event>> {
    val event = loadFile(command.clusterCenterId)?.tagClusterNotModified()
    return AggregateAndEvents.withOnlyEvents(event?.let { listOf(event) } ?: emptyList())
  }

  private fun loadFile(fileId: String): DomainFile? = eventRepository.load(fileId)
}