package com.orange.documentare.clusteringui.biz.parameters.query

import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters
import com.orange.documentare.clusteringui.biz.parameters.persistence.ParametersRepository
import com.tophe.ddd.queries.QueryHandler

class ParametersQueryHandler(private val parametersRepository: ParametersRepository) : QueryHandler<ParametersQuery, Parameters?>() {

  override fun doExecute(query: ParametersQuery): Parameters? {
    val parameters = parametersRepository.findAll().toList()
    return if (parameters.isEmpty()) null else parameters.first()
  }
}