package com.orange.documentare.clusteringui.biz.file.query

class DisplayNamingFilesView(
  @JvmField val noClusters: Boolean,
  @JvmField val sortedMapClusterByListSize: Map<String, ClusterUIView>,
  @JvmField val classNameArrays: Array<String>
)
