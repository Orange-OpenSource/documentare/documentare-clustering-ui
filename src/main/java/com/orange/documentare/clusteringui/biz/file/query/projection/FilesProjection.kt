package com.orange.documentare.clusteringui.biz.file.query.projection

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.event.MultisetElementMovedToNextHierarchyLevel
import com.tophe.ddd.infrastructure.event.Event

class FilesProjection {

  data class FilesHierarchyLevel(private val filesMap: MutableMap<String, DomainFile> = mutableMapOf()) {
    fun filesCount() = filesMap.size
    fun ids() = filesMap.keys.toList()

    fun getOrDefault(aggregateId: String, defaultValue: DomainFile) = filesMap.getOrDefault(aggregateId, defaultValue)
    operator fun get(id: String) = filesMap[id]
    operator fun set(id: String, value: DomainFile) {
      filesMap[id] = value
    }
  }

  private class HierarchyLevels {

    private fun emptyOneHierarchyLevel(): MutableList<FilesHierarchyLevel> = mutableListOf(FilesHierarchyLevel())

    private val hierarchyLevels: MutableList<FilesHierarchyLevel> = emptyOneHierarchyLevel()

    fun get() = hierarchyLevels.last()
    fun hierarchyLevelsCount() = hierarchyLevels.size
    fun createNextHierarchyLevel() {
      hierarchyLevels.add(FilesHierarchyLevel())
    }

    operator fun get(hierarchyLevel: Int) = hierarchyLevels[hierarchyLevel]

    fun nextHierarchyLevelIndex() = currentHierarchyLevelIndex() + 1

    fun currentHierarchyLevelIndex() = hierarchyLevelsCount() - 1

    fun reset() {
      hierarchyLevels.clear()
      hierarchyLevels.addAll(emptyOneHierarchyLevel())
    }

    fun keepOnlyFirstHierarchyLevel() {
      val firstLevel = get(0)
      hierarchyLevels.clear()
      hierarchyLevels.add(firstLevel)
    }
  }

  private val hierarchyLevels = HierarchyLevels()

  @Synchronized
  fun filesCount(): Int = currentHierarchyLevel().filesCount()

  @Synchronized
  fun ids(): List<String> = currentHierarchyLevel().ids()

  @Synchronized
  operator fun get(id: String, hierarchyLevelIndex: Int? = null): DomainFile {
    val levelIndex = hierarchyLevelIndex ?: currentHierarchyLevelIndex()
    return hierarchyLevels[levelIndex][id] ?: throw IllegalStateException("file id not found: $id")
  }

  @Synchronized
  fun reset() {
    hierarchyLevels.reset()
  }

  @Synchronized
  fun keepOnlyFirstHierarchyLevel() {
    hierarchyLevels.keepOnlyFirstHierarchyLevel()
  }

  @Synchronized
  fun initFromHistory(events: Iterable<Event>) = events.forEach { onEvent(it) }

  @Synchronized
  fun onEvent(event: Event) {
    if (event is MultisetElementMovedToNextHierarchyLevel) {
      createHierarchyLevelIfNecessary(event)
      copyFileToNewHierarchyLevel(event)
    }

    var file = currentHierarchyLevel().getOrDefault(event.aggregateId, DomainFile.builder().build())
    file = file.apply(event)
    currentHierarchyLevel()[file.aggregateId] = file
  }

  private fun copyFileToNewHierarchyLevel(event: MultisetElementMovedToNextHierarchyLevel) {
    val file: DomainFile = hierarchyLevels[event.nextHierarchyLevel - 1][event.aggregateId]!!
    currentHierarchyLevel()[file.aggregateId] = file
  }


  private fun createHierarchyLevelIfNecessary(event: MultisetElementMovedToNextHierarchyLevel) {
    if (currentHierarchyLevelIndex() < event.nextHierarchyLevel) {
      hierarchyLevels.createNextHierarchyLevel()
    }
  }

  fun currentHierarchyLevel(): FilesHierarchyLevel = hierarchyLevels.get()

  fun nextHierarchyLevel(): Int = hierarchyLevels.nextHierarchyLevelIndex()

  fun hierarchyLevelCount(): Int = hierarchyLevels.hierarchyLevelsCount()

  fun currentHierarchyLevelIndex(): Int = hierarchyLevels.currentHierarchyLevelIndex()

  fun hierarchyLevel(hierarchyLevelIndex: Int): FilesHierarchyLevel = hierarchyLevels[hierarchyLevelIndex]
}
