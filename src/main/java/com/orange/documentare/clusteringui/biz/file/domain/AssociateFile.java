package com.orange.documentare.clusteringui.biz.file.domain;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AssociateFile {

  public final String idFileToSort;
  public final String idClusterCenter;
}
