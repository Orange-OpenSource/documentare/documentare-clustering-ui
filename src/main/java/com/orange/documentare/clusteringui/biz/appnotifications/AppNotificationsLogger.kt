package com.orange.documentare.clusteringui.biz.appnotifications

import jakarta.annotation.PostConstruct
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class AppNotificationsLogger(private val appNotifications: AppNotifications) {
  private var logger: (message: String) -> Unit = { log.info(it) }

  @PostConstruct
  fun registerToListenAppNotificationsAtStartup() {
    appNotifications.register { onNewAppNotification(it) }
  }

  private fun onNewAppNotification(appNotification: AppNotification) {
    logger("[App Notification] $appNotification")
  }

  fun testPurposeOverrideLogger(testLogger: (message: String) -> Unit) {
    logger = testLogger
  }

  companion object {
    private val log = LoggerFactory.getLogger(AppNotificationsLogger::class.java)
  }
}
