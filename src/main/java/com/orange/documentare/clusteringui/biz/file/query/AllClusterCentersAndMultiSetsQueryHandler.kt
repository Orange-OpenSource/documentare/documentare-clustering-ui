package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.query.AllClusterCentersAndMultiSetsQuery.AllClusterCentersAndMultiSets
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler

class AllClusterCentersAndMultiSetsQueryHandler(private val projections: Projections) : QueryHandler<AllClusterCentersAndMultiSetsQuery, AllClusterCentersAndMultiSets>() {
  override fun doExecute(query: AllClusterCentersAndMultiSetsQuery): AllClusterCentersAndMultiSets {
    val clusterCentersAndMultiSetsIds = projections.files.ids()
      .map { projections.files[it] }
      .filter { it.inMultiset() }
      .map { it.aggregateId }
    return AllClusterCentersAndMultiSets(clusterCentersAndMultiSetsIds, projections.files.currentHierarchyLevelIndex())
  }
}
