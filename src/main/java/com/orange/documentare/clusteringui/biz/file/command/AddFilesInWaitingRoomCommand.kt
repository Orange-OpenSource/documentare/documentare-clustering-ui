package com.orange.documentare.clusteringui.biz.file.command

import com.tophe.ddd.commands.Command

data class AddFilesInWaitingRoomCommand(val files: List<File>) : Command {
  data class File(val filename: String, val bytes: ByteArray) {
    override fun equals(other: Any?): Boolean {
      if (this === other) return true
      if (javaClass != other?.javaClass) return false

      other as File

      if (filename != other.filename) return false
      if (!bytes.contentEquals(other.bytes)) return false

      return true
    }

    override fun hashCode(): Int {
      var result = filename.hashCode()
      result = 31 * result + bytes.contentHashCode()
      return result
    }
  }
}
