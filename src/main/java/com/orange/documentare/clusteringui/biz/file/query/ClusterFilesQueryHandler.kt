package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler

class ClusterFilesQueryHandler(private val projections: Projections) : QueryHandler<ClusterFilesQuery, List<DomainFile>>() {

  override fun doExecute(query: ClusterFilesQuery): List<DomainFile> {
    val clusterFiles = mutableListOf<DomainFile>()
    val hierarchyLevel = query.hierarchyLevel ?: projections.files.currentHierarchyLevelIndex()
    val list: MutableList<String> = projections.clusters.hierarchyLevel(hierarchyLevel).clusters()[query.clusterId] ?: error("cluster ${query.clusterId} was expected")
    if (list.isNotEmpty()) {
      clusterFiles.addAll(list.map { id: String -> projections.files.hierarchyLevel(hierarchyLevel)[id] ?: error("file $id was expected at level $hierarchyLevel") })
    }
    return clusterFiles
  }
}
