package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class DistanceToCenterStatsForAllFilesInClusterQueryHandler(private val projections: Projections) :
  QueryHandler<DistanceToCenterStatsForAllFilesInClusterQuery, ClusterDistancesStats>() {
  override fun doExecute(query: DistanceToCenterStatsForAllFilesInClusterQuery): ClusterDistancesStats {
    val clusterId = query.clusterId
    val hierarchyLevelIndex = query.hierarchyLevelIndex ?: projections.files.currentHierarchyLevelIndex()
    val hierarchyLevelClusters = projections.clusters.hierarchyLevel(hierarchyLevelIndex).clusters()

    if (hierarchyLevelClusters.isEmpty()) {
      val errorMessage = "\n[DistanceToCenterStatsForAllFilesInClusterQuery] ❌ Hierarchy level (level $hierarchyLevelIndex) has no clusters"
      log.error(errorMessage)
      throw IllegalStateException(errorMessage)
    }

    val filesInCluster: List<String>? = hierarchyLevelClusters[clusterId]
    if (filesInCluster == null) {
      val errorMessage = "\n[DistanceToCenterStatsForAllFilesInClusterQuery] ❌ cluster '$clusterId' was expected\n"
      log.error(errorMessage)
      throw IllegalStateException(errorMessage)
    }

    val allFilesInCluster = filesInCluster.map { projections.files[it, hierarchyLevelIndex] }
    return ClusterDistancesStats.buildWithDomainFiles(allFilesInCluster)
  }

  companion object {
    private val log: Logger = LoggerFactory.getLogger(DistanceToCenterStatsForAllFilesInClusterQueryHandler::class.java)
  }
}
