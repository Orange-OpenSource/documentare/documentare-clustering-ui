package com.orange.documentare.clusteringui.biz.processes.feed.domain.events

data class MultisetClustersBroke(val brokeClusters: Int, val multiSetThreshold: Int, val maxMultisetOnAllClustersNotLocked: Int) : FeedLoopEvent() {
  override fun displayInternalHtml() =
    if (brokeClusters == 0) {
      "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; no clusters broke during multiset balancing, with current multiset threshold = <strong>$multiSetThreshold</strong>, biggest multiset had size = $maxMultisetOnAllClustersNotLocked"
    } else {
      "&nbsp;🔨&nbsp;&nbsp; <strong>$brokeClusters</strong> clusters broke during multiset balancing, with current multiset threshold = <strong>$multiSetThreshold</strong>, biggest multiset had size = $maxMultisetOnAllClustersNotLocked"
    }

  override fun displayInternalRaw() =
    if (brokeClusters == 0) {
      "      no clusters broke during multiset balancing, with current multiset threshold = $multiSetThreshold, biggest multiset had size = $maxMultisetOnAllClustersNotLocked"
    } else {
      " 🔨   $brokeClusters clusters broke during multiset balancing, with current multiset threshold = $multiSetThreshold, biggest multiset had size = $maxMultisetOnAllClustersNotLocked"
    }
}
