package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederServiceImpl
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.ClusteringServiceImpl
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceService
import com.tophe.ddd.queries.QueryHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory

data class DisplayHomeFilesQueryHandler(
  private val distanceService: DistanceService,
  private val clusteringService: ClusteringServiceImpl,
  private val feederService: FeederServiceImpl,
  private val projections: Projections
) : QueryHandler<DisplayHomeFilesQuery, DisplayHomeFilesView>() {

  public override fun doExecute(query: DisplayHomeFilesQuery): DisplayHomeFilesView {
    val currentHierarchyLevel = projections.files.currentHierarchyLevelIndex()
    val filesHierarchyLevel = projections.files.currentHierarchyLevel()
    val clustersHierarchyLevel = projections.clusters.currentHierarchyLevel()
    val transform = DisplayHierarchyLevelFileTransform(filesHierarchyLevel, clustersHierarchyLevel) { toFileView(it) }

    val t0 = System.currentTimeMillis()
    val notDeleted = projections.files.ids().map { fileId -> projections.files[fileId] }
      .filter { file -> !file.deleted() }
      .map { file -> toFileView(file) }

    val inWaitingRoom = transform.filesInWaitingRoom()
    val notInClusters = transform.filesNotInClusters()
    val mapClassViews = transform.mapClasses()
    val sortedMap = mapClassViews.toSortedMap()

    val viewES = DisplayHomeFilesView(
      currentHierarchyLevel,
      projections.files.filesCount() == 0, notDeleted, inWaitingRoom, notInClusters, sortedMap,
      distanceService.isRunning(), clusteringService.isRunning(), feederService.isRunning, feederService.stopFeederWhenPossible
    )


    val dtMs = System.currentTimeMillis() - t0
    if (dtMs > 10) {
      log.info("ES VIEW LOAD = $dtMs")
    }

    return viewES
  }

  private fun toFileView(file: DomainFile): FileUIView {
    return FileUIView.from(file).withDistanceComputationInProgress(isDistanceComputationInProgress(file))
  }

  private fun isDistanceComputationInProgress(file: DomainFile): Boolean {
    return distanceService.isDistanceBeingComputedFor(file.aggregateId)
  }

  companion object {
    private val log: Logger = LoggerFactory.getLogger(DisplayHomeFilesQueryHandler::class.java)
  }
}
