package com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

import java.io.IOException;
import java.util.List;

import org.jetbrains.annotations.NotNull;

import com.orange.documentare.clusteringui.EmbeddedComputationServer;
import com.orange.documentare.computationserver.ComputationServerResponse;
import com.orange.documentare.core.computationserver.biz.task.TaskController;
import com.orange.documentare.core.model.ref.clustering.infra.network.dto.ClusteringDTO;
import com.orange.documentare.tasksdispatcher.ApiWrapper;
import com.orange.documentare.tasksdispatcher.RemoteTaskDTO;

import kotlin.Pair;
import retrofit2.Response;

public class ClusteringRequestWrapper extends ApiWrapper<ClusteringRequestWrapperNetworkApi, ClusteringDTO> {

  public final ClusteringRequestUI clusteringRequestUI;
  private final EmbeddedComputationServer embeddedComputationServer;

  ClusteringRequestWrapper(ClusteringRequestUI clusteringRequestUI, EmbeddedComputationServer embeddedComputationServer) {
    this.clusteringRequestUI = clusteringRequestUI;
    this.embeddedComputationServer = embeddedComputationServer;
  }

  @Override
  public ComputationServerResponse<RemoteTaskDTO> callComputationApi(String url) {
    if (embeddedComputationServer.getEnabled()) {
      return embeddedCallComputationServer();
    }
    else
      return remoteCallComputationApi(url);
  }

  @Override
  public ComputationServerResponse<ClusteringDTO> callTaskApi(String taskId, String url) {
    if (embeddedComputationServer.getEnabled())
      return embeddedCallTasks(taskId);
    else
      return remoteCallTaskApi(taskId, url);
  }

  @Override
  public String requestId() {
    List<String> ids = clusteringRequestUI.bytesData[0].ids;
    StringBuilder requestId = new StringBuilder(ids.get(0));
    for (int i = 1; i < ids.size(); i++) {
      requestId.append("_").append(ids.get(i));
    }
    return requestId.toString();
  }

  @NotNull
  private ComputationServerResponse<RemoteTaskDTO> remoteCallComputationApi(String url) {
    Response<RemoteTaskDTO> remoteTaskDTOCall;
    try {
      remoteTaskDTOCall = api(url).computationApi(clusteringRequestUI).execute();
      return ComputationServerResponse.with(remoteTaskDTOCall.body(), remoteTaskDTOCall.code());
    }
    catch (IOException e) {
      return ComputationServerResponse.hasThrown(e);
    }
  }

  @NotNull
  private ComputationServerResponse<ClusteringDTO> remoteCallTaskApi(String taskId, String url) {
    Response<ClusteringDTO> clusteringDTOResponse;
    try {
      clusteringDTOResponse = api(url).taskApi(taskId).execute();
      return ComputationServerResponse.with(clusteringDTOResponse.body(), clusteringDTOResponse.code());
    }
    catch (IOException e) {
      return ComputationServerResponse.hasThrown(e);
    }
  }

  private ClusteringRequestWrapperNetworkApi api(String url) {
    return buildNetworkApi(ClusteringRequestWrapperNetworkApi.class, url);
  }

  private ComputationServerResponse<RemoteTaskDTO> embeddedCallComputationServer() {
    ComputationServerResponse<com.orange.documentare.core.computationserver.biz.RemoteTaskDTO> remoteTaskDTOComputationServerResponse =
      embeddedComputationServer.getClusteringApi().computeClustering(clusteringRequestUI.toComputationServerClusteringRequest());
    int code = remoteTaskDTOComputationServerResponse.code();
    if (remoteTaskDTOComputationServerResponse.isSuccessful()) {
      com.orange.documentare.core.computationserver.biz.RemoteTaskDTO dto = remoteTaskDTOComputationServerResponse.dto();
      return ComputationServerResponse.with(new RemoteTaskDTO(dto.id), code);
    }
    else {
      return ComputationServerResponse.error(code, remoteTaskDTOComputationServerResponse.errorMessage());
    }
  }

  private ComputationServerResponse<ClusteringDTO> embeddedCallTasks(String taskId) {
    Pair<TaskController.TaskState, Object> taskResult = embeddedComputationServer.getTaskApiClient().getTaskResult(taskId);
    TaskController.TaskState taskState = taskResult.getFirst();
    if (taskState == TaskController.TaskState.DONE) {
      ClusteringDTO clusteringRequestResultDTO = (ClusteringDTO) taskResult.component2();
      return ComputationServerResponse.with(clusteringRequestResultDTO, 200);
    }
    else {
      // TODO: error cases not tested
      return ComputationServerResponse.error(418, "Internal error, try to retrieve task which is not finished, current state is $taskState");
    }
  }

}
