package com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure

import com.orange.documentare.core.model.ref.clustering.infra.network.dto.ClusteringDTO
import com.orange.documentare.tasksdispatcher.RemoteTaskDTO
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ClusteringRequestWrapperNetworkApi {
  @POST("/clustering")
  fun computationApi(@Body clusteringRequestUI: ClusteringRequestUI): Call<RemoteTaskDTO>

  @GET("/task/{taskId}")
  fun taskApi(@Path("taskId") taskId: String): Call<ClusteringDTO>
}
