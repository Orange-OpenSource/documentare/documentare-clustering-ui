package com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure;
/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */


import java.util.List;

import com.orange.documentare.clusteringui.biz.remoteservices.infrastructure.ResponseCollector;
import com.orange.documentare.tasksdispatcher.RemoteTaskDTO;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import feign.Response;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ResponseCollectorImpl implements ResponseCollector<ClusteringRequestUI> {

  @Override
  public void add(ClusteringRequestUI clusteringRequestUI) {
  }

  @Override
  public List<ClusteringRequestUI> responses() {
    return null;
  }

  @Override
  public boolean allResponsesCollected() {
    return false;
  }

  interface Clustering {

    @RequestLine("POST /clustering")
    @Headers("Content-Type: application/json")
    RemoteTaskDTO clustering(ClusteringRequestUI clusteringRequestUI);
  }

  interface ClusteringResult {

    @RequestLine("GET /task/{taskId}")
    Response clusteringResult(@Param("taskId") String taskId);
  }
}
