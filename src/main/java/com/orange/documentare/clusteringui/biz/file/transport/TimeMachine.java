package com.orange.documentare.clusteringui.biz.file.transport;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.orange.documentare.clusteringui.biz.file.query.DisplayTimeMachineFilesQuery;
import com.orange.documentare.clusteringui.biz.file.query.DisplayTimeMachineFilesView;
import com.orange.documentare.clusteringui.infrastructure.security.Authent;
import com.tophe.ddd.queries.QueryBus;
import com.tophe.ddd.queries.QueryResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class TimeMachine {

  @Autowired
  QueryBus queryBus;


  @RequestMapping(value = "/timeMachine", method = RequestMethod.POST)
  public String timeMachine(@ModelAttribute commandTM commandTM, Principal principal, Model model) {
    log.info("POST [/timeMachine]");

    model.addAttribute("title", "Time Machine");

    Authent.addAuthenticatedModel(model, principal);

    QueryResponse<DisplayTimeMachineFilesView> response = queryBus.dispatch(new DisplayTimeMachineFilesQuery(commandTM.getDT()));
    response
      .onSuccess(filesView -> fillModel(filesView, model))
      .onFailure(throwable ->
        log.error("POST [/timeMachine] error: " + throwable.getLocalizedMessage()));

    return "time-machine";
  }

  private void fillModel(DisplayTimeMachineFilesView filesView, Model model) {
    model.addAttribute("nofiles", filesView.noFiles);
    model.addAttribute("filesDuplicate", filesView.duplicate);
    model.addAttribute("filesInWaitingRoom", filesView.inWaitingRoom);
    model.addAttribute("filesWithoutClusterId", filesView.notInClusters);
    model.addAttribute("mapClassViews", filesView.mapClassViews);
    model.addAttribute("classAlbum", "album text-muted");
  }
}
