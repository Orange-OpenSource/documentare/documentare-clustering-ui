package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler

data class ClassesFilenamesQueryHandler(val projections: Projections) : QueryHandler<ClassesFilenamesQuery, ClassesFilenamesView>() {

  override fun doExecute(command: ClassesFilenamesQuery): ClassesFilenamesView {
    val classes: Map<String, Map<String, ClusterUIView>> = DisplayHierarchyLevelFileTransform.with(projections) { FileUIView.from(it) }.mapClasses()
    val classesFilenames = mutableMapOf<String, List<String>>()
    classes.forEach { (className, clustersMap) ->
      val currentClassFilenames = classesFilenames.getOrDefault(className, emptyList())
      val classFilenames = mutableListOf<String>()
      classFilenames.addAll(currentClassFilenames)
      classFilenames.addAll(clustersMap.values.map { it.fileUIViews }.flatten().map { it.filename })
      classesFilenames[className] = classFilenames
    }
    return ClassesFilenamesView(classesFilenames)
  }
}
