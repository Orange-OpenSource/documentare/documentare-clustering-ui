package com.orange.documentare.clusteringui.biz.file.command

import com.tophe.ddd.commands.Command

data class AddFilesCommand(val files: List<File>) : Command {

  data class File(val filename: String, val bytes: ByteArray, val clusterId: String?, val clusterCenter: Boolean?) {
    constructor(originalFilename: String, bytes: ByteArray) : this(originalFilename, bytes, null, null)

    override fun equals(other: Any?): Boolean {
      if (this === other) return true
      if (javaClass != other?.javaClass) return false

      other as File

      if (filename != other.filename) return false
      if (!bytes.contentEquals(other.bytes)) return false
      if (clusterId != other.clusterId) return false
      if (clusterCenter != other.clusterCenter) return false

      return true
    }

    override fun hashCode(): Int {
      var result = filename.hashCode()
      result = 31 * result + bytes.contentHashCode()
      result = 31 * result + (clusterId?.hashCode() ?: 0)
      result = 31 * result + (clusterCenter?.hashCode() ?: 0)
      return result
    }
  }
}
