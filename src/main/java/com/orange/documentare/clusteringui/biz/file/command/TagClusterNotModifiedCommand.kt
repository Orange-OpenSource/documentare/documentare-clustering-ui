package com.orange.documentare.clusteringui.biz.file.command

import com.tophe.ddd.commands.Command

data class TagClusterNotModifiedCommand(val clusterCenterId: String) : Command
