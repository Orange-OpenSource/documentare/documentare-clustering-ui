package com.orange.documentare.clusteringui.biz.file.command

import com.tophe.ddd.commands.Command

data class MoveFileFromWaitingRoomToSortRoomCommand(val filesIds: List<String>) : Command
