package com.orange.documentare.clusteringui.biz.processes.feed.domain.events

data class GlobalOrPartialClusteringComputed(val filesCount: Int) : FeedLoopEvent() {
  override fun displayInternalHtml() = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; clustering done on ${filesCount} files"
  override fun displayInternalRaw() = "      clustering done on ${filesCount} files"
}
