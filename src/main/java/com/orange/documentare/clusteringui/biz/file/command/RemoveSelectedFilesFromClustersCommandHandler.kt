package com.orange.documentare.clusteringui.biz.file.command

import com.orange.documentare.clusteringui.AggregateAndEvents
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.tophe.ddd.commands.CommandHandler
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.event.EventBus
import com.tophe.ddd.infrastructure.persistence.EventRepository


class RemoveSelectedFilesFromClustersCommandHandler(private val eventRepository: EventRepository, eventBus: EventBus) :
  CommandHandler<RemoveSelectedFilesFromClustersCommand, Void>(eventBus) {

  override fun doExecute(command: RemoveSelectedFilesFromClustersCommand): AggregateAndEvents<Void?, Collection<Event>> {
    val files = loadFiles(command)

    val destroyedClustersAndUpdatedFiles = DomainFile.removeFilesFromClusters(files, command.clusters, command.fileInRoom)

    val modifiedClusterCenters = files.flatMap { command.clusters[it.clusterId]!! }
      .filter { it.clusterCenter() }
      .filter { !destroyedClustersAndUpdatedFiles.destroyedClusters.contains(it.clusterId) }
      .map { it.tagClusterModified() }

    val events = destroyedClustersAndUpdatedFiles.events.toMutableList()
    events.addAll(modifiedClusterCenters)

    return AggregateAndEvents.withOnlyEvents(events)
  }

  private fun loadFiles(command: RemoveSelectedFilesFromClustersCommand) =
    eventRepository.load(command.filesIds).filter { it.clusterId != null }
}
