package com.orange.documentare.clusteringui.biz.file.hierarchyvisu

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.HierarchiesView
import com.orange.documentare.clusteringui.infrastructure.io.FileIO

private interface Node {
  fun keepOnlyChildren() {
    text = null
    image = null
    link = null
  }

  var text: TreantText?
  var children: TreantChildren?
  var image: String?
  var link: TreantLink?
}

data class TreantText(val name: String)
data class TreantLink(val href: String)
data class TreantNode(override var text: TreantText?, override var children: TreantChildren? = null, override var image: String? = null, override var link: TreantLink? = null) :
  Node

typealias TreantChildren = List<TreantNode>

data class TreantHierarchyDTO(
  override var text: TreantText?,
  override var children: TreantChildren? = null,
  override var image: String? = null,
  override var link: TreantLink? = null
) : Node {
  companion object {
    fun from(hierarchiesView: HierarchiesView): TreantHierarchyDTO {
      val treantModel = mapHierarchyToTreantModel(hierarchiesView)
      removeParentNodeFromLeafIfNotASingleton(treantModel)
      clearParentFromChildren(treantModel, treantModel.text!!.name)
      clearEmptyBranches(treantModel)
      return treantModel
    }

    private fun removeParentNodeFromLeafIfNotASingleton(node: Node) {
      assert(node.children != null)
      node.children?.let { children ->
        val childrenAreALeaf = children.first().children == null
        if (childrenAreALeaf) {
          if (children.size > 1) {
            node.children = children.filter { child ->
              child.text!!.name != node.text!!.name
            }
          }
        } else {
          children.forEach { child -> removeParentNodeFromLeafIfNotASingleton(child) }
        }
      }
    }

    private fun clearParentFromChildren(node: Node, lastBranchesSeparationRoot: String) {
      node.children?.let { children ->
        val nodeIsABranchesSeparationRoot = children.size > 1
        val updatedLastBranchesSeparationRoot = if (nodeIsABranchesSeparationRoot) node.text!!.name else lastBranchesSeparationRoot
        children
          .forEach { child ->
            run {
              val childName = child.text!!.name
              val childIsParent = childName == node.text!!.name
              clearParentFromChildren(child, updatedLastBranchesSeparationRoot)
              if (childIsParent) {
                if (nodeIsABranchesSeparationRoot) {
                  child.keepOnlyChildren()
                } else {
                  node.keepOnlyChildren()
                  if (updatedLastBranchesSeparationRoot == childName) {
                    child.keepOnlyChildren()
                  }
                }
              }
            }
          }
      }
    }

    private fun clearEmptyBranches(node: Node): Boolean {
      if (node.children == null && node.text == null) {
        return true
      }

      var clear = true
      var updatedChildren = mutableListOf<Node>()
      node.children?.let { children ->
        children.forEach { child ->
          if (!clearEmptyBranches(child) || child.text != null) {
            updatedChildren.add(child)
            clear = false
          }
        }
      }
      node.children = updatedChildren as TreantChildren
      return clear
    }

    private fun mapHierarchyToTreantModel(hierarchiesView: HierarchiesView): TreantHierarchyDTO {
      val hierarchyLevel = hierarchiesView.topHierarchyLevel()

      val topNodes: List<DomainFile> = hierarchiesView.topNodes()

      val children = topNodes.map {
        TreantNode(
          TreantText(it.filename),
          retrieveChildren(it, hierarchiesView, hierarchyLevel - 1),
          retrieveThumbnailIfItExists(it),
          TreantLink(FileIO.fileWebPathOf(it))
        )
      }
      return TreantHierarchyDTO(TreantText("Root"), children)
    }

    private fun retrieveChildren(parentNode: DomainFile, hierarchies: HierarchiesView, hierarchyLevel: Int): TreantChildren {
      val children = mutableListOf<TreantNode>()
      val topHierarchyLevel = hierarchies.level(hierarchyLevel)
      val lowerHierarchy = hierarchyLevel == 0

      val childrenFiles: List<DomainFile> = topHierarchyLevel.values.first { domainFiles -> domainFiles.map { it.aggregateId }.contains(parentNode.aggregateId) };
      children.addAll(childrenFiles.map {
        val lowerHierarchyChildren = if (lowerHierarchy) null else retrieveChildren(it, hierarchies, hierarchyLevel - 1)
        TreantNode(TreantText(it.filename), lowerHierarchyChildren, retrieveThumbnailIfItExists(it), TreantLink(FileIO.fileWebPathOf(it)))
      })

      return children
    }

    private fun retrieveThumbnailIfItExists(it: DomainFile): String? {
      return if (FileIO.hasThumbnail(it)) {
        FileIO.thumbnailWebPathOf(it)
      } else {
        null
      }
    }
  }
}
