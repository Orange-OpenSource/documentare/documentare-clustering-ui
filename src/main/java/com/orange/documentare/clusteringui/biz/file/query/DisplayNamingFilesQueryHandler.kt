package com.orange.documentare.clusteringui.biz.file.query

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.query.DisplayHierarchyLevelFileTransform.Companion.orderClustersWithFileViewsBySize
import com.orange.documentare.clusteringui.biz.file.query.DisplayHierarchyLevelFileTransform.Companion.orderFilesInClusterByBizCriteria
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections
import com.tophe.ddd.queries.QueryHandler

class DisplayNamingFilesQueryHandler(private val projections: Projections) : QueryHandler<DisplayNamingFilesQuery, DisplayNamingFilesView>() {

  public override fun doExecute(query: DisplayNamingFilesQuery): DisplayNamingFilesView {
    val clusterIDsLockedAndNotInClass = projections.files.ids()
      .filter { id -> projections.files[id].clusterCenter() }
      .filter { id -> projections.files[id].locked() }
      .filter { id -> !projections.files[id].hasClassName() }
      .map { id -> projections.files[id].clusterId }

    val clustersLockedAndNotInClass = projections.clusters.clusters()
      .filter { clusterIDsLockedAndNotInClass.contains(it.key) }

    val filesOrderedInCluster = orderFilesInClusterByBizCriteria(clustersLockedAndNotInClass, projections.files.currentHierarchyLevel())

    val fileViewsOrderedInCluster: Map<String, ClusterUIView> = filesOrderedInCluster
      .map { Pair(it.key, ClusterUIView(it.key, it.value.map { file -> toFileView(file) })) }
      .associate { Pair(it.first, it.second) }

    val clustersOrderedBySize: Map<String, ClusterUIView> = orderClustersWithFileViewsBySize(fileViewsOrderedInCluster)

    val classNameArrays = projections.clusters.classes().keys.toTypedArray()

    return DisplayNamingFilesView(
      clustersOrderedBySize.isEmpty(), clustersOrderedBySize, classNameArrays
    )
  }

  private fun toFileView(file: DomainFile): FileUIView = FileUIView.from(file)
}
