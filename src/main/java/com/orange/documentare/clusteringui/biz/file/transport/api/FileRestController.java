package com.orange.documentare.clusteringui.biz.file.transport.api;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orange.documentare.clusteringui.biz.file.query.*;
import com.tophe.ddd.queries.QueryBus;
import com.tophe.ddd.queries.QueryResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class FileRestController {

  final QueryBus queryBus;

  public FileRestController(QueryBus queryBus) {
    this.queryBus = queryBus;
  }

  @RequestMapping("/api/files")
  public List<ClusterView> files() {
    QueryResponse<DisplayApiFilesView> response = queryBus.dispatch(new DisplayApiFilesQuery());

    List<ClusterView> clustersView = new ArrayList<>();
    response.value().mapClassViews.keySet().forEach(className -> response.value().mapClassViews.get(className).keySet().forEach(clusterId -> {
      ClusterView clusterView = new ClusterView(clusterId, response.value().mapClassViews.get(className).get(clusterId));
      clustersView.add(clusterView);
    }));

    return clustersView;
  }

  @RequestMapping("/api/bin")
  public List<FileUIView> bin() {
    QueryResponse<BinView> response = queryBus.dispatch(new BinQuery());
    return response.value().fileUIViews;
  }

  @RequestMapping("/api/classes-filenames")
  public ClassesFilenamesView classesFilenames() {
    QueryResponse<ClassesFilenamesView> response = queryBus.dispatch(new ClassesFilenamesQuery());
    return response.value();
  }
}
