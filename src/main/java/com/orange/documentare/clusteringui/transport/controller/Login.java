package com.orange.documentare.clusteringui.transport.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import jakarta.servlet.http.HttpSession;

@Controller
public class Login {

  @Value("${git.commit.id.describe}")
  private String commitIdDescribe;


  @RequestMapping("/login")
  public String login(Model model) {
    model.addAttribute("commitIdDescribe", commitIdDescribe);
    return "login";
  }

  @RequestMapping("/logout")
  public String logout(HttpSession session) {
    session.invalidate();
    return "redirect:/";
  }
}
