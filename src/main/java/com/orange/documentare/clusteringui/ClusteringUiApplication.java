package com.orange.documentare.clusteringui;

import static com.orange.documentare.clusteringui.biz.file.command.AddFilesInWaitingRoomCommandHandler.defaultUUIDSupplier;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.CacheControl;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import com.orange.documentare.clientui.biz.domain.ClientRootDir;
import com.orange.documentare.clusteringui.biz.file.command.*;
import com.orange.documentare.clusteringui.biz.file.query.*;
import com.orange.documentare.clusteringui.biz.file.query.projection.Projections;
import com.orange.documentare.clusteringui.biz.parameters.command.UpdateParametersCommand;
import com.orange.documentare.clusteringui.biz.parameters.command.UpdateParametersCommandHandler;
import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters;
import com.orange.documentare.clusteringui.biz.parameters.persistence.ParametersRepository;
import com.orange.documentare.clusteringui.biz.parameters.query.ParametersQueryHandler;
import com.orange.documentare.clusteringui.biz.processes.feed.domain.FeederServiceImpl;
import com.orange.documentare.clusteringui.biz.processes.hierarchy.transport.HierarchyService;
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.domain.ClusteringServiceImpl;
import com.orange.documentare.clusteringui.biz.remoteservices.clustering.infrastructure.ClusteringRemote;
import com.orange.documentare.clusteringui.biz.remoteservices.distance.domain.DistanceServiceImpl;
import com.orange.documentare.clusteringui.biz.remoteservices.distance.infrastructure.DistanceRemote;
import com.orange.documentare.clusteringui.biz.reset.command.ResetCommandHandler;
import com.orange.documentare.clusteringui.biz.services.impl.ClientHierarchyExportImpl;
import com.orange.documentare.clusteringui.infrastructure.io.FileIO;
import com.orange.documentare.clusteringui.infrastructure.persistence.DBEventRepository;
import com.orange.documentare.clusteringui.infrastructure.persistence.postgres.EventDTORepository;
import com.orange.documentare.clusteringui.infrastructure.persistence.postgres.PersistenceMetrologyTests;
import com.orange.documentare.clusteringui.infrastructure.persistence.postgres.PersistenceOperationalTests;
import com.tophe.ddd.commands.CommandBus;
import com.tophe.ddd.infrastructure.event.EventBus;
import com.tophe.ddd.infrastructure.persistence.EventRepository;
import com.tophe.ddd.queries.QueryBus;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication(scanBasePackages = { "com.orange.documentare" })
@EnableScheduling
public class ClusteringUiApplication implements WebMvcConfigurer {

  @Value("${app.file_dir}")
  String filesDirectory;
  @Value("${client.root.dir.path}")
  String clientFilesStorageDirectory;
  @Value("#{'${app.remote-simdoc-servers-urls}'.split(',')}")
  List<String> remoteSimdocServersUrls;
  @Value("${app.smoke-tests}")
  Boolean appSmokeTests;

  @Value("${git.commit.id.describe}")
  private String commitIdDescribe;

  private final Projections projections;
  private final EventRepository eventRepository;
  private final ParametersRepository parametersRepository;
  private final DistanceServiceImpl distanceService;
  private final ClusteringServiceImpl clusteringService;
  private final FeederServiceImpl feederService;
  private final HierarchyService hierarchyService;
  private final ClientHierarchyExportImpl clientHierarchyExport;

  private final EventBus eventBus;

  @Autowired
  public ClusteringUiApplication(
    EventDTORepository postgresEventRepository,
    PersistenceOperationalTests persistenceOperationalTests,
    PersistenceMetrologyTests persistenceMetrologyTests,
    ParametersRepository parametersRepository,
    DistanceServiceImpl distanceService,
    ClusteringServiceImpl clusteringService,
    FeederServiceImpl feederService,
    HierarchyService hierarchyService,
    ClientHierarchyExportImpl clientHierarchyExport,
    EmbeddedComputationServer embeddedComputationServer)
  {
    if (!persistenceOperationalTests.areTestsOk()) {
      throw new IllegalStateException("❌  Persistence operational tests KO...");
    }

    if (!persistenceMetrologyTests.isFinished()) {
      throw new IllegalStateException("❌  Metrology tests should be finished at application start (dependency injection + @PostConstruct mechanism)...");
    }

    this.eventRepository = new DBEventRepository(postgresEventRepository);
    this.parametersRepository = parametersRepository;
    this.distanceService = distanceService;
    this.clusteringService = clusteringService;
    this.feederService = feederService;
    this.hierarchyService = hierarchyService;
    this.clientHierarchyExport = clientHierarchyExport;
    this.projections = new Projections(eventRepository);

    eventBus = initEventBus(eventRepository, projections, feederService);
    hierarchyService.setPersistencePauseAndResume(eventRepository);
    hierarchyService.setFeederService(feederService);
    hierarchyService.setProjections(projections);

    log.info("Embedded computation server is " + (embeddedComputationServer.getEnabled() ? "ENABLED" : "NOT enabled"));
  }


  public static void main(String[] args) {
    SpringApplication.run(ClusteringUiApplication.class, args);
  }

  @Bean
  public CommandBus commandBus() {
    CommandBus commandBus = new CommandBus();

    distanceService.setCommandBus(commandBus);
    clusteringService.setCommandBus(commandBus);
    feederService.setCommandBus(commandBus);
    hierarchyService.setCommandBus(commandBus);
    hierarchyService.setClientHierarchyExport(clientHierarchyExport);
    hierarchyService.setDistanceService(distanceService);

    commandBus.register(
      new AddFilesCommandHandler(eventBus),
      new DeleteFilesCommandHandler(eventRepository, eventBus),
      new DuplicateFilesCommandHandler(eventRepository, eventBus),
      new AddDistancesToClustersCommandHandler(eventRepository, eventBus),
      new AddToClusterCommandHandler(eventRepository, eventBus),
      new ResetCommandHandler(eventRepository, projections),
      new UpdateParametersCommandHandler(parametersRepository),
      new BinPurgeCommandHandler(eventRepository, eventBus),
      new RemoveSelectedFilesFromClustersCommandHandler(eventRepository, eventBus),
      new AddFilesToClustersCommandHandler(eventRepository, eventBus),
      new AutoAddFileToClosestClusterCommandHandler(eventRepository, eventBus),
      new CreateClusterCommandHandler(eventRepository, eventBus),
      new ResetFileInfoCommandHandler(eventRepository, eventBus),
      new AddFilesInWaitingRoomCommandHandler(eventBus, defaultUUIDSupplier()),
      new MoveFileFromWaitingRoomToSortRoomCommandHandler(eventRepository, eventBus),
      new LockClustersCommandHandler(eventRepository, eventBus),
      new UnLockClustersCommandHandler(eventRepository, eventBus),
      new PutClustersInNamedClassCommandHandler(eventRepository, eventBus),
      new PutFileInNamedClassCommandHandler(eventRepository, eventBus),
      new TagClusterNotModifiedCommandHandler(eventRepository, eventBus),
      new MoveMultisetElementToNextHierarchyLevelCommandHandler(eventBus)
    );

    return commandBus;
  }

  private EventBus initEventBus(EventRepository eventRepo, Projections proj, FeederServiceImpl feederServ) {
    EventBus eventBus = new EventBus(eventRepo);
    eventBus.register(proj);
    eventBus.register(feederServ);
    return eventBus;
  }

  @Bean
  public QueryBus queryBus() {
    QueryBus queryBus = new QueryBus();

    distanceService.setQueryBus(queryBus);
    clusteringService.setQueryBus(queryBus);
    feederService.setQueryBus(queryBus);
    hierarchyService.setQueryBus(queryBus);
    clientHierarchyExport.setQueryBus(queryBus);

    queryBus.register(
      new DisplayApiFilesQueryHandler(projections),
      new DisplayHomeFilesQueryHandler(distanceService, clusteringService, feederService, projections),
      new DisplayHierarchyLevelFilesQueryHandler(projections),
      new DisplayNamingFilesQueryHandler(projections),
      new AssociateFileQueryHandler(projections),
      new AllFilesQueryHandler(projections),
      new ParametersQueryHandler(parametersRepository),
      new BinQueryHandler(projections),
      new ComputeDistancesQueryHandler(projections),
      new SelectedFilesQueryHandler(projections),
      new SelectedFilesAtLevel0QueryHandler(projections),
      new DistanceToCenterStatsForAllFilesInClusterQueryHandler(projections),
      new AllFilesInModifiedClustersQueryHandler(projections),
      new AllFilesToSortQueryHandler(projections),
      new AllFilesToRemoveInClusterQueryHandler(projections),
      new ClusterFilesQueryHandler(projections),
      new DisplayTimeMachineFilesQueryHandler(eventRepository),
      new AllFilesInWaitingRoomQueryHandler(projections),
      new AllSingletonsNotLockedQueryHandler(projections),
      new ClassesFilenamesQueryHandler(projections),
      new AllClustersQueryHandler(projections),
      new AllClusterCentersAndMultiSetsQueryHandler(projections),
      new HierarchiesQueryHandler(projections),
      new MultiHypothesisClustersQueryHandler(projections)
    );

    return queryBus;
  }


  @Bean
  public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
    PropertySourcesPlaceholderConfigurer c = new PropertySourcesPlaceholderConfigurer();
    c.setLocation(new ClassPathResource("git.properties"));
    c.setIgnoreResourceNotFound(true);
    c.setIgnoreUnresolvablePlaceholders(true);
    return c;
  }

  @Bean
  public LocaleResolver localeResolver() {
    AcceptHeaderLocaleResolver localeResolver = new AcceptHeaderLocaleResolver();
    localeResolver.setDefaultLocale(Locale.UK);
    return localeResolver;
  }

  @PostConstruct
  public void initFilesDirectory() {
    FileIO.initFilesDirectory(filesDirectory);
  }

  @PostConstruct
  public void initUrlRemoteSimdocServer() {
    ClusteringRemote.Companion.initRemoteSimdocServerForClustering(remoteSimdocServersUrls);
    DistanceRemote.initRemoteSimdocServerForDistance(remoteSimdocServersUrls);
  }

  @PostConstruct
  public void initSmokeTests() {
    if (appSmokeTests) {
      Parameters parameters = Parameters.smokeTests();
      UpdateParametersCommand command = UpdateParametersCommand.initBuilderWith(parameters).build();
      UpdateParametersCommandHandler handler = new UpdateParametersCommandHandler(parametersRepository);
      handler.execute(command)
        .onSuccess(res -> log.info(" parameters updated with smokeTests value: " + command.toString()))
        .onFailure(throwable -> log.error(" error: " + throwable));
    }
  }

  @PostConstruct
  public void release() {
    log.info("Release " + commitIdDescribe);
  }
}
