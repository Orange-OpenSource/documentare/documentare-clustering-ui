package com.orange.documentare.clusteringui

data class AggregateAndEvents<T1, T2>(@JvmField val aggregate: T1, @JvmField val events: T2) {

  companion object {
    @JvmStatic
    fun <T2> withOnlyEvents(t2: T2): AggregateAndEvents<Void?, T2> = AggregateAndEvents(null, t2)
  }
}