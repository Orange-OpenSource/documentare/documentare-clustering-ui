package com.orange.documentare.clusteringui

data class Couple<T1, T2>(@JvmField val first: T1, @JvmField val second: T2)