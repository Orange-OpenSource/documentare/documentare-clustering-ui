package com.orange.documentare.clusteringui;

import java.util.function.Consumer;
import java.util.function.Supplier;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class TryOrCatch<T> {

  private final T value;
  private final Throwable cause;

  private TryOrCatch(Supplier<? extends T> supplier) {
    T val = null;
    Throwable cauz = null;
    try {
      val = supplier.get();
    }
    catch (Throwable t) {
      cauz = t;
    }
    value = val;
    cause = cauz;
  }

  public TryOrCatch(T val) {
    value = val;
    cause = null;
  }

  public TryOrCatch(Throwable throwable) {
    value = null;
    cause = throwable;
  }

  public TryOrCatch(Runnable runnable) {
    value = null;
    Throwable cauz = null;
    try {
      runnable.run();
    }
    catch (Throwable t) {
      cauz = t;
    }
    cause = cauz;
  }

  public static <T> TryOrCatch<T> of(Supplier<? extends T> supplier) {
    return new TryOrCatch<>(supplier);
  }

  public static <V> TryOrCatch<V> success(V val) {
    return new TryOrCatch<>(val);
  }

  public T get() {
    if (isFailure()) {
      sneakyThrow(cause);
    }
    return value;
  }

  static <T extends Throwable, R> R sneakyThrow(Throwable t) throws T {
    throw (T) t;
  }

  public TryOrCatch<T> onFailure(Consumer<Throwable> action) {
    if (isFailure()) {
      action.accept(getCause());
    }
    return this;
  }

  public TryOrCatch<T> onSuccess(Consumer<T> action) {
    if (isSuccess()) {
      action.accept(get());
    }
    return this;
  }

  public Throwable getCause() {
    return cause;
  }

  public boolean isFailure() {
    return cause != null;
  }

  public boolean isSuccess() {
    return cause == null;
  }

  @NotNull
  public static TryOrCatch<Void> run(Runnable runnable) {
    return new TryOrCatch<>(runnable);
  }

  public void andFinally(@NotNull Runnable runnable) {
    runnable.run();
  }

  @NotNull
  public static <V> TryOrCatch<V> failure(@Nullable Throwable cause) {
    return new TryOrCatch<>(cause);
  }
}
