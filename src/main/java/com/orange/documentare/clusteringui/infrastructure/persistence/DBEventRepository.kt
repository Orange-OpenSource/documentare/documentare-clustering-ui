package com.orange.documentare.clusteringui.infrastructure.persistence

import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.clusteringui.biz.file.domain.DomainFileHistory
import com.orange.documentare.clusteringui.infrastructure.persistence.postgres.EventDTORepository
import com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.EventDTO
import com.tophe.ddd.infrastructure.event.Event
import com.tophe.ddd.infrastructure.persistence.EventRepository
import com.tophe.ddd.infrastructure.persistence.RepoPage
import com.tophe.ddd.infrastructure.persistence.RepoPageable
import org.slf4j.Logger
import org.slf4j.LoggerFactory

open class DBEventRepository(
  private val repoPostgres: EventDTORepository
) : EventRepository {

  private var insertCount = 1L
  private var readCount = 1L
  private var postgresInsertSum = 0L
  private var postgresReadSum = 0L

  private var pauseHardPersistence = false
  private var stopPersistence = false
  private val volatilePersistence = mutableListOf<Event>()

  fun isVolatilePersistenceEmpty() = volatilePersistence.isEmpty()
  fun isNormalPersistenceMode() = !pauseHardPersistence && !stopPersistence

  override fun pauseHardPersistenceAndAppendToVolatilePersistence() {
    log.info("🟣 PAUSE hard persistence and switch to volatile in memory appending")
    this.pauseHardPersistence = true
  }

  override fun stopHardAndVolatilePersistence() {
    log.info("🟣 STOP hard & volatile in memory persistence - ⚠️ Read-Only mode")
    this.stopPersistence = true
  }

  override fun resumeHardPersistenceAndDropVolatilePersistence() {
    log.info("🟣 RESUME hard persistence and DROP volatile in memory persistence - BACK to normal mode")
    volatilePersistence.clear()
    pauseHardPersistence = false
    stopPersistence = false
  }

  override fun count(): Long {
    checkPausedOrStoppedPersistence("count")
    return repoPostgres.count()
  }

  override fun deleteAll() {
    volatilePersistence.clear()
    repoPostgres.deleteAll()
  }

  override fun <S : Event> insert(entities: Iterable<S>) {
    if (stopPersistence) {
      throw IllegalStateException("\n\n❌ persistence is stopped but you tried to persist an event\n")
    }
    if (pauseHardPersistence) {
      volatilePersistence.addAll(entities)
    } else {
      doHardPersistenceInsert(entities)
    }
  }

  private fun <S : Event> doHardPersistenceInsert(entities: Iterable<S>) {
    val dtoPostgres = entities.map { entity -> EventDTO.toDTO(entity) }
    val t0: Long = System.nanoTime()
    repoPostgres.insert(dtoPostgres)
    postgresInsertSum += System.nanoTime() - t0

    if ((insertCount % 200) == 0L) {
      log.info("🔵  insert ($insertCount) ${entities.count()} events; postgres(mean ${postgresInsertSum / 1000 / insertCount} us)")
    }
    insertCount++
  }

  override fun findAll(): Iterable<Event> {
    checkPausedOrStoppedPersistence("findAll")
    return repoPostgres.findAll().map { eventDTO -> eventDTO.toDomain() }
  }

  override fun findAllInPage(repoPageable: RepoPageable): RepoPage<Event> {
    checkPausedOrStoppedPersistence("findAllInPage")

    val repoPageDTO: RepoPage<EventDTO> = repoPostgres.findAllInPage(repoPageable)
    val content = repoPageDTO.content.map { eventDTO -> eventDTO.toDomain() }
    val nextRepoPageable = repoPageDTO.nextRepoPageable
    return RepoPage(content, nextRepoPageable)
  }

  override fun load(aggregateId: String): DomainFile? {
    val t0 = System.nanoTime()
    val eventsPostgres: Collection<EventDTO> = repoPostgres.findByAggregateId(aggregateId)
    postgresReadSum += System.nanoTime() - t0
    val domainFilePostgres = if (eventsPostgres.isEmpty()) null else {
      val domainEvents = eventsPostgres.map(EventDTO::toDomain).toMutableList()
      domainEvents.addAll(volatilePersistence.filter { aggregateId == it.aggregateId })
      DomainFileHistory.from(domainEvents)
    }
    if ((readCount % 200) == 0L) {
      log.info("🔵  read ($readCount / ${eventsPostgres.size} / ${aggregateId.substring(0, 13)}); postgres(mean ${postgresReadSum / 1000 / readCount} us)")
    }
    readCount++
    return domainFilePostgres
  }

  override fun load(aggregateIds: List<String>): List<DomainFile> {
    val dtoEvents: Collection<EventDTO> = repoPostgres.findAllByAggregateIdIn(aggregateIds)
    val domainEvents = dtoEvents.map(EventDTO::toDomain).toMutableList()
    domainEvents.addAll(volatilePersistence.filter { aggregateIds.contains(it.aggregateId) })
    return loadFromDomainEvents(domainEvents)
  }

  private fun loadFromDomainEvents(domainEvents: List<Event>): List<DomainFile> {
    val mapAggregateIdEvents: Map<String, List<Event>> = domainEvents
      .groupBy(Event::aggregateId)
    val domainFiles: List<DomainFile> = mapAggregateIdEvents.keys
      .map { aggregateId ->
        DomainFileHistory.from(
          (mapAggregateIdEvents[aggregateId])!!
        )
      }
    return domainFiles
  }

  private fun checkPausedOrStoppedPersistence(operation: String) {
    if (pauseHardPersistence || stopPersistence) {
      throw IllegalStateException("\n\n❌ persistence is stopped or paused and you tried a not supported operation: $operation\n")
    }
  }

  companion object {
    val log: Logger = LoggerFactory.getLogger(DBEventRepository::class.java)
  }
}
