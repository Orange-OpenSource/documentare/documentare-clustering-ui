package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;

import com.orange.documentare.clusteringui.biz.file.event.*;
import com.tophe.ddd.infrastructure.event.Event;

class DomainEventToDTO {

  static EventDTO mapDomainEventToDTO(Event e) {
    if (e instanceof FileCreated) return FileCreatedDTO.fromDomain((FileCreated) e);
    if (e instanceof FileAddedToCluster) return FileAddedToClusterDTO.fromDomain((FileAddedToCluster) e);
    if (e instanceof FileRemovedFromCluster) return FileRemovedFromClusterDTO.fromDomain((FileRemovedFromCluster) e);
    if (e instanceof DistancesToClusterCenterAdded) return DistancesToClusterCenterAddedDTO.fromDomain((DistancesToClusterCenterAdded) e);
    if (e instanceof ClusterModified) return ClusterModifiedDTO.fromDomain((ClusterModified) e);
    if (e instanceof ClusterNotModified) return ClusterNotModifiedDTO.fromDomain((ClusterNotModified) e);
    if (e instanceof FileDuplicated) return FileDuplicatedDTO.fromDomain((FileDuplicated) e);
    if (e instanceof FileInfoReset) return FileInfoResetDTO.fromDomain((FileInfoReset) e);
    if (e instanceof FileDeleted) return FileDeletedDTO.fromDomain((FileDeleted) e);
    if (e instanceof FileMovedFromWaitingRoomToSortRoom) return FileMovedFromWaitingRoomToSortRoomDTO.fromDomain((FileMovedFromWaitingRoomToSortRoom) e);
    if (e instanceof ClusterLocked) return ClusterLockedDTO.fromDomain((ClusterLocked) e);
    if (e instanceof ClusterUnlocked) return ClusterUnlockedDTO.fromDomain((ClusterUnlocked) e);
    if (e instanceof ClusterPutInNamedClass) return ClusterPutInNamedClassDTO.fromDomain((ClusterPutInNamedClass) e);
    if (e instanceof FilePutInNamedClass) return FilePutInNamedClassDTO.fromDomain((FilePutInNamedClass) e);
    if (e instanceof MultisetElementMovedToNextHierarchyLevel) return MultisetMovedToNextHierarchyLevelDTO.fromDomain((MultisetElementMovedToNextHierarchyLevel) e);

    throw new IllegalStateException("No Matcher for event: " + e.getClass().getName());
  }
}
