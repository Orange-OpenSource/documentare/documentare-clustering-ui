package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.orange.documentare.clusteringui.biz.file.event.FileMovedFromWaitingRoomToSortRoom;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@JsonInclude
public class FileMovedFromWaitingRoomToSortRoomDTO extends EventDTO {

  public static FileMovedFromWaitingRoomToSortRoomDTO fromDomain(FileMovedFromWaitingRoomToSortRoom o) {
    final FileMovedFromWaitingRoomToSortRoomDTO dto = new FileMovedFromWaitingRoomToSortRoomDTO();
    mapDomainEvent(dto, o);

    return dto;
  }

  @Override
  public FileMovedFromWaitingRoomToSortRoom toDomain() {
    return FileMovedFromWaitingRoomToSortRoom.buildFromDB(date, aggregateId.toString());
  }


  //
  // FOR TESTS
  //
  public static FileMovedFromWaitingRoomToSortRoomDTO example() {
    return example(AGGREGATE_ID_EXAMPLE);
  }

  public static FileMovedFromWaitingRoomToSortRoomDTO example(UUID aggregateId) {
    FileMovedFromWaitingRoomToSortRoomDTO dto = new FileMovedFromWaitingRoomToSortRoomDTO();
    dto.id = exampleId(aggregateId);
    dto.aggregateId = aggregateId;
    dto.date = TEST_DATE;
    return dto;
  }
}
