package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.AGGREGATE_ID_EXAMPLE;

import java.util.UUID;

import com.orange.documentare.clusteringui.biz.file.event.FileDeleted;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FileDeletedDTO extends EventDTO {

  public String clusterId;

  public static FileDeletedDTO fromDomain(FileDeleted o) {
    final FileDeletedDTO dto = new FileDeletedDTO();
    mapDomainEvent(dto, o);

    dto.clusterId = o.clusterId;
    return dto;
  }

  @Override
  public FileDeleted toDomain() {
    return FileDeleted.buildFromDB(date, aggregateId.toString(), clusterId);
  }


  //
  // FOR TESTS
  //
  public static FileDeletedDTO example() {
    return example(AGGREGATE_ID_EXAMPLE);
  }

  public static FileDeletedDTO example(UUID aggregateId) {
    FileDeletedDTO dto = new FileDeletedDTO();
    dto.id = FileCreatedDTO.exampleId(aggregateId);
    dto.aggregateId = aggregateId;
    dto.date = FileCreatedDTO.TEST_DATE;
    dto.clusterId = FileCreatedDTO.exampleClusterId(aggregateId);
    return dto;
  }

}
