package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.AGGREGATE_ID_EXAMPLE;

import java.util.UUID;

import com.orange.documentare.clusteringui.biz.file.event.FileDuplicated;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FileDuplicatedDTO extends EventDTO {

  public String clusterId;

  public static FileDuplicatedDTO fromDomain(FileDuplicated o) {
    final FileDuplicatedDTO dto = new FileDuplicatedDTO();
    mapDomainEvent(dto, o);

    dto.clusterId = o.clusterId;
    return dto;
  }

  @Override
  public FileDuplicated toDomain() {
    return FileDuplicated.buildFromDB(date, aggregateId.toString(), clusterId);
  }

  //
  // FOR TESTS
  //
  public static FileDuplicatedDTO example() {
    return example(AGGREGATE_ID_EXAMPLE);
  }

  public static FileDuplicatedDTO example(UUID aggregateId) {
    FileDuplicatedDTO dto = new FileDuplicatedDTO();
    dto.id = FileCreatedDTO.exampleId(aggregateId);
    dto.aggregateId = aggregateId;
    dto.date = FileCreatedDTO.TEST_DATE;
    dto.clusterId = FileCreatedDTO.exampleClusterId(aggregateId);
    return dto;
  }
}
