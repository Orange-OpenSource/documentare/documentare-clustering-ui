package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.AGGREGATE_ID_EXAMPLE;

import java.util.UUID;

import com.orange.documentare.clusteringui.biz.file.event.ClusterModified;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ClusterModifiedDTO extends EventDTO {

  public String clusterId;

  public static ClusterModifiedDTO fromDomain(ClusterModified o) {
    final ClusterModifiedDTO dto = new ClusterModifiedDTO();
    mapDomainEvent(dto, o);

    dto.clusterId = o.clusterId;
    return dto;
  }

  @Override
  public ClusterModified toDomain() {
    return ClusterModified.buildFromDB(date, aggregateId.toString(), clusterId);
  }

  //
  // FOR TESTS
  //
  public static ClusterModifiedDTO example() {
    return example(AGGREGATE_ID_EXAMPLE);
  }

  public static ClusterModifiedDTO example(UUID aggregateId) {
    ClusterModifiedDTO dto = new ClusterModifiedDTO();
    dto.id = FileCreatedDTO.exampleId(aggregateId);
    dto.aggregateId = aggregateId;
    dto.date = FileCreatedDTO.TEST_DATE;
    dto.clusterId = FileCreatedDTO.exampleClusterId(aggregateId);
    return dto;
  }

}
