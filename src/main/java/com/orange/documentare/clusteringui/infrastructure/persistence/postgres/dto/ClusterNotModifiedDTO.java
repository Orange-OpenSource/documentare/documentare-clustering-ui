package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.AGGREGATE_ID_EXAMPLE;

import java.util.UUID;

import com.orange.documentare.clusteringui.biz.file.event.ClusterNotModified;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ClusterNotModifiedDTO extends EventDTO {

  public String clusterId;

  public static ClusterNotModifiedDTO fromDomain(ClusterNotModified o) {
    final ClusterNotModifiedDTO dto = new ClusterNotModifiedDTO();
    mapDomainEvent(dto, o);

    dto.clusterId = o.clusterId;
    return dto;
  }

  @Override
  public ClusterNotModified toDomain() {
    return ClusterNotModified.buildFromDB(date, aggregateId.toString(), clusterId);
  }

  //
  // FOR TESTS
  //
  public static ClusterNotModifiedDTO example() {
    return example(AGGREGATE_ID_EXAMPLE);
  }

  public static ClusterNotModifiedDTO example(UUID aggregateId) {
    ClusterNotModifiedDTO dto = new ClusterNotModifiedDTO();
    dto.id = FileCreatedDTO.exampleId(aggregateId);
    dto.aggregateId = aggregateId;
    dto.date = FileCreatedDTO.TEST_DATE;
    dto.clusterId = FileCreatedDTO.exampleClusterId(aggregateId);
    return dto;
  }

}
