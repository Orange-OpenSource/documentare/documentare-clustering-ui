package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.AGGREGATE_ID_EXAMPLE;

import java.util.UUID;

import com.orange.documentare.clusteringui.biz.file.event.MultisetElementMovedToNextHierarchyLevel;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class MultisetMovedToNextHierarchyLevelDTO extends EventDTO {

  public int nextHierarchyLevel;

  public static MultisetMovedToNextHierarchyLevelDTO fromDomain(MultisetElementMovedToNextHierarchyLevel o) {
    final MultisetMovedToNextHierarchyLevelDTO dto = new MultisetMovedToNextHierarchyLevelDTO();
    mapDomainEvent(dto, o);

    dto.nextHierarchyLevel = o.nextHierarchyLevel;
    return dto;
  }

  @Override
  public MultisetElementMovedToNextHierarchyLevel toDomain() {
    return MultisetElementMovedToNextHierarchyLevel.buildFromDB(date, aggregateId.toString(), nextHierarchyLevel);
  }


  //
  // FOR TESTS
  //
  public static final int EXAMPLE_NEXT_HIERARCHY_LEVEL = 12;

  public static MultisetMovedToNextHierarchyLevelDTO example() {
    return example(AGGREGATE_ID_EXAMPLE);
  }

  public static MultisetMovedToNextHierarchyLevelDTO example(UUID aggregateId) {
    MultisetMovedToNextHierarchyLevelDTO dto = new MultisetMovedToNextHierarchyLevelDTO();
    dto.id = FileCreatedDTO.exampleId(aggregateId);
    dto.aggregateId = aggregateId;
    dto.date = FileCreatedDTO.TEST_DATE;
    dto.nextHierarchyLevel = EXAMPLE_NEXT_HIERARCHY_LEVEL;
    return dto;
  }

}
