package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.AGGREGATE_ID_EXAMPLE;

import java.util.UUID;

import com.orange.documentare.clusteringui.biz.file.event.FilePutInNamedClass;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FilePutInNamedClassDTO extends EventDTO {

  public String[] fileNamedClass;

  public static FilePutInNamedClassDTO fromDomain(FilePutInNamedClass o) {
    final FilePutInNamedClassDTO dto = new FilePutInNamedClassDTO();
    mapDomainEvent(dto, o);
    dto.fileNamedClass = o.fileNamedClass;
    return dto;
  }

  @Override
  public FilePutInNamedClass toDomain() {
    return FilePutInNamedClass.buildFromDB(date, aggregateId.toString(), fileNamedClass);
  }


  //
  // FOR TESTS
  //
  public static FilePutInNamedClassDTO example() {
    return example(AGGREGATE_ID_EXAMPLE);
  }

  public static FilePutInNamedClassDTO example(UUID aggregateId) {
    FilePutInNamedClassDTO dto = new FilePutInNamedClassDTO();
    dto.id = FileCreatedDTO.exampleId(aggregateId);
    dto.aggregateId = aggregateId;
    dto.date = FileCreatedDTO.TEST_DATE;
    dto.fileNamedClass = exampleNamedClass(aggregateId);
    return dto;
  }

  public static String[] exampleNamedClass(UUID aggregateId) {
    return new String[] { "fileNamedClass-" + aggregateId.toString().substring(0, 4), "fileNamedClass-" + aggregateId.toString().substring(1, 5) };
  }

}
