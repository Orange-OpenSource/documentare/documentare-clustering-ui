package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.AGGREGATE_ID_EXAMPLE;

import java.util.UUID;

import com.orange.documentare.clusteringui.biz.file.event.FileRemovedFromCluster;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FileRemovedFromClusterDTO extends EventDTO {

  public String clusterId;
  public Boolean inWaitingRoom;

  public static FileRemovedFromClusterDTO fromDomain(FileRemovedFromCluster o) {
    final FileRemovedFromClusterDTO dto = new FileRemovedFromClusterDTO();
    mapDomainEvent(dto, o);

    dto.clusterId = o.clusterId;
    dto.inWaitingRoom = o.inWaitingRoom;
    return dto;
  }

  @Override
  public FileRemovedFromCluster toDomain() {
    return FileRemovedFromCluster.buildFromDB(date, aggregateId.toString(), clusterId, inWaitingRoom);
  }


  //
  // FOR TESTS
  //
  public static FileRemovedFromClusterDTO example() {
    return example(AGGREGATE_ID_EXAMPLE);
  }

  public static FileRemovedFromClusterDTO example(UUID aggregateId) {
    FileRemovedFromClusterDTO dto = new FileRemovedFromClusterDTO();
    dto.id = FileCreatedDTO.exampleId(aggregateId);
    dto.aggregateId = aggregateId;
    dto.date = FileCreatedDTO.TEST_DATE;
    dto.clusterId = FileCreatedDTO.exampleClusterId(aggregateId);
    dto.inWaitingRoom = true;
    return dto;
  }

}
