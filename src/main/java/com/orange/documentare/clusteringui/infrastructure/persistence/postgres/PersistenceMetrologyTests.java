package com.orange.documentare.clusteringui.infrastructure.persistence.postgres;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.PersistenceOperationalTests.createDTOsExamples;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.EventDTO;

import jakarta.annotation.PostConstruct;

@Service
public class PersistenceMetrologyTests {
/*
Run on IntelliJ with DB in docker

First run with empty DB:
[ 🔗 PersistenceOperationalTests] read all events by page, 1 pages, 30 events, took 9 ms
[ 🏎️ PersistenceMetrologyTests  ] time to write 1000005 events = 20155 ms
[ 🏎️ PersistenceMetrologyTests  ] time to write 10005 events = 184 ms
[ 🏎️ PersistenceMetrologyTests  ] time to read All Events (10005) by aggregate id (667) = 1884 ms

Next run:
[ 🔗 PersistenceOperationalTests] read all events by page, 102 pages, 1010070 events, took 8025 ms
*/

  private static final Logger LOG = LoggerFactory.getLogger(PersistenceMetrologyTests.class);
  private static final String TAG = "[ 🏎️  " + PersistenceMetrologyTests.class.getSimpleName() + "] ";
  private static final int EVENTS_COUNT_TO_TEST = 10000;
  private static final int BIG_DB_EVENTS_COUNT = 1000 * 1000;

  private final PersistenceOperationalTests operationalTests;
  private final PostgresEventDTORepository repository;

  @Value("${app.metrology.db.test.enabled}")
  Boolean metrologyDBTestEnabled;

  private boolean finished = false;

  public PersistenceMetrologyTests(PersistenceOperationalTests operationalTests, PostgresEventDTORepository repository) {
    this.operationalTests = operationalTests;
    this.repository = repository;
  }

  @PostConstruct
  void runTests() {
    doRunTests();
    finished = true;
  }

  private void doRunTests() {
    if (!metrologyDBTestEnabled) {
      LOG.info(TAG + "🤫 metrology tests not enabled");
      return;
    }

    if (!operationalTests.areTestsOk()) {
      LOG.info(TAG + "😱 operational tests not OK, skip metrology tests");
      return;
    }

    if (repository.count() != 0) {
      LOG.info(TAG + "🤫 events table is not empty, skip metrology tests");
      return;
    }

    LOG.info(TAG + "👉 metrology tests are running, be patient 🤓, Jojo, I see you mocking me!");

    createBigDB();
    writeAllEventsAtOnceMultipleInserts();

    repository.deleteAll();
  }

  private void createBigDB() {
    persistExampleEvents(BIG_DB_EVENTS_COUNT);
  }

  private void writeAllEventsAtOnceMultipleInserts() {
    // Given WRITE EXAMPLES
    WrittenEvents writtenEvents = persistExampleEvents(EVENTS_COUNT_TO_TEST);

    // When READ EXAMPLES
    ReadEvents readEvents = readWrittenEventsByAggregateId(writtenEvents);

    // Then WRITE & READ MATCH
    List<EventDTO> readDTOs = readEvents.eventsDTOs;
    for (int i = 0; i < readDTOs.size(); i++) {
      writtenEvents.eventsDTOs.get(i).id = readDTOs.get(i).id;
    }
    if (!readDTOs.equals(writtenEvents.eventsDTOs)) {
      throw new IllegalStateException("Read events does not match...");
    }
    // For the record: 10000 events => 64 seconds on a DB with 2 020 170 events
    if (readEvents.duration > 10 * 1000) {
      LOG.error("❌  🐢  Reading {} events by aggregate id took {} ms; Missing index?", readEvents.eventsDTOs.size(), readEvents.duration);
    }

    LOG.info(TAG + "read events by aggregate id match 💚");


    // When READ EXAMPLES WITH findAllByAggregateIdIn
    Collection<EventDTO> allByAggregateIdIn = readWrittenEventsWithAggregateList(writtenEvents);

    // Then
    if (!allByAggregateIdIn.equals(writtenEvents.eventsDTOs)) {
      throw new IllegalStateException("Read events with 'findAllByAggregateIdIn' does not match...");
    }

    LOG.info(TAG + "read events with 'findAllByAggregateIdIn' match 💚");
  }

  private Collection<EventDTO> readWrittenEventsWithAggregateList(WrittenEvents writtenEvents) {
    long t0 = System.currentTimeMillis();
    Collection<EventDTO> allByAggregateIdIn = repository.findAllByAggregateIdIn(writtenEvents.aggregatesIds.stream().map(UUID::toString).toList());
    long duration = System.currentTimeMillis() - t0;

    LOG.info(TAG + "time to read Events ({}) with 'findAllByAggregateIdIn' ({}) = {} ms", writtenEvents.eventsDTOs.size(), writtenEvents.aggregatesIds.size(),
      duration);

    return allByAggregateIdIn;
  }

  private ReadEvents readWrittenEventsByAggregateId(WrittenEvents writtenEvents) {
    long t0 = System.currentTimeMillis();
    List<EventDTO> readDTOs = writtenEvents.aggregatesIds.stream()
      .map(repository::findByAggregateId)
      .flatMap(List::stream)
      .toList();
    long duration = System.currentTimeMillis() - t0;

    LOG.info(TAG + "time to read All Events ({}) by aggregate id ({}) = {} ms", writtenEvents.eventsDTOs.size(), writtenEvents.aggregatesIds.size(),
      duration);
    return new ReadEvents(readDTOs, duration);
  }

  private record WrittenEvents(List<UUID> aggregatesIds, List<EventDTO> eventsDTOs) {
  }

  private record ReadEvents(List<EventDTO> eventsDTOs, long duration) {
  }

  // For the record: 1 000 000 events => around 20 seconds
  // For the record: 10000 events => around 260 ms
  private WrittenEvents persistExampleEvents(int eventsCount) {
    List<UUID> aggregatesIds = new ArrayList<>();
    List<EventDTO> eventsDTOs = new ArrayList<>();
    while (eventsDTOs.size() < eventsCount) {
      UUID aggregateId = UUID.randomUUID();
      eventsDTOs.addAll(createDTOsExamples(aggregateId));
      aggregatesIds.add(aggregateId);
    }
    long t0 = System.currentTimeMillis();
    repository.insert(eventsDTOs);
    final long duration = System.currentTimeMillis() - t0;
    WrittenEvents writtenEvents = new WrittenEvents(aggregatesIds, eventsDTOs);

    LOG.info(TAG + "time to write {} events = {} ms", eventsDTOs.size(), duration);
    return writtenEvents;
  }

  public boolean isFinished() {
    return finished;
  }
}
