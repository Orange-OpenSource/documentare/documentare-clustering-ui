package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;



import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;

import java.util.UUID;

import com.orange.documentare.clusteringui.biz.file.event.ClusterLocked;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ClusterLockedDTO extends EventDTO {

  public String clusterId;
  public boolean lock;

  static ClusterLockedDTO fromDomain(ClusterLocked o) {
    final ClusterLockedDTO dto = new ClusterLockedDTO();
    mapDomainEvent(dto, o);

    dto.clusterId = o.clusterId;
    dto.lock = o.lock;
    return dto;
  }

  @Override
  public ClusterLocked toDomain() {
    return ClusterLocked.buildFromDB(date, aggregateId.toString(), clusterId, lock);
  }

  //
  // FOR TESTS
  //
  public static ClusterLockedDTO example() {
    return example(AGGREGATE_ID_EXAMPLE);
  }

  public static ClusterLockedDTO example(UUID aggregateId) {
    ClusterLockedDTO dto = new ClusterLockedDTO();
    dto.id = exampleId(aggregateId);
    dto.aggregateId = aggregateId;
    dto.date = TEST_DATE;
    dto.clusterId = exampleClusterId(aggregateId);
    dto.lock = true;
    return dto;
  }

}
