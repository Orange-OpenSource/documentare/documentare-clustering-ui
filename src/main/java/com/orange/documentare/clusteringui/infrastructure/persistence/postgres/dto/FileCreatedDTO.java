package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;

import java.time.Instant;
import java.util.UUID;

import com.orange.documentare.clusteringui.biz.file.event.FileCreated;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FileCreatedDTO extends EventDTO {

  public static final Instant TEST_DATE = Instant.parse("2007-01-01T15:15:30.00Z");

  public String filename;
  public String clusterId;
  public Boolean clusterCenter;
  public Boolean waitingRoom;

  public static FileCreatedDTO fromDomain(FileCreated o) {
    final FileCreatedDTO dto = new FileCreatedDTO();
    mapDomainEvent(dto, o);

    dto.filename = o.filename;
    dto.clusterId = o.clusterId;
    dto.clusterCenter = o.clusterCenter;
    dto.waitingRoom = o.waitingRoom;

    return dto;
  }

  @Override
  public FileCreated toDomain() {
    return FileCreated.buildFromDB(date, aggregateId.toString(), filename, clusterId, clusterCenter, waitingRoom);
  }


  //
  // FOR TESTS
  //
  public static String exampleClusterId(UUID aggregateId) {
    return "clusterId-" + aggregateId.toString().substring(0, 4);
  }

  public static long exampleId(UUID aggregateId) {
    return aggregateId.getMostSignificantBits() & 0xffff;
  }

  public final static UUID AGGREGATE_ID_EXAMPLE = UUID.randomUUID();

  public static FileCreatedDTO example() {
    return example(AGGREGATE_ID_EXAMPLE);
  }

  public static FileCreatedDTO example(UUID aggregateId) {
    FileCreatedDTO dto = new FileCreatedDTO();
    dto.id = exampleId(aggregateId);
    dto.aggregateId = aggregateId;
    dto.date = TEST_DATE;
    dto.filename = exampleFilename(aggregateId);
    dto.clusterId = exampleClusterId(aggregateId);
    dto.clusterCenter = true;
    dto.waitingRoom = true;
    return dto;
  }

  public static String exampleFilename(UUID aggregateId) {
    return "filename-" + aggregateId.toString().substring(0, 4);
  }

}
