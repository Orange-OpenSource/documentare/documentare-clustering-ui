package com.orange.documentare.clusteringui.infrastructure.persistence.postgres;

import java.util.Collection;
import java.util.List;

import com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.EventDTO;
import com.tophe.ddd.infrastructure.persistence.Repository;

public interface EventDTORepository extends Repository<EventDTO> {

  Collection<EventDTO> findByAggregateId(String aggregateId);

  Collection<EventDTO> findAllByAggregateIdIn(List<String> aggregateIds);
}
