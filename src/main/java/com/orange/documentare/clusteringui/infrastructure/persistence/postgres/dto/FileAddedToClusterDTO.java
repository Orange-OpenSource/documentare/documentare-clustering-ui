package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.ClusterPutInNamedClassDTO.exampleClassName;
import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.*;

import java.util.UUID;

import com.orange.documentare.clusteringui.biz.file.event.FileAddedToCluster;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FileAddedToClusterDTO extends EventDTO {

  public String clusterId;
  public Boolean clusterCenter;
  public Integer distanceToCenter;
  public Boolean automaticInclude;
  public Boolean inMultiset;
  public Boolean lock;
  public String className;

  public static FileAddedToClusterDTO fromDomain(FileAddedToCluster o) {
    final FileAddedToClusterDTO dto = new FileAddedToClusterDTO();
    mapDomainEvent(dto, o);

    dto.clusterId = o.clusterId;
    dto.clusterCenter = o.clusterCenter;
    dto.distanceToCenter = o.distanceToCenter;
    dto.automaticInclude = o.automaticInclude;
    dto.inMultiset = o.inMultiset;
    dto.lock = o.lock;
    dto.className = o.className;

    return dto;
  }

  @Override
  public FileAddedToCluster toDomain() {
    return FileAddedToCluster.buildFromDB(date, aggregateId.toString(), clusterId, clusterCenter, distanceToCenter, automaticInclude, inMultiset,
      lock, className);
  }

  //
  // FOR TESTS
  //
  public static final int EXAMPLE_DISTANCE_TO_CENTER = 123456789;

  public static FileAddedToClusterDTO example() {
    return example(AGGREGATE_ID_EXAMPLE);
  }

  public static FileAddedToClusterDTO example(UUID aggregateId) {
    FileAddedToClusterDTO dto = new FileAddedToClusterDTO();
    dto.id = exampleId(aggregateId);
    dto.aggregateId = aggregateId;
    dto.date = TEST_DATE;
    dto.clusterId = exampleClusterId(aggregateId);
    dto.automaticInclude = true;
    dto.lock = true;
    dto.clusterCenter = true;
    dto.inMultiset = true;
    dto.distanceToCenter = EXAMPLE_DISTANCE_TO_CENTER;
    dto.className = exampleClassName(aggregateId);
    return dto;
  }

}
