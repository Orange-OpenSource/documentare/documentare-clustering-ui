package com.orange.documentare.clusteringui.infrastructure.security;

import static org.springframework.security.config.Customizer.withDefaults;

import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.savedrequest.NullRequestCache;
import org.springframework.security.web.session.HttpSessionEventPublisher;

@Configuration
public class SecurityConfig {

  @Bean
  public UserDetailsService userDetailsService() {
    User.UserBuilder users = User.builder();
    users.passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder()::encode);
    InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
    manager.createUser(users.username("Joël").password("Gardes").roles("USER").build());
    manager.createUser(users.username("Denis").password("Boisset").roles("USER").build());
    manager.createUser(users.username("Christophe").password("Maldivi").roles("USER").build());
    manager.createUser(users.username("Jacques").password("Demongeot").roles("USER").build());
    manager.createUser(users.username("DemoUser").password("DemoPassword").roles("USER").build());
    return manager;
  }


  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    disableAuthRequestCache(http);
    disableCsrfForNonBrowserApi(http);
    initApiBasicAuth(http);
    initWebAuth(http);
    return http.build();
  }

  private void disableAuthRequestCache(HttpSecurity http) throws Exception {
    // see https://docs.spring.io/spring-security/reference/5.8/servlet/architecture.html#requestcache-prevent-saved-request
    // it is causing issues after login with a redirect with a query string "?continue"
    http.requestCache((cache) -> cache.requestCache(new NullRequestCache()));
  }

  private void initWebAuth(HttpSecurity http) throws Exception {
    disableSecurityOnAssets(http);

    // FIXME: should configure dropzone to inject 'X-CSRF-Token' header
    disableCsrfToEaseDropzoneUsage(http);

    http.authorizeHttpRequests(authorize -> authorize
        .anyRequest().authenticated()
      )
      .formLogin(formLogin -> formLogin
        .loginPage("/login")
        .permitAll()
      );
  }

  private void disableSecurityOnAssets(HttpSecurity http) throws Exception {
    http.authorizeHttpRequests(auth -> auth.requestMatchers("/css/**", "/img/**", "/font/**", "/js/**").permitAll());
  }

  private void disableCsrfToEaseDropzoneUsage(HttpSecurity http) throws Exception {
    http.csrf(AbstractHttpConfigurer::disable);
  }

  private void initApiBasicAuth(HttpSecurity http) throws Exception {
    http
      .authorizeHttpRequests(auth -> auth.requestMatchers("/api/**").authenticated())
      .httpBasic(withDefaults());
  }

  /**
   * see <a href="http://docs.spring.io/spring-security/site/docs/4.1.0.RELEASE/reference/htmlsingle/#when-to-use-csrf-protection">...</a>
   */
  private void disableCsrfForNonBrowserApi(HttpSecurity http) throws Exception {
    http.csrf(AbstractHttpConfigurer::disable);
  }

  @Bean
  SessionRegistry sessionRegistry() {
    return new SessionRegistryImpl();
  }

  @Bean
  public static ServletListenerRegistrationBean<HttpSessionEventPublisher> httpSessionEventPublisher() {
    return new ServletListenerRegistrationBean<>(new HttpSessionEventPublisher());
  }
}
