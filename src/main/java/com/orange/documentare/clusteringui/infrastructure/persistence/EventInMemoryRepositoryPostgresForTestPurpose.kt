package com.orange.documentare.clusteringui.infrastructure.persistence

import com.orange.documentare.clusteringui.infrastructure.persistence.postgres.EventDTORepository
import com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.EventDTO
import com.tophe.ddd.infrastructure.persistence.InMemoryRepository

class EventInMemoryRepositoryPostgresForTestPurpose : InMemoryRepository<EventDTO, String>(),
  EventDTORepository {
  override fun findByAggregateId(aggregateId: String): Collection<EventDTO> =
    findAllByAggregateIdIn(listOf(aggregateId))

  override fun findAllByAggregateIdIn(aggregateIds: List<String>): Collection<EventDTO> =
    findAll().filter { aggregateIds.contains(it.aggregateId.toString()) }
}
