package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.AGGREGATE_ID_EXAMPLE;

import java.util.List;
import java.util.UUID;

import com.orange.documentare.clusteringui.biz.file.domain.DistanceToCluster;
import com.orange.documentare.clusteringui.biz.file.event.DistancesToClusterCenterAdded;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class DistancesToClusterCenterAddedDTO extends EventDTO {

  public List<DistanceToCluster> distancesToClustersCenters;

  public static DistancesToClusterCenterAddedDTO fromDomain(DistancesToClusterCenterAdded o) {
    final DistancesToClusterCenterAddedDTO dto = new DistancesToClusterCenterAddedDTO();
    mapDomainEvent(dto, o);

    dto.distancesToClustersCenters = o.distancesToClustersCenters;
    return dto;
  }

  @Override
  public DistancesToClusterCenterAdded toDomain() {
    return DistancesToClusterCenterAdded.buildFromDB(date, aggregateId.toString(), distancesToClustersCenters);
  }

  //
  // FOR TESTS
  //
  public static DistancesToClusterCenterAddedDTO example() {
    return example(AGGREGATE_ID_EXAMPLE);
  }

  public static DistancesToClusterCenterAddedDTO example(UUID aggregateId) {
    DistancesToClusterCenterAddedDTO dto = new DistancesToClusterCenterAddedDTO();
    dto.id = FileCreatedDTO.exampleId(aggregateId);
    dto.aggregateId = aggregateId;
    dto.date = FileCreatedDTO.TEST_DATE;
    dto.distancesToClustersCenters = exampleDistancesToCenters(aggregateId);
    return dto;
  }

  public static List<DistanceToCluster> exampleDistancesToCenters(UUID aggregateId) {
    return List.of(
      new DistanceToCluster(123456789, "clusterId-" + aggregateId.toString().substring(0, 4)),
      new DistanceToCluster(987654321, "clusterId-" + aggregateId.toString().substring(1, 5))
    );
  }

}
