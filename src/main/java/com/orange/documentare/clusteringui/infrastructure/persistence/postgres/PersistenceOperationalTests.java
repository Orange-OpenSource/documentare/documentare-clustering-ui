package com.orange.documentare.clusteringui.infrastructure.persistence.postgres;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.PostgresEventDTORepository.READ_PAGE_SIZE;

import java.util.*;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.orange.documentare.clusteringui.biz.parameters.domain.Parameters;
import com.orange.documentare.clusteringui.biz.parameters.persistence.PostgresParametersRepository;
import com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.*;

import jakarta.annotation.PostConstruct;

@Service
public class PersistenceOperationalTests {

  private static final Logger LOG = LoggerFactory.getLogger(PersistenceOperationalTests.class);
  private static final String TAG = "[ 🔗 " + PersistenceOperationalTests.class.getSimpleName() + "] ";
  private static final UUID UUID_1 = UUID.fromString("11111111-1111-1111-1111-111111111111");
  private static final UUID UUID_2 = UUID.fromString("22222222-2222-2222-2222-222222222222");

  @Value("${app.operational.db.test.enabled}")
  Boolean operationalDBTestEnabled;

  final SetupDatabasePostgres setupDatabasePostgres;

  final PostgresEventDTORepository repoEvents;
  final PostgresParametersRepository repoParameters;

  private boolean testsOk = false;

  public PersistenceOperationalTests(SetupDatabasePostgres setupDatabasePostgres, PostgresEventDTORepository repoEvents, PostgresParametersRepository repoParameters) {
    this.setupDatabasePostgres = setupDatabasePostgres;
    this.repoEvents = repoEvents;
    this.repoParameters = repoParameters;
  }

  public boolean areTestsOk() {
    return testsOk;
  }

  @PostConstruct
  void runTests() {
    if (!setupDatabasePostgres.initialized()) return;

    //////////////////////////////////////////////
    // Initial cleanupCheckingDeletedData in case a crash occurred previously leaving tests data in DB
    deleteAllTestDatas();

    if (!operationalDBTestEnabled) {
      LOG.info(TAG + "🤫 operational tests not enabled");
      testsOk = true;
      return;
    }

    //////////////////////////////////////////////
    // When/Then - Parameters
    checkParameters();

    //////////////////////////////////////////////
    // Given
    final long numberOfEventsBeforeTests = repoEvents.count();
    LOG.info(TAG + "number of events in DB = {}", numberOfEventsBeforeTests);

    List<EventDTO> dtoExamplesOneByOne;
    List<EventDTO> dtoExamplesMultipleInserts;
    try {
      dtoExamplesOneByOne = persistDTOsExamplesOneByOneWith(UUID_1);
      dtoExamplesMultipleInserts = persistDTOsExamplesMultipleInsertsWith(UUID_2);
    }
    catch (Exception e) {
      LOG.error(TAG + "❌ failed to persist events", e);
      deleteAllTestDatas();
      return;
    }

    checkCount(numberOfEventsBeforeTests, dtoExamplesOneByOne.size(), dtoExamplesMultipleInserts.size());


    // When/Then
    checkReadEvents(UUID_1, dtoExamplesOneByOne);
    checkReadEvents(UUID_2, dtoExamplesMultipleInserts);

    // When/Then
    Collection<EventDTO> allEventsDTOs = readAllEventsInDB();
    checkExamplesInEventsList(allEventsDTOs, dtoExamplesOneByOne, dtoExamplesMultipleInserts, "read all");

    // When/Then
    Collection<EventDTO> our2TestAggregatesEventsDTOs =
      repoEvents.findAllByAggregateIdIn(List.of(UUID_1.toString(), UUID_2.toString()));
    checkExamplesInEventsList(our2TestAggregatesEventsDTOs, dtoExamplesOneByOne, dtoExamplesMultipleInserts,
      "read by aggregate id list");

    // Teardown
    cleanupCheckingDeletedData(dtoExamplesOneByOne.size(), dtoExamplesMultipleInserts.size(), true);

    LOG.info(TAG + "💚 read events OK");
    testsOk = true;
  }

  private void deleteAllTestDatas() {
    cleanupCheckingDeletedData(-1, -1, false);
  }

  private void checkParameters() {
    if (repoParameters.count() == 0) {
      try {
        doCheckParameters();
      }
      catch (Exception e) {
        repoParameters.deleteAll();
        throw new IllegalStateException(TAG + "❌ Parameters test raised an exception", e);
      }
    }
    else {
      LOG.info(TAG + "🤫 parameters operational tests not enabled (Table is not empty)");
    }
  }

  private void doCheckParameters() {
    Parameters defaults = Parameters.defaults();
    repoParameters.insert(List.of(defaults));
    List<Parameters> all = StreamSupport.stream(repoParameters.findAll().spliterator(), false).toList();
    if (all.size() != 1) {
      throw new IllegalStateException(String.format(TAG + "❌ failed to insert parameters in DB"));
    }
    Parameters readParameters = all.get(0);
    Parameters expected = defaults.toBuilder()
      .id(readParameters.id)
      .consolidateCluster(defaults.consolidateCluster != null && defaults.consolidateCluster)
      .build();
    if (!readParameters.equals(expected)) {
      // to ease test: go back to previous state
      repoParameters.deleteAll();
      throw new IllegalStateException(String.format(TAG + "❌ read parameters in DB do not match defaults values: \n%s\n!=\n%s", readParameters, expected));
    }
    repoParameters.deleteAll();
    LOG.info(TAG + "💚 parameters OK");
  }

  private void checkCount(long numberOfEventsBeforeTests, int dtoExamplesOneByOneCount, int dtoExamplesMultipleInsertsCount)
  {
    final long t0 = System.currentTimeMillis();
    final long numberOfEventsAfterPersistence = repoEvents.count();
    final long duration = System.currentTimeMillis() - t0;
    LOG.info(TAG + "number of events after persistence = {} (count duration = {} ms)", numberOfEventsAfterPersistence, duration);

    int expectedNewRowsCount = dtoExamplesOneByOneCount + dtoExamplesMultipleInsertsCount;
    long newRowsCount = numberOfEventsAfterPersistence - numberOfEventsBeforeTests;
    if (newRowsCount != expectedNewRowsCount) {
      throw new IllegalStateException(String.format(TAG + "❌ was expecting a count of %d new rows, found %d", expectedNewRowsCount, newRowsCount));
    }
  }

  private void cleanupCheckingDeletedData(int aggregateIdWriteOneByOneEventsCount, int aggregateIdWriteMultipleInsertsEventsCount, boolean check)
  {
    int eventsDeletedCountAggregateIdWriteOneByOne = 0;
    int eventsDeletedCountAggregateIdWriteMultipleInserts = 0;
    try {
      eventsDeletedCountAggregateIdWriteOneByOne = repoEvents.delete(UUID_1);
    }
    catch (Exception e) {
      LOG.error(TAG + "❌ failed to delete events AggregateIdWriteOneByOne", e);
    }
    try {
      eventsDeletedCountAggregateIdWriteMultipleInserts = repoEvents.delete(UUID_2);
    }
    catch (Exception e) {
      LOG.error(TAG + "❌ failed to delete events AggregateIdWriteMultipleInserts", e);
    }

    if (check) {
      if (eventsDeletedCountAggregateIdWriteOneByOne != aggregateIdWriteOneByOneEventsCount) {
        throw new IllegalStateException(
          String.format(TAG + "❌ should have deleted %d events for aggregated id %s, deleted %d", aggregateIdWriteOneByOneEventsCount, UUID_1,
            eventsDeletedCountAggregateIdWriteOneByOne));
      }

      if (eventsDeletedCountAggregateIdWriteMultipleInserts != aggregateIdWriteMultipleInsertsEventsCount) {
        throw new IllegalStateException(String.format(TAG + "❌ should have deleted %d events for aggregated id %s, deleted %d", aggregateIdWriteMultipleInsertsEventsCount,
          UUID_2, eventsDeletedCountAggregateIdWriteMultipleInserts));
      }
    }

    LOG.info(TAG + "delete {} events", eventsDeletedCountAggregateIdWriteOneByOne + eventsDeletedCountAggregateIdWriteMultipleInserts);
  }

  private void checkExamplesInEventsList(Collection<EventDTO> allEventsDTOs, List<EventDTO> dtoExamplesOneByOne,
    List<EventDTO> dtoExamplesMultipleInserts, String readMethod)
  {
    List<EventDTO> foundWriteOneByOneEventDTOs = allEventsDTOs.stream().filter(eventDTO -> eventDTO.aggregateId.equals(PersistenceOperationalTests.UUID_1)).toList();
    List<EventDTO> foundWriteMultipleInsertsEventDTOs = allEventsDTOs.stream().filter(eventDTO -> eventDTO.aggregateId.equals(PersistenceOperationalTests.UUID_2)).toList();
    // Then
    for (int i = 0; i < dtoExamplesOneByOne.size(); i++) {
      updateId(dtoExamplesOneByOne.get(i), foundWriteOneByOneEventDTOs.get(i).id);
    }
    if (!foundWriteOneByOneEventDTOs.equals(dtoExamplesOneByOne)) {
      throw new IllegalStateException(String.format("%s ❌ read events do not match: %s / %s", TAG, dtoExamplesOneByOne, foundWriteOneByOneEventDTOs));
    }

    for (int i = 0; i < dtoExamplesMultipleInserts.size(); i++) {
      updateId(dtoExamplesMultipleInserts.get(i), foundWriteMultipleInsertsEventDTOs.get(i).id);
    }
    if (!foundWriteMultipleInsertsEventDTOs.equals(dtoExamplesMultipleInserts)) {
      throw new IllegalStateException(String.format("%s ❌ read events do not match: %s / %s", TAG, dtoExamplesMultipleInserts, foundWriteMultipleInsertsEventDTOs));
    }

    LOG.info(TAG + "check test aggregates in events list (method " + readMethod + "): previously created events were retrieved and match");
  }

  private void checkReadEvents(UUID aggregateId, List<EventDTO> expectedDtoExamples) {
    List<EventDTO> readDTOs;

    boolean readMethodReadEvents = (new Random()).nextBoolean();
    if (readMethodReadEvents) {
      readDTOs = repoEvents.findByAggregateId(aggregateId);
    }
    else {
      readDTOs = repoEvents.findByAggregateId(aggregateId.toString()).stream().toList();
    }
    LOG.debug(TAG + "📖 read events for aggregate {} with method {}: {}", aggregateId, readMethodReadEvents ? "readEvents" : "findByAggregateId", readDTOs);

    // Then
    for (int i = 0; i < expectedDtoExamples.size(); i++) {
      updateId(expectedDtoExamples.get(i), readDTOs.get(i).id);
    }
    if (!readDTOs.equals(expectedDtoExamples)) {
      throw new IllegalStateException(String.format("%s ❌ read events do not match: %s / %s", TAG, expectedDtoExamples, readDTOs));
    }
  }

  private List<EventDTO> readAllEventsInDB() {
    try {
      repoEvents.findByAggregateId(1, 5, OptionalLong.empty());
      throw new IllegalStateException("should raise an exception: last page index can not be empty if page number is not zero");
    }
    catch (IllegalStateException ignored) {
    }

    List<EventDTO> allEventsDTOs = new ArrayList<>();
    final long t0 = System.currentTimeMillis();
    boolean findAllMethod = (new Random()).nextBoolean();
    if (findAllMethod) {
      allEventsDTOs = (List<EventDTO>) repoEvents.findAll();
    }
    else {
      repoEvents.applyOnAll(allEventsDTOs::addAll, READ_PAGE_SIZE);
    }
    long duration = System.currentTimeMillis() - t0;

    LOG.info(TAG + "read all events by page, {} events, took {} ms, method {}", allEventsDTOs.size(), duration, findAllMethod ? "findAll" : "applyOnAll");

    return allEventsDTOs;
  }

  private List<EventDTO> persistDTOsExamplesOneByOneWith(UUID aggregateId) {
    List<EventDTO> dtoExamples = createDTOsExamples(aggregateId);
    dtoExamples.forEach(this::persist);
    return dtoExamples;
  }

  private List<EventDTO> persistDTOsExamplesMultipleInsertsWith(UUID aggregateId) {
    List<EventDTO> dtoExamples = createDTOsExamples(aggregateId);
    repoEvents.insert(dtoExamples);
    return dtoExamples;
  }

  static List<EventDTO> createDTOsExamples(UUID aggregateId) {
    ClusterLockedDTO clusterLockedDTO = ClusterLockedDTO.example(aggregateId);
    ClusterModifiedDTO clusterModifiedDTO = ClusterModifiedDTO.example(aggregateId);
    ClusterNotModifiedDTO clusterNotModifiedDTO = ClusterNotModifiedDTO.example(aggregateId);
    ClusterPutInNamedClassDTO clusterPutInNamedClassDTO = ClusterPutInNamedClassDTO.example(aggregateId);
    ClusterUnlockedDTO clusterUnlockedDTO = ClusterUnlockedDTO.example(aggregateId);
    DistancesToClusterCenterAddedDTO distancesToClusterCenterAddedDTO = DistancesToClusterCenterAddedDTO.example(aggregateId);
    FileAddedToClusterDTO fileAddedToClusterDTO = FileAddedToClusterDTO.example(aggregateId);
    FileCreatedDTO fileCreatedDTO = FileCreatedDTO.example(aggregateId);
    FileDeletedDTO fileDeletedDTO = FileDeletedDTO.example(aggregateId);
    FileDuplicatedDTO fileDuplicatedDTO = FileDuplicatedDTO.example(aggregateId);
    FileInfoResetDTO fileInfoResetDTO = FileInfoResetDTO.example(aggregateId);
    FileMovedFromWaitingRoomToSortRoomDTO fileMovedFromWaitingRoomToSortRoomDTO = FileMovedFromWaitingRoomToSortRoomDTO.example(aggregateId);
    FilePutInNamedClassDTO filePutInNamedClassDTO = FilePutInNamedClassDTO.example(aggregateId);
    FileRemovedFromClusterDTO fileRemovedFromClusterDTO = FileRemovedFromClusterDTO.example(aggregateId);
    MultisetMovedToNextHierarchyLevelDTO multisetMovedToNextHierarchyLevelDTO = MultisetMovedToNextHierarchyLevelDTO.example(aggregateId);

    return List.of(
      clusterLockedDTO,
      clusterModifiedDTO,
      clusterNotModifiedDTO,
      clusterPutInNamedClassDTO,
      clusterUnlockedDTO,
      distancesToClusterCenterAddedDTO,
      fileAddedToClusterDTO,
      fileCreatedDTO,
      fileDeletedDTO,
      fileDuplicatedDTO,
      fileInfoResetDTO,
      fileMovedFromWaitingRoomToSortRoomDTO,
      filePutInNamedClassDTO,
      fileRemovedFromClusterDTO,
      multisetMovedToNextHierarchyLevelDTO);
  }

  private static void updateId(EventDTO eventDTO, long id) {
    eventDTO.id = id;
  }

  private void persist(EventDTO eventDTO) {
    repoEvents.insert(eventDTO);
  }

}
