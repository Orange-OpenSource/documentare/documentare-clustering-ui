package com.orange.documentare.clusteringui.infrastructure.persistence.postgres;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

import org.postgresql.util.PGobject;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.EventDTO;
import com.tophe.ddd.infrastructure.persistence.RepoPage;
import com.tophe.ddd.infrastructure.persistence.RepoPageable;

@SuppressWarnings("SqlResolve") @Service
public class PostgresEventDTORepository implements EventDTORepository {

  public static final Calendar tzUTC = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
  private static final int INSERT_BATCH_SIZE = 800;
  static final int READ_PAGE_SIZE = 10000;
  private final EventJsonIO jsonIO = new EventJsonIO();
  private final JdbcTemplate jdbcTemplate;

  public PostgresEventDTORepository(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public void insert(EventDTO event) {
    if (jdbcTemplate.update(
      "INSERT INTO EVENTS(AGGREGATE_ID, EVENT_DATE, EVENT_TYPE, JSON_DATA) VALUES(?, ?, ?, ?)",
      ps -> persistEvent(ps, event)) <= 0)
    {
      throw new IllegalStateException(String.format("Failed to add event %s", event.toString()));
    }
  }

  @Override
  public <S extends EventDTO> void insert(Iterable<S> entities) {
    jdbcTemplate.batchUpdate("INSERT INTO EVENTS(AGGREGATE_ID, EVENT_DATE, EVENT_TYPE, JSON_DATA) VALUES(?, ?, ?, ?)",
      (List<EventDTO>) entities,
      INSERT_BATCH_SIZE,
      this::persistEvent);
  }

  public List<EventDTO> findByAggregateId(UUID aggregateId) {
    return jdbcTemplate.query(
      "SELECT ID, AGGREGATE_ID, EVENT_DATE, EVENT_TYPE, JSON_DATA FROM EVENTS WHERE AGGREGATE_ID = ? ORDER BY ID",
      (rs, rowNum) -> toEventDTO(rs),
      aggregateId);
  }

  @Override
  public Collection<EventDTO> findByAggregateId(String aggregateId) {
    return findByAggregateId(UUID.fromString(aggregateId));
  }

  public List<EventDTO> findByAggregateId(int pageNumber, int pageSize, OptionalLong lastPageIndex) {
    if (lastPageIndex.isEmpty() && pageNumber != 0) {
      throw new IllegalStateException("last page index not provided, assuming it is the first page, but page number = " + pageNumber);
    }

    long abovePageIndex = lastPageIndex.orElse(-1);

    return jdbcTemplate.query(
      "SELECT ID, AGGREGATE_ID, EVENT_DATE, EVENT_TYPE, JSON_DATA FROM EVENTS WHERE id > ? ORDER BY id LIMIT ?",
      (rs, rowNum) -> toEventDTO(rs),
      abovePageIndex, pageSize);
  }

  @Override
  public Iterable<EventDTO> findAll() {
    // For the record: 3.5 seconds for 418 574 events, pageSize = 10000
    // For the record: 7.8 seconds for 1 018 574 events, pageSize = 10000
    List<EventDTO> allEventsDTOs = new ArrayList<>();
    RepoPageable nextRepoPageable = new RepoPageable(0, READ_PAGE_SIZE, OptionalLong.empty());
    do {
      RepoPage<EventDTO> page = findAllInPage(nextRepoPageable);
      nextRepoPageable = page.nextRepoPageable();
      allEventsDTOs.addAll(page.content());
    }
    while (nextRepoPageable != null);

    return allEventsDTOs;
  }

  @Override
  public RepoPage<EventDTO> findAllInPage(RepoPageable repoPageable) {
    List<EventDTO> eventDTOS = findByAggregateId(repoPageable.pageNumber(), repoPageable.pageSize(), repoPageable.lastPageIndex());

    RepoPageable nextRepoPageable = null;
    int size = eventDTOS.size();
    if (size > 0) {
      OptionalLong newLastPageIndex = OptionalLong.of(eventDTOS.get(size - 1).id);
      nextRepoPageable = new RepoPageable(repoPageable.pageNumber() + 1, repoPageable.pageSize(), newLastPageIndex);
    }
    return new RepoPage<>(eventDTOS, nextRepoPageable);
  }

  // https://www.baeldung.com/spring-jdbctemplate-in-list
  @Override
  public Collection<EventDTO> findAllByAggregateIdIn(List<String> aggregateIds) {
    if (aggregateIds.size() == 0) {
      return List.of();
    }
    String inSql = String.join(",", Collections.nCopies(aggregateIds.size(), "?"));
    return jdbcTemplate.query(
      String.format("SELECT ID, AGGREGATE_ID, EVENT_DATE, EVENT_TYPE, JSON_DATA FROM EVENTS WHERE AGGREGATE_ID IN (%s) ORDER BY ID", inSql),
      (rs, rowNum) -> toEventDTO(rs),
      aggregateIds.stream().map(UUID::fromString).toArray());
  }

  // could be improved? ok for one million so far (~50ms)
  @Override
  public long count() {
    return jdbcTemplate.queryForObject(
      "SELECT COUNT(*) FROM events;",
      Integer.class);
  }

  public int delete(UUID aggregateId) {
    return jdbcTemplate.update("DELETE FROM EVENTS WHERE AGGREGATE_ID = ?", aggregateId);
  }

  @Override
  public void deleteAll() {
    jdbcTemplate.update("DELETE FROM events;");
  }

  private void persistEvent(PreparedStatement ps, EventDTO event) throws SQLException {
    ps.setObject(1, event.aggregateId);
    Timestamp ts = event.date != null ? Timestamp.from(event.date) : null;
    ps.setTimestamp(2, ts, tzUTC);
    ps.setString(3, event.eventType());

    PGobject jsonbObj = new PGobject();
    jsonbObj.setType("json");
    jsonbObj.setValue(jsonIO.serialize(event));
    ps.setObject(4, jsonbObj);
  }

  private EventDTO toEventDTO(ResultSet rs) throws SQLException {
    EventDTO eventDTO = buildEventDTOFromJsonData(rs);
    eventDTO.id = rs.getLong("ID");
    eventDTO.aggregateId = (UUID) rs.getObject("AGGREGATE_ID");
    eventDTO.date = retrieveDate(rs);
    return eventDTO;
  }

  private Instant retrieveDate(ResultSet rs) throws SQLException {
    Timestamp ts = rs.getTimestamp("EVENT_DATE", tzUTC);
    return ts != null ? ts.toInstant() : null;
  }

  private EventDTO buildEventDTOFromJsonData(ResultSet rs) throws SQLException {
    PGobject jsonObj = (PGobject) rs.getObject("JSON_DATA");
    String json = jsonObj.getValue();
    String eventType = rs.getString("EVENT_TYPE");
    return jsonIO.deserialize(json, eventType);
  }
}
