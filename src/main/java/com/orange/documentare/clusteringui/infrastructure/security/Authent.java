package com.orange.documentare.clusteringui.infrastructure.security;

import java.security.Principal;

import org.springframework.ui.Model;

public class Authent {

  public static boolean isAuthenticated(Principal principal) {
    return principal != null && principal.getName() != null;
  }

  public static void addAuthenticatedModel(Model model, Principal principal) {
    boolean authenticated = isAuthenticated(principal);
    model.addAttribute("authenticated", authenticated);
    if (authenticated) {
      model.addAttribute("username", principal.getName());
    }
  }
}
