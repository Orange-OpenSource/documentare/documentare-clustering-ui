package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.AGGREGATE_ID_EXAMPLE;

import java.util.UUID;

import com.orange.documentare.clusteringui.biz.file.event.ClusterUnlocked;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ClusterUnlockedDTO extends EventDTO {

  public String clusterId;
  public boolean lock;
  public String className;

  public static ClusterUnlockedDTO fromDomain(ClusterUnlocked o) {
    final ClusterUnlockedDTO dto = new ClusterUnlockedDTO();
    mapDomainEvent(dto, o);

    dto.clusterId = o.clusterId;
    dto.lock = o.lock;
    dto.className = o.className;
    return dto;
  }

  @Override
  public ClusterUnlocked toDomain() {
    return ClusterUnlocked.buildFromDB(date, aggregateId.toString(), clusterId, lock, className);
  }

  //
  // FOR TESTS
  //
  public static ClusterUnlockedDTO example() {
    return example(AGGREGATE_ID_EXAMPLE);
  }

  public static ClusterUnlockedDTO example(UUID aggregateId) {
    ClusterUnlockedDTO dto = new ClusterUnlockedDTO();
    dto.id = FileCreatedDTO.exampleId(aggregateId);
    dto.aggregateId = aggregateId;
    dto.date = FileCreatedDTO.TEST_DATE;
    dto.clusterId = FileCreatedDTO.exampleClusterId(aggregateId);
    dto.className = "className-" + aggregateId.toString().substring(0, 4);
    dto.lock = true;
    return dto;
  }

}
