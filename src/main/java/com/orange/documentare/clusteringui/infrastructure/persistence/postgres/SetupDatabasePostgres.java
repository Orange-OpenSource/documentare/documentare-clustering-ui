package com.orange.documentare.clusteringui.infrastructure.persistence.postgres;

import java.sql.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.orange.documentare.clusteringui.ShutdownApplication;

import jakarta.annotation.PostConstruct;

@Service
public class SetupDatabasePostgres {

  private static final String CREATE_EVENT_TABLE_IF_NOT_EXISTS = """
    CREATE TABLE IF NOT EXISTS events (
        ID BIGSERIAL PRIMARY KEY,
        AGGREGATE_ID UUID NOT NULL,
        EVENT_DATE TIMESTAMPTZ NOT NULL,
        EVENT_TYPE VARCHAR(255) NOT NULL,
        JSON_DATA JSON NOT NULL
    )
    """;

  private static final String CREATE_AGGREGATE_ID_INDEX = """
    CREATE INDEX IF NOT EXISTS aggreggate_id_index ON events ( AGGREGATE_ID )
    """;

  private static final String CREATE_PARAMETERS_TABLE_IF_NOT_EXISTS = """
    CREATE TABLE IF NOT EXISTS parameters (
         ID BIGSERIAL PRIMARY KEY,
         RAW_CONVERTER BOOLEAN,
         P_COUNT INT,
         ACUT_SD_FACTOR FLOAT(2),
         QCUT_SD_FACTOR FLOAT(2),
         SCUT_SD_FACTOR FLOAT(2),
         CCUT_PERCENTILE SMALLINT,
         KNN_THRESHOLD SMALLINT,
         SLOOP BOOLEAN,
         CONSOLIDATE_CLUSTER BOOLEAN,
         ENROLL BOOLEAN,
         ENROLL_ACUT_SD_FACTOR FLOAT(2),
         ENROLL_QCUT_SD_FACTOR FLOAT(2),
         ENROLL_SCUT_SD_FACTOR FLOAT(2),
         ENROLL_CCUT_PERCENTILE SMALLINT,
         ENROLL_KNN_THRESHOLD SMALLINT,
         THRESHOLD_FACTOR FLOAT(2),
         FEED BOOLEAN,
         FEED_THRESHOLD SMALLINT,
         MIN_MULTISET_THRESHOLD SMALLINT,
         MAX_MULTISET_THRESHOLD SMALLINT,
         SUFFIX_ARRAY_ALGORITHM VARCHAR(31)
    )
    """;

  private final ShutdownApplication shutdownApplication;

  @Value("${app.datasource.url}")
  String datasourceUrl;

  @Value("${app.datasource.dbname}")
  String dbName;
  @Value("${spring.datasource.username}")
  String username;
  @Value("${spring.datasource.password}")
  String password;

  private boolean isInitialized = false;

  public SetupDatabasePostgres(ShutdownApplication shutdownApplication) {
    this.shutdownApplication = shutdownApplication;
  }

  boolean initialized() {
    return isInitialized;
  }

  @PostConstruct
  void setupDatabase() throws SQLException {
    Connection connectionToCreateDatabase = connectToCreateDatabase(shutdownApplication);
    if (connectionToCreateDatabase == null) {
      return;
    }

    if (createDatabaseIfNotExists(dbName, connectionToCreateDatabase)) {
      Connection connectToCreateTables = connectToCreateTables(shutdownApplication, dbName);
      isInitialized = createTablesIfNotExist(connectToCreateTables);
      connectToCreateTables.close();
    }
    connectionToCreateDatabase.close();
  }

  private Connection connectToCreateDatabase(ShutdownApplication shutdownApplication) {
    return connect(shutdownApplication, null);
  }

  private Connection connectToCreateTables(ShutdownApplication shutdownApplication, String databaseName) {
    return connect(shutdownApplication, databaseName);
  }

  private Connection connect(ShutdownApplication shutdownApplication, String dbName) {
    int attemptCount = 0;
    do {
      try {
        String connectToDatabase = dbName == null ? "" : dbName;
        return DriverManager.getConnection(datasourceUrl + connectToDatabase, username, password);
      }
      catch (SQLException e) {
        log.error("⚠️ {}", e, e);
        log.error("\n\n⚠️ Failed to connect to database ({}: {}...), wait for a while and retry.\n\n", e.getClass().getSimpleName(), e.getMessage());
      }
      try {
        Thread.sleep(5000);
      }
      catch (InterruptedException e) {
        log.error("⚠️ {}", e, e);
      }
    }
    while (attemptCount++ < 6);
    log.error("\n\n🔴 give up trying to connect to database, shutdown application\n\n");
    shutdownApplication.initiateShutdown(1);
    return null;
  }

  private static final Logger log = LoggerFactory.getLogger(SetupDatabasePostgres.class);

  private static boolean createDatabaseIfNotExists(String databaseName, Connection connection) {
    boolean ok = false;
    Statement statement = null;
    try {
      statement = connection.createStatement();
      statement.executeQuery("SELECT count(*) FROM pg_database WHERE datname = '" + databaseName + "'");
      ResultSet resultSet = statement.getResultSet();
      resultSet.next();
      int count = resultSet.getInt(1);

      if (count <= 0) {
        statement.executeUpdate("CREATE DATABASE " + databaseName);
        log.info("🟢 Database {} created", databaseName);
      }
      else {
        log.info("🔵 Database {} already exist", databaseName);
      }
      ok = true;
    }
    catch (SQLException e) {
      log.error("🔴 {}", e, e);
    }
    finally {
      try {
        if (statement != null) {
          statement.close();
        }
      }
      catch (SQLException e) {
        log.error(e.toString());
      }
    }
    return ok;
  }

  private static boolean createTablesIfNotExist(Connection connection) {
    boolean ok = false;
    Statement statement = null;
    try {
      statement = connection.createStatement();
      statement.executeUpdate(CREATE_EVENT_TABLE_IF_NOT_EXISTS);
      statement.executeUpdate(CREATE_AGGREGATE_ID_INDEX);
      log.info("🔵 Event Table OK");
      statement.executeUpdate(CREATE_PARAMETERS_TABLE_IF_NOT_EXISTS);
      log.info("🔵 Parameters Table OK");
      ok = true;
    }
    catch (SQLException e) {
      log.error("🔴 {}", e.toString());
    }
    finally {
      try {
        if (statement != null) {
          statement.close();
        }
      }
      catch (SQLException e) {
        log.error(e.toString());
      }
    }
    return ok;
  }

}
