package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;

import java.time.Instant;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tophe.ddd.infrastructure.event.Event;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@NoArgsConstructor
@EqualsAndHashCode
public abstract class EventDTO {

  @JsonIgnore
  public long id = -1;
  @JsonIgnore
  public UUID aggregateId;

  @JsonIgnore
  public Instant date;

  public EventDTO(UUID aggregateId, Instant date) {
    this.aggregateId = aggregateId;
    this.date = date;
  }

  public String eventType() {
    return this.getClass().getSimpleName();
  }

  protected static void mapDomainEvent(EventDTO dto, Event e) {
    dto.aggregateId = UUID.fromString(e.aggregateId);
    dto.date = e.date;
  }

  public abstract Event toDomain();

  public static EventDTO toDTO(Event e) {
    return DomainEventToDTO.mapDomainEventToDTO(e);
  }
}
