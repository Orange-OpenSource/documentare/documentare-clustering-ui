package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.AGGREGATE_ID_EXAMPLE;

import java.util.UUID;

import com.orange.documentare.clusteringui.biz.file.event.ClusterPutInNamedClass;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ClusterPutInNamedClassDTO extends EventDTO {

  public String clusterId;
  public String className;

  public static ClusterPutInNamedClassDTO fromDomain(ClusterPutInNamedClass o) {
    final ClusterPutInNamedClassDTO dto = new ClusterPutInNamedClassDTO();
    mapDomainEvent(dto, o);

    dto.clusterId = o.clusterId;
    dto.className = o.className;
    return dto;
  }

  @Override
  public ClusterPutInNamedClass toDomain() {
    return ClusterPutInNamedClass.buildFromDB(date, aggregateId.toString(), clusterId, className);
  }

  //
  // FOR TESTS
  //
  public static ClusterPutInNamedClassDTO example() {
    return example(AGGREGATE_ID_EXAMPLE);
  }

  public static ClusterPutInNamedClassDTO example(UUID aggregateId) {
    ClusterPutInNamedClassDTO dto = new ClusterPutInNamedClassDTO();
    dto.id = FileCreatedDTO.exampleId(aggregateId);
    dto.aggregateId = aggregateId;
    dto.date = FileCreatedDTO.TEST_DATE;
    dto.clusterId = FileCreatedDTO.exampleClusterId(aggregateId);
    dto.className = exampleClassName(aggregateId);
    return dto;
  }

  public static String exampleClassName(UUID aggregateId) {
    return "className-" + aggregateId.toString().substring(0, 4);
  }
}
