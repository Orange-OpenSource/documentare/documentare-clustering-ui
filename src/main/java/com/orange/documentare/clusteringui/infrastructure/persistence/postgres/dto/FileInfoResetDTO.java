package com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto;

import static com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.FileCreatedDTO.AGGREGATE_ID_EXAMPLE;

import java.util.UUID;

import com.orange.documentare.clusteringui.biz.file.event.FileInfoReset;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class FileInfoResetDTO extends EventDTO {

  public String clusterId;

  public static FileInfoResetDTO fromDomain(FileInfoReset o) {
    final FileInfoResetDTO dto = new FileInfoResetDTO();
    mapDomainEvent(dto, o);

    dto.clusterId = o.clusterId;
    return dto;
  }

  @Override
  public FileInfoReset toDomain() {
    return FileInfoReset.buildFromDB(date, aggregateId.toString(), clusterId);
  }


  //
  // FOR TESTS
  //
  public static FileInfoResetDTO example() {
    return example(AGGREGATE_ID_EXAMPLE);
  }

  public static FileInfoResetDTO example(UUID aggregateId) {
    FileInfoResetDTO dto = new FileInfoResetDTO();
    dto.id = FileCreatedDTO.exampleId(aggregateId);
    dto.aggregateId = aggregateId;
    dto.date = FileCreatedDTO.TEST_DATE;
    dto.clusterId = FileCreatedDTO.exampleClusterId(aggregateId);
    return dto;
  }
}
