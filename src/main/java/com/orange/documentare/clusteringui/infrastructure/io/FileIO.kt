/*
 * Copyright (c) 2017 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.clusteringui.infrastructure.io

import com.orange.documentare.clusteringui.Couple
import com.orange.documentare.clusteringui.biz.file.domain.DomainFile
import com.orange.documentare.core.system.thumbnail.Thumbnail
import com.orange.documentare.core.system.thumbnail.ThumbnailOptions
import org.slf4j.LoggerFactory
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

object FileIO {
  private const val THUMBNAILS_DIR = "thumbnails"
  private const val NOT_INITIALIZED_PATH = "/not-initialized-yet/"

  private val log = LoggerFactory.getLogger(FileIO::class.java)
  private val CONVERT_OPTIONS = arrayOf(
    "-thumbnail", "'100x100>'", "-gravity", "center", "-extent", "100x100", "-auto-orient"
  )
  private const val OUTPUT_DIR = "/output/"
  private const val FILES_DIR = "/"
  private const val EXPORT_CLASSES_FILE_NAME = "/export-classes.csv"
  private const val EXPORT_CLIENT_FILE_NAME = "/export-client.json"
  private var filesDirectoryPath = NOT_INITIALIZED_PATH

  @JvmStatic
  fun initFilesDirectory(filesDirectoryPath: String) {
    FileIO.filesDirectoryPath = if (filesDirectoryPath.endsWith("/")) filesDirectoryPath else "$filesDirectoryPath/"
    createDirectory(filesDirectoryPath)
    createDirectory(outputDirectory())
    createDirectory(thumbnailDirectory())
  }

  @JvmStatic
  fun resetFilesDirectoryForTestsPurpose() {
    initFilesDirectory(NOT_INITIALIZED_PATH)
  }

    fun thumbnailPathOf(file: DomainFile): String = thumbnailDirectory() + File.separator + file.aggregateId

  @JvmStatic
  fun thumbnailWebPathOf(file: DomainFile): String {
    return if (hasThumbnail(file)) {
      "/${THUMBNAILS_DIR}/${file.aggregateId}"
    } else {
      "/img/no_image.png"
    }
  }

  fun filesPathOf(files: List<DomainFile>): List<String> = files.map { filePathOf(it) }

  fun fileWebPathOf(file: DomainFile): String = filePathOf(FILES_DIR, file)

  fun filePathOf(file: DomainFile): String = filePathOf(filesDirectoryPath, file)

  private fun filePathOf(dirPath: String, file: DomainFile): String {
    var extension = Optional.empty<String>()
    val i = file.filename.lastIndexOf('.')
    if (i > 0) {
      extension = Optional.of(file.filename.substring(i + 1))
    }
    val pathWithIdOnly = dirPath + file.aggregateId
    return extension.map { s: String -> "$pathWithIdOnly.$s" }.orElse(pathWithIdOnly)
  }

  fun createThumbnail(domainFile: DomainFile) {
    val filePath = filePathOf(domainFile)
    val thumbnailPath = thumbnailPathOf(domainFile)
    log.debug("    [THUMBNAILS] create $filePath")
    val sourceImage = File(filePath)
    val destImage = File(thumbnailPath)
    try {
      if (Thumbnail.canCreateThumbnail(sourceImage)) {
        val options = ThumbnailOptions.builder()
          .forcePng()
          .convertOptions(CONVERT_OPTIONS)
          .build()
        Thumbnail.createThumbnail(sourceImage, destImage, options)
      }
    } catch (e: IOException) {
      val msg = String.format("Failed to create thumbnail '%s': '%s'", sourceImage.absolutePath, e.message)
      log.error(msg, e)
      throw IllegalArgumentException(msg)
    }
  }

  private fun thumbnailDirectory(): String = filesDirectoryPath + File.separator + THUMBNAILS_DIR

  fun outputDirectory(): String = filesDirectoryPath + OUTPUT_DIR

  private fun createDirectory(directory: String) = try {
    File(directory).mkdirs()
  } catch (e: IOException) {
    log.error("Failed to create directory: $directory", e)
  }

  fun write(domainFile: DomainFile, bytes: ByteArray) {
    try {
      Files.write(Paths.get(filePathOf(domainFile)), bytes)
    } catch (e: IOException) {
      val msg = String.format("Failed to write file '%s': '%s'", domainFile.filename, e)
      log.error(msg, e)
      throw IllegalArgumentException(msg)
    }
  }

  @JvmStatic
  fun reset() {
    File(filesDirectoryPath).deleteRecursively()
    initFilesDirectory(filesDirectoryPath)
  }

  fun createExportClassesFile(): Couple<String, FileOutputStream> {
    val filename = filesDirectoryPath + EXPORT_CLASSES_FILE_NAME
    return Couple(filename, FileOutputStream(filename))
  }

  fun createExportClientFile(): Couple<String, FileOutputStream> {
    val filename = filesDirectoryPath + EXPORT_CLIENT_FILE_NAME
    return Couple(filename, FileOutputStream(filename))
  }

  fun hasThumbnail(it: DomainFile) = File(thumbnailDirectory() + File.separator + it.aggregateId).exists()
}
