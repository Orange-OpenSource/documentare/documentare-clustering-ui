package com.orange.documentare.clusteringui.infrastructure.persistence.postgres;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.documentare.clusteringui.infrastructure.persistence.postgres.dto.*;

public class EventJsonIO {

  private final ObjectMapper objectMapper = new ObjectMapper();

  public String serialize(EventDTO event) {
    try {
      return objectMapper.writeValueAsString(event);
    }
    catch (JsonProcessingException e) {
      throw new IllegalStateException(e);
    }
  }

  @SuppressWarnings("unchecked")
  public <T extends EventDTO> T deserialize(String json, String eventType) {
    try {
      return (T) objectMapper.readValue(json, toClass(eventType));
    }
    catch (JsonProcessingException | ClassNotFoundException e) {
      throw new IllegalStateException(e);
    }
  }

  private Class<? extends EventDTO> toClass(String eventType) throws ClassNotFoundException {
    return switch (eventType) {
      case "ClusterLockedDTO" -> ClusterLockedDTO.class;
      case "ClusterModifiedDTO" -> ClusterModifiedDTO.class;
      case "ClusterNotModifiedDTO" -> ClusterNotModifiedDTO.class;
      case "ClusterPutInNamedClassDTO" -> ClusterPutInNamedClassDTO.class;
      case "ClusterUnlockedDTO" -> ClusterUnlockedDTO.class;
      case "DistancesToClusterCenterAddedDTO" -> DistancesToClusterCenterAddedDTO.class;
      case "FileAddedToClusterDTO" -> FileAddedToClusterDTO.class;
      case "FileCreatedDTO" -> FileCreatedDTO.class;
      case "FileDeletedDTO" -> FileDeletedDTO.class;
      case "FileDuplicatedDTO" -> FileDuplicatedDTO.class;
      case "FileInfoResetDTO" -> FileInfoResetDTO.class;
      case "FileMovedFromWaitingRoomToSortRoomDTO" -> FileMovedFromWaitingRoomToSortRoomDTO.class;
      case "FilePutInNamedClassDTO" -> FilePutInNamedClassDTO.class;
      case "FileRemovedFromClusterDTO" -> FileRemovedFromClusterDTO.class;
      case "MultisetMovedToNextHierarchyLevelDTO" -> MultisetMovedToNextHierarchyLevelDTO.class;
      default -> throw new IllegalStateException("Not supported eventType: " + eventType);
    };
  }
}
