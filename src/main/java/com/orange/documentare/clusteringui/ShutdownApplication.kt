package com.orange.documentare.clusteringui

import org.springframework.boot.ExitCodeGenerator
import org.springframework.boot.SpringApplication
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component

@Component
internal class ShutdownApplication(private val appContext: ApplicationContext) {
  fun initiateShutdown(returnCode: Int) {
    SpringApplication.exit(appContext, ExitCodeGenerator { returnCode })
  }
}
