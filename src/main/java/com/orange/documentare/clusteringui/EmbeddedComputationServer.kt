package com.orange.documentare.clusteringui

import com.orange.documentare.computationserver.clustering.ClusteringApi
import com.orange.documentare.computationserver.distances.DistancesApi
import com.orange.documentare.core.comp.ncd.cache.NcdCache
import com.orange.documentare.core.computationserver.biz.SharedDirectory
import com.orange.documentare.core.computationserver.biz.clustering.ClusteringController
import com.orange.documentare.core.computationserver.biz.distances.DistancesController
import com.orange.documentare.core.computationserver.biz.prepdata.AppPrepData
import com.orange.documentare.core.computationserver.biz.task.TaskController
import com.orange.documentare.core.computationserver.biz.task.Tasks
import com.orange.documentare.tasksdispatcher.AvailableResultsTasksIdDTO
import com.orange.documentare.tasksdispatcher.tasksclient.TasksApiClient
import jakarta.annotation.PostConstruct
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

// TODO: EmbeddedComputationServer, shared directory can be used for nfs sharing for instance, not implemented here (see simdoc-server)

// TODO: EmbeddedComputationServer
// renommer en computation server proxy
// c'est ici que l'on devrait injecter les implem en fonction de si l'embedded server est activé ou pas
@Service
class EmbeddedComputationServer(
  @Value("\${app.embedded-computation-server-enabled}") val enabled: Boolean,
  @Value("\${app.embedded-computation-server-distance-prep-directory}") val distancePrepDir: String,
  @Value("\${app.embedded-computation-server-raw-image-cache-directory}") val rawImageCacheDir: String,
  @Value("\${app.datasource.host}") val persistenceHost: String
) {

  interface TaskApiClient {
    fun getTaskResult(taskId: String): Pair<TaskController.TaskState, Any?>
  }

  private val sharedDirectory = buildSharedDirectory()
  private var appPrepData = AppPrepData(distancePrepDir, rawImageCacheDir, sharedDirectory)

  private val tasks = Tasks()
  private val taskController = TaskController(tasks)


  val taskApiClient: TaskApiClient = buildTaskApiClient()
  val tasksApiClient: TasksApiClient = buildTasksApiClient()
  val clusteringApi: ClusteringApi = ClusteringApi(ClusteringController(appPrepData, sharedDirectory, tasks))
  val distancesApi: DistancesApi = DistancesApi(DistancesController(tasks, appPrepData))

  @PostConstruct
  fun init() {
    NcdCache.initCachePersistence(persistenceHost)
    appPrepData = AppPrepData(distancePrepDir, rawImageCacheDir, sharedDirectory)
  }

  private fun buildTasksApiClient() = object : TasksApiClient {
    override fun availableResultsTasksIdSync(): AvailableResultsTasksIdDTO = AvailableResultsTasksIdDTO(taskController.retrieveAvailableResultsTasksId().availableResultsTasksId)
  }

  private fun buildTaskApiClient(): TaskApiClient {
    return object : TaskApiClient {
      override fun getTaskResult(taskId: String): Pair<TaskController.TaskState, Any?> {
        return taskController.retrieveTaskResult(taskId)
      }
    }
  }

  private fun buildSharedDirectory() = object : SharedDirectory {
    override fun sharedDirectoryAvailable() = false

    override fun sharedDirectoryRootPath(): String? {
      TODO("Not yet implemented")
    }
  }

  companion object {
    fun disabled() = EmbeddedComputationServer(false, "", "", "")
    fun with(distancesPrepDir: String, rawImageCacheDir: String) = EmbeddedComputationServer(true, distancesPrepDir, rawImageCacheDir, "")
  }

}
