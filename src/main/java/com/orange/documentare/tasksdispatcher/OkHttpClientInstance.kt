/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.tasksdispatcher

import okhttp3.OkHttpClient
import java.lang.ref.SoftReference
import java.util.concurrent.TimeUnit

object OkHttpClientInstance {

  // reuse okhttp client, so share connection pool, etc
  // see https://stackoverflow.com/questions/42948985/okhttp-connection-pool-and-file-handles#42949077
  private var softReference: SoftReference<OkHttpClient>? = null

  @Synchronized
  fun retrieveClient(): OkHttpClient {
    if (softReference == null || softReference!!.get() == null) {
      val client = OkHttpClient.Builder()
        .writeTimeout(1, TimeUnit.HOURS)
        .readTimeout(1, TimeUnit.HOURS)
        .build()
      softReference = SoftReference(client)
    }
    return softReference!!.get()!!
  }
}