/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.tasksdispatcher

class ApiWrappersBucket<API, RES>(
  apiWrappersCollection: Collection<ApiWrapper<API, RES>>,
  private val basket: MutableList<ApiWrapper<API, RES>> = apiWrappersCollection.toMutableList(),
  private val allWrappers: List<ApiWrapper<API, RES>> = apiWrappersCollection.toList(),
  val totalCount: Int = apiWrappersCollection.size
) {

  fun hasNext() = basket.isNotEmpty()

  fun next(): ApiWrapper<API, RES> = basket[0].also { basket.removeAt(0) }

  fun addBack(apiWrapper: ApiWrapper<API, RES>) {
    basket.add(apiWrapper)
  }

  fun withTaskId(taskId: String) = allWrappers.firstOrNull { it.taskId == taskId }
}
