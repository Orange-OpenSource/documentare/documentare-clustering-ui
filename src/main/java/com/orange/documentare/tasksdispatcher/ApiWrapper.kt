/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.tasksdispatcher

import com.orange.documentare.computationserver.ComputationServerResponse
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

abstract class ApiWrapper<API, RES> {
  var taskId: String? = null

  protected fun buildNetworkApi(clazz: Class<API>, url: String): API =
    Retrofit.Builder()
      .baseUrl(url)
      .addConverterFactory(JacksonConverterFactory.create())
      .client(OkHttpClientInstance.retrieveClient())
      .build()
      .create(clazz)

  abstract fun callComputationApi(url: String): ComputationServerResponse<RemoteTaskDTO>
  abstract fun callTaskApi(taskId: String, url: String): ComputationServerResponse<RES>
  abstract fun requestId(): String
}
