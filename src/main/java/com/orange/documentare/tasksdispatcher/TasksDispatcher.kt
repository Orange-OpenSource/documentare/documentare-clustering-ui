/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.tasksdispatcher

import com.orange.documentare.computationserver.ComputationServerResponse
import com.orange.documentare.core.comp.clustering.stats.DescriptiveStats
import com.orange.documentare.tasksdispatcher.tasksclient.RemoteTasksApiClient
import com.orange.documentare.tasksdispatcher.tasksclient.TasksApiClient
import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicInteger
import java.util.function.BiConsumer

open class TasksDispatcher<API, RES>(
  apiWrappers: Collection<ApiWrapper<API, RES>>,
  private val serversUrls: List<String>,
  private val resultObserver: BiConsumer<ApiWrapper<API, RES>, RES>,
  private val changeListener: ChangeListener? = null,
  private val tasksApiClient: TasksApiClient?
) {

  interface ChangeListener {
    fun receivedTaskId(taskId: String, url: String)
    fun receivedResultForTaskId(taskId: String, url: String)
    fun willRetrieveATaskId(url: String)
    fun willRetrieveAvailableResultsTasksId(url: String)
    fun willRetrieveResult(forTaskId: String, url: String)
  }

  private val log = LoggerFactory.getLogger(TasksDispatcher::class.java)
  private val apiWrappers: ApiWrappersBucket<API, RES> = ApiWrappersBucket(apiWrappers)
  private val actualReceivedResults = AtomicInteger()
  private val mapTaskDuration = mutableMapOf<String, Long>()

  private var serversTaskDistributionIndex: Int = 0
  private var percentDone = 0

  fun distributeTasks() {
    while (!allResultsReceived()) {
      val availableResults = retrieveAvailableResultsSync()
      val distributedTasks = distributeNewTasksSync()
      waitForAWhileIfNothingHappened(availableResults, distributedTasks)

      showPercentDone()
    }
    showTasksStats()
  }

  private fun showTasksStats() {
    DescriptiveStats().statistics(mapTaskDuration.values.map { it.toInt() }).apply {
      log.info("\t\tTasks stats (task duration + delay to retrieve it): mean $mean ms, std $standardDeviation")
    }
  }

  private fun retrieveAvailableResultsSync(): Int {
    var availableResults = 0
    serversUrls.forEach { url ->
      changeListener?.willRetrieveAvailableResultsTasksId(url)
      taskClientFor(url).availableResultsTasksIdSync().availableResultsTasksId?.map { taskId ->
        val apiWrapper = apiWrappers.withTaskId(taskId)
        apiWrapper?.let {
          availableResults++
          requestResultSync(RemoteTaskDTO(taskId), apiWrapper, url)
        }
      }
    }
    return availableResults
  }

  private fun taskClientFor(url: String): TasksApiClient = tasksApiClient ?: RemoteTasksApiClient(url)

  private fun distributeNewTasksSync(): Int {
    var distributedTasks = 0
    (0..serversUrls.size).forEach { _ ->
      var errorOccurred: Boolean
      do {
        if (apiWrappers.hasNext()) {
          errorOccurred = retrieveATaskIdSync(apiWrappers.next(), nextServerPath())
          if (!errorOccurred) {
            distributedTasks++
          }
        } else {
          return distributedTasks
        }
      } while (!errorOccurred)
    }
    return distributedTasks
  }

  private fun waitForAWhileIfNothingHappened(availableResults: Int, distributedTasks: Int) {
    if (availableResults == 0 && distributedTasks == 0) {
      Thread.sleep(1000)
    }
  }

  private fun retrieveATaskIdSync(apiWrapper: ApiWrapper<API, RES>, url: String): Boolean {
    var errorOccurred = true
    changeListener?.willRetrieveATaskId(url)

    val response: ComputationServerResponse<RemoteTaskDTO> = apiWrapper.callComputationApi(url)
    if (response.hasThrownAnException()) {
      taskIdError(response.throwable(), null, apiWrapper)
    } else {
      if (response.isSuccessful()) {
        val remoteTaskDTO: RemoteTaskDTO = response.dto()
        receiveTaskId(remoteTaskDTO, apiWrapper, url)
        errorOccurred = false
      } else {
        taskIdError(null, response, apiWrapper)
      }
    }
    return errorOccurred
  }

  private fun receiveTaskId(remoteTaskDTO: RemoteTaskDTO, apiWrapper: ApiWrapper<API, RES>, url: String): String {
    val taskId = remoteTaskDTO.id
    apiWrapper.taskId = taskId
    mapTaskDuration[taskId] = System.currentTimeMillis()
    changeListener?.receivedTaskId(taskId, url)
    return taskId
  }

  private fun requestResultSync(remoteTaskDTO: RemoteTaskDTO, apiWrapper: ApiWrapper<API, RES>, url: String) {
    changeListener?.willRetrieveResult(remoteTaskDTO.id, url)

    val response: ComputationServerResponse<RES> = apiWrapper.callTaskApi(remoteTaskDTO.id, url)
    if (response.hasThrownAnException()) {
      taskResultError(response.throwable(), null, remoteTaskDTO, apiWrapper)
    } else {
      if (response.isSuccessful()) {
        receiveResult(response, remoteTaskDTO, apiWrapper, url)
      } else {
        taskResultError(null, response, remoteTaskDTO, apiWrapper)
      }
    }
  }

  private fun taskIdError(throwable: Throwable?, responseError: ComputationServerResponse<RemoteTaskDTO>?, apiWrapper: ApiWrapper<API, RES>) {
    val err503 = responseError?.code() == 503
    if (!err503) {
      log.error("[reqId '{}']: FAILED to get a task id, http error status '${responseError?.code()}'", apiWrapper.requestId(), throwable)
    }
    pushBackTaskToRetryLater(apiWrapper)
  }

  private fun taskResultError(throwable: Throwable?, response: ComputationServerResponse<RES>?, remoteTaskDTO: RemoteTaskDTO, apiWrapper: ApiWrapper<API, RES>) {
    log.error("[reqId '{}']: taskId '{}']: FAILED to receive result; error '{}'", apiWrapper.requestId(), remoteTaskDTO.id, throwable ?: response?.code())
    pushBackTaskToRetryLater(apiWrapper)
  }

  private fun receiveResult(response: ComputationServerResponse<*>, remoteTaskDTO: RemoteTaskDTO, apiWrapper: ApiWrapper<API, RES>, url: String) {
    when (response.code()) {
      200 -> {
        val result = response.dto()
        saveResult(apiWrapper, result)
        changeListener?.receivedResultForTaskId(remoteTaskDTO.id, url)
        mapTaskDuration[remoteTaskDTO.id] = System.currentTimeMillis() - mapTaskDuration[remoteTaskDTO.id]!!
      }

      207 -> {
        pushBackTaskToRetryLater(apiWrapper)
      }

      else -> {
        log.error(
          "[reqId '{}']: taskId '{}']: an error occurred while retrieving result; http status code: {}; response: {}; response body {}",
          apiWrapper.requestId(),
          remoteTaskDTO.id,
          response.code(),
          response,
          response.dto()
        )
        pushBackTaskToRetryLater(apiWrapper)
      }
    }
  }

  private fun saveResult(apiWrapper: ApiWrapper<API, RES>, result: Any?) {
    @Suppress("UNCHECKED_CAST")
    resultObserver.accept(apiWrapper, result as RES)
    actualReceivedResults.incrementAndGet()
  }

  private fun showPercentDone() {
    actualReceivedResults.get().times(100).div(apiWrappers.totalCount).let {
      if (it != percentDone) {
        log.info("\t\tDistributed tasks done: $it%")
        percentDone = it
      }
    }
  }

  open fun allResultsReceived() = apiWrappers.totalCount == actualReceivedResults.get()

  private fun pushBackTaskToRetryLater(apiWrapper: ApiWrapper<API, RES>) {
    apiWrappers.addBack(apiWrapper)
  }

  private fun nextServerPath(): String = serversUrls[serversTaskDistributionIndex]
    .also {
      serversTaskDistributionIndex = (serversTaskDistributionIndex + 1) % serversUrls.size
    }
}
