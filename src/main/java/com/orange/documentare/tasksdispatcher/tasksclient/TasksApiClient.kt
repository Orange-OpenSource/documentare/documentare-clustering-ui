package com.orange.documentare.tasksdispatcher.tasksclient

import com.orange.documentare.tasksdispatcher.AvailableResultsTasksIdDTO

interface TasksApiClient {
  fun availableResultsTasksIdSync(): AvailableResultsTasksIdDTO
}
