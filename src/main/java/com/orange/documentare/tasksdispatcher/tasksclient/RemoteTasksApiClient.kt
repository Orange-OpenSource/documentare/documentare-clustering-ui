/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package com.orange.documentare.tasksdispatcher.tasksclient

import com.orange.documentare.tasksdispatcher.AvailableResultsTasksIdDTO
import com.orange.documentare.tasksdispatcher.OkHttpClientInstance
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.GET

class RemoteTasksApiClient(url: String) : TasksApiClient {

  private val log: Logger = LoggerFactory.getLogger(RemoteTasksApiClient::class.java)
  private val api = buildApi(url)

  override fun availableResultsTasksIdSync(): AvailableResultsTasksIdDTO {
    val result = runCatching { api.availableResultsTasksIdSync().execute().body() }
    if (result.isFailure) {
      log.error("An error occurred while trying to retrieve available tasks results", result.exceptionOrNull())
    }

    return result.getOrDefault(AvailableResultsTasksIdDTO(emptyList()))!!
  }

  private interface Api {
    @GET("/tasks/availableResultsTasksId")
    fun availableResultsTasksIdSync(): Call<AvailableResultsTasksIdDTO>
  }

  private fun buildApi(url: String): Api {
    return Retrofit.Builder()
      .baseUrl(url)
      .addConverterFactory(JacksonConverterFactory.create())
      .client(OkHttpClientInstance.retrieveClient())
      .build()
      .create(Api::class.java)
  }
}
