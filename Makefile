VERSION=`git describe --always`
MVN=mvn -B -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn

# Used to build debian package
# here we consider that artifacts are already built (to speed up continuous integration)
# so we do nothing here
build:

build-tests:
	${MVN} ${MAVEN_OPTS} --version
	${MVN} ${MAVEN_OPTS} install

# Build debian package
deb:
	dch -v ${VERSION} "git update, version ${VERSION}"
	bash .dh_build.sh
	mv ../clustering-ui*.deb .

# Debian package install
install:
	rm -rf debian/clustering-ui/usr && mkdir -p debian/clustering-ui/usr/share/java
	cp target/clustering-ui-*.jar debian/clustering-ui/usr/share/java/clustering-ui.jar
	rm -rf debian/clustering-ui/etc && mkdir -p debian/clustering-ui/etc/systemd/system/ && cp debian/clustering-ui.service debian/clustering-ui/etc/systemd/system/

prep-docker-build:
	mkdir prep-docker-build
	(cd prep-docker-build && cp ../target/clustering-ui-0.0.1-SNAPSHOT.jar clustering-ui.jar)

clean:
	#debclean
