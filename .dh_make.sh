#!/bin/sh

export DEBEMAIL=denis.boisset@orange.com
export DEBFULLNAME="Denis Boisset"

VERSION=`git describe`
PACKAGENAME=clustering-ui

dh_make -p ${PACKAGENAME}_${VERSION} -c gpl2 --native -s

