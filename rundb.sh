#!/bin/sh

# Brew info, direct launch
# LC_ALL="C" /usr/local/opt/postgresql@15/bin/postgres -D /usr/local/var/postgresql@15

# Macos flavor (need to be adapted for linux)
POSTGRES_SERVICE="postgresql@15"
PSQL="$(brew --prefix $POSTGRES_SERVICE)/bin/psql"
CMD_START_SERVICE="brew services run"
CMD_GREP_POSTGRES_PROCESS="pgrep -l postgres"

die() {
	echo
	echo "❌ $1"
	echo
	exit 1
}

start_service() {
	if $CMD_START_SERVICE "$1" > /dev/null
	then
		echo "✅ [SERVICE] $1 started"
	else
		die "[SERVICE] failed to start $1"
	fi
}

wait_for_postgres() {
  while :
  do
  	if $PSQL postgres -c 'SELECT * FROM pg_roles;' &> /dev/null
    then
  		echo "✅ [$POSTGRES_SERVICE] running"
  		break
  	fi
  	sleep 1
  done
}
create_postgres_user() {
  $PSQL postgres -c "CREATE USER postgres PASSWORD 'sandbox';" &> /dev/null
}
update_postgres_user_password() {
  if ! $PSQL postgres -c "ALTER USER postgres PASSWORD 'sandbox';" > /dev/null
  then
    die "[$POSTGRES_SERVICE] failed to update password"
  fi
}
update_postgres_createdb_permission() {
  if ! $PSQL postgres -c "ALTER USER postgres createdb;" > /dev/null
  then
    die "[$POSTGRES_SERVICE] failed to add 'createdb' permission"
  fi
}


####################################### MAIN #######################################
if $CMD_GREP_POSTGRES_PROCESS > /dev/null
then
  echo "✅ [SERVICE] $POSTGRES_SERVICE already started"
else
  start_service "$POSTGRES_SERVICE"
fi
wait_for_postgres
create_postgres_user
update_postgres_user_password
update_postgres_createdb_permission
echo "✅ [$POSTGRES_SERVICE] configured"

#docker run --name docui-redis      --rm -d -v docui-redis:/data/redis                   -p6379:6379   redis:7-bookworm redis-server --maxmemory 2gb --maxmemory-policy allkeys-lfu
#docker run --name docui-postgresql --rm -d -v docui-postgresql:/var/lib/postgresql/data -p5432:5432 -e POSTGRES_PASSWORD=sandbox postgres:15
